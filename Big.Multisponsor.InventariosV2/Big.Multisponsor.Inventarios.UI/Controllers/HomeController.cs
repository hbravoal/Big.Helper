﻿using Big.Multisponsor.Inventarios.BusinessLogic.Controller;
using Big.Multisponsor.Inventarios.Generated.Domain;
using Big.Multisponsor.Inventarios.UI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.Multisponsor.Inventarios.UI.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            //var product = Product.GetAllRecords().ToList();
            //ViewBag.datasource = product;
            //return View();
            List<InvFactura> p = PreBillingsController.GetFacturasPorProductosFechas(44, Convert.ToDateTime("12/11/17 2:52:35 PM"), Convert.ToDateTime("12/11/17 2:52:35 PM"), 0, "");
            ViewBag.datasource = p;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}