﻿using Big.Multisponsor.Inventarios.BusinessLogic.Controller;
using Big.Multisponsor.Inventarios.Generated.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.Multisponsor.Inventarios.UI.Controllers
{
    public class PreBillingController : Controller
    {
        // GET: PreBilling
        public ActionResult Index()
        {

            List<InvFactura> p = PreBillingsController.GetFacturasPorProductosFechas(44, Convert.ToDateTime("12/11/17 2:52:35 PM"), Convert.ToDateTime("12/11/17 2:52:35 PM"), 0, "");
            ViewBag.datasource = p;
            return View();
        }

        // GET: PreBilling/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: PreBilling/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: PreBilling/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PreBilling/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: PreBilling/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: PreBilling/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: PreBilling/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
