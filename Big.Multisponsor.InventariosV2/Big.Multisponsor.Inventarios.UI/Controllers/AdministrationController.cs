﻿using Big.Multisponsor.Inventarios.BusinessLogic.Controller;
using Big.Multisponsor.Inventarios.Generated.Domain;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Big.Multisponsor.Inventarios.UI.Controllers
{
    public class AdministrationController : Controller
    {
        // GET: Administration
        public ActionResult Index()
        {
             int totalrecords = 0;
             DataSet data = CompanyController.GetEmpresasByCriterias("s", "s", "s,", 1, 1, out totalrecords);
             List<InvEmpresa> invList = new List<InvEmpresa>();
             if (data.Tables.Count > 0)
             {
                 if (data.Tables[0].Rows.Count > 0)
                 {
                     foreach (DataRow item in data.Tables[0].Rows)
                     {
                         invList.Add(
                             new InvEmpresa
                             {
                                 CodigoEmpresa = Convert.ToString(item["CODIGO_EMPRESA"]),
                                 RazonSocial = Convert.ToString(item["RAZON_SOCIAL"]),
                                 Nit = Convert.ToString(item["NIT"]),
                                 Direccion = Convert.ToString(item["DIRECCION"]),
                                 Telefono = Convert.ToString(item["TELEFONO"]),
                             }
                             );
                     }
                 }
             }
             ViewBag.datasource = invList;
            return View(invList);
            
        }
        public List<InvEmpresa> Data()
        {
            int totalrecords = 0;
            DataSet data = CompanyController.GetEmpresasByCriterias("s", "s", "s,", 1, 1, out totalrecords);
            List<InvEmpresa> invList = new List<InvEmpresa>();
            if (data.Tables.Count > 0)
            {
                if (data.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow item in data.Tables[0].Rows)
                    {
                        invList.Add(
                            new InvEmpresa
                            {
                                CodigoEmpresa = Convert.ToString(item["CODIGO_EMPRESA"]),
                                RazonSocial = Convert.ToString(item["RAZON_SOCIAL"]),
                                Nit = Convert.ToString(item["NIT"]),
                                Direccion = Convert.ToString(item["DIRECCION"]),
                                Telefono = Convert.ToString(item["TELEFONO"]),
                            }
                            );
                    }
                }
            }
            return invList;
            

        }

    

        [HttpPost]
        public ActionResult Index(int id)
        {
            return View();
        }

        // GET: Administration/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administration/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administration/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administration/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
