var swPeticionDelete = 0;

$(document).ready(function(evt){
	$(window).bind("resize", function() {
	    updateMask();
	});

	$(window).load(function(e){
	    updateMask();
	});

	updateMask();

	$(document).mouseup(function (e)
	{
	    var container = $(".navSite");

	    if (!container.is(e.target) // if the target of the click isn't the container...
	        && container.has(e.target).length === 0) // ... nor a descendant of the container
	    {
	    	$(".navSite li .subMenu").css("display", "none");
	    }
	});

	
	$(".navSite > li > a").on("click", function(evt){
		evt.preventDefault();
		$(".navSite li .subMenu").css("display", "none");
		$(this).parent().find(".subMenu").css("display", "block");
	});
});

function updateMask(){
    var maskHeight = $(window).height();
    var maskWidth = $(window).width();
}