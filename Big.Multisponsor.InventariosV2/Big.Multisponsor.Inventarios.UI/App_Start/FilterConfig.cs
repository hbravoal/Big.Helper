﻿using System.Web;
using System.Web.Mvc;

namespace Big.Multisponsor.Inventarios.UI
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
