if (!window) {

        var window = exports.window = {};
    }
    window.samplesList =[
  {
    "name": "Chart",
    "directory": "Chart",
    "category": "Data Visualization",
    "type": "update",
    "samples": [
      {
        "url": "Line",
        "name": "Line",
        "category": "Line Charts",
        "uid": "00000",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Spline",
        "name": "Spline",
        "category": "Line Charts",
        "uid": "00001",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StepLine",
        "name": "Step Line",
        "category": "Line Charts",
        "uid": "00002",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "DashedLine",
        "name": "Dashed Line",
        "category": "Line Charts",
        "uid": "00003",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "InversedSpline",
        "name": "Inversed Spline",
        "category": "Line Charts",
        "uid": "00004",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "LineZone",
        "name": "Line Zone",
        "category": "Line Charts",
        "uid": "00005",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "MultiColoredLine",
        "name": "Multi Colored Line",
        "category": "Line Charts",
        "uid": "00006",
        "order": 0,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Area",
        "name": "Area",
        "category": "Area Charts",
        "uid": "00007",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "SplineArea",
        "name": "Spline Area",
        "category": "Area Charts",
        "uid": "00008",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StepArea",
        "name": "Step Area",
        "category": "Area Charts",
        "uid": "00009",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "RangeArea",
        "name": "Range Area",
        "category": "Area Charts",
        "uid": "000010",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StackedArea",
        "name": "Stacked Area",
        "category": "Area Charts",
        "uid": "000011",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StackedArea100",
        "name": "100% Stacked Area",
        "category": "Area Charts",
        "uid": "000012",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "AreaEmptyPoints",
        "name": "Area EmptyPoints",
        "category": "Area Charts",
        "uid": "000013",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "AreaZone",
        "name": "Area Zone",
        "category": "Area Charts",
        "uid": "000014",
        "order": 1,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Column",
        "name": "Column",
        "category": "Bar Charts",
        "uid": "000015",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "RoundedColumn",
        "name": "Rounded Column",
        "category": "Bar Charts",
        "uid": "000016",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "BackToBackColumn",
        "name": "Back To Back Column",
        "category": "Bar Charts",
        "uid": "000017",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "RangeColumn",
        "name": "Range Column",
        "category": "Bar Charts",
        "uid": "000018",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "InversedRangeColumn",
        "name": "Inversed RangeColumn",
        "category": "Bar Charts",
        "uid": "000019",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Bar",
        "name": "Bar",
        "category": "Bar Charts",
        "uid": "000020",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StackedColumn",
        "name": "Stacked Column",
        "category": "Bar Charts",
        "uid": "000021",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StackedColumn100",
        "name": "100% Stacked Column",
        "category": "Bar Charts",
        "uid": "000022",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StackedBar",
        "name": "Stacked Bar",
        "category": "Bar Charts",
        "uid": "000023",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StackedBar100",
        "name": "100% Stacked Bar",
        "category": "Bar Charts",
        "uid": "000024",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "NegativeStack",
        "name": "Negative Stack",
        "category": "Bar Charts",
        "uid": "000025",
        "order": 2,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Scatter",
        "name": "Scatter",
        "category": "Scatter and Bubble",
        "uid": "000026",
        "order": 3,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Bubble",
        "name": "Bubble",
        "category": "Scatter and Bubble",
        "uid": "000027",
        "order": 3,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Waterfall",
        "name": "Waterfall",
        "category": "Other Types",
        "uid": "000028",
        "order": 4,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "BoxAndWhisker",
        "name": "Box and Whisker",
        "category": "Other Types",
        "uid": "000029",
        "order": 4,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "ErrorBar",
        "name": "Error Bar",
        "category": "Other Types",
        "uid": "000030",
        "order": 4,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Trendlines",
        "name": "Trendlines",
        "category": "Other Types",
        "uid": "000031",
        "order": 4,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "CombinationSeries",
        "name": "Combination Series",
        "category": "Other Types",
        "uid": "000032",
        "order": 4,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "ParetoChart",
        "name": "Pare to Chart",
        "category": "Other Types",
        "uid": "000033",
        "order": 4,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Hilo",
        "name": "Hilo",
        "category": "Financial Charts",
        "uid": "000034",
        "order": 5,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "HiloOpenClose",
        "name": "Hilo Open Close",
        "category": "Financial Charts",
        "uid": "000035",
        "order": 5,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Candle",
        "name": "Candle",
        "category": "Financial Charts",
        "uid": "000036",
        "order": 5,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "AccumulationDistribution",
        "name": "Accumulation Distribution",
        "category": "Technical Indicators",
        "uid": "000037",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "ATR",
        "name": "ATR",
        "category": "Technical Indicators",
        "uid": "000038",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Bollinger",
        "name": "Bollinger",
        "category": "Technical Indicators",
        "uid": "000039",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "EMA",
        "name": "EMA",
        "category": "Technical Indicators",
        "uid": "000040",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "MACD",
        "name": "MACD",
        "category": "Technical Indicators",
        "uid": "000041",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Momentum",
        "name": "Momentum",
        "category": "Technical Indicators",
        "uid": "000042",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "RSI",
        "name": "RSI",
        "category": "Technical Indicators",
        "uid": "000043",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "SMA",
        "name": "SMA",
        "category": "Technical Indicators",
        "uid": "000044",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Stochastic",
        "name": "Stochastic",
        "category": "Technical Indicators",
        "uid": "000045",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "TMA",
        "name": "TMA",
        "category": "Technical Indicators",
        "uid": "000046",
        "order": 6,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Benchmark",
        "name": "Benchmark",
        "category": "Performance",
        "uid": "000047",
        "order": 7,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Pie",
        "name": "Pie",
        "category": "Accumulation Charts",
        "uid": "000048",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Doughnut",
        "name": "Doughnut",
        "category": "Accumulation Charts",
        "uid": "000049",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Pyramid",
        "name": "Pyramid",
        "category": "Accumulation Charts",
        "uid": "000050",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Funnel",
        "name": "Funnel",
        "category": "Accumulation Charts",
        "uid": "000051",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PieWithLegend",
        "name": "Pie with Legend",
        "category": "Accumulation Charts",
        "uid": "000052",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "SemiPie",
        "name": "Semi Pie",
        "category": "Accumulation Charts",
        "uid": "000053",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "SmartLabels",
        "name": "Smart Labels",
        "category": "Accumulation Charts",
        "uid": "000054",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Drilldown",
        "name": "Drilldown",
        "category": "Accumulation Charts",
        "uid": "000055",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Grouping",
        "name": "Grouping",
        "category": "Accumulation Charts",
        "uid": "000056",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "EmptyPoints",
        "name": "Empty Points",
        "category": "Accumulation Charts",
        "uid": "000057",
        "order": 8,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarLine",
        "name": "Line",
        "category": "Polar Radar",
        "uid": "000058",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarSpline",
        "name": "Spline",
        "category": "Polar Radar",
        "uid": "000059",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarArea",
        "name": "Area",
        "category": "Polar Radar",
        "uid": "000060",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarStackedArea",
        "name": "Stacked Area",
        "category": "Polar Radar",
        "uid": "000061",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarScatter",
        "name": "Scatter",
        "category": "Polar Radar",
        "uid": "000062",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarColumn",
        "name": "Column",
        "category": "Polar Radar",
        "uid": "000063",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "WindRose",
        "name": "WindRose",
        "category": "Polar Radar",
        "uid": "000064",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "PolarRadarRangeColumn",
        "name": "RangeColumn",
        "category": "Polar Radar",
        "uid": "000065",
        "order": 9,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "LocalData",
        "name": "Local Data",
        "category": "Data Binding",
        "uid": "000066",
        "order": 10,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "RemoteData",
        "name": "Remote Data",
        "category": "Data Binding",
        "uid": "000067",
        "order": 10,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "NumericAxis",
        "name": "Numeric Axis",
        "category": "Chart Axes",
        "uid": "000068",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "DateTimeAxis",
        "name": "DateTime Axis",
        "category": "Chart Axes",
        "uid": "000069",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "DateTimeCategoryAxis",
        "name": "DateTime Category Axis",
        "category": "Chart Axes",
        "uid": "000070",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "CategoryAxis",
        "name": "Category Axis",
        "category": "Chart Axes",
        "uid": "000071",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "IndexedCategoryAxis",
        "name": "Indexed Category Axis",
        "category": "Chart Axes",
        "uid": "000072",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "LogAxis",
        "name": "Log Axis",
        "category": "Chart Axes",
        "uid": "000073",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "MultipleAxes",
        "name": "Multiple Axes",
        "category": "Chart Axes",
        "uid": "000074",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "InversedAxis",
        "name": "Inversed Axis",
        "category": "Chart Axes",
        "uid": "000075",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "StripLine",
        "name": "Strip Line",
        "category": "Chart Axes",
        "uid": "000076",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "SmartAxisLabels",
        "name": "Smart Axis Labels",
        "category": "Chart Axes",
        "uid": "000077",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "MultiLevelLabels",
        "name": "MultiLevel Labels",
        "category": "Chart Axes",
        "uid": "000078",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "AxesCrossing",
        "name": "Axes Crossing",
        "category": "Chart Axes",
        "uid": "000079",
        "order": 11,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Sorting",
        "name": "Sorting",
        "category": "Chart Customization",
        "uid": "000080",
        "order": 12,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Symbols",
        "name": "Symbols",
        "category": "Chart Customization",
        "uid": "000081",
        "order": 12,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "DataLabelTemplate",
        "name": "DataLabel Template",
        "category": "Chart Customization",
        "uid": "000082",
        "order": 12,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "VerticalChart",
        "name": "Vertical Chart",
        "category": "Chart Customization",
        "uid": "000083",
        "order": 12,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "EmptyPointChart",
        "name": "Empty Point",
        "category": "Chart Customization",
        "uid": "000084",
        "order": 12,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Print",
        "name": "Print",
        "category": "Print and Export",
        "uid": "000085",
        "order": 13,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Export",
        "name": "Export",
        "category": "Print and Export",
        "uid": "000086",
        "order": 13,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Selection",
        "name": "Selection",
        "category": "User Interaction",
        "uid": "000087",
        "order": 14,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "RangeSelection",
        "name": "Range Selection",
        "category": "User Interaction",
        "uid": "000088",
        "order": 14,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Crosshair",
        "name": "Crosshair",
        "category": "User Interaction",
        "uid": "000089",
        "order": 14,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Trackball",
        "name": "Trackball",
        "category": "User Interaction",
        "uid": "000090",
        "order": 14,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      },
      {
        "url": "Zooming",
        "name": "Zooming",
        "category": "User Interaction",
        "uid": "000091",
        "order": 14,
        "component": "Chart",
        "dir": "Chart",
        "parentId": "00"
      }
    ],
    "order": 0,
    "uid": "00"
  },
  {
    "name": "Maps",
    "directory": "Maps",
    "category": "Data Visualization",
    "type": "preview",
    "samples": [
      {
        "url": "Default",
        "name": "Default Functionalities",
        "category": "Maps",
        "uid": "00110",
        "order": 0,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Projection",
        "name": "Projection",
        "category": "Features",
        "uid": "00111",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Multilayer",
        "name": "Multi-layers",
        "category": "Features",
        "uid": "00112",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Marker",
        "name": "Marker",
        "category": "Features",
        "uid": "00113",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "MarkerTemplate",
        "name": "Marker template",
        "category": "Features",
        "uid": "00114",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Datalabel",
        "name": "Labels",
        "category": "Features",
        "uid": "00115",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Bubble",
        "name": "Bubble",
        "category": "Features",
        "uid": "00116",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Navigation",
        "name": "Navigation Lines",
        "category": "Features",
        "uid": "00117",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Legend",
        "name": "Legend",
        "category": "Features",
        "uid": "00118",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Annotation",
        "name": "Annotations",
        "category": "Features",
        "uid": "00119",
        "order": 1,
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Tooltip",
        "name": "Tooltip",
        "category": "User Interaction",
        "order": 2,
        "uid": "001110",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "SelectionAndHighlight",
        "name": "Selection & Highlight",
        "category": "User Interaction",
        "order": 2,
        "uid": "001111",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "ZoomingAndPanning",
        "name": "Zooming & Panning",
        "category": "User Interaction",
        "order": 2,
        "uid": "001112",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "DrillDown",
        "name": "Drill Down",
        "category": "User Interaction",
        "order": 2,
        "uid": "001113",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "HeatMap",
        "name": "Heat Map",
        "category": "Use Cases",
        "order": 3,
        "uid": "001114",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "FlightRoutes",
        "name": "Flight routes",
        "category": "Use Cases",
        "order": 3,
        "uid": "001115",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "EarthQuakeIndication",
        "name": "Earthquake indication",
        "category": "Use Cases",
        "order": 3,
        "uid": "001116",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "HighlightedRegion",
        "name": "Highlighted region",
        "category": "Use Cases",
        "order": 3,
        "uid": "001117",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Mapwithpie",
        "name": "Map with Pie Chart",
        "category": "Use Cases",
        "order": 3,
        "uid": "001118",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      },
      {
        "url": "Seatbooking",
        "name": "Bus seat booking",
        "category": "Use Cases",
        "order": 3,
        "uid": "001119",
        "component": "Maps",
        "dir": "Maps",
        "parentId": "01"
      }
    ],
    "order": 0,
    "uid": "01"
  },
  {
    "name": "CircularGauge",
    "directory": "CircularGauge",
    "category": "Data Visualization",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "DefaultFunctionalities",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00220",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "Range",
        "name": "Range",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00221",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "TicksAndLabels",
        "name": "TicksAndLabels",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00222",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "Annotation",
        "name": "Annotation",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00223",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "GaugeCustomization",
        "name": "GaugeCustomization",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00224",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "DirectionCompass",
        "name": "DirectionCompass",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00225",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "PointerImage",
        "name": "PointerImage",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00226",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "PointerCustomization",
        "name": "PointerCustomization",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00227",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "MultipleAxis",
        "name": "MultipleAxis",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00228",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "PointerDrag",
        "name": "PointerDrag",
        "category": "CircularGauge",
        "order": 0,
        "uid": "00229",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "Tooltip",
        "name": "Tooltip",
        "category": "CircularGauge",
        "order": 0,
        "uid": "002210",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      },
      {
        "url": "DataSample",
        "name": "DataSample",
        "category": "CircularGauge",
        "order": 0,
        "uid": "002211",
        "component": "CircularGauge",
        "dir": "CircularGauge",
        "parentId": "02"
      }
    ],
    "order": 0,
    "uid": "02"
  },
  {
    "name": "LinearGauge",
    "directory": "LinearGauge",
    "category": "Data Visualization",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "DefaultFunctionalities",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00330",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "Container",
        "name": "Container",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00331",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "Range",
        "name": "Range",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00332",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "DataSample",
        "name": "DataSample",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00333",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "AxesAndPointers",
        "name": "AxesAndPointers",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00334",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "Annotation",
        "name": "Annotation",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00335",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "Tooltip",
        "name": "Tooltip",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00336",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      },
      {
        "url": "Styles",
        "name": "Styles",
        "category": "LinearGauge",
        "order": 0,
        "uid": "00337",
        "component": "LinearGauge",
        "dir": "LinearGauge",
        "parentId": "03"
      }
    ],
    "order": 0,
    "uid": "03"
  },
  {
    "name": "Grid",
    "directory": "Grid",
    "category": "Grid",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Grid",
        "uid": "00440",
        "order": 0,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Grouping",
        "name": "Grouping",
        "category": "Grid",
        "uid": "00441",
        "order": 0,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "GridLines",
        "name": "GridLines",
        "category": "Grid",
        "uid": "00442",
        "order": 0,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Events",
        "name": "Events",
        "category": "Grid",
        "uid": "00443",
        "order": 0,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Clipboard",
        "name": "Clipboard",
        "category": "Grid",
        "uid": "00444",
        "order": 0,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ContextMenu",
        "name": "Context Menu",
        "category": "Grid",
        "uid": "00445",
        "order": 0,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DefaultScrolling",
        "name": "Default Scrolling",
        "category": "Scrolling",
        "uid": "00446",
        "order": 1,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "VirtualScrolling",
        "name": "Virtual Scrolling",
        "category": "Scrolling",
        "uid": "00447",
        "order": 1,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "LocalData",
        "name": "List Binding",
        "category": "DataBinding",
        "uid": "00448",
        "order": 2,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "RemoteData",
        "name": "Remote Data",
        "category": "DataBinding",
        "uid": "00449",
        "order": 2,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "AutoWrap",
        "name": "AutoWrap Column Cells",
        "category": "Columns",
        "uid": "004410",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ShowHideColumn",
        "name": "Show or Hide Column",
        "category": "Columns",
        "uid": "004411",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ColumnTemplate",
        "name": "Column Template",
        "category": "Columns",
        "uid": "004412",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "StackedHeader",
        "name": "Stacked Header",
        "category": "Columns",
        "uid": "004413",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Reorder",
        "name": "Reorder",
        "category": "Columns",
        "uid": "004414",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ColumnChooser",
        "name": "Column Chooser",
        "category": "Columns",
        "uid": "004415",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "HeaderTemplate",
        "name": "Header Template",
        "category": "Columns",
        "uid": "004416",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ColumnResize",
        "name": "Column Resize",
        "category": "Columns",
        "uid": "004417",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ColumnSpanning",
        "name": "Column Spanning",
        "category": "Columns",
        "uid": "004418",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "FrozenRows",
        "name": "Frozen Rows And Columns",
        "category": "Columns",
        "uid": "004419",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ColumnMenu",
        "name": "Column Menu",
        "category": "Columns",
        "uid": "004420",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "ForeignKey",
        "name": "Foreign Key Column",
        "category": "Columns",
        "uid": "004421",
        "order": 3,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "RowTemplate",
        "name": "Row Template",
        "category": "Rows",
        "uid": "004422",
        "order": 4,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DetailTemplate",
        "name": "Detail Template",
        "category": "Rows",
        "uid": "004423",
        "order": 4,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "RowHover",
        "name": "Row Hover",
        "category": "Rows",
        "uid": "004424",
        "order": 4,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "RowHeight",
        "name": "Row Height",
        "category": "Rows",
        "uid": "004425",
        "order": 4,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "RowDragDrop",
        "name": "Row Drag And Drop",
        "category": "Rows",
        "uid": "004426",
        "order": 4,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "MultiSorting",
        "name": "Multi Sorting",
        "category": "Sorting",
        "uid": "004427",
        "order": 5,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "SortingAPI",
        "name": "Sorting API",
        "category": "Sorting",
        "uid": "004428",
        "order": 5,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DefaultFiltering",
        "name": "Default Filtering",
        "category": "Filtering",
        "uid": "004429",
        "order": 6,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "FilterMenu",
        "name": "Filter Menu",
        "category": "Filtering",
        "uid": "004430",
        "order": 6,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Search",
        "name": "Search",
        "category": "Filtering",
        "uid": "004431",
        "order": 6,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DefaultPaging",
        "name": "Default Paging",
        "category": "Paging",
        "uid": "004432",
        "order": 7,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "PagingAPI",
        "name": "Paging API",
        "category": "Paging",
        "uid": "004433",
        "order": 7,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Selection",
        "name": "Default Selection",
        "category": "Selection",
        "uid": "004434",
        "order": 8,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "SelectionAPI",
        "name": "Selection API",
        "category": "Selection",
        "uid": "004435",
        "order": 8,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "CheckboxSelection",
        "name": "Checkbox Selection",
        "category": "Selection",
        "uid": "004436",
        "order": 8,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DefaultAggregate",
        "name": "Default Aggregate",
        "category": "Aggregates",
        "uid": "004437",
        "order": 9,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "GroupAndCaptionAggregate",
        "name": "Group and Caption Aggregate",
        "category": "Aggregates",
        "uid": "004438",
        "order": 9,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "InlineEditing",
        "name": "Inline Editing",
        "category": "Editing",
        "uid": "004439",
        "order": 10,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DialogEditing",
        "name": "Dialog Editing",
        "category": "Editing",
        "uid": "004440",
        "order": 10,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "BatchEditing",
        "name": "Batch Editing",
        "category": "Editing",
        "uid": "004441",
        "order": 10,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "CommandColumn",
        "name": "Command Column",
        "category": "Editing",
        "uid": "004442",
        "order": 10,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "DefaultExporting",
        "name": "Default Exporting",
        "category": "Exporting",
        "uid": "004443",
        "order": 11,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "AdvancedExporting",
        "name": "Advanced Exporting",
        "category": "Exporting",
        "uid": "004444",
        "order": 11,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      },
      {
        "url": "Print",
        "name": "Print",
        "category": "Exporting",
        "uid": "004445",
        "order": 11,
        "component": "Grid",
        "dir": "Grid",
        "parentId": "04"
      }
    ],
    "order": 1,
    "uid": "04"
  },
  {
    "name": "Schedule",
    "directory": "Schedule",
    "category": "Calendar",
    "type": "preview",
    "samples": [
      {
        "url": "Default",
        "name": "Default Functionalities",
        "category": "Schedule",
        "uid": "00550",
        "order": 0,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "DefaultEvents",
        "name": "Normal",
        "category": "Appointments",
        "uid": "00551",
        "order": 1,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "RecurrenceEvents",
        "name": "Recurrence",
        "category": "Appointments",
        "uid": "00552",
        "order": 1,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "Timezone",
        "name": "Timezone",
        "category": "Appointments",
        "uid": "00553",
        "order": 1,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "Views",
        "name": "Basic",
        "category": "Views",
        "uid": "00554",
        "order": 2,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "Agenda",
        "name": "Agenda",
        "category": "Views",
        "uid": "00555",
        "order": 2,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "MonthAgenda",
        "name": "Month Agenda",
        "category": "Views",
        "uid": "00556",
        "order": 2,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "ViewBasedSettings",
        "name": "View-based Settings",
        "category": "Views",
        "uid": "00557",
        "order": 2,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "LocalData",
        "name": "Local Data",
        "category": "Data Binding",
        "uid": "00558",
        "order": 3,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "RemoteData",
        "name": "Remote Data",
        "category": "Data Binding",
        "uid": "00559",
        "order": 3,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "CellTemplate",
        "name": "Cells",
        "category": "Template",
        "uid": "005510",
        "order": 4,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "DateHeader",
        "name": "Date Header",
        "category": "Template",
        "uid": "005511",
        "order": 4,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "EventTemplate",
        "name": "Events",
        "category": "Template",
        "uid": "005512",
        "order": 4,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "Tooltip",
        "name": "Tooltip",
        "category": "Template",
        "uid": "005513",
        "order": 4,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "EditorValidation",
        "name": "Field Validation",
        "category": "Editor",
        "uid": "005514",
        "order": 5,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "EditorCustomField",
        "name": "Additional Fields",
        "category": "Editor",
        "uid": "005515",
        "order": 5,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "EditorTemplate",
        "name": "Editor Template",
        "category": "Editor",
        "uid": "005516",
        "order": 5,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "HeaderBar",
        "name": "Header Bar",
        "category": "Customization",
        "uid": "005517",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "ScrollTime",
        "name": "Scroll Time",
        "category": "Customization",
        "uid": "005518",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "WorkDays",
        "name": "Work Days",
        "category": "Customization",
        "uid": "005519",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "HideWeekend",
        "name": "Hide Weekend",
        "category": "Customization",
        "uid": "005520",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "WorkHours",
        "name": "Work Hours",
        "category": "Customization",
        "uid": "005521",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "DayHourLimit",
        "name": "Day Hour Limit",
        "category": "Customization",
        "uid": "005522",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "CellDimension",
        "name": "Cell Dimension",
        "category": "Customization",
        "uid": "005523",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "ReadonlyEvents",
        "name": "Readonly Events",
        "category": "Customization",
        "uid": "005524",
        "order": 6,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "RecurrenceRuleGenerator",
        "name": "Rule Generator",
        "category": "Recurrence editor",
        "uid": "005525",
        "order": 7,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "RecurrencePopulateRule",
        "name": "Populate Rule",
        "category": "Recurrence editor",
        "uid": "005526",
        "order": 7,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "KeyboardInteraction",
        "name": "Keyboard Interaction",
        "category": "Miscellaneous",
        "uid": "005527",
        "order": 8,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      },
      {
        "url": "Events",
        "name": "Events",
        "category": "Miscellaneous",
        "uid": "005528",
        "order": 8,
        "component": "Schedule",
        "dir": "Schedule",
        "parentId": "05"
      }
    ],
    "order": 2,
    "uid": "05"
  },
  {
    "name": "Calendar",
    "directory": "Calendar",
    "category": "Calendar",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Calendar",
        "order": 0,
        "component": "Calendar",
        "dir": "Calendar",
        "parentId": "06",
        "uid": "00660"
      },
      {
        "url": "DisabledDates",
        "name": "Disabled Dates",
        "category": "Calendar",
        "order": 0,
        "component": "Calendar",
        "dir": "Calendar",
        "parentId": "06",
        "uid": "00661"
      },
      {
        "url": "SpecialDates",
        "name": "Special Dates",
        "category": "Calendar",
        "order": 0,
        "component": "Calendar",
        "dir": "Calendar",
        "parentId": "06",
        "uid": "00662"
      },
      {
        "url": "DateRange",
        "name": "Date Range",
        "category": "Calendar",
        "order": 0,
        "component": "Calendar",
        "dir": "Calendar",
        "parentId": "06",
        "uid": "00663"
      },
      {
        "url": "Globalization",
        "name": "Globalization",
        "category": "Calendar",
        "order": 0,
        "component": "Calendar",
        "dir": "Calendar",
        "parentId": "06",
        "uid": "00664"
      }
    ],
    "order": 2,
    "uid": "06"
  },
  {
    "name": "AutoComplete",
    "directory": "AutoComplete",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "AutoComplete",
        "uid": "00770",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      },
      {
        "url": "GroupingAndIcon",
        "name": "Grouping and Icon",
        "category": "AutoComplete",
        "uid": "00771",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      },
      {
        "url": "DataBinding",
        "name": "Data Binding",
        "category": "AutoComplete",
        "uid": "00772",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "AutoComplete",
        "uid": "00773",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      },
      {
        "url": "Highlight",
        "name": "Highlight",
        "category": "AutoComplete",
        "uid": "00774",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      },
      {
        "url": "CustomFiltering",
        "name": "Custom Filtering",
        "category": "AutoComplete",
        "uid": "00775",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      },
      {
        "url": "DiacriticsFiltering",
        "name": "Diacritics Filtering",
        "category": "AutoComplete",
        "uid": "00776",
        "order": 0,
        "component": "AutoComplete",
        "dir": "AutoComplete",
        "parentId": "07"
      }
    ],
    "order": 3,
    "uid": "07"
  },
  {
    "name": "Button",
    "directory": "Button",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Button",
        "order": 0,
        "component": "Button",
        "dir": "Button",
        "parentId": "08",
        "uid": "00880"
      },
      {
        "url": "CheckBox",
        "name": "CheckBox",
        "category": "Button",
        "order": 0,
        "component": "Button",
        "dir": "Button",
        "parentId": "08",
        "uid": "00881"
      },
      {
        "url": "RadioButton",
        "name": "RadioButton",
        "category": "Button",
        "order": 0,
        "component": "Button",
        "dir": "Button",
        "parentId": "08",
        "uid": "00882"
      },
      {
        "url": "DropDownButton",
        "name": "DropDownButton",
        "category": "Button",
        "order": 0,
        "component": "Button",
        "dir": "Button",
        "parentId": "08",
        "uid": "00883"
      },
      {
        "url": "SplitButton",
        "name": "SplitButton",
        "category": "Button",
        "order": 0,
        "component": "Button",
        "dir": "Button",
        "parentId": "08",
        "uid": "00884"
      }
    ],
    "order": 3,
    "uid": "08"
  },
  {
    "name": "ComboBox",
    "directory": "ComboBox",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "ComboBox",
        "uid": "00990",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "GroupingAndIcon",
        "name": "Grouping and Icon",
        "category": "ComboBox",
        "uid": "00991",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "DataBinding",
        "name": "Data Binding",
        "category": "ComboBox",
        "uid": "00992",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "CustomValue",
        "name": "CustomValue",
        "category": "ComboBox",
        "uid": "00993",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "ComboBox",
        "uid": "00994",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "Filtering",
        "name": "Filtering",
        "category": "ComboBox",
        "uid": "00995",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "Cascading",
        "name": "Cascading",
        "category": "ComboBox",
        "uid": "00996",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      },
      {
        "url": "DiacriticsFiltering",
        "name": "Diacritics Filtering",
        "category": "ComboBox",
        "uid": "00997",
        "order": 0,
        "component": "ComboBox",
        "dir": "ComboBox",
        "parentId": "09"
      }
    ],
    "order": 3,
    "uid": "09"
  },
  {
    "name": "DatePicker",
    "directory": "DatePicker",
    "category": "Editors",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "DatePicker",
        "order": 0,
        "component": "DatePicker",
        "dir": "DatePicker",
        "parentId": "010",
        "uid": "0010100"
      },
      {
        "url": "DisabledDates",
        "name": "Disabled Dates",
        "category": "DatePicker",
        "order": 0,
        "component": "DatePicker",
        "dir": "DatePicker",
        "parentId": "010",
        "uid": "0010101"
      },
      {
        "url": "DateRange",
        "name": "Date Range",
        "category": "DatePicker",
        "order": 0,
        "component": "DatePicker",
        "dir": "DatePicker",
        "parentId": "010",
        "uid": "0010102"
      },
      {
        "url": "DateFormats",
        "name": "Date Formats",
        "category": "DatePicker",
        "order": 0,
        "component": "DatePicker",
        "dir": "DatePicker",
        "parentId": "010",
        "uid": "0010103"
      },
      {
        "url": "SpecialDates",
        "name": "Special Dates",
        "category": "DatePicker",
        "order": 0,
        "component": "DatePicker",
        "dir": "DatePicker",
        "parentId": "010",
        "uid": "0010104"
      },
      {
        "url": "Globalization",
        "name": "Globalization",
        "category": "DatePicker",
        "order": 0,
        "component": "DatePicker",
        "dir": "DatePicker",
        "parentId": "010",
        "uid": "0010105"
      }
    ],
    "order": 3,
    "uid": "010"
  },
  {
    "name": "DateRangePicker",
    "directory": "DateRangePicker",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "DateRangePicker",
        "order": 0,
        "component": "DateRangePicker",
        "dir": "DateRangePicker",
        "parentId": "011",
        "uid": "0011110"
      },
      {
        "url": "DateRange",
        "name": "Date Range",
        "category": "DateRangePicker",
        "order": 0,
        "component": "DateRangePicker",
        "dir": "DateRangePicker",
        "parentId": "011",
        "uid": "0011111"
      },
      {
        "url": "DaySpan",
        "name": "Day Span",
        "category": "DateRangePicker",
        "order": 0,
        "component": "DateRangePicker",
        "dir": "DateRangePicker",
        "parentId": "011",
        "uid": "0011112"
      },
      {
        "url": "Format",
        "name": "Format",
        "category": "DateRangePicker",
        "order": 0,
        "component": "DateRangePicker",
        "dir": "DateRangePicker",
        "parentId": "011",
        "uid": "0011113"
      },
      {
        "url": "Globalization",
        "name": "Globalization",
        "category": "DateRangePicker",
        "order": 0,
        "component": "DateRangePicker",
        "dir": "DateRangePicker",
        "parentId": "011",
        "uid": "0011114"
      }
    ],
    "order": 3,
    "uid": "011"
  },
  {
    "name": "DateTimePicker",
    "directory": "DateTimePicker",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "DateTimePicker",
        "order": 0,
        "component": "DateTimePicker",
        "dir": "DateTimePicker",
        "parentId": "012",
        "uid": "0012120"
      },
      {
        "url": "DisabledDates",
        "name": "Disabled Dates",
        "category": "DateTimePicker",
        "order": 0,
        "component": "DateTimePicker",
        "dir": "DateTimePicker",
        "parentId": "012",
        "uid": "0012121"
      },
      {
        "url": "DateRange",
        "name": "Date Range",
        "category": "DateTimePicker",
        "order": 0,
        "component": "DateTimePicker",
        "dir": "DateTimePicker",
        "parentId": "012",
        "uid": "0012122"
      },
      {
        "url": "Format",
        "name": "Format",
        "category": "DateTimePicker",
        "order": 0,
        "component": "DateTimePicker",
        "dir": "DateTimePicker",
        "parentId": "012",
        "uid": "0012123"
      },
      {
        "url": "SpecialDates",
        "name": "Special Dates",
        "category": "DateTimePicker",
        "order": 0,
        "component": "DateTimePicker",
        "dir": "DateTimePicker",
        "parentId": "012",
        "uid": "0012124"
      },
      {
        "url": "Globalization",
        "name": "Globalization",
        "category": "DateTimePicker",
        "order": 0,
        "component": "DateTimePicker",
        "dir": "DateTimePicker",
        "parentId": "012",
        "uid": "0012125"
      }
    ],
    "order": 3,
    "uid": "012"
  },
  {
    "name": "DropDownList",
    "directory": "DropDownList",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "DropDownList",
        "uid": "0013130",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "GroupingAndIcon",
        "name": "Grouping and Icon",
        "category": "DropDownList",
        "uid": "0013131",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "DataBinding",
        "name": "Data Binding",
        "category": "DropDownList",
        "uid": "0013132",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "DropDownList",
        "uid": "0013133",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "Filtering",
        "name": "Filtering",
        "category": "DropDownList",
        "uid": "0013134",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "Cascading",
        "name": "Cascading",
        "category": "DropDownList",
        "uid": "0013135",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "Inline",
        "name": "Inline",
        "category": "DropDownList",
        "uid": "0013136",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      },
      {
        "url": "DiacriticsFiltering",
        "name": "Diacritics Filtering",
        "category": "DropDownList",
        "uid": "0013137",
        "order": 0,
        "component": "DropDownList",
        "dir": "DropDownList",
        "parentId": "013"
      }
    ],
    "order": 3,
    "uid": "013"
  },
  {
    "name": "MultiSelect",
    "directory": "MultiSelect",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "MultiSelect",
        "uid": "0014140",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "DataBinding",
        "name": "Data Binding",
        "category": "MultiSelect",
        "uid": "0014141",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "Grouping",
        "name": "Grouping",
        "category": "MultiSelect",
        "uid": "0014142",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "MultiSelect",
        "uid": "0014143",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "Filtering",
        "name": "Filtering",
        "category": "MultiSelect",
        "uid": "0014144",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "CustomValue",
        "name": "Custom Values",
        "category": "MultiSelect",
        "uid": "0014145",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "ChipCustomization",
        "name": "Chip Customization",
        "category": "MultiSelect",
        "uid": "0014146",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "DiacriticsFiltering",
        "name": "Diacritics Filtering",
        "category": "MultiSelect",
        "uid": "0014147",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "CheckBox",
        "name": "CheckBox",
        "category": "MultiSelect",
        "uid": "0014148",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      },
      {
        "url": "SelectionLimit",
        "name": "Selection Limit",
        "category": "MultiSelect",
        "uid": "0014149",
        "order": 0,
        "component": "MultiSelect",
        "dir": "MultiSelect",
        "parentId": "014"
      }
    ],
    "order": 3,
    "uid": "014"
  },
  {
    "name": "MaskedTextBox",
    "directory": "MaskedTextBox",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "MaskedTextBox",
        "order": 0,
        "component": "MaskedTextBox",
        "dir": "MaskedTextBox",
        "parentId": "015",
        "uid": "0015150"
      },
      {
        "url": "CustomMask",
        "name": "Custom Mask",
        "category": "MaskedTextBox",
        "order": 0,
        "component": "MaskedTextBox",
        "dir": "MaskedTextBox",
        "parentId": "015",
        "uid": "0015151"
      },
      {
        "url": "Formats",
        "name": "Formats",
        "category": "MaskedTextBox",
        "order": 0,
        "component": "MaskedTextBox",
        "dir": "MaskedTextBox",
        "parentId": "015",
        "uid": "0015152"
      }
    ],
    "order": 3,
    "uid": "015"
  },
  {
    "name": "NumericTextBox",
    "directory": "NumericTextBox",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "NumericTextBox",
        "order": 0,
        "component": "NumericTextBox",
        "dir": "NumericTextBox",
        "parentId": "016",
        "uid": "0016160"
      },
      {
        "url": "RangeValidation",
        "name": "Range Validation",
        "category": "NumericTextBox",
        "order": 0,
        "component": "NumericTextBox",
        "dir": "NumericTextBox",
        "parentId": "016",
        "uid": "0016161"
      },
      {
        "url": "Internationalization",
        "name": "Internationalization",
        "category": "NumericTextBox",
        "order": 0,
        "component": "NumericTextBox",
        "dir": "NumericTextBox",
        "parentId": "016",
        "uid": "0016162"
      },
      {
        "url": "CustomFormat",
        "name": "Custom Format",
        "category": "NumericTextBox",
        "order": 0,
        "component": "NumericTextBox",
        "dir": "NumericTextBox",
        "parentId": "016",
        "uid": "0016163"
      },
      {
        "url": "RestrictDecimals",
        "name": "Restrict Decimals",
        "category": "NumericTextBox",
        "order": 0,
        "component": "NumericTextBox",
        "dir": "NumericTextBox",
        "parentId": "016",
        "uid": "0016164"
      }
    ],
    "order": 3,
    "uid": "016"
  },
  {
    "name": "Slider",
    "directory": "Slider",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "Default",
        "name": "Default Functionalities",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017170"
      },
      {
        "url": "Ticks",
        "name": "Ticks",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017171"
      },
      {
        "url": "Tooltip",
        "name": "Tooltip",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017172"
      },
      {
        "url": "Orientation",
        "name": "Vertical Orientation",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017173"
      },
      {
        "url": "Formatting",
        "name": "Formatting",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017174"
      },
      {
        "url": "API",
        "name": "API",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017175"
      },
      {
        "url": "Events",
        "name": "Events",
        "category": "Slider",
        "order": 0,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017176"
      },
      {
        "url": "Thumb",
        "name": "Thumb",
        "category": "Customization",
        "order": 1,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017177"
      },
      {
        "url": "Bar",
        "name": "Bar",
        "category": "Customization",
        "order": 1,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017178"
      },
      {
        "url": "CustomTicks",
        "name": "Ticks",
        "category": "Customization",
        "order": 1,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "0017179"
      },
      {
        "url": "CustomTooltip",
        "name": "Tooltip",
        "category": "Customization",
        "order": 1,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "00171710"
      },
      {
        "url": "CloudPricing",
        "name": "Cloud Pricing",
        "category": "Use Case",
        "order": 2,
        "component": "Slider",
        "dir": "Slider",
        "parentId": "017",
        "uid": "00171711"
      }
    ],
    "order": 3,
    "uid": "017"
  },
  {
    "name": "TextBoxes",
    "directory": "TextBoxes",
    "category": "Editors",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "TextBoxes",
        "uid": "0018180",
        "order": 0,
        "component": "TextBoxes",
        "dir": "TextBoxes",
        "parentId": "018"
      }
    ],
    "order": 3,
    "uid": "018"
  },
  {
    "name": "TimePicker",
    "directory": "TimePicker",
    "category": "Editors",
    "type": "updated",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "TimePicker",
        "order": 0,
        "component": "TimePicker",
        "dir": "TimePicker",
        "parentId": "019",
        "uid": "0019190"
      },
      {
        "url": "TimeRange",
        "name": "Time Range",
        "category": "TimePicker",
        "order": 0,
        "component": "TimePicker",
        "dir": "TimePicker",
        "parentId": "019",
        "uid": "0019191"
      },
      {
        "url": "TimeFormat",
        "name": "Time Format",
        "category": "TimePicker",
        "order": 0,
        "component": "TimePicker",
        "dir": "TimePicker",
        "parentId": "019",
        "uid": "0019192"
      },
      {
        "url": "TimeDuration",
        "name": "Time Duration",
        "category": "TimePicker",
        "order": 0,
        "component": "TimePicker",
        "dir": "TimePicker",
        "parentId": "019",
        "type": "new",
        "uid": "0019193"
      },
      {
        "url": "Globalization",
        "name": "Globalization",
        "category": "TimePicker",
        "order": 0,
        "component": "TimePicker",
        "dir": "TimePicker",
        "parentId": "019",
        "uid": "0019194"
      }
    ],
    "order": 3,
    "uid": "019"
  },
  {
    "name": "Uploader",
    "directory": "Uploader",
    "category": "Editors",
    "type": "Preview",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Uploader",
        "uid": "0020200",
        "order": 0,
        "component": "Uploader",
        "dir": "Uploader",
        "parentId": "020"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "Uploader",
        "uid": "0020201",
        "order": 0,
        "component": "Uploader",
        "dir": "Uploader",
        "parentId": "020"
      },
      {
        "url": "PreloadFiles",
        "name": "Preload Files",
        "category": "Uploader",
        "uid": "0020202",
        "order": 0,
        "component": "Uploader",
        "dir": "Uploader",
        "parentId": "020"
      },
      {
        "url": "FileValidation",
        "name": "File Validation",
        "category": "Uploader",
        "uid": "0020203",
        "order": 0,
        "component": "Uploader",
        "dir": "Uploader",
        "parentId": "020"
      },
      {
        "url": "ImagePreview",
        "name": "Image Preview",
        "category": "Uploader",
        "uid": "0020204",
        "order": 0,
        "component": "Uploader",
        "dir": "Uploader",
        "parentId": "020"
      },
      {
        "url": "RTL",
        "name": "RTL",
        "category": "Uploader",
        "uid": "0020205",
        "order": 0,
        "component": "Uploader",
        "dir": "Uploader",
        "parentId": "020"
      }
    ],
    "order": 3,
    "uid": "020"
  },
  {
    "name": "ListView",
    "directory": "ListView",
    "category": "Layout",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "ListView",
        "order": 0,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021210"
      },
      {
        "url": "RemoteData",
        "name": "Remote Data",
        "category": "ListView",
        "order": 0,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021211"
      },
      {
        "url": "Checklist",
        "name": "Checklist",
        "category": "ListView",
        "order": 0,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021212"
      },
      {
        "url": "NestedList",
        "name": "Nested List",
        "category": "ListView",
        "order": 0,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021213"
      },
      {
        "url": "Template",
        "name": "Templates",
        "category": "Customization",
        "order": 1,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021214"
      },
      {
        "url": "GroupTemplate",
        "name": "Group Template",
        "category": "Customization",
        "order": 1,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021215"
      },
      {
        "url": "CallHistory",
        "name": "Call History",
        "category": "Use Case",
        "order": 2,
        "component": "ListView",
        "dir": "ListView",
        "parentId": "021",
        "uid": "0021216"
      }
    ],
    "order": 4,
    "uid": "021"
  },
  {
    "name": "Dialog",
    "directory": "Dialog",
    "category": "Layout",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Dialog",
        "uid": "0022220",
        "order": 0,
        "component": "Dialog",
        "dir": "Dialog",
        "parentId": "022",
        "type": "update"
      },
      {
        "url": "CustomDialogs",
        "name": "Custom Dialogs",
        "category": "Dialog",
        "uid": "0022221",
        "order": 0,
        "component": "Dialog",
        "dir": "Dialog",
        "parentId": "022"
      },
      {
        "url": "Modal",
        "name": "Modal",
        "category": "Dialog",
        "uid": "0022222",
        "order": 0,
        "component": "Dialog",
        "dir": "Dialog",
        "parentId": "022"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "Dialog",
        "uid": "0022223",
        "order": 0,
        "component": "Dialog",
        "dir": "Dialog",
        "parentId": "022",
        "type": "update"
      },
      {
        "url": "AjaxContent",
        "name": "Ajax Content",
        "category": "Dialog",
        "uid": "0022224",
        "order": 0,
        "component": "Dialog",
        "dir": "Dialog",
        "parentId": "022",
        "type": "update"
      },
      {
        "url": "RTL",
        "name": "RTL",
        "category": "Dialog",
        "uid": "0022225",
        "order": 0,
        "component": "Dialog",
        "dir": "Dialog",
        "parentId": "022"
      }
    ],
    "order": 4,
    "uid": "022"
  },
  {
    "name": "Tooltip",
    "directory": "Tooltip",
    "category": "Layout",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Tooltip",
        "order": 0,
        "component": "Tooltip",
        "dir": "Tooltip",
        "parentId": "023",
        "uid": "0023230"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "Tooltip",
        "order": 0,
        "component": "Tooltip",
        "dir": "Tooltip",
        "parentId": "023",
        "uid": "0023231"
      },
      {
        "url": "AjaxContent",
        "name": "Ajax Content",
        "category": "Tooltip",
        "order": 0,
        "component": "Tooltip",
        "dir": "Tooltip",
        "parentId": "023",
        "uid": "0023232"
      },
      {
        "url": "SmartPositioning",
        "name": "Smart Positioning",
        "category": "Tooltip",
        "order": 0,
        "component": "Tooltip",
        "dir": "Tooltip",
        "parentId": "023",
        "uid": "0023233"
      }
    ],
    "order": 4,
    "uid": "023"
  },
  {
    "name": "Card",
    "directory": "Card",
    "category": "Layout",
    "type": "update",
    "samples": [
      {
        "url": "BasicCard",
        "name": "Basic Card",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024240"
      },
      {
        "url": "VerticalCard",
        "name": "Vertical Card",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024241"
      },
      {
        "url": "HorizontalCard",
        "name": "Horizontal Card",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024242"
      },
      {
        "url": "SwipeableCard",
        "name": "Swipeable Card",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024243"
      },
      {
        "url": "FlipCard",
        "name": "Flip Card",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024244"
      },
      {
        "url": "RevealCard",
        "name": "Reveal Card",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024245"
      },
      {
        "url": "TileView",
        "name": "Tile View",
        "category": "Card",
        "order": 0,
        "component": "Card",
        "dir": "Card",
        "parentId": "024",
        "uid": "0024246"
      }
    ],
    "order": 4,
    "uid": "024"
  },
  {
    "name": "Sidebar",
    "directory": "Sidebar",
    "category": "Navigation",
    "type": "Preview",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Sidebar",
        "order": 0,
        "component": "Sidebar",
        "dir": "Sidebar",
        "parentId": "025",
        "uid": "0025250"
      },
      {
        "url": "Dock",
        "name": "Dock",
        "category": "Sidebar",
        "order": 0,
        "component": "Sidebar",
        "dir": "Sidebar",
        "parentId": "025",
        "uid": "0025251"
      },
      {
        "url": "API",
        "name": "API",
        "category": "Sidebar",
        "order": 0,
        "component": "Sidebar",
        "dir": "Sidebar",
        "parentId": "025",
        "uid": "0025252"
      },
      {
        "url": "SidebarWithListView",
        "name": "Sidebar With List View",
        "category": "Sidebar",
        "order": 0,
        "component": "Sidebar",
        "dir": "Sidebar",
        "parentId": "025",
        "uid": "0025253"
      }
    ],
    "order": 5,
    "uid": "025"
  },
  {
    "name": "TreeView",
    "directory": "TreeView",
    "category": "Navigation",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026260"
      },
      {
        "url": "IconsandImages",
        "name": "Icons and Images",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026261"
      },
      {
        "url": "CheckBox",
        "name": "CheckBox",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026262"
      },
      {
        "url": "NodeEditing",
        "name": "Node Editing",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026263"
      },
      {
        "url": "MultipleSelection",
        "name": "Multiple Selection",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026264"
      },
      {
        "url": "DragandDrop",
        "name": "Drag and Drop",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026265"
      },
      {
        "url": "Template",
        "name": "Template",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026266"
      },
      {
        "url": "RTL",
        "name": "RTL",
        "category": "TreeView",
        "order": 0,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026267"
      },
      {
        "url": "LocalData",
        "name": "Local Data",
        "category": "Data Binding",
        "order": 1,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026268"
      },
      {
        "url": "RemoteData",
        "name": "Remote Data",
        "category": "Data Binding",
        "order": 1,
        "component": "TreeView",
        "dir": "TreeView",
        "parentId": "026",
        "uid": "0026269"
      }
    ],
    "order": 5,
    "uid": "026"
  },
  {
    "name": "Tab",
    "directory": "Tab",
    "category": "Navigation",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Tab",
        "order": 0,
        "component": "Tab",
        "dir": "Tab",
        "parentId": "027",
        "uid": "0027270"
      },
      {
        "url": "Orientation",
        "name": "Orientation",
        "category": "Tab",
        "order": 0,
        "component": "Tab",
        "dir": "Tab",
        "parentId": "027",
        "uid": "0027271"
      },
      {
        "url": "ResponsiveModes",
        "name": "Responsive Modes",
        "category": "Tab",
        "order": 0,
        "component": "Tab",
        "dir": "Tab",
        "parentId": "027",
        "uid": "0027272"
      },
      {
        "url": "RTL",
        "name": "RTL",
        "category": "Tab",
        "order": 0,
        "component": "Tab",
        "dir": "Tab",
        "parentId": "027",
        "uid": "0027273"
      },
      {
        "url": "Wizard",
        "name": "Wizard",
        "category": "Tab",
        "order": 0,
        "component": "Tab",
        "dir": "Tab",
        "parentId": "027",
        "uid": "0027274"
      }
    ],
    "order": 5,
    "uid": "027"
  },
  {
    "name": "Toolbar",
    "directory": "Toolbar",
    "category": "Navigation",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Toolbar",
        "order": 0,
        "component": "Toolbar",
        "dir": "Toolbar",
        "parentId": "028",
        "uid": "0028280"
      },
      {
        "url": "Popup",
        "name": "Popup",
        "category": "Toolbar",
        "order": 0,
        "component": "Toolbar",
        "dir": "Toolbar",
        "parentId": "028",
        "uid": "0028281"
      },
      {
        "url": "Alignment",
        "name": "Alignment",
        "category": "Toolbar",
        "order": 0,
        "component": "Toolbar",
        "dir": "Toolbar",
        "parentId": "028",
        "uid": "0028282"
      },
      {
        "url": "RTL",
        "name": "RTL",
        "category": "Toolbar",
        "order": 0,
        "component": "Toolbar",
        "dir": "Toolbar",
        "parentId": "028",
        "uid": "0028283"
      }
    ],
    "order": 5,
    "uid": "028"
  },
  {
    "name": "ContextMenu",
    "directory": "ContextMenu",
    "category": "Navigation",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "DefaultFunctionalities",
        "category": "ContextMenu",
        "order": 0,
        "component": "ContextMenu",
        "dir": "ContextMenu",
        "parentId": "029",
        "uid": "0029290"
      }
    ],
    "order": 5,
    "uid": "029"
  },
  {
    "name": "Accordion",
    "directory": "Accordion",
    "category": "Navigation",
    "type": "update",
    "samples": [
      {
        "url": "DefaultFunctionalities",
        "name": "Default Functionalities",
        "category": "Accordion",
        "order": 0,
        "component": "Accordion",
        "dir": "Accordion",
        "parentId": "030",
        "uid": "0030300"
      },
      {
        "url": "AjaxContent",
        "name": "Ajax Content",
        "category": "Accordion",
        "order": 0,
        "component": "Accordion",
        "dir": "Accordion",
        "parentId": "030",
        "uid": "0030301"
      },
      {
        "url": "Icons",
        "name": "Icons",
        "category": "Accordion",
        "order": 0,
        "component": "Accordion",
        "dir": "Accordion",
        "parentId": "030",
        "uid": "0030302"
      },
      {
        "url": "RTL",
        "name": "RTL",
        "category": "Accordion",
        "order": 0,
        "component": "Accordion",
        "dir": "Accordion",
        "parentId": "030",
        "uid": "0030303"
      }
    ],
    "order": 5,
    "uid": "030"
  }
]