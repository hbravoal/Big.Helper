﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Big.Multisponsor.Inventarios.UI.Startup))]
namespace Big.Multisponsor.Inventarios.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
