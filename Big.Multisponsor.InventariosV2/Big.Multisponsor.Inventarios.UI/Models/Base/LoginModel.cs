﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace Big.Multisponsor.Inventarios.UI.Models.Base
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Debes ingresar  {0}")]

        public string User { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Debes ingresar  {0}")]
        public string Password { get; set; }
    }
}