﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Big.Multisponsor.Inventarios.Domain;
using Big.Multisponsor.Inventarios.Generated.Domain;
using Big.Multisponsor.Inventarios.Domain.Generated;
using System.Data;
namespace Big.Multisponsor.Inventarios.BusinessLogic.Controller
{
    public class PreBillingsController
    {

        public static List<InvFactura> GetFacturasPorProductosFechas(int IdPrograma, DateTime FechaInicio, DateTime FechaFin, int IdProveedor, string GuidProducto)
        {
            try
            {

                DataSet facturas= SPs.SelectPrefacturacionFacturasProductos(FechaInicio, FechaFin, IdPrograma, IdProveedor, GuidProducto).GetDataSet();
                List<InvFactura> invList = new List<InvFactura>();
                if (facturas.Tables.Count > 0)
                {
                    if (facturas.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow item in facturas.Tables[0].Rows)
                        {
                            invList.Add(
                                new InvFactura
                                {
                                    GuidFactura = Convert.ToString(item["GUID_FACTURA"]),
                                    
                                    
                                    FechaFactura = Convert.ToDateTime(item["FECHA_FACTURA"]),
                                    Iva = Convert.ToInt32(item["IVA"]),
                                    TotalBase = Convert.ToInt32(item["TOTAL_BASE"]),
                                    DireccionCliente = Convert.ToString(item["NOTA_CREDITO"]),
                                }
                                );
                        }
                    }
                }
                return invList;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasPorFechaFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                //Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
       
    }
}
