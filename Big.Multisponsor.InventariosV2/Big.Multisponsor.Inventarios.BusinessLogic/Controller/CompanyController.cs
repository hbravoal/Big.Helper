﻿using Big.Multisponsor.Inventarios.Domain.Generated;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Big.Multisponsor.Inventarios.BusinessLogic.Controller
{
    public class CompanyController
    {

        public static DataSet GetEmpresasByCriterias(string codigoProveedor, string nit, string razonSocial, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.SelectEmpresas(codigoProveedor, nit, razonSocial, TotalRecords);
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                //Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

    }
}
