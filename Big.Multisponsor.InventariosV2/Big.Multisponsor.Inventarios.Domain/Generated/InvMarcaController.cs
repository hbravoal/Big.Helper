using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MARCAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMarcaController
    {
        // Preload our schema..
        InvMarca thisSchemaLoad = new InvMarca();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMarcaCollection FetchAll()
        {
            InvMarcaCollection coll = new InvMarcaCollection();
            Query qry = new Query(InvMarca.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMarcaCollection FetchByID(object IdMarca)
        {
            InvMarcaCollection coll = new InvMarcaCollection().Where("ID_MARCA", IdMarca).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMarcaCollection FetchByQuery(Query qry)
        {
            InvMarcaCollection coll = new InvMarcaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdMarca)
        {
            return (InvMarca.Delete(IdMarca) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdMarca)
        {
            return (InvMarca.Destroy(IdMarca) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string NombreMarca,int IdPrograma,int IdCiudad,string Direccion,decimal? CiCoordinadora,decimal? PrefijoGuias,decimal? GuiaInicial,decimal? GuiaFinal)
	    {
		    InvMarca item = new InvMarca();
		    
            item.NombreMarca = NombreMarca;
            
            item.IdPrograma = IdPrograma;
            
            item.IdCiudad = IdCiudad;
            
            item.Direccion = Direccion;
            
            item.CiCoordinadora = CiCoordinadora;
            
            item.PrefijoGuias = PrefijoGuias;
            
            item.GuiaInicial = GuiaInicial;
            
            item.GuiaFinal = GuiaFinal;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdMarca,string NombreMarca,int IdPrograma,int IdCiudad,string Direccion,decimal? CiCoordinadora,decimal? PrefijoGuias,decimal? GuiaInicial,decimal? GuiaFinal)
	    {
		    InvMarca item = new InvMarca();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdMarca = IdMarca;
				
			item.NombreMarca = NombreMarca;
				
			item.IdPrograma = IdPrograma;
				
			item.IdCiudad = IdCiudad;
				
			item.Direccion = Direccion;
				
			item.CiCoordinadora = CiCoordinadora;
				
			item.PrefijoGuias = PrefijoGuias;
				
			item.GuiaInicial = GuiaInicial;
				
			item.GuiaFinal = GuiaFinal;
				
	        item.Save(UserName);
	    }
    }
}
