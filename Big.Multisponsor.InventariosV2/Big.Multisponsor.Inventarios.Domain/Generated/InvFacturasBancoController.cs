using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_FACTURAS_BANCO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvFacturasBancoController
    {
        // Preload our schema..
        InvFacturasBanco thisSchemaLoad = new InvFacturasBanco();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvFacturasBancoCollection FetchAll()
        {
            InvFacturasBancoCollection coll = new InvFacturasBancoCollection();
            Query qry = new Query(InvFacturasBanco.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvFacturasBancoCollection FetchByID(object GuidFacturaBanco)
        {
            InvFacturasBancoCollection coll = new InvFacturasBancoCollection().Where("GUID_FACTURA_BANCO", GuidFacturaBanco).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvFacturasBancoCollection FetchByQuery(Query qry)
        {
            InvFacturasBancoCollection coll = new InvFacturasBancoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidFacturaBanco)
        {
            return (InvFacturasBanco.Delete(GuidFacturaBanco) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidFacturaBanco)
        {
            return (InvFacturasBanco.Destroy(GuidFacturaBanco) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidFacturaBanco,int ConsecutivoFacturaBanco,decimal TotalBase,decimal Iva,DateTime FechaFacturaBanco,int IdProveedor,int IdPrograma,int IdPrefactura)
	    {
		    InvFacturasBanco item = new InvFacturasBanco();
		    
            item.GuidFacturaBanco = GuidFacturaBanco;
            
            item.ConsecutivoFacturaBanco = ConsecutivoFacturaBanco;
            
            item.TotalBase = TotalBase;
            
            item.Iva = Iva;
            
            item.FechaFacturaBanco = FechaFacturaBanco;
            
            item.IdProveedor = IdProveedor;
            
            item.IdPrograma = IdPrograma;
            
            item.IdPrefactura = IdPrefactura;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidFacturaBanco,int ConsecutivoFacturaBanco,decimal TotalBase,decimal Iva,DateTime FechaFacturaBanco,int IdProveedor,int IdPrograma,int IdPrefactura)
	    {
		    InvFacturasBanco item = new InvFacturasBanco();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidFacturaBanco = GuidFacturaBanco;
				
			item.ConsecutivoFacturaBanco = ConsecutivoFacturaBanco;
				
			item.TotalBase = TotalBase;
				
			item.Iva = Iva;
				
			item.FechaFacturaBanco = FechaFacturaBanco;
				
			item.IdProveedor = IdProveedor;
				
			item.IdPrograma = IdPrograma;
				
			item.IdPrefactura = IdPrefactura;
				
	        item.Save(UserName);
	    }
    }
}
