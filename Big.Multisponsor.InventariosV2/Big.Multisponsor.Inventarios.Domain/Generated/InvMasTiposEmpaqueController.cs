using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_EMPAQUE
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposEmpaqueController
    {
        // Preload our schema..
        InvMasTiposEmpaque thisSchemaLoad = new InvMasTiposEmpaque();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposEmpaqueCollection FetchAll()
        {
            InvMasTiposEmpaqueCollection coll = new InvMasTiposEmpaqueCollection();
            Query qry = new Query(InvMasTiposEmpaque.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposEmpaqueCollection FetchByID(object IdTipoEmpaque)
        {
            InvMasTiposEmpaqueCollection coll = new InvMasTiposEmpaqueCollection().Where("ID_TIPO_EMPAQUE", IdTipoEmpaque).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposEmpaqueCollection FetchByQuery(Query qry)
        {
            InvMasTiposEmpaqueCollection coll = new InvMasTiposEmpaqueCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoEmpaque)
        {
            return (InvMasTiposEmpaque.Delete(IdTipoEmpaque) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoEmpaque)
        {
            return (InvMasTiposEmpaque.Destroy(IdTipoEmpaque) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Nombre)
	    {
		    InvMasTiposEmpaque item = new InvMasTiposEmpaque();
		    
            item.Nombre = Nombre;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoEmpaque,string Nombre)
	    {
		    InvMasTiposEmpaque item = new InvMasTiposEmpaque();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoEmpaque = IdTipoEmpaque;
				
			item.Nombre = Nombre;
				
	        item.Save(UserName);
	    }
    }
}
