using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_CGUNO_CAUSACIONES_PERIODOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoCausacionesPeriodoController
    {
        // Preload our schema..
        InvCgunoCausacionesPeriodo thisSchemaLoad = new InvCgunoCausacionesPeriodo();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoCausacionesPeriodoCollection FetchAll()
        {
            InvCgunoCausacionesPeriodoCollection coll = new InvCgunoCausacionesPeriodoCollection();
            Query qry = new Query(InvCgunoCausacionesPeriodo.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCausacionesPeriodoCollection FetchByID(object Id)
        {
            InvCgunoCausacionesPeriodoCollection coll = new InvCgunoCausacionesPeriodoCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCausacionesPeriodoCollection FetchByQuery(Query qry)
        {
            InvCgunoCausacionesPeriodoCollection coll = new InvCgunoCausacionesPeriodoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoCausacionesPeriodo.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoCausacionesPeriodo.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,string Documento,DateTime Fecha,string IdentificacionCliente,string Cliente,decimal Subtotal,decimal Iva,decimal Retencion,decimal Total,decimal IdPeriodo,bool Correcto,bool Exportado)
	    {
		    InvCgunoCausacionesPeriodo item = new InvCgunoCausacionesPeriodo();
		    
            item.Guid = Guid;
            
            item.Documento = Documento;
            
            item.Fecha = Fecha;
            
            item.IdentificacionCliente = IdentificacionCliente;
            
            item.Cliente = Cliente;
            
            item.Subtotal = Subtotal;
            
            item.Iva = Iva;
            
            item.Retencion = Retencion;
            
            item.Total = Total;
            
            item.IdPeriodo = IdPeriodo;
            
            item.Correcto = Correcto;
            
            item.Exportado = Exportado;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(decimal Id,string Guid,string Documento,DateTime Fecha,string IdentificacionCliente,string Cliente,decimal Subtotal,decimal Iva,decimal Retencion,decimal Total,decimal IdPeriodo,bool Correcto,bool Exportado)
	    {
		    InvCgunoCausacionesPeriodo item = new InvCgunoCausacionesPeriodo();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Guid = Guid;
				
			item.Documento = Documento;
				
			item.Fecha = Fecha;
				
			item.IdentificacionCliente = IdentificacionCliente;
				
			item.Cliente = Cliente;
				
			item.Subtotal = Subtotal;
				
			item.Iva = Iva;
				
			item.Retencion = Retencion;
				
			item.Total = Total;
				
			item.IdPeriodo = IdPeriodo;
				
			item.Correcto = Correcto;
				
			item.Exportado = Exportado;
				
	        item.Save(UserName);
	    }
    }
}
