using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_BODEGA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposBodegaController
    {
        // Preload our schema..
        InvMasTiposBodega thisSchemaLoad = new InvMasTiposBodega();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposBodegaCollection FetchAll()
        {
            InvMasTiposBodegaCollection coll = new InvMasTiposBodegaCollection();
            Query qry = new Query(InvMasTiposBodega.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposBodegaCollection FetchByID(object IdTipoBodega)
        {
            InvMasTiposBodegaCollection coll = new InvMasTiposBodegaCollection().Where("ID_TIPO_BODEGA", IdTipoBodega).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposBodegaCollection FetchByQuery(Query qry)
        {
            InvMasTiposBodegaCollection coll = new InvMasTiposBodegaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoBodega)
        {
            return (InvMasTiposBodega.Delete(IdTipoBodega) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoBodega)
        {
            return (InvMasTiposBodega.Destroy(IdTipoBodega) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string NombreTipo)
	    {
		    InvMasTiposBodega item = new InvMasTiposBodega();
		    
            item.NombreTipo = NombreTipo;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoBodega,string NombreTipo)
	    {
		    InvMasTiposBodega item = new InvMasTiposBodega();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoBodega = IdTipoBodega;
				
			item.NombreTipo = NombreTipo;
				
	        item.Save(UserName);
	    }
    }
}
