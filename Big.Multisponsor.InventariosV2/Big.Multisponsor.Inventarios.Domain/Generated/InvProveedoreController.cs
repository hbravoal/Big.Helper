using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_PROVEEDORES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvProveedoreController
    {
        // Preload our schema..
        InvProveedore thisSchemaLoad = new InvProveedore();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvProveedoreCollection FetchAll()
        {
            InvProveedoreCollection coll = new InvProveedoreCollection();
            Query qry = new Query(InvProveedore.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvProveedoreCollection FetchByID(object IdProveedor)
        {
            InvProveedoreCollection coll = new InvProveedoreCollection().Where("ID_PROVEEDOR", IdProveedor).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvProveedoreCollection FetchByQuery(Query qry)
        {
            InvProveedoreCollection coll = new InvProveedoreCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdProveedor)
        {
            return (InvProveedore.Delete(IdProveedor) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdProveedor)
        {
            return (InvProveedore.Destroy(IdProveedor) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string CodigoProveedor,string RazonSocial,string Nit,string Direccion,string Telefono,string Fax,int IdCiudad,int IdEntidadBancaria,string NumeroCuenta,int IdTipoCuenta,int IdEstado,bool? GranContribuyente,bool? RegimenComun,bool? AutoRetenedor,bool? RetenedorIva,int? PlazoPago,decimal? Cupo,int? IdCuentaInventarios,int? IdCuentaCostosVenta)
	    {
		    InvProveedore item = new InvProveedore();
		    
            item.CodigoProveedor = CodigoProveedor;
            
            item.RazonSocial = RazonSocial;
            
            item.Nit = Nit;
            
            item.Direccion = Direccion;
            
            item.Telefono = Telefono;
            
            item.Fax = Fax;
            
            item.IdCiudad = IdCiudad;
            
            item.IdEntidadBancaria = IdEntidadBancaria;
            
            item.NumeroCuenta = NumeroCuenta;
            
            item.IdTipoCuenta = IdTipoCuenta;
            
            item.IdEstado = IdEstado;
            
            item.GranContribuyente = GranContribuyente;
            
            item.RegimenComun = RegimenComun;
            
            item.AutoRetenedor = AutoRetenedor;
            
            item.RetenedorIva = RetenedorIva;
            
            item.PlazoPago = PlazoPago;
            
            item.Cupo = Cupo;
            
            item.IdCuentaInventarios = IdCuentaInventarios;
            
            item.IdCuentaCostosVenta = IdCuentaCostosVenta;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdProveedor,string CodigoProveedor,string RazonSocial,string Nit,string Direccion,string Telefono,string Fax,int IdCiudad,int IdEntidadBancaria,string NumeroCuenta,int IdTipoCuenta,int IdEstado,bool? GranContribuyente,bool? RegimenComun,bool? AutoRetenedor,bool? RetenedorIva,int? PlazoPago,decimal? Cupo,int? IdCuentaInventarios,int? IdCuentaCostosVenta)
	    {
		    InvProveedore item = new InvProveedore();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdProveedor = IdProveedor;
				
			item.CodigoProveedor = CodigoProveedor;
				
			item.RazonSocial = RazonSocial;
				
			item.Nit = Nit;
				
			item.Direccion = Direccion;
				
			item.Telefono = Telefono;
				
			item.Fax = Fax;
				
			item.IdCiudad = IdCiudad;
				
			item.IdEntidadBancaria = IdEntidadBancaria;
				
			item.NumeroCuenta = NumeroCuenta;
				
			item.IdTipoCuenta = IdTipoCuenta;
				
			item.IdEstado = IdEstado;
				
			item.GranContribuyente = GranContribuyente;
				
			item.RegimenComun = RegimenComun;
				
			item.AutoRetenedor = AutoRetenedor;
				
			item.RetenedorIva = RetenedorIva;
				
			item.PlazoPago = PlazoPago;
				
			item.Cupo = Cupo;
				
			item.IdCuentaInventarios = IdCuentaInventarios;
				
			item.IdCuentaCostosVenta = IdCuentaCostosVenta;
				
	        item.Save(UserName);
	    }
    }
}
