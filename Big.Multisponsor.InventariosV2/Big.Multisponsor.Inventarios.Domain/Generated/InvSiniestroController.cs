using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_SINIESTROS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvSiniestroController
    {
        // Preload our schema..
        InvSiniestro thisSchemaLoad = new InvSiniestro();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvSiniestroCollection FetchAll()
        {
            InvSiniestroCollection coll = new InvSiniestroCollection();
            Query qry = new Query(InvSiniestro.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvSiniestroCollection FetchByID(object Guid)
        {
            InvSiniestroCollection coll = new InvSiniestroCollection().Where("GUID", Guid).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvSiniestroCollection FetchByQuery(Query qry)
        {
            InvSiniestroCollection coll = new InvSiniestroCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Guid)
        {
            return (InvSiniestro.Delete(Guid) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Guid)
        {
            return (InvSiniestro.Destroy(Guid) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,int IdPrograma,string GuidReferenciaProducto,int Cantidad)
	    {
		    InvSiniestro item = new InvSiniestro();
		    
            item.Guid = Guid;
            
            item.IdPrograma = IdPrograma;
            
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
            item.Cantidad = Cantidad;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,int IdPrograma,string GuidReferenciaProducto,int Cantidad)
	    {
		    InvSiniestro item = new InvSiniestro();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.IdPrograma = IdPrograma;
				
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
			item.Cantidad = Cantidad;
				
	        item.Save(UserName);
	    }
    }
}
