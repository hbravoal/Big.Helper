using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvEmpresa class.
	/// </summary>
    [Serializable]
	public partial class InvEmpresaCollection : ActiveList<InvEmpresa, InvEmpresaCollection>
	{	   
		public InvEmpresaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvEmpresaCollection</returns>
		public InvEmpresaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvEmpresa o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_EMPRESAS table.
	/// </summary>
	[Serializable]
	public partial class InvEmpresa : ActiveRecord<InvEmpresa>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvEmpresa()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvEmpresa(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvEmpresa(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvEmpresa(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_EMPRESAS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdEmpresa = new TableSchema.TableColumn(schema);
				colvarIdEmpresa.ColumnName = "ID_EMPRESA";
				colvarIdEmpresa.DataType = DbType.Int32;
				colvarIdEmpresa.MaxLength = 0;
				colvarIdEmpresa.AutoIncrement = true;
				colvarIdEmpresa.IsNullable = false;
				colvarIdEmpresa.IsPrimaryKey = true;
				colvarIdEmpresa.IsForeignKey = false;
				colvarIdEmpresa.IsReadOnly = false;
				colvarIdEmpresa.DefaultSetting = @"";
				colvarIdEmpresa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdEmpresa);
				
				TableSchema.TableColumn colvarCodigoEmpresa = new TableSchema.TableColumn(schema);
				colvarCodigoEmpresa.ColumnName = "CODIGO_EMPRESA";
				colvarCodigoEmpresa.DataType = DbType.AnsiString;
				colvarCodigoEmpresa.MaxLength = 6;
				colvarCodigoEmpresa.AutoIncrement = false;
				colvarCodigoEmpresa.IsNullable = false;
				colvarCodigoEmpresa.IsPrimaryKey = false;
				colvarCodigoEmpresa.IsForeignKey = false;
				colvarCodigoEmpresa.IsReadOnly = false;
				colvarCodigoEmpresa.DefaultSetting = @"";
				colvarCodigoEmpresa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigoEmpresa);
				
				TableSchema.TableColumn colvarRazonSocial = new TableSchema.TableColumn(schema);
				colvarRazonSocial.ColumnName = "RAZON_SOCIAL";
				colvarRazonSocial.DataType = DbType.String;
				colvarRazonSocial.MaxLength = 200;
				colvarRazonSocial.AutoIncrement = false;
				colvarRazonSocial.IsNullable = false;
				colvarRazonSocial.IsPrimaryKey = false;
				colvarRazonSocial.IsForeignKey = false;
				colvarRazonSocial.IsReadOnly = false;
				colvarRazonSocial.DefaultSetting = @"";
				colvarRazonSocial.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRazonSocial);
				
				TableSchema.TableColumn colvarNit = new TableSchema.TableColumn(schema);
				colvarNit.ColumnName = "NIT";
				colvarNit.DataType = DbType.AnsiString;
				colvarNit.MaxLength = 15;
				colvarNit.AutoIncrement = false;
				colvarNit.IsNullable = false;
				colvarNit.IsPrimaryKey = false;
				colvarNit.IsForeignKey = false;
				colvarNit.IsReadOnly = false;
				colvarNit.DefaultSetting = @"";
				colvarNit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNit);
				
				TableSchema.TableColumn colvarDireccion = new TableSchema.TableColumn(schema);
				colvarDireccion.ColumnName = "DIRECCION";
				colvarDireccion.DataType = DbType.String;
				colvarDireccion.MaxLength = 200;
				colvarDireccion.AutoIncrement = false;
				colvarDireccion.IsNullable = false;
				colvarDireccion.IsPrimaryKey = false;
				colvarDireccion.IsForeignKey = false;
				colvarDireccion.IsReadOnly = false;
				colvarDireccion.DefaultSetting = @"";
				colvarDireccion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDireccion);
				
				TableSchema.TableColumn colvarTelefono = new TableSchema.TableColumn(schema);
				colvarTelefono.ColumnName = "TELEFONO";
				colvarTelefono.DataType = DbType.AnsiString;
				colvarTelefono.MaxLength = 20;
				colvarTelefono.AutoIncrement = false;
				colvarTelefono.IsNullable = false;
				colvarTelefono.IsPrimaryKey = false;
				colvarTelefono.IsForeignKey = false;
				colvarTelefono.IsReadOnly = false;
				colvarTelefono.DefaultSetting = @"";
				colvarTelefono.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTelefono);
				
				TableSchema.TableColumn colvarFax = new TableSchema.TableColumn(schema);
				colvarFax.ColumnName = "FAX";
				colvarFax.DataType = DbType.AnsiString;
				colvarFax.MaxLength = 20;
				colvarFax.AutoIncrement = false;
				colvarFax.IsNullable = true;
				colvarFax.IsPrimaryKey = false;
				colvarFax.IsForeignKey = false;
				colvarFax.IsReadOnly = false;
				colvarFax.DefaultSetting = @"";
				colvarFax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFax);
				
				TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
				colvarIdCiudad.ColumnName = "ID_CIUDAD";
				colvarIdCiudad.DataType = DbType.Int32;
				colvarIdCiudad.MaxLength = 0;
				colvarIdCiudad.AutoIncrement = false;
				colvarIdCiudad.IsNullable = false;
				colvarIdCiudad.IsPrimaryKey = false;
				colvarIdCiudad.IsForeignKey = true;
				colvarIdCiudad.IsReadOnly = false;
				colvarIdCiudad.DefaultSetting = @"";
				
					colvarIdCiudad.ForeignKeyTableName = "INV_MAS_CIUDADES";
				schema.Columns.Add(colvarIdCiudad);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_EMPRESAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdEmpresa")]
		[Bindable(true)]
		public int IdEmpresa 
		{
			get { return GetColumnValue<int>(Columns.IdEmpresa); }
			set { SetColumnValue(Columns.IdEmpresa, value); }
		}
		  
		[XmlAttribute("CodigoEmpresa")]
		[Bindable(true)]
		public string CodigoEmpresa 
		{
			get { return GetColumnValue<string>(Columns.CodigoEmpresa); }
			set { SetColumnValue(Columns.CodigoEmpresa, value); }
		}
		  
		[XmlAttribute("RazonSocial")]
		[Bindable(true)]
		public string RazonSocial 
		{
			get { return GetColumnValue<string>(Columns.RazonSocial); }
			set { SetColumnValue(Columns.RazonSocial, value); }
		}
		  
		[XmlAttribute("Nit")]
		[Bindable(true)]
		public string Nit 
		{
			get { return GetColumnValue<string>(Columns.Nit); }
			set { SetColumnValue(Columns.Nit, value); }
		}
		  
		[XmlAttribute("Direccion")]
		[Bindable(true)]
		public string Direccion 
		{
			get { return GetColumnValue<string>(Columns.Direccion); }
			set { SetColumnValue(Columns.Direccion, value); }
		}
		  
		[XmlAttribute("Telefono")]
		[Bindable(true)]
		public string Telefono 
		{
			get { return GetColumnValue<string>(Columns.Telefono); }
			set { SetColumnValue(Columns.Telefono, value); }
		}
		  
		[XmlAttribute("Fax")]
		[Bindable(true)]
		public string Fax 
		{
			get { return GetColumnValue<string>(Columns.Fax); }
			set { SetColumnValue(Columns.Fax, value); }
		}
		  
		[XmlAttribute("IdCiudad")]
		[Bindable(true)]
		public int IdCiudad 
		{
			get { return GetColumnValue<int>(Columns.IdCiudad); }
			set { SetColumnValue(Columns.IdCiudad, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasProveedorPuntoCollection InvFacturasProveedorPuntos()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasProveedorPuntoCollection().Where(InvFacturasProveedorPunto.Columns.IdEmpresa, IdEmpresa).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvProgramaCollection InvProgramas()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvProgramaCollection().Where(InvPrograma.Columns.IdEmpresa, IdEmpresa).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasCiudade ActiveRecord object related to this InvEmpresa
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasCiudade InvMasCiudade
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvMasCiudade.FetchByID(this.IdCiudad); }
			set { SetColumnValue("ID_CIUDAD", value.IdCiudad); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varCodigoEmpresa,string varRazonSocial,string varNit,string varDireccion,string varTelefono,string varFax,int varIdCiudad)
		{
			InvEmpresa item = new InvEmpresa();
			
			item.CodigoEmpresa = varCodigoEmpresa;
			
			item.RazonSocial = varRazonSocial;
			
			item.Nit = varNit;
			
			item.Direccion = varDireccion;
			
			item.Telefono = varTelefono;
			
			item.Fax = varFax;
			
			item.IdCiudad = varIdCiudad;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdEmpresa,string varCodigoEmpresa,string varRazonSocial,string varNit,string varDireccion,string varTelefono,string varFax,int varIdCiudad)
		{
			InvEmpresa item = new InvEmpresa();
			
				item.IdEmpresa = varIdEmpresa;
			
				item.CodigoEmpresa = varCodigoEmpresa;
			
				item.RazonSocial = varRazonSocial;
			
				item.Nit = varNit;
			
				item.Direccion = varDireccion;
			
				item.Telefono = varTelefono;
			
				item.Fax = varFax;
			
				item.IdCiudad = varIdCiudad;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdEmpresaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoEmpresaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RazonSocialColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NitColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DireccionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TelefonoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn FaxColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCiudadColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdEmpresa = @"ID_EMPRESA";
			 public static string CodigoEmpresa = @"CODIGO_EMPRESA";
			 public static string RazonSocial = @"RAZON_SOCIAL";
			 public static string Nit = @"NIT";
			 public static string Direccion = @"DIRECCION";
			 public static string Telefono = @"TELEFONO";
			 public static string Fax = @"FAX";
			 public static string IdCiudad = @"ID_CIUDAD";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
