using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvDetallesFacturaProveedorPunto class.
	/// </summary>
    [Serializable]
	public partial class InvDetallesFacturaProveedorPuntoCollection : ActiveList<InvDetallesFacturaProveedorPunto, InvDetallesFacturaProveedorPuntoCollection>
	{	   
		public InvDetallesFacturaProveedorPuntoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvDetallesFacturaProveedorPuntoCollection</returns>
		public InvDetallesFacturaProveedorPuntoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvDetallesFacturaProveedorPunto o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS table.
	/// </summary>
	[Serializable]
	public partial class InvDetallesFacturaProveedorPunto : ActiveRecord<InvDetallesFacturaProveedorPunto>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvDetallesFacturaProveedorPunto()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvDetallesFacturaProveedorPunto(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvDetallesFacturaProveedorPunto(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvDetallesFacturaProveedorPunto(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidDetalleFacturaProveedorPuntos = new TableSchema.TableColumn(schema);
				colvarGuidDetalleFacturaProveedorPuntos.ColumnName = "GUID_DETALLE_FACTURA_PROVEEDOR_PUNTOS";
				colvarGuidDetalleFacturaProveedorPuntos.DataType = DbType.String;
				colvarGuidDetalleFacturaProveedorPuntos.MaxLength = 36;
				colvarGuidDetalleFacturaProveedorPuntos.AutoIncrement = false;
				colvarGuidDetalleFacturaProveedorPuntos.IsNullable = false;
				colvarGuidDetalleFacturaProveedorPuntos.IsPrimaryKey = true;
				colvarGuidDetalleFacturaProveedorPuntos.IsForeignKey = false;
				colvarGuidDetalleFacturaProveedorPuntos.IsReadOnly = false;
				colvarGuidDetalleFacturaProveedorPuntos.DefaultSetting = @"";
				colvarGuidDetalleFacturaProveedorPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidDetalleFacturaProveedorPuntos);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarValorBasePuntosTotal = new TableSchema.TableColumn(schema);
				colvarValorBasePuntosTotal.ColumnName = "VALOR_BASE_PUNTOS_TOTAL";
				colvarValorBasePuntosTotal.DataType = DbType.Decimal;
				colvarValorBasePuntosTotal.MaxLength = 0;
				colvarValorBasePuntosTotal.AutoIncrement = false;
				colvarValorBasePuntosTotal.IsNullable = false;
				colvarValorBasePuntosTotal.IsPrimaryKey = false;
				colvarValorBasePuntosTotal.IsForeignKey = false;
				colvarValorBasePuntosTotal.IsReadOnly = false;
				colvarValorBasePuntosTotal.DefaultSetting = @"";
				colvarValorBasePuntosTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorBasePuntosTotal);
				
				TableSchema.TableColumn colvarIvaTotal = new TableSchema.TableColumn(schema);
				colvarIvaTotal.ColumnName = "IVA_TOTAL";
				colvarIvaTotal.DataType = DbType.Decimal;
				colvarIvaTotal.MaxLength = 0;
				colvarIvaTotal.AutoIncrement = false;
				colvarIvaTotal.IsNullable = false;
				colvarIvaTotal.IsPrimaryKey = false;
				colvarIvaTotal.IsForeignKey = false;
				colvarIvaTotal.IsReadOnly = false;
				colvarIvaTotal.DefaultSetting = @"";
				colvarIvaTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIvaTotal);
				
				TableSchema.TableColumn colvarGuidFacturaProveedorPuntos = new TableSchema.TableColumn(schema);
				colvarGuidFacturaProveedorPuntos.ColumnName = "GUID_FACTURA_PROVEEDOR_PUNTOS";
				colvarGuidFacturaProveedorPuntos.DataType = DbType.String;
				colvarGuidFacturaProveedorPuntos.MaxLength = 36;
				colvarGuidFacturaProveedorPuntos.AutoIncrement = false;
				colvarGuidFacturaProveedorPuntos.IsNullable = false;
				colvarGuidFacturaProveedorPuntos.IsPrimaryKey = false;
				colvarGuidFacturaProveedorPuntos.IsForeignKey = true;
				colvarGuidFacturaProveedorPuntos.IsReadOnly = false;
				colvarGuidFacturaProveedorPuntos.DefaultSetting = @"";
				
					colvarGuidFacturaProveedorPuntos.ForeignKeyTableName = "INV_FACTURAS_PROVEEDOR_PUNTOS";
				schema.Columns.Add(colvarGuidFacturaProveedorPuntos);
				
				TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
				colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
				colvarGuidProducto.DataType = DbType.String;
				colvarGuidProducto.MaxLength = 36;
				colvarGuidProducto.AutoIncrement = false;
				colvarGuidProducto.IsNullable = false;
				colvarGuidProducto.IsPrimaryKey = false;
				colvarGuidProducto.IsForeignKey = true;
				colvarGuidProducto.IsReadOnly = false;
				colvarGuidProducto.DefaultSetting = @"";
				
					colvarGuidProducto.ForeignKeyTableName = "INV_PRODUCTOS";
				schema.Columns.Add(colvarGuidProducto);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidDetalleFacturaProveedorPuntos")]
		[Bindable(true)]
		public string GuidDetalleFacturaProveedorPuntos 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleFacturaProveedorPuntos); }
			set { SetColumnValue(Columns.GuidDetalleFacturaProveedorPuntos, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("ValorBasePuntosTotal")]
		[Bindable(true)]
		public decimal ValorBasePuntosTotal 
		{
			get { return GetColumnValue<decimal>(Columns.ValorBasePuntosTotal); }
			set { SetColumnValue(Columns.ValorBasePuntosTotal, value); }
		}
		  
		[XmlAttribute("IvaTotal")]
		[Bindable(true)]
		public decimal IvaTotal 
		{
			get { return GetColumnValue<decimal>(Columns.IvaTotal); }
			set { SetColumnValue(Columns.IvaTotal, value); }
		}
		  
		[XmlAttribute("GuidFacturaProveedorPuntos")]
		[Bindable(true)]
		public string GuidFacturaProveedorPuntos 
		{
			get { return GetColumnValue<string>(Columns.GuidFacturaProveedorPuntos); }
			set { SetColumnValue(Columns.GuidFacturaProveedorPuntos, value); }
		}
		  
		[XmlAttribute("GuidProducto")]
		[Bindable(true)]
		public string GuidProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidProducto); }
			set { SetColumnValue(Columns.GuidProducto, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvHistoricoBodegaCollection InvHistoricoBodegaRecords()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvHistoricoBodegaCollection().Where(InvHistoricoBodega.Columns.GuidDetalleFacturaEmpresa, GuidDetalleFacturaProveedorPuntos).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvFacturasProveedorPunto ActiveRecord object related to this InvDetallesFacturaProveedorPunto
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasProveedorPunto InvFacturasProveedorPunto
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasProveedorPunto.FetchByID(this.GuidFacturaProveedorPuntos); }
			set { SetColumnValue("GUID_FACTURA_PROVEEDOR_PUNTOS", value.GuidFacturaProveedorPuntos); }
		}
		
		
		/// <summary>
		/// Returns a InvProducto ActiveRecord object related to this InvDetallesFacturaProveedorPunto
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvProducto InvProducto
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvProducto.FetchByID(this.GuidProducto); }
			set { SetColumnValue("GUID_PRODUCTO", value.Guid); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidDetalleFacturaProveedorPuntos,int varCantidad,decimal varValorBasePuntosTotal,decimal varIvaTotal,string varGuidFacturaProveedorPuntos,string varGuidProducto)
		{
			InvDetallesFacturaProveedorPunto item = new InvDetallesFacturaProveedorPunto();
			
			item.GuidDetalleFacturaProveedorPuntos = varGuidDetalleFacturaProveedorPuntos;
			
			item.Cantidad = varCantidad;
			
			item.ValorBasePuntosTotal = varValorBasePuntosTotal;
			
			item.IvaTotal = varIvaTotal;
			
			item.GuidFacturaProveedorPuntos = varGuidFacturaProveedorPuntos;
			
			item.GuidProducto = varGuidProducto;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidDetalleFacturaProveedorPuntos,int varCantidad,decimal varValorBasePuntosTotal,decimal varIvaTotal,string varGuidFacturaProveedorPuntos,string varGuidProducto)
		{
			InvDetallesFacturaProveedorPunto item = new InvDetallesFacturaProveedorPunto();
			
				item.GuidDetalleFacturaProveedorPuntos = varGuidDetalleFacturaProveedorPuntos;
			
				item.Cantidad = varCantidad;
			
				item.ValorBasePuntosTotal = varValorBasePuntosTotal;
			
				item.IvaTotal = varIvaTotal;
			
				item.GuidFacturaProveedorPuntos = varGuidFacturaProveedorPuntos;
			
				item.GuidProducto = varGuidProducto;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidDetalleFacturaProveedorPuntosColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorBasePuntosTotalColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaTotalColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidFacturaProveedorPuntosColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidProductoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidDetalleFacturaProveedorPuntos = @"GUID_DETALLE_FACTURA_PROVEEDOR_PUNTOS";
			 public static string Cantidad = @"CANTIDAD";
			 public static string ValorBasePuntosTotal = @"VALOR_BASE_PUNTOS_TOTAL";
			 public static string IvaTotal = @"IVA_TOTAL";
			 public static string GuidFacturaProveedorPuntos = @"GUID_FACTURA_PROVEEDOR_PUNTOS";
			 public static string GuidProducto = @"GUID_PRODUCTO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
