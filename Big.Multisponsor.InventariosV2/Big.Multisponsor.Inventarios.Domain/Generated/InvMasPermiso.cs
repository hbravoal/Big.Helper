using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvMasPermiso class.
	/// </summary>
    [Serializable]
	public partial class InvMasPermisoCollection : ActiveList<InvMasPermiso, InvMasPermisoCollection>
	{	   
		public InvMasPermisoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasPermisoCollection</returns>
		public InvMasPermisoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasPermiso o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_PERMISOS table.
	/// </summary>
	[Serializable]
	public partial class InvMasPermiso : ActiveRecord<InvMasPermiso>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasPermiso()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasPermiso(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasPermiso(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasPermiso(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_PERMISOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPermiso = new TableSchema.TableColumn(schema);
				colvarIdPermiso.ColumnName = "ID_PERMISO";
				colvarIdPermiso.DataType = DbType.Int32;
				colvarIdPermiso.MaxLength = 0;
				colvarIdPermiso.AutoIncrement = false;
				colvarIdPermiso.IsNullable = false;
				colvarIdPermiso.IsPrimaryKey = true;
				colvarIdPermiso.IsForeignKey = false;
				colvarIdPermiso.IsReadOnly = false;
				colvarIdPermiso.DefaultSetting = @"";
				colvarIdPermiso.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPermiso);
				
				TableSchema.TableColumn colvarNombrePermiso = new TableSchema.TableColumn(schema);
				colvarNombrePermiso.ColumnName = "NOMBRE_PERMISO";
				colvarNombrePermiso.DataType = DbType.String;
				colvarNombrePermiso.MaxLength = 100;
				colvarNombrePermiso.AutoIncrement = false;
				colvarNombrePermiso.IsNullable = false;
				colvarNombrePermiso.IsPrimaryKey = false;
				colvarNombrePermiso.IsForeignKey = false;
				colvarNombrePermiso.IsReadOnly = false;
				colvarNombrePermiso.DefaultSetting = @"";
				colvarNombrePermiso.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombrePermiso);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_PERMISOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPermiso")]
		[Bindable(true)]
		public int IdPermiso 
		{
			get { return GetColumnValue<int>(Columns.IdPermiso); }
			set { SetColumnValue(Columns.IdPermiso, value); }
		}
		  
		[XmlAttribute("NombrePermiso")]
		[Bindable(true)]
		public string NombrePermiso 
		{
			get { return GetColumnValue<string>(Columns.NombrePermiso); }
			set { SetColumnValue(Columns.NombrePermiso, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvPerfilesPermisoCollection InvPerfilesPermisos()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvPerfilesPermisoCollection().Where(InvPerfilesPermiso.Columns.IdPermiso, IdPermiso).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdPermiso,string varNombrePermiso)
		{
			InvMasPermiso item = new InvMasPermiso();
			
			item.IdPermiso = varIdPermiso;
			
			item.NombrePermiso = varNombrePermiso;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdPermiso,string varNombrePermiso)
		{
			InvMasPermiso item = new InvMasPermiso();
			
				item.IdPermiso = varIdPermiso;
			
				item.NombrePermiso = varNombrePermiso;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdPermisoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombrePermisoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPermiso = @"ID_PERMISO";
			 public static string NombrePermiso = @"NOMBRE_PERMISO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
