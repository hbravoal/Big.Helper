using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_DETALLE_AJUSTE
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvDetalleAjusteController
    {
        // Preload our schema..
        InvDetalleAjuste thisSchemaLoad = new InvDetalleAjuste();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvDetalleAjusteCollection FetchAll()
        {
            InvDetalleAjusteCollection coll = new InvDetalleAjusteCollection();
            Query qry = new Query(InvDetalleAjuste.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetalleAjusteCollection FetchByID(object IdDetalleAjuste)
        {
            InvDetalleAjusteCollection coll = new InvDetalleAjusteCollection().Where("ID_DETALLE_AJUSTE", IdDetalleAjuste).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetalleAjusteCollection FetchByQuery(Query qry)
        {
            InvDetalleAjusteCollection coll = new InvDetalleAjusteCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdDetalleAjuste)
        {
            return (InvDetalleAjuste.Delete(IdDetalleAjuste) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdDetalleAjuste)
        {
            return (InvDetalleAjuste.Destroy(IdDetalleAjuste) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string IdDetalleAjuste,string GuidReferenciaProducto,int Cantidad,decimal CostoUnitario,decimal CostoTotal,decimal PrecioUnitario,decimal PrecioTotal,int NumeroItem,string GuidAjuste)
	    {
		    InvDetalleAjuste item = new InvDetalleAjuste();
		    
            item.IdDetalleAjuste = IdDetalleAjuste;
            
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
            item.Cantidad = Cantidad;
            
            item.CostoUnitario = CostoUnitario;
            
            item.CostoTotal = CostoTotal;
            
            item.PrecioUnitario = PrecioUnitario;
            
            item.PrecioTotal = PrecioTotal;
            
            item.NumeroItem = NumeroItem;
            
            item.GuidAjuste = GuidAjuste;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string IdDetalleAjuste,string GuidReferenciaProducto,int Cantidad,decimal CostoUnitario,decimal CostoTotal,decimal PrecioUnitario,decimal PrecioTotal,int NumeroItem,string GuidAjuste)
	    {
		    InvDetalleAjuste item = new InvDetalleAjuste();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdDetalleAjuste = IdDetalleAjuste;
				
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
			item.Cantidad = Cantidad;
				
			item.CostoUnitario = CostoUnitario;
				
			item.CostoTotal = CostoTotal;
				
			item.PrecioUnitario = PrecioUnitario;
				
			item.PrecioTotal = PrecioTotal;
				
			item.NumeroItem = NumeroItem;
				
			item.GuidAjuste = GuidAjuste;
				
	        item.Save(UserName);
	    }
    }
}
