using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_PRODUCTO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposProductoController
    {
        // Preload our schema..
        InvMasTiposProducto thisSchemaLoad = new InvMasTiposProducto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposProductoCollection FetchAll()
        {
            InvMasTiposProductoCollection coll = new InvMasTiposProductoCollection();
            Query qry = new Query(InvMasTiposProducto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposProductoCollection FetchByID(object IdTipoProducto)
        {
            InvMasTiposProductoCollection coll = new InvMasTiposProductoCollection().Where("ID_TIPO_PRODUCTO", IdTipoProducto).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposProductoCollection FetchByQuery(Query qry)
        {
            InvMasTiposProductoCollection coll = new InvMasTiposProductoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoProducto)
        {
            return (InvMasTiposProducto.Delete(IdTipoProducto) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoProducto)
        {
            return (InvMasTiposProducto.Destroy(IdTipoProducto) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Codigo,string Nombre,string Descripcion,int IdEstado)
	    {
		    InvMasTiposProducto item = new InvMasTiposProducto();
		    
            item.Codigo = Codigo;
            
            item.Nombre = Nombre;
            
            item.Descripcion = Descripcion;
            
            item.IdEstado = IdEstado;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoProducto,string Codigo,string Nombre,string Descripcion,int IdEstado)
	    {
		    InvMasTiposProducto item = new InvMasTiposProducto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoProducto = IdTipoProducto;
				
			item.Codigo = Codigo;
				
			item.Nombre = Nombre;
				
			item.Descripcion = Descripcion;
				
			item.IdEstado = IdEstado;
				
	        item.Save(UserName);
	    }
    }
}
