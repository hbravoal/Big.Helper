using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvAjuste class.
	/// </summary>
    [Serializable]
	public partial class InvAjusteCollection : ActiveList<InvAjuste, InvAjusteCollection>
	{	   
		public InvAjusteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvAjusteCollection</returns>
		public InvAjusteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvAjuste o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_AJUSTES table.
	/// </summary>
	[Serializable]
	public partial class InvAjuste : ActiveRecord<InvAjuste>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvAjuste()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvAjuste(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvAjuste(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvAjuste(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_AJUSTES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidAjuste = new TableSchema.TableColumn(schema);
				colvarGuidAjuste.ColumnName = "GUID_AJUSTE";
				colvarGuidAjuste.DataType = DbType.String;
				colvarGuidAjuste.MaxLength = 36;
				colvarGuidAjuste.AutoIncrement = false;
				colvarGuidAjuste.IsNullable = false;
				colvarGuidAjuste.IsPrimaryKey = true;
				colvarGuidAjuste.IsForeignKey = false;
				colvarGuidAjuste.IsReadOnly = false;
				colvarGuidAjuste.DefaultSetting = @"";
				colvarGuidAjuste.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidAjuste);
				
				TableSchema.TableColumn colvarConsecutivo = new TableSchema.TableColumn(schema);
				colvarConsecutivo.ColumnName = "CONSECUTIVO";
				colvarConsecutivo.DataType = DbType.Int32;
				colvarConsecutivo.MaxLength = 0;
				colvarConsecutivo.AutoIncrement = false;
				colvarConsecutivo.IsNullable = false;
				colvarConsecutivo.IsPrimaryKey = false;
				colvarConsecutivo.IsForeignKey = false;
				colvarConsecutivo.IsReadOnly = false;
				colvarConsecutivo.DefaultSetting = @"";
				colvarConsecutivo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsecutivo);
				
				TableSchema.TableColumn colvarIdBodega = new TableSchema.TableColumn(schema);
				colvarIdBodega.ColumnName = "ID_BODEGA";
				colvarIdBodega.DataType = DbType.Int32;
				colvarIdBodega.MaxLength = 0;
				colvarIdBodega.AutoIncrement = false;
				colvarIdBodega.IsNullable = false;
				colvarIdBodega.IsPrimaryKey = false;
				colvarIdBodega.IsForeignKey = true;
				colvarIdBodega.IsReadOnly = false;
				colvarIdBodega.DefaultSetting = @"";
				
					colvarIdBodega.ForeignKeyTableName = "INV_BODEGAS";
				schema.Columns.Add(colvarIdBodega);
				
				TableSchema.TableColumn colvarTotalBase = new TableSchema.TableColumn(schema);
				colvarTotalBase.ColumnName = "TOTAL_BASE";
				colvarTotalBase.DataType = DbType.Decimal;
				colvarTotalBase.MaxLength = 0;
				colvarTotalBase.AutoIncrement = false;
				colvarTotalBase.IsNullable = false;
				colvarTotalBase.IsPrimaryKey = false;
				colvarTotalBase.IsForeignKey = false;
				colvarTotalBase.IsReadOnly = false;
				colvarTotalBase.DefaultSetting = @"";
				colvarTotalBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalBase);
				
				TableSchema.TableColumn colvarPrecioBase = new TableSchema.TableColumn(schema);
				colvarPrecioBase.ColumnName = "PRECIO_BASE";
				colvarPrecioBase.DataType = DbType.Decimal;
				colvarPrecioBase.MaxLength = 0;
				colvarPrecioBase.AutoIncrement = false;
				colvarPrecioBase.IsNullable = false;
				colvarPrecioBase.IsPrimaryKey = false;
				colvarPrecioBase.IsForeignKey = false;
				colvarPrecioBase.IsReadOnly = false;
				colvarPrecioBase.DefaultSetting = @"";
				colvarPrecioBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrecioBase);
				
				TableSchema.TableColumn colvarFechaAjuste = new TableSchema.TableColumn(schema);
				colvarFechaAjuste.ColumnName = "FECHA_AJUSTE";
				colvarFechaAjuste.DataType = DbType.DateTime;
				colvarFechaAjuste.MaxLength = 0;
				colvarFechaAjuste.AutoIncrement = false;
				colvarFechaAjuste.IsNullable = false;
				colvarFechaAjuste.IsPrimaryKey = false;
				colvarFechaAjuste.IsForeignKey = false;
				colvarFechaAjuste.IsReadOnly = false;
				colvarFechaAjuste.DefaultSetting = @"";
				colvarFechaAjuste.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaAjuste);
				
				TableSchema.TableColumn colvarValorEnLetrasTotal = new TableSchema.TableColumn(schema);
				colvarValorEnLetrasTotal.ColumnName = "VALOR_EN_LETRAS_TOTAL";
				colvarValorEnLetrasTotal.DataType = DbType.String;
				colvarValorEnLetrasTotal.MaxLength = 1000;
				colvarValorEnLetrasTotal.AutoIncrement = false;
				colvarValorEnLetrasTotal.IsNullable = false;
				colvarValorEnLetrasTotal.IsPrimaryKey = false;
				colvarValorEnLetrasTotal.IsForeignKey = false;
				colvarValorEnLetrasTotal.IsReadOnly = false;
				colvarValorEnLetrasTotal.DefaultSetting = @"";
				colvarValorEnLetrasTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorEnLetrasTotal);
				
				TableSchema.TableColumn colvarObservaciones = new TableSchema.TableColumn(schema);
				colvarObservaciones.ColumnName = "OBSERVACIONES";
				colvarObservaciones.DataType = DbType.String;
				colvarObservaciones.MaxLength = 200;
				colvarObservaciones.AutoIncrement = false;
				colvarObservaciones.IsNullable = true;
				colvarObservaciones.IsPrimaryKey = false;
				colvarObservaciones.IsForeignKey = false;
				colvarObservaciones.IsReadOnly = false;
				colvarObservaciones.DefaultSetting = @"";
				colvarObservaciones.ForeignKeyTableName = "";
				schema.Columns.Add(colvarObservaciones);
				
				TableSchema.TableColumn colvarEntrada = new TableSchema.TableColumn(schema);
				colvarEntrada.ColumnName = "ENTRADA";
				colvarEntrada.DataType = DbType.Boolean;
				colvarEntrada.MaxLength = 0;
				colvarEntrada.AutoIncrement = false;
				colvarEntrada.IsNullable = false;
				colvarEntrada.IsPrimaryKey = false;
				colvarEntrada.IsForeignKey = false;
				colvarEntrada.IsReadOnly = false;
				colvarEntrada.DefaultSetting = @"";
				colvarEntrada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntrada);
				
				TableSchema.TableColumn colvarValorEnLetrasPrecio = new TableSchema.TableColumn(schema);
				colvarValorEnLetrasPrecio.ColumnName = "VALOR_EN_LETRAS_PRECIO";
				colvarValorEnLetrasPrecio.DataType = DbType.String;
				colvarValorEnLetrasPrecio.MaxLength = 1000;
				colvarValorEnLetrasPrecio.AutoIncrement = false;
				colvarValorEnLetrasPrecio.IsNullable = false;
				colvarValorEnLetrasPrecio.IsPrimaryKey = false;
				colvarValorEnLetrasPrecio.IsForeignKey = false;
				colvarValorEnLetrasPrecio.IsReadOnly = false;
				colvarValorEnLetrasPrecio.DefaultSetting = @"";
				colvarValorEnLetrasPrecio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorEnLetrasPrecio);
				
				TableSchema.TableColumn colvarAnulada = new TableSchema.TableColumn(schema);
				colvarAnulada.ColumnName = "ANULADA";
				colvarAnulada.DataType = DbType.Boolean;
				colvarAnulada.MaxLength = 0;
				colvarAnulada.AutoIncrement = false;
				colvarAnulada.IsNullable = true;
				colvarAnulada.IsPrimaryKey = false;
				colvarAnulada.IsForeignKey = false;
				colvarAnulada.IsReadOnly = false;
				colvarAnulada.DefaultSetting = @"";
				colvarAnulada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAnulada);
				
				TableSchema.TableColumn colvarGuidUsuarioAnulada = new TableSchema.TableColumn(schema);
				colvarGuidUsuarioAnulada.ColumnName = "GUID_USUARIO_ANULADA";
				colvarGuidUsuarioAnulada.DataType = DbType.String;
				colvarGuidUsuarioAnulada.MaxLength = 36;
				colvarGuidUsuarioAnulada.AutoIncrement = false;
				colvarGuidUsuarioAnulada.IsNullable = true;
				colvarGuidUsuarioAnulada.IsPrimaryKey = false;
				colvarGuidUsuarioAnulada.IsForeignKey = false;
				colvarGuidUsuarioAnulada.IsReadOnly = false;
				colvarGuidUsuarioAnulada.DefaultSetting = @"";
				colvarGuidUsuarioAnulada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidUsuarioAnulada);
				
				TableSchema.TableColumn colvarFechaAnulada = new TableSchema.TableColumn(schema);
				colvarFechaAnulada.ColumnName = "FECHA_ANULADA";
				colvarFechaAnulada.DataType = DbType.DateTime;
				colvarFechaAnulada.MaxLength = 0;
				colvarFechaAnulada.AutoIncrement = false;
				colvarFechaAnulada.IsNullable = true;
				colvarFechaAnulada.IsPrimaryKey = false;
				colvarFechaAnulada.IsForeignKey = false;
				colvarFechaAnulada.IsReadOnly = false;
				colvarFechaAnulada.DefaultSetting = @"";
				colvarFechaAnulada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaAnulada);
				
				TableSchema.TableColumn colvarAprobado = new TableSchema.TableColumn(schema);
				colvarAprobado.ColumnName = "APROBADO";
				colvarAprobado.DataType = DbType.Boolean;
				colvarAprobado.MaxLength = 0;
				colvarAprobado.AutoIncrement = false;
				colvarAprobado.IsNullable = false;
				colvarAprobado.IsPrimaryKey = false;
				colvarAprobado.IsForeignKey = false;
				colvarAprobado.IsReadOnly = false;
				
						colvarAprobado.DefaultSetting = @"((0))";
				colvarAprobado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAprobado);
				
				TableSchema.TableColumn colvarGuidUsuarioAprobacion = new TableSchema.TableColumn(schema);
				colvarGuidUsuarioAprobacion.ColumnName = "GUID_USUARIO_APROBACION";
				colvarGuidUsuarioAprobacion.DataType = DbType.String;
				colvarGuidUsuarioAprobacion.MaxLength = 36;
				colvarGuidUsuarioAprobacion.AutoIncrement = false;
				colvarGuidUsuarioAprobacion.IsNullable = true;
				colvarGuidUsuarioAprobacion.IsPrimaryKey = false;
				colvarGuidUsuarioAprobacion.IsForeignKey = false;
				colvarGuidUsuarioAprobacion.IsReadOnly = false;
				colvarGuidUsuarioAprobacion.DefaultSetting = @"";
				colvarGuidUsuarioAprobacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidUsuarioAprobacion);
				
				TableSchema.TableColumn colvarFechaAprobacion = new TableSchema.TableColumn(schema);
				colvarFechaAprobacion.ColumnName = "FECHA_APROBACION";
				colvarFechaAprobacion.DataType = DbType.DateTime;
				colvarFechaAprobacion.MaxLength = 0;
				colvarFechaAprobacion.AutoIncrement = false;
				colvarFechaAprobacion.IsNullable = true;
				colvarFechaAprobacion.IsPrimaryKey = false;
				colvarFechaAprobacion.IsForeignKey = false;
				colvarFechaAprobacion.IsReadOnly = false;
				colvarFechaAprobacion.DefaultSetting = @"";
				colvarFechaAprobacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaAprobacion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_AJUSTES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidAjuste")]
		[Bindable(true)]
		public string GuidAjuste 
		{
			get { return GetColumnValue<string>(Columns.GuidAjuste); }
			set { SetColumnValue(Columns.GuidAjuste, value); }
		}
		  
		[XmlAttribute("Consecutivo")]
		[Bindable(true)]
		public int Consecutivo 
		{
			get { return GetColumnValue<int>(Columns.Consecutivo); }
			set { SetColumnValue(Columns.Consecutivo, value); }
		}
		  
		[XmlAttribute("IdBodega")]
		[Bindable(true)]
		public int IdBodega 
		{
			get { return GetColumnValue<int>(Columns.IdBodega); }
			set { SetColumnValue(Columns.IdBodega, value); }
		}
		  
		[XmlAttribute("TotalBase")]
		[Bindable(true)]
		public decimal TotalBase 
		{
			get { return GetColumnValue<decimal>(Columns.TotalBase); }
			set { SetColumnValue(Columns.TotalBase, value); }
		}
		  
		[XmlAttribute("PrecioBase")]
		[Bindable(true)]
		public decimal PrecioBase 
		{
			get { return GetColumnValue<decimal>(Columns.PrecioBase); }
			set { SetColumnValue(Columns.PrecioBase, value); }
		}
		  
		[XmlAttribute("FechaAjuste")]
		[Bindable(true)]
		public DateTime FechaAjuste 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaAjuste); }
			set { SetColumnValue(Columns.FechaAjuste, value); }
		}
		  
		[XmlAttribute("ValorEnLetrasTotal")]
		[Bindable(true)]
		public string ValorEnLetrasTotal 
		{
			get { return GetColumnValue<string>(Columns.ValorEnLetrasTotal); }
			set { SetColumnValue(Columns.ValorEnLetrasTotal, value); }
		}
		  
		[XmlAttribute("Observaciones")]
		[Bindable(true)]
		public string Observaciones 
		{
			get { return GetColumnValue<string>(Columns.Observaciones); }
			set { SetColumnValue(Columns.Observaciones, value); }
		}
		  
		[XmlAttribute("Entrada")]
		[Bindable(true)]
		public bool Entrada 
		{
			get { return GetColumnValue<bool>(Columns.Entrada); }
			set { SetColumnValue(Columns.Entrada, value); }
		}
		  
		[XmlAttribute("ValorEnLetrasPrecio")]
		[Bindable(true)]
		public string ValorEnLetrasPrecio 
		{
			get { return GetColumnValue<string>(Columns.ValorEnLetrasPrecio); }
			set { SetColumnValue(Columns.ValorEnLetrasPrecio, value); }
		}
		  
		[XmlAttribute("Anulada")]
		[Bindable(true)]
		public bool? Anulada 
		{
			get { return GetColumnValue<bool?>(Columns.Anulada); }
			set { SetColumnValue(Columns.Anulada, value); }
		}
		  
		[XmlAttribute("GuidUsuarioAnulada")]
		[Bindable(true)]
		public string GuidUsuarioAnulada 
		{
			get { return GetColumnValue<string>(Columns.GuidUsuarioAnulada); }
			set { SetColumnValue(Columns.GuidUsuarioAnulada, value); }
		}
		  
		[XmlAttribute("FechaAnulada")]
		[Bindable(true)]
		public DateTime? FechaAnulada 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaAnulada); }
			set { SetColumnValue(Columns.FechaAnulada, value); }
		}
		  
		[XmlAttribute("Aprobado")]
		[Bindable(true)]
		public bool Aprobado 
		{
			get { return GetColumnValue<bool>(Columns.Aprobado); }
			set { SetColumnValue(Columns.Aprobado, value); }
		}
		  
		[XmlAttribute("GuidUsuarioAprobacion")]
		[Bindable(true)]
		public string GuidUsuarioAprobacion 
		{
			get { return GetColumnValue<string>(Columns.GuidUsuarioAprobacion); }
			set { SetColumnValue(Columns.GuidUsuarioAprobacion, value); }
		}
		  
		[XmlAttribute("FechaAprobacion")]
		[Bindable(true)]
		public DateTime? FechaAprobacion 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaAprobacion); }
			set { SetColumnValue(Columns.FechaAprobacion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvDetalleAjusteCollection InvDetalleAjusteRecords()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvDetalleAjusteCollection().Where(InvDetalleAjuste.Columns.GuidAjuste, GuidAjuste).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvBodega ActiveRecord object related to this InvAjuste
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvBodega InvBodega
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvBodega.FetchByID(this.IdBodega); }
			set { SetColumnValue("ID_BODEGA", value.IdBodega); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidAjuste,int varConsecutivo,int varIdBodega,decimal varTotalBase,decimal varPrecioBase,DateTime varFechaAjuste,string varValorEnLetrasTotal,string varObservaciones,bool varEntrada,string varValorEnLetrasPrecio,bool? varAnulada,string varGuidUsuarioAnulada,DateTime? varFechaAnulada,bool varAprobado,string varGuidUsuarioAprobacion,DateTime? varFechaAprobacion)
		{
			InvAjuste item = new InvAjuste();
			
			item.GuidAjuste = varGuidAjuste;
			
			item.Consecutivo = varConsecutivo;
			
			item.IdBodega = varIdBodega;
			
			item.TotalBase = varTotalBase;
			
			item.PrecioBase = varPrecioBase;
			
			item.FechaAjuste = varFechaAjuste;
			
			item.ValorEnLetrasTotal = varValorEnLetrasTotal;
			
			item.Observaciones = varObservaciones;
			
			item.Entrada = varEntrada;
			
			item.ValorEnLetrasPrecio = varValorEnLetrasPrecio;
			
			item.Anulada = varAnulada;
			
			item.GuidUsuarioAnulada = varGuidUsuarioAnulada;
			
			item.FechaAnulada = varFechaAnulada;
			
			item.Aprobado = varAprobado;
			
			item.GuidUsuarioAprobacion = varGuidUsuarioAprobacion;
			
			item.FechaAprobacion = varFechaAprobacion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidAjuste,int varConsecutivo,int varIdBodega,decimal varTotalBase,decimal varPrecioBase,DateTime varFechaAjuste,string varValorEnLetrasTotal,string varObservaciones,bool varEntrada,string varValorEnLetrasPrecio,bool? varAnulada,string varGuidUsuarioAnulada,DateTime? varFechaAnulada,bool varAprobado,string varGuidUsuarioAprobacion,DateTime? varFechaAprobacion)
		{
			InvAjuste item = new InvAjuste();
			
				item.GuidAjuste = varGuidAjuste;
			
				item.Consecutivo = varConsecutivo;
			
				item.IdBodega = varIdBodega;
			
				item.TotalBase = varTotalBase;
			
				item.PrecioBase = varPrecioBase;
			
				item.FechaAjuste = varFechaAjuste;
			
				item.ValorEnLetrasTotal = varValorEnLetrasTotal;
			
				item.Observaciones = varObservaciones;
			
				item.Entrada = varEntrada;
			
				item.ValorEnLetrasPrecio = varValorEnLetrasPrecio;
			
				item.Anulada = varAnulada;
			
				item.GuidUsuarioAnulada = varGuidUsuarioAnulada;
			
				item.FechaAnulada = varFechaAnulada;
			
				item.Aprobado = varAprobado;
			
				item.GuidUsuarioAprobacion = varGuidUsuarioAprobacion;
			
				item.FechaAprobacion = varFechaAprobacion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidAjusteColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsecutivoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdBodegaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalBaseColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PrecioBaseColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaAjusteColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorEnLetrasTotalColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ObservacionesColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn EntradaColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorEnLetrasPrecioColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AnuladaColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidUsuarioAnuladaColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaAnuladaColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn AprobadoColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidUsuarioAprobacionColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaAprobacionColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidAjuste = @"GUID_AJUSTE";
			 public static string Consecutivo = @"CONSECUTIVO";
			 public static string IdBodega = @"ID_BODEGA";
			 public static string TotalBase = @"TOTAL_BASE";
			 public static string PrecioBase = @"PRECIO_BASE";
			 public static string FechaAjuste = @"FECHA_AJUSTE";
			 public static string ValorEnLetrasTotal = @"VALOR_EN_LETRAS_TOTAL";
			 public static string Observaciones = @"OBSERVACIONES";
			 public static string Entrada = @"ENTRADA";
			 public static string ValorEnLetrasPrecio = @"VALOR_EN_LETRAS_PRECIO";
			 public static string Anulada = @"ANULADA";
			 public static string GuidUsuarioAnulada = @"GUID_USUARIO_ANULADA";
			 public static string FechaAnulada = @"FECHA_ANULADA";
			 public static string Aprobado = @"APROBADO";
			 public static string GuidUsuarioAprobacion = @"GUID_USUARIO_APROBACION";
			 public static string FechaAprobacion = @"FECHA_APROBACION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
