using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_CGUNO_CAUSACIONES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoCausacioneController
    {
        // Preload our schema..
        InvCgunoCausacione thisSchemaLoad = new InvCgunoCausacione();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoCausacioneCollection FetchAll()
        {
            InvCgunoCausacioneCollection coll = new InvCgunoCausacioneCollection();
            Query qry = new Query(InvCgunoCausacione.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCausacioneCollection FetchByID(object Id)
        {
            InvCgunoCausacioneCollection coll = new InvCgunoCausacioneCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCausacioneCollection FetchByQuery(Query qry)
        {
            InvCgunoCausacioneCollection coll = new InvCgunoCausacioneCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoCausacione.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoCausacione.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,string Codigo,int IdTipoCausacion,DateTime FechaCreacion)
	    {
		    InvCgunoCausacione item = new InvCgunoCausacione();
		    
            item.Guid = Guid;
            
            item.Codigo = Codigo;
            
            item.IdTipoCausacion = IdTipoCausacion;
            
            item.FechaCreacion = FechaCreacion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,decimal Id,string Codigo,int IdTipoCausacion,DateTime FechaCreacion)
	    {
		    InvCgunoCausacione item = new InvCgunoCausacione();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.Id = Id;
				
			item.Codigo = Codigo;
				
			item.IdTipoCausacion = IdTipoCausacion;
				
			item.FechaCreacion = FechaCreacion;
				
	        item.Save(UserName);
	    }
    }
}
