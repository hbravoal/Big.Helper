using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_USUARIOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvUsuarioController
    {
        // Preload our schema..
        InvUsuario thisSchemaLoad = new InvUsuario();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvUsuarioCollection FetchAll()
        {
            InvUsuarioCollection coll = new InvUsuarioCollection();
            Query qry = new Query(InvUsuario.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvUsuarioCollection FetchByID(object IdUsuario)
        {
            InvUsuarioCollection coll = new InvUsuarioCollection().Where("ID_USUARIO", IdUsuario).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvUsuarioCollection FetchByQuery(Query qry)
        {
            InvUsuarioCollection coll = new InvUsuarioCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdUsuario)
        {
            return (InvUsuario.Delete(IdUsuario) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdUsuario)
        {
            return (InvUsuario.Destroy(IdUsuario) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Identificacion,string Nombre,string Email,string NombreUsuario,string Password,int IdPerfil,DateTime FechaPassword,int IntentosFallidos,bool PrimeraVez,int IdEstado)
	    {
		    InvUsuario item = new InvUsuario();
		    
            item.Identificacion = Identificacion;
            
            item.Nombre = Nombre;
            
            item.Email = Email;
            
            item.NombreUsuario = NombreUsuario;
            
            item.Password = Password;
            
            item.IdPerfil = IdPerfil;
            
            item.FechaPassword = FechaPassword;
            
            item.IntentosFallidos = IntentosFallidos;
            
            item.PrimeraVez = PrimeraVez;
            
            item.IdEstado = IdEstado;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdUsuario,string Identificacion,string Nombre,string Email,string NombreUsuario,string Password,int IdPerfil,DateTime FechaPassword,int IntentosFallidos,bool PrimeraVez,int IdEstado)
	    {
		    InvUsuario item = new InvUsuario();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdUsuario = IdUsuario;
				
			item.Identificacion = Identificacion;
				
			item.Nombre = Nombre;
				
			item.Email = Email;
				
			item.NombreUsuario = NombreUsuario;
				
			item.Password = Password;
				
			item.IdPerfil = IdPerfil;
				
			item.FechaPassword = FechaPassword;
				
			item.IntentosFallidos = IntentosFallidos;
				
			item.PrimeraVez = PrimeraVez;
				
			item.IdEstado = IdEstado;
				
	        item.Save(UserName);
	    }
    }
}
