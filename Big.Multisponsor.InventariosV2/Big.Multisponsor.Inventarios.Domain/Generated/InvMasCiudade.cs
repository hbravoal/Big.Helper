using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvMasCiudade class.
	/// </summary>
    [Serializable]
	public partial class InvMasCiudadeCollection : ActiveList<InvMasCiudade, InvMasCiudadeCollection>
	{	   
		public InvMasCiudadeCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasCiudadeCollection</returns>
		public InvMasCiudadeCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasCiudade o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_CIUDADES table.
	/// </summary>
	[Serializable]
	public partial class InvMasCiudade : ActiveRecord<InvMasCiudade>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasCiudade()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasCiudade(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasCiudade(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasCiudade(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_CIUDADES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
				colvarIdCiudad.ColumnName = "ID_CIUDAD";
				colvarIdCiudad.DataType = DbType.Int32;
				colvarIdCiudad.MaxLength = 0;
				colvarIdCiudad.AutoIncrement = false;
				colvarIdCiudad.IsNullable = false;
				colvarIdCiudad.IsPrimaryKey = true;
				colvarIdCiudad.IsForeignKey = false;
				colvarIdCiudad.IsReadOnly = false;
				colvarIdCiudad.DefaultSetting = @"";
				colvarIdCiudad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdCiudad);
				
				TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
				colvarNombre.ColumnName = "NOMBRE";
				colvarNombre.DataType = DbType.String;
				colvarNombre.MaxLength = 50;
				colvarNombre.AutoIncrement = false;
				colvarNombre.IsNullable = false;
				colvarNombre.IsPrimaryKey = false;
				colvarNombre.IsForeignKey = false;
				colvarNombre.IsReadOnly = false;
				colvarNombre.DefaultSetting = @"";
				colvarNombre.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombre);
				
				TableSchema.TableColumn colvarIdDepartamento = new TableSchema.TableColumn(schema);
				colvarIdDepartamento.ColumnName = "ID_DEPARTAMENTO";
				colvarIdDepartamento.DataType = DbType.Int32;
				colvarIdDepartamento.MaxLength = 0;
				colvarIdDepartamento.AutoIncrement = false;
				colvarIdDepartamento.IsNullable = false;
				colvarIdDepartamento.IsPrimaryKey = false;
				colvarIdDepartamento.IsForeignKey = true;
				colvarIdDepartamento.IsReadOnly = false;
				colvarIdDepartamento.DefaultSetting = @"";
				
					colvarIdDepartamento.ForeignKeyTableName = "INV_MAS_DEPARTAMENTOS";
				schema.Columns.Add(colvarIdDepartamento);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_CIUDADES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdCiudad")]
		[Bindable(true)]
		public int IdCiudad 
		{
			get { return GetColumnValue<int>(Columns.IdCiudad); }
			set { SetColumnValue(Columns.IdCiudad, value); }
		}
		  
		[XmlAttribute("Nombre")]
		[Bindable(true)]
		public string Nombre 
		{
			get { return GetColumnValue<string>(Columns.Nombre); }
			set { SetColumnValue(Columns.Nombre, value); }
		}
		  
		[XmlAttribute("IdDepartamento")]
		[Bindable(true)]
		public int IdDepartamento 
		{
			get { return GetColumnValue<int>(Columns.IdDepartamento); }
			set { SetColumnValue(Columns.IdDepartamento, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvCostoEnvioAlternoCollection InvCostoEnvioAlternoRecords()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvCostoEnvioAlternoCollection().Where(InvCostoEnvioAlterno.Columns.IdCiudad, IdCiudad).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvEmpresaCollection InvEmpresas()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvEmpresaCollection().Where(InvEmpresa.Columns.IdCiudad, IdCiudad).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvFacturaCollection InvFacturas()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvFacturaCollection().Where(InvFactura.Columns.IdCiudadCliente, IdCiudad).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMarcaCollection InvMarcas()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvMarcaCollection().Where(InvMarca.Columns.IdCiudad, IdCiudad).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvProveedoreCollection InvProveedores()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvProveedoreCollection().Where(InvProveedore.Columns.IdCiudad, IdCiudad).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasDepartamento ActiveRecord object related to this InvMasCiudade
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasDepartamento InvMasDepartamento
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvMasDepartamento.FetchByID(this.IdDepartamento); }
			set { SetColumnValue("ID_DEPARTAMENTO", value.IdDepartamento); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdCiudad,string varNombre,int varIdDepartamento)
		{
			InvMasCiudade item = new InvMasCiudade();
			
			item.IdCiudad = varIdCiudad;
			
			item.Nombre = varNombre;
			
			item.IdDepartamento = varIdDepartamento;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdCiudad,string varNombre,int varIdDepartamento)
		{
			InvMasCiudade item = new InvMasCiudade();
			
				item.IdCiudad = varIdCiudad;
			
				item.Nombre = varNombre;
			
				item.IdDepartamento = varIdDepartamento;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdCiudadColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdDepartamentoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdCiudad = @"ID_CIUDAD";
			 public static string Nombre = @"NOMBRE";
			 public static string IdDepartamento = @"ID_DEPARTAMENTO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
