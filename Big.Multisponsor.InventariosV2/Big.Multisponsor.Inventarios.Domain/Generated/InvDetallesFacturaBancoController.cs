using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_DETALLES_FACTURA_BANCO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvDetallesFacturaBancoController
    {
        // Preload our schema..
        InvDetallesFacturaBanco thisSchemaLoad = new InvDetallesFacturaBanco();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvDetallesFacturaBancoCollection FetchAll()
        {
            InvDetallesFacturaBancoCollection coll = new InvDetallesFacturaBancoCollection();
            Query qry = new Query(InvDetallesFacturaBanco.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetallesFacturaBancoCollection FetchByID(object GuidDetalleFacturaBanco)
        {
            InvDetallesFacturaBancoCollection coll = new InvDetallesFacturaBancoCollection().Where("GUID_DETALLE_FACTURA_BANCO", GuidDetalleFacturaBanco).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetallesFacturaBancoCollection FetchByQuery(Query qry)
        {
            InvDetallesFacturaBancoCollection coll = new InvDetallesFacturaBancoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidDetalleFacturaBanco)
        {
            return (InvDetallesFacturaBanco.Delete(GuidDetalleFacturaBanco) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidDetalleFacturaBanco)
        {
            return (InvDetallesFacturaBanco.Destroy(GuidDetalleFacturaBanco) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidDetalleFacturaBanco,int Cantidad,decimal CostoBase,decimal Iva,string GuidFacturaBanco,string GuidReferenciaProducto)
	    {
		    InvDetallesFacturaBanco item = new InvDetallesFacturaBanco();
		    
            item.GuidDetalleFacturaBanco = GuidDetalleFacturaBanco;
            
            item.Cantidad = Cantidad;
            
            item.CostoBase = CostoBase;
            
            item.Iva = Iva;
            
            item.GuidFacturaBanco = GuidFacturaBanco;
            
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidDetalleFacturaBanco,int Cantidad,decimal CostoBase,decimal Iva,string GuidFacturaBanco,string GuidReferenciaProducto)
	    {
		    InvDetallesFacturaBanco item = new InvDetallesFacturaBanco();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidDetalleFacturaBanco = GuidDetalleFacturaBanco;
				
			item.Cantidad = Cantidad;
				
			item.CostoBase = CostoBase;
				
			item.Iva = Iva;
				
			item.GuidFacturaBanco = GuidFacturaBanco;
				
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
	        item.Save(UserName);
	    }
    }
}
