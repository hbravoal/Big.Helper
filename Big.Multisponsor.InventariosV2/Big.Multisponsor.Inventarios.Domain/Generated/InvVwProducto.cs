using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain{
    /// <summary>
    /// Strongly-typed collection for the InvVwProducto class.
    /// </summary>
    [Serializable]
    public partial class InvVwProductoCollection : ReadOnlyList<InvVwProducto, InvVwProductoCollection>
    {        
        public InvVwProductoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_PRODUCTOS view.
    /// </summary>
    [Serializable]
    public partial class InvVwProducto : ReadOnlyRecord<InvVwProducto>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_PRODUCTOS", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
                colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
                colvarGuidReferenciaProducto.DataType = DbType.String;
                colvarGuidReferenciaProducto.MaxLength = 36;
                colvarGuidReferenciaProducto.AutoIncrement = false;
                colvarGuidReferenciaProducto.IsNullable = false;
                colvarGuidReferenciaProducto.IsPrimaryKey = false;
                colvarGuidReferenciaProducto.IsForeignKey = false;
                colvarGuidReferenciaProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidReferenciaProducto);
                
                TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
                colvarCodigo.ColumnName = "CODIGO";
                colvarCodigo.DataType = DbType.AnsiString;
                colvarCodigo.MaxLength = 15;
                colvarCodigo.AutoIncrement = false;
                colvarCodigo.IsNullable = false;
                colvarCodigo.IsPrimaryKey = false;
                colvarCodigo.IsForeignKey = false;
                colvarCodigo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigo);
                
                TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
                colvarNombre.ColumnName = "NOMBRE";
                colvarNombre.DataType = DbType.String;
                colvarNombre.MaxLength = 150;
                colvarNombre.AutoIncrement = false;
                colvarNombre.IsNullable = false;
                colvarNombre.IsPrimaryKey = false;
                colvarNombre.IsForeignKey = false;
                colvarNombre.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombre);
                
                TableSchema.TableColumn colvarCodigoProveedor = new TableSchema.TableColumn(schema);
                colvarCodigoProveedor.ColumnName = "CODIGO_PROVEEDOR";
                colvarCodigoProveedor.DataType = DbType.AnsiString;
                colvarCodigoProveedor.MaxLength = 20;
                colvarCodigoProveedor.AutoIncrement = false;
                colvarCodigoProveedor.IsNullable = false;
                colvarCodigoProveedor.IsPrimaryKey = false;
                colvarCodigoProveedor.IsForeignKey = false;
                colvarCodigoProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoProveedor);
                
                TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
                colvarIdPrograma.ColumnName = "ID_PROGRAMA";
                colvarIdPrograma.DataType = DbType.Int32;
                colvarIdPrograma.MaxLength = 0;
                colvarIdPrograma.AutoIncrement = false;
                colvarIdPrograma.IsNullable = false;
                colvarIdPrograma.IsPrimaryKey = false;
                colvarIdPrograma.IsForeignKey = false;
                colvarIdPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdPrograma);
                
                TableSchema.TableColumn colvarRazonSocial = new TableSchema.TableColumn(schema);
                colvarRazonSocial.ColumnName = "RAZON_SOCIAL";
                colvarRazonSocial.DataType = DbType.String;
                colvarRazonSocial.MaxLength = 200;
                colvarRazonSocial.AutoIncrement = false;
                colvarRazonSocial.IsNullable = false;
                colvarRazonSocial.IsPrimaryKey = false;
                colvarRazonSocial.IsForeignKey = false;
                colvarRazonSocial.IsReadOnly = false;
                
                schema.Columns.Add(colvarRazonSocial);
                
                TableSchema.TableColumn colvarIdTipoEmpaque = new TableSchema.TableColumn(schema);
                colvarIdTipoEmpaque.ColumnName = "ID_TIPO_EMPAQUE";
                colvarIdTipoEmpaque.DataType = DbType.Int32;
                colvarIdTipoEmpaque.MaxLength = 0;
                colvarIdTipoEmpaque.AutoIncrement = false;
                colvarIdTipoEmpaque.IsNullable = false;
                colvarIdTipoEmpaque.IsPrimaryKey = false;
                colvarIdTipoEmpaque.IsForeignKey = false;
                colvarIdTipoEmpaque.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdTipoEmpaque);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_PRODUCTOS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwProducto()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwProducto(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwProducto(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwProducto(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("GuidReferenciaProducto")]
        [Bindable(true)]
        public string GuidReferenciaProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_REFERENCIA_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("GUID_REFERENCIA_PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("Codigo")]
        [Bindable(true)]
        public string Codigo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO");
		    }
            set 
		    {
			    SetColumnValue("CODIGO", value);
            }
        }
	      
        [XmlAttribute("Nombre")]
        [Bindable(true)]
        public string Nombre 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE", value);
            }
        }
	      
        [XmlAttribute("CodigoProveedor")]
        [Bindable(true)]
        public string CodigoProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("CODIGO_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("IdPrograma")]
        [Bindable(true)]
        public int IdPrograma 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("ID_PROGRAMA", value);
            }
        }
	      
        [XmlAttribute("RazonSocial")]
        [Bindable(true)]
        public string RazonSocial 
	    {
		    get
		    {
			    return GetColumnValue<string>("RAZON_SOCIAL");
		    }
            set 
		    {
			    SetColumnValue("RAZON_SOCIAL", value);
            }
        }
	      
        [XmlAttribute("IdTipoEmpaque")]
        [Bindable(true)]
        public int IdTipoEmpaque 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_TIPO_EMPAQUE");
		    }
            set 
		    {
			    SetColumnValue("ID_TIPO_EMPAQUE", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
            
            public static string Codigo = @"CODIGO";
            
            public static string Nombre = @"NOMBRE";
            
            public static string CodigoProveedor = @"CODIGO_PROVEEDOR";
            
            public static string IdPrograma = @"ID_PROGRAMA";
            
            public static string RazonSocial = @"RAZON_SOCIAL";
            
            public static string IdTipoEmpaque = @"ID_TIPO_EMPAQUE";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
