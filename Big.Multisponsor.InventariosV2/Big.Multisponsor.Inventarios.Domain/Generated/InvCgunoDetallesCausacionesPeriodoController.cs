using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_CGUNO_DETALLES_CAUSACIONES_PERIODO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoDetallesCausacionesPeriodoController
    {
        // Preload our schema..
        InvCgunoDetallesCausacionesPeriodo thisSchemaLoad = new InvCgunoDetallesCausacionesPeriodo();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoDetallesCausacionesPeriodoCollection FetchAll()
        {
            InvCgunoDetallesCausacionesPeriodoCollection coll = new InvCgunoDetallesCausacionesPeriodoCollection();
            Query qry = new Query(InvCgunoDetallesCausacionesPeriodo.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoDetallesCausacionesPeriodoCollection FetchByID(object Id)
        {
            InvCgunoDetallesCausacionesPeriodoCollection coll = new InvCgunoDetallesCausacionesPeriodoCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoDetallesCausacionesPeriodoCollection FetchByQuery(Query qry)
        {
            InvCgunoDetallesCausacionesPeriodoCollection coll = new InvCgunoDetallesCausacionesPeriodoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoDetallesCausacionesPeriodo.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoDetallesCausacionesPeriodo.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,int IdCuenta,decimal IdCausacionPeriodo,int? IdNaturaleza,decimal? ValorCausacion,string GuidReferencia,int? Cantidad,string CentroUtilidad,string Nit)
	    {
		    InvCgunoDetallesCausacionesPeriodo item = new InvCgunoDetallesCausacionesPeriodo();
		    
            item.Guid = Guid;
            
            item.IdCuenta = IdCuenta;
            
            item.IdCausacionPeriodo = IdCausacionPeriodo;
            
            item.IdNaturaleza = IdNaturaleza;
            
            item.ValorCausacion = ValorCausacion;
            
            item.GuidReferencia = GuidReferencia;
            
            item.Cantidad = Cantidad;
            
            item.CentroUtilidad = CentroUtilidad;
            
            item.Nit = Nit;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,decimal Id,int IdCuenta,decimal IdCausacionPeriodo,int? IdNaturaleza,decimal? ValorCausacion,string GuidReferencia,int? Cantidad,string CentroUtilidad,string Nit)
	    {
		    InvCgunoDetallesCausacionesPeriodo item = new InvCgunoDetallesCausacionesPeriodo();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.Id = Id;
				
			item.IdCuenta = IdCuenta;
				
			item.IdCausacionPeriodo = IdCausacionPeriodo;
				
			item.IdNaturaleza = IdNaturaleza;
				
			item.ValorCausacion = ValorCausacion;
				
			item.GuidReferencia = GuidReferencia;
				
			item.Cantidad = Cantidad;
				
			item.CentroUtilidad = CentroUtilidad;
				
			item.Nit = Nit;
				
	        item.Save(UserName);
	    }
    }
}
