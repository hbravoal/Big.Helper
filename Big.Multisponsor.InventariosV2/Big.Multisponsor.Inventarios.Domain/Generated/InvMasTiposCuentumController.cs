using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_CUENTA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposCuentumController
    {
        // Preload our schema..
        InvMasTiposCuentum thisSchemaLoad = new InvMasTiposCuentum();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposCuentumCollection FetchAll()
        {
            InvMasTiposCuentumCollection coll = new InvMasTiposCuentumCollection();
            Query qry = new Query(InvMasTiposCuentum.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposCuentumCollection FetchByID(object IdTipoCuenta)
        {
            InvMasTiposCuentumCollection coll = new InvMasTiposCuentumCollection().Where("ID_TIPO_CUENTA", IdTipoCuenta).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposCuentumCollection FetchByQuery(Query qry)
        {
            InvMasTiposCuentumCollection coll = new InvMasTiposCuentumCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoCuenta)
        {
            return (InvMasTiposCuentum.Delete(IdTipoCuenta) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoCuenta)
        {
            return (InvMasTiposCuentum.Destroy(IdTipoCuenta) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Nombre)
	    {
		    InvMasTiposCuentum item = new InvMasTiposCuentum();
		    
            item.Nombre = Nombre;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoCuenta,string Nombre)
	    {
		    InvMasTiposCuentum item = new InvMasTiposCuentum();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoCuenta = IdTipoCuenta;
				
			item.Nombre = Nombre;
				
	        item.Save(UserName);
	    }
    }
}
