using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain{
    /// <summary>
    /// Strongly-typed collection for the InvVwPreciosExportar class.
    /// </summary>
    [Serializable]
    public partial class InvVwPreciosExportarCollection : ReadOnlyList<InvVwPreciosExportar, InvVwPreciosExportarCollection>
    {        
        public InvVwPreciosExportarCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_PRECIOS_EXPORTAR view.
    /// </summary>
    [Serializable]
    public partial class InvVwPreciosExportar : ReadOnlyRecord<InvVwPreciosExportar>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_PRECIOS_EXPORTAR", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.String;
                colvarGuid.MaxLength = 36;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
                colvarCodigo.ColumnName = "CODIGO";
                colvarCodigo.DataType = DbType.AnsiString;
                colvarCodigo.MaxLength = 15;
                colvarCodigo.AutoIncrement = false;
                colvarCodigo.IsNullable = false;
                colvarCodigo.IsPrimaryKey = false;
                colvarCodigo.IsForeignKey = false;
                colvarCodigo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigo);
                
                TableSchema.TableColumn colvarNombreProducto = new TableSchema.TableColumn(schema);
                colvarNombreProducto.ColumnName = "Nombre Producto";
                colvarNombreProducto.DataType = DbType.String;
                colvarNombreProducto.MaxLength = 100;
                colvarNombreProducto.AutoIncrement = false;
                colvarNombreProducto.IsNullable = false;
                colvarNombreProducto.IsPrimaryKey = false;
                colvarNombreProducto.IsForeignKey = false;
                colvarNombreProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreProducto);
                
                TableSchema.TableColumn colvarDescripciónProducto = new TableSchema.TableColumn(schema);
                colvarDescripciónProducto.ColumnName = "Descripción Producto";
                colvarDescripciónProducto.DataType = DbType.String;
                colvarDescripciónProducto.MaxLength = 4000;
                colvarDescripciónProducto.AutoIncrement = false;
                colvarDescripciónProducto.IsNullable = true;
                colvarDescripciónProducto.IsPrimaryKey = false;
                colvarDescripciónProducto.IsForeignKey = false;
                colvarDescripciónProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescripciónProducto);
                
                TableSchema.TableColumn colvarLocalizacion = new TableSchema.TableColumn(schema);
                colvarLocalizacion.ColumnName = "Localizacion";
                colvarLocalizacion.DataType = DbType.String;
                colvarLocalizacion.MaxLength = 200;
                colvarLocalizacion.AutoIncrement = false;
                colvarLocalizacion.IsNullable = false;
                colvarLocalizacion.IsPrimaryKey = false;
                colvarLocalizacion.IsForeignKey = false;
                colvarLocalizacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocalizacion);
                
                TableSchema.TableColumn colvarCodigoBarras = new TableSchema.TableColumn(schema);
                colvarCodigoBarras.ColumnName = "CodigoBarras";
                colvarCodigoBarras.DataType = DbType.String;
                colvarCodigoBarras.MaxLength = 50;
                colvarCodigoBarras.AutoIncrement = false;
                colvarCodigoBarras.IsNullable = false;
                colvarCodigoBarras.IsPrimaryKey = false;
                colvarCodigoBarras.IsForeignKey = false;
                colvarCodigoBarras.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoBarras);
                
                TableSchema.TableColumn colvarIdTipoEmpaque = new TableSchema.TableColumn(schema);
                colvarIdTipoEmpaque.ColumnName = "IdTipoEmpaque";
                colvarIdTipoEmpaque.DataType = DbType.Int32;
                colvarIdTipoEmpaque.MaxLength = 0;
                colvarIdTipoEmpaque.AutoIncrement = false;
                colvarIdTipoEmpaque.IsNullable = false;
                colvarIdTipoEmpaque.IsPrimaryKey = false;
                colvarIdTipoEmpaque.IsForeignKey = false;
                colvarIdTipoEmpaque.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdTipoEmpaque);
                
                TableSchema.TableColumn colvarUnidadesPorEmpaque = new TableSchema.TableColumn(schema);
                colvarUnidadesPorEmpaque.ColumnName = "Unidades Por Empaque";
                colvarUnidadesPorEmpaque.DataType = DbType.AnsiString;
                colvarUnidadesPorEmpaque.MaxLength = 3;
                colvarUnidadesPorEmpaque.AutoIncrement = false;
                colvarUnidadesPorEmpaque.IsNullable = false;
                colvarUnidadesPorEmpaque.IsPrimaryKey = false;
                colvarUnidadesPorEmpaque.IsForeignKey = false;
                colvarUnidadesPorEmpaque.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnidadesPorEmpaque);
                
                TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
                colvarCostoBase.ColumnName = "Costo Base";
                colvarCostoBase.DataType = DbType.Decimal;
                colvarCostoBase.MaxLength = 0;
                colvarCostoBase.AutoIncrement = false;
                colvarCostoBase.IsNullable = false;
                colvarCostoBase.IsPrimaryKey = false;
                colvarCostoBase.IsForeignKey = false;
                colvarCostoBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarCostoBase);
                
                TableSchema.TableColumn colvarPrecioDeVentaBase = new TableSchema.TableColumn(schema);
                colvarPrecioDeVentaBase.ColumnName = "Precio de Venta Base";
                colvarPrecioDeVentaBase.DataType = DbType.Decimal;
                colvarPrecioDeVentaBase.MaxLength = 0;
                colvarPrecioDeVentaBase.AutoIncrement = false;
                colvarPrecioDeVentaBase.IsNullable = true;
                colvarPrecioDeVentaBase.IsPrimaryKey = false;
                colvarPrecioDeVentaBase.IsForeignKey = false;
                colvarPrecioDeVentaBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrecioDeVentaBase);
                
                TableSchema.TableColumn colvarCostoDeEnvíoBase = new TableSchema.TableColumn(schema);
                colvarCostoDeEnvíoBase.ColumnName = "Costo de Envío Base";
                colvarCostoDeEnvíoBase.DataType = DbType.Decimal;
                colvarCostoDeEnvíoBase.MaxLength = 0;
                colvarCostoDeEnvíoBase.AutoIncrement = false;
                colvarCostoDeEnvíoBase.IsNullable = true;
                colvarCostoDeEnvíoBase.IsPrimaryKey = false;
                colvarCostoDeEnvíoBase.IsForeignKey = false;
                colvarCostoDeEnvíoBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarCostoDeEnvíoBase);
                
                TableSchema.TableColumn colvarLargo = new TableSchema.TableColumn(schema);
                colvarLargo.ColumnName = "LARGO";
                colvarLargo.DataType = DbType.Decimal;
                colvarLargo.MaxLength = 0;
                colvarLargo.AutoIncrement = false;
                colvarLargo.IsNullable = false;
                colvarLargo.IsPrimaryKey = false;
                colvarLargo.IsForeignKey = false;
                colvarLargo.IsReadOnly = false;
                
                schema.Columns.Add(colvarLargo);
                
                TableSchema.TableColumn colvarAncho = new TableSchema.TableColumn(schema);
                colvarAncho.ColumnName = "ANCHO";
                colvarAncho.DataType = DbType.Decimal;
                colvarAncho.MaxLength = 0;
                colvarAncho.AutoIncrement = false;
                colvarAncho.IsNullable = false;
                colvarAncho.IsPrimaryKey = false;
                colvarAncho.IsForeignKey = false;
                colvarAncho.IsReadOnly = false;
                
                schema.Columns.Add(colvarAncho);
                
                TableSchema.TableColumn colvarAlto = new TableSchema.TableColumn(schema);
                colvarAlto.ColumnName = "ALTO";
                colvarAlto.DataType = DbType.Decimal;
                colvarAlto.MaxLength = 0;
                colvarAlto.AutoIncrement = false;
                colvarAlto.IsNullable = false;
                colvarAlto.IsPrimaryKey = false;
                colvarAlto.IsForeignKey = false;
                colvarAlto.IsReadOnly = false;
                
                schema.Columns.Add(colvarAlto);
                
                TableSchema.TableColumn colvarPeso = new TableSchema.TableColumn(schema);
                colvarPeso.ColumnName = "PESO";
                colvarPeso.DataType = DbType.Decimal;
                colvarPeso.MaxLength = 0;
                colvarPeso.AutoIncrement = false;
                colvarPeso.IsNullable = false;
                colvarPeso.IsPrimaryKey = false;
                colvarPeso.IsForeignKey = false;
                colvarPeso.IsReadOnly = false;
                
                schema.Columns.Add(colvarPeso);
                
                TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
                colvarIdProveedor.ColumnName = "IdProveedor";
                colvarIdProveedor.DataType = DbType.Int32;
                colvarIdProveedor.MaxLength = 0;
                colvarIdProveedor.AutoIncrement = false;
                colvarIdProveedor.IsNullable = false;
                colvarIdProveedor.IsPrimaryKey = false;
                colvarIdProveedor.IsForeignKey = false;
                colvarIdProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdProveedor);
                
                TableSchema.TableColumn colvarIdTipoProducto = new TableSchema.TableColumn(schema);
                colvarIdTipoProducto.ColumnName = "IdTipoProducto";
                colvarIdTipoProducto.DataType = DbType.Int32;
                colvarIdTipoProducto.MaxLength = 0;
                colvarIdTipoProducto.AutoIncrement = false;
                colvarIdTipoProducto.IsNullable = false;
                colvarIdTipoProducto.IsPrimaryKey = false;
                colvarIdTipoProducto.IsForeignKey = false;
                colvarIdTipoProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdTipoProducto);
                
                TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
                colvarIva.ColumnName = "IVA";
                colvarIva.DataType = DbType.Decimal;
                colvarIva.MaxLength = 0;
                colvarIva.AutoIncrement = false;
                colvarIva.IsNullable = false;
                colvarIva.IsPrimaryKey = false;
                colvarIva.IsForeignKey = false;
                colvarIva.IsReadOnly = false;
                
                schema.Columns.Add(colvarIva);
                
                TableSchema.TableColumn colvarRetenciónEnLaFuente = new TableSchema.TableColumn(schema);
                colvarRetenciónEnLaFuente.ColumnName = "Retención en la Fuente";
                colvarRetenciónEnLaFuente.DataType = DbType.Decimal;
                colvarRetenciónEnLaFuente.MaxLength = 0;
                colvarRetenciónEnLaFuente.AutoIncrement = false;
                colvarRetenciónEnLaFuente.IsNullable = false;
                colvarRetenciónEnLaFuente.IsPrimaryKey = false;
                colvarRetenciónEnLaFuente.IsForeignKey = false;
                colvarRetenciónEnLaFuente.IsReadOnly = false;
                
                schema.Columns.Add(colvarRetenciónEnLaFuente);
                
                TableSchema.TableColumn colvarDescuento = new TableSchema.TableColumn(schema);
                colvarDescuento.ColumnName = "DESCUENTO";
                colvarDescuento.DataType = DbType.Decimal;
                colvarDescuento.MaxLength = 0;
                colvarDescuento.AutoIncrement = false;
                colvarDescuento.IsNullable = true;
                colvarDescuento.IsPrimaryKey = false;
                colvarDescuento.IsForeignKey = false;
                colvarDescuento.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescuento);
                
                TableSchema.TableColumn colvarNombreDelPrograma = new TableSchema.TableColumn(schema);
                colvarNombreDelPrograma.ColumnName = "Nombre del Programa";
                colvarNombreDelPrograma.DataType = DbType.String;
                colvarNombreDelPrograma.MaxLength = 100;
                colvarNombreDelPrograma.AutoIncrement = false;
                colvarNombreDelPrograma.IsNullable = true;
                colvarNombreDelPrograma.IsPrimaryKey = false;
                colvarNombreDelPrograma.IsForeignKey = false;
                colvarNombreDelPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreDelPrograma);
                
                TableSchema.TableColumn colvarTipoDelivery = new TableSchema.TableColumn(schema);
                colvarTipoDelivery.ColumnName = "Tipo Delivery";
                colvarTipoDelivery.DataType = DbType.String;
                colvarTipoDelivery.MaxLength = 100;
                colvarTipoDelivery.AutoIncrement = false;
                colvarTipoDelivery.IsNullable = true;
                colvarTipoDelivery.IsPrimaryKey = false;
                colvarTipoDelivery.IsForeignKey = false;
                colvarTipoDelivery.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoDelivery);
                
                TableSchema.TableColumn colvarProveedor = new TableSchema.TableColumn(schema);
                colvarProveedor.ColumnName = "Proveedor";
                colvarProveedor.DataType = DbType.String;
                colvarProveedor.MaxLength = 200;
                colvarProveedor.AutoIncrement = false;
                colvarProveedor.IsNullable = false;
                colvarProveedor.IsPrimaryKey = false;
                colvarProveedor.IsForeignKey = false;
                colvarProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarProveedor);
                
                TableSchema.TableColumn colvarIdMarca = new TableSchema.TableColumn(schema);
                colvarIdMarca.ColumnName = "ID_MARCA";
                colvarIdMarca.DataType = DbType.Int32;
                colvarIdMarca.MaxLength = 0;
                colvarIdMarca.AutoIncrement = false;
                colvarIdMarca.IsNullable = true;
                colvarIdMarca.IsPrimaryKey = false;
                colvarIdMarca.IsForeignKey = false;
                colvarIdMarca.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdMarca);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_PRECIOS_EXPORTAR",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwPreciosExportar()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwPreciosExportar(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwPreciosExportar(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwPreciosExportar(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public string Guid 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("Codigo")]
        [Bindable(true)]
        public string Codigo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO");
		    }
            set 
		    {
			    SetColumnValue("CODIGO", value);
            }
        }
	      
        [XmlAttribute("NombreProducto")]
        [Bindable(true)]
        public string NombreProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nombre Producto");
		    }
            set 
		    {
			    SetColumnValue("Nombre Producto", value);
            }
        }
	      
        [XmlAttribute("DescripciónProducto")]
        [Bindable(true)]
        public string DescripciónProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("Descripción Producto");
		    }
            set 
		    {
			    SetColumnValue("Descripción Producto", value);
            }
        }
	      
        [XmlAttribute("Localizacion")]
        [Bindable(true)]
        public string Localizacion 
	    {
		    get
		    {
			    return GetColumnValue<string>("Localizacion");
		    }
            set 
		    {
			    SetColumnValue("Localizacion", value);
            }
        }
	      
        [XmlAttribute("CodigoBarras")]
        [Bindable(true)]
        public string CodigoBarras 
	    {
		    get
		    {
			    return GetColumnValue<string>("CodigoBarras");
		    }
            set 
		    {
			    SetColumnValue("CodigoBarras", value);
            }
        }
	      
        [XmlAttribute("IdTipoEmpaque")]
        [Bindable(true)]
        public int IdTipoEmpaque 
	    {
		    get
		    {
			    return GetColumnValue<int>("IdTipoEmpaque");
		    }
            set 
		    {
			    SetColumnValue("IdTipoEmpaque", value);
            }
        }
	      
        [XmlAttribute("UnidadesPorEmpaque")]
        [Bindable(true)]
        public string UnidadesPorEmpaque 
	    {
		    get
		    {
			    return GetColumnValue<string>("Unidades Por Empaque");
		    }
            set 
		    {
			    SetColumnValue("Unidades Por Empaque", value);
            }
        }
	      
        [XmlAttribute("CostoBase")]
        [Bindable(true)]
        public decimal CostoBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Costo Base");
		    }
            set 
		    {
			    SetColumnValue("Costo Base", value);
            }
        }
	      
        [XmlAttribute("PrecioDeVentaBase")]
        [Bindable(true)]
        public decimal? PrecioDeVentaBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("Precio de Venta Base");
		    }
            set 
		    {
			    SetColumnValue("Precio de Venta Base", value);
            }
        }
	      
        [XmlAttribute("CostoDeEnvíoBase")]
        [Bindable(true)]
        public decimal? CostoDeEnvíoBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("Costo de Envío Base");
		    }
            set 
		    {
			    SetColumnValue("Costo de Envío Base", value);
            }
        }
	      
        [XmlAttribute("Largo")]
        [Bindable(true)]
        public decimal Largo 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("LARGO");
		    }
            set 
		    {
			    SetColumnValue("LARGO", value);
            }
        }
	      
        [XmlAttribute("Ancho")]
        [Bindable(true)]
        public decimal Ancho 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("ANCHO");
		    }
            set 
		    {
			    SetColumnValue("ANCHO", value);
            }
        }
	      
        [XmlAttribute("Alto")]
        [Bindable(true)]
        public decimal Alto 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("ALTO");
		    }
            set 
		    {
			    SetColumnValue("ALTO", value);
            }
        }
	      
        [XmlAttribute("Peso")]
        [Bindable(true)]
        public decimal Peso 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("PESO");
		    }
            set 
		    {
			    SetColumnValue("PESO", value);
            }
        }
	      
        [XmlAttribute("IdProveedor")]
        [Bindable(true)]
        public int IdProveedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("IdProveedor");
		    }
            set 
		    {
			    SetColumnValue("IdProveedor", value);
            }
        }
	      
        [XmlAttribute("IdTipoProducto")]
        [Bindable(true)]
        public int IdTipoProducto 
	    {
		    get
		    {
			    return GetColumnValue<int>("IdTipoProducto");
		    }
            set 
		    {
			    SetColumnValue("IdTipoProducto", value);
            }
        }
	      
        [XmlAttribute("Iva")]
        [Bindable(true)]
        public decimal Iva 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("IVA");
		    }
            set 
		    {
			    SetColumnValue("IVA", value);
            }
        }
	      
        [XmlAttribute("RetenciónEnLaFuente")]
        [Bindable(true)]
        public decimal RetenciónEnLaFuente 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Retención en la Fuente");
		    }
            set 
		    {
			    SetColumnValue("Retención en la Fuente", value);
            }
        }
	      
        [XmlAttribute("Descuento")]
        [Bindable(true)]
        public decimal? Descuento 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("DESCUENTO");
		    }
            set 
		    {
			    SetColumnValue("DESCUENTO", value);
            }
        }
	      
        [XmlAttribute("NombreDelPrograma")]
        [Bindable(true)]
        public string NombreDelPrograma 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nombre del Programa");
		    }
            set 
		    {
			    SetColumnValue("Nombre del Programa", value);
            }
        }
	      
        [XmlAttribute("TipoDelivery")]
        [Bindable(true)]
        public string TipoDelivery 
	    {
		    get
		    {
			    return GetColumnValue<string>("Tipo Delivery");
		    }
            set 
		    {
			    SetColumnValue("Tipo Delivery", value);
            }
        }
	      
        [XmlAttribute("Proveedor")]
        [Bindable(true)]
        public string Proveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("Proveedor");
		    }
            set 
		    {
			    SetColumnValue("Proveedor", value);
            }
        }
	      
        [XmlAttribute("IdMarca")]
        [Bindable(true)]
        public int? IdMarca 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ID_MARCA");
		    }
            set 
		    {
			    SetColumnValue("ID_MARCA", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Guid = @"GUID";
            
            public static string Codigo = @"CODIGO";
            
            public static string NombreProducto = @"Nombre Producto";
            
            public static string DescripciónProducto = @"Descripción Producto";
            
            public static string Localizacion = @"Localizacion";
            
            public static string CodigoBarras = @"CodigoBarras";
            
            public static string IdTipoEmpaque = @"IdTipoEmpaque";
            
            public static string UnidadesPorEmpaque = @"Unidades Por Empaque";
            
            public static string CostoBase = @"Costo Base";
            
            public static string PrecioDeVentaBase = @"Precio de Venta Base";
            
            public static string CostoDeEnvíoBase = @"Costo de Envío Base";
            
            public static string Largo = @"LARGO";
            
            public static string Ancho = @"ANCHO";
            
            public static string Alto = @"ALTO";
            
            public static string Peso = @"PESO";
            
            public static string IdProveedor = @"IdProveedor";
            
            public static string IdTipoProducto = @"IdTipoProducto";
            
            public static string Iva = @"IVA";
            
            public static string RetenciónEnLaFuente = @"Retención en la Fuente";
            
            public static string Descuento = @"DESCUENTO";
            
            public static string NombreDelPrograma = @"Nombre del Programa";
            
            public static string TipoDelivery = @"Tipo Delivery";
            
            public static string Proveedor = @"Proveedor";
            
            public static string IdMarca = @"ID_MARCA";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
