using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvCacheDetalleAjuste class.
	/// </summary>
    [Serializable]
	public partial class InvCacheDetalleAjusteCollection : ActiveList<InvCacheDetalleAjuste, InvCacheDetalleAjusteCollection>
	{	   
		public InvCacheDetalleAjusteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCacheDetalleAjusteCollection</returns>
		public InvCacheDetalleAjusteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCacheDetalleAjuste o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CACHE_DETALLE_AJUSTE table.
	/// </summary>
	[Serializable]
	public partial class InvCacheDetalleAjuste : ActiveRecord<InvCacheDetalleAjuste>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCacheDetalleAjuste()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCacheDetalleAjuste(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCacheDetalleAjuste(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCacheDetalleAjuste(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CACHE_DETALLE_AJUSTE", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdCacheDetalleAjuste = new TableSchema.TableColumn(schema);
				colvarIdCacheDetalleAjuste.ColumnName = "ID_CACHE_DETALLE_AJUSTE";
				colvarIdCacheDetalleAjuste.DataType = DbType.Int32;
				colvarIdCacheDetalleAjuste.MaxLength = 0;
				colvarIdCacheDetalleAjuste.AutoIncrement = true;
				colvarIdCacheDetalleAjuste.IsNullable = false;
				colvarIdCacheDetalleAjuste.IsPrimaryKey = true;
				colvarIdCacheDetalleAjuste.IsForeignKey = false;
				colvarIdCacheDetalleAjuste.IsReadOnly = false;
				colvarIdCacheDetalleAjuste.DefaultSetting = @"";
				colvarIdCacheDetalleAjuste.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdCacheDetalleAjuste);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarCostoUnitario = new TableSchema.TableColumn(schema);
				colvarCostoUnitario.ColumnName = "COSTO_UNITARIO";
				colvarCostoUnitario.DataType = DbType.Decimal;
				colvarCostoUnitario.MaxLength = 0;
				colvarCostoUnitario.AutoIncrement = false;
				colvarCostoUnitario.IsNullable = false;
				colvarCostoUnitario.IsPrimaryKey = false;
				colvarCostoUnitario.IsForeignKey = false;
				colvarCostoUnitario.IsReadOnly = false;
				colvarCostoUnitario.DefaultSetting = @"";
				colvarCostoUnitario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoUnitario);
				
				TableSchema.TableColumn colvarPrecioUnitario = new TableSchema.TableColumn(schema);
				colvarPrecioUnitario.ColumnName = "PRECIO_UNITARIO";
				colvarPrecioUnitario.DataType = DbType.Decimal;
				colvarPrecioUnitario.MaxLength = 0;
				colvarPrecioUnitario.AutoIncrement = false;
				colvarPrecioUnitario.IsNullable = false;
				colvarPrecioUnitario.IsPrimaryKey = false;
				colvarPrecioUnitario.IsForeignKey = false;
				colvarPrecioUnitario.IsReadOnly = false;
				colvarPrecioUnitario.DefaultSetting = @"";
				colvarPrecioUnitario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrecioUnitario);
				
				TableSchema.TableColumn colvarNumeroItem = new TableSchema.TableColumn(schema);
				colvarNumeroItem.ColumnName = "NUMERO_ITEM";
				colvarNumeroItem.DataType = DbType.Int32;
				colvarNumeroItem.MaxLength = 0;
				colvarNumeroItem.AutoIncrement = false;
				colvarNumeroItem.IsNullable = false;
				colvarNumeroItem.IsPrimaryKey = false;
				colvarNumeroItem.IsForeignKey = false;
				colvarNumeroItem.IsReadOnly = false;
				colvarNumeroItem.DefaultSetting = @"";
				colvarNumeroItem.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroItem);
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = false;
				colvarGuidReferenciaProducto.IsForeignKey = false;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				colvarGuidReferenciaProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				TableSchema.TableColumn colvarIdUsuario = new TableSchema.TableColumn(schema);
				colvarIdUsuario.ColumnName = "ID_USUARIO";
				colvarIdUsuario.DataType = DbType.Int32;
				colvarIdUsuario.MaxLength = 0;
				colvarIdUsuario.AutoIncrement = false;
				colvarIdUsuario.IsNullable = false;
				colvarIdUsuario.IsPrimaryKey = false;
				colvarIdUsuario.IsForeignKey = false;
				colvarIdUsuario.IsReadOnly = false;
				colvarIdUsuario.DefaultSetting = @"";
				colvarIdUsuario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdUsuario);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CACHE_DETALLE_AJUSTE",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdCacheDetalleAjuste")]
		[Bindable(true)]
		public int IdCacheDetalleAjuste 
		{
			get { return GetColumnValue<int>(Columns.IdCacheDetalleAjuste); }
			set { SetColumnValue(Columns.IdCacheDetalleAjuste, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("CostoUnitario")]
		[Bindable(true)]
		public decimal CostoUnitario 
		{
			get { return GetColumnValue<decimal>(Columns.CostoUnitario); }
			set { SetColumnValue(Columns.CostoUnitario, value); }
		}
		  
		[XmlAttribute("PrecioUnitario")]
		[Bindable(true)]
		public decimal PrecioUnitario 
		{
			get { return GetColumnValue<decimal>(Columns.PrecioUnitario); }
			set { SetColumnValue(Columns.PrecioUnitario, value); }
		}
		  
		[XmlAttribute("NumeroItem")]
		[Bindable(true)]
		public int NumeroItem 
		{
			get { return GetColumnValue<int>(Columns.NumeroItem); }
			set { SetColumnValue(Columns.NumeroItem, value); }
		}
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		  
		[XmlAttribute("IdUsuario")]
		[Bindable(true)]
		public int IdUsuario 
		{
			get { return GetColumnValue<int>(Columns.IdUsuario); }
			set { SetColumnValue(Columns.IdUsuario, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varCantidad,decimal varCostoUnitario,decimal varPrecioUnitario,int varNumeroItem,string varGuidReferenciaProducto,int varIdUsuario)
		{
			InvCacheDetalleAjuste item = new InvCacheDetalleAjuste();
			
			item.Cantidad = varCantidad;
			
			item.CostoUnitario = varCostoUnitario;
			
			item.PrecioUnitario = varPrecioUnitario;
			
			item.NumeroItem = varNumeroItem;
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.IdUsuario = varIdUsuario;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdCacheDetalleAjuste,int varCantidad,decimal varCostoUnitario,decimal varPrecioUnitario,int varNumeroItem,string varGuidReferenciaProducto,int varIdUsuario)
		{
			InvCacheDetalleAjuste item = new InvCacheDetalleAjuste();
			
				item.IdCacheDetalleAjuste = varIdCacheDetalleAjuste;
			
				item.Cantidad = varCantidad;
			
				item.CostoUnitario = varCostoUnitario;
			
				item.PrecioUnitario = varPrecioUnitario;
			
				item.NumeroItem = varNumeroItem;
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
				item.IdUsuario = varIdUsuario;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdCacheDetalleAjusteColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoUnitarioColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn PrecioUnitarioColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroItemColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdUsuarioColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdCacheDetalleAjuste = @"ID_CACHE_DETALLE_AJUSTE";
			 public static string Cantidad = @"CANTIDAD";
			 public static string CostoUnitario = @"COSTO_UNITARIO";
			 public static string PrecioUnitario = @"PRECIO_UNITARIO";
			 public static string NumeroItem = @"NUMERO_ITEM";
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
			 public static string IdUsuario = @"ID_USUARIO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
