using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvDetallesFacturaProveedorPuntoController
    {
        // Preload our schema..
        InvDetallesFacturaProveedorPunto thisSchemaLoad = new InvDetallesFacturaProveedorPunto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvDetallesFacturaProveedorPuntoCollection FetchAll()
        {
            InvDetallesFacturaProveedorPuntoCollection coll = new InvDetallesFacturaProveedorPuntoCollection();
            Query qry = new Query(InvDetallesFacturaProveedorPunto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetallesFacturaProveedorPuntoCollection FetchByID(object GuidDetalleFacturaProveedorPuntos)
        {
            InvDetallesFacturaProveedorPuntoCollection coll = new InvDetallesFacturaProveedorPuntoCollection().Where("GUID_DETALLE_FACTURA_PROVEEDOR_PUNTOS", GuidDetalleFacturaProveedorPuntos).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetallesFacturaProveedorPuntoCollection FetchByQuery(Query qry)
        {
            InvDetallesFacturaProveedorPuntoCollection coll = new InvDetallesFacturaProveedorPuntoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidDetalleFacturaProveedorPuntos)
        {
            return (InvDetallesFacturaProveedorPunto.Delete(GuidDetalleFacturaProveedorPuntos) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidDetalleFacturaProveedorPuntos)
        {
            return (InvDetallesFacturaProveedorPunto.Destroy(GuidDetalleFacturaProveedorPuntos) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidDetalleFacturaProveedorPuntos,int Cantidad,decimal ValorBasePuntosTotal,decimal IvaTotal,string GuidFacturaProveedorPuntos,string GuidProducto)
	    {
		    InvDetallesFacturaProveedorPunto item = new InvDetallesFacturaProveedorPunto();
		    
            item.GuidDetalleFacturaProveedorPuntos = GuidDetalleFacturaProveedorPuntos;
            
            item.Cantidad = Cantidad;
            
            item.ValorBasePuntosTotal = ValorBasePuntosTotal;
            
            item.IvaTotal = IvaTotal;
            
            item.GuidFacturaProveedorPuntos = GuidFacturaProveedorPuntos;
            
            item.GuidProducto = GuidProducto;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidDetalleFacturaProveedorPuntos,int Cantidad,decimal ValorBasePuntosTotal,decimal IvaTotal,string GuidFacturaProveedorPuntos,string GuidProducto)
	    {
		    InvDetallesFacturaProveedorPunto item = new InvDetallesFacturaProveedorPunto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidDetalleFacturaProveedorPuntos = GuidDetalleFacturaProveedorPuntos;
				
			item.Cantidad = Cantidad;
				
			item.ValorBasePuntosTotal = ValorBasePuntosTotal;
				
			item.IvaTotal = IvaTotal;
				
			item.GuidFacturaProveedorPuntos = GuidFacturaProveedorPuntos;
				
			item.GuidProducto = GuidProducto;
				
	        item.Save(UserName);
	    }
    }
}
