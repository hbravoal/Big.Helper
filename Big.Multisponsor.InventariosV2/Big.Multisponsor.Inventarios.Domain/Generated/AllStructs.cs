using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	#region Tables Struct
	public partial struct Tables
	{
		
		public static readonly string CosteoPrefactura = @"CosteoPrefactura";
        
		public static readonly string InvAjuste = @"INV_AJUSTES";
        
		public static readonly string InvAvmCliente = @"INV_AVM_CLIENTES";
        
		public static readonly string InvAvmControlArchivo = @"INV_AVM_CONTROL_ARCHIVOS";
        
		public static readonly string InvBodega = @"INV_BODEGAS";
        
		public static readonly string InvBodegasProducto = @"INV_BODEGAS_PRODUCTOS";
        
		public static readonly string InvCacheDetalleAjuste = @"INV_CACHE_DETALLE_AJUSTE";
        
		public static readonly string InvCacheDetallesFactura = @"INV_CACHE_DETALLES_FACTURA";
        
		public static readonly string InvCgunoCausacione = @"INV_CGUNO_CAUSACIONES";
        
		public static readonly string InvCgunoCausacionesPeriodo = @"INV_CGUNO_CAUSACIONES_PERIODOS";
        
		public static readonly string InvCgunoCentroUtilidad = @"INV_CGUNO_CENTRO_UTILIDAD";
        
		public static readonly string InvCgunoCuenta = @"INV_CGUNO_CUENTAS";
        
		public static readonly string InvCgunoDetallesCausacion = @"INV_CGUNO_DETALLES_CAUSACION";
        
		public static readonly string InvCgunoDetallesCausacionesPeriodo = @"INV_CGUNO_DETALLES_CAUSACIONES_PERIODO";
        
		public static readonly string InvCgunoLog = @"INV_CGUNO_LOGS";
        
		public static readonly string InvCgunoMasCamposTipoCausacion = @"INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION";
        
		public static readonly string InvCgunoMasEstadosPeriodo = @"INV_CGUNO_MAS_ESTADOS_PERIODO";
        
		public static readonly string InvCgunoMasNaturalezasDetalleCausacion = @"INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION";
        
		public static readonly string InvCgunoMasTiposCausacion = @"INV_CGUNO_MAS_TIPOS_CAUSACION";
        
		public static readonly string InvCgunoPeriodo = @"INV_CGUNO_PERIODOS";
        
		public static readonly string InvCierreMensual = @"INV_CIERRE_MENSUAL";
        
		public static readonly string InvCliente = @"INV_CLIENTES";
        
		public static readonly string InvCostoEnvioAlterno = @"INV_COSTO_ENVIO_ALTERNO";
        
		public static readonly string InvCostoEnvioPrefacturaCurrier = @"INV_COSTO_ENVIO_PREFACTURA_CURRIER";
        
		public static readonly string InvDetalleAjuste = @"INV_DETALLE_AJUSTE";
        
		public static readonly string InvDetallesFactura = @"INV_DETALLES_FACTURA";
        
		public static readonly string InvDetallesFacturaBanco = @"INV_DETALLES_FACTURA_BANCO";
        
		public static readonly string InvDetallesFacturaProveedorPunto = @"INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS";
        
		public static readonly string InvEmpresa = @"INV_EMPRESAS";
        
		public static readonly string InvFactura = @"INV_FACTURAS";
        
		public static readonly string InvFacturasBanco = @"INV_FACTURAS_BANCO";
        
		public static readonly string InvFacturasProveedorPunto = @"INV_FACTURAS_PROVEEDOR_PUNTOS";
        
		public static readonly string InvHistoricoBodega = @"INV_HISTORICO_BODEGA";
        
		public static readonly string InvHistoricoSiniestro = @"INV_HISTORICO_SINIESTROS";
        
		public static readonly string InvHistoricosPrecio = @"INV_HISTORICOS_PRECIO";
        
		public static readonly string InvMarca = @"INV_MARCAS";
        
		public static readonly string InvMasCiudade = @"INV_MAS_CIUDADES";
        
		public static readonly string InvMasDepartamento = @"INV_MAS_DEPARTAMENTOS";
        
		public static readonly string InvMasEntidadesBancaria = @"INV_MAS_ENTIDADES_BANCARIAS";
        
		public static readonly string InvMasEstado = @"INV_MAS_ESTADOS";
        
		public static readonly string InvMasIvaRetefuente = @"INV_MAS_IVA_RETEFUENTE";
        
		public static readonly string InvMasPaise = @"INV_MAS_PAISES";
        
		public static readonly string InvMasPermiso = @"INV_MAS_PERMISOS";
        
		public static readonly string InvMasTiposBodega = @"INV_MAS_TIPOS_BODEGA";
        
		public static readonly string InvMasTiposCuentum = @"INV_MAS_TIPOS_CUENTA";
        
		public static readonly string InvMasTiposDelivery = @"INV_MAS_TIPOS_DELIVERY";
        
		public static readonly string InvMasTiposEmpaque = @"INV_MAS_TIPOS_EMPAQUE";
        
		public static readonly string InvMasTiposEstado = @"INV_MAS_TIPOS_ESTADO";
        
		public static readonly string InvMasTiposFacturacion = @"INV_MAS_TIPOS_FACTURACION";
        
		public static readonly string InvMasTiposProducto = @"INV_MAS_TIPOS_PRODUCTO";
        
		public static readonly string InvMasTiposResolucion = @"INV_MAS_TIPOS_RESOLUCION";
        
		public static readonly string InvPerfile = @"INV_PERFILES";
        
		public static readonly string InvPerfilesPermiso = @"INV_PERFILES_PERMISOS";
        
		public static readonly string InvPeriodoFacturacionPunto = @"INV_PERIODO_FACTURACION_PUNTOS";
        
		public static readonly string InvPreciosBasePrograma = @"INV_PRECIOS_BASE_PROGRAMA";
        
		public static readonly string InvPrefacturaPeriodo = @"INV_PREFACTURA_PERIODO";
        
		public static readonly string InvPrefactura = @"INV_PREFACTURAS";
        
		public static readonly string InvProducto = @"INV_PRODUCTOS";
        
		public static readonly string InvPrograma = @"INV_PROGRAMAS";
        
		public static readonly string InvProveedore = @"INV_PROVEEDORES";
        
		public static readonly string InvReferenciasProducto = @"INV_REFERENCIAS_PRODUCTO";
        
		public static readonly string InvReplicacionModulo = @"INV_REPLICACION_MODULOS";
        
		public static readonly string InvReplicacionPeticione = @"INV_REPLICACION_PETICIONES";
        
		public static readonly string InvResolucione = @"INV_RESOLUCIONES";
        
		public static readonly string InvSiniestro = @"INV_SINIESTROS";
        
		public static readonly string InvTmpCoordinadora = @"INV_TMP_COORDINADORA";
        
		public static readonly string InvTmpCostoEnvioAlterno = @"INV_TMP_COSTO_ENVIO_ALTERNO";
        
		public static readonly string InvUsuario = @"INV_USUARIOS";
        
		public static readonly string InvVigenciaResolucione = @"INV_VIGENCIA_RESOLUCIONES";
        
	}
	#endregion
    #region Schemas
    public partial class Schemas {
		
		public static TableSchema.Table CosteoPrefactura
		{
            get { return DataService.GetSchema("CosteoPrefactura", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvAjuste
		{
            get { return DataService.GetSchema("INV_AJUSTES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvAvmCliente
		{
            get { return DataService.GetSchema("INV_AVM_CLIENTES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvAvmControlArchivo
		{
            get { return DataService.GetSchema("INV_AVM_CONTROL_ARCHIVOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvBodega
		{
            get { return DataService.GetSchema("INV_BODEGAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvBodegasProducto
		{
            get { return DataService.GetSchema("INV_BODEGAS_PRODUCTOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCacheDetalleAjuste
		{
            get { return DataService.GetSchema("INV_CACHE_DETALLE_AJUSTE", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCacheDetallesFactura
		{
            get { return DataService.GetSchema("INV_CACHE_DETALLES_FACTURA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoCausacione
		{
            get { return DataService.GetSchema("INV_CGUNO_CAUSACIONES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoCausacionesPeriodo
		{
            get { return DataService.GetSchema("INV_CGUNO_CAUSACIONES_PERIODOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoCentroUtilidad
		{
            get { return DataService.GetSchema("INV_CGUNO_CENTRO_UTILIDAD", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoCuenta
		{
            get { return DataService.GetSchema("INV_CGUNO_CUENTAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoDetallesCausacion
		{
            get { return DataService.GetSchema("INV_CGUNO_DETALLES_CAUSACION", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoDetallesCausacionesPeriodo
		{
            get { return DataService.GetSchema("INV_CGUNO_DETALLES_CAUSACIONES_PERIODO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoLog
		{
            get { return DataService.GetSchema("INV_CGUNO_LOGS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoMasCamposTipoCausacion
		{
            get { return DataService.GetSchema("INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoMasEstadosPeriodo
		{
            get { return DataService.GetSchema("INV_CGUNO_MAS_ESTADOS_PERIODO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoMasNaturalezasDetalleCausacion
		{
            get { return DataService.GetSchema("INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoMasTiposCausacion
		{
            get { return DataService.GetSchema("INV_CGUNO_MAS_TIPOS_CAUSACION", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCgunoPeriodo
		{
            get { return DataService.GetSchema("INV_CGUNO_PERIODOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCierreMensual
		{
            get { return DataService.GetSchema("INV_CIERRE_MENSUAL", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCliente
		{
            get { return DataService.GetSchema("INV_CLIENTES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCostoEnvioAlterno
		{
            get { return DataService.GetSchema("INV_COSTO_ENVIO_ALTERNO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvCostoEnvioPrefacturaCurrier
		{
            get { return DataService.GetSchema("INV_COSTO_ENVIO_PREFACTURA_CURRIER", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvDetalleAjuste
		{
            get { return DataService.GetSchema("INV_DETALLE_AJUSTE", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvDetallesFactura
		{
            get { return DataService.GetSchema("INV_DETALLES_FACTURA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvDetallesFacturaBanco
		{
            get { return DataService.GetSchema("INV_DETALLES_FACTURA_BANCO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvDetallesFacturaProveedorPunto
		{
            get { return DataService.GetSchema("INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvEmpresa
		{
            get { return DataService.GetSchema("INV_EMPRESAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvFactura
		{
            get { return DataService.GetSchema("INV_FACTURAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvFacturasBanco
		{
            get { return DataService.GetSchema("INV_FACTURAS_BANCO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvFacturasProveedorPunto
		{
            get { return DataService.GetSchema("INV_FACTURAS_PROVEEDOR_PUNTOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvHistoricoBodega
		{
            get { return DataService.GetSchema("INV_HISTORICO_BODEGA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvHistoricoSiniestro
		{
            get { return DataService.GetSchema("INV_HISTORICO_SINIESTROS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvHistoricosPrecio
		{
            get { return DataService.GetSchema("INV_HISTORICOS_PRECIO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMarca
		{
            get { return DataService.GetSchema("INV_MARCAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasCiudade
		{
            get { return DataService.GetSchema("INV_MAS_CIUDADES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasDepartamento
		{
            get { return DataService.GetSchema("INV_MAS_DEPARTAMENTOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasEntidadesBancaria
		{
            get { return DataService.GetSchema("INV_MAS_ENTIDADES_BANCARIAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasEstado
		{
            get { return DataService.GetSchema("INV_MAS_ESTADOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasIvaRetefuente
		{
            get { return DataService.GetSchema("INV_MAS_IVA_RETEFUENTE", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasPaise
		{
            get { return DataService.GetSchema("INV_MAS_PAISES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasPermiso
		{
            get { return DataService.GetSchema("INV_MAS_PERMISOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposBodega
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_BODEGA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposCuentum
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_CUENTA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposDelivery
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_DELIVERY", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposEmpaque
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_EMPAQUE", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposEstado
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_ESTADO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposFacturacion
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_FACTURACION", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposProducto
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_PRODUCTO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvMasTiposResolucion
		{
            get { return DataService.GetSchema("INV_MAS_TIPOS_RESOLUCION", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPerfile
		{
            get { return DataService.GetSchema("INV_PERFILES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPerfilesPermiso
		{
            get { return DataService.GetSchema("INV_PERFILES_PERMISOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPeriodoFacturacionPunto
		{
            get { return DataService.GetSchema("INV_PERIODO_FACTURACION_PUNTOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPreciosBasePrograma
		{
            get { return DataService.GetSchema("INV_PRECIOS_BASE_PROGRAMA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPrefacturaPeriodo
		{
            get { return DataService.GetSchema("INV_PREFACTURA_PERIODO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPrefactura
		{
            get { return DataService.GetSchema("INV_PREFACTURAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvProducto
		{
            get { return DataService.GetSchema("INV_PRODUCTOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvPrograma
		{
            get { return DataService.GetSchema("INV_PROGRAMAS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvProveedore
		{
            get { return DataService.GetSchema("INV_PROVEEDORES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvReferenciasProducto
		{
            get { return DataService.GetSchema("INV_REFERENCIAS_PRODUCTO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvReplicacionModulo
		{
            get { return DataService.GetSchema("INV_REPLICACION_MODULOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvReplicacionPeticione
		{
            get { return DataService.GetSchema("INV_REPLICACION_PETICIONES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvResolucione
		{
            get { return DataService.GetSchema("INV_RESOLUCIONES", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvSiniestro
		{
            get { return DataService.GetSchema("INV_SINIESTROS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvTmpCoordinadora
		{
            get { return DataService.GetSchema("INV_TMP_COORDINADORA", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvTmpCostoEnvioAlterno
		{
            get { return DataService.GetSchema("INV_TMP_COSTO_ENVIO_ALTERNO", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvUsuario
		{
            get { return DataService.GetSchema("INV_USUARIOS", "dbInventariosV2"); }
		}
        
		public static TableSchema.Table InvVigenciaResolucione
		{
            get { return DataService.GetSchema("INV_VIGENCIA_RESOLUCIONES", "dbInventariosV2"); }
		}
        
	
    }
    #endregion
    #region View Struct
    public partial struct Views 
    {
		
		public static readonly string InvVwCiudade = @"INV_VW_CIUDADES";
        
		public static readonly string InvVwCiudadesMilla = @"INV_VW_CIUDADES_MILLAS";
        
		public static readonly string InvVwClientesMilla = @"INV_VW_CLIENTES_MILLAS";
        
		public static readonly string InvVwConsultaSaldo = @"INV_VW_CONSULTA_SALDOS";
        
		public static readonly string InvVwDetallesFacturaMilla = @"INV_VW_DETALLES_FACTURA_MILLAS";
        
		public static readonly string InvVwExportarProveedore = @"INV_VW_EXPORTAR_PROVEEDORES";
        
		public static readonly string InvVwFACTURAPROVPUNTOSProductoProveedorMilla = @"INV_VW_FACTURA(PROV_PUNTOS)_PRODUCTO_PROVEEDOR_MILLAS";
        
		public static readonly string InvVwFacturaProductoProveedorMilla = @"INV_VW_FACTURA_PRODUCTO_PROVEEDOR_MILLAS";
        
		public static readonly string InvVwFacturasMilla = @"INV_VW_FACTURAS_MILLAS";
        
		public static readonly string InvVwMicrocanje = @"INV_VW_MICROCANJES";
        
		public static readonly string InvVwPreciosExportar = @"INV_VW_PRECIOS_EXPORTAR";
        
		public static readonly string InvVwProducto = @"INV_VW_PRODUCTOS";
        
		public static readonly string InvVwProductosExportar = @"INV_VW_PRODUCTOS_EXPORTAR";
        
		public static readonly string InvVwReferenciasExportar = @"INV_VW_REFERENCIAS_EXPORTAR";
        
		public static readonly string InvVwReportesFacturacion = @"INV_VW_REPORTES_FACTURACION";
        
		public static readonly string InvVwReportesFacturacionV2 = @"INV_VW_REPORTES_FACTURACION_V2";
        
		public static readonly string ViewTransProveedorCostoVentum = @"View_Trans_Proveedor_Costo_Venta";
        
    }
    #endregion
    
    #region Query Factories
	public static partial class DB
	{
        public static DataProvider _provider = DataService.Providers["dbInventariosV2"];
        static ISubSonicRepository _repository;
        public static ISubSonicRepository Repository 
        {
            get 
            {
                if (_repository == null)
                    return new SubSonicRepository(_provider);
                return _repository; 
            }
            set { _repository = value; }
        }
        public static Select SelectAllColumnsFrom<T>() where T : RecordBase<T>, new()
	    {
            return Repository.SelectAllColumnsFrom<T>();
	    }
	    public static Select Select()
	    {
            return Repository.Select();
	    }
	    
		public static Select Select(params string[] columns)
		{
            return Repository.Select(columns);
        }
	    
		public static Select Select(params Aggregate[] aggregates)
		{
            return Repository.Select(aggregates);
        }
   
	    public static Update Update<T>() where T : RecordBase<T>, new()
	    {
            return Repository.Update<T>();
	    }
	    
	    public static Insert Insert()
	    {
            return Repository.Insert();
	    }
	    
	    public static Delete Delete()
	    {
            return Repository.Delete();
	    }
	    
	    public static InlineQuery Query()
	    {
            return Repository.Query();
	    }
	    	    
	    
	}
    #endregion
    
}
#region Databases
public partial struct Databases 
{
	
	public static readonly string dbInventariosV2 = @"dbInventariosV2";
    
}
#endregion