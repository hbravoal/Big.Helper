using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MAS_ESTADOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasEstadoController
    {
        // Preload our schema..
        InvMasEstado thisSchemaLoad = new InvMasEstado();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasEstadoCollection FetchAll()
        {
            InvMasEstadoCollection coll = new InvMasEstadoCollection();
            Query qry = new Query(InvMasEstado.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasEstadoCollection FetchByID(object IdEstado)
        {
            InvMasEstadoCollection coll = new InvMasEstadoCollection().Where("ID_ESTADO", IdEstado).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasEstadoCollection FetchByQuery(Query qry)
        {
            InvMasEstadoCollection coll = new InvMasEstadoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdEstado)
        {
            return (InvMasEstado.Delete(IdEstado) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdEstado)
        {
            return (InvMasEstado.Destroy(IdEstado) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdEstado,string Descripcion,int IdTipoEstado)
	    {
		    InvMasEstado item = new InvMasEstado();
		    
            item.IdEstado = IdEstado;
            
            item.Descripcion = Descripcion;
            
            item.IdTipoEstado = IdTipoEstado;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdEstado,string Descripcion,int IdTipoEstado)
	    {
		    InvMasEstado item = new InvMasEstado();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdEstado = IdEstado;
				
			item.Descripcion = Descripcion;
				
			item.IdTipoEstado = IdTipoEstado;
				
	        item.Save(UserName);
	    }
    }
}
