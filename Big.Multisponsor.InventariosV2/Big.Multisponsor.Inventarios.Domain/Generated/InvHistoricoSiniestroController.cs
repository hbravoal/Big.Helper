using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_HISTORICO_SINIESTROS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvHistoricoSiniestroController
    {
        // Preload our schema..
        InvHistoricoSiniestro thisSchemaLoad = new InvHistoricoSiniestro();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvHistoricoSiniestroCollection FetchAll()
        {
            InvHistoricoSiniestroCollection coll = new InvHistoricoSiniestroCollection();
            Query qry = new Query(InvHistoricoSiniestro.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvHistoricoSiniestroCollection FetchByID(object GuidHistoricoSiniestros)
        {
            InvHistoricoSiniestroCollection coll = new InvHistoricoSiniestroCollection().Where("GUID_HISTORICO_SINIESTROS", GuidHistoricoSiniestros).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvHistoricoSiniestroCollection FetchByQuery(Query qry)
        {
            InvHistoricoSiniestroCollection coll = new InvHistoricoSiniestroCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidHistoricoSiniestros)
        {
            return (InvHistoricoSiniestro.Delete(GuidHistoricoSiniestros) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidHistoricoSiniestros)
        {
            return (InvHistoricoSiniestro.Destroy(GuidHistoricoSiniestros) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidHistoricoSiniestros,string Justificacion,DateTime Fecha,int Cantidad,string GuidSiniestro)
	    {
		    InvHistoricoSiniestro item = new InvHistoricoSiniestro();
		    
            item.GuidHistoricoSiniestros = GuidHistoricoSiniestros;
            
            item.Justificacion = Justificacion;
            
            item.Fecha = Fecha;
            
            item.Cantidad = Cantidad;
            
            item.GuidSiniestro = GuidSiniestro;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidHistoricoSiniestros,string Justificacion,DateTime Fecha,int Cantidad,string GuidSiniestro)
	    {
		    InvHistoricoSiniestro item = new InvHistoricoSiniestro();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidHistoricoSiniestros = GuidHistoricoSiniestros;
				
			item.Justificacion = Justificacion;
				
			item.Fecha = Fecha;
				
			item.Cantidad = Cantidad;
				
			item.GuidSiniestro = GuidSiniestro;
				
	        item.Save(UserName);
	    }
    }
}
