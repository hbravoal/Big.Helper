using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoCausacione class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoCausacioneCollection : ActiveList<InvCgunoCausacione, InvCgunoCausacioneCollection>
	{	   
		public InvCgunoCausacioneCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoCausacioneCollection</returns>
		public InvCgunoCausacioneCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoCausacione o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_CAUSACIONES table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoCausacione : ActiveRecord<InvCgunoCausacione>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoCausacione()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoCausacione(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoCausacione(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoCausacione(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_CAUSACIONES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.AnsiString;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Decimal;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
				colvarCodigo.ColumnName = "CODIGO";
				colvarCodigo.DataType = DbType.AnsiString;
				colvarCodigo.MaxLength = 10;
				colvarCodigo.AutoIncrement = false;
				colvarCodigo.IsNullable = false;
				colvarCodigo.IsPrimaryKey = false;
				colvarCodigo.IsForeignKey = false;
				colvarCodigo.IsReadOnly = false;
				colvarCodigo.DefaultSetting = @"";
				colvarCodigo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigo);
				
				TableSchema.TableColumn colvarIdTipoCausacion = new TableSchema.TableColumn(schema);
				colvarIdTipoCausacion.ColumnName = "ID_TIPO_CAUSACION";
				colvarIdTipoCausacion.DataType = DbType.Int32;
				colvarIdTipoCausacion.MaxLength = 0;
				colvarIdTipoCausacion.AutoIncrement = false;
				colvarIdTipoCausacion.IsNullable = false;
				colvarIdTipoCausacion.IsPrimaryKey = false;
				colvarIdTipoCausacion.IsForeignKey = true;
				colvarIdTipoCausacion.IsReadOnly = false;
				colvarIdTipoCausacion.DefaultSetting = @"";
				
					colvarIdTipoCausacion.ForeignKeyTableName = "INV_CGUNO_MAS_TIPOS_CAUSACION";
				schema.Columns.Add(colvarIdTipoCausacion);
				
				TableSchema.TableColumn colvarFechaCreacion = new TableSchema.TableColumn(schema);
				colvarFechaCreacion.ColumnName = "FECHA_CREACION";
				colvarFechaCreacion.DataType = DbType.DateTime;
				colvarFechaCreacion.MaxLength = 0;
				colvarFechaCreacion.AutoIncrement = false;
				colvarFechaCreacion.IsNullable = false;
				colvarFechaCreacion.IsPrimaryKey = false;
				colvarFechaCreacion.IsForeignKey = false;
				colvarFechaCreacion.IsReadOnly = false;
				colvarFechaCreacion.DefaultSetting = @"";
				colvarFechaCreacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaCreacion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_CAUSACIONES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public decimal Id 
		{
			get { return GetColumnValue<decimal>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Codigo")]
		[Bindable(true)]
		public string Codigo 
		{
			get { return GetColumnValue<string>(Columns.Codigo); }
			set { SetColumnValue(Columns.Codigo, value); }
		}
		  
		[XmlAttribute("IdTipoCausacion")]
		[Bindable(true)]
		public int IdTipoCausacion 
		{
			get { return GetColumnValue<int>(Columns.IdTipoCausacion); }
			set { SetColumnValue(Columns.IdTipoCausacion, value); }
		}
		  
		[XmlAttribute("FechaCreacion")]
		[Bindable(true)]
		public DateTime FechaCreacion 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaCreacion); }
			set { SetColumnValue(Columns.FechaCreacion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoDetallesCausacionCollection InvCgunoDetallesCausacionRecords()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoDetallesCausacionCollection().Where(InvCgunoDetallesCausacion.Columns.IdCausacion, Id).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoPeriodoCollection InvCgunoPeriodos()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoPeriodoCollection().Where(InvCgunoPeriodo.Columns.IdCausacion, Id).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoMasTiposCausacion ActiveRecord object related to this InvCgunoCausacione
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoMasTiposCausacion InvCgunoMasTiposCausacion
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoMasTiposCausacion.FetchByID(this.IdTipoCausacion); }
			set { SetColumnValue("ID_TIPO_CAUSACION", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,string varCodigo,int varIdTipoCausacion,DateTime varFechaCreacion)
		{
			InvCgunoCausacione item = new InvCgunoCausacione();
			
			item.Guid = varGuid;
			
			item.Codigo = varCodigo;
			
			item.IdTipoCausacion = varIdTipoCausacion;
			
			item.FechaCreacion = varFechaCreacion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,decimal varId,string varCodigo,int varIdTipoCausacion,DateTime varFechaCreacion)
		{
			InvCgunoCausacione item = new InvCgunoCausacione();
			
				item.Guid = varGuid;
			
				item.Id = varId;
			
				item.Codigo = varCodigo;
			
				item.IdTipoCausacion = varIdTipoCausacion;
			
				item.FechaCreacion = varFechaCreacion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoCausacionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaCreacionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string Id = @"ID";
			 public static string Codigo = @"CODIGO";
			 public static string IdTipoCausacion = @"ID_TIPO_CAUSACION";
			 public static string FechaCreacion = @"FECHA_CREACION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
