using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_FACTURAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvFacturaController
    {
        // Preload our schema..
        InvFactura thisSchemaLoad = new InvFactura();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvFacturaCollection FetchAll()
        {
            InvFacturaCollection coll = new InvFacturaCollection();
            Query qry = new Query(InvFactura.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvFacturaCollection FetchByID(object GuidFactura)
        {
            InvFacturaCollection coll = new InvFacturaCollection().Where("GUID_FACTURA", GuidFactura).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvFacturaCollection FetchByQuery(Query qry)
        {
            InvFacturaCollection coll = new InvFacturaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidFactura)
        {
            return (InvFactura.Delete(GuidFactura) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidFactura)
        {
            return (InvFactura.Destroy(GuidFactura) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidFactura,int? NumeroFactura,decimal TotalBase,decimal Iva,bool Prefacturada,bool Anulada,bool GeneradaEmpresa,DateTime? FechaFactura,DateTime? FechaAnulacion,DateTime? FechaPrefacturada,DateTime? FechaGeneracionEmpresa,string ValorEnLetras,string DireccionCliente,string TelefonoCliente,string FaxCliente,int IdCiudadCliente,int IdPrograma,string GuidCliente,bool NotaCredito,bool Siniestro,DateTime? FechaCompra,int? FacturaNotaCredito,string MotivoNotaCredito,DateTime? FechaEntrega,bool? GeneradaEstablecimiento,DateTime? FechaGeneracionEstablecimiento,DateTime? FechaTransaccion,DateTime FechaVencimiento,string Observaciones,bool BitNotaCreditoValor)
	    {
		    InvFactura item = new InvFactura();
		    
            item.GuidFactura = GuidFactura;
            
            item.NumeroFactura = NumeroFactura;
            
            item.TotalBase = TotalBase;
            
            item.Iva = Iva;
            
            item.Prefacturada = Prefacturada;
            
            item.Anulada = Anulada;
            
            item.GeneradaEmpresa = GeneradaEmpresa;
            
            item.FechaFactura = FechaFactura;
            
            item.FechaAnulacion = FechaAnulacion;
            
            item.FechaPrefacturada = FechaPrefacturada;
            
            item.FechaGeneracionEmpresa = FechaGeneracionEmpresa;
            
            item.ValorEnLetras = ValorEnLetras;
            
            item.DireccionCliente = DireccionCliente;
            
            item.TelefonoCliente = TelefonoCliente;
            
            item.FaxCliente = FaxCliente;
            
            item.IdCiudadCliente = IdCiudadCliente;
            
            item.IdPrograma = IdPrograma;
            
            item.GuidCliente = GuidCliente;
            
            item.NotaCredito = NotaCredito;
            
            item.Siniestro = Siniestro;
            
            item.FechaCompra = FechaCompra;
            
            item.FacturaNotaCredito = FacturaNotaCredito;
            
            item.MotivoNotaCredito = MotivoNotaCredito;
            
            item.FechaEntrega = FechaEntrega;
            
            item.GeneradaEstablecimiento = GeneradaEstablecimiento;
            
            item.FechaGeneracionEstablecimiento = FechaGeneracionEstablecimiento;
            
            item.FechaTransaccion = FechaTransaccion;
            
            item.FechaVencimiento = FechaVencimiento;
            
            item.Observaciones = Observaciones;
            
            item.BitNotaCreditoValor = BitNotaCreditoValor;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidFactura,int? NumeroFactura,decimal TotalBase,decimal Iva,bool Prefacturada,bool Anulada,bool GeneradaEmpresa,DateTime? FechaFactura,DateTime? FechaAnulacion,DateTime? FechaPrefacturada,DateTime? FechaGeneracionEmpresa,string ValorEnLetras,string DireccionCliente,string TelefonoCliente,string FaxCliente,int IdCiudadCliente,int IdPrograma,string GuidCliente,bool NotaCredito,bool Siniestro,DateTime? FechaCompra,int? FacturaNotaCredito,string MotivoNotaCredito,DateTime? FechaEntrega,bool? GeneradaEstablecimiento,DateTime? FechaGeneracionEstablecimiento,DateTime? FechaTransaccion,DateTime FechaVencimiento,string Observaciones,bool BitNotaCreditoValor)
	    {
		    InvFactura item = new InvFactura();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidFactura = GuidFactura;
				
			item.NumeroFactura = NumeroFactura;
				
			item.TotalBase = TotalBase;
				
			item.Iva = Iva;
				
			item.Prefacturada = Prefacturada;
				
			item.Anulada = Anulada;
				
			item.GeneradaEmpresa = GeneradaEmpresa;
				
			item.FechaFactura = FechaFactura;
				
			item.FechaAnulacion = FechaAnulacion;
				
			item.FechaPrefacturada = FechaPrefacturada;
				
			item.FechaGeneracionEmpresa = FechaGeneracionEmpresa;
				
			item.ValorEnLetras = ValorEnLetras;
				
			item.DireccionCliente = DireccionCliente;
				
			item.TelefonoCliente = TelefonoCliente;
				
			item.FaxCliente = FaxCliente;
				
			item.IdCiudadCliente = IdCiudadCliente;
				
			item.IdPrograma = IdPrograma;
				
			item.GuidCliente = GuidCliente;
				
			item.NotaCredito = NotaCredito;
				
			item.Siniestro = Siniestro;
				
			item.FechaCompra = FechaCompra;
				
			item.FacturaNotaCredito = FacturaNotaCredito;
				
			item.MotivoNotaCredito = MotivoNotaCredito;
				
			item.FechaEntrega = FechaEntrega;
				
			item.GeneradaEstablecimiento = GeneradaEstablecimiento;
				
			item.FechaGeneracionEstablecimiento = FechaGeneracionEstablecimiento;
				
			item.FechaTransaccion = FechaTransaccion;
				
			item.FechaVencimiento = FechaVencimiento;
				
			item.Observaciones = Observaciones;
				
			item.BitNotaCreditoValor = BitNotaCreditoValor;
				
	        item.Save(UserName);
	    }
    }
}
