using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_BODEGAS_PRODUCTOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvBodegasProductoController
    {
        // Preload our schema..
        InvBodegasProducto thisSchemaLoad = new InvBodegasProducto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvBodegasProductoCollection FetchAll()
        {
            InvBodegasProductoCollection coll = new InvBodegasProductoCollection();
            Query qry = new Query(InvBodegasProducto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvBodegasProductoCollection FetchByID(object IdBodegaProducto)
        {
            InvBodegasProductoCollection coll = new InvBodegasProductoCollection().Where("ID_BODEGA_PRODUCTO", IdBodegaProducto).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvBodegasProductoCollection FetchByQuery(Query qry)
        {
            InvBodegasProductoCollection coll = new InvBodegasProductoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdBodegaProducto)
        {
            return (InvBodegasProducto.Delete(IdBodegaProducto) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdBodegaProducto)
        {
            return (InvBodegasProducto.Destroy(IdBodegaProducto) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdBodega,string GuidReferenciaProducto,decimal Cantidad)
	    {
		    InvBodegasProducto item = new InvBodegasProducto();
		    
            item.IdBodega = IdBodega;
            
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
            item.Cantidad = Cantidad;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(decimal IdBodegaProducto,int IdBodega,string GuidReferenciaProducto,decimal Cantidad)
	    {
		    InvBodegasProducto item = new InvBodegasProducto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdBodegaProducto = IdBodegaProducto;
				
			item.IdBodega = IdBodega;
				
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
			item.Cantidad = Cantidad;
				
	        item.Save(UserName);
	    }
    }
}
