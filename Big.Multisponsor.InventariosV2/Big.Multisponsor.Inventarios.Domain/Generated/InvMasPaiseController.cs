using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_MAS_PAISES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasPaiseController
    {
        // Preload our schema..
        InvMasPaise thisSchemaLoad = new InvMasPaise();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasPaiseCollection FetchAll()
        {
            InvMasPaiseCollection coll = new InvMasPaiseCollection();
            Query qry = new Query(InvMasPaise.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasPaiseCollection FetchByID(object IdPais)
        {
            InvMasPaiseCollection coll = new InvMasPaiseCollection().Where("ID_PAIS", IdPais).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasPaiseCollection FetchByQuery(Query qry)
        {
            InvMasPaiseCollection coll = new InvMasPaiseCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPais)
        {
            return (InvMasPaise.Delete(IdPais) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPais)
        {
            return (InvMasPaise.Destroy(IdPais) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Nombre)
	    {
		    InvMasPaise item = new InvMasPaise();
		    
            item.Nombre = Nombre;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPais,string Nombre)
	    {
		    InvMasPaise item = new InvMasPaise();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPais = IdPais;
				
			item.Nombre = Nombre;
				
	        item.Save(UserName);
	    }
    }
}
