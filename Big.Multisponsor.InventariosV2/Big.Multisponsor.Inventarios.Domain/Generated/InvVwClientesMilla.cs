using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain{
    /// <summary>
    /// Strongly-typed collection for the InvVwClientesMilla class.
    /// </summary>
    [Serializable]
    public partial class InvVwClientesMillaCollection : ReadOnlyList<InvVwClientesMilla, InvVwClientesMillaCollection>
    {        
        public InvVwClientesMillaCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_CLIENTES_MILLAS view.
    /// </summary>
    [Serializable]
    public partial class InvVwClientesMilla : ReadOnlyRecord<InvVwClientesMilla>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_CLIENTES_MILLAS", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarNombres = new TableSchema.TableColumn(schema);
                colvarNombres.ColumnName = "NOMBRES";
                colvarNombres.DataType = DbType.String;
                colvarNombres.MaxLength = 50;
                colvarNombres.AutoIncrement = false;
                colvarNombres.IsNullable = false;
                colvarNombres.IsPrimaryKey = false;
                colvarNombres.IsForeignKey = false;
                colvarNombres.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombres);
                
                TableSchema.TableColumn colvarApellidos = new TableSchema.TableColumn(schema);
                colvarApellidos.ColumnName = "APELLIDOS";
                colvarApellidos.DataType = DbType.String;
                colvarApellidos.MaxLength = 50;
                colvarApellidos.AutoIncrement = false;
                colvarApellidos.IsNullable = true;
                colvarApellidos.IsPrimaryKey = false;
                colvarApellidos.IsForeignKey = false;
                colvarApellidos.IsReadOnly = false;
                
                schema.Columns.Add(colvarApellidos);
                
                TableSchema.TableColumn colvarExpr2 = new TableSchema.TableColumn(schema);
                colvarExpr2.ColumnName = "Expr2";
                colvarExpr2.DataType = DbType.AnsiString;
                colvarExpr2.MaxLength = 15;
                colvarExpr2.AutoIncrement = false;
                colvarExpr2.IsNullable = false;
                colvarExpr2.IsPrimaryKey = false;
                colvarExpr2.IsForeignKey = false;
                colvarExpr2.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpr2);
                
                TableSchema.TableColumn colvarIdCiudadCliente = new TableSchema.TableColumn(schema);
                colvarIdCiudadCliente.ColumnName = "ID_CIUDAD_CLIENTE";
                colvarIdCiudadCliente.DataType = DbType.Int32;
                colvarIdCiudadCliente.MaxLength = 0;
                colvarIdCiudadCliente.AutoIncrement = false;
                colvarIdCiudadCliente.IsNullable = false;
                colvarIdCiudadCliente.IsPrimaryKey = false;
                colvarIdCiudadCliente.IsForeignKey = false;
                colvarIdCiudadCliente.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCiudadCliente);
                
                TableSchema.TableColumn colvarDireccionCliente = new TableSchema.TableColumn(schema);
                colvarDireccionCliente.ColumnName = "DIRECCION_CLIENTE";
                colvarDireccionCliente.DataType = DbType.String;
                colvarDireccionCliente.MaxLength = 200;
                colvarDireccionCliente.AutoIncrement = false;
                colvarDireccionCliente.IsNullable = false;
                colvarDireccionCliente.IsPrimaryKey = false;
                colvarDireccionCliente.IsForeignKey = false;
                colvarDireccionCliente.IsReadOnly = false;
                
                schema.Columns.Add(colvarDireccionCliente);
                
                TableSchema.TableColumn colvarTelefonoCliente = new TableSchema.TableColumn(schema);
                colvarTelefonoCliente.ColumnName = "TELEFONO_CLIENTE";
                colvarTelefonoCliente.DataType = DbType.AnsiString;
                colvarTelefonoCliente.MaxLength = 20;
                colvarTelefonoCliente.AutoIncrement = false;
                colvarTelefonoCliente.IsNullable = false;
                colvarTelefonoCliente.IsPrimaryKey = false;
                colvarTelefonoCliente.IsForeignKey = false;
                colvarTelefonoCliente.IsReadOnly = false;
                
                schema.Columns.Add(colvarTelefonoCliente);
                
                TableSchema.TableColumn colvarFaxCliente = new TableSchema.TableColumn(schema);
                colvarFaxCliente.ColumnName = "FAX_CLIENTE";
                colvarFaxCliente.DataType = DbType.AnsiString;
                colvarFaxCliente.MaxLength = 20;
                colvarFaxCliente.AutoIncrement = false;
                colvarFaxCliente.IsNullable = true;
                colvarFaxCliente.IsPrimaryKey = false;
                colvarFaxCliente.IsForeignKey = false;
                colvarFaxCliente.IsReadOnly = false;
                
                schema.Columns.Add(colvarFaxCliente);
                
                TableSchema.TableColumn colvarFechaFactura = new TableSchema.TableColumn(schema);
                colvarFechaFactura.ColumnName = "FECHA_FACTURA";
                colvarFechaFactura.DataType = DbType.DateTime;
                colvarFechaFactura.MaxLength = 0;
                colvarFechaFactura.AutoIncrement = false;
                colvarFechaFactura.IsNullable = true;
                colvarFechaFactura.IsPrimaryKey = false;
                colvarFechaFactura.IsForeignKey = false;
                colvarFechaFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaFactura);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_CLIENTES_MILLAS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwClientesMilla()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwClientesMilla(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwClientesMilla(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwClientesMilla(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Nombres")]
        [Bindable(true)]
        public string Nombres 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRES");
		    }
            set 
		    {
			    SetColumnValue("NOMBRES", value);
            }
        }
	      
        [XmlAttribute("Apellidos")]
        [Bindable(true)]
        public string Apellidos 
	    {
		    get
		    {
			    return GetColumnValue<string>("APELLIDOS");
		    }
            set 
		    {
			    SetColumnValue("APELLIDOS", value);
            }
        }
	      
        [XmlAttribute("Expr2")]
        [Bindable(true)]
        public string Expr2 
	    {
		    get
		    {
			    return GetColumnValue<string>("Expr2");
		    }
            set 
		    {
			    SetColumnValue("Expr2", value);
            }
        }
	      
        [XmlAttribute("IdCiudadCliente")]
        [Bindable(true)]
        public int IdCiudadCliente 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_CIUDAD_CLIENTE");
		    }
            set 
		    {
			    SetColumnValue("ID_CIUDAD_CLIENTE", value);
            }
        }
	      
        [XmlAttribute("DireccionCliente")]
        [Bindable(true)]
        public string DireccionCliente 
	    {
		    get
		    {
			    return GetColumnValue<string>("DIRECCION_CLIENTE");
		    }
            set 
		    {
			    SetColumnValue("DIRECCION_CLIENTE", value);
            }
        }
	      
        [XmlAttribute("TelefonoCliente")]
        [Bindable(true)]
        public string TelefonoCliente 
	    {
		    get
		    {
			    return GetColumnValue<string>("TELEFONO_CLIENTE");
		    }
            set 
		    {
			    SetColumnValue("TELEFONO_CLIENTE", value);
            }
        }
	      
        [XmlAttribute("FaxCliente")]
        [Bindable(true)]
        public string FaxCliente 
	    {
		    get
		    {
			    return GetColumnValue<string>("FAX_CLIENTE");
		    }
            set 
		    {
			    SetColumnValue("FAX_CLIENTE", value);
            }
        }
	      
        [XmlAttribute("FechaFactura")]
        [Bindable(true)]
        public DateTime? FechaFactura 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("FECHA_FACTURA", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Nombres = @"NOMBRES";
            
            public static string Apellidos = @"APELLIDOS";
            
            public static string Expr2 = @"Expr2";
            
            public static string IdCiudadCliente = @"ID_CIUDAD_CLIENTE";
            
            public static string DireccionCliente = @"DIRECCION_CLIENTE";
            
            public static string TelefonoCliente = @"TELEFONO_CLIENTE";
            
            public static string FaxCliente = @"FAX_CLIENTE";
            
            public static string FechaFactura = @"FECHA_FACTURA";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
