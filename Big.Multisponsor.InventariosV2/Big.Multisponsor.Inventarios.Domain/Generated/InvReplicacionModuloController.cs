using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_REPLICACION_MODULOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvReplicacionModuloController
    {
        // Preload our schema..
        InvReplicacionModulo thisSchemaLoad = new InvReplicacionModulo();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvReplicacionModuloCollection FetchAll()
        {
            InvReplicacionModuloCollection coll = new InvReplicacionModuloCollection();
            Query qry = new Query(InvReplicacionModulo.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvReplicacionModuloCollection FetchByID(object Id)
        {
            InvReplicacionModuloCollection coll = new InvReplicacionModuloCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvReplicacionModuloCollection FetchByQuery(Query qry)
        {
            InvReplicacionModuloCollection coll = new InvReplicacionModuloCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvReplicacionModulo.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvReplicacionModulo.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int Id,string Nombre,int IdPrograma,string Proveedores)
	    {
		    InvReplicacionModulo item = new InvReplicacionModulo();
		    
            item.Id = Id;
            
            item.Nombre = Nombre;
            
            item.IdPrograma = IdPrograma;
            
            item.Proveedores = Proveedores;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Nombre,int IdPrograma,string Proveedores)
	    {
		    InvReplicacionModulo item = new InvReplicacionModulo();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Nombre = Nombre;
				
			item.IdPrograma = IdPrograma;
				
			item.Proveedores = Proveedores;
				
	        item.Save(UserName);
	    }
    }
}
