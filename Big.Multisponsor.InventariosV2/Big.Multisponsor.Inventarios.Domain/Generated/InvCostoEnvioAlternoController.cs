using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_COSTO_ENVIO_ALTERNO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCostoEnvioAlternoController
    {
        // Preload our schema..
        InvCostoEnvioAlterno thisSchemaLoad = new InvCostoEnvioAlterno();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCostoEnvioAlternoCollection FetchAll()
        {
            InvCostoEnvioAlternoCollection coll = new InvCostoEnvioAlternoCollection();
            Query qry = new Query(InvCostoEnvioAlterno.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCostoEnvioAlternoCollection FetchByID(object Guid)
        {
            InvCostoEnvioAlternoCollection coll = new InvCostoEnvioAlternoCollection().Where("GUID", Guid).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCostoEnvioAlternoCollection FetchByQuery(Query qry)
        {
            InvCostoEnvioAlternoCollection coll = new InvCostoEnvioAlternoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Guid)
        {
            return (InvCostoEnvioAlterno.Delete(Guid) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Guid)
        {
            return (InvCostoEnvioAlterno.Destroy(Guid) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,int IdPrograma,int IdCiudad,string GuidProducto,int IdTipoDelivery,decimal ValorAlterno,bool Activo)
	    {
		    InvCostoEnvioAlterno item = new InvCostoEnvioAlterno();
		    
            item.Guid = Guid;
            
            item.IdPrograma = IdPrograma;
            
            item.IdCiudad = IdCiudad;
            
            item.GuidProducto = GuidProducto;
            
            item.IdTipoDelivery = IdTipoDelivery;
            
            item.ValorAlterno = ValorAlterno;
            
            item.Activo = Activo;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,int IdPrograma,int IdCiudad,string GuidProducto,int IdTipoDelivery,decimal ValorAlterno,bool Activo)
	    {
		    InvCostoEnvioAlterno item = new InvCostoEnvioAlterno();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.IdPrograma = IdPrograma;
				
			item.IdCiudad = IdCiudad;
				
			item.GuidProducto = GuidProducto;
				
			item.IdTipoDelivery = IdTipoDelivery;
				
			item.ValorAlterno = ValorAlterno;
				
			item.Activo = Activo;
				
	        item.Save(UserName);
	    }
    }
}
