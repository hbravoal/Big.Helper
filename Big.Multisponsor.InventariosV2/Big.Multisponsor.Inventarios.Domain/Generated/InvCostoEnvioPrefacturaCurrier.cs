using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvCostoEnvioPrefacturaCurrier class.
	/// </summary>
    [Serializable]
	public partial class InvCostoEnvioPrefacturaCurrierCollection : ActiveList<InvCostoEnvioPrefacturaCurrier, InvCostoEnvioPrefacturaCurrierCollection>
	{	   
		public InvCostoEnvioPrefacturaCurrierCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCostoEnvioPrefacturaCurrierCollection</returns>
		public InvCostoEnvioPrefacturaCurrierCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCostoEnvioPrefacturaCurrier o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_COSTO_ENVIO_PREFACTURA_CURRIER table.
	/// </summary>
	[Serializable]
	public partial class InvCostoEnvioPrefacturaCurrier : ActiveRecord<InvCostoEnvioPrefacturaCurrier>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCostoEnvioPrefacturaCurrier()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCostoEnvioPrefacturaCurrier(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCostoEnvioPrefacturaCurrier(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCostoEnvioPrefacturaCurrier(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_COSTO_ENVIO_PREFACTURA_CURRIER", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPeriodo = new TableSchema.TableColumn(schema);
				colvarIdPeriodo.ColumnName = "ID_PERIODO";
				colvarIdPeriodo.DataType = DbType.Int32;
				colvarIdPeriodo.MaxLength = 0;
				colvarIdPeriodo.AutoIncrement = false;
				colvarIdPeriodo.IsNullable = false;
				colvarIdPeriodo.IsPrimaryKey = true;
				colvarIdPeriodo.IsForeignKey = false;
				colvarIdPeriodo.IsReadOnly = false;
				colvarIdPeriodo.DefaultSetting = @"";
				colvarIdPeriodo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPeriodo);
				
				TableSchema.TableColumn colvarGuidDetalleFactura = new TableSchema.TableColumn(schema);
				colvarGuidDetalleFactura.ColumnName = "GUID_DETALLE_FACTURA";
				colvarGuidDetalleFactura.DataType = DbType.String;
				colvarGuidDetalleFactura.MaxLength = 36;
				colvarGuidDetalleFactura.AutoIncrement = false;
				colvarGuidDetalleFactura.IsNullable = false;
				colvarGuidDetalleFactura.IsPrimaryKey = true;
				colvarGuidDetalleFactura.IsForeignKey = false;
				colvarGuidDetalleFactura.IsReadOnly = false;
				colvarGuidDetalleFactura.DefaultSetting = @"";
				colvarGuidDetalleFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidDetalleFactura);
				
				TableSchema.TableColumn colvarCostoEnvio = new TableSchema.TableColumn(schema);
				colvarCostoEnvio.ColumnName = "COSTO_ENVIO";
				colvarCostoEnvio.DataType = DbType.Decimal;
				colvarCostoEnvio.MaxLength = 0;
				colvarCostoEnvio.AutoIncrement = false;
				colvarCostoEnvio.IsNullable = false;
				colvarCostoEnvio.IsPrimaryKey = false;
				colvarCostoEnvio.IsForeignKey = false;
				colvarCostoEnvio.IsReadOnly = false;
				colvarCostoEnvio.DefaultSetting = @"";
				colvarCostoEnvio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoEnvio);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_COSTO_ENVIO_PREFACTURA_CURRIER",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPeriodo")]
		[Bindable(true)]
		public int IdPeriodo 
		{
			get { return GetColumnValue<int>(Columns.IdPeriodo); }
			set { SetColumnValue(Columns.IdPeriodo, value); }
		}
		  
		[XmlAttribute("GuidDetalleFactura")]
		[Bindable(true)]
		public string GuidDetalleFactura 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleFactura); }
			set { SetColumnValue(Columns.GuidDetalleFactura, value); }
		}
		  
		[XmlAttribute("CostoEnvio")]
		[Bindable(true)]
		public decimal CostoEnvio 
		{
			get { return GetColumnValue<decimal>(Columns.CostoEnvio); }
			set { SetColumnValue(Columns.CostoEnvio, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdPeriodo,string varGuidDetalleFactura,decimal varCostoEnvio)
		{
			InvCostoEnvioPrefacturaCurrier item = new InvCostoEnvioPrefacturaCurrier();
			
			item.IdPeriodo = varIdPeriodo;
			
			item.GuidDetalleFactura = varGuidDetalleFactura;
			
			item.CostoEnvio = varCostoEnvio;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdPeriodo,string varGuidDetalleFactura,decimal varCostoEnvio)
		{
			InvCostoEnvioPrefacturaCurrier item = new InvCostoEnvioPrefacturaCurrier();
			
				item.IdPeriodo = varIdPeriodo;
			
				item.GuidDetalleFactura = varGuidDetalleFactura;
			
				item.CostoEnvio = varCostoEnvio;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdPeriodoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidDetalleFacturaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoEnvioColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPeriodo = @"ID_PERIODO";
			 public static string GuidDetalleFactura = @"GUID_DETALLE_FACTURA";
			 public static string CostoEnvio = @"COSTO_ENVIO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
