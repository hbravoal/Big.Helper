using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_AVM_CLIENTES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvAvmClienteController
    {
        // Preload our schema..
        InvAvmCliente thisSchemaLoad = new InvAvmCliente();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvAvmClienteCollection FetchAll()
        {
            InvAvmClienteCollection coll = new InvAvmClienteCollection();
            Query qry = new Query(InvAvmCliente.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvAvmClienteCollection FetchByID(object Cedula)
        {
            InvAvmClienteCollection coll = new InvAvmClienteCollection().Where("CEDULA", Cedula).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvAvmClienteCollection FetchByQuery(Query qry)
        {
            InvAvmClienteCollection coll = new InvAvmClienteCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Cedula)
        {
            return (InvAvmCliente.Delete(Cedula) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Cedula)
        {
            return (InvAvmCliente.Destroy(Cedula) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Cedula,decimal? CodigoAdvermind)
	    {
		    InvAvmCliente item = new InvAvmCliente();
		    
            item.Cedula = Cedula;
            
            item.CodigoAdvermind = CodigoAdvermind;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Cedula,decimal? CodigoAdvermind)
	    {
		    InvAvmCliente item = new InvAvmCliente();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Cedula = Cedula;
				
			item.CodigoAdvermind = CodigoAdvermind;
				
	        item.Save(UserName);
	    }
    }
}
