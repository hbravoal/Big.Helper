using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvHistoricoSiniestro class.
	/// </summary>
    [Serializable]
	public partial class InvHistoricoSiniestroCollection : ActiveList<InvHistoricoSiniestro, InvHistoricoSiniestroCollection>
	{	   
		public InvHistoricoSiniestroCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvHistoricoSiniestroCollection</returns>
		public InvHistoricoSiniestroCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvHistoricoSiniestro o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_HISTORICO_SINIESTROS table.
	/// </summary>
	[Serializable]
	public partial class InvHistoricoSiniestro : ActiveRecord<InvHistoricoSiniestro>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvHistoricoSiniestro()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvHistoricoSiniestro(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvHistoricoSiniestro(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvHistoricoSiniestro(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_HISTORICO_SINIESTROS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidHistoricoSiniestros = new TableSchema.TableColumn(schema);
				colvarGuidHistoricoSiniestros.ColumnName = "GUID_HISTORICO_SINIESTROS";
				colvarGuidHistoricoSiniestros.DataType = DbType.String;
				colvarGuidHistoricoSiniestros.MaxLength = 36;
				colvarGuidHistoricoSiniestros.AutoIncrement = false;
				colvarGuidHistoricoSiniestros.IsNullable = false;
				colvarGuidHistoricoSiniestros.IsPrimaryKey = true;
				colvarGuidHistoricoSiniestros.IsForeignKey = false;
				colvarGuidHistoricoSiniestros.IsReadOnly = false;
				colvarGuidHistoricoSiniestros.DefaultSetting = @"";
				colvarGuidHistoricoSiniestros.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidHistoricoSiniestros);
				
				TableSchema.TableColumn colvarJustificacion = new TableSchema.TableColumn(schema);
				colvarJustificacion.ColumnName = "JUSTIFICACION";
				colvarJustificacion.DataType = DbType.String;
				colvarJustificacion.MaxLength = 200;
				colvarJustificacion.AutoIncrement = false;
				colvarJustificacion.IsNullable = false;
				colvarJustificacion.IsPrimaryKey = false;
				colvarJustificacion.IsForeignKey = false;
				colvarJustificacion.IsReadOnly = false;
				colvarJustificacion.DefaultSetting = @"";
				colvarJustificacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarJustificacion);
				
				TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
				colvarFecha.ColumnName = "FECHA";
				colvarFecha.DataType = DbType.DateTime;
				colvarFecha.MaxLength = 0;
				colvarFecha.AutoIncrement = false;
				colvarFecha.IsNullable = false;
				colvarFecha.IsPrimaryKey = false;
				colvarFecha.IsForeignKey = false;
				colvarFecha.IsReadOnly = false;
				colvarFecha.DefaultSetting = @"";
				colvarFecha.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecha);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarGuidSiniestro = new TableSchema.TableColumn(schema);
				colvarGuidSiniestro.ColumnName = "GUID_SINIESTRO";
				colvarGuidSiniestro.DataType = DbType.String;
				colvarGuidSiniestro.MaxLength = 36;
				colvarGuidSiniestro.AutoIncrement = false;
				colvarGuidSiniestro.IsNullable = false;
				colvarGuidSiniestro.IsPrimaryKey = false;
				colvarGuidSiniestro.IsForeignKey = true;
				colvarGuidSiniestro.IsReadOnly = false;
				colvarGuidSiniestro.DefaultSetting = @"";
				
					colvarGuidSiniestro.ForeignKeyTableName = "INV_SINIESTROS";
				schema.Columns.Add(colvarGuidSiniestro);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_HISTORICO_SINIESTROS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidHistoricoSiniestros")]
		[Bindable(true)]
		public string GuidHistoricoSiniestros 
		{
			get { return GetColumnValue<string>(Columns.GuidHistoricoSiniestros); }
			set { SetColumnValue(Columns.GuidHistoricoSiniestros, value); }
		}
		  
		[XmlAttribute("Justificacion")]
		[Bindable(true)]
		public string Justificacion 
		{
			get { return GetColumnValue<string>(Columns.Justificacion); }
			set { SetColumnValue(Columns.Justificacion, value); }
		}
		  
		[XmlAttribute("Fecha")]
		[Bindable(true)]
		public DateTime Fecha 
		{
			get { return GetColumnValue<DateTime>(Columns.Fecha); }
			set { SetColumnValue(Columns.Fecha, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("GuidSiniestro")]
		[Bindable(true)]
		public string GuidSiniestro 
		{
			get { return GetColumnValue<string>(Columns.GuidSiniestro); }
			set { SetColumnValue(Columns.GuidSiniestro, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvSiniestro ActiveRecord object related to this InvHistoricoSiniestro
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvSiniestro InvSiniestro
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvSiniestro.FetchByID(this.GuidSiniestro); }
			set { SetColumnValue("GUID_SINIESTRO", value.Guid); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidHistoricoSiniestros,string varJustificacion,DateTime varFecha,int varCantidad,string varGuidSiniestro)
		{
			InvHistoricoSiniestro item = new InvHistoricoSiniestro();
			
			item.GuidHistoricoSiniestros = varGuidHistoricoSiniestros;
			
			item.Justificacion = varJustificacion;
			
			item.Fecha = varFecha;
			
			item.Cantidad = varCantidad;
			
			item.GuidSiniestro = varGuidSiniestro;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidHistoricoSiniestros,string varJustificacion,DateTime varFecha,int varCantidad,string varGuidSiniestro)
		{
			InvHistoricoSiniestro item = new InvHistoricoSiniestro();
			
				item.GuidHistoricoSiniestros = varGuidHistoricoSiniestros;
			
				item.Justificacion = varJustificacion;
			
				item.Fecha = varFecha;
			
				item.Cantidad = varCantidad;
			
				item.GuidSiniestro = varGuidSiniestro;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidHistoricoSiniestrosColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn JustificacionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidSiniestroColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidHistoricoSiniestros = @"GUID_HISTORICO_SINIESTROS";
			 public static string Justificacion = @"JUSTIFICACION";
			 public static string Fecha = @"FECHA";
			 public static string Cantidad = @"CANTIDAD";
			 public static string GuidSiniestro = @"GUID_SINIESTRO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
