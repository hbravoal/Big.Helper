using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvProveedore class.
	/// </summary>
    [Serializable]
	public partial class InvProveedoreCollection : ActiveList<InvProveedore, InvProveedoreCollection>
	{	   
		public InvProveedoreCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvProveedoreCollection</returns>
		public InvProveedoreCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvProveedore o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PROVEEDORES table.
	/// </summary>
	[Serializable]
	public partial class InvProveedore : ActiveRecord<InvProveedore>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvProveedore()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvProveedore(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvProveedore(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvProveedore(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PROVEEDORES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
				colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
				colvarIdProveedor.DataType = DbType.Int32;
				colvarIdProveedor.MaxLength = 0;
				colvarIdProveedor.AutoIncrement = true;
				colvarIdProveedor.IsNullable = false;
				colvarIdProveedor.IsPrimaryKey = true;
				colvarIdProveedor.IsForeignKey = false;
				colvarIdProveedor.IsReadOnly = false;
				colvarIdProveedor.DefaultSetting = @"";
				colvarIdProveedor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdProveedor);
				
				TableSchema.TableColumn colvarCodigoProveedor = new TableSchema.TableColumn(schema);
				colvarCodigoProveedor.ColumnName = "CODIGO_PROVEEDOR";
				colvarCodigoProveedor.DataType = DbType.AnsiString;
				colvarCodigoProveedor.MaxLength = 20;
				colvarCodigoProveedor.AutoIncrement = false;
				colvarCodigoProveedor.IsNullable = false;
				colvarCodigoProveedor.IsPrimaryKey = false;
				colvarCodigoProveedor.IsForeignKey = false;
				colvarCodigoProveedor.IsReadOnly = false;
				colvarCodigoProveedor.DefaultSetting = @"";
				colvarCodigoProveedor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigoProveedor);
				
				TableSchema.TableColumn colvarRazonSocial = new TableSchema.TableColumn(schema);
				colvarRazonSocial.ColumnName = "RAZON_SOCIAL";
				colvarRazonSocial.DataType = DbType.String;
				colvarRazonSocial.MaxLength = 200;
				colvarRazonSocial.AutoIncrement = false;
				colvarRazonSocial.IsNullable = false;
				colvarRazonSocial.IsPrimaryKey = false;
				colvarRazonSocial.IsForeignKey = false;
				colvarRazonSocial.IsReadOnly = false;
				colvarRazonSocial.DefaultSetting = @"";
				colvarRazonSocial.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRazonSocial);
				
				TableSchema.TableColumn colvarNit = new TableSchema.TableColumn(schema);
				colvarNit.ColumnName = "NIT";
				colvarNit.DataType = DbType.AnsiString;
				colvarNit.MaxLength = 15;
				colvarNit.AutoIncrement = false;
				colvarNit.IsNullable = false;
				colvarNit.IsPrimaryKey = false;
				colvarNit.IsForeignKey = false;
				colvarNit.IsReadOnly = false;
				colvarNit.DefaultSetting = @"";
				colvarNit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNit);
				
				TableSchema.TableColumn colvarDireccion = new TableSchema.TableColumn(schema);
				colvarDireccion.ColumnName = "DIRECCION";
				colvarDireccion.DataType = DbType.String;
				colvarDireccion.MaxLength = 200;
				colvarDireccion.AutoIncrement = false;
				colvarDireccion.IsNullable = false;
				colvarDireccion.IsPrimaryKey = false;
				colvarDireccion.IsForeignKey = false;
				colvarDireccion.IsReadOnly = false;
				colvarDireccion.DefaultSetting = @"";
				colvarDireccion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDireccion);
				
				TableSchema.TableColumn colvarTelefono = new TableSchema.TableColumn(schema);
				colvarTelefono.ColumnName = "TELEFONO";
				colvarTelefono.DataType = DbType.AnsiString;
				colvarTelefono.MaxLength = 20;
				colvarTelefono.AutoIncrement = false;
				colvarTelefono.IsNullable = false;
				colvarTelefono.IsPrimaryKey = false;
				colvarTelefono.IsForeignKey = false;
				colvarTelefono.IsReadOnly = false;
				colvarTelefono.DefaultSetting = @"";
				colvarTelefono.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTelefono);
				
				TableSchema.TableColumn colvarFax = new TableSchema.TableColumn(schema);
				colvarFax.ColumnName = "FAX";
				colvarFax.DataType = DbType.AnsiString;
				colvarFax.MaxLength = 20;
				colvarFax.AutoIncrement = false;
				colvarFax.IsNullable = true;
				colvarFax.IsPrimaryKey = false;
				colvarFax.IsForeignKey = false;
				colvarFax.IsReadOnly = false;
				colvarFax.DefaultSetting = @"";
				colvarFax.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFax);
				
				TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
				colvarIdCiudad.ColumnName = "ID_CIUDAD";
				colvarIdCiudad.DataType = DbType.Int32;
				colvarIdCiudad.MaxLength = 0;
				colvarIdCiudad.AutoIncrement = false;
				colvarIdCiudad.IsNullable = false;
				colvarIdCiudad.IsPrimaryKey = false;
				colvarIdCiudad.IsForeignKey = true;
				colvarIdCiudad.IsReadOnly = false;
				colvarIdCiudad.DefaultSetting = @"";
				
					colvarIdCiudad.ForeignKeyTableName = "INV_MAS_CIUDADES";
				schema.Columns.Add(colvarIdCiudad);
				
				TableSchema.TableColumn colvarIdEntidadBancaria = new TableSchema.TableColumn(schema);
				colvarIdEntidadBancaria.ColumnName = "ID_ENTIDAD_BANCARIA";
				colvarIdEntidadBancaria.DataType = DbType.Int32;
				colvarIdEntidadBancaria.MaxLength = 0;
				colvarIdEntidadBancaria.AutoIncrement = false;
				colvarIdEntidadBancaria.IsNullable = false;
				colvarIdEntidadBancaria.IsPrimaryKey = false;
				colvarIdEntidadBancaria.IsForeignKey = true;
				colvarIdEntidadBancaria.IsReadOnly = false;
				colvarIdEntidadBancaria.DefaultSetting = @"";
				
					colvarIdEntidadBancaria.ForeignKeyTableName = "INV_MAS_ENTIDADES_BANCARIAS";
				schema.Columns.Add(colvarIdEntidadBancaria);
				
				TableSchema.TableColumn colvarNumeroCuenta = new TableSchema.TableColumn(schema);
				colvarNumeroCuenta.ColumnName = "NUMERO_CUENTA";
				colvarNumeroCuenta.DataType = DbType.AnsiString;
				colvarNumeroCuenta.MaxLength = 20;
				colvarNumeroCuenta.AutoIncrement = false;
				colvarNumeroCuenta.IsNullable = false;
				colvarNumeroCuenta.IsPrimaryKey = false;
				colvarNumeroCuenta.IsForeignKey = false;
				colvarNumeroCuenta.IsReadOnly = false;
				colvarNumeroCuenta.DefaultSetting = @"";
				colvarNumeroCuenta.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroCuenta);
				
				TableSchema.TableColumn colvarIdTipoCuenta = new TableSchema.TableColumn(schema);
				colvarIdTipoCuenta.ColumnName = "ID_TIPO_CUENTA";
				colvarIdTipoCuenta.DataType = DbType.Int32;
				colvarIdTipoCuenta.MaxLength = 0;
				colvarIdTipoCuenta.AutoIncrement = false;
				colvarIdTipoCuenta.IsNullable = false;
				colvarIdTipoCuenta.IsPrimaryKey = false;
				colvarIdTipoCuenta.IsForeignKey = true;
				colvarIdTipoCuenta.IsReadOnly = false;
				colvarIdTipoCuenta.DefaultSetting = @"";
				
					colvarIdTipoCuenta.ForeignKeyTableName = "INV_MAS_TIPOS_CUENTA";
				schema.Columns.Add(colvarIdTipoCuenta);
				
				TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
				colvarIdEstado.ColumnName = "ID_ESTADO";
				colvarIdEstado.DataType = DbType.Int32;
				colvarIdEstado.MaxLength = 0;
				colvarIdEstado.AutoIncrement = false;
				colvarIdEstado.IsNullable = false;
				colvarIdEstado.IsPrimaryKey = false;
				colvarIdEstado.IsForeignKey = true;
				colvarIdEstado.IsReadOnly = false;
				
						colvarIdEstado.DefaultSetting = @"((4))";
				
					colvarIdEstado.ForeignKeyTableName = "INV_MAS_ESTADOS";
				schema.Columns.Add(colvarIdEstado);
				
				TableSchema.TableColumn colvarGranContribuyente = new TableSchema.TableColumn(schema);
				colvarGranContribuyente.ColumnName = "GRAN_CONTRIBUYENTE";
				colvarGranContribuyente.DataType = DbType.Boolean;
				colvarGranContribuyente.MaxLength = 0;
				colvarGranContribuyente.AutoIncrement = false;
				colvarGranContribuyente.IsNullable = true;
				colvarGranContribuyente.IsPrimaryKey = false;
				colvarGranContribuyente.IsForeignKey = false;
				colvarGranContribuyente.IsReadOnly = false;
				colvarGranContribuyente.DefaultSetting = @"";
				colvarGranContribuyente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGranContribuyente);
				
				TableSchema.TableColumn colvarRegimenComun = new TableSchema.TableColumn(schema);
				colvarRegimenComun.ColumnName = "REGIMEN_COMUN";
				colvarRegimenComun.DataType = DbType.Boolean;
				colvarRegimenComun.MaxLength = 0;
				colvarRegimenComun.AutoIncrement = false;
				colvarRegimenComun.IsNullable = true;
				colvarRegimenComun.IsPrimaryKey = false;
				colvarRegimenComun.IsForeignKey = false;
				colvarRegimenComun.IsReadOnly = false;
				colvarRegimenComun.DefaultSetting = @"";
				colvarRegimenComun.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRegimenComun);
				
				TableSchema.TableColumn colvarAutoRetenedor = new TableSchema.TableColumn(schema);
				colvarAutoRetenedor.ColumnName = "AUTO_RETENEDOR";
				colvarAutoRetenedor.DataType = DbType.Boolean;
				colvarAutoRetenedor.MaxLength = 0;
				colvarAutoRetenedor.AutoIncrement = false;
				colvarAutoRetenedor.IsNullable = true;
				colvarAutoRetenedor.IsPrimaryKey = false;
				colvarAutoRetenedor.IsForeignKey = false;
				colvarAutoRetenedor.IsReadOnly = false;
				colvarAutoRetenedor.DefaultSetting = @"";
				colvarAutoRetenedor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAutoRetenedor);
				
				TableSchema.TableColumn colvarRetenedorIva = new TableSchema.TableColumn(schema);
				colvarRetenedorIva.ColumnName = "RETENEDOR_IVA";
				colvarRetenedorIva.DataType = DbType.Boolean;
				colvarRetenedorIva.MaxLength = 0;
				colvarRetenedorIva.AutoIncrement = false;
				colvarRetenedorIva.IsNullable = true;
				colvarRetenedorIva.IsPrimaryKey = false;
				colvarRetenedorIva.IsForeignKey = false;
				colvarRetenedorIva.IsReadOnly = false;
				colvarRetenedorIva.DefaultSetting = @"";
				colvarRetenedorIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRetenedorIva);
				
				TableSchema.TableColumn colvarPlazoPago = new TableSchema.TableColumn(schema);
				colvarPlazoPago.ColumnName = "PLAZO_PAGO";
				colvarPlazoPago.DataType = DbType.Int32;
				colvarPlazoPago.MaxLength = 0;
				colvarPlazoPago.AutoIncrement = false;
				colvarPlazoPago.IsNullable = true;
				colvarPlazoPago.IsPrimaryKey = false;
				colvarPlazoPago.IsForeignKey = false;
				colvarPlazoPago.IsReadOnly = false;
				colvarPlazoPago.DefaultSetting = @"";
				colvarPlazoPago.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPlazoPago);
				
				TableSchema.TableColumn colvarCupo = new TableSchema.TableColumn(schema);
				colvarCupo.ColumnName = "CUPO";
				colvarCupo.DataType = DbType.Decimal;
				colvarCupo.MaxLength = 0;
				colvarCupo.AutoIncrement = false;
				colvarCupo.IsNullable = true;
				colvarCupo.IsPrimaryKey = false;
				colvarCupo.IsForeignKey = false;
				colvarCupo.IsReadOnly = false;
				colvarCupo.DefaultSetting = @"";
				colvarCupo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCupo);
				
				TableSchema.TableColumn colvarIdCuentaInventarios = new TableSchema.TableColumn(schema);
				colvarIdCuentaInventarios.ColumnName = "ID_CUENTA_INVENTARIOS";
				colvarIdCuentaInventarios.DataType = DbType.Int32;
				colvarIdCuentaInventarios.MaxLength = 0;
				colvarIdCuentaInventarios.AutoIncrement = false;
				colvarIdCuentaInventarios.IsNullable = true;
				colvarIdCuentaInventarios.IsPrimaryKey = false;
				colvarIdCuentaInventarios.IsForeignKey = true;
				colvarIdCuentaInventarios.IsReadOnly = false;
				colvarIdCuentaInventarios.DefaultSetting = @"";
				
					colvarIdCuentaInventarios.ForeignKeyTableName = "INV_CGUNO_CUENTAS";
				schema.Columns.Add(colvarIdCuentaInventarios);
				
				TableSchema.TableColumn colvarIdCuentaCostosVenta = new TableSchema.TableColumn(schema);
				colvarIdCuentaCostosVenta.ColumnName = "ID_CUENTA_COSTOS_VENTA";
				colvarIdCuentaCostosVenta.DataType = DbType.Int32;
				colvarIdCuentaCostosVenta.MaxLength = 0;
				colvarIdCuentaCostosVenta.AutoIncrement = false;
				colvarIdCuentaCostosVenta.IsNullable = true;
				colvarIdCuentaCostosVenta.IsPrimaryKey = false;
				colvarIdCuentaCostosVenta.IsForeignKey = true;
				colvarIdCuentaCostosVenta.IsReadOnly = false;
				colvarIdCuentaCostosVenta.DefaultSetting = @"";
				
					colvarIdCuentaCostosVenta.ForeignKeyTableName = "INV_CGUNO_CUENTAS";
				schema.Columns.Add(colvarIdCuentaCostosVenta);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_PROVEEDORES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdProveedor")]
		[Bindable(true)]
		public int IdProveedor 
		{
			get { return GetColumnValue<int>(Columns.IdProveedor); }
			set { SetColumnValue(Columns.IdProveedor, value); }
		}
		  
		[XmlAttribute("CodigoProveedor")]
		[Bindable(true)]
		public string CodigoProveedor 
		{
			get { return GetColumnValue<string>(Columns.CodigoProveedor); }
			set { SetColumnValue(Columns.CodigoProveedor, value); }
		}
		  
		[XmlAttribute("RazonSocial")]
		[Bindable(true)]
		public string RazonSocial 
		{
			get { return GetColumnValue<string>(Columns.RazonSocial); }
			set { SetColumnValue(Columns.RazonSocial, value); }
		}
		  
		[XmlAttribute("Nit")]
		[Bindable(true)]
		public string Nit 
		{
			get { return GetColumnValue<string>(Columns.Nit); }
			set { SetColumnValue(Columns.Nit, value); }
		}
		  
		[XmlAttribute("Direccion")]
		[Bindable(true)]
		public string Direccion 
		{
			get { return GetColumnValue<string>(Columns.Direccion); }
			set { SetColumnValue(Columns.Direccion, value); }
		}
		  
		[XmlAttribute("Telefono")]
		[Bindable(true)]
		public string Telefono 
		{
			get { return GetColumnValue<string>(Columns.Telefono); }
			set { SetColumnValue(Columns.Telefono, value); }
		}
		  
		[XmlAttribute("Fax")]
		[Bindable(true)]
		public string Fax 
		{
			get { return GetColumnValue<string>(Columns.Fax); }
			set { SetColumnValue(Columns.Fax, value); }
		}
		  
		[XmlAttribute("IdCiudad")]
		[Bindable(true)]
		public int IdCiudad 
		{
			get { return GetColumnValue<int>(Columns.IdCiudad); }
			set { SetColumnValue(Columns.IdCiudad, value); }
		}
		  
		[XmlAttribute("IdEntidadBancaria")]
		[Bindable(true)]
		public int IdEntidadBancaria 
		{
			get { return GetColumnValue<int>(Columns.IdEntidadBancaria); }
			set { SetColumnValue(Columns.IdEntidadBancaria, value); }
		}
		  
		[XmlAttribute("NumeroCuenta")]
		[Bindable(true)]
		public string NumeroCuenta 
		{
			get { return GetColumnValue<string>(Columns.NumeroCuenta); }
			set { SetColumnValue(Columns.NumeroCuenta, value); }
		}
		  
		[XmlAttribute("IdTipoCuenta")]
		[Bindable(true)]
		public int IdTipoCuenta 
		{
			get { return GetColumnValue<int>(Columns.IdTipoCuenta); }
			set { SetColumnValue(Columns.IdTipoCuenta, value); }
		}
		  
		[XmlAttribute("IdEstado")]
		[Bindable(true)]
		public int IdEstado 
		{
			get { return GetColumnValue<int>(Columns.IdEstado); }
			set { SetColumnValue(Columns.IdEstado, value); }
		}
		  
		[XmlAttribute("GranContribuyente")]
		[Bindable(true)]
		public bool? GranContribuyente 
		{
			get { return GetColumnValue<bool?>(Columns.GranContribuyente); }
			set { SetColumnValue(Columns.GranContribuyente, value); }
		}
		  
		[XmlAttribute("RegimenComun")]
		[Bindable(true)]
		public bool? RegimenComun 
		{
			get { return GetColumnValue<bool?>(Columns.RegimenComun); }
			set { SetColumnValue(Columns.RegimenComun, value); }
		}
		  
		[XmlAttribute("AutoRetenedor")]
		[Bindable(true)]
		public bool? AutoRetenedor 
		{
			get { return GetColumnValue<bool?>(Columns.AutoRetenedor); }
			set { SetColumnValue(Columns.AutoRetenedor, value); }
		}
		  
		[XmlAttribute("RetenedorIva")]
		[Bindable(true)]
		public bool? RetenedorIva 
		{
			get { return GetColumnValue<bool?>(Columns.RetenedorIva); }
			set { SetColumnValue(Columns.RetenedorIva, value); }
		}
		  
		[XmlAttribute("PlazoPago")]
		[Bindable(true)]
		public int? PlazoPago 
		{
			get { return GetColumnValue<int?>(Columns.PlazoPago); }
			set { SetColumnValue(Columns.PlazoPago, value); }
		}
		  
		[XmlAttribute("Cupo")]
		[Bindable(true)]
		public decimal? Cupo 
		{
			get { return GetColumnValue<decimal?>(Columns.Cupo); }
			set { SetColumnValue(Columns.Cupo, value); }
		}
		  
		[XmlAttribute("IdCuentaInventarios")]
		[Bindable(true)]
		public int? IdCuentaInventarios 
		{
			get { return GetColumnValue<int?>(Columns.IdCuentaInventarios); }
			set { SetColumnValue(Columns.IdCuentaInventarios, value); }
		}
		  
		[XmlAttribute("IdCuentaCostosVenta")]
		[Bindable(true)]
		public int? IdCuentaCostosVenta 
		{
			get { return GetColumnValue<int?>(Columns.IdCuentaCostosVenta); }
			set { SetColumnValue(Columns.IdCuentaCostosVenta, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasBancoCollection InvFacturasBancoRecords()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasBancoCollection().Where(InvFacturasBanco.Columns.IdProveedor, IdProveedor).Load();
		}
		public Big.Multisponsor.Inventarios.Generated.Domain.InvProductoCollection InvProductos()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvProductoCollection().Where(InvProducto.Columns.IdProveedor, IdProveedor).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoCuenta ActiveRecord object related to this InvProveedore
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoCuenta InvCgunoCuenta
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoCuenta.FetchByID(this.IdCuentaCostosVenta); }
			set { SetColumnValue("ID_CUENTA_COSTOS_VENTA", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoCuenta ActiveRecord object related to this InvProveedore
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoCuenta InvCgunoCuentaToIdCuentaInventarios
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvCgunoCuenta.FetchByID(this.IdCuentaInventarios); }
			set { SetColumnValue("ID_CUENTA_INVENTARIOS", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvMasCiudade ActiveRecord object related to this InvProveedore
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasCiudade InvMasCiudade
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvMasCiudade.FetchByID(this.IdCiudad); }
			set { SetColumnValue("ID_CIUDAD", value.IdCiudad); }
		}
		
		
		/// <summary>
		/// Returns a InvMasEntidadesBancaria ActiveRecord object related to this InvProveedore
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasEntidadesBancaria InvMasEntidadesBancaria
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvMasEntidadesBancaria.FetchByID(this.IdEntidadBancaria); }
			set { SetColumnValue("ID_ENTIDAD_BANCARIA", value.IdEntidadBancaria); }
		}
		
		
		/// <summary>
		/// Returns a InvMasEstado ActiveRecord object related to this InvProveedore
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasEstado InvMasEstado
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvMasEstado.FetchByID(this.IdEstado); }
			set { SetColumnValue("ID_ESTADO", value.IdEstado); }
		}
		
		
		/// <summary>
		/// Returns a InvMasTiposCuentum ActiveRecord object related to this InvProveedore
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasTiposCuentum InvMasTiposCuentum
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvMasTiposCuentum.FetchByID(this.IdTipoCuenta); }
			set { SetColumnValue("ID_TIPO_CUENTA", value.IdTipoCuenta); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varCodigoProveedor,string varRazonSocial,string varNit,string varDireccion,string varTelefono,string varFax,int varIdCiudad,int varIdEntidadBancaria,string varNumeroCuenta,int varIdTipoCuenta,int varIdEstado,bool? varGranContribuyente,bool? varRegimenComun,bool? varAutoRetenedor,bool? varRetenedorIva,int? varPlazoPago,decimal? varCupo,int? varIdCuentaInventarios,int? varIdCuentaCostosVenta)
		{
			InvProveedore item = new InvProveedore();
			
			item.CodigoProveedor = varCodigoProveedor;
			
			item.RazonSocial = varRazonSocial;
			
			item.Nit = varNit;
			
			item.Direccion = varDireccion;
			
			item.Telefono = varTelefono;
			
			item.Fax = varFax;
			
			item.IdCiudad = varIdCiudad;
			
			item.IdEntidadBancaria = varIdEntidadBancaria;
			
			item.NumeroCuenta = varNumeroCuenta;
			
			item.IdTipoCuenta = varIdTipoCuenta;
			
			item.IdEstado = varIdEstado;
			
			item.GranContribuyente = varGranContribuyente;
			
			item.RegimenComun = varRegimenComun;
			
			item.AutoRetenedor = varAutoRetenedor;
			
			item.RetenedorIva = varRetenedorIva;
			
			item.PlazoPago = varPlazoPago;
			
			item.Cupo = varCupo;
			
			item.IdCuentaInventarios = varIdCuentaInventarios;
			
			item.IdCuentaCostosVenta = varIdCuentaCostosVenta;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdProveedor,string varCodigoProveedor,string varRazonSocial,string varNit,string varDireccion,string varTelefono,string varFax,int varIdCiudad,int varIdEntidadBancaria,string varNumeroCuenta,int varIdTipoCuenta,int varIdEstado,bool? varGranContribuyente,bool? varRegimenComun,bool? varAutoRetenedor,bool? varRetenedorIva,int? varPlazoPago,decimal? varCupo,int? varIdCuentaInventarios,int? varIdCuentaCostosVenta)
		{
			InvProveedore item = new InvProveedore();
			
				item.IdProveedor = varIdProveedor;
			
				item.CodigoProveedor = varCodigoProveedor;
			
				item.RazonSocial = varRazonSocial;
			
				item.Nit = varNit;
			
				item.Direccion = varDireccion;
			
				item.Telefono = varTelefono;
			
				item.Fax = varFax;
			
				item.IdCiudad = varIdCiudad;
			
				item.IdEntidadBancaria = varIdEntidadBancaria;
			
				item.NumeroCuenta = varNumeroCuenta;
			
				item.IdTipoCuenta = varIdTipoCuenta;
			
				item.IdEstado = varIdEstado;
			
				item.GranContribuyente = varGranContribuyente;
			
				item.RegimenComun = varRegimenComun;
			
				item.AutoRetenedor = varAutoRetenedor;
			
				item.RetenedorIva = varRetenedorIva;
			
				item.PlazoPago = varPlazoPago;
			
				item.Cupo = varCupo;
			
				item.IdCuentaInventarios = varIdCuentaInventarios;
			
				item.IdCuentaCostosVenta = varIdCuentaCostosVenta;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdProveedorColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoProveedorColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RazonSocialColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NitColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DireccionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn TelefonoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn FaxColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCiudadColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEntidadBancariaColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroCuentaColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoCuentaColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEstadoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn GranContribuyenteColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn RegimenComunColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn AutoRetenedorColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn RetenedorIvaColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn PlazoPagoColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn CupoColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCuentaInventariosColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCuentaCostosVentaColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdProveedor = @"ID_PROVEEDOR";
			 public static string CodigoProveedor = @"CODIGO_PROVEEDOR";
			 public static string RazonSocial = @"RAZON_SOCIAL";
			 public static string Nit = @"NIT";
			 public static string Direccion = @"DIRECCION";
			 public static string Telefono = @"TELEFONO";
			 public static string Fax = @"FAX";
			 public static string IdCiudad = @"ID_CIUDAD";
			 public static string IdEntidadBancaria = @"ID_ENTIDAD_BANCARIA";
			 public static string NumeroCuenta = @"NUMERO_CUENTA";
			 public static string IdTipoCuenta = @"ID_TIPO_CUENTA";
			 public static string IdEstado = @"ID_ESTADO";
			 public static string GranContribuyente = @"GRAN_CONTRIBUYENTE";
			 public static string RegimenComun = @"REGIMEN_COMUN";
			 public static string AutoRetenedor = @"AUTO_RETENEDOR";
			 public static string RetenedorIva = @"RETENEDOR_IVA";
			 public static string PlazoPago = @"PLAZO_PAGO";
			 public static string Cupo = @"CUPO";
			 public static string IdCuentaInventarios = @"ID_CUENTA_INVENTARIOS";
			 public static string IdCuentaCostosVenta = @"ID_CUENTA_COSTOS_VENTA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
