using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_PERIODO_FACTURACION_PUNTOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvPeriodoFacturacionPuntoController
    {
        // Preload our schema..
        InvPeriodoFacturacionPunto thisSchemaLoad = new InvPeriodoFacturacionPunto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvPeriodoFacturacionPuntoCollection FetchAll()
        {
            InvPeriodoFacturacionPuntoCollection coll = new InvPeriodoFacturacionPuntoCollection();
            Query qry = new Query(InvPeriodoFacturacionPunto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPeriodoFacturacionPuntoCollection FetchByID(object IdFacturacionPuntos)
        {
            InvPeriodoFacturacionPuntoCollection coll = new InvPeriodoFacturacionPuntoCollection().Where("ID_FACTURACION_PUNTOS", IdFacturacionPuntos).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPeriodoFacturacionPuntoCollection FetchByQuery(Query qry)
        {
            InvPeriodoFacturacionPuntoCollection coll = new InvPeriodoFacturacionPuntoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdFacturacionPuntos)
        {
            return (InvPeriodoFacturacionPunto.Delete(IdFacturacionPuntos) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdFacturacionPuntos)
        {
            return (InvPeriodoFacturacionPunto.Destroy(IdFacturacionPuntos) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdPrograma,DateTime FechaInicio,DateTime FechaFin,bool? EsPeriodoBanco,bool? PeriodoActual)
	    {
		    InvPeriodoFacturacionPunto item = new InvPeriodoFacturacionPunto();
		    
            item.IdPrograma = IdPrograma;
            
            item.FechaInicio = FechaInicio;
            
            item.FechaFin = FechaFin;
            
            item.EsPeriodoBanco = EsPeriodoBanco;
            
            item.PeriodoActual = PeriodoActual;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdFacturacionPuntos,int IdPrograma,DateTime FechaInicio,DateTime FechaFin,bool? EsPeriodoBanco,bool? PeriodoActual)
	    {
		    InvPeriodoFacturacionPunto item = new InvPeriodoFacturacionPunto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdFacturacionPuntos = IdFacturacionPuntos;
				
			item.IdPrograma = IdPrograma;
				
			item.FechaInicio = FechaInicio;
				
			item.FechaFin = FechaFin;
				
			item.EsPeriodoBanco = EsPeriodoBanco;
				
			item.PeriodoActual = PeriodoActual;
				
	        item.Save(UserName);
	    }
    }
}
