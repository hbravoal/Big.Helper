using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvCierreMensual class.
	/// </summary>
    [Serializable]
	public partial class InvCierreMensualCollection : ActiveList<InvCierreMensual, InvCierreMensualCollection>
	{	   
		public InvCierreMensualCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCierreMensualCollection</returns>
		public InvCierreMensualCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCierreMensual o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CIERRE_MENSUAL table.
	/// </summary>
	[Serializable]
	public partial class InvCierreMensual : ActiveRecord<InvCierreMensual>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCierreMensual()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCierreMensual(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCierreMensual(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCierreMensual(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CIERRE_MENSUAL", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarAno = new TableSchema.TableColumn(schema);
				colvarAno.ColumnName = "ANO";
				colvarAno.DataType = DbType.Int32;
				colvarAno.MaxLength = 0;
				colvarAno.AutoIncrement = false;
				colvarAno.IsNullable = false;
				colvarAno.IsPrimaryKey = false;
				colvarAno.IsForeignKey = false;
				colvarAno.IsReadOnly = false;
				colvarAno.DefaultSetting = @"";
				colvarAno.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAno);
				
				TableSchema.TableColumn colvarMes = new TableSchema.TableColumn(schema);
				colvarMes.ColumnName = "MES";
				colvarMes.DataType = DbType.Int32;
				colvarMes.MaxLength = 0;
				colvarMes.AutoIncrement = false;
				colvarMes.IsNullable = false;
				colvarMes.IsPrimaryKey = false;
				colvarMes.IsForeignKey = false;
				colvarMes.IsReadOnly = false;
				colvarMes.DefaultSetting = @"";
				colvarMes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMes);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CIERRE_MENSUAL",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("Ano")]
		[Bindable(true)]
		public int Ano 
		{
			get { return GetColumnValue<int>(Columns.Ano); }
			set { SetColumnValue(Columns.Ano, value); }
		}
		  
		[XmlAttribute("Mes")]
		[Bindable(true)]
		public int Mes 
		{
			get { return GetColumnValue<int>(Columns.Mes); }
			set { SetColumnValue(Columns.Mes, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvPrograma ActiveRecord object related to this InvCierreMensual
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvPrograma InvPrograma
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvPrograma.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdPrograma,int varAno,int varMes)
		{
			InvCierreMensual item = new InvCierreMensual();
			
			item.IdPrograma = varIdPrograma;
			
			item.Ano = varAno;
			
			item.Mes = varMes;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varIdPrograma,int varAno,int varMes)
		{
			InvCierreMensual item = new InvCierreMensual();
			
				item.Id = varId;
			
				item.IdPrograma = varIdPrograma;
			
				item.Ano = varAno;
			
				item.Mes = varMes;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn AnoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MesColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string Ano = @"ANO";
			 public static string Mes = @"MES";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
