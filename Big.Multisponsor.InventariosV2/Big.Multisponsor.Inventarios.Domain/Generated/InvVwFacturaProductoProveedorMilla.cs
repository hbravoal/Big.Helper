using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain{
    /// <summary>
    /// Strongly-typed collection for the InvVwFacturaProductoProveedorMilla class.
    /// </summary>
    [Serializable]
    public partial class InvVwFacturaProductoProveedorMillaCollection : ReadOnlyList<InvVwFacturaProductoProveedorMilla, InvVwFacturaProductoProveedorMillaCollection>
    {        
        public InvVwFacturaProductoProveedorMillaCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_FACTURA_PRODUCTO_PROVEEDOR_MILLAS view.
    /// </summary>
    [Serializable]
    public partial class InvVwFacturaProductoProveedorMilla : ReadOnlyRecord<InvVwFacturaProductoProveedorMilla>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_FACTURA_PRODUCTO_PROVEEDOR_MILLAS", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
                colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
                colvarNumeroFactura.DataType = DbType.Int32;
                colvarNumeroFactura.MaxLength = 0;
                colvarNumeroFactura.AutoIncrement = false;
                colvarNumeroFactura.IsNullable = true;
                colvarNumeroFactura.IsPrimaryKey = false;
                colvarNumeroFactura.IsForeignKey = false;
                colvarNumeroFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumeroFactura);
                
                TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
                colvarCodigo.ColumnName = "CODIGO";
                colvarCodigo.DataType = DbType.AnsiString;
                colvarCodigo.MaxLength = 15;
                colvarCodigo.AutoIncrement = false;
                colvarCodigo.IsNullable = false;
                colvarCodigo.IsPrimaryKey = false;
                colvarCodigo.IsForeignKey = false;
                colvarCodigo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigo);
                
                TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
                colvarNombre.ColumnName = "NOMBRE";
                colvarNombre.DataType = DbType.String;
                colvarNombre.MaxLength = 100;
                colvarNombre.AutoIncrement = false;
                colvarNombre.IsNullable = false;
                colvarNombre.IsPrimaryKey = false;
                colvarNombre.IsForeignKey = false;
                colvarNombre.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombre);
                
                TableSchema.TableColumn colvarPrecioVentaBase = new TableSchema.TableColumn(schema);
                colvarPrecioVentaBase.ColumnName = "PRECIO_VENTA_BASE";
                colvarPrecioVentaBase.DataType = DbType.Decimal;
                colvarPrecioVentaBase.MaxLength = 0;
                colvarPrecioVentaBase.AutoIncrement = false;
                colvarPrecioVentaBase.IsNullable = false;
                colvarPrecioVentaBase.IsPrimaryKey = false;
                colvarPrecioVentaBase.IsForeignKey = false;
                colvarPrecioVentaBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrecioVentaBase);
                
                TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
                colvarCostoBase.ColumnName = "COSTO_BASE";
                colvarCostoBase.DataType = DbType.Decimal;
                colvarCostoBase.MaxLength = 0;
                colvarCostoBase.AutoIncrement = false;
                colvarCostoBase.IsNullable = false;
                colvarCostoBase.IsPrimaryKey = false;
                colvarCostoBase.IsForeignKey = false;
                colvarCostoBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarCostoBase);
                
                TableSchema.TableColumn colvarCodigoProveedor = new TableSchema.TableColumn(schema);
                colvarCodigoProveedor.ColumnName = "CODIGO_PROVEEDOR";
                colvarCodigoProveedor.DataType = DbType.AnsiString;
                colvarCodigoProveedor.MaxLength = 20;
                colvarCodigoProveedor.AutoIncrement = false;
                colvarCodigoProveedor.IsNullable = false;
                colvarCodigoProveedor.IsPrimaryKey = false;
                colvarCodigoProveedor.IsForeignKey = false;
                colvarCodigoProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoProveedor);
                
                TableSchema.TableColumn colvarRazonSocial = new TableSchema.TableColumn(schema);
                colvarRazonSocial.ColumnName = "RAZON_SOCIAL";
                colvarRazonSocial.DataType = DbType.String;
                colvarRazonSocial.MaxLength = 200;
                colvarRazonSocial.AutoIncrement = false;
                colvarRazonSocial.IsNullable = false;
                colvarRazonSocial.IsPrimaryKey = false;
                colvarRazonSocial.IsForeignKey = false;
                colvarRazonSocial.IsReadOnly = false;
                
                schema.Columns.Add(colvarRazonSocial);
                
                TableSchema.TableColumn colvarFechaFactura = new TableSchema.TableColumn(schema);
                colvarFechaFactura.ColumnName = "FECHA_FACTURA";
                colvarFechaFactura.DataType = DbType.DateTime;
                colvarFechaFactura.MaxLength = 0;
                colvarFechaFactura.AutoIncrement = false;
                colvarFechaFactura.IsNullable = true;
                colvarFechaFactura.IsPrimaryKey = false;
                colvarFechaFactura.IsForeignKey = false;
                colvarFechaFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaFactura);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_FACTURA_PRODUCTO_PROVEEDOR_MILLAS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwFacturaProductoProveedorMilla()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwFacturaProductoProveedorMilla(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwFacturaProductoProveedorMilla(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwFacturaProductoProveedorMilla(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("NumeroFactura")]
        [Bindable(true)]
        public int? NumeroFactura 
	    {
		    get
		    {
			    return GetColumnValue<int?>("NUMERO_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("NUMERO_FACTURA", value);
            }
        }
	      
        [XmlAttribute("Codigo")]
        [Bindable(true)]
        public string Codigo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO");
		    }
            set 
		    {
			    SetColumnValue("CODIGO", value);
            }
        }
	      
        [XmlAttribute("Nombre")]
        [Bindable(true)]
        public string Nombre 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE", value);
            }
        }
	      
        [XmlAttribute("PrecioVentaBase")]
        [Bindable(true)]
        public decimal PrecioVentaBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("PRECIO_VENTA_BASE");
		    }
            set 
		    {
			    SetColumnValue("PRECIO_VENTA_BASE", value);
            }
        }
	      
        [XmlAttribute("CostoBase")]
        [Bindable(true)]
        public decimal CostoBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("COSTO_BASE");
		    }
            set 
		    {
			    SetColumnValue("COSTO_BASE", value);
            }
        }
	      
        [XmlAttribute("CodigoProveedor")]
        [Bindable(true)]
        public string CodigoProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("CODIGO_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("RazonSocial")]
        [Bindable(true)]
        public string RazonSocial 
	    {
		    get
		    {
			    return GetColumnValue<string>("RAZON_SOCIAL");
		    }
            set 
		    {
			    SetColumnValue("RAZON_SOCIAL", value);
            }
        }
	      
        [XmlAttribute("FechaFactura")]
        [Bindable(true)]
        public DateTime? FechaFactura 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("FECHA_FACTURA", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string NumeroFactura = @"NUMERO_FACTURA";
            
            public static string Codigo = @"CODIGO";
            
            public static string Nombre = @"NOMBRE";
            
            public static string PrecioVentaBase = @"PRECIO_VENTA_BASE";
            
            public static string CostoBase = @"COSTO_BASE";
            
            public static string CodigoProveedor = @"CODIGO_PROVEEDOR";
            
            public static string RazonSocial = @"RAZON_SOCIAL";
            
            public static string FechaFactura = @"FECHA_FACTURA";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
