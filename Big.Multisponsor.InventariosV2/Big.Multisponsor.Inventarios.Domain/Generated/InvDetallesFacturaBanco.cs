using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvDetallesFacturaBanco class.
	/// </summary>
    [Serializable]
	public partial class InvDetallesFacturaBancoCollection : ActiveList<InvDetallesFacturaBanco, InvDetallesFacturaBancoCollection>
	{	   
		public InvDetallesFacturaBancoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvDetallesFacturaBancoCollection</returns>
		public InvDetallesFacturaBancoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvDetallesFacturaBanco o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_DETALLES_FACTURA_BANCO table.
	/// </summary>
	[Serializable]
	public partial class InvDetallesFacturaBanco : ActiveRecord<InvDetallesFacturaBanco>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvDetallesFacturaBanco()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvDetallesFacturaBanco(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvDetallesFacturaBanco(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvDetallesFacturaBanco(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_DETALLES_FACTURA_BANCO", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidDetalleFacturaBanco = new TableSchema.TableColumn(schema);
				colvarGuidDetalleFacturaBanco.ColumnName = "GUID_DETALLE_FACTURA_BANCO";
				colvarGuidDetalleFacturaBanco.DataType = DbType.String;
				colvarGuidDetalleFacturaBanco.MaxLength = 36;
				colvarGuidDetalleFacturaBanco.AutoIncrement = false;
				colvarGuidDetalleFacturaBanco.IsNullable = false;
				colvarGuidDetalleFacturaBanco.IsPrimaryKey = true;
				colvarGuidDetalleFacturaBanco.IsForeignKey = false;
				colvarGuidDetalleFacturaBanco.IsReadOnly = false;
				colvarGuidDetalleFacturaBanco.DefaultSetting = @"";
				colvarGuidDetalleFacturaBanco.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidDetalleFacturaBanco);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
				colvarCostoBase.ColumnName = "COSTO_BASE";
				colvarCostoBase.DataType = DbType.Decimal;
				colvarCostoBase.MaxLength = 0;
				colvarCostoBase.AutoIncrement = false;
				colvarCostoBase.IsNullable = false;
				colvarCostoBase.IsPrimaryKey = false;
				colvarCostoBase.IsForeignKey = false;
				colvarCostoBase.IsReadOnly = false;
				colvarCostoBase.DefaultSetting = @"";
				colvarCostoBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoBase);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarGuidFacturaBanco = new TableSchema.TableColumn(schema);
				colvarGuidFacturaBanco.ColumnName = "GUID_FACTURA_BANCO";
				colvarGuidFacturaBanco.DataType = DbType.String;
				colvarGuidFacturaBanco.MaxLength = 36;
				colvarGuidFacturaBanco.AutoIncrement = false;
				colvarGuidFacturaBanco.IsNullable = false;
				colvarGuidFacturaBanco.IsPrimaryKey = false;
				colvarGuidFacturaBanco.IsForeignKey = true;
				colvarGuidFacturaBanco.IsReadOnly = false;
				colvarGuidFacturaBanco.DefaultSetting = @"";
				
					colvarGuidFacturaBanco.ForeignKeyTableName = "INV_FACTURAS_BANCO";
				schema.Columns.Add(colvarGuidFacturaBanco);
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = false;
				colvarGuidReferenciaProducto.IsForeignKey = true;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				
					colvarGuidReferenciaProducto.ForeignKeyTableName = "INV_REFERENCIAS_PRODUCTO";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_DETALLES_FACTURA_BANCO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidDetalleFacturaBanco")]
		[Bindable(true)]
		public string GuidDetalleFacturaBanco 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleFacturaBanco); }
			set { SetColumnValue(Columns.GuidDetalleFacturaBanco, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("CostoBase")]
		[Bindable(true)]
		public decimal CostoBase 
		{
			get { return GetColumnValue<decimal>(Columns.CostoBase); }
			set { SetColumnValue(Columns.CostoBase, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("GuidFacturaBanco")]
		[Bindable(true)]
		public string GuidFacturaBanco 
		{
			get { return GetColumnValue<string>(Columns.GuidFacturaBanco); }
			set { SetColumnValue(Columns.GuidFacturaBanco, value); }
		}
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvFacturasBanco ActiveRecord object related to this InvDetallesFacturaBanco
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasBanco InvFacturasBanco
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvFacturasBanco.FetchByID(this.GuidFacturaBanco); }
			set { SetColumnValue("GUID_FACTURA_BANCO", value.GuidFacturaBanco); }
		}
		
		
		/// <summary>
		/// Returns a InvReferenciasProducto ActiveRecord object related to this InvDetallesFacturaBanco
		/// 
		/// </summary>
		public Big.Multisponsor.Inventarios.Generated.Domain.InvReferenciasProducto InvReferenciasProducto
		{
			get { return Big.Multisponsor.Inventarios.Generated.Domain.InvReferenciasProducto.FetchByID(this.GuidReferenciaProducto); }
			set { SetColumnValue("GUID_REFERENCIA_PRODUCTO", value.GuidReferenciaProducto); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidDetalleFacturaBanco,int varCantidad,decimal varCostoBase,decimal varIva,string varGuidFacturaBanco,string varGuidReferenciaProducto)
		{
			InvDetallesFacturaBanco item = new InvDetallesFacturaBanco();
			
			item.GuidDetalleFacturaBanco = varGuidDetalleFacturaBanco;
			
			item.Cantidad = varCantidad;
			
			item.CostoBase = varCostoBase;
			
			item.Iva = varIva;
			
			item.GuidFacturaBanco = varGuidFacturaBanco;
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidDetalleFacturaBanco,int varCantidad,decimal varCostoBase,decimal varIva,string varGuidFacturaBanco,string varGuidReferenciaProducto)
		{
			InvDetallesFacturaBanco item = new InvDetallesFacturaBanco();
			
				item.GuidDetalleFacturaBanco = varGuidDetalleFacturaBanco;
			
				item.Cantidad = varCantidad;
			
				item.CostoBase = varCostoBase;
			
				item.Iva = varIva;
			
				item.GuidFacturaBanco = varGuidFacturaBanco;
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidDetalleFacturaBancoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoBaseColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidFacturaBancoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidDetalleFacturaBanco = @"GUID_DETALLE_FACTURA_BANCO";
			 public static string Cantidad = @"CANTIDAD";
			 public static string CostoBase = @"COSTO_BASE";
			 public static string Iva = @"IVA";
			 public static string GuidFacturaBanco = @"GUID_FACTURA_BANCO";
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
