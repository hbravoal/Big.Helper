using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvAvmCliente class.
	/// </summary>
    [Serializable]
	public partial class InvAvmClienteCollection : ActiveList<InvAvmCliente, InvAvmClienteCollection>
	{	   
		public InvAvmClienteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvAvmClienteCollection</returns>
		public InvAvmClienteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvAvmCliente o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_AVM_CLIENTES table.
	/// </summary>
	[Serializable]
	public partial class InvAvmCliente : ActiveRecord<InvAvmCliente>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvAvmCliente()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvAvmCliente(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvAvmCliente(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvAvmCliente(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_AVM_CLIENTES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarCedula = new TableSchema.TableColumn(schema);
				colvarCedula.ColumnName = "CEDULA";
				colvarCedula.DataType = DbType.AnsiString;
				colvarCedula.MaxLength = 15;
				colvarCedula.AutoIncrement = false;
				colvarCedula.IsNullable = false;
				colvarCedula.IsPrimaryKey = true;
				colvarCedula.IsForeignKey = false;
				colvarCedula.IsReadOnly = false;
				colvarCedula.DefaultSetting = @"";
				colvarCedula.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCedula);
				
				TableSchema.TableColumn colvarCodigoAdvermind = new TableSchema.TableColumn(schema);
				colvarCodigoAdvermind.ColumnName = "CODIGO_ADVERMIND";
				colvarCodigoAdvermind.DataType = DbType.Decimal;
				colvarCodigoAdvermind.MaxLength = 0;
				colvarCodigoAdvermind.AutoIncrement = false;
				colvarCodigoAdvermind.IsNullable = true;
				colvarCodigoAdvermind.IsPrimaryKey = false;
				colvarCodigoAdvermind.IsForeignKey = false;
				colvarCodigoAdvermind.IsReadOnly = false;
				colvarCodigoAdvermind.DefaultSetting = @"";
				colvarCodigoAdvermind.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigoAdvermind);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_AVM_CLIENTES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Cedula")]
		[Bindable(true)]
		public string Cedula 
		{
			get { return GetColumnValue<string>(Columns.Cedula); }
			set { SetColumnValue(Columns.Cedula, value); }
		}
		  
		[XmlAttribute("CodigoAdvermind")]
		[Bindable(true)]
		public decimal? CodigoAdvermind 
		{
			get { return GetColumnValue<decimal?>(Columns.CodigoAdvermind); }
			set { SetColumnValue(Columns.CodigoAdvermind, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varCedula,decimal? varCodigoAdvermind)
		{
			InvAvmCliente item = new InvAvmCliente();
			
			item.Cedula = varCedula;
			
			item.CodigoAdvermind = varCodigoAdvermind;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varCedula,decimal? varCodigoAdvermind)
		{
			InvAvmCliente item = new InvAvmCliente();
			
				item.Cedula = varCedula;
			
				item.CodigoAdvermind = varCodigoAdvermind;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn CedulaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoAdvermindColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Cedula = @"CEDULA";
			 public static string CodigoAdvermind = @"CODIGO_ADVERMIND";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
