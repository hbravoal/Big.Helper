using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_PROGRAMAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvProgramaController
    {
        // Preload our schema..
        InvPrograma thisSchemaLoad = new InvPrograma();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvProgramaCollection FetchAll()
        {
            InvProgramaCollection coll = new InvProgramaCollection();
            Query qry = new Query(InvPrograma.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvProgramaCollection FetchByID(object IdPrograma)
        {
            InvProgramaCollection coll = new InvProgramaCollection().Where("ID_PROGRAMA", IdPrograma).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvProgramaCollection FetchByQuery(Query qry)
        {
            InvProgramaCollection coll = new InvProgramaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPrograma)
        {
            return (InvPrograma.Delete(IdPrograma) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPrograma)
        {
            return (InvPrograma.Destroy(IdPrograma) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdPrograma,string NombrePrograma,int IdEmpresa,DateTime FechaInicio,int? IdResolucion,int? IdNotaCredito,int? IdResolucionBanco,decimal FactorPunto,int Redondeo,bool FacturaProveedorPuntos,decimal PorcentajeComision,bool CobrarEnvio,int? PlazoVencimientoFactura,int? IdTipoFacturacion,int? NumeroPeriodosProveedorPuntos,int? NumeroPeriodosEstablecimientos,bool? Cerrado,decimal? IvaCurrier)
	    {
		    InvPrograma item = new InvPrograma();
		    
            item.IdPrograma = IdPrograma;
            
            item.NombrePrograma = NombrePrograma;
            
            item.IdEmpresa = IdEmpresa;
            
            item.FechaInicio = FechaInicio;
            
            item.IdResolucion = IdResolucion;
            
            item.IdNotaCredito = IdNotaCredito;
            
            item.IdResolucionBanco = IdResolucionBanco;
            
            item.FactorPunto = FactorPunto;
            
            item.Redondeo = Redondeo;
            
            item.FacturaProveedorPuntos = FacturaProveedorPuntos;
            
            item.PorcentajeComision = PorcentajeComision;
            
            item.CobrarEnvio = CobrarEnvio;
            
            item.PlazoVencimientoFactura = PlazoVencimientoFactura;
            
            item.IdTipoFacturacion = IdTipoFacturacion;
            
            item.NumeroPeriodosProveedorPuntos = NumeroPeriodosProveedorPuntos;
            
            item.NumeroPeriodosEstablecimientos = NumeroPeriodosEstablecimientos;
            
            item.Cerrado = Cerrado;
            
            item.IvaCurrier = IvaCurrier;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPrograma,string NombrePrograma,int IdEmpresa,DateTime FechaInicio,int? IdResolucion,int? IdNotaCredito,int? IdResolucionBanco,decimal FactorPunto,int Redondeo,bool FacturaProveedorPuntos,decimal PorcentajeComision,bool CobrarEnvio,int? PlazoVencimientoFactura,int? IdTipoFacturacion,int? NumeroPeriodosProveedorPuntos,int? NumeroPeriodosEstablecimientos,bool? Cerrado,decimal? IvaCurrier)
	    {
		    InvPrograma item = new InvPrograma();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPrograma = IdPrograma;
				
			item.NombrePrograma = NombrePrograma;
				
			item.IdEmpresa = IdEmpresa;
				
			item.FechaInicio = FechaInicio;
				
			item.IdResolucion = IdResolucion;
				
			item.IdNotaCredito = IdNotaCredito;
				
			item.IdResolucionBanco = IdResolucionBanco;
				
			item.FactorPunto = FactorPunto;
				
			item.Redondeo = Redondeo;
				
			item.FacturaProveedorPuntos = FacturaProveedorPuntos;
				
			item.PorcentajeComision = PorcentajeComision;
				
			item.CobrarEnvio = CobrarEnvio;
				
			item.PlazoVencimientoFactura = PlazoVencimientoFactura;
				
			item.IdTipoFacturacion = IdTipoFacturacion;
				
			item.NumeroPeriodosProveedorPuntos = NumeroPeriodosProveedorPuntos;
				
			item.NumeroPeriodosEstablecimientos = NumeroPeriodosEstablecimientos;
				
			item.Cerrado = Cerrado;
				
			item.IvaCurrier = IvaCurrier;
				
	        item.Save(UserName);
	    }
    }
}
