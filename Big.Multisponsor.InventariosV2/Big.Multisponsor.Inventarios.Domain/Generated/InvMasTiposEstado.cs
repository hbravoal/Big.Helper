using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvMasTiposEstado class.
	/// </summary>
    [Serializable]
	public partial class InvMasTiposEstadoCollection : ActiveList<InvMasTiposEstado, InvMasTiposEstadoCollection>
	{	   
		public InvMasTiposEstadoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasTiposEstadoCollection</returns>
		public InvMasTiposEstadoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasTiposEstado o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_ESTADO table.
	/// </summary>
	[Serializable]
	public partial class InvMasTiposEstado : ActiveRecord<InvMasTiposEstado>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasTiposEstado()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasTiposEstado(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasTiposEstado(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasTiposEstado(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_ESTADO", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoEstado = new TableSchema.TableColumn(schema);
				colvarIdTipoEstado.ColumnName = "ID_TIPO_ESTADO";
				colvarIdTipoEstado.DataType = DbType.Int32;
				colvarIdTipoEstado.MaxLength = 0;
				colvarIdTipoEstado.AutoIncrement = false;
				colvarIdTipoEstado.IsNullable = false;
				colvarIdTipoEstado.IsPrimaryKey = true;
				colvarIdTipoEstado.IsForeignKey = false;
				colvarIdTipoEstado.IsReadOnly = false;
				colvarIdTipoEstado.DefaultSetting = @"";
				colvarIdTipoEstado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTipoEstado);
				
				TableSchema.TableColumn colvarNombreGrupo = new TableSchema.TableColumn(schema);
				colvarNombreGrupo.ColumnName = "NOMBRE_GRUPO";
				colvarNombreGrupo.DataType = DbType.AnsiString;
				colvarNombreGrupo.MaxLength = 50;
				colvarNombreGrupo.AutoIncrement = false;
				colvarNombreGrupo.IsNullable = false;
				colvarNombreGrupo.IsPrimaryKey = false;
				colvarNombreGrupo.IsForeignKey = false;
				colvarNombreGrupo.IsReadOnly = false;
				colvarNombreGrupo.DefaultSetting = @"";
				colvarNombreGrupo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreGrupo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_TIPOS_ESTADO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoEstado")]
		[Bindable(true)]
		public int IdTipoEstado 
		{
			get { return GetColumnValue<int>(Columns.IdTipoEstado); }
			set { SetColumnValue(Columns.IdTipoEstado, value); }
		}
		  
		[XmlAttribute("NombreGrupo")]
		[Bindable(true)]
		public string NombreGrupo 
		{
			get { return GetColumnValue<string>(Columns.NombreGrupo); }
			set { SetColumnValue(Columns.NombreGrupo, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Multisponsor.Inventarios.Generated.Domain.InvMasEstadoCollection InvMasEstados()
		{
			return new Big.Multisponsor.Inventarios.Generated.Domain.InvMasEstadoCollection().Where(InvMasEstado.Columns.IdTipoEstado, IdTipoEstado).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdTipoEstado,string varNombreGrupo)
		{
			InvMasTiposEstado item = new InvMasTiposEstado();
			
			item.IdTipoEstado = varIdTipoEstado;
			
			item.NombreGrupo = varNombreGrupo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoEstado,string varNombreGrupo)
		{
			InvMasTiposEstado item = new InvMasTiposEstado();
			
				item.IdTipoEstado = varIdTipoEstado;
			
				item.NombreGrupo = varNombreGrupo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoEstadoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreGrupoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoEstado = @"ID_TIPO_ESTADO";
			 public static string NombreGrupo = @"NOMBRE_GRUPO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
