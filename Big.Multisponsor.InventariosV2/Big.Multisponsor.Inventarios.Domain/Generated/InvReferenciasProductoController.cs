using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_REFERENCIAS_PRODUCTO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvReferenciasProductoController
    {
        // Preload our schema..
        InvReferenciasProducto thisSchemaLoad = new InvReferenciasProducto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvReferenciasProductoCollection FetchAll()
        {
            InvReferenciasProductoCollection coll = new InvReferenciasProductoCollection();
            Query qry = new Query(InvReferenciasProducto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvReferenciasProductoCollection FetchByID(object GuidReferenciaProducto)
        {
            InvReferenciasProductoCollection coll = new InvReferenciasProductoCollection().Where("GUID_REFERENCIA_PRODUCTO", GuidReferenciaProducto).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvReferenciasProductoCollection FetchByQuery(Query qry)
        {
            InvReferenciasProductoCollection coll = new InvReferenciasProductoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidReferenciaProducto)
        {
            return (InvReferenciasProducto.Delete(GuidReferenciaProducto) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidReferenciaProducto)
        {
            return (InvReferenciasProducto.Destroy(GuidReferenciaProducto) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidReferenciaProducto,string NombreReferencia,string GuidProducto,int IdEstado,int IdPrograma)
	    {
		    InvReferenciasProducto item = new InvReferenciasProducto();
		    
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
            item.NombreReferencia = NombreReferencia;
            
            item.GuidProducto = GuidProducto;
            
            item.IdEstado = IdEstado;
            
            item.IdPrograma = IdPrograma;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidReferenciaProducto,string NombreReferencia,string GuidProducto,int IdEstado,int IdPrograma)
	    {
		    InvReferenciasProducto item = new InvReferenciasProducto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
			item.NombreReferencia = NombreReferencia;
				
			item.GuidProducto = GuidProducto;
				
			item.IdEstado = IdEstado;
				
			item.IdPrograma = IdPrograma;
				
	        item.Save(UserName);
	    }
    }
}
