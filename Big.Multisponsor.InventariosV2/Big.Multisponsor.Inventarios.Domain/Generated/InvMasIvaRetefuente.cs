using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
	/// <summary>
	/// Strongly-typed collection for the InvMasIvaRetefuente class.
	/// </summary>
    [Serializable]
	public partial class InvMasIvaRetefuenteCollection : ActiveList<InvMasIvaRetefuente, InvMasIvaRetefuenteCollection>
	{	   
		public InvMasIvaRetefuenteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasIvaRetefuenteCollection</returns>
		public InvMasIvaRetefuenteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasIvaRetefuente o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_IVA_RETEFUENTE table.
	/// </summary>
	[Serializable]
	public partial class InvMasIvaRetefuente : ActiveRecord<InvMasIvaRetefuente>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasIvaRetefuente()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasIvaRetefuente(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasIvaRetefuente(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasIvaRetefuente(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_IVA_RETEFUENTE", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdIvaRetefuente = new TableSchema.TableColumn(schema);
				colvarIdIvaRetefuente.ColumnName = "ID_IVA_RETEFUENTE";
				colvarIdIvaRetefuente.DataType = DbType.Int32;
				colvarIdIvaRetefuente.MaxLength = 0;
				colvarIdIvaRetefuente.AutoIncrement = false;
				colvarIdIvaRetefuente.IsNullable = false;
				colvarIdIvaRetefuente.IsPrimaryKey = true;
				colvarIdIvaRetefuente.IsForeignKey = false;
				colvarIdIvaRetefuente.IsReadOnly = false;
				colvarIdIvaRetefuente.DefaultSetting = @"";
				colvarIdIvaRetefuente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdIvaRetefuente);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarRetencionFuente = new TableSchema.TableColumn(schema);
				colvarRetencionFuente.ColumnName = "RETENCION_FUENTE";
				colvarRetencionFuente.DataType = DbType.Decimal;
				colvarRetencionFuente.MaxLength = 0;
				colvarRetencionFuente.AutoIncrement = false;
				colvarRetencionFuente.IsNullable = false;
				colvarRetencionFuente.IsPrimaryKey = false;
				colvarRetencionFuente.IsForeignKey = false;
				colvarRetencionFuente.IsReadOnly = false;
				colvarRetencionFuente.DefaultSetting = @"";
				colvarRetencionFuente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRetencionFuente);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_IVA_RETEFUENTE",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdIvaRetefuente")]
		[Bindable(true)]
		public int IdIvaRetefuente 
		{
			get { return GetColumnValue<int>(Columns.IdIvaRetefuente); }
			set { SetColumnValue(Columns.IdIvaRetefuente, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("RetencionFuente")]
		[Bindable(true)]
		public decimal RetencionFuente 
		{
			get { return GetColumnValue<decimal>(Columns.RetencionFuente); }
			set { SetColumnValue(Columns.RetencionFuente, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdIvaRetefuente,decimal varIva,decimal varRetencionFuente)
		{
			InvMasIvaRetefuente item = new InvMasIvaRetefuente();
			
			item.IdIvaRetefuente = varIdIvaRetefuente;
			
			item.Iva = varIva;
			
			item.RetencionFuente = varRetencionFuente;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdIvaRetefuente,decimal varIva,decimal varRetencionFuente)
		{
			InvMasIvaRetefuente item = new InvMasIvaRetefuente();
			
				item.IdIvaRetefuente = varIdIvaRetefuente;
			
				item.Iva = varIva;
			
				item.RetencionFuente = varRetencionFuente;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdIvaRetefuenteColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn RetencionFuenteColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdIvaRetefuente = @"ID_IVA_RETEFUENTE";
			 public static string Iva = @"IVA";
			 public static string RetencionFuente = @"RETENCION_FUENTE";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
