using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Multisponsor.Inventarios.Generated.Domain
{
    /// <summary>
    /// Controller class for INV_CGUNO_MAS_ESTADOS_PERIODO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoMasEstadosPeriodoController
    {
        // Preload our schema..
        InvCgunoMasEstadosPeriodo thisSchemaLoad = new InvCgunoMasEstadosPeriodo();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoMasEstadosPeriodoCollection FetchAll()
        {
            InvCgunoMasEstadosPeriodoCollection coll = new InvCgunoMasEstadosPeriodoCollection();
            Query qry = new Query(InvCgunoMasEstadosPeriodo.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoMasEstadosPeriodoCollection FetchByID(object Id)
        {
            InvCgunoMasEstadosPeriodoCollection coll = new InvCgunoMasEstadosPeriodoCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoMasEstadosPeriodoCollection FetchByQuery(Query qry)
        {
            InvCgunoMasEstadosPeriodoCollection coll = new InvCgunoMasEstadosPeriodoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoMasEstadosPeriodo.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoMasEstadosPeriodo.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int Id,string Estado)
	    {
		    InvCgunoMasEstadosPeriodo item = new InvCgunoMasEstadosPeriodo();
		    
            item.Id = Id;
            
            item.Estado = Estado;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Estado)
	    {
		    InvCgunoMasEstadosPeriodo item = new InvCgunoMasEstadosPeriodo();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Estado = Estado;
				
	        item.Save(UserName);
	    }
    }
}
