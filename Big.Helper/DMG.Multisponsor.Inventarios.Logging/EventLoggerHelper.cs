using System;
using System.Data;
using System.Configuration;
using System.Web;

namespace DMG.Multisponsor.Inventarios.Logging
{
    /// <summary>
    /// Clase de apoyo para escribir en el log de eventos de la maquina.
    /// </summary>
    public static class EventLoggerHelper
    {
        private static DMG.EventLogger.EventLogger logger;

        public static void WriteInfo(string message, string source)
        {
            try
            {
                InstanciateLogger(source);
                logger.writeInfo(message);
            }
            catch { }
        }

        public static void WriteWarning(string message, string source)
        {
            try
            {
                InstanciateLogger(source);
                logger.writeWarning(message);
            }
            catch { }
        }

        public static void WriteError(string message, string source)
        {
            try
            {
                InstanciateLogger(source);
                logger.writeError(message);
            }
            catch { }
        }

        private static void InstanciateLogger(string source)
        {
            logger = DMG.EventLogger.EventLogger.getInstance(source, ConfigurationManager.AppSettings["EventLog"]);
        }
    }
}