using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoMasCamposTipoCausacionController
    {
        // Preload our schema..
        InvCgunoMasCamposTipoCausacion thisSchemaLoad = new InvCgunoMasCamposTipoCausacion();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoMasCamposTipoCausacionCollection FetchAll()
        {
            InvCgunoMasCamposTipoCausacionCollection coll = new InvCgunoMasCamposTipoCausacionCollection();
            Query qry = new Query(InvCgunoMasCamposTipoCausacion.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoMasCamposTipoCausacionCollection FetchByID(object Id)
        {
            InvCgunoMasCamposTipoCausacionCollection coll = new InvCgunoMasCamposTipoCausacionCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoMasCamposTipoCausacionCollection FetchByQuery(Query qry)
        {
            InvCgunoMasCamposTipoCausacionCollection coll = new InvCgunoMasCamposTipoCausacionCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoMasCamposTipoCausacion.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoMasCamposTipoCausacion.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int Id,int IdTipoCausacion,string Descripcion,string MostrarComo)
	    {
		    InvCgunoMasCamposTipoCausacion item = new InvCgunoMasCamposTipoCausacion();
		    
            item.Id = Id;
            
            item.IdTipoCausacion = IdTipoCausacion;
            
            item.Descripcion = Descripcion;
            
            item.MostrarComo = MostrarComo;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int IdTipoCausacion,string Descripcion,string MostrarComo)
	    {
		    InvCgunoMasCamposTipoCausacion item = new InvCgunoMasCamposTipoCausacion();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.IdTipoCausacion = IdTipoCausacion;
				
			item.Descripcion = Descripcion;
				
			item.MostrarComo = MostrarComo;
				
	        item.Save(UserName);
	    }
    }
}
