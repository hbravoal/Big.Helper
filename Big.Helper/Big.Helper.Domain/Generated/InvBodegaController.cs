using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_BODEGAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvBodegaController
    {
        // Preload our schema..
        InvBodega thisSchemaLoad = new InvBodega();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvBodegaCollection FetchAll()
        {
            InvBodegaCollection coll = new InvBodegaCollection();
            Query qry = new Query(InvBodega.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvBodegaCollection FetchByID(object IdBodega)
        {
            InvBodegaCollection coll = new InvBodegaCollection().Where("ID_BODEGA", IdBodega).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvBodegaCollection FetchByQuery(Query qry)
        {
            InvBodegaCollection coll = new InvBodegaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdBodega)
        {
            return (InvBodega.Delete(IdBodega) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdBodega)
        {
            return (InvBodega.Destroy(IdBodega) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string NombreBodega,int IdPrograma,int IdTipoBodega)
	    {
		    InvBodega item = new InvBodega();
		    
            item.NombreBodega = NombreBodega;
            
            item.IdPrograma = IdPrograma;
            
            item.IdTipoBodega = IdTipoBodega;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdBodega,string NombreBodega,int IdPrograma,int IdTipoBodega)
	    {
		    InvBodega item = new InvBodega();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdBodega = IdBodega;
				
			item.NombreBodega = NombreBodega;
				
			item.IdPrograma = IdPrograma;
				
			item.IdTipoBodega = IdTipoBodega;
				
	        item.Save(UserName);
	    }
    }
}
