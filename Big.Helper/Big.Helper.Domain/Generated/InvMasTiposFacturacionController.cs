using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_FACTURACION
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposFacturacionController
    {
        // Preload our schema..
        InvMasTiposFacturacion thisSchemaLoad = new InvMasTiposFacturacion();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposFacturacionCollection FetchAll()
        {
            InvMasTiposFacturacionCollection coll = new InvMasTiposFacturacionCollection();
            Query qry = new Query(InvMasTiposFacturacion.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposFacturacionCollection FetchByID(object IdTipoFacturacion)
        {
            InvMasTiposFacturacionCollection coll = new InvMasTiposFacturacionCollection().Where("ID_TIPO_FACTURACION", IdTipoFacturacion).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposFacturacionCollection FetchByQuery(Query qry)
        {
            InvMasTiposFacturacionCollection coll = new InvMasTiposFacturacionCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoFacturacion)
        {
            return (InvMasTiposFacturacion.Delete(IdTipoFacturacion) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoFacturacion)
        {
            return (InvMasTiposFacturacion.Destroy(IdTipoFacturacion) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Descripcion)
	    {
		    InvMasTiposFacturacion item = new InvMasTiposFacturacion();
		    
            item.Descripcion = Descripcion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoFacturacion,string Descripcion)
	    {
		    InvMasTiposFacturacion item = new InvMasTiposFacturacion();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoFacturacion = IdTipoFacturacion;
				
			item.Descripcion = Descripcion;
				
	        item.Save(UserName);
	    }
    }
}
