using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_DELIVERY
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposDeliveryController
    {
        // Preload our schema..
        InvMasTiposDelivery thisSchemaLoad = new InvMasTiposDelivery();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposDeliveryCollection FetchAll()
        {
            InvMasTiposDeliveryCollection coll = new InvMasTiposDeliveryCollection();
            Query qry = new Query(InvMasTiposDelivery.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposDeliveryCollection FetchByID(object IdTipoDelivery)
        {
            InvMasTiposDeliveryCollection coll = new InvMasTiposDeliveryCollection().Where("ID_TIPO_DELIVERY", IdTipoDelivery).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposDeliveryCollection FetchByQuery(Query qry)
        {
            InvMasTiposDeliveryCollection coll = new InvMasTiposDeliveryCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoDelivery)
        {
            return (InvMasTiposDelivery.Delete(IdTipoDelivery) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoDelivery)
        {
            return (InvMasTiposDelivery.Destroy(IdTipoDelivery) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdTipoDelivery,string TipoDelivery,bool? SystemManaged)
	    {
		    InvMasTiposDelivery item = new InvMasTiposDelivery();
		    
            item.IdTipoDelivery = IdTipoDelivery;
            
            item.TipoDelivery = TipoDelivery;
            
            item.SystemManaged = SystemManaged;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoDelivery,string TipoDelivery,bool? SystemManaged)
	    {
		    InvMasTiposDelivery item = new InvMasTiposDelivery();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoDelivery = IdTipoDelivery;
				
			item.TipoDelivery = TipoDelivery;
				
			item.SystemManaged = SystemManaged;
				
	        item.Save(UserName);
	    }
    }
}
