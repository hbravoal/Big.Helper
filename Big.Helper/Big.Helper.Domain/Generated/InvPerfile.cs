using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvPerfile class.
	/// </summary>
    [Serializable]
	public partial class InvPerfileCollection : ActiveList<InvPerfile, InvPerfileCollection>
	{	   
		public InvPerfileCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvPerfileCollection</returns>
		public InvPerfileCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvPerfile o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PERFILES table.
	/// </summary>
	[Serializable]
	public partial class InvPerfile : ActiveRecord<InvPerfile>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvPerfile()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvPerfile(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvPerfile(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvPerfile(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PERFILES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPerfil = new TableSchema.TableColumn(schema);
				colvarIdPerfil.ColumnName = "ID_PERFIL";
				colvarIdPerfil.DataType = DbType.Int32;
				colvarIdPerfil.MaxLength = 0;
				colvarIdPerfil.AutoIncrement = true;
				colvarIdPerfil.IsNullable = false;
				colvarIdPerfil.IsPrimaryKey = true;
				colvarIdPerfil.IsForeignKey = false;
				colvarIdPerfil.IsReadOnly = false;
				colvarIdPerfil.DefaultSetting = @"";
				colvarIdPerfil.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPerfil);
				
				TableSchema.TableColumn colvarNombrePerfil = new TableSchema.TableColumn(schema);
				colvarNombrePerfil.ColumnName = "NOMBRE_PERFIL";
				colvarNombrePerfil.DataType = DbType.String;
				colvarNombrePerfil.MaxLength = 100;
				colvarNombrePerfil.AutoIncrement = false;
				colvarNombrePerfil.IsNullable = false;
				colvarNombrePerfil.IsPrimaryKey = false;
				colvarNombrePerfil.IsForeignKey = false;
				colvarNombrePerfil.IsReadOnly = false;
				colvarNombrePerfil.DefaultSetting = @"";
				colvarNombrePerfil.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombrePerfil);
				
				TableSchema.TableColumn colvarDescripcionPerfil = new TableSchema.TableColumn(schema);
				colvarDescripcionPerfil.ColumnName = "DESCRIPCION_PERFIL";
				colvarDescripcionPerfil.DataType = DbType.String;
				colvarDescripcionPerfil.MaxLength = 400;
				colvarDescripcionPerfil.AutoIncrement = false;
				colvarDescripcionPerfil.IsNullable = true;
				colvarDescripcionPerfil.IsPrimaryKey = false;
				colvarDescripcionPerfil.IsForeignKey = false;
				colvarDescripcionPerfil.IsReadOnly = false;
				colvarDescripcionPerfil.DefaultSetting = @"";
				colvarDescripcionPerfil.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcionPerfil);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_PERFILES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPerfil")]
		[Bindable(true)]
		public int IdPerfil 
		{
			get { return GetColumnValue<int>(Columns.IdPerfil); }
			set { SetColumnValue(Columns.IdPerfil, value); }
		}
		  
		[XmlAttribute("NombrePerfil")]
		[Bindable(true)]
		public string NombrePerfil 
		{
			get { return GetColumnValue<string>(Columns.NombrePerfil); }
			set { SetColumnValue(Columns.NombrePerfil, value); }
		}
		  
		[XmlAttribute("DescripcionPerfil")]
		[Bindable(true)]
		public string DescripcionPerfil 
		{
			get { return GetColumnValue<string>(Columns.DescripcionPerfil); }
			set { SetColumnValue(Columns.DescripcionPerfil, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvPerfilesPermisoCollection InvPerfilesPermisos()
		{
			return new Big.Helper.Domain.Generated.InvPerfilesPermisoCollection().Where(InvPerfilesPermiso.Columns.IdPerfil, IdPerfil).Load();
		}
		public Big.Helper.Domain.Generated.InvUsuarioCollection InvUsuarios()
		{
			return new Big.Helper.Domain.Generated.InvUsuarioCollection().Where(InvUsuario.Columns.IdPerfil, IdPerfil).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombrePerfil,string varDescripcionPerfil)
		{
			InvPerfile item = new InvPerfile();
			
			item.NombrePerfil = varNombrePerfil;
			
			item.DescripcionPerfil = varDescripcionPerfil;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdPerfil,string varNombrePerfil,string varDescripcionPerfil)
		{
			InvPerfile item = new InvPerfile();
			
				item.IdPerfil = varIdPerfil;
			
				item.NombrePerfil = varNombrePerfil;
			
				item.DescripcionPerfil = varDescripcionPerfil;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdPerfilColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombrePerfilColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionPerfilColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPerfil = @"ID_PERFIL";
			 public static string NombrePerfil = @"NOMBRE_PERFIL";
			 public static string DescripcionPerfil = @"DESCRIPCION_PERFIL";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
