using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoMasCamposTipoCausacion class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoMasCamposTipoCausacionCollection : ActiveList<InvCgunoMasCamposTipoCausacion, InvCgunoMasCamposTipoCausacionCollection>
	{	   
		public InvCgunoMasCamposTipoCausacionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoMasCamposTipoCausacionCollection</returns>
		public InvCgunoMasCamposTipoCausacionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoMasCamposTipoCausacion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoMasCamposTipoCausacion : ActiveRecord<InvCgunoMasCamposTipoCausacion>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoMasCamposTipoCausacion()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoMasCamposTipoCausacion(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoMasCamposTipoCausacion(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoMasCamposTipoCausacion(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIdTipoCausacion = new TableSchema.TableColumn(schema);
				colvarIdTipoCausacion.ColumnName = "ID_TIPO_CAUSACION";
				colvarIdTipoCausacion.DataType = DbType.Int32;
				colvarIdTipoCausacion.MaxLength = 0;
				colvarIdTipoCausacion.AutoIncrement = false;
				colvarIdTipoCausacion.IsNullable = false;
				colvarIdTipoCausacion.IsPrimaryKey = false;
				colvarIdTipoCausacion.IsForeignKey = true;
				colvarIdTipoCausacion.IsReadOnly = false;
				colvarIdTipoCausacion.DefaultSetting = @"";
				
					colvarIdTipoCausacion.ForeignKeyTableName = "INV_CGUNO_MAS_TIPOS_CAUSACION";
				schema.Columns.Add(colvarIdTipoCausacion);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "DESCRIPCION";
				colvarDescripcion.DataType = DbType.AnsiString;
				colvarDescripcion.MaxLength = 20;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = false;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				TableSchema.TableColumn colvarMostrarComo = new TableSchema.TableColumn(schema);
				colvarMostrarComo.ColumnName = "MOSTRAR_COMO";
				colvarMostrarComo.DataType = DbType.AnsiString;
				colvarMostrarComo.MaxLength = 35;
				colvarMostrarComo.AutoIncrement = false;
				colvarMostrarComo.IsNullable = true;
				colvarMostrarComo.IsPrimaryKey = false;
				colvarMostrarComo.IsForeignKey = false;
				colvarMostrarComo.IsReadOnly = false;
				colvarMostrarComo.DefaultSetting = @"";
				colvarMostrarComo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMostrarComo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IdTipoCausacion")]
		[Bindable(true)]
		public int IdTipoCausacion 
		{
			get { return GetColumnValue<int>(Columns.IdTipoCausacion); }
			set { SetColumnValue(Columns.IdTipoCausacion, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		  
		[XmlAttribute("MostrarComo")]
		[Bindable(true)]
		public string MostrarComo 
		{
			get { return GetColumnValue<string>(Columns.MostrarComo); }
			set { SetColumnValue(Columns.MostrarComo, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvCgunoDetallesCausacionCollection InvCgunoDetallesCausacionRecords()
		{
			return new Big.Helper.Domain.Generated.InvCgunoDetallesCausacionCollection().Where(InvCgunoDetallesCausacion.Columns.IdCampoTipoCausacion, Id).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoMasTiposCausacion ActiveRecord object related to this InvCgunoMasCamposTipoCausacion
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoMasTiposCausacion InvCgunoMasTiposCausacion
		{
			get { return Big.Helper.Domain.Generated.InvCgunoMasTiposCausacion.FetchByID(this.IdTipoCausacion); }
			set { SetColumnValue("ID_TIPO_CAUSACION", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varId,int varIdTipoCausacion,string varDescripcion,string varMostrarComo)
		{
			InvCgunoMasCamposTipoCausacion item = new InvCgunoMasCamposTipoCausacion();
			
			item.Id = varId;
			
			item.IdTipoCausacion = varIdTipoCausacion;
			
			item.Descripcion = varDescripcion;
			
			item.MostrarComo = varMostrarComo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varIdTipoCausacion,string varDescripcion,string varMostrarComo)
		{
			InvCgunoMasCamposTipoCausacion item = new InvCgunoMasCamposTipoCausacion();
			
				item.Id = varId;
			
				item.IdTipoCausacion = varIdTipoCausacion;
			
				item.Descripcion = varDescripcion;
			
				item.MostrarComo = varMostrarComo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoCausacionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn MostrarComoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string IdTipoCausacion = @"ID_TIPO_CAUSACION";
			 public static string Descripcion = @"DESCRIPCION";
			 public static string MostrarComo = @"MOSTRAR_COMO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
