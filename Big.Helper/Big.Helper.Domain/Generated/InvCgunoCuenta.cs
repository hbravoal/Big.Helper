using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoCuenta class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoCuentaCollection : ActiveList<InvCgunoCuenta, InvCgunoCuentaCollection>
	{	   
		public InvCgunoCuentaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoCuentaCollection</returns>
		public InvCgunoCuentaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoCuenta o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_CUENTAS table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoCuenta : ActiveRecord<InvCgunoCuenta>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoCuenta()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoCuenta(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoCuenta(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoCuenta(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_CUENTAS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.AnsiString;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarNumeroCuenta = new TableSchema.TableColumn(schema);
				colvarNumeroCuenta.ColumnName = "NUMERO_CUENTA";
				colvarNumeroCuenta.DataType = DbType.String;
				colvarNumeroCuenta.MaxLength = 20;
				colvarNumeroCuenta.AutoIncrement = false;
				colvarNumeroCuenta.IsNullable = false;
				colvarNumeroCuenta.IsPrimaryKey = false;
				colvarNumeroCuenta.IsForeignKey = false;
				colvarNumeroCuenta.IsReadOnly = false;
				colvarNumeroCuenta.DefaultSetting = @"";
				colvarNumeroCuenta.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroCuenta);
				
				TableSchema.TableColumn colvarNombreCuenta = new TableSchema.TableColumn(schema);
				colvarNombreCuenta.ColumnName = "NOMBRE_CUENTA";
				colvarNombreCuenta.DataType = DbType.String;
				colvarNombreCuenta.MaxLength = 100;
				colvarNombreCuenta.AutoIncrement = false;
				colvarNombreCuenta.IsNullable = false;
				colvarNombreCuenta.IsPrimaryKey = false;
				colvarNombreCuenta.IsForeignKey = false;
				colvarNombreCuenta.IsReadOnly = false;
				colvarNombreCuenta.DefaultSetting = @"";
				colvarNombreCuenta.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreCuenta);
				
				TableSchema.TableColumn colvarRequiereTerceros = new TableSchema.TableColumn(schema);
				colvarRequiereTerceros.ColumnName = "REQUIERE_TERCEROS";
				colvarRequiereTerceros.DataType = DbType.Boolean;
				colvarRequiereTerceros.MaxLength = 0;
				colvarRequiereTerceros.AutoIncrement = false;
				colvarRequiereTerceros.IsNullable = false;
				colvarRequiereTerceros.IsPrimaryKey = false;
				colvarRequiereTerceros.IsForeignKey = false;
				colvarRequiereTerceros.IsReadOnly = false;
				colvarRequiereTerceros.DefaultSetting = @"";
				colvarRequiereTerceros.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequiereTerceros);
				
				TableSchema.TableColumn colvarRequiereCentroCostos = new TableSchema.TableColumn(schema);
				colvarRequiereCentroCostos.ColumnName = "REQUIERE_CENTRO_COSTOS";
				colvarRequiereCentroCostos.DataType = DbType.Boolean;
				colvarRequiereCentroCostos.MaxLength = 0;
				colvarRequiereCentroCostos.AutoIncrement = false;
				colvarRequiereCentroCostos.IsNullable = false;
				colvarRequiereCentroCostos.IsPrimaryKey = false;
				colvarRequiereCentroCostos.IsForeignKey = false;
				colvarRequiereCentroCostos.IsReadOnly = false;
				colvarRequiereCentroCostos.DefaultSetting = @"";
				colvarRequiereCentroCostos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequiereCentroCostos);
				
				TableSchema.TableColumn colvarRequiereReferencia = new TableSchema.TableColumn(schema);
				colvarRequiereReferencia.ColumnName = "REQUIERE_REFERENCIA";
				colvarRequiereReferencia.DataType = DbType.Boolean;
				colvarRequiereReferencia.MaxLength = 0;
				colvarRequiereReferencia.AutoIncrement = false;
				colvarRequiereReferencia.IsNullable = false;
				colvarRequiereReferencia.IsPrimaryKey = false;
				colvarRequiereReferencia.IsForeignKey = false;
				colvarRequiereReferencia.IsReadOnly = false;
				colvarRequiereReferencia.DefaultSetting = @"";
				colvarRequiereReferencia.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRequiereReferencia);
				
				TableSchema.TableColumn colvarUtilizaBase = new TableSchema.TableColumn(schema);
				colvarUtilizaBase.ColumnName = "UTILIZA_BASE";
				colvarUtilizaBase.DataType = DbType.Boolean;
				colvarUtilizaBase.MaxLength = 0;
				colvarUtilizaBase.AutoIncrement = false;
				colvarUtilizaBase.IsNullable = false;
				colvarUtilizaBase.IsPrimaryKey = false;
				colvarUtilizaBase.IsForeignKey = false;
				colvarUtilizaBase.IsReadOnly = false;
				colvarUtilizaBase.DefaultSetting = @"";
				colvarUtilizaBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUtilizaBase);
				
				TableSchema.TableColumn colvarBaseX = new TableSchema.TableColumn(schema);
				colvarBaseX.ColumnName = "BASE";
				colvarBaseX.DataType = DbType.Decimal;
				colvarBaseX.MaxLength = 0;
				colvarBaseX.AutoIncrement = false;
				colvarBaseX.IsNullable = true;
				colvarBaseX.IsPrimaryKey = false;
				colvarBaseX.IsForeignKey = false;
				colvarBaseX.IsReadOnly = false;
				colvarBaseX.DefaultSetting = @"";
				colvarBaseX.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBaseX);
				
				TableSchema.TableColumn colvarFechaCreacion = new TableSchema.TableColumn(schema);
				colvarFechaCreacion.ColumnName = "FECHA_CREACION";
				colvarFechaCreacion.DataType = DbType.DateTime;
				colvarFechaCreacion.MaxLength = 0;
				colvarFechaCreacion.AutoIncrement = false;
				colvarFechaCreacion.IsNullable = false;
				colvarFechaCreacion.IsPrimaryKey = false;
				colvarFechaCreacion.IsForeignKey = false;
				colvarFechaCreacion.IsReadOnly = false;
				colvarFechaCreacion.DefaultSetting = @"";
				colvarFechaCreacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaCreacion);
				
				TableSchema.TableColumn colvarIdCentroUtilidad = new TableSchema.TableColumn(schema);
				colvarIdCentroUtilidad.ColumnName = "ID_CENTRO_UTILIDAD";
				colvarIdCentroUtilidad.DataType = DbType.Int32;
				colvarIdCentroUtilidad.MaxLength = 0;
				colvarIdCentroUtilidad.AutoIncrement = false;
				colvarIdCentroUtilidad.IsNullable = true;
				colvarIdCentroUtilidad.IsPrimaryKey = false;
				colvarIdCentroUtilidad.IsForeignKey = true;
				colvarIdCentroUtilidad.IsReadOnly = false;
				colvarIdCentroUtilidad.DefaultSetting = @"";
				
					colvarIdCentroUtilidad.ForeignKeyTableName = "INV_CGUNO_CENTRO_UTILIDAD";
				schema.Columns.Add(colvarIdCentroUtilidad);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_CUENTAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("NumeroCuenta")]
		[Bindable(true)]
		public string NumeroCuenta 
		{
			get { return GetColumnValue<string>(Columns.NumeroCuenta); }
			set { SetColumnValue(Columns.NumeroCuenta, value); }
		}
		  
		[XmlAttribute("NombreCuenta")]
		[Bindable(true)]
		public string NombreCuenta 
		{
			get { return GetColumnValue<string>(Columns.NombreCuenta); }
			set { SetColumnValue(Columns.NombreCuenta, value); }
		}
		  
		[XmlAttribute("RequiereTerceros")]
		[Bindable(true)]
		public bool RequiereTerceros 
		{
			get { return GetColumnValue<bool>(Columns.RequiereTerceros); }
			set { SetColumnValue(Columns.RequiereTerceros, value); }
		}
		  
		[XmlAttribute("RequiereCentroCostos")]
		[Bindable(true)]
		public bool RequiereCentroCostos 
		{
			get { return GetColumnValue<bool>(Columns.RequiereCentroCostos); }
			set { SetColumnValue(Columns.RequiereCentroCostos, value); }
		}
		  
		[XmlAttribute("RequiereReferencia")]
		[Bindable(true)]
		public bool RequiereReferencia 
		{
			get { return GetColumnValue<bool>(Columns.RequiereReferencia); }
			set { SetColumnValue(Columns.RequiereReferencia, value); }
		}
		  
		[XmlAttribute("UtilizaBase")]
		[Bindable(true)]
		public bool UtilizaBase 
		{
			get { return GetColumnValue<bool>(Columns.UtilizaBase); }
			set { SetColumnValue(Columns.UtilizaBase, value); }
		}
		  
		[XmlAttribute("BaseX")]
		[Bindable(true)]
		public decimal? BaseX 
		{
			get { return GetColumnValue<decimal?>(Columns.BaseX); }
			set { SetColumnValue(Columns.BaseX, value); }
		}
		  
		[XmlAttribute("FechaCreacion")]
		[Bindable(true)]
		public DateTime FechaCreacion 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaCreacion); }
			set { SetColumnValue(Columns.FechaCreacion, value); }
		}
		  
		[XmlAttribute("IdCentroUtilidad")]
		[Bindable(true)]
		public int? IdCentroUtilidad 
		{
			get { return GetColumnValue<int?>(Columns.IdCentroUtilidad); }
			set { SetColumnValue(Columns.IdCentroUtilidad, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvCgunoDetallesCausacionCollection InvCgunoDetallesCausacionRecords()
		{
			return new Big.Helper.Domain.Generated.InvCgunoDetallesCausacionCollection().Where(InvCgunoDetallesCausacion.Columns.IdCuenta, Id).Load();
		}
		public Big.Helper.Domain.Generated.InvCgunoDetallesCausacionesPeriodoCollection InvCgunoDetallesCausacionesPeriodoRecords()
		{
			return new Big.Helper.Domain.Generated.InvCgunoDetallesCausacionesPeriodoCollection().Where(InvCgunoDetallesCausacionesPeriodo.Columns.IdCuenta, Id).Load();
		}
		public Big.Helper.Domain.Generated.InvProveedoreCollection InvProveedores()
		{
			return new Big.Helper.Domain.Generated.InvProveedoreCollection().Where(InvProveedore.Columns.IdCuentaCostosVenta, Id).Load();
		}
		public Big.Helper.Domain.Generated.InvProveedoreCollection InvProveedoresFromInvCgunoCuenta()
		{
			return new Big.Helper.Domain.Generated.InvProveedoreCollection().Where(InvProveedore.Columns.IdCuentaInventarios, Id).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoCentroUtilidad ActiveRecord object related to this InvCgunoCuenta
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCentroUtilidad InvCgunoCentroUtilidad
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCentroUtilidad.FetchByID(this.IdCentroUtilidad); }
			set { SetColumnValue("ID_CENTRO_UTILIDAD", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,string varNumeroCuenta,string varNombreCuenta,bool varRequiereTerceros,bool varRequiereCentroCostos,bool varRequiereReferencia,bool varUtilizaBase,decimal? varBaseX,DateTime varFechaCreacion,int? varIdCentroUtilidad)
		{
			InvCgunoCuenta item = new InvCgunoCuenta();
			
			item.Guid = varGuid;
			
			item.NumeroCuenta = varNumeroCuenta;
			
			item.NombreCuenta = varNombreCuenta;
			
			item.RequiereTerceros = varRequiereTerceros;
			
			item.RequiereCentroCostos = varRequiereCentroCostos;
			
			item.RequiereReferencia = varRequiereReferencia;
			
			item.UtilizaBase = varUtilizaBase;
			
			item.BaseX = varBaseX;
			
			item.FechaCreacion = varFechaCreacion;
			
			item.IdCentroUtilidad = varIdCentroUtilidad;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,int varId,string varNumeroCuenta,string varNombreCuenta,bool varRequiereTerceros,bool varRequiereCentroCostos,bool varRequiereReferencia,bool varUtilizaBase,decimal? varBaseX,DateTime varFechaCreacion,int? varIdCentroUtilidad)
		{
			InvCgunoCuenta item = new InvCgunoCuenta();
			
				item.Guid = varGuid;
			
				item.Id = varId;
			
				item.NumeroCuenta = varNumeroCuenta;
			
				item.NombreCuenta = varNombreCuenta;
			
				item.RequiereTerceros = varRequiereTerceros;
			
				item.RequiereCentroCostos = varRequiereCentroCostos;
			
				item.RequiereReferencia = varRequiereReferencia;
			
				item.UtilizaBase = varUtilizaBase;
			
				item.BaseX = varBaseX;
			
				item.FechaCreacion = varFechaCreacion;
			
				item.IdCentroUtilidad = varIdCentroUtilidad;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroCuentaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreCuentaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn RequiereTercerosColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn RequiereCentroCostosColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn RequiereReferenciaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UtilizaBaseColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn BaseXColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaCreacionColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCentroUtilidadColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string Id = @"ID";
			 public static string NumeroCuenta = @"NUMERO_CUENTA";
			 public static string NombreCuenta = @"NOMBRE_CUENTA";
			 public static string RequiereTerceros = @"REQUIERE_TERCEROS";
			 public static string RequiereCentroCostos = @"REQUIERE_CENTRO_COSTOS";
			 public static string RequiereReferencia = @"REQUIERE_REFERENCIA";
			 public static string UtilizaBase = @"UTILIZA_BASE";
			 public static string BaseX = @"BASE";
			 public static string FechaCreacion = @"FECHA_CREACION";
			 public static string IdCentroUtilidad = @"ID_CENTRO_UTILIDAD";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
