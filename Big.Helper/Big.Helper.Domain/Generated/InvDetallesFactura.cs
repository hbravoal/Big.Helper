using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvDetallesFactura class.
	/// </summary>
    [Serializable]
	public partial class InvDetallesFacturaCollection : ActiveList<InvDetallesFactura, InvDetallesFacturaCollection>
	{	   
		public InvDetallesFacturaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvDetallesFacturaCollection</returns>
		public InvDetallesFacturaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvDetallesFactura o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_DETALLES_FACTURA table.
	/// </summary>
	[Serializable]
	public partial class InvDetallesFactura : ActiveRecord<InvDetallesFactura>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvDetallesFactura()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvDetallesFactura(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvDetallesFactura(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvDetallesFactura(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_DETALLES_FACTURA", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidDetalleFactura = new TableSchema.TableColumn(schema);
				colvarGuidDetalleFactura.ColumnName = "GUID_DETALLE_FACTURA";
				colvarGuidDetalleFactura.DataType = DbType.String;
				colvarGuidDetalleFactura.MaxLength = 36;
				colvarGuidDetalleFactura.AutoIncrement = false;
				colvarGuidDetalleFactura.IsNullable = false;
				colvarGuidDetalleFactura.IsPrimaryKey = true;
				colvarGuidDetalleFactura.IsForeignKey = false;
				colvarGuidDetalleFactura.IsReadOnly = false;
				colvarGuidDetalleFactura.DefaultSetting = @"";
				colvarGuidDetalleFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidDetalleFactura);
				
				TableSchema.TableColumn colvarNumeroItem = new TableSchema.TableColumn(schema);
				colvarNumeroItem.ColumnName = "NUMERO_ITEM";
				colvarNumeroItem.DataType = DbType.Int32;
				colvarNumeroItem.MaxLength = 0;
				colvarNumeroItem.AutoIncrement = false;
				colvarNumeroItem.IsNullable = false;
				colvarNumeroItem.IsPrimaryKey = false;
				colvarNumeroItem.IsForeignKey = false;
				colvarNumeroItem.IsReadOnly = false;
				colvarNumeroItem.DefaultSetting = @"";
				colvarNumeroItem.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroItem);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarValorBase = new TableSchema.TableColumn(schema);
				colvarValorBase.ColumnName = "VALOR_BASE";
				colvarValorBase.DataType = DbType.Decimal;
				colvarValorBase.MaxLength = 0;
				colvarValorBase.AutoIncrement = false;
				colvarValorBase.IsNullable = false;
				colvarValorBase.IsPrimaryKey = false;
				colvarValorBase.IsForeignKey = false;
				colvarValorBase.IsReadOnly = false;
				colvarValorBase.DefaultSetting = @"";
				colvarValorBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorBase);
				
				TableSchema.TableColumn colvarEnvioBase = new TableSchema.TableColumn(schema);
				colvarEnvioBase.ColumnName = "ENVIO_BASE";
				colvarEnvioBase.DataType = DbType.Decimal;
				colvarEnvioBase.MaxLength = 0;
				colvarEnvioBase.AutoIncrement = false;
				colvarEnvioBase.IsNullable = false;
				colvarEnvioBase.IsPrimaryKey = false;
				colvarEnvioBase.IsForeignKey = false;
				colvarEnvioBase.IsReadOnly = false;
				colvarEnvioBase.DefaultSetting = @"";
				colvarEnvioBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnvioBase);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarGuidFactura = new TableSchema.TableColumn(schema);
				colvarGuidFactura.ColumnName = "GUID_FACTURA";
				colvarGuidFactura.DataType = DbType.String;
				colvarGuidFactura.MaxLength = 36;
				colvarGuidFactura.AutoIncrement = false;
				colvarGuidFactura.IsNullable = false;
				colvarGuidFactura.IsPrimaryKey = false;
				colvarGuidFactura.IsForeignKey = false;
				colvarGuidFactura.IsReadOnly = false;
				colvarGuidFactura.DefaultSetting = @"";
				colvarGuidFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidFactura);
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = false;
				colvarGuidReferenciaProducto.IsForeignKey = true;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				
					colvarGuidReferenciaProducto.ForeignKeyTableName = "INV_REFERENCIAS_PRODUCTO";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				TableSchema.TableColumn colvarFactorCompraPuntos = new TableSchema.TableColumn(schema);
				colvarFactorCompraPuntos.ColumnName = "FACTOR_COMPRA_PUNTOS";
				colvarFactorCompraPuntos.DataType = DbType.Decimal;
				colvarFactorCompraPuntos.MaxLength = 0;
				colvarFactorCompraPuntos.AutoIncrement = false;
				colvarFactorCompraPuntos.IsNullable = false;
				colvarFactorCompraPuntos.IsPrimaryKey = false;
				colvarFactorCompraPuntos.IsForeignKey = false;
				colvarFactorCompraPuntos.IsReadOnly = false;
				colvarFactorCompraPuntos.DefaultSetting = @"";
				colvarFactorCompraPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFactorCompraPuntos);
				
				TableSchema.TableColumn colvarDescargoEnInventario = new TableSchema.TableColumn(schema);
				colvarDescargoEnInventario.ColumnName = "DESCARGO_EN_INVENTARIO";
				colvarDescargoEnInventario.DataType = DbType.Boolean;
				colvarDescargoEnInventario.MaxLength = 0;
				colvarDescargoEnInventario.AutoIncrement = false;
				colvarDescargoEnInventario.IsNullable = true;
				colvarDescargoEnInventario.IsPrimaryKey = false;
				colvarDescargoEnInventario.IsForeignKey = false;
				colvarDescargoEnInventario.IsReadOnly = false;
				colvarDescargoEnInventario.DefaultSetting = @"";
				colvarDescargoEnInventario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescargoEnInventario);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_DETALLES_FACTURA",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidDetalleFactura")]
		[Bindable(true)]
		public string GuidDetalleFactura 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleFactura); }
			set { SetColumnValue(Columns.GuidDetalleFactura, value); }
		}
		  
		[XmlAttribute("NumeroItem")]
		[Bindable(true)]
		public int NumeroItem 
		{
			get { return GetColumnValue<int>(Columns.NumeroItem); }
			set { SetColumnValue(Columns.NumeroItem, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("ValorBase")]
		[Bindable(true)]
		public decimal ValorBase 
		{
			get { return GetColumnValue<decimal>(Columns.ValorBase); }
			set { SetColumnValue(Columns.ValorBase, value); }
		}
		  
		[XmlAttribute("EnvioBase")]
		[Bindable(true)]
		public decimal EnvioBase 
		{
			get { return GetColumnValue<decimal>(Columns.EnvioBase); }
			set { SetColumnValue(Columns.EnvioBase, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("GuidFactura")]
		[Bindable(true)]
		public string GuidFactura 
		{
			get { return GetColumnValue<string>(Columns.GuidFactura); }
			set { SetColumnValue(Columns.GuidFactura, value); }
		}
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		  
		[XmlAttribute("FactorCompraPuntos")]
		[Bindable(true)]
		public decimal FactorCompraPuntos 
		{
			get { return GetColumnValue<decimal>(Columns.FactorCompraPuntos); }
			set { SetColumnValue(Columns.FactorCompraPuntos, value); }
		}
		  
		[XmlAttribute("DescargoEnInventario")]
		[Bindable(true)]
		public bool? DescargoEnInventario 
		{
			get { return GetColumnValue<bool?>(Columns.DescargoEnInventario); }
			set { SetColumnValue(Columns.DescargoEnInventario, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvHistoricoBodegaCollection InvHistoricoBodegaRecords()
		{
			return new Big.Helper.Domain.Generated.InvHistoricoBodegaCollection().Where(InvHistoricoBodega.Columns.GuidDetalleFactura, GuidDetalleFactura).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvReferenciasProducto ActiveRecord object related to this InvDetallesFactura
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvReferenciasProducto InvReferenciasProducto
		{
			get { return Big.Helper.Domain.Generated.InvReferenciasProducto.FetchByID(this.GuidReferenciaProducto); }
			set { SetColumnValue("GUID_REFERENCIA_PRODUCTO", value.GuidReferenciaProducto); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidDetalleFactura,int varNumeroItem,int varCantidad,decimal varValorBase,decimal varEnvioBase,decimal varIva,string varGuidFactura,string varGuidReferenciaProducto,decimal varFactorCompraPuntos,bool? varDescargoEnInventario)
		{
			InvDetallesFactura item = new InvDetallesFactura();
			
			item.GuidDetalleFactura = varGuidDetalleFactura;
			
			item.NumeroItem = varNumeroItem;
			
			item.Cantidad = varCantidad;
			
			item.ValorBase = varValorBase;
			
			item.EnvioBase = varEnvioBase;
			
			item.Iva = varIva;
			
			item.GuidFactura = varGuidFactura;
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.FactorCompraPuntos = varFactorCompraPuntos;
			
			item.DescargoEnInventario = varDescargoEnInventario;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidDetalleFactura,int varNumeroItem,int varCantidad,decimal varValorBase,decimal varEnvioBase,decimal varIva,string varGuidFactura,string varGuidReferenciaProducto,decimal varFactorCompraPuntos,bool? varDescargoEnInventario)
		{
			InvDetallesFactura item = new InvDetallesFactura();
			
				item.GuidDetalleFactura = varGuidDetalleFactura;
			
				item.NumeroItem = varNumeroItem;
			
				item.Cantidad = varCantidad;
			
				item.ValorBase = varValorBase;
			
				item.EnvioBase = varEnvioBase;
			
				item.Iva = varIva;
			
				item.GuidFactura = varGuidFactura;
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
				item.FactorCompraPuntos = varFactorCompraPuntos;
			
				item.DescargoEnInventario = varDescargoEnInventario;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidDetalleFacturaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroItemColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorBaseColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EnvioBaseColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidFacturaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FactorCompraPuntosColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn DescargoEnInventarioColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidDetalleFactura = @"GUID_DETALLE_FACTURA";
			 public static string NumeroItem = @"NUMERO_ITEM";
			 public static string Cantidad = @"CANTIDAD";
			 public static string ValorBase = @"VALOR_BASE";
			 public static string EnvioBase = @"ENVIO_BASE";
			 public static string Iva = @"IVA";
			 public static string GuidFactura = @"GUID_FACTURA";
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
			 public static string FactorCompraPuntos = @"FACTOR_COMPRA_PUNTOS";
			 public static string DescargoEnInventario = @"DESCARGO_EN_INVENTARIO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
