using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_PRECIOS_BASE_PROGRAMA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvPreciosBaseProgramaController
    {
        // Preload our schema..
        InvPreciosBasePrograma thisSchemaLoad = new InvPreciosBasePrograma();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvPreciosBaseProgramaCollection FetchAll()
        {
            InvPreciosBaseProgramaCollection coll = new InvPreciosBaseProgramaCollection();
            Query qry = new Query(InvPreciosBasePrograma.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPreciosBaseProgramaCollection FetchByID(object GuidPrecioProducto)
        {
            InvPreciosBaseProgramaCollection coll = new InvPreciosBaseProgramaCollection().Where("GUID_PRECIO_PRODUCTO", GuidPrecioProducto).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPreciosBaseProgramaCollection FetchByQuery(Query qry)
        {
            InvPreciosBaseProgramaCollection coll = new InvPreciosBaseProgramaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidPrecioProducto)
        {
            return (InvPreciosBasePrograma.Delete(GuidPrecioProducto) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidPrecioProducto)
        {
            return (InvPreciosBasePrograma.Destroy(GuidPrecioProducto) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidPrecioProducto,decimal PrecioVentaBase,decimal CostoEnvioBase,string GuidProducto,int IdPrograma,int IdTipoDelivery,int IdMarca,decimal? ValorAsegurado,int? IdCiudad,string Direccion)
	    {
		    InvPreciosBasePrograma item = new InvPreciosBasePrograma();
		    
            item.GuidPrecioProducto = GuidPrecioProducto;
            
            item.PrecioVentaBase = PrecioVentaBase;
            
            item.CostoEnvioBase = CostoEnvioBase;
            
            item.GuidProducto = GuidProducto;
            
            item.IdPrograma = IdPrograma;
            
            item.IdTipoDelivery = IdTipoDelivery;
            
            item.IdMarca = IdMarca;
            
            item.ValorAsegurado = ValorAsegurado;
            
            item.IdCiudad = IdCiudad;
            
            item.Direccion = Direccion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidPrecioProducto,decimal PrecioVentaBase,decimal CostoEnvioBase,string GuidProducto,int IdPrograma,int IdTipoDelivery,int IdMarca,decimal? ValorAsegurado,int? IdCiudad,string Direccion)
	    {
		    InvPreciosBasePrograma item = new InvPreciosBasePrograma();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidPrecioProducto = GuidPrecioProducto;
				
			item.PrecioVentaBase = PrecioVentaBase;
				
			item.CostoEnvioBase = CostoEnvioBase;
				
			item.GuidProducto = GuidProducto;
				
			item.IdPrograma = IdPrograma;
				
			item.IdTipoDelivery = IdTipoDelivery;
				
			item.IdMarca = IdMarca;
				
			item.ValorAsegurado = ValorAsegurado;
				
			item.IdCiudad = IdCiudad;
				
			item.Direccion = Direccion;
				
	        item.Save(UserName);
	    }
    }
}
