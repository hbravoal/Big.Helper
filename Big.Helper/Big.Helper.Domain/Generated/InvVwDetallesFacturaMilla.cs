using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwDetallesFacturaMilla class.
    /// </summary>
    [Serializable]
    public partial class InvVwDetallesFacturaMillaCollection : ReadOnlyList<InvVwDetallesFacturaMilla, InvVwDetallesFacturaMillaCollection>
    {        
        public InvVwDetallesFacturaMillaCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_DETALLES_FACTURA_MILLAS view.
    /// </summary>
    [Serializable]
    public partial class InvVwDetallesFacturaMilla : ReadOnlyRecord<InvVwDetallesFacturaMilla>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_DETALLES_FACTURA_MILLAS", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarExpr1 = new TableSchema.TableColumn(schema);
                colvarExpr1.ColumnName = "Expr1";
                colvarExpr1.DataType = DbType.AnsiString;
                colvarExpr1.MaxLength = 32;
                colvarExpr1.AutoIncrement = false;
                colvarExpr1.IsNullable = true;
                colvarExpr1.IsPrimaryKey = false;
                colvarExpr1.IsForeignKey = false;
                colvarExpr1.IsReadOnly = false;
                
                schema.Columns.Add(colvarExpr1);
                
                TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
                colvarCodigo.ColumnName = "CODIGO";
                colvarCodigo.DataType = DbType.AnsiString;
                colvarCodigo.MaxLength = 15;
                colvarCodigo.AutoIncrement = false;
                colvarCodigo.IsNullable = false;
                colvarCodigo.IsPrimaryKey = false;
                colvarCodigo.IsForeignKey = false;
                colvarCodigo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigo);
                
                TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
                colvarCantidad.ColumnName = "CANTIDAD";
                colvarCantidad.DataType = DbType.Int32;
                colvarCantidad.MaxLength = 0;
                colvarCantidad.AutoIncrement = false;
                colvarCantidad.IsNullable = false;
                colvarCantidad.IsPrimaryKey = false;
                colvarCantidad.IsForeignKey = false;
                colvarCantidad.IsReadOnly = false;
                
                schema.Columns.Add(colvarCantidad);
                
                TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
                colvarCostoBase.ColumnName = "COSTO_BASE";
                colvarCostoBase.DataType = DbType.Decimal;
                colvarCostoBase.MaxLength = 0;
                colvarCostoBase.AutoIncrement = false;
                colvarCostoBase.IsNullable = false;
                colvarCostoBase.IsPrimaryKey = false;
                colvarCostoBase.IsForeignKey = false;
                colvarCostoBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarCostoBase);
                
                TableSchema.TableColumn colvarValorBasePuntosTotal = new TableSchema.TableColumn(schema);
                colvarValorBasePuntosTotal.ColumnName = "VALOR_BASE_PUNTOS_TOTAL";
                colvarValorBasePuntosTotal.DataType = DbType.Decimal;
                colvarValorBasePuntosTotal.MaxLength = 0;
                colvarValorBasePuntosTotal.AutoIncrement = false;
                colvarValorBasePuntosTotal.IsNullable = false;
                colvarValorBasePuntosTotal.IsPrimaryKey = false;
                colvarValorBasePuntosTotal.IsForeignKey = false;
                colvarValorBasePuntosTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarValorBasePuntosTotal);
                
                TableSchema.TableColumn colvarNumeroItem = new TableSchema.TableColumn(schema);
                colvarNumeroItem.ColumnName = "NUMERO_ITEM";
                colvarNumeroItem.DataType = DbType.Int32;
                colvarNumeroItem.MaxLength = 0;
                colvarNumeroItem.AutoIncrement = false;
                colvarNumeroItem.IsNullable = false;
                colvarNumeroItem.IsPrimaryKey = false;
                colvarNumeroItem.IsForeignKey = false;
                colvarNumeroItem.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumeroItem);
                
                TableSchema.TableColumn colvarEstado = new TableSchema.TableColumn(schema);
                colvarEstado.ColumnName = "ESTADO";
                colvarEstado.DataType = DbType.Int32;
                colvarEstado.MaxLength = 0;
                colvarEstado.AutoIncrement = false;
                colvarEstado.IsNullable = false;
                colvarEstado.IsPrimaryKey = false;
                colvarEstado.IsForeignKey = false;
                colvarEstado.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstado);
                
                TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
                colvarIdPrograma.ColumnName = "ID_PROGRAMA";
                colvarIdPrograma.DataType = DbType.Int32;
                colvarIdPrograma.MaxLength = 0;
                colvarIdPrograma.AutoIncrement = false;
                colvarIdPrograma.IsNullable = false;
                colvarIdPrograma.IsPrimaryKey = false;
                colvarIdPrograma.IsForeignKey = false;
                colvarIdPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdPrograma);
                
                TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
                colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
                colvarNumeroFactura.DataType = DbType.Int32;
                colvarNumeroFactura.MaxLength = 0;
                colvarNumeroFactura.AutoIncrement = false;
                colvarNumeroFactura.IsNullable = true;
                colvarNumeroFactura.IsPrimaryKey = false;
                colvarNumeroFactura.IsForeignKey = false;
                colvarNumeroFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumeroFactura);
                
                TableSchema.TableColumn colvarFechaFactura = new TableSchema.TableColumn(schema);
                colvarFechaFactura.ColumnName = "FECHA_FACTURA";
                colvarFechaFactura.DataType = DbType.DateTime;
                colvarFechaFactura.MaxLength = 0;
                colvarFechaFactura.AutoIncrement = false;
                colvarFechaFactura.IsNullable = true;
                colvarFechaFactura.IsPrimaryKey = false;
                colvarFechaFactura.IsForeignKey = false;
                colvarFechaFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaFactura);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_DETALLES_FACTURA_MILLAS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwDetallesFacturaMilla()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwDetallesFacturaMilla(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwDetallesFacturaMilla(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwDetallesFacturaMilla(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Expr1")]
        [Bindable(true)]
        public string Expr1 
	    {
		    get
		    {
			    return GetColumnValue<string>("Expr1");
		    }
            set 
		    {
			    SetColumnValue("Expr1", value);
            }
        }
	      
        [XmlAttribute("Codigo")]
        [Bindable(true)]
        public string Codigo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO");
		    }
            set 
		    {
			    SetColumnValue("CODIGO", value);
            }
        }
	      
        [XmlAttribute("Cantidad")]
        [Bindable(true)]
        public int Cantidad 
	    {
		    get
		    {
			    return GetColumnValue<int>("CANTIDAD");
		    }
            set 
		    {
			    SetColumnValue("CANTIDAD", value);
            }
        }
	      
        [XmlAttribute("CostoBase")]
        [Bindable(true)]
        public decimal CostoBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("COSTO_BASE");
		    }
            set 
		    {
			    SetColumnValue("COSTO_BASE", value);
            }
        }
	      
        [XmlAttribute("ValorBasePuntosTotal")]
        [Bindable(true)]
        public decimal ValorBasePuntosTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("VALOR_BASE_PUNTOS_TOTAL");
		    }
            set 
		    {
			    SetColumnValue("VALOR_BASE_PUNTOS_TOTAL", value);
            }
        }
	      
        [XmlAttribute("NumeroItem")]
        [Bindable(true)]
        public int NumeroItem 
	    {
		    get
		    {
			    return GetColumnValue<int>("NUMERO_ITEM");
		    }
            set 
		    {
			    SetColumnValue("NUMERO_ITEM", value);
            }
        }
	      
        [XmlAttribute("Estado")]
        [Bindable(true)]
        public int Estado 
	    {
		    get
		    {
			    return GetColumnValue<int>("ESTADO");
		    }
            set 
		    {
			    SetColumnValue("ESTADO", value);
            }
        }
	      
        [XmlAttribute("IdPrograma")]
        [Bindable(true)]
        public int IdPrograma 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("ID_PROGRAMA", value);
            }
        }
	      
        [XmlAttribute("NumeroFactura")]
        [Bindable(true)]
        public int? NumeroFactura 
	    {
		    get
		    {
			    return GetColumnValue<int?>("NUMERO_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("NUMERO_FACTURA", value);
            }
        }
	      
        [XmlAttribute("FechaFactura")]
        [Bindable(true)]
        public DateTime? FechaFactura 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("FECHA_FACTURA", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Expr1 = @"Expr1";
            
            public static string Codigo = @"CODIGO";
            
            public static string Cantidad = @"CANTIDAD";
            
            public static string CostoBase = @"COSTO_BASE";
            
            public static string ValorBasePuntosTotal = @"VALOR_BASE_PUNTOS_TOTAL";
            
            public static string NumeroItem = @"NUMERO_ITEM";
            
            public static string Estado = @"ESTADO";
            
            public static string IdPrograma = @"ID_PROGRAMA";
            
            public static string NumeroFactura = @"NUMERO_FACTURA";
            
            public static string FechaFactura = @"FECHA_FACTURA";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
