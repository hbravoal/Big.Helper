using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwCiudade class.
    /// </summary>
    [Serializable]
    public partial class InvVwCiudadeCollection : ReadOnlyList<InvVwCiudade, InvVwCiudadeCollection>
    {        
        public InvVwCiudadeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_CIUDADES view.
    /// </summary>
    [Serializable]
    public partial class InvVwCiudade : ReadOnlyRecord<InvVwCiudade>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_CIUDADES", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
                colvarIdCiudad.ColumnName = "ID_CIUDAD";
                colvarIdCiudad.DataType = DbType.Int32;
                colvarIdCiudad.MaxLength = 0;
                colvarIdCiudad.AutoIncrement = false;
                colvarIdCiudad.IsNullable = false;
                colvarIdCiudad.IsPrimaryKey = false;
                colvarIdCiudad.IsForeignKey = false;
                colvarIdCiudad.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCiudad);
                
                TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
                colvarNombre.ColumnName = "NOMBRE";
                colvarNombre.DataType = DbType.String;
                colvarNombre.MaxLength = 103;
                colvarNombre.AutoIncrement = false;
                colvarNombre.IsNullable = true;
                colvarNombre.IsPrimaryKey = false;
                colvarNombre.IsForeignKey = false;
                colvarNombre.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombre);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_CIUDADES",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwCiudade()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwCiudade(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwCiudade(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwCiudade(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdCiudad")]
        [Bindable(true)]
        public int IdCiudad 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_CIUDAD");
		    }
            set 
		    {
			    SetColumnValue("ID_CIUDAD", value);
            }
        }
	      
        [XmlAttribute("Nombre")]
        [Bindable(true)]
        public string Nombre 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdCiudad = @"ID_CIUDAD";
            
            public static string Nombre = @"NOMBRE";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
