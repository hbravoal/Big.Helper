using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvProducto class.
	/// </summary>
    [Serializable]
	public partial class InvProductoCollection : ActiveList<InvProducto, InvProductoCollection>
	{	   
		public InvProductoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvProductoCollection</returns>
		public InvProductoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvProducto o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PRODUCTOS table.
	/// </summary>
	[Serializable]
	public partial class InvProducto : ActiveRecord<InvProducto>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvProducto()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvProducto(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvProducto(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvProducto(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PRODUCTOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.String;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
				colvarCodigo.ColumnName = "CODIGO";
				colvarCodigo.DataType = DbType.AnsiString;
				colvarCodigo.MaxLength = 15;
				colvarCodigo.AutoIncrement = false;
				colvarCodigo.IsNullable = false;
				colvarCodigo.IsPrimaryKey = false;
				colvarCodigo.IsForeignKey = false;
				colvarCodigo.IsReadOnly = false;
				colvarCodigo.DefaultSetting = @"";
				colvarCodigo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigo);
				
				TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
				colvarNombre.ColumnName = "NOMBRE";
				colvarNombre.DataType = DbType.String;
				colvarNombre.MaxLength = 150;
				colvarNombre.AutoIncrement = false;
				colvarNombre.IsNullable = false;
				colvarNombre.IsPrimaryKey = false;
				colvarNombre.IsForeignKey = false;
				colvarNombre.IsReadOnly = false;
				colvarNombre.DefaultSetting = @"";
				colvarNombre.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombre);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "DESCRIPCION";
				colvarDescripcion.DataType = DbType.String;
				colvarDescripcion.MaxLength = 4000;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = true;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				TableSchema.TableColumn colvarLocalizacion = new TableSchema.TableColumn(schema);
				colvarLocalizacion.ColumnName = "LOCALIZACION";
				colvarLocalizacion.DataType = DbType.String;
				colvarLocalizacion.MaxLength = 200;
				colvarLocalizacion.AutoIncrement = false;
				colvarLocalizacion.IsNullable = true;
				colvarLocalizacion.IsPrimaryKey = false;
				colvarLocalizacion.IsForeignKey = false;
				colvarLocalizacion.IsReadOnly = false;
				colvarLocalizacion.DefaultSetting = @"";
				colvarLocalizacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLocalizacion);
				
				TableSchema.TableColumn colvarCodigoBarras = new TableSchema.TableColumn(schema);
				colvarCodigoBarras.ColumnName = "CODIGO_BARRAS";
				colvarCodigoBarras.DataType = DbType.String;
				colvarCodigoBarras.MaxLength = 50;
				colvarCodigoBarras.AutoIncrement = false;
				colvarCodigoBarras.IsNullable = true;
				colvarCodigoBarras.IsPrimaryKey = false;
				colvarCodigoBarras.IsForeignKey = false;
				colvarCodigoBarras.IsReadOnly = false;
				colvarCodigoBarras.DefaultSetting = @"";
				colvarCodigoBarras.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigoBarras);
				
				TableSchema.TableColumn colvarIdTipoEmpaque = new TableSchema.TableColumn(schema);
				colvarIdTipoEmpaque.ColumnName = "ID_TIPO_EMPAQUE";
				colvarIdTipoEmpaque.DataType = DbType.Int32;
				colvarIdTipoEmpaque.MaxLength = 0;
				colvarIdTipoEmpaque.AutoIncrement = false;
				colvarIdTipoEmpaque.IsNullable = false;
				colvarIdTipoEmpaque.IsPrimaryKey = false;
				colvarIdTipoEmpaque.IsForeignKey = true;
				colvarIdTipoEmpaque.IsReadOnly = false;
				colvarIdTipoEmpaque.DefaultSetting = @"";
				
					colvarIdTipoEmpaque.ForeignKeyTableName = "INV_MAS_TIPOS_EMPAQUE";
				schema.Columns.Add(colvarIdTipoEmpaque);
				
				TableSchema.TableColumn colvarUnidadesxempaque = new TableSchema.TableColumn(schema);
				colvarUnidadesxempaque.ColumnName = "UNIDADESXEMPAQUE";
				colvarUnidadesxempaque.DataType = DbType.AnsiString;
				colvarUnidadesxempaque.MaxLength = 3;
				colvarUnidadesxempaque.AutoIncrement = false;
				colvarUnidadesxempaque.IsNullable = false;
				colvarUnidadesxempaque.IsPrimaryKey = false;
				colvarUnidadesxempaque.IsForeignKey = false;
				colvarUnidadesxempaque.IsReadOnly = false;
				colvarUnidadesxempaque.DefaultSetting = @"";
				colvarUnidadesxempaque.ForeignKeyTableName = "";
				schema.Columns.Add(colvarUnidadesxempaque);
				
				TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
				colvarCostoBase.ColumnName = "COSTO_BASE";
				colvarCostoBase.DataType = DbType.Decimal;
				colvarCostoBase.MaxLength = 0;
				colvarCostoBase.AutoIncrement = false;
				colvarCostoBase.IsNullable = false;
				colvarCostoBase.IsPrimaryKey = false;
				colvarCostoBase.IsForeignKey = false;
				colvarCostoBase.IsReadOnly = false;
				colvarCostoBase.DefaultSetting = @"";
				colvarCostoBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoBase);
				
				TableSchema.TableColumn colvarValorAsegurado = new TableSchema.TableColumn(schema);
				colvarValorAsegurado.ColumnName = "VALOR_ASEGURADO";
				colvarValorAsegurado.DataType = DbType.Decimal;
				colvarValorAsegurado.MaxLength = 0;
				colvarValorAsegurado.AutoIncrement = false;
				colvarValorAsegurado.IsNullable = true;
				colvarValorAsegurado.IsPrimaryKey = false;
				colvarValorAsegurado.IsForeignKey = false;
				colvarValorAsegurado.IsReadOnly = false;
				colvarValorAsegurado.DefaultSetting = @"";
				colvarValorAsegurado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorAsegurado);
				
				TableSchema.TableColumn colvarLargo = new TableSchema.TableColumn(schema);
				colvarLargo.ColumnName = "LARGO";
				colvarLargo.DataType = DbType.Decimal;
				colvarLargo.MaxLength = 0;
				colvarLargo.AutoIncrement = false;
				colvarLargo.IsNullable = false;
				colvarLargo.IsPrimaryKey = false;
				colvarLargo.IsForeignKey = false;
				colvarLargo.IsReadOnly = false;
				colvarLargo.DefaultSetting = @"";
				colvarLargo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLargo);
				
				TableSchema.TableColumn colvarAncho = new TableSchema.TableColumn(schema);
				colvarAncho.ColumnName = "ANCHO";
				colvarAncho.DataType = DbType.Decimal;
				colvarAncho.MaxLength = 0;
				colvarAncho.AutoIncrement = false;
				colvarAncho.IsNullable = false;
				colvarAncho.IsPrimaryKey = false;
				colvarAncho.IsForeignKey = false;
				colvarAncho.IsReadOnly = false;
				colvarAncho.DefaultSetting = @"";
				colvarAncho.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAncho);
				
				TableSchema.TableColumn colvarAlto = new TableSchema.TableColumn(schema);
				colvarAlto.ColumnName = "ALTO";
				colvarAlto.DataType = DbType.Decimal;
				colvarAlto.MaxLength = 0;
				colvarAlto.AutoIncrement = false;
				colvarAlto.IsNullable = false;
				colvarAlto.IsPrimaryKey = false;
				colvarAlto.IsForeignKey = false;
				colvarAlto.IsReadOnly = false;
				colvarAlto.DefaultSetting = @"";
				colvarAlto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAlto);
				
				TableSchema.TableColumn colvarPeso = new TableSchema.TableColumn(schema);
				colvarPeso.ColumnName = "PESO";
				colvarPeso.DataType = DbType.Decimal;
				colvarPeso.MaxLength = 0;
				colvarPeso.AutoIncrement = false;
				colvarPeso.IsNullable = false;
				colvarPeso.IsPrimaryKey = false;
				colvarPeso.IsForeignKey = false;
				colvarPeso.IsReadOnly = false;
				colvarPeso.DefaultSetting = @"";
				colvarPeso.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeso);
				
				TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
				colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
				colvarIdProveedor.DataType = DbType.Int32;
				colvarIdProveedor.MaxLength = 0;
				colvarIdProveedor.AutoIncrement = false;
				colvarIdProveedor.IsNullable = false;
				colvarIdProveedor.IsPrimaryKey = false;
				colvarIdProveedor.IsForeignKey = true;
				colvarIdProveedor.IsReadOnly = false;
				colvarIdProveedor.DefaultSetting = @"";
				
					colvarIdProveedor.ForeignKeyTableName = "INV_PROVEEDORES";
				schema.Columns.Add(colvarIdProveedor);
				
				TableSchema.TableColumn colvarIdTipoProducto = new TableSchema.TableColumn(schema);
				colvarIdTipoProducto.ColumnName = "ID_TIPO_PRODUCTO";
				colvarIdTipoProducto.DataType = DbType.Int32;
				colvarIdTipoProducto.MaxLength = 0;
				colvarIdTipoProducto.AutoIncrement = false;
				colvarIdTipoProducto.IsNullable = false;
				colvarIdTipoProducto.IsPrimaryKey = false;
				colvarIdTipoProducto.IsForeignKey = true;
				colvarIdTipoProducto.IsReadOnly = false;
				colvarIdTipoProducto.DefaultSetting = @"";
				
					colvarIdTipoProducto.ForeignKeyTableName = "INV_MAS_TIPOS_PRODUCTO";
				schema.Columns.Add(colvarIdTipoProducto);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarReteFuente = new TableSchema.TableColumn(schema);
				colvarReteFuente.ColumnName = "RETE_FUENTE";
				colvarReteFuente.DataType = DbType.Decimal;
				colvarReteFuente.MaxLength = 0;
				colvarReteFuente.AutoIncrement = false;
				colvarReteFuente.IsNullable = false;
				colvarReteFuente.IsPrimaryKey = false;
				colvarReteFuente.IsForeignKey = false;
				colvarReteFuente.IsReadOnly = false;
				colvarReteFuente.DefaultSetting = @"";
				colvarReteFuente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarReteFuente);
				
				TableSchema.TableColumn colvarDescuento = new TableSchema.TableColumn(schema);
				colvarDescuento.ColumnName = "DESCUENTO";
				colvarDescuento.DataType = DbType.Decimal;
				colvarDescuento.MaxLength = 0;
				colvarDescuento.AutoIncrement = false;
				colvarDescuento.IsNullable = true;
				colvarDescuento.IsPrimaryKey = false;
				colvarDescuento.IsForeignKey = false;
				colvarDescuento.IsReadOnly = false;
				
						colvarDescuento.DefaultSetting = @"((0))";
				colvarDescuento.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescuento);
				
				TableSchema.TableColumn colvarIvaCosto = new TableSchema.TableColumn(schema);
				colvarIvaCosto.ColumnName = "IVA_COSTO";
				colvarIvaCosto.DataType = DbType.Decimal;
				colvarIvaCosto.MaxLength = 0;
				colvarIvaCosto.AutoIncrement = false;
				colvarIvaCosto.IsNullable = true;
				colvarIvaCosto.IsPrimaryKey = false;
				colvarIvaCosto.IsForeignKey = false;
				colvarIvaCosto.IsReadOnly = false;
				colvarIvaCosto.DefaultSetting = @"";
				colvarIvaCosto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIvaCosto);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_PRODUCTOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Codigo")]
		[Bindable(true)]
		public string Codigo 
		{
			get { return GetColumnValue<string>(Columns.Codigo); }
			set { SetColumnValue(Columns.Codigo, value); }
		}
		  
		[XmlAttribute("Nombre")]
		[Bindable(true)]
		public string Nombre 
		{
			get { return GetColumnValue<string>(Columns.Nombre); }
			set { SetColumnValue(Columns.Nombre, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		  
		[XmlAttribute("Localizacion")]
		[Bindable(true)]
		public string Localizacion 
		{
			get { return GetColumnValue<string>(Columns.Localizacion); }
			set { SetColumnValue(Columns.Localizacion, value); }
		}
		  
		[XmlAttribute("CodigoBarras")]
		[Bindable(true)]
		public string CodigoBarras 
		{
			get { return GetColumnValue<string>(Columns.CodigoBarras); }
			set { SetColumnValue(Columns.CodigoBarras, value); }
		}
		  
		[XmlAttribute("IdTipoEmpaque")]
		[Bindable(true)]
		public int IdTipoEmpaque 
		{
			get { return GetColumnValue<int>(Columns.IdTipoEmpaque); }
			set { SetColumnValue(Columns.IdTipoEmpaque, value); }
		}
		  
		[XmlAttribute("Unidadesxempaque")]
		[Bindable(true)]
		public string Unidadesxempaque 
		{
			get { return GetColumnValue<string>(Columns.Unidadesxempaque); }
			set { SetColumnValue(Columns.Unidadesxempaque, value); }
		}
		  
		[XmlAttribute("CostoBase")]
		[Bindable(true)]
		public decimal CostoBase 
		{
			get { return GetColumnValue<decimal>(Columns.CostoBase); }
			set { SetColumnValue(Columns.CostoBase, value); }
		}
		  
		[XmlAttribute("ValorAsegurado")]
		[Bindable(true)]
		public decimal? ValorAsegurado 
		{
			get { return GetColumnValue<decimal?>(Columns.ValorAsegurado); }
			set { SetColumnValue(Columns.ValorAsegurado, value); }
		}
		  
		[XmlAttribute("Largo")]
		[Bindable(true)]
		public decimal Largo 
		{
			get { return GetColumnValue<decimal>(Columns.Largo); }
			set { SetColumnValue(Columns.Largo, value); }
		}
		  
		[XmlAttribute("Ancho")]
		[Bindable(true)]
		public decimal Ancho 
		{
			get { return GetColumnValue<decimal>(Columns.Ancho); }
			set { SetColumnValue(Columns.Ancho, value); }
		}
		  
		[XmlAttribute("Alto")]
		[Bindable(true)]
		public decimal Alto 
		{
			get { return GetColumnValue<decimal>(Columns.Alto); }
			set { SetColumnValue(Columns.Alto, value); }
		}
		  
		[XmlAttribute("Peso")]
		[Bindable(true)]
		public decimal Peso 
		{
			get { return GetColumnValue<decimal>(Columns.Peso); }
			set { SetColumnValue(Columns.Peso, value); }
		}
		  
		[XmlAttribute("IdProveedor")]
		[Bindable(true)]
		public int IdProveedor 
		{
			get { return GetColumnValue<int>(Columns.IdProveedor); }
			set { SetColumnValue(Columns.IdProveedor, value); }
		}
		  
		[XmlAttribute("IdTipoProducto")]
		[Bindable(true)]
		public int IdTipoProducto 
		{
			get { return GetColumnValue<int>(Columns.IdTipoProducto); }
			set { SetColumnValue(Columns.IdTipoProducto, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("ReteFuente")]
		[Bindable(true)]
		public decimal ReteFuente 
		{
			get { return GetColumnValue<decimal>(Columns.ReteFuente); }
			set { SetColumnValue(Columns.ReteFuente, value); }
		}
		  
		[XmlAttribute("Descuento")]
		[Bindable(true)]
		public decimal? Descuento 
		{
			get { return GetColumnValue<decimal?>(Columns.Descuento); }
			set { SetColumnValue(Columns.Descuento, value); }
		}
		  
		[XmlAttribute("IvaCosto")]
		[Bindable(true)]
		public decimal? IvaCosto 
		{
			get { return GetColumnValue<decimal?>(Columns.IvaCosto); }
			set { SetColumnValue(Columns.IvaCosto, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvCostoEnvioAlternoCollection InvCostoEnvioAlternoRecords()
		{
			return new Big.Helper.Domain.Generated.InvCostoEnvioAlternoCollection().Where(InvCostoEnvioAlterno.Columns.GuidProducto, Guid).Load();
		}
		public Big.Helper.Domain.Generated.InvDetallesFacturaProveedorPuntoCollection InvDetallesFacturaProveedorPuntos()
		{
			return new Big.Helper.Domain.Generated.InvDetallesFacturaProveedorPuntoCollection().Where(InvDetallesFacturaProveedorPunto.Columns.GuidProducto, Guid).Load();
		}
		public Big.Helper.Domain.Generated.InvHistoricosPrecioCollection InvHistoricosPrecioRecords()
		{
			return new Big.Helper.Domain.Generated.InvHistoricosPrecioCollection().Where(InvHistoricosPrecio.Columns.GuidProducto, Guid).Load();
		}
		public Big.Helper.Domain.Generated.InvPreciosBaseProgramaCollection InvPreciosBaseProgramaRecords()
		{
			return new Big.Helper.Domain.Generated.InvPreciosBaseProgramaCollection().Where(InvPreciosBasePrograma.Columns.GuidProducto, Guid).Load();
		}
		public Big.Helper.Domain.Generated.InvReferenciasProductoCollection InvReferenciasProductoRecords()
		{
			return new Big.Helper.Domain.Generated.InvReferenciasProductoCollection().Where(InvReferenciasProducto.Columns.GuidProducto, Guid).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasTiposEmpaque ActiveRecord object related to this InvProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasTiposEmpaque InvMasTiposEmpaque
		{
			get { return Big.Helper.Domain.Generated.InvMasTiposEmpaque.FetchByID(this.IdTipoEmpaque); }
			set { SetColumnValue("ID_TIPO_EMPAQUE", value.IdTipoEmpaque); }
		}
		
		
		/// <summary>
		/// Returns a InvMasTiposProducto ActiveRecord object related to this InvProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasTiposProducto InvMasTiposProducto
		{
			get { return Big.Helper.Domain.Generated.InvMasTiposProducto.FetchByID(this.IdTipoProducto); }
			set { SetColumnValue("ID_TIPO_PRODUCTO", value.IdTipoProducto); }
		}
		
		
		/// <summary>
		/// Returns a InvProveedore ActiveRecord object related to this InvProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvProveedore InvProveedore
		{
			get { return Big.Helper.Domain.Generated.InvProveedore.FetchByID(this.IdProveedor); }
			set { SetColumnValue("ID_PROVEEDOR", value.IdProveedor); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,string varCodigo,string varNombre,string varDescripcion,string varLocalizacion,string varCodigoBarras,int varIdTipoEmpaque,string varUnidadesxempaque,decimal varCostoBase,decimal? varValorAsegurado,decimal varLargo,decimal varAncho,decimal varAlto,decimal varPeso,int varIdProveedor,int varIdTipoProducto,decimal varIva,decimal varReteFuente,decimal? varDescuento,decimal? varIvaCosto)
		{
			InvProducto item = new InvProducto();
			
			item.Guid = varGuid;
			
			item.Codigo = varCodigo;
			
			item.Nombre = varNombre;
			
			item.Descripcion = varDescripcion;
			
			item.Localizacion = varLocalizacion;
			
			item.CodigoBarras = varCodigoBarras;
			
			item.IdTipoEmpaque = varIdTipoEmpaque;
			
			item.Unidadesxempaque = varUnidadesxempaque;
			
			item.CostoBase = varCostoBase;
			
			item.ValorAsegurado = varValorAsegurado;
			
			item.Largo = varLargo;
			
			item.Ancho = varAncho;
			
			item.Alto = varAlto;
			
			item.Peso = varPeso;
			
			item.IdProveedor = varIdProveedor;
			
			item.IdTipoProducto = varIdTipoProducto;
			
			item.Iva = varIva;
			
			item.ReteFuente = varReteFuente;
			
			item.Descuento = varDescuento;
			
			item.IvaCosto = varIvaCosto;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,string varCodigo,string varNombre,string varDescripcion,string varLocalizacion,string varCodigoBarras,int varIdTipoEmpaque,string varUnidadesxempaque,decimal varCostoBase,decimal? varValorAsegurado,decimal varLargo,decimal varAncho,decimal varAlto,decimal varPeso,int varIdProveedor,int varIdTipoProducto,decimal varIva,decimal varReteFuente,decimal? varDescuento,decimal? varIvaCosto)
		{
			InvProducto item = new InvProducto();
			
				item.Guid = varGuid;
			
				item.Codigo = varCodigo;
			
				item.Nombre = varNombre;
			
				item.Descripcion = varDescripcion;
			
				item.Localizacion = varLocalizacion;
			
				item.CodigoBarras = varCodigoBarras;
			
				item.IdTipoEmpaque = varIdTipoEmpaque;
			
				item.Unidadesxempaque = varUnidadesxempaque;
			
				item.CostoBase = varCostoBase;
			
				item.ValorAsegurado = varValorAsegurado;
			
				item.Largo = varLargo;
			
				item.Ancho = varAncho;
			
				item.Alto = varAlto;
			
				item.Peso = varPeso;
			
				item.IdProveedor = varIdProveedor;
			
				item.IdTipoProducto = varIdTipoProducto;
			
				item.Iva = varIva;
			
				item.ReteFuente = varReteFuente;
			
				item.Descuento = varDescuento;
			
				item.IvaCosto = varIvaCosto;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn LocalizacionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoBarrasColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoEmpaqueColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn UnidadesxempaqueColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoBaseColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorAseguradoColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn LargoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn AnchoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn AltoColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn PesoColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProveedorColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoProductoColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn ReteFuenteColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn DescuentoColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaCostoColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string Codigo = @"CODIGO";
			 public static string Nombre = @"NOMBRE";
			 public static string Descripcion = @"DESCRIPCION";
			 public static string Localizacion = @"LOCALIZACION";
			 public static string CodigoBarras = @"CODIGO_BARRAS";
			 public static string IdTipoEmpaque = @"ID_TIPO_EMPAQUE";
			 public static string Unidadesxempaque = @"UNIDADESXEMPAQUE";
			 public static string CostoBase = @"COSTO_BASE";
			 public static string ValorAsegurado = @"VALOR_ASEGURADO";
			 public static string Largo = @"LARGO";
			 public static string Ancho = @"ANCHO";
			 public static string Alto = @"ALTO";
			 public static string Peso = @"PESO";
			 public static string IdProveedor = @"ID_PROVEEDOR";
			 public static string IdTipoProducto = @"ID_TIPO_PRODUCTO";
			 public static string Iva = @"IVA";
			 public static string ReteFuente = @"RETE_FUENTE";
			 public static string Descuento = @"DESCUENTO";
			 public static string IvaCosto = @"IVA_COSTO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
