using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the ViewTransProveedorCostoVentum class.
    /// </summary>
    [Serializable]
    public partial class ViewTransProveedorCostoVentumCollection : ReadOnlyList<ViewTransProveedorCostoVentum, ViewTransProveedorCostoVentumCollection>
    {        
        public ViewTransProveedorCostoVentumCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the View_Trans_Proveedor_Costo_Venta view.
    /// </summary>
    [Serializable]
    public partial class ViewTransProveedorCostoVentum : ReadOnlyRecord<ViewTransProveedorCostoVentum>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("View_Trans_Proveedor_Costo_Venta", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCodigoProveedor = new TableSchema.TableColumn(schema);
                colvarCodigoProveedor.ColumnName = "CODIGO_PROVEEDOR";
                colvarCodigoProveedor.DataType = DbType.AnsiString;
                colvarCodigoProveedor.MaxLength = 20;
                colvarCodigoProveedor.AutoIncrement = false;
                colvarCodigoProveedor.IsNullable = false;
                colvarCodigoProveedor.IsPrimaryKey = false;
                colvarCodigoProveedor.IsForeignKey = false;
                colvarCodigoProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoProveedor);
                
                TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
                colvarNombre.ColumnName = "NOMBRE";
                colvarNombre.DataType = DbType.String;
                colvarNombre.MaxLength = 100;
                colvarNombre.AutoIncrement = false;
                colvarNombre.IsNullable = false;
                colvarNombre.IsPrimaryKey = false;
                colvarNombre.IsForeignKey = false;
                colvarNombre.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombre);
                
                TableSchema.TableColumn colvarSumaValorBase = new TableSchema.TableColumn(schema);
                colvarSumaValorBase.ColumnName = "SUMA_VALOR_BASE";
                colvarSumaValorBase.DataType = DbType.Decimal;
                colvarSumaValorBase.MaxLength = 0;
                colvarSumaValorBase.AutoIncrement = false;
                colvarSumaValorBase.IsNullable = true;
                colvarSumaValorBase.IsPrimaryKey = false;
                colvarSumaValorBase.IsForeignKey = false;
                colvarSumaValorBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarSumaValorBase);
                
                TableSchema.TableColumn colvarSumaCantidad = new TableSchema.TableColumn(schema);
                colvarSumaCantidad.ColumnName = "SUMA_CANTIDAD";
                colvarSumaCantidad.DataType = DbType.Int32;
                colvarSumaCantidad.MaxLength = 0;
                colvarSumaCantidad.AutoIncrement = false;
                colvarSumaCantidad.IsNullable = true;
                colvarSumaCantidad.IsPrimaryKey = false;
                colvarSumaCantidad.IsForeignKey = false;
                colvarSumaCantidad.IsReadOnly = false;
                
                schema.Columns.Add(colvarSumaCantidad);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("View_Trans_Proveedor_Costo_Venta",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public ViewTransProveedorCostoVentum()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public ViewTransProveedorCostoVentum(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public ViewTransProveedorCostoVentum(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public ViewTransProveedorCostoVentum(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CodigoProveedor")]
        [Bindable(true)]
        public string CodigoProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("CODIGO_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("Nombre")]
        [Bindable(true)]
        public string Nombre 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE", value);
            }
        }
	      
        [XmlAttribute("SumaValorBase")]
        [Bindable(true)]
        public decimal? SumaValorBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("SUMA_VALOR_BASE");
		    }
            set 
		    {
			    SetColumnValue("SUMA_VALOR_BASE", value);
            }
        }
	      
        [XmlAttribute("SumaCantidad")]
        [Bindable(true)]
        public int? SumaCantidad 
	    {
		    get
		    {
			    return GetColumnValue<int?>("SUMA_CANTIDAD");
		    }
            set 
		    {
			    SetColumnValue("SUMA_CANTIDAD", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CodigoProveedor = @"CODIGO_PROVEEDOR";
            
            public static string Nombre = @"NOMBRE";
            
            public static string SumaValorBase = @"SUMA_VALOR_BASE";
            
            public static string SumaCantidad = @"SUMA_CANTIDAD";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
