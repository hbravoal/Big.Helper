using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoCentroUtilidad class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoCentroUtilidadCollection : ActiveList<InvCgunoCentroUtilidad, InvCgunoCentroUtilidadCollection>
	{	   
		public InvCgunoCentroUtilidadCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoCentroUtilidadCollection</returns>
		public InvCgunoCentroUtilidadCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoCentroUtilidad o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_CENTRO_UTILIDAD table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoCentroUtilidad : ActiveRecord<InvCgunoCentroUtilidad>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoCentroUtilidad()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoCentroUtilidad(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoCentroUtilidad(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoCentroUtilidad(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_CENTRO_UTILIDAD", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = true;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				
					colvarId.ForeignKeyTableName = "INV_CGUNO_CENTRO_UTILIDAD";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.String;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarNumeroCentroUtilidad = new TableSchema.TableColumn(schema);
				colvarNumeroCentroUtilidad.ColumnName = "NUMERO_CENTRO_UTILIDAD";
				colvarNumeroCentroUtilidad.DataType = DbType.String;
				colvarNumeroCentroUtilidad.MaxLength = 50;
				colvarNumeroCentroUtilidad.AutoIncrement = false;
				colvarNumeroCentroUtilidad.IsNullable = false;
				colvarNumeroCentroUtilidad.IsPrimaryKey = false;
				colvarNumeroCentroUtilidad.IsForeignKey = false;
				colvarNumeroCentroUtilidad.IsReadOnly = false;
				colvarNumeroCentroUtilidad.DefaultSetting = @"";
				colvarNumeroCentroUtilidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroCentroUtilidad);
				
				TableSchema.TableColumn colvarNombreCentroUtilidad = new TableSchema.TableColumn(schema);
				colvarNombreCentroUtilidad.ColumnName = "NOMBRE_CENTRO_UTILIDAD";
				colvarNombreCentroUtilidad.DataType = DbType.String;
				colvarNombreCentroUtilidad.MaxLength = 100;
				colvarNombreCentroUtilidad.AutoIncrement = false;
				colvarNombreCentroUtilidad.IsNullable = false;
				colvarNombreCentroUtilidad.IsPrimaryKey = false;
				colvarNombreCentroUtilidad.IsForeignKey = false;
				colvarNombreCentroUtilidad.IsReadOnly = false;
				colvarNombreCentroUtilidad.DefaultSetting = @"";
				colvarNombreCentroUtilidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreCentroUtilidad);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_CENTRO_UTILIDAD",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("NumeroCentroUtilidad")]
		[Bindable(true)]
		public string NumeroCentroUtilidad 
		{
			get { return GetColumnValue<string>(Columns.NumeroCentroUtilidad); }
			set { SetColumnValue(Columns.NumeroCentroUtilidad, value); }
		}
		  
		[XmlAttribute("NombreCentroUtilidad")]
		[Bindable(true)]
		public string NombreCentroUtilidad 
		{
			get { return GetColumnValue<string>(Columns.NombreCentroUtilidad); }
			set { SetColumnValue(Columns.NombreCentroUtilidad, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvCgunoCentroUtilidadCollection ChildInvCgunoCentroUtilidadRecords()
		{
			return new Big.Helper.Domain.Generated.InvCgunoCentroUtilidadCollection().Where(InvCgunoCentroUtilidad.Columns.Id, Id).Load();
		}
		public Big.Helper.Domain.Generated.InvCgunoCuentaCollection InvCgunoCuentas()
		{
			return new Big.Helper.Domain.Generated.InvCgunoCuentaCollection().Where(InvCgunoCuenta.Columns.IdCentroUtilidad, Id).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoCentroUtilidad ActiveRecord object related to this InvCgunoCentroUtilidad
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCentroUtilidad ParentInvCgunoCentroUtilidad
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCentroUtilidad.FetchByID(this.Id); }
			set { SetColumnValue("ID", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,string varNumeroCentroUtilidad,string varNombreCentroUtilidad)
		{
			InvCgunoCentroUtilidad item = new InvCgunoCentroUtilidad();
			
			item.Guid = varGuid;
			
			item.NumeroCentroUtilidad = varNumeroCentroUtilidad;
			
			item.NombreCentroUtilidad = varNombreCentroUtilidad;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varGuid,string varNumeroCentroUtilidad,string varNombreCentroUtilidad)
		{
			InvCgunoCentroUtilidad item = new InvCgunoCentroUtilidad();
			
				item.Id = varId;
			
				item.Guid = varGuid;
			
				item.NumeroCentroUtilidad = varNumeroCentroUtilidad;
			
				item.NombreCentroUtilidad = varNombreCentroUtilidad;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroCentroUtilidadColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreCentroUtilidadColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Guid = @"GUID";
			 public static string NumeroCentroUtilidad = @"NUMERO_CENTRO_UTILIDAD";
			 public static string NombreCentroUtilidad = @"NOMBRE_CENTRO_UTILIDAD";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
