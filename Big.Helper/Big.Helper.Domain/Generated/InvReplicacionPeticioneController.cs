using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_REPLICACION_PETICIONES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvReplicacionPeticioneController
    {
        // Preload our schema..
        InvReplicacionPeticione thisSchemaLoad = new InvReplicacionPeticione();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvReplicacionPeticioneCollection FetchAll()
        {
            InvReplicacionPeticioneCollection coll = new InvReplicacionPeticioneCollection();
            Query qry = new Query(InvReplicacionPeticione.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvReplicacionPeticioneCollection FetchByID(object Id)
        {
            InvReplicacionPeticioneCollection coll = new InvReplicacionPeticioneCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvReplicacionPeticioneCollection FetchByQuery(Query qry)
        {
            InvReplicacionPeticioneCollection coll = new InvReplicacionPeticioneCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvReplicacionPeticione.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvReplicacionPeticione.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdModulo,DateTime FechaPeticion,bool Procesada)
	    {
		    InvReplicacionPeticione item = new InvReplicacionPeticione();
		    
            item.IdModulo = IdModulo;
            
            item.FechaPeticion = FechaPeticion;
            
            item.Procesada = Procesada;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int IdModulo,DateTime FechaPeticion,bool Procesada)
	    {
		    InvReplicacionPeticione item = new InvReplicacionPeticione();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.IdModulo = IdModulo;
				
			item.FechaPeticion = FechaPeticion;
				
			item.Procesada = Procesada;
				
	        item.Save(UserName);
	    }
    }
}
