using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvMarca class.
	/// </summary>
    [Serializable]
	public partial class InvMarcaCollection : ActiveList<InvMarca, InvMarcaCollection>
	{	   
		public InvMarcaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMarcaCollection</returns>
		public InvMarcaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMarca o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MARCAS table.
	/// </summary>
	[Serializable]
	public partial class InvMarca : ActiveRecord<InvMarca>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMarca()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMarca(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMarca(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMarca(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MARCAS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdMarca = new TableSchema.TableColumn(schema);
				colvarIdMarca.ColumnName = "ID_MARCA";
				colvarIdMarca.DataType = DbType.Int32;
				colvarIdMarca.MaxLength = 0;
				colvarIdMarca.AutoIncrement = true;
				colvarIdMarca.IsNullable = false;
				colvarIdMarca.IsPrimaryKey = true;
				colvarIdMarca.IsForeignKey = false;
				colvarIdMarca.IsReadOnly = false;
				colvarIdMarca.DefaultSetting = @"";
				colvarIdMarca.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdMarca);
				
				TableSchema.TableColumn colvarNombreMarca = new TableSchema.TableColumn(schema);
				colvarNombreMarca.ColumnName = "NOMBRE_MARCA";
				colvarNombreMarca.DataType = DbType.String;
				colvarNombreMarca.MaxLength = 200;
				colvarNombreMarca.AutoIncrement = false;
				colvarNombreMarca.IsNullable = false;
				colvarNombreMarca.IsPrimaryKey = false;
				colvarNombreMarca.IsForeignKey = false;
				colvarNombreMarca.IsReadOnly = false;
				colvarNombreMarca.DefaultSetting = @"";
				colvarNombreMarca.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreMarca);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
				colvarIdCiudad.ColumnName = "ID_CIUDAD";
				colvarIdCiudad.DataType = DbType.Int32;
				colvarIdCiudad.MaxLength = 0;
				colvarIdCiudad.AutoIncrement = false;
				colvarIdCiudad.IsNullable = false;
				colvarIdCiudad.IsPrimaryKey = false;
				colvarIdCiudad.IsForeignKey = true;
				colvarIdCiudad.IsReadOnly = false;
				colvarIdCiudad.DefaultSetting = @"";
				
					colvarIdCiudad.ForeignKeyTableName = "INV_MAS_CIUDADES";
				schema.Columns.Add(colvarIdCiudad);
				
				TableSchema.TableColumn colvarDireccion = new TableSchema.TableColumn(schema);
				colvarDireccion.ColumnName = "DIRECCION";
				colvarDireccion.DataType = DbType.String;
				colvarDireccion.MaxLength = 50;
				colvarDireccion.AutoIncrement = false;
				colvarDireccion.IsNullable = false;
				colvarDireccion.IsPrimaryKey = false;
				colvarDireccion.IsForeignKey = false;
				colvarDireccion.IsReadOnly = false;
				colvarDireccion.DefaultSetting = @"";
				colvarDireccion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDireccion);
				
				TableSchema.TableColumn colvarCiCoordinadora = new TableSchema.TableColumn(schema);
				colvarCiCoordinadora.ColumnName = "CI_COORDINADORA";
				colvarCiCoordinadora.DataType = DbType.Decimal;
				colvarCiCoordinadora.MaxLength = 0;
				colvarCiCoordinadora.AutoIncrement = false;
				colvarCiCoordinadora.IsNullable = true;
				colvarCiCoordinadora.IsPrimaryKey = false;
				colvarCiCoordinadora.IsForeignKey = false;
				colvarCiCoordinadora.IsReadOnly = false;
				colvarCiCoordinadora.DefaultSetting = @"";
				colvarCiCoordinadora.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCiCoordinadora);
				
				TableSchema.TableColumn colvarPrefijoGuias = new TableSchema.TableColumn(schema);
				colvarPrefijoGuias.ColumnName = "PREFIJO_GUIAS";
				colvarPrefijoGuias.DataType = DbType.Decimal;
				colvarPrefijoGuias.MaxLength = 0;
				colvarPrefijoGuias.AutoIncrement = false;
				colvarPrefijoGuias.IsNullable = true;
				colvarPrefijoGuias.IsPrimaryKey = false;
				colvarPrefijoGuias.IsForeignKey = false;
				colvarPrefijoGuias.IsReadOnly = false;
				colvarPrefijoGuias.DefaultSetting = @"";
				colvarPrefijoGuias.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrefijoGuias);
				
				TableSchema.TableColumn colvarGuiaInicial = new TableSchema.TableColumn(schema);
				colvarGuiaInicial.ColumnName = "GUIA_INICIAL";
				colvarGuiaInicial.DataType = DbType.Decimal;
				colvarGuiaInicial.MaxLength = 0;
				colvarGuiaInicial.AutoIncrement = false;
				colvarGuiaInicial.IsNullable = true;
				colvarGuiaInicial.IsPrimaryKey = false;
				colvarGuiaInicial.IsForeignKey = false;
				colvarGuiaInicial.IsReadOnly = false;
				colvarGuiaInicial.DefaultSetting = @"";
				colvarGuiaInicial.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuiaInicial);
				
				TableSchema.TableColumn colvarGuiaFinal = new TableSchema.TableColumn(schema);
				colvarGuiaFinal.ColumnName = "GUIA_FINAL";
				colvarGuiaFinal.DataType = DbType.Decimal;
				colvarGuiaFinal.MaxLength = 0;
				colvarGuiaFinal.AutoIncrement = false;
				colvarGuiaFinal.IsNullable = true;
				colvarGuiaFinal.IsPrimaryKey = false;
				colvarGuiaFinal.IsForeignKey = false;
				colvarGuiaFinal.IsReadOnly = false;
				colvarGuiaFinal.DefaultSetting = @"";
				colvarGuiaFinal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuiaFinal);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MARCAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdMarca")]
		[Bindable(true)]
		public int IdMarca 
		{
			get { return GetColumnValue<int>(Columns.IdMarca); }
			set { SetColumnValue(Columns.IdMarca, value); }
		}
		  
		[XmlAttribute("NombreMarca")]
		[Bindable(true)]
		public string NombreMarca 
		{
			get { return GetColumnValue<string>(Columns.NombreMarca); }
			set { SetColumnValue(Columns.NombreMarca, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdCiudad")]
		[Bindable(true)]
		public int IdCiudad 
		{
			get { return GetColumnValue<int>(Columns.IdCiudad); }
			set { SetColumnValue(Columns.IdCiudad, value); }
		}
		  
		[XmlAttribute("Direccion")]
		[Bindable(true)]
		public string Direccion 
		{
			get { return GetColumnValue<string>(Columns.Direccion); }
			set { SetColumnValue(Columns.Direccion, value); }
		}
		  
		[XmlAttribute("CiCoordinadora")]
		[Bindable(true)]
		public decimal? CiCoordinadora 
		{
			get { return GetColumnValue<decimal?>(Columns.CiCoordinadora); }
			set { SetColumnValue(Columns.CiCoordinadora, value); }
		}
		  
		[XmlAttribute("PrefijoGuias")]
		[Bindable(true)]
		public decimal? PrefijoGuias 
		{
			get { return GetColumnValue<decimal?>(Columns.PrefijoGuias); }
			set { SetColumnValue(Columns.PrefijoGuias, value); }
		}
		  
		[XmlAttribute("GuiaInicial")]
		[Bindable(true)]
		public decimal? GuiaInicial 
		{
			get { return GetColumnValue<decimal?>(Columns.GuiaInicial); }
			set { SetColumnValue(Columns.GuiaInicial, value); }
		}
		  
		[XmlAttribute("GuiaFinal")]
		[Bindable(true)]
		public decimal? GuiaFinal 
		{
			get { return GetColumnValue<decimal?>(Columns.GuiaFinal); }
			set { SetColumnValue(Columns.GuiaFinal, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvHistoricosPrecioCollection InvHistoricosPrecioRecords()
		{
			return new Big.Helper.Domain.Generated.InvHistoricosPrecioCollection().Where(InvHistoricosPrecio.Columns.IdMarca, IdMarca).Load();
		}
		public Big.Helper.Domain.Generated.InvPreciosBaseProgramaCollection InvPreciosBaseProgramaRecords()
		{
			return new Big.Helper.Domain.Generated.InvPreciosBaseProgramaCollection().Where(InvPreciosBasePrograma.Columns.IdMarca, IdMarca).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasCiudade ActiveRecord object related to this InvMarca
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasCiudade InvMasCiudade
		{
			get { return Big.Helper.Domain.Generated.InvMasCiudade.FetchByID(this.IdCiudad); }
			set { SetColumnValue("ID_CIUDAD", value.IdCiudad); }
		}
		
		
		/// <summary>
		/// Returns a InvPrograma ActiveRecord object related to this InvMarca
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvPrograma InvPrograma
		{
			get { return Big.Helper.Domain.Generated.InvPrograma.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombreMarca,int varIdPrograma,int varIdCiudad,string varDireccion,decimal? varCiCoordinadora,decimal? varPrefijoGuias,decimal? varGuiaInicial,decimal? varGuiaFinal)
		{
			InvMarca item = new InvMarca();
			
			item.NombreMarca = varNombreMarca;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdCiudad = varIdCiudad;
			
			item.Direccion = varDireccion;
			
			item.CiCoordinadora = varCiCoordinadora;
			
			item.PrefijoGuias = varPrefijoGuias;
			
			item.GuiaInicial = varGuiaInicial;
			
			item.GuiaFinal = varGuiaFinal;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdMarca,string varNombreMarca,int varIdPrograma,int varIdCiudad,string varDireccion,decimal? varCiCoordinadora,decimal? varPrefijoGuias,decimal? varGuiaInicial,decimal? varGuiaFinal)
		{
			InvMarca item = new InvMarca();
			
				item.IdMarca = varIdMarca;
			
				item.NombreMarca = varNombreMarca;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdCiudad = varIdCiudad;
			
				item.Direccion = varDireccion;
			
				item.CiCoordinadora = varCiCoordinadora;
			
				item.PrefijoGuias = varPrefijoGuias;
			
				item.GuiaInicial = varGuiaInicial;
			
				item.GuiaFinal = varGuiaFinal;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdMarcaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreMarcaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCiudadColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn DireccionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn CiCoordinadoraColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PrefijoGuiasColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn GuiaInicialColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn GuiaFinalColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdMarca = @"ID_MARCA";
			 public static string NombreMarca = @"NOMBRE_MARCA";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdCiudad = @"ID_CIUDAD";
			 public static string Direccion = @"DIRECCION";
			 public static string CiCoordinadora = @"CI_COORDINADORA";
			 public static string PrefijoGuias = @"PREFIJO_GUIAS";
			 public static string GuiaInicial = @"GUIA_INICIAL";
			 public static string GuiaFinal = @"GUIA_FINAL";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
