using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCliente class.
	/// </summary>
    [Serializable]
	public partial class InvClienteCollection : ActiveList<InvCliente, InvClienteCollection>
	{	   
		public InvClienteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvClienteCollection</returns>
		public InvClienteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCliente o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CLIENTES table.
	/// </summary>
	[Serializable]
	public partial class InvCliente : ActiveRecord<InvCliente>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCliente()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCliente(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCliente(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCliente(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CLIENTES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidCliente = new TableSchema.TableColumn(schema);
				colvarGuidCliente.ColumnName = "GUID_CLIENTE";
				colvarGuidCliente.DataType = DbType.String;
				colvarGuidCliente.MaxLength = 36;
				colvarGuidCliente.AutoIncrement = false;
				colvarGuidCliente.IsNullable = false;
				colvarGuidCliente.IsPrimaryKey = true;
				colvarGuidCliente.IsForeignKey = false;
				colvarGuidCliente.IsReadOnly = false;
				colvarGuidCliente.DefaultSetting = @"";
				colvarGuidCliente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidCliente);
				
				TableSchema.TableColumn colvarNombres = new TableSchema.TableColumn(schema);
				colvarNombres.ColumnName = "NOMBRES";
				colvarNombres.DataType = DbType.String;
				colvarNombres.MaxLength = 256;
				colvarNombres.AutoIncrement = false;
				colvarNombres.IsNullable = false;
				colvarNombres.IsPrimaryKey = false;
				colvarNombres.IsForeignKey = false;
				colvarNombres.IsReadOnly = false;
				colvarNombres.DefaultSetting = @"";
				colvarNombres.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombres);
				
				TableSchema.TableColumn colvarApellidos = new TableSchema.TableColumn(schema);
				colvarApellidos.ColumnName = "APELLIDOS";
				colvarApellidos.DataType = DbType.String;
				colvarApellidos.MaxLength = 256;
				colvarApellidos.AutoIncrement = false;
				colvarApellidos.IsNullable = true;
				colvarApellidos.IsPrimaryKey = false;
				colvarApellidos.IsForeignKey = false;
				colvarApellidos.IsReadOnly = false;
				colvarApellidos.DefaultSetting = @"";
				colvarApellidos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarApellidos);
				
				TableSchema.TableColumn colvarCedula = new TableSchema.TableColumn(schema);
				colvarCedula.ColumnName = "CEDULA";
				colvarCedula.DataType = DbType.AnsiString;
				colvarCedula.MaxLength = 15;
				colvarCedula.AutoIncrement = false;
				colvarCedula.IsNullable = false;
				colvarCedula.IsPrimaryKey = false;
				colvarCedula.IsForeignKey = false;
				colvarCedula.IsReadOnly = false;
				colvarCedula.DefaultSetting = @"";
				colvarCedula.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCedula);
				
				TableSchema.TableColumn colvarPersonaJuridica = new TableSchema.TableColumn(schema);
				colvarPersonaJuridica.ColumnName = "PERSONA_JURIDICA";
				colvarPersonaJuridica.DataType = DbType.Boolean;
				colvarPersonaJuridica.MaxLength = 0;
				colvarPersonaJuridica.AutoIncrement = false;
				colvarPersonaJuridica.IsNullable = true;
				colvarPersonaJuridica.IsPrimaryKey = false;
				colvarPersonaJuridica.IsForeignKey = false;
				colvarPersonaJuridica.IsReadOnly = false;
				colvarPersonaJuridica.DefaultSetting = @"";
				colvarPersonaJuridica.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPersonaJuridica);
				
				TableSchema.TableColumn colvarNombreContacto = new TableSchema.TableColumn(schema);
				colvarNombreContacto.ColumnName = "NOMBRE_CONTACTO";
				colvarNombreContacto.DataType = DbType.String;
				colvarNombreContacto.MaxLength = 256;
				colvarNombreContacto.AutoIncrement = false;
				colvarNombreContacto.IsNullable = true;
				colvarNombreContacto.IsPrimaryKey = false;
				colvarNombreContacto.IsForeignKey = false;
				colvarNombreContacto.IsReadOnly = false;
				colvarNombreContacto.DefaultSetting = @"";
				colvarNombreContacto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreContacto);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CLIENTES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidCliente")]
		[Bindable(true)]
		public string GuidCliente 
		{
			get { return GetColumnValue<string>(Columns.GuidCliente); }
			set { SetColumnValue(Columns.GuidCliente, value); }
		}
		  
		[XmlAttribute("Nombres")]
		[Bindable(true)]
		public string Nombres 
		{
			get { return GetColumnValue<string>(Columns.Nombres); }
			set { SetColumnValue(Columns.Nombres, value); }
		}
		  
		[XmlAttribute("Apellidos")]
		[Bindable(true)]
		public string Apellidos 
		{
			get { return GetColumnValue<string>(Columns.Apellidos); }
			set { SetColumnValue(Columns.Apellidos, value); }
		}
		  
		[XmlAttribute("Cedula")]
		[Bindable(true)]
		public string Cedula 
		{
			get { return GetColumnValue<string>(Columns.Cedula); }
			set { SetColumnValue(Columns.Cedula, value); }
		}
		  
		[XmlAttribute("PersonaJuridica")]
		[Bindable(true)]
		public bool? PersonaJuridica 
		{
			get { return GetColumnValue<bool?>(Columns.PersonaJuridica); }
			set { SetColumnValue(Columns.PersonaJuridica, value); }
		}
		  
		[XmlAttribute("NombreContacto")]
		[Bindable(true)]
		public string NombreContacto 
		{
			get { return GetColumnValue<string>(Columns.NombreContacto); }
			set { SetColumnValue(Columns.NombreContacto, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvFacturaCollection InvFacturas()
		{
			return new Big.Helper.Domain.Generated.InvFacturaCollection().Where(InvFactura.Columns.GuidCliente, GuidCliente).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidCliente,string varNombres,string varApellidos,string varCedula,bool? varPersonaJuridica,string varNombreContacto)
		{
			InvCliente item = new InvCliente();
			
			item.GuidCliente = varGuidCliente;
			
			item.Nombres = varNombres;
			
			item.Apellidos = varApellidos;
			
			item.Cedula = varCedula;
			
			item.PersonaJuridica = varPersonaJuridica;
			
			item.NombreContacto = varNombreContacto;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidCliente,string varNombres,string varApellidos,string varCedula,bool? varPersonaJuridica,string varNombreContacto)
		{
			InvCliente item = new InvCliente();
			
				item.GuidCliente = varGuidCliente;
			
				item.Nombres = varNombres;
			
				item.Apellidos = varApellidos;
			
				item.Cedula = varCedula;
			
				item.PersonaJuridica = varPersonaJuridica;
			
				item.NombreContacto = varNombreContacto;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidClienteColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombresColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ApellidosColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CedulaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PersonaJuridicaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreContactoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidCliente = @"GUID_CLIENTE";
			 public static string Nombres = @"NOMBRES";
			 public static string Apellidos = @"APELLIDOS";
			 public static string Cedula = @"CEDULA";
			 public static string PersonaJuridica = @"PERSONA_JURIDICA";
			 public static string NombreContacto = @"NOMBRE_CONTACTO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
