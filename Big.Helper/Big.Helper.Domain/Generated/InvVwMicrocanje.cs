using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwMicrocanje class.
    /// </summary>
    [Serializable]
    public partial class InvVwMicrocanjeCollection : ReadOnlyList<InvVwMicrocanje, InvVwMicrocanjeCollection>
    {        
        public InvVwMicrocanjeCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_MICROCANJES view.
    /// </summary>
    [Serializable]
    public partial class InvVwMicrocanje : ReadOnlyRecord<InvVwMicrocanje>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_MICROCANJES", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuidFactura = new TableSchema.TableColumn(schema);
                colvarGuidFactura.ColumnName = "GUID_FACTURA";
                colvarGuidFactura.DataType = DbType.String;
                colvarGuidFactura.MaxLength = 36;
                colvarGuidFactura.AutoIncrement = false;
                colvarGuidFactura.IsNullable = false;
                colvarGuidFactura.IsPrimaryKey = false;
                colvarGuidFactura.IsForeignKey = false;
                colvarGuidFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidFactura);
                
                TableSchema.TableColumn colvarQuantity = new TableSchema.TableColumn(schema);
                colvarQuantity.ColumnName = "QUANTITY";
                colvarQuantity.DataType = DbType.Int32;
                colvarQuantity.MaxLength = 0;
                colvarQuantity.AutoIncrement = false;
                colvarQuantity.IsNullable = false;
                colvarQuantity.IsPrimaryKey = false;
                colvarQuantity.IsForeignKey = false;
                colvarQuantity.IsReadOnly = false;
                
                schema.Columns.Add(colvarQuantity);
                
                TableSchema.TableColumn colvarPointsUnitValue = new TableSchema.TableColumn(schema);
                colvarPointsUnitValue.ColumnName = "POINTS_UNIT_VALUE";
                colvarPointsUnitValue.DataType = DbType.Decimal;
                colvarPointsUnitValue.MaxLength = 0;
                colvarPointsUnitValue.AutoIncrement = false;
                colvarPointsUnitValue.IsNullable = false;
                colvarPointsUnitValue.IsPrimaryKey = false;
                colvarPointsUnitValue.IsForeignKey = false;
                colvarPointsUnitValue.IsReadOnly = false;
                
                schema.Columns.Add(colvarPointsUnitValue);
                
                TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
                colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
                colvarGuidReferenciaProducto.DataType = DbType.String;
                colvarGuidReferenciaProducto.MaxLength = 36;
                colvarGuidReferenciaProducto.AutoIncrement = false;
                colvarGuidReferenciaProducto.IsNullable = false;
                colvarGuidReferenciaProducto.IsPrimaryKey = false;
                colvarGuidReferenciaProducto.IsForeignKey = false;
                colvarGuidReferenciaProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidReferenciaProducto);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_MICROCANJES",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwMicrocanje()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwMicrocanje(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwMicrocanje(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwMicrocanje(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("GuidFactura")]
        [Bindable(true)]
        public string GuidFactura 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("GUID_FACTURA", value);
            }
        }
	      
        [XmlAttribute("Quantity")]
        [Bindable(true)]
        public int Quantity 
	    {
		    get
		    {
			    return GetColumnValue<int>("QUANTITY");
		    }
            set 
		    {
			    SetColumnValue("QUANTITY", value);
            }
        }
	      
        [XmlAttribute("PointsUnitValue")]
        [Bindable(true)]
        public decimal PointsUnitValue 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("POINTS_UNIT_VALUE");
		    }
            set 
		    {
			    SetColumnValue("POINTS_UNIT_VALUE", value);
            }
        }
	      
        [XmlAttribute("GuidReferenciaProducto")]
        [Bindable(true)]
        public string GuidReferenciaProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_REFERENCIA_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("GUID_REFERENCIA_PRODUCTO", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string GuidFactura = @"GUID_FACTURA";
            
            public static string Quantity = @"QUANTITY";
            
            public static string PointsUnitValue = @"POINTS_UNIT_VALUE";
            
            public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
