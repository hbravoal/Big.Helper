using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvResolucione class.
	/// </summary>
    [Serializable]
	public partial class InvResolucioneCollection : ActiveList<InvResolucione, InvResolucioneCollection>
	{	   
		public InvResolucioneCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvResolucioneCollection</returns>
		public InvResolucioneCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvResolucione o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_RESOLUCIONES table.
	/// </summary>
	[Serializable]
	public partial class InvResolucione : ActiveRecord<InvResolucione>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvResolucione()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvResolucione(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvResolucione(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvResolucione(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_RESOLUCIONES", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdResolucion = new TableSchema.TableColumn(schema);
				colvarIdResolucion.ColumnName = "ID_RESOLUCION";
				colvarIdResolucion.DataType = DbType.Int32;
				colvarIdResolucion.MaxLength = 0;
				colvarIdResolucion.AutoIncrement = true;
				colvarIdResolucion.IsNullable = false;
				colvarIdResolucion.IsPrimaryKey = true;
				colvarIdResolucion.IsForeignKey = false;
				colvarIdResolucion.IsReadOnly = false;
				colvarIdResolucion.DefaultSetting = @"";
				colvarIdResolucion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdResolucion);
				
				TableSchema.TableColumn colvarResolucion = new TableSchema.TableColumn(schema);
				colvarResolucion.ColumnName = "RESOLUCION";
				colvarResolucion.DataType = DbType.String;
				colvarResolucion.MaxLength = 500;
				colvarResolucion.AutoIncrement = false;
				colvarResolucion.IsNullable = true;
				colvarResolucion.IsPrimaryKey = false;
				colvarResolucion.IsForeignKey = false;
				colvarResolucion.IsReadOnly = false;
				colvarResolucion.DefaultSetting = @"";
				colvarResolucion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarResolucion);
				
				TableSchema.TableColumn colvarFechaResolucion = new TableSchema.TableColumn(schema);
				colvarFechaResolucion.ColumnName = "FECHA_RESOLUCION";
				colvarFechaResolucion.DataType = DbType.DateTime;
				colvarFechaResolucion.MaxLength = 0;
				colvarFechaResolucion.AutoIncrement = false;
				colvarFechaResolucion.IsNullable = false;
				colvarFechaResolucion.IsPrimaryKey = false;
				colvarFechaResolucion.IsForeignKey = false;
				colvarFechaResolucion.IsReadOnly = false;
				colvarFechaResolucion.DefaultSetting = @"";
				colvarFechaResolucion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaResolucion);
				
				TableSchema.TableColumn colvarCodigoCentroCosto = new TableSchema.TableColumn(schema);
				colvarCodigoCentroCosto.ColumnName = "CODIGO_CENTRO_COSTO";
				colvarCodigoCentroCosto.DataType = DbType.AnsiString;
				colvarCodigoCentroCosto.MaxLength = 6;
				colvarCodigoCentroCosto.AutoIncrement = false;
				colvarCodigoCentroCosto.IsNullable = false;
				colvarCodigoCentroCosto.IsPrimaryKey = false;
				colvarCodigoCentroCosto.IsForeignKey = false;
				colvarCodigoCentroCosto.IsReadOnly = false;
				colvarCodigoCentroCosto.DefaultSetting = @"";
				colvarCodigoCentroCosto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigoCentroCosto);
				
				TableSchema.TableColumn colvarValorInicial = new TableSchema.TableColumn(schema);
				colvarValorInicial.ColumnName = "VALOR_INICIAL";
				colvarValorInicial.DataType = DbType.Int32;
				colvarValorInicial.MaxLength = 0;
				colvarValorInicial.AutoIncrement = false;
				colvarValorInicial.IsNullable = false;
				colvarValorInicial.IsPrimaryKey = false;
				colvarValorInicial.IsForeignKey = false;
				colvarValorInicial.IsReadOnly = false;
				colvarValorInicial.DefaultSetting = @"";
				colvarValorInicial.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorInicial);
				
				TableSchema.TableColumn colvarValorFinal = new TableSchema.TableColumn(schema);
				colvarValorFinal.ColumnName = "VALOR_FINAL";
				colvarValorFinal.DataType = DbType.Int32;
				colvarValorFinal.MaxLength = 0;
				colvarValorFinal.AutoIncrement = false;
				colvarValorFinal.IsNullable = false;
				colvarValorFinal.IsPrimaryKey = false;
				colvarValorFinal.IsForeignKey = false;
				colvarValorFinal.IsReadOnly = false;
				colvarValorFinal.DefaultSetting = @"";
				colvarValorFinal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorFinal);
				
				TableSchema.TableColumn colvarValorActual = new TableSchema.TableColumn(schema);
				colvarValorActual.ColumnName = "VALOR_ACTUAL";
				colvarValorActual.DataType = DbType.Int32;
				colvarValorActual.MaxLength = 0;
				colvarValorActual.AutoIncrement = false;
				colvarValorActual.IsNullable = false;
				colvarValorActual.IsPrimaryKey = false;
				colvarValorActual.IsForeignKey = false;
				colvarValorActual.IsReadOnly = false;
				colvarValorActual.DefaultSetting = @"";
				colvarValorActual.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorActual);
				
				TableSchema.TableColumn colvarIdTipoResolucion = new TableSchema.TableColumn(schema);
				colvarIdTipoResolucion.ColumnName = "ID_TIPO_RESOLUCION";
				colvarIdTipoResolucion.DataType = DbType.Int32;
				colvarIdTipoResolucion.MaxLength = 0;
				colvarIdTipoResolucion.AutoIncrement = false;
				colvarIdTipoResolucion.IsNullable = false;
				colvarIdTipoResolucion.IsPrimaryKey = false;
				colvarIdTipoResolucion.IsForeignKey = true;
				colvarIdTipoResolucion.IsReadOnly = false;
				colvarIdTipoResolucion.DefaultSetting = @"";
				
					colvarIdTipoResolucion.ForeignKeyTableName = "INV_MAS_TIPOS_RESOLUCION";
				schema.Columns.Add(colvarIdTipoResolucion);
				
				TableSchema.TableColumn colvarNumeroResolucion = new TableSchema.TableColumn(schema);
				colvarNumeroResolucion.ColumnName = "NUMERO_RESOLUCION";
				colvarNumeroResolucion.DataType = DbType.String;
				colvarNumeroResolucion.MaxLength = 50;
				colvarNumeroResolucion.AutoIncrement = false;
				colvarNumeroResolucion.IsNullable = false;
				colvarNumeroResolucion.IsPrimaryKey = false;
				colvarNumeroResolucion.IsForeignKey = false;
				colvarNumeroResolucion.IsReadOnly = false;
				colvarNumeroResolucion.DefaultSetting = @"";
				colvarNumeroResolucion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroResolucion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_RESOLUCIONES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdResolucion")]
		[Bindable(true)]
		public int IdResolucion 
		{
			get { return GetColumnValue<int>(Columns.IdResolucion); }
			set { SetColumnValue(Columns.IdResolucion, value); }
		}
		  
		[XmlAttribute("Resolucion")]
		[Bindable(true)]
		public string Resolucion 
		{
			get { return GetColumnValue<string>(Columns.Resolucion); }
			set { SetColumnValue(Columns.Resolucion, value); }
		}
		  
		[XmlAttribute("FechaResolucion")]
		[Bindable(true)]
		public DateTime FechaResolucion 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaResolucion); }
			set { SetColumnValue(Columns.FechaResolucion, value); }
		}
		  
		[XmlAttribute("CodigoCentroCosto")]
		[Bindable(true)]
		public string CodigoCentroCosto 
		{
			get { return GetColumnValue<string>(Columns.CodigoCentroCosto); }
			set { SetColumnValue(Columns.CodigoCentroCosto, value); }
		}
		  
		[XmlAttribute("ValorInicial")]
		[Bindable(true)]
		public int ValorInicial 
		{
			get { return GetColumnValue<int>(Columns.ValorInicial); }
			set { SetColumnValue(Columns.ValorInicial, value); }
		}
		  
		[XmlAttribute("ValorFinal")]
		[Bindable(true)]
		public int ValorFinal 
		{
			get { return GetColumnValue<int>(Columns.ValorFinal); }
			set { SetColumnValue(Columns.ValorFinal, value); }
		}
		  
		[XmlAttribute("ValorActual")]
		[Bindable(true)]
		public int ValorActual 
		{
			get { return GetColumnValue<int>(Columns.ValorActual); }
			set { SetColumnValue(Columns.ValorActual, value); }
		}
		  
		[XmlAttribute("IdTipoResolucion")]
		[Bindable(true)]
		public int IdTipoResolucion 
		{
			get { return GetColumnValue<int>(Columns.IdTipoResolucion); }
			set { SetColumnValue(Columns.IdTipoResolucion, value); }
		}
		  
		[XmlAttribute("NumeroResolucion")]
		[Bindable(true)]
		public string NumeroResolucion 
		{
			get { return GetColumnValue<string>(Columns.NumeroResolucion); }
			set { SetColumnValue(Columns.NumeroResolucion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvProgramaCollection InvProgramas()
		{
			return new Big.Helper.Domain.Generated.InvProgramaCollection().Where(InvPrograma.Columns.IdResolucion, IdResolucion).Load();
		}
		public Big.Helper.Domain.Generated.InvProgramaCollection InvProgramasFromInvResolucione()
		{
			return new Big.Helper.Domain.Generated.InvProgramaCollection().Where(InvPrograma.Columns.IdNotaCredito, IdResolucion).Load();
		}
		public Big.Helper.Domain.Generated.InvProgramaCollection InvProgramasFromInvResolucioneIdResolucionBanco()
		{
			return new Big.Helper.Domain.Generated.InvProgramaCollection().Where(InvPrograma.Columns.IdResolucionBanco, IdResolucion).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasTiposResolucion ActiveRecord object related to this InvResolucione
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasTiposResolucion InvMasTiposResolucion
		{
			get { return Big.Helper.Domain.Generated.InvMasTiposResolucion.FetchByID(this.IdTipoResolucion); }
			set { SetColumnValue("ID_TIPO_RESOLUCION", value.IdTipoResolucion); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varResolucion,DateTime varFechaResolucion,string varCodigoCentroCosto,int varValorInicial,int varValorFinal,int varValorActual,int varIdTipoResolucion,string varNumeroResolucion)
		{
			InvResolucione item = new InvResolucione();
			
			item.Resolucion = varResolucion;
			
			item.FechaResolucion = varFechaResolucion;
			
			item.CodigoCentroCosto = varCodigoCentroCosto;
			
			item.ValorInicial = varValorInicial;
			
			item.ValorFinal = varValorFinal;
			
			item.ValorActual = varValorActual;
			
			item.IdTipoResolucion = varIdTipoResolucion;
			
			item.NumeroResolucion = varNumeroResolucion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdResolucion,string varResolucion,DateTime varFechaResolucion,string varCodigoCentroCosto,int varValorInicial,int varValorFinal,int varValorActual,int varIdTipoResolucion,string varNumeroResolucion)
		{
			InvResolucione item = new InvResolucione();
			
				item.IdResolucion = varIdResolucion;
			
				item.Resolucion = varResolucion;
			
				item.FechaResolucion = varFechaResolucion;
			
				item.CodigoCentroCosto = varCodigoCentroCosto;
			
				item.ValorInicial = varValorInicial;
			
				item.ValorFinal = varValorFinal;
			
				item.ValorActual = varValorActual;
			
				item.IdTipoResolucion = varIdTipoResolucion;
			
				item.NumeroResolucion = varNumeroResolucion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdResolucionColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ResolucionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaResolucionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoCentroCostoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorInicialColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorFinalColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorActualColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoResolucionColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroResolucionColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdResolucion = @"ID_RESOLUCION";
			 public static string Resolucion = @"RESOLUCION";
			 public static string FechaResolucion = @"FECHA_RESOLUCION";
			 public static string CodigoCentroCosto = @"CODIGO_CENTRO_COSTO";
			 public static string ValorInicial = @"VALOR_INICIAL";
			 public static string ValorFinal = @"VALOR_FINAL";
			 public static string ValorActual = @"VALOR_ACTUAL";
			 public static string IdTipoResolucion = @"ID_TIPO_RESOLUCION";
			 public static string NumeroResolucion = @"NUMERO_RESOLUCION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
