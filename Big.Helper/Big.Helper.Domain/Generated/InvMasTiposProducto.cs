using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvMasTiposProducto class.
	/// </summary>
    [Serializable]
	public partial class InvMasTiposProductoCollection : ActiveList<InvMasTiposProducto, InvMasTiposProductoCollection>
	{	   
		public InvMasTiposProductoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasTiposProductoCollection</returns>
		public InvMasTiposProductoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasTiposProducto o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_PRODUCTO table.
	/// </summary>
	[Serializable]
	public partial class InvMasTiposProducto : ActiveRecord<InvMasTiposProducto>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasTiposProducto()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasTiposProducto(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasTiposProducto(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasTiposProducto(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_PRODUCTO", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoProducto = new TableSchema.TableColumn(schema);
				colvarIdTipoProducto.ColumnName = "ID_TIPO_PRODUCTO";
				colvarIdTipoProducto.DataType = DbType.Int32;
				colvarIdTipoProducto.MaxLength = 0;
				colvarIdTipoProducto.AutoIncrement = true;
				colvarIdTipoProducto.IsNullable = false;
				colvarIdTipoProducto.IsPrimaryKey = true;
				colvarIdTipoProducto.IsForeignKey = false;
				colvarIdTipoProducto.IsReadOnly = false;
				colvarIdTipoProducto.DefaultSetting = @"";
				colvarIdTipoProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTipoProducto);
				
				TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
				colvarCodigo.ColumnName = "CODIGO";
				colvarCodigo.DataType = DbType.AnsiString;
				colvarCodigo.MaxLength = 15;
				colvarCodigo.AutoIncrement = false;
				colvarCodigo.IsNullable = false;
				colvarCodigo.IsPrimaryKey = false;
				colvarCodigo.IsForeignKey = false;
				colvarCodigo.IsReadOnly = false;
				colvarCodigo.DefaultSetting = @"";
				colvarCodigo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigo);
				
				TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
				colvarNombre.ColumnName = "NOMBRE";
				colvarNombre.DataType = DbType.String;
				colvarNombre.MaxLength = 100;
				colvarNombre.AutoIncrement = false;
				colvarNombre.IsNullable = false;
				colvarNombre.IsPrimaryKey = false;
				colvarNombre.IsForeignKey = false;
				colvarNombre.IsReadOnly = false;
				colvarNombre.DefaultSetting = @"";
				colvarNombre.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombre);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "DESCRIPCION";
				colvarDescripcion.DataType = DbType.String;
				colvarDescripcion.MaxLength = 400;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = false;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
				colvarIdEstado.ColumnName = "ID_ESTADO";
				colvarIdEstado.DataType = DbType.Int32;
				colvarIdEstado.MaxLength = 0;
				colvarIdEstado.AutoIncrement = false;
				colvarIdEstado.IsNullable = false;
				colvarIdEstado.IsPrimaryKey = false;
				colvarIdEstado.IsForeignKey = true;
				colvarIdEstado.IsReadOnly = false;
				
						colvarIdEstado.DefaultSetting = @"((6))";
				
					colvarIdEstado.ForeignKeyTableName = "INV_MAS_ESTADOS";
				schema.Columns.Add(colvarIdEstado);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_TIPOS_PRODUCTO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoProducto")]
		[Bindable(true)]
		public int IdTipoProducto 
		{
			get { return GetColumnValue<int>(Columns.IdTipoProducto); }
			set { SetColumnValue(Columns.IdTipoProducto, value); }
		}
		  
		[XmlAttribute("Codigo")]
		[Bindable(true)]
		public string Codigo 
		{
			get { return GetColumnValue<string>(Columns.Codigo); }
			set { SetColumnValue(Columns.Codigo, value); }
		}
		  
		[XmlAttribute("Nombre")]
		[Bindable(true)]
		public string Nombre 
		{
			get { return GetColumnValue<string>(Columns.Nombre); }
			set { SetColumnValue(Columns.Nombre, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		  
		[XmlAttribute("IdEstado")]
		[Bindable(true)]
		public int IdEstado 
		{
			get { return GetColumnValue<int>(Columns.IdEstado); }
			set { SetColumnValue(Columns.IdEstado, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvProductoCollection InvProductos()
		{
			return new Big.Helper.Domain.Generated.InvProductoCollection().Where(InvProducto.Columns.IdTipoProducto, IdTipoProducto).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasEstado ActiveRecord object related to this InvMasTiposProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasEstado InvMasEstado
		{
			get { return Big.Helper.Domain.Generated.InvMasEstado.FetchByID(this.IdEstado); }
			set { SetColumnValue("ID_ESTADO", value.IdEstado); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varCodigo,string varNombre,string varDescripcion,int varIdEstado)
		{
			InvMasTiposProducto item = new InvMasTiposProducto();
			
			item.Codigo = varCodigo;
			
			item.Nombre = varNombre;
			
			item.Descripcion = varDescripcion;
			
			item.IdEstado = varIdEstado;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoProducto,string varCodigo,string varNombre,string varDescripcion,int varIdEstado)
		{
			InvMasTiposProducto item = new InvMasTiposProducto();
			
				item.IdTipoProducto = varIdTipoProducto;
			
				item.Codigo = varCodigo;
			
				item.Nombre = varNombre;
			
				item.Descripcion = varDescripcion;
			
				item.IdEstado = varIdEstado;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoProductoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEstadoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoProducto = @"ID_TIPO_PRODUCTO";
			 public static string Codigo = @"CODIGO";
			 public static string Nombre = @"NOMBRE";
			 public static string Descripcion = @"DESCRIPCION";
			 public static string IdEstado = @"ID_ESTADO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
