using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_PERMISOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasPermisoController
    {
        // Preload our schema..
        InvMasPermiso thisSchemaLoad = new InvMasPermiso();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasPermisoCollection FetchAll()
        {
            InvMasPermisoCollection coll = new InvMasPermisoCollection();
            Query qry = new Query(InvMasPermiso.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasPermisoCollection FetchByID(object IdPermiso)
        {
            InvMasPermisoCollection coll = new InvMasPermisoCollection().Where("ID_PERMISO", IdPermiso).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasPermisoCollection FetchByQuery(Query qry)
        {
            InvMasPermisoCollection coll = new InvMasPermisoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPermiso)
        {
            return (InvMasPermiso.Delete(IdPermiso) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPermiso)
        {
            return (InvMasPermiso.Destroy(IdPermiso) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdPermiso,string NombrePermiso)
	    {
		    InvMasPermiso item = new InvMasPermiso();
		    
            item.IdPermiso = IdPermiso;
            
            item.NombrePermiso = NombrePermiso;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPermiso,string NombrePermiso)
	    {
		    InvMasPermiso item = new InvMasPermiso();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPermiso = IdPermiso;
				
			item.NombrePermiso = NombrePermiso;
				
	        item.Save(UserName);
	    }
    }
}
