using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_TIPOS_RESOLUCION
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasTiposResolucionController
    {
        // Preload our schema..
        InvMasTiposResolucion thisSchemaLoad = new InvMasTiposResolucion();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasTiposResolucionCollection FetchAll()
        {
            InvMasTiposResolucionCollection coll = new InvMasTiposResolucionCollection();
            Query qry = new Query(InvMasTiposResolucion.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposResolucionCollection FetchByID(object IdTipoResolucion)
        {
            InvMasTiposResolucionCollection coll = new InvMasTiposResolucionCollection().Where("ID_TIPO_RESOLUCION", IdTipoResolucion).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasTiposResolucionCollection FetchByQuery(Query qry)
        {
            InvMasTiposResolucionCollection coll = new InvMasTiposResolucionCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdTipoResolucion)
        {
            return (InvMasTiposResolucion.Delete(IdTipoResolucion) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdTipoResolucion)
        {
            return (InvMasTiposResolucion.Destroy(IdTipoResolucion) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdTipoResolucion,string TipoResolucion)
	    {
		    InvMasTiposResolucion item = new InvMasTiposResolucion();
		    
            item.IdTipoResolucion = IdTipoResolucion;
            
            item.TipoResolucion = TipoResolucion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdTipoResolucion,string TipoResolucion)
	    {
		    InvMasTiposResolucion item = new InvMasTiposResolucion();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdTipoResolucion = IdTipoResolucion;
				
			item.TipoResolucion = TipoResolucion;
				
	        item.Save(UserName);
	    }
    }
}
