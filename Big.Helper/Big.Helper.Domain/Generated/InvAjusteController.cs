using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_AJUSTES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvAjusteController
    {
        // Preload our schema..
        InvAjuste thisSchemaLoad = new InvAjuste();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvAjusteCollection FetchAll()
        {
            InvAjusteCollection coll = new InvAjusteCollection();
            Query qry = new Query(InvAjuste.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvAjusteCollection FetchByID(object GuidAjuste)
        {
            InvAjusteCollection coll = new InvAjusteCollection().Where("GUID_AJUSTE", GuidAjuste).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvAjusteCollection FetchByQuery(Query qry)
        {
            InvAjusteCollection coll = new InvAjusteCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidAjuste)
        {
            return (InvAjuste.Delete(GuidAjuste) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidAjuste)
        {
            return (InvAjuste.Destroy(GuidAjuste) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidAjuste,int Consecutivo,int IdBodega,decimal TotalBase,decimal PrecioBase,DateTime FechaAjuste,string ValorEnLetrasTotal,string Observaciones,bool Entrada,string ValorEnLetrasPrecio,bool? Anulada,string GuidUsuarioAnulada,DateTime? FechaAnulada,bool Aprobado,string GuidUsuarioAprobacion,DateTime? FechaAprobacion)
	    {
		    InvAjuste item = new InvAjuste();
		    
            item.GuidAjuste = GuidAjuste;
            
            item.Consecutivo = Consecutivo;
            
            item.IdBodega = IdBodega;
            
            item.TotalBase = TotalBase;
            
            item.PrecioBase = PrecioBase;
            
            item.FechaAjuste = FechaAjuste;
            
            item.ValorEnLetrasTotal = ValorEnLetrasTotal;
            
            item.Observaciones = Observaciones;
            
            item.Entrada = Entrada;
            
            item.ValorEnLetrasPrecio = ValorEnLetrasPrecio;
            
            item.Anulada = Anulada;
            
            item.GuidUsuarioAnulada = GuidUsuarioAnulada;
            
            item.FechaAnulada = FechaAnulada;
            
            item.Aprobado = Aprobado;
            
            item.GuidUsuarioAprobacion = GuidUsuarioAprobacion;
            
            item.FechaAprobacion = FechaAprobacion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidAjuste,int Consecutivo,int IdBodega,decimal TotalBase,decimal PrecioBase,DateTime FechaAjuste,string ValorEnLetrasTotal,string Observaciones,bool Entrada,string ValorEnLetrasPrecio,bool? Anulada,string GuidUsuarioAnulada,DateTime? FechaAnulada,bool Aprobado,string GuidUsuarioAprobacion,DateTime? FechaAprobacion)
	    {
		    InvAjuste item = new InvAjuste();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidAjuste = GuidAjuste;
				
			item.Consecutivo = Consecutivo;
				
			item.IdBodega = IdBodega;
				
			item.TotalBase = TotalBase;
				
			item.PrecioBase = PrecioBase;
				
			item.FechaAjuste = FechaAjuste;
				
			item.ValorEnLetrasTotal = ValorEnLetrasTotal;
				
			item.Observaciones = Observaciones;
				
			item.Entrada = Entrada;
				
			item.ValorEnLetrasPrecio = ValorEnLetrasPrecio;
				
			item.Anulada = Anulada;
				
			item.GuidUsuarioAnulada = GuidUsuarioAnulada;
				
			item.FechaAnulada = FechaAnulada;
				
			item.Aprobado = Aprobado;
				
			item.GuidUsuarioAprobacion = GuidUsuarioAprobacion;
				
			item.FechaAprobacion = FechaAprobacion;
				
	        item.Save(UserName);
	    }
    }
}
