using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_HISTORICOS_PRECIO
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvHistoricosPrecioController
    {
        // Preload our schema..
        InvHistoricosPrecio thisSchemaLoad = new InvHistoricosPrecio();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvHistoricosPrecioCollection FetchAll()
        {
            InvHistoricosPrecioCollection coll = new InvHistoricosPrecioCollection();
            Query qry = new Query(InvHistoricosPrecio.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvHistoricosPrecioCollection FetchByID(object IdHistorialProducto)
        {
            InvHistoricosPrecioCollection coll = new InvHistoricosPrecioCollection().Where("ID_HISTORIAL_PRODUCTO", IdHistorialProducto).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvHistoricosPrecioCollection FetchByQuery(Query qry)
        {
            InvHistoricosPrecioCollection coll = new InvHistoricosPrecioCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdHistorialProducto)
        {
            return (InvHistoricosPrecio.Delete(IdHistorialProducto) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdHistorialProducto)
        {
            return (InvHistoricosPrecio.Destroy(IdHistorialProducto) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidProducto,string GuidPrecioProducto,decimal? CostoBase,decimal? ValorAsegurado,decimal? PrecioBase,decimal? CostoEnvioBase,int? IdMarca,int? IdTipoDelivery,DateTime FechaCambio,int IdUsuarioCambio,bool? Activo)
	    {
		    InvHistoricosPrecio item = new InvHistoricosPrecio();
		    
            item.GuidProducto = GuidProducto;
            
            item.GuidPrecioProducto = GuidPrecioProducto;
            
            item.CostoBase = CostoBase;
            
            item.ValorAsegurado = ValorAsegurado;
            
            item.PrecioBase = PrecioBase;
            
            item.CostoEnvioBase = CostoEnvioBase;
            
            item.IdMarca = IdMarca;
            
            item.IdTipoDelivery = IdTipoDelivery;
            
            item.FechaCambio = FechaCambio;
            
            item.IdUsuarioCambio = IdUsuarioCambio;
            
            item.Activo = Activo;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(decimal IdHistorialProducto,string GuidProducto,string GuidPrecioProducto,decimal? CostoBase,decimal? ValorAsegurado,decimal? PrecioBase,decimal? CostoEnvioBase,int? IdMarca,int? IdTipoDelivery,DateTime FechaCambio,int IdUsuarioCambio,bool? Activo)
	    {
		    InvHistoricosPrecio item = new InvHistoricosPrecio();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdHistorialProducto = IdHistorialProducto;
				
			item.GuidProducto = GuidProducto;
				
			item.GuidPrecioProducto = GuidPrecioProducto;
				
			item.CostoBase = CostoBase;
				
			item.ValorAsegurado = ValorAsegurado;
				
			item.PrecioBase = PrecioBase;
				
			item.CostoEnvioBase = CostoEnvioBase;
				
			item.IdMarca = IdMarca;
				
			item.IdTipoDelivery = IdTipoDelivery;
				
			item.FechaCambio = FechaCambio;
				
			item.IdUsuarioCambio = IdUsuarioCambio;
				
			item.Activo = Activo;
				
	        item.Save(UserName);
	    }
    }
}
