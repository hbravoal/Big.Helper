using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvPeriodoFacturacionPunto class.
	/// </summary>
    [Serializable]
	public partial class InvPeriodoFacturacionPuntoCollection : ActiveList<InvPeriodoFacturacionPunto, InvPeriodoFacturacionPuntoCollection>
	{	   
		public InvPeriodoFacturacionPuntoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvPeriodoFacturacionPuntoCollection</returns>
		public InvPeriodoFacturacionPuntoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvPeriodoFacturacionPunto o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PERIODO_FACTURACION_PUNTOS table.
	/// </summary>
	[Serializable]
	public partial class InvPeriodoFacturacionPunto : ActiveRecord<InvPeriodoFacturacionPunto>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvPeriodoFacturacionPunto()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvPeriodoFacturacionPunto(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvPeriodoFacturacionPunto(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvPeriodoFacturacionPunto(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PERIODO_FACTURACION_PUNTOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdFacturacionPuntos = new TableSchema.TableColumn(schema);
				colvarIdFacturacionPuntos.ColumnName = "ID_FACTURACION_PUNTOS";
				colvarIdFacturacionPuntos.DataType = DbType.Int32;
				colvarIdFacturacionPuntos.MaxLength = 0;
				colvarIdFacturacionPuntos.AutoIncrement = true;
				colvarIdFacturacionPuntos.IsNullable = false;
				colvarIdFacturacionPuntos.IsPrimaryKey = true;
				colvarIdFacturacionPuntos.IsForeignKey = false;
				colvarIdFacturacionPuntos.IsReadOnly = false;
				colvarIdFacturacionPuntos.DefaultSetting = @"";
				colvarIdFacturacionPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdFacturacionPuntos);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarFechaInicio = new TableSchema.TableColumn(schema);
				colvarFechaInicio.ColumnName = "FECHA_INICIO";
				colvarFechaInicio.DataType = DbType.DateTime;
				colvarFechaInicio.MaxLength = 0;
				colvarFechaInicio.AutoIncrement = false;
				colvarFechaInicio.IsNullable = false;
				colvarFechaInicio.IsPrimaryKey = false;
				colvarFechaInicio.IsForeignKey = false;
				colvarFechaInicio.IsReadOnly = false;
				colvarFechaInicio.DefaultSetting = @"";
				colvarFechaInicio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaInicio);
				
				TableSchema.TableColumn colvarFechaFin = new TableSchema.TableColumn(schema);
				colvarFechaFin.ColumnName = "FECHA_FIN";
				colvarFechaFin.DataType = DbType.DateTime;
				colvarFechaFin.MaxLength = 0;
				colvarFechaFin.AutoIncrement = false;
				colvarFechaFin.IsNullable = false;
				colvarFechaFin.IsPrimaryKey = false;
				colvarFechaFin.IsForeignKey = false;
				colvarFechaFin.IsReadOnly = false;
				colvarFechaFin.DefaultSetting = @"";
				colvarFechaFin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaFin);
				
				TableSchema.TableColumn colvarEsPeriodoBanco = new TableSchema.TableColumn(schema);
				colvarEsPeriodoBanco.ColumnName = "ES_PERIODO_BANCO";
				colvarEsPeriodoBanco.DataType = DbType.Boolean;
				colvarEsPeriodoBanco.MaxLength = 0;
				colvarEsPeriodoBanco.AutoIncrement = false;
				colvarEsPeriodoBanco.IsNullable = true;
				colvarEsPeriodoBanco.IsPrimaryKey = false;
				colvarEsPeriodoBanco.IsForeignKey = false;
				colvarEsPeriodoBanco.IsReadOnly = false;
				colvarEsPeriodoBanco.DefaultSetting = @"";
				colvarEsPeriodoBanco.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEsPeriodoBanco);
				
				TableSchema.TableColumn colvarPeriodoActual = new TableSchema.TableColumn(schema);
				colvarPeriodoActual.ColumnName = "PERIODO_ACTUAL";
				colvarPeriodoActual.DataType = DbType.Boolean;
				colvarPeriodoActual.MaxLength = 0;
				colvarPeriodoActual.AutoIncrement = false;
				colvarPeriodoActual.IsNullable = true;
				colvarPeriodoActual.IsPrimaryKey = false;
				colvarPeriodoActual.IsForeignKey = false;
				colvarPeriodoActual.IsReadOnly = false;
				colvarPeriodoActual.DefaultSetting = @"";
				colvarPeriodoActual.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeriodoActual);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_PERIODO_FACTURACION_PUNTOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdFacturacionPuntos")]
		[Bindable(true)]
		public int IdFacturacionPuntos 
		{
			get { return GetColumnValue<int>(Columns.IdFacturacionPuntos); }
			set { SetColumnValue(Columns.IdFacturacionPuntos, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("FechaInicio")]
		[Bindable(true)]
		public DateTime FechaInicio 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaInicio); }
			set { SetColumnValue(Columns.FechaInicio, value); }
		}
		  
		[XmlAttribute("FechaFin")]
		[Bindable(true)]
		public DateTime FechaFin 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaFin); }
			set { SetColumnValue(Columns.FechaFin, value); }
		}
		  
		[XmlAttribute("EsPeriodoBanco")]
		[Bindable(true)]
		public bool? EsPeriodoBanco 
		{
			get { return GetColumnValue<bool?>(Columns.EsPeriodoBanco); }
			set { SetColumnValue(Columns.EsPeriodoBanco, value); }
		}
		  
		[XmlAttribute("PeriodoActual")]
		[Bindable(true)]
		public bool? PeriodoActual 
		{
			get { return GetColumnValue<bool?>(Columns.PeriodoActual); }
			set { SetColumnValue(Columns.PeriodoActual, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvFacturasProveedorPuntoCollection InvFacturasProveedorPuntos()
		{
			return new Big.Helper.Domain.Generated.InvFacturasProveedorPuntoCollection().Where(InvFacturasProveedorPunto.Columns.IdPeriodoFacturacionPuntos, IdFacturacionPuntos).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvPrograma ActiveRecord object related to this InvPeriodoFacturacionPunto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvPrograma InvPrograma
		{
			get { return Big.Helper.Domain.Generated.InvPrograma.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdPrograma,DateTime varFechaInicio,DateTime varFechaFin,bool? varEsPeriodoBanco,bool? varPeriodoActual)
		{
			InvPeriodoFacturacionPunto item = new InvPeriodoFacturacionPunto();
			
			item.IdPrograma = varIdPrograma;
			
			item.FechaInicio = varFechaInicio;
			
			item.FechaFin = varFechaFin;
			
			item.EsPeriodoBanco = varEsPeriodoBanco;
			
			item.PeriodoActual = varPeriodoActual;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdFacturacionPuntos,int varIdPrograma,DateTime varFechaInicio,DateTime varFechaFin,bool? varEsPeriodoBanco,bool? varPeriodoActual)
		{
			InvPeriodoFacturacionPunto item = new InvPeriodoFacturacionPunto();
			
				item.IdFacturacionPuntos = varIdFacturacionPuntos;
			
				item.IdPrograma = varIdPrograma;
			
				item.FechaInicio = varFechaInicio;
			
				item.FechaFin = varFechaFin;
			
				item.EsPeriodoBanco = varEsPeriodoBanco;
			
				item.PeriodoActual = varPeriodoActual;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdFacturacionPuntosColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaInicioColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaFinColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn EsPeriodoBancoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PeriodoActualColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdFacturacionPuntos = @"ID_FACTURACION_PUNTOS";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string FechaInicio = @"FECHA_INICIO";
			 public static string FechaFin = @"FECHA_FIN";
			 public static string EsPeriodoBanco = @"ES_PERIODO_BANCO";
			 public static string PeriodoActual = @"PERIODO_ACTUAL";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
