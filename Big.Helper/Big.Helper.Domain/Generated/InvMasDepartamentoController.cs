using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_DEPARTAMENTOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasDepartamentoController
    {
        // Preload our schema..
        InvMasDepartamento thisSchemaLoad = new InvMasDepartamento();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasDepartamentoCollection FetchAll()
        {
            InvMasDepartamentoCollection coll = new InvMasDepartamentoCollection();
            Query qry = new Query(InvMasDepartamento.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasDepartamentoCollection FetchByID(object IdDepartamento)
        {
            InvMasDepartamentoCollection coll = new InvMasDepartamentoCollection().Where("ID_DEPARTAMENTO", IdDepartamento).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasDepartamentoCollection FetchByQuery(Query qry)
        {
            InvMasDepartamentoCollection coll = new InvMasDepartamentoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdDepartamento)
        {
            return (InvMasDepartamento.Delete(IdDepartamento) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdDepartamento)
        {
            return (InvMasDepartamento.Destroy(IdDepartamento) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Nombre,int IdPais)
	    {
		    InvMasDepartamento item = new InvMasDepartamento();
		    
            item.Nombre = Nombre;
            
            item.IdPais = IdPais;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdDepartamento,string Nombre,int IdPais)
	    {
		    InvMasDepartamento item = new InvMasDepartamento();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdDepartamento = IdDepartamento;
				
			item.Nombre = Nombre;
				
			item.IdPais = IdPais;
				
	        item.Save(UserName);
	    }
    }
}
