using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwExportarProveedore class.
    /// </summary>
    [Serializable]
    public partial class InvVwExportarProveedoreCollection : ReadOnlyList<InvVwExportarProveedore, InvVwExportarProveedoreCollection>
    {        
        public InvVwExportarProveedoreCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_EXPORTAR_PROVEEDORES view.
    /// </summary>
    [Serializable]
    public partial class InvVwExportarProveedore : ReadOnlyRecord<InvVwExportarProveedore>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_EXPORTAR_PROVEEDORES", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
                colvarIdProveedor.ColumnName = "Id Proveedor";
                colvarIdProveedor.DataType = DbType.Int32;
                colvarIdProveedor.MaxLength = 0;
                colvarIdProveedor.AutoIncrement = false;
                colvarIdProveedor.IsNullable = false;
                colvarIdProveedor.IsPrimaryKey = false;
                colvarIdProveedor.IsForeignKey = false;
                colvarIdProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdProveedor);
                
                TableSchema.TableColumn colvarCódigoDelProveedorOEstablecimiento = new TableSchema.TableColumn(schema);
                colvarCódigoDelProveedorOEstablecimiento.ColumnName = "Código del proveedor o establecimiento";
                colvarCódigoDelProveedorOEstablecimiento.DataType = DbType.AnsiString;
                colvarCódigoDelProveedorOEstablecimiento.MaxLength = 20;
                colvarCódigoDelProveedorOEstablecimiento.AutoIncrement = false;
                colvarCódigoDelProveedorOEstablecimiento.IsNullable = false;
                colvarCódigoDelProveedorOEstablecimiento.IsPrimaryKey = false;
                colvarCódigoDelProveedorOEstablecimiento.IsForeignKey = false;
                colvarCódigoDelProveedorOEstablecimiento.IsReadOnly = false;
                
                schema.Columns.Add(colvarCódigoDelProveedorOEstablecimiento);
                
                TableSchema.TableColumn colvarRazónSocialDelProveedor = new TableSchema.TableColumn(schema);
                colvarRazónSocialDelProveedor.ColumnName = "Razón social del proveedor";
                colvarRazónSocialDelProveedor.DataType = DbType.String;
                colvarRazónSocialDelProveedor.MaxLength = 200;
                colvarRazónSocialDelProveedor.AutoIncrement = false;
                colvarRazónSocialDelProveedor.IsNullable = false;
                colvarRazónSocialDelProveedor.IsPrimaryKey = false;
                colvarRazónSocialDelProveedor.IsForeignKey = false;
                colvarRazónSocialDelProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarRazónSocialDelProveedor);
                
                TableSchema.TableColumn colvarNit = new TableSchema.TableColumn(schema);
                colvarNit.ColumnName = "NIT";
                colvarNit.DataType = DbType.AnsiString;
                colvarNit.MaxLength = 15;
                colvarNit.AutoIncrement = false;
                colvarNit.IsNullable = false;
                colvarNit.IsPrimaryKey = false;
                colvarNit.IsForeignKey = false;
                colvarNit.IsReadOnly = false;
                
                schema.Columns.Add(colvarNit);
                
                TableSchema.TableColumn colvarDirección = new TableSchema.TableColumn(schema);
                colvarDirección.ColumnName = "Dirección";
                colvarDirección.DataType = DbType.String;
                colvarDirección.MaxLength = 200;
                colvarDirección.AutoIncrement = false;
                colvarDirección.IsNullable = false;
                colvarDirección.IsPrimaryKey = false;
                colvarDirección.IsForeignKey = false;
                colvarDirección.IsReadOnly = false;
                
                schema.Columns.Add(colvarDirección);
                
                TableSchema.TableColumn colvarTeléfono = new TableSchema.TableColumn(schema);
                colvarTeléfono.ColumnName = "Teléfono";
                colvarTeléfono.DataType = DbType.AnsiString;
                colvarTeléfono.MaxLength = 20;
                colvarTeléfono.AutoIncrement = false;
                colvarTeléfono.IsNullable = false;
                colvarTeléfono.IsPrimaryKey = false;
                colvarTeléfono.IsForeignKey = false;
                colvarTeléfono.IsReadOnly = false;
                
                schema.Columns.Add(colvarTeléfono);
                
                TableSchema.TableColumn colvarFax = new TableSchema.TableColumn(schema);
                colvarFax.ColumnName = "Fax";
                colvarFax.DataType = DbType.AnsiString;
                colvarFax.MaxLength = 20;
                colvarFax.AutoIncrement = false;
                colvarFax.IsNullable = false;
                colvarFax.IsPrimaryKey = false;
                colvarFax.IsForeignKey = false;
                colvarFax.IsReadOnly = false;
                
                schema.Columns.Add(colvarFax);
                
                TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
                colvarIdCiudad.ColumnName = "ID_CIUDAD";
                colvarIdCiudad.DataType = DbType.Int32;
                colvarIdCiudad.MaxLength = 0;
                colvarIdCiudad.AutoIncrement = false;
                colvarIdCiudad.IsNullable = false;
                colvarIdCiudad.IsPrimaryKey = false;
                colvarIdCiudad.IsForeignKey = false;
                colvarIdCiudad.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdCiudad);
                
                TableSchema.TableColumn colvarPaís = new TableSchema.TableColumn(schema);
                colvarPaís.ColumnName = "País";
                colvarPaís.DataType = DbType.String;
                colvarPaís.MaxLength = 50;
                colvarPaís.AutoIncrement = false;
                colvarPaís.IsNullable = false;
                colvarPaís.IsPrimaryKey = false;
                colvarPaís.IsForeignKey = false;
                colvarPaís.IsReadOnly = false;
                
                schema.Columns.Add(colvarPaís);
                
                TableSchema.TableColumn colvarDepartamento = new TableSchema.TableColumn(schema);
                colvarDepartamento.ColumnName = "Departamento";
                colvarDepartamento.DataType = DbType.String;
                colvarDepartamento.MaxLength = 50;
                colvarDepartamento.AutoIncrement = false;
                colvarDepartamento.IsNullable = false;
                colvarDepartamento.IsPrimaryKey = false;
                colvarDepartamento.IsForeignKey = false;
                colvarDepartamento.IsReadOnly = false;
                
                schema.Columns.Add(colvarDepartamento);
                
                TableSchema.TableColumn colvarCiudad = new TableSchema.TableColumn(schema);
                colvarCiudad.ColumnName = "Ciudad";
                colvarCiudad.DataType = DbType.String;
                colvarCiudad.MaxLength = 50;
                colvarCiudad.AutoIncrement = false;
                colvarCiudad.IsNullable = false;
                colvarCiudad.IsPrimaryKey = false;
                colvarCiudad.IsForeignKey = false;
                colvarCiudad.IsReadOnly = false;
                
                schema.Columns.Add(colvarCiudad);
                
                TableSchema.TableColumn colvarEntidadBancaria = new TableSchema.TableColumn(schema);
                colvarEntidadBancaria.ColumnName = "Entidad bancaria";
                colvarEntidadBancaria.DataType = DbType.String;
                colvarEntidadBancaria.MaxLength = 100;
                colvarEntidadBancaria.AutoIncrement = false;
                colvarEntidadBancaria.IsNullable = false;
                colvarEntidadBancaria.IsPrimaryKey = false;
                colvarEntidadBancaria.IsForeignKey = false;
                colvarEntidadBancaria.IsReadOnly = false;
                
                schema.Columns.Add(colvarEntidadBancaria);
                
                TableSchema.TableColumn colvarNúmeroDeCuenta = new TableSchema.TableColumn(schema);
                colvarNúmeroDeCuenta.ColumnName = "Número de Cuenta";
                colvarNúmeroDeCuenta.DataType = DbType.AnsiString;
                colvarNúmeroDeCuenta.MaxLength = 20;
                colvarNúmeroDeCuenta.AutoIncrement = false;
                colvarNúmeroDeCuenta.IsNullable = false;
                colvarNúmeroDeCuenta.IsPrimaryKey = false;
                colvarNúmeroDeCuenta.IsForeignKey = false;
                colvarNúmeroDeCuenta.IsReadOnly = false;
                
                schema.Columns.Add(colvarNúmeroDeCuenta);
                
                TableSchema.TableColumn colvarTipoDeCuenta = new TableSchema.TableColumn(schema);
                colvarTipoDeCuenta.ColumnName = "Tipo de Cuenta";
                colvarTipoDeCuenta.DataType = DbType.String;
                colvarTipoDeCuenta.MaxLength = 200;
                colvarTipoDeCuenta.AutoIncrement = false;
                colvarTipoDeCuenta.IsNullable = false;
                colvarTipoDeCuenta.IsPrimaryKey = false;
                colvarTipoDeCuenta.IsForeignKey = false;
                colvarTipoDeCuenta.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoDeCuenta);
                
                TableSchema.TableColumn colvarEstado = new TableSchema.TableColumn(schema);
                colvarEstado.ColumnName = "Estado";
                colvarEstado.DataType = DbType.AnsiString;
                colvarEstado.MaxLength = 50;
                colvarEstado.AutoIncrement = false;
                colvarEstado.IsNullable = false;
                colvarEstado.IsPrimaryKey = false;
                colvarEstado.IsForeignKey = false;
                colvarEstado.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstado);
                
                TableSchema.TableColumn colvarPlazoDePago = new TableSchema.TableColumn(schema);
                colvarPlazoDePago.ColumnName = "Plazo de Pago";
                colvarPlazoDePago.DataType = DbType.Int32;
                colvarPlazoDePago.MaxLength = 0;
                colvarPlazoDePago.AutoIncrement = false;
                colvarPlazoDePago.IsNullable = false;
                colvarPlazoDePago.IsPrimaryKey = false;
                colvarPlazoDePago.IsForeignKey = false;
                colvarPlazoDePago.IsReadOnly = false;
                
                schema.Columns.Add(colvarPlazoDePago);
                
                TableSchema.TableColumn colvarCupo = new TableSchema.TableColumn(schema);
                colvarCupo.ColumnName = "Cupo";
                colvarCupo.DataType = DbType.Decimal;
                colvarCupo.MaxLength = 0;
                colvarCupo.AutoIncrement = false;
                colvarCupo.IsNullable = false;
                colvarCupo.IsPrimaryKey = false;
                colvarCupo.IsForeignKey = false;
                colvarCupo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCupo);
                
                TableSchema.TableColumn colvarEsGranContribuyente = new TableSchema.TableColumn(schema);
                colvarEsGranContribuyente.ColumnName = "Es gran contribuyente";
                colvarEsGranContribuyente.DataType = DbType.AnsiString;
                colvarEsGranContribuyente.MaxLength = 2;
                colvarEsGranContribuyente.AutoIncrement = false;
                colvarEsGranContribuyente.IsNullable = false;
                colvarEsGranContribuyente.IsPrimaryKey = false;
                colvarEsGranContribuyente.IsForeignKey = false;
                colvarEsGranContribuyente.IsReadOnly = false;
                
                schema.Columns.Add(colvarEsGranContribuyente);
                
                TableSchema.TableColumn colvarPerteneceAlRégimenComún = new TableSchema.TableColumn(schema);
                colvarPerteneceAlRégimenComún.ColumnName = "Pertenece al régimen común";
                colvarPerteneceAlRégimenComún.DataType = DbType.AnsiString;
                colvarPerteneceAlRégimenComún.MaxLength = 2;
                colvarPerteneceAlRégimenComún.AutoIncrement = false;
                colvarPerteneceAlRégimenComún.IsNullable = false;
                colvarPerteneceAlRégimenComún.IsPrimaryKey = false;
                colvarPerteneceAlRégimenComún.IsForeignKey = false;
                colvarPerteneceAlRégimenComún.IsReadOnly = false;
                
                schema.Columns.Add(colvarPerteneceAlRégimenComún);
                
                TableSchema.TableColumn colvarEsAutoretendor = new TableSchema.TableColumn(schema);
                colvarEsAutoretendor.ColumnName = "Es autoretendor";
                colvarEsAutoretendor.DataType = DbType.AnsiString;
                colvarEsAutoretendor.MaxLength = 2;
                colvarEsAutoretendor.AutoIncrement = false;
                colvarEsAutoretendor.IsNullable = false;
                colvarEsAutoretendor.IsPrimaryKey = false;
                colvarEsAutoretendor.IsForeignKey = false;
                colvarEsAutoretendor.IsReadOnly = false;
                
                schema.Columns.Add(colvarEsAutoretendor);
                
                TableSchema.TableColumn colvarEsRetenedorDeIva = new TableSchema.TableColumn(schema);
                colvarEsRetenedorDeIva.ColumnName = "Es retenedor de Iva";
                colvarEsRetenedorDeIva.DataType = DbType.AnsiString;
                colvarEsRetenedorDeIva.MaxLength = 2;
                colvarEsRetenedorDeIva.AutoIncrement = false;
                colvarEsRetenedorDeIva.IsNullable = false;
                colvarEsRetenedorDeIva.IsPrimaryKey = false;
                colvarEsRetenedorDeIva.IsForeignKey = false;
                colvarEsRetenedorDeIva.IsReadOnly = false;
                
                schema.Columns.Add(colvarEsRetenedorDeIva);
                
                TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
                colvarIdEstado.ColumnName = "ID_ESTADO";
                colvarIdEstado.DataType = DbType.Int32;
                colvarIdEstado.MaxLength = 0;
                colvarIdEstado.AutoIncrement = false;
                colvarIdEstado.IsNullable = false;
                colvarIdEstado.IsPrimaryKey = false;
                colvarIdEstado.IsForeignKey = false;
                colvarIdEstado.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdEstado);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_EXPORTAR_PROVEEDORES",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwExportarProveedore()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwExportarProveedore(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwExportarProveedore(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwExportarProveedore(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("IdProveedor")]
        [Bindable(true)]
        public int IdProveedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("Id Proveedor");
		    }
            set 
		    {
			    SetColumnValue("Id Proveedor", value);
            }
        }
	      
        [XmlAttribute("CódigoDelProveedorOEstablecimiento")]
        [Bindable(true)]
        public string CódigoDelProveedorOEstablecimiento 
	    {
		    get
		    {
			    return GetColumnValue<string>("Código del proveedor o establecimiento");
		    }
            set 
		    {
			    SetColumnValue("Código del proveedor o establecimiento", value);
            }
        }
	      
        [XmlAttribute("RazónSocialDelProveedor")]
        [Bindable(true)]
        public string RazónSocialDelProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("Razón social del proveedor");
		    }
            set 
		    {
			    SetColumnValue("Razón social del proveedor", value);
            }
        }
	      
        [XmlAttribute("Nit")]
        [Bindable(true)]
        public string Nit 
	    {
		    get
		    {
			    return GetColumnValue<string>("NIT");
		    }
            set 
		    {
			    SetColumnValue("NIT", value);
            }
        }
	      
        [XmlAttribute("Dirección")]
        [Bindable(true)]
        public string Dirección 
	    {
		    get
		    {
			    return GetColumnValue<string>("Dirección");
		    }
            set 
		    {
			    SetColumnValue("Dirección", value);
            }
        }
	      
        [XmlAttribute("Teléfono")]
        [Bindable(true)]
        public string Teléfono 
	    {
		    get
		    {
			    return GetColumnValue<string>("Teléfono");
		    }
            set 
		    {
			    SetColumnValue("Teléfono", value);
            }
        }
	      
        [XmlAttribute("Fax")]
        [Bindable(true)]
        public string Fax 
	    {
		    get
		    {
			    return GetColumnValue<string>("Fax");
		    }
            set 
		    {
			    SetColumnValue("Fax", value);
            }
        }
	      
        [XmlAttribute("IdCiudad")]
        [Bindable(true)]
        public int IdCiudad 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_CIUDAD");
		    }
            set 
		    {
			    SetColumnValue("ID_CIUDAD", value);
            }
        }
	      
        [XmlAttribute("País")]
        [Bindable(true)]
        public string País 
	    {
		    get
		    {
			    return GetColumnValue<string>("País");
		    }
            set 
		    {
			    SetColumnValue("País", value);
            }
        }
	      
        [XmlAttribute("Departamento")]
        [Bindable(true)]
        public string Departamento 
	    {
		    get
		    {
			    return GetColumnValue<string>("Departamento");
		    }
            set 
		    {
			    SetColumnValue("Departamento", value);
            }
        }
	      
        [XmlAttribute("Ciudad")]
        [Bindable(true)]
        public string Ciudad 
	    {
		    get
		    {
			    return GetColumnValue<string>("Ciudad");
		    }
            set 
		    {
			    SetColumnValue("Ciudad", value);
            }
        }
	      
        [XmlAttribute("EntidadBancaria")]
        [Bindable(true)]
        public string EntidadBancaria 
	    {
		    get
		    {
			    return GetColumnValue<string>("Entidad bancaria");
		    }
            set 
		    {
			    SetColumnValue("Entidad bancaria", value);
            }
        }
	      
        [XmlAttribute("NúmeroDeCuenta")]
        [Bindable(true)]
        public string NúmeroDeCuenta 
	    {
		    get
		    {
			    return GetColumnValue<string>("Número de Cuenta");
		    }
            set 
		    {
			    SetColumnValue("Número de Cuenta", value);
            }
        }
	      
        [XmlAttribute("TipoDeCuenta")]
        [Bindable(true)]
        public string TipoDeCuenta 
	    {
		    get
		    {
			    return GetColumnValue<string>("Tipo de Cuenta");
		    }
            set 
		    {
			    SetColumnValue("Tipo de Cuenta", value);
            }
        }
	      
        [XmlAttribute("Estado")]
        [Bindable(true)]
        public string Estado 
	    {
		    get
		    {
			    return GetColumnValue<string>("Estado");
		    }
            set 
		    {
			    SetColumnValue("Estado", value);
            }
        }
	      
        [XmlAttribute("PlazoDePago")]
        [Bindable(true)]
        public int PlazoDePago 
	    {
		    get
		    {
			    return GetColumnValue<int>("Plazo de Pago");
		    }
            set 
		    {
			    SetColumnValue("Plazo de Pago", value);
            }
        }
	      
        [XmlAttribute("Cupo")]
        [Bindable(true)]
        public decimal Cupo 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Cupo");
		    }
            set 
		    {
			    SetColumnValue("Cupo", value);
            }
        }
	      
        [XmlAttribute("EsGranContribuyente")]
        [Bindable(true)]
        public string EsGranContribuyente 
	    {
		    get
		    {
			    return GetColumnValue<string>("Es gran contribuyente");
		    }
            set 
		    {
			    SetColumnValue("Es gran contribuyente", value);
            }
        }
	      
        [XmlAttribute("PerteneceAlRégimenComún")]
        [Bindable(true)]
        public string PerteneceAlRégimenComún 
	    {
		    get
		    {
			    return GetColumnValue<string>("Pertenece al régimen común");
		    }
            set 
		    {
			    SetColumnValue("Pertenece al régimen común", value);
            }
        }
	      
        [XmlAttribute("EsAutoretendor")]
        [Bindable(true)]
        public string EsAutoretendor 
	    {
		    get
		    {
			    return GetColumnValue<string>("Es autoretendor");
		    }
            set 
		    {
			    SetColumnValue("Es autoretendor", value);
            }
        }
	      
        [XmlAttribute("EsRetenedorDeIva")]
        [Bindable(true)]
        public string EsRetenedorDeIva 
	    {
		    get
		    {
			    return GetColumnValue<string>("Es retenedor de Iva");
		    }
            set 
		    {
			    SetColumnValue("Es retenedor de Iva", value);
            }
        }
	      
        [XmlAttribute("IdEstado")]
        [Bindable(true)]
        public int IdEstado 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_ESTADO");
		    }
            set 
		    {
			    SetColumnValue("ID_ESTADO", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string IdProveedor = @"Id Proveedor";
            
            public static string CódigoDelProveedorOEstablecimiento = @"Código del proveedor o establecimiento";
            
            public static string RazónSocialDelProveedor = @"Razón social del proveedor";
            
            public static string Nit = @"NIT";
            
            public static string Dirección = @"Dirección";
            
            public static string Teléfono = @"Teléfono";
            
            public static string Fax = @"Fax";
            
            public static string IdCiudad = @"ID_CIUDAD";
            
            public static string País = @"País";
            
            public static string Departamento = @"Departamento";
            
            public static string Ciudad = @"Ciudad";
            
            public static string EntidadBancaria = @"Entidad bancaria";
            
            public static string NúmeroDeCuenta = @"Número de Cuenta";
            
            public static string TipoDeCuenta = @"Tipo de Cuenta";
            
            public static string Estado = @"Estado";
            
            public static string PlazoDePago = @"Plazo de Pago";
            
            public static string Cupo = @"Cupo";
            
            public static string EsGranContribuyente = @"Es gran contribuyente";
            
            public static string PerteneceAlRégimenComún = @"Pertenece al régimen común";
            
            public static string EsAutoretendor = @"Es autoretendor";
            
            public static string EsRetenedorDeIva = @"Es retenedor de Iva";
            
            public static string IdEstado = @"ID_ESTADO";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
