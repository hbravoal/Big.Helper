using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CACHE_DETALLES_FACTURA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCacheDetallesFacturaController
    {
        // Preload our schema..
        InvCacheDetallesFactura thisSchemaLoad = new InvCacheDetallesFactura();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCacheDetallesFacturaCollection FetchAll()
        {
            InvCacheDetallesFacturaCollection coll = new InvCacheDetallesFacturaCollection();
            Query qry = new Query(InvCacheDetallesFactura.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCacheDetallesFacturaCollection FetchByID(object IdCacheDetalle)
        {
            InvCacheDetallesFacturaCollection coll = new InvCacheDetallesFacturaCollection().Where("ID_CACHE_DETALLE", IdCacheDetalle).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCacheDetallesFacturaCollection FetchByQuery(Query qry)
        {
            InvCacheDetallesFacturaCollection coll = new InvCacheDetallesFacturaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdCacheDetalle)
        {
            return (InvCacheDetallesFactura.Delete(IdCacheDetalle) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdCacheDetalle)
        {
            return (InvCacheDetallesFactura.Destroy(IdCacheDetalle) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int Cantidad,decimal ValorBase,decimal EnvioBase,decimal Iva,string GuidReferenciaProducto,int IdUsuario,decimal FactorCompraPuntos)
	    {
		    InvCacheDetallesFactura item = new InvCacheDetallesFactura();
		    
            item.Cantidad = Cantidad;
            
            item.ValorBase = ValorBase;
            
            item.EnvioBase = EnvioBase;
            
            item.Iva = Iva;
            
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
            item.IdUsuario = IdUsuario;
            
            item.FactorCompraPuntos = FactorCompraPuntos;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdCacheDetalle,int Cantidad,decimal ValorBase,decimal EnvioBase,decimal Iva,string GuidReferenciaProducto,int IdUsuario,decimal FactorCompraPuntos)
	    {
		    InvCacheDetallesFactura item = new InvCacheDetallesFactura();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdCacheDetalle = IdCacheDetalle;
				
			item.Cantidad = Cantidad;
				
			item.ValorBase = ValorBase;
				
			item.EnvioBase = EnvioBase;
				
			item.Iva = Iva;
				
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
			item.IdUsuario = IdUsuario;
				
			item.FactorCompraPuntos = FactorCompraPuntos;
				
	        item.Save(UserName);
	    }
    }
}
