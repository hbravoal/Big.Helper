using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwFacturasMilla class.
    /// </summary>
    [Serializable]
    public partial class InvVwFacturasMillaCollection : ReadOnlyList<InvVwFacturasMilla, InvVwFacturasMillaCollection>
    {        
        public InvVwFacturasMillaCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_FACTURAS_MILLAS view.
    /// </summary>
    [Serializable]
    public partial class InvVwFacturasMilla : ReadOnlyRecord<InvVwFacturasMilla>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_FACTURAS_MILLAS", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarCodigoEmpresa = new TableSchema.TableColumn(schema);
                colvarCodigoEmpresa.ColumnName = "CODIGO_EMPRESA";
                colvarCodigoEmpresa.DataType = DbType.AnsiString;
                colvarCodigoEmpresa.MaxLength = 2;
                colvarCodigoEmpresa.AutoIncrement = false;
                colvarCodigoEmpresa.IsNullable = false;
                colvarCodigoEmpresa.IsPrimaryKey = false;
                colvarCodigoEmpresa.IsForeignKey = false;
                colvarCodigoEmpresa.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoEmpresa);
                
                TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
                colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
                colvarNumeroFactura.DataType = DbType.Int32;
                colvarNumeroFactura.MaxLength = 0;
                colvarNumeroFactura.AutoIncrement = false;
                colvarNumeroFactura.IsNullable = true;
                colvarNumeroFactura.IsPrimaryKey = false;
                colvarNumeroFactura.IsForeignKey = false;
                colvarNumeroFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumeroFactura);
                
                TableSchema.TableColumn colvarTipoDocumento = new TableSchema.TableColumn(schema);
                colvarTipoDocumento.ColumnName = "TIPO_DOCUMENTO";
                colvarTipoDocumento.DataType = DbType.AnsiString;
                colvarTipoDocumento.MaxLength = 2;
                colvarTipoDocumento.AutoIncrement = false;
                colvarTipoDocumento.IsNullable = false;
                colvarTipoDocumento.IsPrimaryKey = false;
                colvarTipoDocumento.IsForeignKey = false;
                colvarTipoDocumento.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoDocumento);
                
                TableSchema.TableColumn colvarNumeroUnico = new TableSchema.TableColumn(schema);
                colvarNumeroUnico.ColumnName = "NUMERO_UNICO";
                colvarNumeroUnico.DataType = DbType.AnsiString;
                colvarNumeroUnico.MaxLength = 32;
                colvarNumeroUnico.AutoIncrement = false;
                colvarNumeroUnico.IsNullable = true;
                colvarNumeroUnico.IsPrimaryKey = false;
                colvarNumeroUnico.IsForeignKey = false;
                colvarNumeroUnico.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumeroUnico);
                
                TableSchema.TableColumn colvarNit = new TableSchema.TableColumn(schema);
                colvarNit.ColumnName = "NIT";
                colvarNit.DataType = DbType.AnsiString;
                colvarNit.MaxLength = 15;
                colvarNit.AutoIncrement = false;
                colvarNit.IsNullable = false;
                colvarNit.IsPrimaryKey = false;
                colvarNit.IsForeignKey = false;
                colvarNit.IsReadOnly = false;
                
                schema.Columns.Add(colvarNit);
                
                TableSchema.TableColumn colvarFechaVencimiento = new TableSchema.TableColumn(schema);
                colvarFechaVencimiento.ColumnName = "FECHA_VENCIMIENTO";
                colvarFechaVencimiento.DataType = DbType.DateTime;
                colvarFechaVencimiento.MaxLength = 0;
                colvarFechaVencimiento.AutoIncrement = false;
                colvarFechaVencimiento.IsNullable = true;
                colvarFechaVencimiento.IsPrimaryKey = false;
                colvarFechaVencimiento.IsForeignKey = false;
                colvarFechaVencimiento.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaVencimiento);
                
                TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
                colvarSubtotal.ColumnName = "SUBTOTAL";
                colvarSubtotal.DataType = DbType.Decimal;
                colvarSubtotal.MaxLength = 0;
                colvarSubtotal.AutoIncrement = false;
                colvarSubtotal.IsNullable = false;
                colvarSubtotal.IsPrimaryKey = false;
                colvarSubtotal.IsForeignKey = false;
                colvarSubtotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarSubtotal);
                
                TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
                colvarIva.ColumnName = "IVA";
                colvarIva.DataType = DbType.Decimal;
                colvarIva.MaxLength = 0;
                colvarIva.AutoIncrement = false;
                colvarIva.IsNullable = false;
                colvarIva.IsPrimaryKey = false;
                colvarIva.IsForeignKey = false;
                colvarIva.IsReadOnly = false;
                
                schema.Columns.Add(colvarIva);
                
                TableSchema.TableColumn colvarRetencion = new TableSchema.TableColumn(schema);
                colvarRetencion.ColumnName = "RETENCION";
                colvarRetencion.DataType = DbType.AnsiString;
                colvarRetencion.MaxLength = 1;
                colvarRetencion.AutoIncrement = false;
                colvarRetencion.IsNullable = false;
                colvarRetencion.IsPrimaryKey = false;
                colvarRetencion.IsForeignKey = false;
                colvarRetencion.IsReadOnly = false;
                
                schema.Columns.Add(colvarRetencion);
                
                TableSchema.TableColumn colvarAnulada = new TableSchema.TableColumn(schema);
                colvarAnulada.ColumnName = "ANULADA";
                colvarAnulada.DataType = DbType.Boolean;
                colvarAnulada.MaxLength = 0;
                colvarAnulada.AutoIncrement = false;
                colvarAnulada.IsNullable = true;
                colvarAnulada.IsPrimaryKey = false;
                colvarAnulada.IsForeignKey = false;
                colvarAnulada.IsReadOnly = false;
                
                schema.Columns.Add(colvarAnulada);
                
                TableSchema.TableColumn colvarFechaAnulacion = new TableSchema.TableColumn(schema);
                colvarFechaAnulacion.ColumnName = "FECHA_ANULACION";
                colvarFechaAnulacion.DataType = DbType.DateTime;
                colvarFechaAnulacion.MaxLength = 0;
                colvarFechaAnulacion.AutoIncrement = false;
                colvarFechaAnulacion.IsNullable = true;
                colvarFechaAnulacion.IsPrimaryKey = false;
                colvarFechaAnulacion.IsForeignKey = false;
                colvarFechaAnulacion.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaAnulacion);
                
                TableSchema.TableColumn colvarCodCc = new TableSchema.TableColumn(schema);
                colvarCodCc.ColumnName = "COD_CC";
                colvarCodCc.DataType = DbType.Int32;
                colvarCodCc.MaxLength = 0;
                colvarCodCc.AutoIncrement = false;
                colvarCodCc.IsNullable = false;
                colvarCodCc.IsPrimaryKey = false;
                colvarCodCc.IsForeignKey = false;
                colvarCodCc.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodCc);
                
                TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
                colvarIdPrograma.ColumnName = "ID_PROGRAMA";
                colvarIdPrograma.DataType = DbType.Int32;
                colvarIdPrograma.MaxLength = 0;
                colvarIdPrograma.AutoIncrement = false;
                colvarIdPrograma.IsNullable = false;
                colvarIdPrograma.IsPrimaryKey = false;
                colvarIdPrograma.IsForeignKey = false;
                colvarIdPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdPrograma);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_FACTURAS_MILLAS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwFacturasMilla()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwFacturasMilla(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwFacturasMilla(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwFacturasMilla(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("CodigoEmpresa")]
        [Bindable(true)]
        public string CodigoEmpresa 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO_EMPRESA");
		    }
            set 
		    {
			    SetColumnValue("CODIGO_EMPRESA", value);
            }
        }
	      
        [XmlAttribute("NumeroFactura")]
        [Bindable(true)]
        public int? NumeroFactura 
	    {
		    get
		    {
			    return GetColumnValue<int?>("NUMERO_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("NUMERO_FACTURA", value);
            }
        }
	      
        [XmlAttribute("TipoDocumento")]
        [Bindable(true)]
        public string TipoDocumento 
	    {
		    get
		    {
			    return GetColumnValue<string>("TIPO_DOCUMENTO");
		    }
            set 
		    {
			    SetColumnValue("TIPO_DOCUMENTO", value);
            }
        }
	      
        [XmlAttribute("NumeroUnico")]
        [Bindable(true)]
        public string NumeroUnico 
	    {
		    get
		    {
			    return GetColumnValue<string>("NUMERO_UNICO");
		    }
            set 
		    {
			    SetColumnValue("NUMERO_UNICO", value);
            }
        }
	      
        [XmlAttribute("Nit")]
        [Bindable(true)]
        public string Nit 
	    {
		    get
		    {
			    return GetColumnValue<string>("NIT");
		    }
            set 
		    {
			    SetColumnValue("NIT", value);
            }
        }
	      
        [XmlAttribute("FechaVencimiento")]
        [Bindable(true)]
        public DateTime? FechaVencimiento 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA_VENCIMIENTO");
		    }
            set 
		    {
			    SetColumnValue("FECHA_VENCIMIENTO", value);
            }
        }
	      
        [XmlAttribute("Subtotal")]
        [Bindable(true)]
        public decimal Subtotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("SUBTOTAL");
		    }
            set 
		    {
			    SetColumnValue("SUBTOTAL", value);
            }
        }
	      
        [XmlAttribute("Iva")]
        [Bindable(true)]
        public decimal Iva 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("IVA");
		    }
            set 
		    {
			    SetColumnValue("IVA", value);
            }
        }
	      
        [XmlAttribute("Retencion")]
        [Bindable(true)]
        public string Retencion 
	    {
		    get
		    {
			    return GetColumnValue<string>("RETENCION");
		    }
            set 
		    {
			    SetColumnValue("RETENCION", value);
            }
        }
	      
        [XmlAttribute("Anulada")]
        [Bindable(true)]
        public bool? Anulada 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("ANULADA");
		    }
            set 
		    {
			    SetColumnValue("ANULADA", value);
            }
        }
	      
        [XmlAttribute("FechaAnulacion")]
        [Bindable(true)]
        public DateTime? FechaAnulacion 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA_ANULACION");
		    }
            set 
		    {
			    SetColumnValue("FECHA_ANULACION", value);
            }
        }
	      
        [XmlAttribute("CodCc")]
        [Bindable(true)]
        public int CodCc 
	    {
		    get
		    {
			    return GetColumnValue<int>("COD_CC");
		    }
            set 
		    {
			    SetColumnValue("COD_CC", value);
            }
        }
	      
        [XmlAttribute("IdPrograma")]
        [Bindable(true)]
        public int IdPrograma 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("ID_PROGRAMA", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string CodigoEmpresa = @"CODIGO_EMPRESA";
            
            public static string NumeroFactura = @"NUMERO_FACTURA";
            
            public static string TipoDocumento = @"TIPO_DOCUMENTO";
            
            public static string NumeroUnico = @"NUMERO_UNICO";
            
            public static string Nit = @"NIT";
            
            public static string FechaVencimiento = @"FECHA_VENCIMIENTO";
            
            public static string Subtotal = @"SUBTOTAL";
            
            public static string Iva = @"IVA";
            
            public static string Retencion = @"RETENCION";
            
            public static string Anulada = @"ANULADA";
            
            public static string FechaAnulacion = @"FECHA_ANULACION";
            
            public static string CodCc = @"COD_CC";
            
            public static string IdPrograma = @"ID_PROGRAMA";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
