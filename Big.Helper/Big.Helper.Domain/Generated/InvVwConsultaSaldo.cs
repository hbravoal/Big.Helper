using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwConsultaSaldo class.
    /// </summary>
    [Serializable]
    public partial class InvVwConsultaSaldoCollection : ReadOnlyList<InvVwConsultaSaldo, InvVwConsultaSaldoCollection>
    {        
        public InvVwConsultaSaldoCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_CONSULTA_SALDOS view.
    /// </summary>
    [Serializable]
    public partial class InvVwConsultaSaldo : ReadOnlyRecord<InvVwConsultaSaldo>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_CONSULTA_SALDOS", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarNombrePrograma = new TableSchema.TableColumn(schema);
                colvarNombrePrograma.ColumnName = "NOMBRE_PROGRAMA";
                colvarNombrePrograma.DataType = DbType.String;
                colvarNombrePrograma.MaxLength = 100;
                colvarNombrePrograma.AutoIncrement = false;
                colvarNombrePrograma.IsNullable = false;
                colvarNombrePrograma.IsPrimaryKey = false;
                colvarNombrePrograma.IsForeignKey = false;
                colvarNombrePrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombrePrograma);
                
                TableSchema.TableColumn colvarCodigoProveedor = new TableSchema.TableColumn(schema);
                colvarCodigoProveedor.ColumnName = "CODIGO_PROVEEDOR";
                colvarCodigoProveedor.DataType = DbType.AnsiString;
                colvarCodigoProveedor.MaxLength = 20;
                colvarCodigoProveedor.AutoIncrement = false;
                colvarCodigoProveedor.IsNullable = false;
                colvarCodigoProveedor.IsPrimaryKey = false;
                colvarCodigoProveedor.IsForeignKey = false;
                colvarCodigoProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoProveedor);
                
                TableSchema.TableColumn colvarRazonSocial = new TableSchema.TableColumn(schema);
                colvarRazonSocial.ColumnName = "RAZON_SOCIAL";
                colvarRazonSocial.DataType = DbType.String;
                colvarRazonSocial.MaxLength = 200;
                colvarRazonSocial.AutoIncrement = false;
                colvarRazonSocial.IsNullable = false;
                colvarRazonSocial.IsPrimaryKey = false;
                colvarRazonSocial.IsForeignKey = false;
                colvarRazonSocial.IsReadOnly = false;
                
                schema.Columns.Add(colvarRazonSocial);
                
                TableSchema.TableColumn colvarNombreMarca = new TableSchema.TableColumn(schema);
                colvarNombreMarca.ColumnName = "NOMBRE_MARCA";
                colvarNombreMarca.DataType = DbType.String;
                colvarNombreMarca.MaxLength = 200;
                colvarNombreMarca.AutoIncrement = false;
                colvarNombreMarca.IsNullable = false;
                colvarNombreMarca.IsPrimaryKey = false;
                colvarNombreMarca.IsForeignKey = false;
                colvarNombreMarca.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreMarca);
                
                TableSchema.TableColumn colvarCodigoProducto = new TableSchema.TableColumn(schema);
                colvarCodigoProducto.ColumnName = "CODIGO_PRODUCTO";
                colvarCodigoProducto.DataType = DbType.AnsiString;
                colvarCodigoProducto.MaxLength = 15;
                colvarCodigoProducto.AutoIncrement = false;
                colvarCodigoProducto.IsNullable = false;
                colvarCodigoProducto.IsPrimaryKey = false;
                colvarCodigoProducto.IsForeignKey = false;
                colvarCodigoProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigoProducto);
                
                TableSchema.TableColumn colvarNombreProducto = new TableSchema.TableColumn(schema);
                colvarNombreProducto.ColumnName = "NOMBRE_PRODUCTO";
                colvarNombreProducto.DataType = DbType.String;
                colvarNombreProducto.MaxLength = 100;
                colvarNombreProducto.AutoIncrement = false;
                colvarNombreProducto.IsNullable = false;
                colvarNombreProducto.IsPrimaryKey = false;
                colvarNombreProducto.IsForeignKey = false;
                colvarNombreProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreProducto);
                
                TableSchema.TableColumn colvarNombreReferencia = new TableSchema.TableColumn(schema);
                colvarNombreReferencia.ColumnName = "NOMBRE_REFERENCIA";
                colvarNombreReferencia.DataType = DbType.String;
                colvarNombreReferencia.MaxLength = 100;
                colvarNombreReferencia.AutoIncrement = false;
                colvarNombreReferencia.IsNullable = false;
                colvarNombreReferencia.IsPrimaryKey = false;
                colvarNombreReferencia.IsForeignKey = false;
                colvarNombreReferencia.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreReferencia);
                
                TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
                colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
                colvarGuidReferenciaProducto.DataType = DbType.String;
                colvarGuidReferenciaProducto.MaxLength = 36;
                colvarGuidReferenciaProducto.AutoIncrement = false;
                colvarGuidReferenciaProducto.IsNullable = false;
                colvarGuidReferenciaProducto.IsPrimaryKey = false;
                colvarGuidReferenciaProducto.IsForeignKey = false;
                colvarGuidReferenciaProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidReferenciaProducto);
                
                TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
                colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
                colvarGuidProducto.DataType = DbType.String;
                colvarGuidProducto.MaxLength = 36;
                colvarGuidProducto.AutoIncrement = false;
                colvarGuidProducto.IsNullable = false;
                colvarGuidProducto.IsPrimaryKey = false;
                colvarGuidProducto.IsForeignKey = false;
                colvarGuidProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidProducto);
                
                TableSchema.TableColumn colvarFechaInicio = new TableSchema.TableColumn(schema);
                colvarFechaInicio.ColumnName = "FECHA_INICIO";
                colvarFechaInicio.DataType = DbType.DateTime;
                colvarFechaInicio.MaxLength = 0;
                colvarFechaInicio.AutoIncrement = false;
                colvarFechaInicio.IsNullable = false;
                colvarFechaInicio.IsPrimaryKey = false;
                colvarFechaInicio.IsForeignKey = false;
                colvarFechaInicio.IsReadOnly = false;
                
                schema.Columns.Add(colvarFechaInicio);
                
                TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
                colvarIdPrograma.ColumnName = "ID_PROGRAMA";
                colvarIdPrograma.DataType = DbType.Int32;
                colvarIdPrograma.MaxLength = 0;
                colvarIdPrograma.AutoIncrement = false;
                colvarIdPrograma.IsNullable = false;
                colvarIdPrograma.IsPrimaryKey = false;
                colvarIdPrograma.IsForeignKey = false;
                colvarIdPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdPrograma);
                
                TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
                colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
                colvarIdProveedor.DataType = DbType.Int32;
                colvarIdProveedor.MaxLength = 0;
                colvarIdProveedor.AutoIncrement = false;
                colvarIdProveedor.IsNullable = false;
                colvarIdProveedor.IsPrimaryKey = false;
                colvarIdProveedor.IsForeignKey = false;
                colvarIdProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdProveedor);
                
                TableSchema.TableColumn colvarIdMarca = new TableSchema.TableColumn(schema);
                colvarIdMarca.ColumnName = "ID_MARCA";
                colvarIdMarca.DataType = DbType.Int32;
                colvarIdMarca.MaxLength = 0;
                colvarIdMarca.AutoIncrement = false;
                colvarIdMarca.IsNullable = false;
                colvarIdMarca.IsPrimaryKey = false;
                colvarIdMarca.IsForeignKey = false;
                colvarIdMarca.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdMarca);
                
                TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
                colvarFecha.ColumnName = "FECHA";
                colvarFecha.DataType = DbType.DateTime;
                colvarFecha.MaxLength = 0;
                colvarFecha.AutoIncrement = false;
                colvarFecha.IsNullable = false;
                colvarFecha.IsPrimaryKey = false;
                colvarFecha.IsForeignKey = false;
                colvarFecha.IsReadOnly = false;
                
                schema.Columns.Add(colvarFecha);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_CONSULTA_SALDOS",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwConsultaSaldo()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwConsultaSaldo(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwConsultaSaldo(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwConsultaSaldo(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("NombrePrograma")]
        [Bindable(true)]
        public string NombrePrograma 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_PROGRAMA", value);
            }
        }
	      
        [XmlAttribute("CodigoProveedor")]
        [Bindable(true)]
        public string CodigoProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("CODIGO_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("RazonSocial")]
        [Bindable(true)]
        public string RazonSocial 
	    {
		    get
		    {
			    return GetColumnValue<string>("RAZON_SOCIAL");
		    }
            set 
		    {
			    SetColumnValue("RAZON_SOCIAL", value);
            }
        }
	      
        [XmlAttribute("NombreMarca")]
        [Bindable(true)]
        public string NombreMarca 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_MARCA");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_MARCA", value);
            }
        }
	      
        [XmlAttribute("CodigoProducto")]
        [Bindable(true)]
        public string CodigoProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("CODIGO_PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("NombreProducto")]
        [Bindable(true)]
        public string NombreProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("NombreReferencia")]
        [Bindable(true)]
        public string NombreReferencia 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_REFERENCIA");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_REFERENCIA", value);
            }
        }
	      
        [XmlAttribute("GuidReferenciaProducto")]
        [Bindable(true)]
        public string GuidReferenciaProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_REFERENCIA_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("GUID_REFERENCIA_PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("GuidProducto")]
        [Bindable(true)]
        public string GuidProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("GUID_PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("FechaInicio")]
        [Bindable(true)]
        public DateTime FechaInicio 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("FECHA_INICIO");
		    }
            set 
		    {
			    SetColumnValue("FECHA_INICIO", value);
            }
        }
	      
        [XmlAttribute("IdPrograma")]
        [Bindable(true)]
        public int IdPrograma 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("ID_PROGRAMA", value);
            }
        }
	      
        [XmlAttribute("IdProveedor")]
        [Bindable(true)]
        public int IdProveedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("ID_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("IdMarca")]
        [Bindable(true)]
        public int IdMarca 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_MARCA");
		    }
            set 
		    {
			    SetColumnValue("ID_MARCA", value);
            }
        }
	      
        [XmlAttribute("Fecha")]
        [Bindable(true)]
        public DateTime Fecha 
	    {
		    get
		    {
			    return GetColumnValue<DateTime>("FECHA");
		    }
            set 
		    {
			    SetColumnValue("FECHA", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string NombrePrograma = @"NOMBRE_PROGRAMA";
            
            public static string CodigoProveedor = @"CODIGO_PROVEEDOR";
            
            public static string RazonSocial = @"RAZON_SOCIAL";
            
            public static string NombreMarca = @"NOMBRE_MARCA";
            
            public static string CodigoProducto = @"CODIGO_PRODUCTO";
            
            public static string NombreProducto = @"NOMBRE_PRODUCTO";
            
            public static string NombreReferencia = @"NOMBRE_REFERENCIA";
            
            public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
            
            public static string GuidProducto = @"GUID_PRODUCTO";
            
            public static string FechaInicio = @"FECHA_INICIO";
            
            public static string IdPrograma = @"ID_PROGRAMA";
            
            public static string IdProveedor = @"ID_PROVEEDOR";
            
            public static string IdMarca = @"ID_MARCA";
            
            public static string Fecha = @"FECHA";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
