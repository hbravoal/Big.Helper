using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CIERRE_MENSUAL
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCierreMensualController
    {
        // Preload our schema..
        InvCierreMensual thisSchemaLoad = new InvCierreMensual();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCierreMensualCollection FetchAll()
        {
            InvCierreMensualCollection coll = new InvCierreMensualCollection();
            Query qry = new Query(InvCierreMensual.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCierreMensualCollection FetchByID(object Id)
        {
            InvCierreMensualCollection coll = new InvCierreMensualCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCierreMensualCollection FetchByQuery(Query qry)
        {
            InvCierreMensualCollection coll = new InvCierreMensualCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCierreMensual.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCierreMensual.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdPrograma,int Ano,int Mes)
	    {
		    InvCierreMensual item = new InvCierreMensual();
		    
            item.IdPrograma = IdPrograma;
            
            item.Ano = Ano;
            
            item.Mes = Mes;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int IdPrograma,int Ano,int Mes)
	    {
		    InvCierreMensual item = new InvCierreMensual();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.IdPrograma = IdPrograma;
				
			item.Ano = Ano;
				
			item.Mes = Mes;
				
	        item.Save(UserName);
	    }
    }
}
