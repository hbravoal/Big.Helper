using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_FACTURAS_PROVEEDOR_PUNTOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvFacturasProveedorPuntoController
    {
        // Preload our schema..
        InvFacturasProveedorPunto thisSchemaLoad = new InvFacturasProveedorPunto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvFacturasProveedorPuntoCollection FetchAll()
        {
            InvFacturasProveedorPuntoCollection coll = new InvFacturasProveedorPuntoCollection();
            Query qry = new Query(InvFacturasProveedorPunto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvFacturasProveedorPuntoCollection FetchByID(object GuidFacturaProveedorPuntos)
        {
            InvFacturasProveedorPuntoCollection coll = new InvFacturasProveedorPuntoCollection().Where("GUID_FACTURA_PROVEEDOR_PUNTOS", GuidFacturaProveedorPuntos).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvFacturasProveedorPuntoCollection FetchByQuery(Query qry)
        {
            InvFacturasProveedorPuntoCollection coll = new InvFacturasProveedorPuntoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidFacturaProveedorPuntos)
        {
            return (InvFacturasProveedorPunto.Delete(GuidFacturaProveedorPuntos) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidFacturaProveedorPuntos)
        {
            return (InvFacturasProveedorPunto.Destroy(GuidFacturaProveedorPuntos) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidFacturaProveedorPuntos,int NumeroFactura,decimal TotalBase,decimal Iva,DateTime FechaFactura,string ValorEnLetras,int IdPrograma,int IdEmpresa,int? IdPeriodoFacturacionPuntos,bool NotaCreditoProveedorPuntos,bool? Anulada,DateTime? FechaAnulacion,bool TipoCurrier,string Observaciones,string MotivoNotaCredito,int? FacturaNotaCredito,bool? NotaCredito,bool BitNotaCreditoValor)
	    {
		    InvFacturasProveedorPunto item = new InvFacturasProveedorPunto();
		    
            item.GuidFacturaProveedorPuntos = GuidFacturaProveedorPuntos;
            
            item.NumeroFactura = NumeroFactura;
            
            item.TotalBase = TotalBase;
            
            item.Iva = Iva;
            
            item.FechaFactura = FechaFactura;
            
            item.ValorEnLetras = ValorEnLetras;
            
            item.IdPrograma = IdPrograma;
            
            item.IdEmpresa = IdEmpresa;
            
            item.IdPeriodoFacturacionPuntos = IdPeriodoFacturacionPuntos;
            
            item.NotaCreditoProveedorPuntos = NotaCreditoProveedorPuntos;
            
            item.Anulada = Anulada;
            
            item.FechaAnulacion = FechaAnulacion;
            
            item.TipoCurrier = TipoCurrier;
            
            item.Observaciones = Observaciones;
            
            item.MotivoNotaCredito = MotivoNotaCredito;
            
            item.FacturaNotaCredito = FacturaNotaCredito;
            
            item.NotaCredito = NotaCredito;
            
            item.BitNotaCreditoValor = BitNotaCreditoValor;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidFacturaProveedorPuntos,int NumeroFactura,decimal TotalBase,decimal Iva,DateTime FechaFactura,string ValorEnLetras,int IdPrograma,int IdEmpresa,int? IdPeriodoFacturacionPuntos,bool NotaCreditoProveedorPuntos,bool? Anulada,DateTime? FechaAnulacion,bool TipoCurrier,string Observaciones,string MotivoNotaCredito,int? FacturaNotaCredito,bool? NotaCredito,bool BitNotaCreditoValor)
	    {
		    InvFacturasProveedorPunto item = new InvFacturasProveedorPunto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidFacturaProveedorPuntos = GuidFacturaProveedorPuntos;
				
			item.NumeroFactura = NumeroFactura;
				
			item.TotalBase = TotalBase;
				
			item.Iva = Iva;
				
			item.FechaFactura = FechaFactura;
				
			item.ValorEnLetras = ValorEnLetras;
				
			item.IdPrograma = IdPrograma;
				
			item.IdEmpresa = IdEmpresa;
				
			item.IdPeriodoFacturacionPuntos = IdPeriodoFacturacionPuntos;
				
			item.NotaCreditoProveedorPuntos = NotaCreditoProveedorPuntos;
				
			item.Anulada = Anulada;
				
			item.FechaAnulacion = FechaAnulacion;
				
			item.TipoCurrier = TipoCurrier;
				
			item.Observaciones = Observaciones;
				
			item.MotivoNotaCredito = MotivoNotaCredito;
				
			item.FacturaNotaCredito = FacturaNotaCredito;
				
			item.NotaCredito = NotaCredito;
				
			item.BitNotaCreditoValor = BitNotaCreditoValor;
				
	        item.Save(UserName);
	    }
    }
}
