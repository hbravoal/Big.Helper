using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CGUNO_LOGS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoLogController
    {
        // Preload our schema..
        InvCgunoLog thisSchemaLoad = new InvCgunoLog();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoLogCollection FetchAll()
        {
            InvCgunoLogCollection coll = new InvCgunoLogCollection();
            Query qry = new Query(InvCgunoLog.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoLogCollection FetchByID(object Guid)
        {
            InvCgunoLogCollection coll = new InvCgunoLogCollection().Where("GUID", Guid).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoLogCollection FetchByQuery(Query qry)
        {
            InvCgunoLogCollection coll = new InvCgunoLogCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Guid)
        {
            return (InvCgunoLog.Delete(Guid) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Guid)
        {
            return (InvCgunoLog.Destroy(Guid) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,string PeriodoCausacion,string Estado,int Usuario,DateTime Fecha)
	    {
		    InvCgunoLog item = new InvCgunoLog();
		    
            item.Guid = Guid;
            
            item.PeriodoCausacion = PeriodoCausacion;
            
            item.Estado = Estado;
            
            item.Usuario = Usuario;
            
            item.Fecha = Fecha;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,string PeriodoCausacion,string Estado,int Usuario,DateTime Fecha)
	    {
		    InvCgunoLog item = new InvCgunoLog();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.PeriodoCausacion = PeriodoCausacion;
				
			item.Estado = Estado;
				
			item.Usuario = Usuario;
				
			item.Fecha = Fecha;
				
	        item.Save(UserName);
	    }
    }
}
