using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_DETALLES_FACTURA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvDetallesFacturaController
    {
        // Preload our schema..
        InvDetallesFactura thisSchemaLoad = new InvDetallesFactura();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvDetallesFacturaCollection FetchAll()
        {
            InvDetallesFacturaCollection coll = new InvDetallesFacturaCollection();
            Query qry = new Query(InvDetallesFactura.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetallesFacturaCollection FetchByID(object GuidDetalleFactura)
        {
            InvDetallesFacturaCollection coll = new InvDetallesFacturaCollection().Where("GUID_DETALLE_FACTURA", GuidDetalleFactura).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvDetallesFacturaCollection FetchByQuery(Query qry)
        {
            InvDetallesFacturaCollection coll = new InvDetallesFacturaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object GuidDetalleFactura)
        {
            return (InvDetallesFactura.Delete(GuidDetalleFactura) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object GuidDetalleFactura)
        {
            return (InvDetallesFactura.Destroy(GuidDetalleFactura) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string GuidDetalleFactura,int NumeroItem,int Cantidad,decimal ValorBase,decimal EnvioBase,decimal Iva,string GuidFactura,string GuidReferenciaProducto,decimal FactorCompraPuntos,bool? DescargoEnInventario)
	    {
		    InvDetallesFactura item = new InvDetallesFactura();
		    
            item.GuidDetalleFactura = GuidDetalleFactura;
            
            item.NumeroItem = NumeroItem;
            
            item.Cantidad = Cantidad;
            
            item.ValorBase = ValorBase;
            
            item.EnvioBase = EnvioBase;
            
            item.Iva = Iva;
            
            item.GuidFactura = GuidFactura;
            
            item.GuidReferenciaProducto = GuidReferenciaProducto;
            
            item.FactorCompraPuntos = FactorCompraPuntos;
            
            item.DescargoEnInventario = DescargoEnInventario;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string GuidDetalleFactura,int NumeroItem,int Cantidad,decimal ValorBase,decimal EnvioBase,decimal Iva,string GuidFactura,string GuidReferenciaProducto,decimal FactorCompraPuntos,bool? DescargoEnInventario)
	    {
		    InvDetallesFactura item = new InvDetallesFactura();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.GuidDetalleFactura = GuidDetalleFactura;
				
			item.NumeroItem = NumeroItem;
				
			item.Cantidad = Cantidad;
				
			item.ValorBase = ValorBase;
				
			item.EnvioBase = EnvioBase;
				
			item.Iva = Iva;
				
			item.GuidFactura = GuidFactura;
				
			item.GuidReferenciaProducto = GuidReferenciaProducto;
				
			item.FactorCompraPuntos = FactorCompraPuntos;
				
			item.DescargoEnInventario = DescargoEnInventario;
				
	        item.Save(UserName);
	    }
    }
}
