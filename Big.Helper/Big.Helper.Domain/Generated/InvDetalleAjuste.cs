using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvDetalleAjuste class.
	/// </summary>
    [Serializable]
	public partial class InvDetalleAjusteCollection : ActiveList<InvDetalleAjuste, InvDetalleAjusteCollection>
	{	   
		public InvDetalleAjusteCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvDetalleAjusteCollection</returns>
		public InvDetalleAjusteCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvDetalleAjuste o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_DETALLE_AJUSTE table.
	/// </summary>
	[Serializable]
	public partial class InvDetalleAjuste : ActiveRecord<InvDetalleAjuste>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvDetalleAjuste()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvDetalleAjuste(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvDetalleAjuste(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvDetalleAjuste(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_DETALLE_AJUSTE", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdDetalleAjuste = new TableSchema.TableColumn(schema);
				colvarIdDetalleAjuste.ColumnName = "ID_DETALLE_AJUSTE";
				colvarIdDetalleAjuste.DataType = DbType.String;
				colvarIdDetalleAjuste.MaxLength = 36;
				colvarIdDetalleAjuste.AutoIncrement = false;
				colvarIdDetalleAjuste.IsNullable = false;
				colvarIdDetalleAjuste.IsPrimaryKey = true;
				colvarIdDetalleAjuste.IsForeignKey = false;
				colvarIdDetalleAjuste.IsReadOnly = false;
				colvarIdDetalleAjuste.DefaultSetting = @"";
				colvarIdDetalleAjuste.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdDetalleAjuste);
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = false;
				colvarGuidReferenciaProducto.IsForeignKey = false;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				colvarGuidReferenciaProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarCostoUnitario = new TableSchema.TableColumn(schema);
				colvarCostoUnitario.ColumnName = "COSTO_UNITARIO";
				colvarCostoUnitario.DataType = DbType.Decimal;
				colvarCostoUnitario.MaxLength = 0;
				colvarCostoUnitario.AutoIncrement = false;
				colvarCostoUnitario.IsNullable = false;
				colvarCostoUnitario.IsPrimaryKey = false;
				colvarCostoUnitario.IsForeignKey = false;
				colvarCostoUnitario.IsReadOnly = false;
				colvarCostoUnitario.DefaultSetting = @"";
				colvarCostoUnitario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoUnitario);
				
				TableSchema.TableColumn colvarCostoTotal = new TableSchema.TableColumn(schema);
				colvarCostoTotal.ColumnName = "COSTO_TOTAL";
				colvarCostoTotal.DataType = DbType.Decimal;
				colvarCostoTotal.MaxLength = 0;
				colvarCostoTotal.AutoIncrement = false;
				colvarCostoTotal.IsNullable = false;
				colvarCostoTotal.IsPrimaryKey = false;
				colvarCostoTotal.IsForeignKey = false;
				colvarCostoTotal.IsReadOnly = false;
				colvarCostoTotal.DefaultSetting = @"";
				colvarCostoTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoTotal);
				
				TableSchema.TableColumn colvarPrecioUnitario = new TableSchema.TableColumn(schema);
				colvarPrecioUnitario.ColumnName = "PRECIO_UNITARIO";
				colvarPrecioUnitario.DataType = DbType.Decimal;
				colvarPrecioUnitario.MaxLength = 0;
				colvarPrecioUnitario.AutoIncrement = false;
				colvarPrecioUnitario.IsNullable = false;
				colvarPrecioUnitario.IsPrimaryKey = false;
				colvarPrecioUnitario.IsForeignKey = false;
				colvarPrecioUnitario.IsReadOnly = false;
				colvarPrecioUnitario.DefaultSetting = @"";
				colvarPrecioUnitario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrecioUnitario);
				
				TableSchema.TableColumn colvarPrecioTotal = new TableSchema.TableColumn(schema);
				colvarPrecioTotal.ColumnName = "PRECIO_TOTAL";
				colvarPrecioTotal.DataType = DbType.Decimal;
				colvarPrecioTotal.MaxLength = 0;
				colvarPrecioTotal.AutoIncrement = false;
				colvarPrecioTotal.IsNullable = false;
				colvarPrecioTotal.IsPrimaryKey = false;
				colvarPrecioTotal.IsForeignKey = false;
				colvarPrecioTotal.IsReadOnly = false;
				colvarPrecioTotal.DefaultSetting = @"";
				colvarPrecioTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrecioTotal);
				
				TableSchema.TableColumn colvarNumeroItem = new TableSchema.TableColumn(schema);
				colvarNumeroItem.ColumnName = "NUMERO_ITEM";
				colvarNumeroItem.DataType = DbType.Int32;
				colvarNumeroItem.MaxLength = 0;
				colvarNumeroItem.AutoIncrement = false;
				colvarNumeroItem.IsNullable = false;
				colvarNumeroItem.IsPrimaryKey = false;
				colvarNumeroItem.IsForeignKey = false;
				colvarNumeroItem.IsReadOnly = false;
				colvarNumeroItem.DefaultSetting = @"";
				colvarNumeroItem.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroItem);
				
				TableSchema.TableColumn colvarGuidAjuste = new TableSchema.TableColumn(schema);
				colvarGuidAjuste.ColumnName = "GUID_AJUSTE";
				colvarGuidAjuste.DataType = DbType.String;
				colvarGuidAjuste.MaxLength = 36;
				colvarGuidAjuste.AutoIncrement = false;
				colvarGuidAjuste.IsNullable = false;
				colvarGuidAjuste.IsPrimaryKey = false;
				colvarGuidAjuste.IsForeignKey = true;
				colvarGuidAjuste.IsReadOnly = false;
				colvarGuidAjuste.DefaultSetting = @"";
				
					colvarGuidAjuste.ForeignKeyTableName = "INV_AJUSTES";
				schema.Columns.Add(colvarGuidAjuste);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_DETALLE_AJUSTE",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdDetalleAjuste")]
		[Bindable(true)]
		public string IdDetalleAjuste 
		{
			get { return GetColumnValue<string>(Columns.IdDetalleAjuste); }
			set { SetColumnValue(Columns.IdDetalleAjuste, value); }
		}
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("CostoUnitario")]
		[Bindable(true)]
		public decimal CostoUnitario 
		{
			get { return GetColumnValue<decimal>(Columns.CostoUnitario); }
			set { SetColumnValue(Columns.CostoUnitario, value); }
		}
		  
		[XmlAttribute("CostoTotal")]
		[Bindable(true)]
		public decimal CostoTotal 
		{
			get { return GetColumnValue<decimal>(Columns.CostoTotal); }
			set { SetColumnValue(Columns.CostoTotal, value); }
		}
		  
		[XmlAttribute("PrecioUnitario")]
		[Bindable(true)]
		public decimal PrecioUnitario 
		{
			get { return GetColumnValue<decimal>(Columns.PrecioUnitario); }
			set { SetColumnValue(Columns.PrecioUnitario, value); }
		}
		  
		[XmlAttribute("PrecioTotal")]
		[Bindable(true)]
		public decimal PrecioTotal 
		{
			get { return GetColumnValue<decimal>(Columns.PrecioTotal); }
			set { SetColumnValue(Columns.PrecioTotal, value); }
		}
		  
		[XmlAttribute("NumeroItem")]
		[Bindable(true)]
		public int NumeroItem 
		{
			get { return GetColumnValue<int>(Columns.NumeroItem); }
			set { SetColumnValue(Columns.NumeroItem, value); }
		}
		  
		[XmlAttribute("GuidAjuste")]
		[Bindable(true)]
		public string GuidAjuste 
		{
			get { return GetColumnValue<string>(Columns.GuidAjuste); }
			set { SetColumnValue(Columns.GuidAjuste, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvHistoricoBodegaCollection InvHistoricoBodegaRecords()
		{
			return new Big.Helper.Domain.Generated.InvHistoricoBodegaCollection().Where(InvHistoricoBodega.Columns.GuidDetalleAjuste, IdDetalleAjuste).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvAjuste ActiveRecord object related to this InvDetalleAjuste
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvAjuste InvAjuste
		{
			get { return Big.Helper.Domain.Generated.InvAjuste.FetchByID(this.GuidAjuste); }
			set { SetColumnValue("GUID_AJUSTE", value.GuidAjuste); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varIdDetalleAjuste,string varGuidReferenciaProducto,int varCantidad,decimal varCostoUnitario,decimal varCostoTotal,decimal varPrecioUnitario,decimal varPrecioTotal,int varNumeroItem,string varGuidAjuste)
		{
			InvDetalleAjuste item = new InvDetalleAjuste();
			
			item.IdDetalleAjuste = varIdDetalleAjuste;
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.Cantidad = varCantidad;
			
			item.CostoUnitario = varCostoUnitario;
			
			item.CostoTotal = varCostoTotal;
			
			item.PrecioUnitario = varPrecioUnitario;
			
			item.PrecioTotal = varPrecioTotal;
			
			item.NumeroItem = varNumeroItem;
			
			item.GuidAjuste = varGuidAjuste;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varIdDetalleAjuste,string varGuidReferenciaProducto,int varCantidad,decimal varCostoUnitario,decimal varCostoTotal,decimal varPrecioUnitario,decimal varPrecioTotal,int varNumeroItem,string varGuidAjuste)
		{
			InvDetalleAjuste item = new InvDetalleAjuste();
			
				item.IdDetalleAjuste = varIdDetalleAjuste;
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
				item.Cantidad = varCantidad;
			
				item.CostoUnitario = varCostoUnitario;
			
				item.CostoTotal = varCostoTotal;
			
				item.PrecioUnitario = varPrecioUnitario;
			
				item.PrecioTotal = varPrecioTotal;
			
				item.NumeroItem = varNumeroItem;
			
				item.GuidAjuste = varGuidAjuste;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdDetalleAjusteColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoUnitarioColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoTotalColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PrecioUnitarioColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn PrecioTotalColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroItemColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidAjusteColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdDetalleAjuste = @"ID_DETALLE_AJUSTE";
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
			 public static string Cantidad = @"CANTIDAD";
			 public static string CostoUnitario = @"COSTO_UNITARIO";
			 public static string CostoTotal = @"COSTO_TOTAL";
			 public static string PrecioUnitario = @"PRECIO_UNITARIO";
			 public static string PrecioTotal = @"PRECIO_TOTAL";
			 public static string NumeroItem = @"NUMERO_ITEM";
			 public static string GuidAjuste = @"GUID_AJUSTE";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
