using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CGUNO_DETALLES_CAUSACION
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoDetallesCausacionController
    {
        // Preload our schema..
        InvCgunoDetallesCausacion thisSchemaLoad = new InvCgunoDetallesCausacion();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoDetallesCausacionCollection FetchAll()
        {
            InvCgunoDetallesCausacionCollection coll = new InvCgunoDetallesCausacionCollection();
            Query qry = new Query(InvCgunoDetallesCausacion.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoDetallesCausacionCollection FetchByID(object Id)
        {
            InvCgunoDetallesCausacionCollection coll = new InvCgunoDetallesCausacionCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoDetallesCausacionCollection FetchByQuery(Query qry)
        {
            InvCgunoDetallesCausacionCollection coll = new InvCgunoDetallesCausacionCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoDetallesCausacion.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoDetallesCausacion.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,int IdCuenta,decimal IdCausacion,int IdNaturaleza,int IdCampoTipoCausacion,DateTime FechaCreacion)
	    {
		    InvCgunoDetallesCausacion item = new InvCgunoDetallesCausacion();
		    
            item.Guid = Guid;
            
            item.IdCuenta = IdCuenta;
            
            item.IdCausacion = IdCausacion;
            
            item.IdNaturaleza = IdNaturaleza;
            
            item.IdCampoTipoCausacion = IdCampoTipoCausacion;
            
            item.FechaCreacion = FechaCreacion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,decimal Id,int IdCuenta,decimal IdCausacion,int IdNaturaleza,int IdCampoTipoCausacion,DateTime FechaCreacion)
	    {
		    InvCgunoDetallesCausacion item = new InvCgunoDetallesCausacion();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.Id = Id;
				
			item.IdCuenta = IdCuenta;
				
			item.IdCausacion = IdCausacion;
				
			item.IdNaturaleza = IdNaturaleza;
				
			item.IdCampoTipoCausacion = IdCampoTipoCausacion;
				
			item.FechaCreacion = FechaCreacion;
				
	        item.Save(UserName);
	    }
    }
}
