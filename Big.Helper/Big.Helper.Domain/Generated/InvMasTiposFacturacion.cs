using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvMasTiposFacturacion class.
	/// </summary>
    [Serializable]
	public partial class InvMasTiposFacturacionCollection : ActiveList<InvMasTiposFacturacion, InvMasTiposFacturacionCollection>
	{	   
		public InvMasTiposFacturacionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasTiposFacturacionCollection</returns>
		public InvMasTiposFacturacionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasTiposFacturacion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_FACTURACION table.
	/// </summary>
	[Serializable]
	public partial class InvMasTiposFacturacion : ActiveRecord<InvMasTiposFacturacion>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasTiposFacturacion()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasTiposFacturacion(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasTiposFacturacion(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasTiposFacturacion(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_FACTURACION", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoFacturacion = new TableSchema.TableColumn(schema);
				colvarIdTipoFacturacion.ColumnName = "ID_TIPO_FACTURACION";
				colvarIdTipoFacturacion.DataType = DbType.Int32;
				colvarIdTipoFacturacion.MaxLength = 0;
				colvarIdTipoFacturacion.AutoIncrement = true;
				colvarIdTipoFacturacion.IsNullable = false;
				colvarIdTipoFacturacion.IsPrimaryKey = true;
				colvarIdTipoFacturacion.IsForeignKey = true;
				colvarIdTipoFacturacion.IsReadOnly = false;
				colvarIdTipoFacturacion.DefaultSetting = @"";
				
					colvarIdTipoFacturacion.ForeignKeyTableName = "INV_MAS_TIPOS_FACTURACION";
				schema.Columns.Add(colvarIdTipoFacturacion);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "DESCRIPCION";
				colvarDescripcion.DataType = DbType.String;
				colvarDescripcion.MaxLength = 100;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = false;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_TIPOS_FACTURACION",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoFacturacion")]
		[Bindable(true)]
		public int IdTipoFacturacion 
		{
			get { return GetColumnValue<int>(Columns.IdTipoFacturacion); }
			set { SetColumnValue(Columns.IdTipoFacturacion, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvMasTiposFacturacionCollection ChildInvMasTiposFacturacionRecords()
		{
			return new Big.Helper.Domain.Generated.InvMasTiposFacturacionCollection().Where(InvMasTiposFacturacion.Columns.IdTipoFacturacion, IdTipoFacturacion).Load();
		}
		public Big.Helper.Domain.Generated.InvProgramaCollection InvProgramas()
		{
			return new Big.Helper.Domain.Generated.InvProgramaCollection().Where(InvPrograma.Columns.IdTipoFacturacion, IdTipoFacturacion).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasTiposFacturacion ActiveRecord object related to this InvMasTiposFacturacion
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasTiposFacturacion ParentInvMasTiposFacturacion
		{
			get { return Big.Helper.Domain.Generated.InvMasTiposFacturacion.FetchByID(this.IdTipoFacturacion); }
			set { SetColumnValue("ID_TIPO_FACTURACION", value.IdTipoFacturacion); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varDescripcion)
		{
			InvMasTiposFacturacion item = new InvMasTiposFacturacion();
			
			item.Descripcion = varDescripcion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoFacturacion,string varDescripcion)
		{
			InvMasTiposFacturacion item = new InvMasTiposFacturacion();
			
				item.IdTipoFacturacion = varIdTipoFacturacion;
			
				item.Descripcion = varDescripcion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoFacturacionColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoFacturacion = @"ID_TIPO_FACTURACION";
			 public static string Descripcion = @"DESCRIPCION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
