using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoMasNaturalezasDetalleCausacionController
    {
        // Preload our schema..
        InvCgunoMasNaturalezasDetalleCausacion thisSchemaLoad = new InvCgunoMasNaturalezasDetalleCausacion();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoMasNaturalezasDetalleCausacionCollection FetchAll()
        {
            InvCgunoMasNaturalezasDetalleCausacionCollection coll = new InvCgunoMasNaturalezasDetalleCausacionCollection();
            Query qry = new Query(InvCgunoMasNaturalezasDetalleCausacion.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoMasNaturalezasDetalleCausacionCollection FetchByID(object Id)
        {
            InvCgunoMasNaturalezasDetalleCausacionCollection coll = new InvCgunoMasNaturalezasDetalleCausacionCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoMasNaturalezasDetalleCausacionCollection FetchByQuery(Query qry)
        {
            InvCgunoMasNaturalezasDetalleCausacionCollection coll = new InvCgunoMasNaturalezasDetalleCausacionCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoMasNaturalezasDetalleCausacion.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoMasNaturalezasDetalleCausacion.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int Id,string Descripcion)
	    {
		    InvCgunoMasNaturalezasDetalleCausacion item = new InvCgunoMasNaturalezasDetalleCausacion();
		    
            item.Id = Id;
            
            item.Descripcion = Descripcion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Descripcion)
	    {
		    InvCgunoMasNaturalezasDetalleCausacion item = new InvCgunoMasNaturalezasDetalleCausacion();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Descripcion = Descripcion;
				
	        item.Save(UserName);
	    }
    }
}
