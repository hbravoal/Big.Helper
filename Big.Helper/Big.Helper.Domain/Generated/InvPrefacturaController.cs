using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_PREFACTURAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvPrefacturaController
    {
        // Preload our schema..
        InvPrefactura thisSchemaLoad = new InvPrefactura();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvPrefacturaCollection FetchAll()
        {
            InvPrefacturaCollection coll = new InvPrefacturaCollection();
            Query qry = new Query(InvPrefactura.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPrefacturaCollection FetchByID(object IdPrefactura)
        {
            InvPrefacturaCollection coll = new InvPrefacturaCollection().Where("ID_PREFACTURA", IdPrefactura).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPrefacturaCollection FetchByQuery(Query qry)
        {
            InvPrefacturaCollection coll = new InvPrefacturaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPrefactura)
        {
            return (InvPrefactura.Delete(IdPrefactura) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPrefactura)
        {
            return (InvPrefactura.Destroy(IdPrefactura) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int? IdPrograma,DateTime? FechaInicio,DateTime? FechaFin)
	    {
		    InvPrefactura item = new InvPrefactura();
		    
            item.IdPrograma = IdPrograma;
            
            item.FechaInicio = FechaInicio;
            
            item.FechaFin = FechaFin;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPrefactura,int? IdPrograma,DateTime? FechaInicio,DateTime? FechaFin)
	    {
		    InvPrefactura item = new InvPrefactura();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPrefactura = IdPrefactura;
				
			item.IdPrograma = IdPrograma;
				
			item.FechaInicio = FechaInicio;
				
			item.FechaFin = FechaFin;
				
	        item.Save(UserName);
	    }
    }
}
