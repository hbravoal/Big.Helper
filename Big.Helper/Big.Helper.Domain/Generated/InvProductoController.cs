using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_PRODUCTOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvProductoController
    {
        // Preload our schema..
        InvProducto thisSchemaLoad = new InvProducto();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvProductoCollection FetchAll()
        {
            InvProductoCollection coll = new InvProductoCollection();
            Query qry = new Query(InvProducto.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvProductoCollection FetchByID(object Guid)
        {
            InvProductoCollection coll = new InvProductoCollection().Where("GUID", Guid).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvProductoCollection FetchByQuery(Query qry)
        {
            InvProductoCollection coll = new InvProductoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Guid)
        {
            return (InvProducto.Delete(Guid) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Guid)
        {
            return (InvProducto.Destroy(Guid) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,string Codigo,string Nombre,string Descripcion,string Localizacion,string CodigoBarras,int IdTipoEmpaque,string Unidadesxempaque,decimal CostoBase,decimal? ValorAsegurado,decimal Largo,decimal Ancho,decimal Alto,decimal Peso,int IdProveedor,int IdTipoProducto,decimal Iva,decimal ReteFuente,decimal? Descuento,decimal? IvaCosto)
	    {
		    InvProducto item = new InvProducto();
		    
            item.Guid = Guid;
            
            item.Codigo = Codigo;
            
            item.Nombre = Nombre;
            
            item.Descripcion = Descripcion;
            
            item.Localizacion = Localizacion;
            
            item.CodigoBarras = CodigoBarras;
            
            item.IdTipoEmpaque = IdTipoEmpaque;
            
            item.Unidadesxempaque = Unidadesxempaque;
            
            item.CostoBase = CostoBase;
            
            item.ValorAsegurado = ValorAsegurado;
            
            item.Largo = Largo;
            
            item.Ancho = Ancho;
            
            item.Alto = Alto;
            
            item.Peso = Peso;
            
            item.IdProveedor = IdProveedor;
            
            item.IdTipoProducto = IdTipoProducto;
            
            item.Iva = Iva;
            
            item.ReteFuente = ReteFuente;
            
            item.Descuento = Descuento;
            
            item.IvaCosto = IvaCosto;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,string Codigo,string Nombre,string Descripcion,string Localizacion,string CodigoBarras,int IdTipoEmpaque,string Unidadesxempaque,decimal CostoBase,decimal? ValorAsegurado,decimal Largo,decimal Ancho,decimal Alto,decimal Peso,int IdProveedor,int IdTipoProducto,decimal Iva,decimal ReteFuente,decimal? Descuento,decimal? IvaCosto)
	    {
		    InvProducto item = new InvProducto();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.Codigo = Codigo;
				
			item.Nombre = Nombre;
				
			item.Descripcion = Descripcion;
				
			item.Localizacion = Localizacion;
				
			item.CodigoBarras = CodigoBarras;
				
			item.IdTipoEmpaque = IdTipoEmpaque;
				
			item.Unidadesxempaque = Unidadesxempaque;
				
			item.CostoBase = CostoBase;
				
			item.ValorAsegurado = ValorAsegurado;
				
			item.Largo = Largo;
				
			item.Ancho = Ancho;
				
			item.Alto = Alto;
				
			item.Peso = Peso;
				
			item.IdProveedor = IdProveedor;
				
			item.IdTipoProducto = IdTipoProducto;
				
			item.Iva = Iva;
				
			item.ReteFuente = ReteFuente;
				
			item.Descuento = Descuento;
				
			item.IvaCosto = IvaCosto;
				
	        item.Save(UserName);
	    }
    }
}
