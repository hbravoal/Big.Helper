using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwReportesFacturacion class.
    /// </summary>
    [Serializable]
    public partial class InvVwReportesFacturacionCollection : ReadOnlyList<InvVwReportesFacturacion, InvVwReportesFacturacionCollection>
    {        
        public InvVwReportesFacturacionCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_REPORTES_FACTURACION view.
    /// </summary>
    [Serializable]
    public partial class InvVwReportesFacturacion : ReadOnlyRecord<InvVwReportesFacturacion>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_REPORTES_FACTURACION", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
                colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
                colvarNumeroFactura.DataType = DbType.Int32;
                colvarNumeroFactura.MaxLength = 0;
                colvarNumeroFactura.AutoIncrement = false;
                colvarNumeroFactura.IsNullable = true;
                colvarNumeroFactura.IsPrimaryKey = false;
                colvarNumeroFactura.IsForeignKey = false;
                colvarNumeroFactura.IsReadOnly = false;
                
                schema.Columns.Add(colvarNumeroFactura);
                
                TableSchema.TableColumn colvarTotalNeto = new TableSchema.TableColumn(schema);
                colvarTotalNeto.ColumnName = "TOTAL_NETO";
                colvarTotalNeto.DataType = DbType.Decimal;
                colvarTotalNeto.MaxLength = 0;
                colvarTotalNeto.AutoIncrement = false;
                colvarTotalNeto.IsNullable = true;
                colvarTotalNeto.IsPrimaryKey = false;
                colvarTotalNeto.IsForeignKey = false;
                colvarTotalNeto.IsReadOnly = false;
                
                schema.Columns.Add(colvarTotalNeto);
                
                TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
                colvarFecha.ColumnName = "FECHA";
                colvarFecha.DataType = DbType.DateTime;
                colvarFecha.MaxLength = 0;
                colvarFecha.AutoIncrement = false;
                colvarFecha.IsNullable = true;
                colvarFecha.IsPrimaryKey = false;
                colvarFecha.IsForeignKey = false;
                colvarFecha.IsReadOnly = false;
                
                schema.Columns.Add(colvarFecha);
                
                TableSchema.TableColumn colvarNit = new TableSchema.TableColumn(schema);
                colvarNit.ColumnName = "NIT";
                colvarNit.DataType = DbType.AnsiString;
                colvarNit.MaxLength = 15;
                colvarNit.AutoIncrement = false;
                colvarNit.IsNullable = false;
                colvarNit.IsPrimaryKey = false;
                colvarNit.IsForeignKey = false;
                colvarNit.IsReadOnly = false;
                
                schema.Columns.Add(colvarNit);
                
                TableSchema.TableColumn colvarNombreCliente = new TableSchema.TableColumn(schema);
                colvarNombreCliente.ColumnName = "NOMBRE_CLIENTE";
                colvarNombreCliente.DataType = DbType.String;
                colvarNombreCliente.MaxLength = 200;
                colvarNombreCliente.AutoIncrement = false;
                colvarNombreCliente.IsNullable = true;
                colvarNombreCliente.IsPrimaryKey = false;
                colvarNombreCliente.IsForeignKey = false;
                colvarNombreCliente.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreCliente);
                
                TableSchema.TableColumn colvarIdAnulada = new TableSchema.TableColumn(schema);
                colvarIdAnulada.ColumnName = "ID_ANULADA";
                colvarIdAnulada.DataType = DbType.Boolean;
                colvarIdAnulada.MaxLength = 0;
                colvarIdAnulada.AutoIncrement = false;
                colvarIdAnulada.IsNullable = true;
                colvarIdAnulada.IsPrimaryKey = false;
                colvarIdAnulada.IsForeignKey = false;
                colvarIdAnulada.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdAnulada);
                
                TableSchema.TableColumn colvarIdNotacredito = new TableSchema.TableColumn(schema);
                colvarIdNotacredito.ColumnName = "ID_NOTACREDITO";
                colvarIdNotacredito.DataType = DbType.Boolean;
                colvarIdNotacredito.MaxLength = 0;
                colvarIdNotacredito.AutoIncrement = false;
                colvarIdNotacredito.IsNullable = true;
                colvarIdNotacredito.IsPrimaryKey = false;
                colvarIdNotacredito.IsForeignKey = false;
                colvarIdNotacredito.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdNotacredito);
                
                TableSchema.TableColumn colvarAnulada = new TableSchema.TableColumn(schema);
                colvarAnulada.ColumnName = "ANULADA";
                colvarAnulada.DataType = DbType.AnsiString;
                colvarAnulada.MaxLength = 2;
                colvarAnulada.AutoIncrement = false;
                colvarAnulada.IsNullable = true;
                colvarAnulada.IsPrimaryKey = false;
                colvarAnulada.IsForeignKey = false;
                colvarAnulada.IsReadOnly = false;
                
                schema.Columns.Add(colvarAnulada);
                
                TableSchema.TableColumn colvarNotaCredito = new TableSchema.TableColumn(schema);
                colvarNotaCredito.ColumnName = "NOTA_CREDITO";
                colvarNotaCredito.DataType = DbType.AnsiString;
                colvarNotaCredito.MaxLength = 2;
                colvarNotaCredito.AutoIncrement = false;
                colvarNotaCredito.IsNullable = true;
                colvarNotaCredito.IsPrimaryKey = false;
                colvarNotaCredito.IsForeignKey = false;
                colvarNotaCredito.IsReadOnly = false;
                
                schema.Columns.Add(colvarNotaCredito);
                
                TableSchema.TableColumn colvarRazonSocial = new TableSchema.TableColumn(schema);
                colvarRazonSocial.ColumnName = "RAZON_SOCIAL";
                colvarRazonSocial.DataType = DbType.String;
                colvarRazonSocial.MaxLength = 200;
                colvarRazonSocial.AutoIncrement = false;
                colvarRazonSocial.IsNullable = false;
                colvarRazonSocial.IsPrimaryKey = false;
                colvarRazonSocial.IsForeignKey = false;
                colvarRazonSocial.IsReadOnly = false;
                
                schema.Columns.Add(colvarRazonSocial);
                
                TableSchema.TableColumn colvarProducto = new TableSchema.TableColumn(schema);
                colvarProducto.ColumnName = "PRODUCTO";
                colvarProducto.DataType = DbType.String;
                colvarProducto.MaxLength = 100;
                colvarProducto.AutoIncrement = false;
                colvarProducto.IsNullable = false;
                colvarProducto.IsPrimaryKey = false;
                colvarProducto.IsForeignKey = false;
                colvarProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarProducto);
                
                TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
                colvarCantidad.ColumnName = "CANTIDAD";
                colvarCantidad.DataType = DbType.Int32;
                colvarCantidad.MaxLength = 0;
                colvarCantidad.AutoIncrement = false;
                colvarCantidad.IsNullable = false;
                colvarCantidad.IsPrimaryKey = false;
                colvarCantidad.IsForeignKey = false;
                colvarCantidad.IsReadOnly = false;
                
                schema.Columns.Add(colvarCantidad);
                
                TableSchema.TableColumn colvarValorUnitario = new TableSchema.TableColumn(schema);
                colvarValorUnitario.ColumnName = "VALOR_UNITARIO";
                colvarValorUnitario.DataType = DbType.Decimal;
                colvarValorUnitario.MaxLength = 0;
                colvarValorUnitario.AutoIncrement = false;
                colvarValorUnitario.IsNullable = false;
                colvarValorUnitario.IsPrimaryKey = false;
                colvarValorUnitario.IsForeignKey = false;
                colvarValorUnitario.IsReadOnly = false;
                
                schema.Columns.Add(colvarValorUnitario);
                
                TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
                colvarIva.ColumnName = "IVA";
                colvarIva.DataType = DbType.Decimal;
                colvarIva.MaxLength = 0;
                colvarIva.AutoIncrement = false;
                colvarIva.IsNullable = false;
                colvarIva.IsPrimaryKey = false;
                colvarIva.IsForeignKey = false;
                colvarIva.IsReadOnly = false;
                
                schema.Columns.Add(colvarIva);
                
                TableSchema.TableColumn colvarValorTotal = new TableSchema.TableColumn(schema);
                colvarValorTotal.ColumnName = "VALOR_TOTAL";
                colvarValorTotal.DataType = DbType.Decimal;
                colvarValorTotal.MaxLength = 0;
                colvarValorTotal.AutoIncrement = false;
                colvarValorTotal.IsNullable = true;
                colvarValorTotal.IsPrimaryKey = false;
                colvarValorTotal.IsForeignKey = false;
                colvarValorTotal.IsReadOnly = false;
                
                schema.Columns.Add(colvarValorTotal);
                
                TableSchema.TableColumn colvarPorcentajePuntos = new TableSchema.TableColumn(schema);
                colvarPorcentajePuntos.ColumnName = "PORCENTAJE_PUNTOS";
                colvarPorcentajePuntos.DataType = DbType.Decimal;
                colvarPorcentajePuntos.MaxLength = 0;
                colvarPorcentajePuntos.AutoIncrement = false;
                colvarPorcentajePuntos.IsNullable = false;
                colvarPorcentajePuntos.IsPrimaryKey = false;
                colvarPorcentajePuntos.IsForeignKey = false;
                colvarPorcentajePuntos.IsReadOnly = false;
                
                schema.Columns.Add(colvarPorcentajePuntos);
                
                TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
                colvarIdPrograma.ColumnName = "ID_PROGRAMA";
                colvarIdPrograma.DataType = DbType.Int32;
                colvarIdPrograma.MaxLength = 0;
                colvarIdPrograma.AutoIncrement = false;
                colvarIdPrograma.IsNullable = false;
                colvarIdPrograma.IsPrimaryKey = false;
                colvarIdPrograma.IsForeignKey = false;
                colvarIdPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdPrograma);
                
                TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
                colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
                colvarIdProveedor.DataType = DbType.Int32;
                colvarIdProveedor.MaxLength = 0;
                colvarIdProveedor.AutoIncrement = false;
                colvarIdProveedor.IsNullable = false;
                colvarIdProveedor.IsPrimaryKey = false;
                colvarIdProveedor.IsForeignKey = false;
                colvarIdProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdProveedor);
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.String;
                colvarGuid.MaxLength = 36;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarGuidFacturaDetalle = new TableSchema.TableColumn(schema);
                colvarGuidFacturaDetalle.ColumnName = "GUID_FACTURA_DETALLE";
                colvarGuidFacturaDetalle.DataType = DbType.String;
                colvarGuidFacturaDetalle.MaxLength = 36;
                colvarGuidFacturaDetalle.AutoIncrement = false;
                colvarGuidFacturaDetalle.IsNullable = false;
                colvarGuidFacturaDetalle.IsPrimaryKey = false;
                colvarGuidFacturaDetalle.IsForeignKey = false;
                colvarGuidFacturaDetalle.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidFacturaDetalle);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_REPORTES_FACTURACION",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwReportesFacturacion()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwReportesFacturacion(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwReportesFacturacion(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwReportesFacturacion(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("NumeroFactura")]
        [Bindable(true)]
        public int? NumeroFactura 
	    {
		    get
		    {
			    return GetColumnValue<int?>("NUMERO_FACTURA");
		    }
            set 
		    {
			    SetColumnValue("NUMERO_FACTURA", value);
            }
        }
	      
        [XmlAttribute("TotalNeto")]
        [Bindable(true)]
        public decimal? TotalNeto 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("TOTAL_NETO");
		    }
            set 
		    {
			    SetColumnValue("TOTAL_NETO", value);
            }
        }
	      
        [XmlAttribute("Fecha")]
        [Bindable(true)]
        public DateTime? Fecha 
	    {
		    get
		    {
			    return GetColumnValue<DateTime?>("FECHA");
		    }
            set 
		    {
			    SetColumnValue("FECHA", value);
            }
        }
	      
        [XmlAttribute("Nit")]
        [Bindable(true)]
        public string Nit 
	    {
		    get
		    {
			    return GetColumnValue<string>("NIT");
		    }
            set 
		    {
			    SetColumnValue("NIT", value);
            }
        }
	      
        [XmlAttribute("NombreCliente")]
        [Bindable(true)]
        public string NombreCliente 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOMBRE_CLIENTE");
		    }
            set 
		    {
			    SetColumnValue("NOMBRE_CLIENTE", value);
            }
        }
	      
        [XmlAttribute("IdAnulada")]
        [Bindable(true)]
        public bool? IdAnulada 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("ID_ANULADA");
		    }
            set 
		    {
			    SetColumnValue("ID_ANULADA", value);
            }
        }
	      
        [XmlAttribute("IdNotacredito")]
        [Bindable(true)]
        public bool? IdNotacredito 
	    {
		    get
		    {
			    return GetColumnValue<bool?>("ID_NOTACREDITO");
		    }
            set 
		    {
			    SetColumnValue("ID_NOTACREDITO", value);
            }
        }
	      
        [XmlAttribute("Anulada")]
        [Bindable(true)]
        public string Anulada 
	    {
		    get
		    {
			    return GetColumnValue<string>("ANULADA");
		    }
            set 
		    {
			    SetColumnValue("ANULADA", value);
            }
        }
	      
        [XmlAttribute("NotaCredito")]
        [Bindable(true)]
        public string NotaCredito 
	    {
		    get
		    {
			    return GetColumnValue<string>("NOTA_CREDITO");
		    }
            set 
		    {
			    SetColumnValue("NOTA_CREDITO", value);
            }
        }
	      
        [XmlAttribute("RazonSocial")]
        [Bindable(true)]
        public string RazonSocial 
	    {
		    get
		    {
			    return GetColumnValue<string>("RAZON_SOCIAL");
		    }
            set 
		    {
			    SetColumnValue("RAZON_SOCIAL", value);
            }
        }
	      
        [XmlAttribute("Producto")]
        [Bindable(true)]
        public string Producto 
	    {
		    get
		    {
			    return GetColumnValue<string>("PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("Cantidad")]
        [Bindable(true)]
        public int Cantidad 
	    {
		    get
		    {
			    return GetColumnValue<int>("CANTIDAD");
		    }
            set 
		    {
			    SetColumnValue("CANTIDAD", value);
            }
        }
	      
        [XmlAttribute("ValorUnitario")]
        [Bindable(true)]
        public decimal ValorUnitario 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("VALOR_UNITARIO");
		    }
            set 
		    {
			    SetColumnValue("VALOR_UNITARIO", value);
            }
        }
	      
        [XmlAttribute("Iva")]
        [Bindable(true)]
        public decimal Iva 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("IVA");
		    }
            set 
		    {
			    SetColumnValue("IVA", value);
            }
        }
	      
        [XmlAttribute("ValorTotal")]
        [Bindable(true)]
        public decimal? ValorTotal 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("VALOR_TOTAL");
		    }
            set 
		    {
			    SetColumnValue("VALOR_TOTAL", value);
            }
        }
	      
        [XmlAttribute("PorcentajePuntos")]
        [Bindable(true)]
        public decimal PorcentajePuntos 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("PORCENTAJE_PUNTOS");
		    }
            set 
		    {
			    SetColumnValue("PORCENTAJE_PUNTOS", value);
            }
        }
	      
        [XmlAttribute("IdPrograma")]
        [Bindable(true)]
        public int IdPrograma 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("ID_PROGRAMA", value);
            }
        }
	      
        [XmlAttribute("IdProveedor")]
        [Bindable(true)]
        public int IdProveedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("ID_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public string Guid 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("GuidFacturaDetalle")]
        [Bindable(true)]
        public string GuidFacturaDetalle 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_FACTURA_DETALLE");
		    }
            set 
		    {
			    SetColumnValue("GUID_FACTURA_DETALLE", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string NumeroFactura = @"NUMERO_FACTURA";
            
            public static string TotalNeto = @"TOTAL_NETO";
            
            public static string Fecha = @"FECHA";
            
            public static string Nit = @"NIT";
            
            public static string NombreCliente = @"NOMBRE_CLIENTE";
            
            public static string IdAnulada = @"ID_ANULADA";
            
            public static string IdNotacredito = @"ID_NOTACREDITO";
            
            public static string Anulada = @"ANULADA";
            
            public static string NotaCredito = @"NOTA_CREDITO";
            
            public static string RazonSocial = @"RAZON_SOCIAL";
            
            public static string Producto = @"PRODUCTO";
            
            public static string Cantidad = @"CANTIDAD";
            
            public static string ValorUnitario = @"VALOR_UNITARIO";
            
            public static string Iva = @"IVA";
            
            public static string ValorTotal = @"VALOR_TOTAL";
            
            public static string PorcentajePuntos = @"PORCENTAJE_PUNTOS";
            
            public static string IdPrograma = @"ID_PROGRAMA";
            
            public static string IdProveedor = @"ID_PROVEEDOR";
            
            public static string Guid = @"GUID";
            
            public static string GuidFacturaDetalle = @"GUID_FACTURA_DETALLE";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
