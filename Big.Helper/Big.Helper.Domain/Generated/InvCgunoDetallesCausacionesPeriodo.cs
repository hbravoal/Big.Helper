using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoDetallesCausacionesPeriodo class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoDetallesCausacionesPeriodoCollection : ActiveList<InvCgunoDetallesCausacionesPeriodo, InvCgunoDetallesCausacionesPeriodoCollection>
	{	   
		public InvCgunoDetallesCausacionesPeriodoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoDetallesCausacionesPeriodoCollection</returns>
		public InvCgunoDetallesCausacionesPeriodoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoDetallesCausacionesPeriodo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_DETALLES_CAUSACIONES_PERIODO table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoDetallesCausacionesPeriodo : ActiveRecord<InvCgunoDetallesCausacionesPeriodo>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoDetallesCausacionesPeriodo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoDetallesCausacionesPeriodo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoDetallesCausacionesPeriodo(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoDetallesCausacionesPeriodo(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_DETALLES_CAUSACIONES_PERIODO", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.String;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Decimal;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIdCuenta = new TableSchema.TableColumn(schema);
				colvarIdCuenta.ColumnName = "ID_CUENTA";
				colvarIdCuenta.DataType = DbType.Int32;
				colvarIdCuenta.MaxLength = 0;
				colvarIdCuenta.AutoIncrement = false;
				colvarIdCuenta.IsNullable = false;
				colvarIdCuenta.IsPrimaryKey = false;
				colvarIdCuenta.IsForeignKey = true;
				colvarIdCuenta.IsReadOnly = false;
				colvarIdCuenta.DefaultSetting = @"";
				
					colvarIdCuenta.ForeignKeyTableName = "INV_CGUNO_CUENTAS";
				schema.Columns.Add(colvarIdCuenta);
				
				TableSchema.TableColumn colvarIdCausacionPeriodo = new TableSchema.TableColumn(schema);
				colvarIdCausacionPeriodo.ColumnName = "ID_CAUSACION_PERIODO";
				colvarIdCausacionPeriodo.DataType = DbType.Decimal;
				colvarIdCausacionPeriodo.MaxLength = 0;
				colvarIdCausacionPeriodo.AutoIncrement = false;
				colvarIdCausacionPeriodo.IsNullable = false;
				colvarIdCausacionPeriodo.IsPrimaryKey = false;
				colvarIdCausacionPeriodo.IsForeignKey = true;
				colvarIdCausacionPeriodo.IsReadOnly = false;
				colvarIdCausacionPeriodo.DefaultSetting = @"";
				
					colvarIdCausacionPeriodo.ForeignKeyTableName = "INV_CGUNO_CAUSACIONES_PERIODOS";
				schema.Columns.Add(colvarIdCausacionPeriodo);
				
				TableSchema.TableColumn colvarIdNaturaleza = new TableSchema.TableColumn(schema);
				colvarIdNaturaleza.ColumnName = "ID_NATURALEZA";
				colvarIdNaturaleza.DataType = DbType.Int32;
				colvarIdNaturaleza.MaxLength = 0;
				colvarIdNaturaleza.AutoIncrement = false;
				colvarIdNaturaleza.IsNullable = true;
				colvarIdNaturaleza.IsPrimaryKey = false;
				colvarIdNaturaleza.IsForeignKey = true;
				colvarIdNaturaleza.IsReadOnly = false;
				colvarIdNaturaleza.DefaultSetting = @"";
				
					colvarIdNaturaleza.ForeignKeyTableName = "INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION";
				schema.Columns.Add(colvarIdNaturaleza);
				
				TableSchema.TableColumn colvarValorCausacion = new TableSchema.TableColumn(schema);
				colvarValorCausacion.ColumnName = "VALOR_CAUSACION";
				colvarValorCausacion.DataType = DbType.Decimal;
				colvarValorCausacion.MaxLength = 0;
				colvarValorCausacion.AutoIncrement = false;
				colvarValorCausacion.IsNullable = true;
				colvarValorCausacion.IsPrimaryKey = false;
				colvarValorCausacion.IsForeignKey = false;
				colvarValorCausacion.IsReadOnly = false;
				colvarValorCausacion.DefaultSetting = @"";
				colvarValorCausacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorCausacion);
				
				TableSchema.TableColumn colvarGuidReferencia = new TableSchema.TableColumn(schema);
				colvarGuidReferencia.ColumnName = "GUID_REFERENCIA";
				colvarGuidReferencia.DataType = DbType.String;
				colvarGuidReferencia.MaxLength = 36;
				colvarGuidReferencia.AutoIncrement = false;
				colvarGuidReferencia.IsNullable = true;
				colvarGuidReferencia.IsPrimaryKey = false;
				colvarGuidReferencia.IsForeignKey = true;
				colvarGuidReferencia.IsReadOnly = false;
				colvarGuidReferencia.DefaultSetting = @"";
				
					colvarGuidReferencia.ForeignKeyTableName = "INV_REFERENCIAS_PRODUCTO";
				schema.Columns.Add(colvarGuidReferencia);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = true;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarCentroUtilidad = new TableSchema.TableColumn(schema);
				colvarCentroUtilidad.ColumnName = "CENTRO_UTILIDAD";
				colvarCentroUtilidad.DataType = DbType.String;
				colvarCentroUtilidad.MaxLength = 15;
				colvarCentroUtilidad.AutoIncrement = false;
				colvarCentroUtilidad.IsNullable = true;
				colvarCentroUtilidad.IsPrimaryKey = false;
				colvarCentroUtilidad.IsForeignKey = false;
				colvarCentroUtilidad.IsReadOnly = false;
				colvarCentroUtilidad.DefaultSetting = @"";
				colvarCentroUtilidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCentroUtilidad);
				
				TableSchema.TableColumn colvarNit = new TableSchema.TableColumn(schema);
				colvarNit.ColumnName = "NIT";
				colvarNit.DataType = DbType.String;
				colvarNit.MaxLength = 20;
				colvarNit.AutoIncrement = false;
				colvarNit.IsNullable = true;
				colvarNit.IsPrimaryKey = false;
				colvarNit.IsForeignKey = false;
				colvarNit.IsReadOnly = false;
				colvarNit.DefaultSetting = @"";
				colvarNit.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNit);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_DETALLES_CAUSACIONES_PERIODO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public decimal Id 
		{
			get { return GetColumnValue<decimal>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IdCuenta")]
		[Bindable(true)]
		public int IdCuenta 
		{
			get { return GetColumnValue<int>(Columns.IdCuenta); }
			set { SetColumnValue(Columns.IdCuenta, value); }
		}
		  
		[XmlAttribute("IdCausacionPeriodo")]
		[Bindable(true)]
		public decimal IdCausacionPeriodo 
		{
			get { return GetColumnValue<decimal>(Columns.IdCausacionPeriodo); }
			set { SetColumnValue(Columns.IdCausacionPeriodo, value); }
		}
		  
		[XmlAttribute("IdNaturaleza")]
		[Bindable(true)]
		public int? IdNaturaleza 
		{
			get { return GetColumnValue<int?>(Columns.IdNaturaleza); }
			set { SetColumnValue(Columns.IdNaturaleza, value); }
		}
		  
		[XmlAttribute("ValorCausacion")]
		[Bindable(true)]
		public decimal? ValorCausacion 
		{
			get { return GetColumnValue<decimal?>(Columns.ValorCausacion); }
			set { SetColumnValue(Columns.ValorCausacion, value); }
		}
		  
		[XmlAttribute("GuidReferencia")]
		[Bindable(true)]
		public string GuidReferencia 
		{
			get { return GetColumnValue<string>(Columns.GuidReferencia); }
			set { SetColumnValue(Columns.GuidReferencia, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int? Cantidad 
		{
			get { return GetColumnValue<int?>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("CentroUtilidad")]
		[Bindable(true)]
		public string CentroUtilidad 
		{
			get { return GetColumnValue<string>(Columns.CentroUtilidad); }
			set { SetColumnValue(Columns.CentroUtilidad, value); }
		}
		  
		[XmlAttribute("Nit")]
		[Bindable(true)]
		public string Nit 
		{
			get { return GetColumnValue<string>(Columns.Nit); }
			set { SetColumnValue(Columns.Nit, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoCausacionesPeriodo ActiveRecord object related to this InvCgunoDetallesCausacionesPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCausacionesPeriodo InvCgunoCausacionesPeriodo
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCausacionesPeriodo.FetchByID(this.IdCausacionPeriodo); }
			set { SetColumnValue("ID_CAUSACION_PERIODO", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoCuenta ActiveRecord object related to this InvCgunoDetallesCausacionesPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCuenta InvCgunoCuenta
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCuenta.FetchByID(this.IdCuenta); }
			set { SetColumnValue("ID_CUENTA", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoMasNaturalezasDetalleCausacion ActiveRecord object related to this InvCgunoDetallesCausacionesPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoMasNaturalezasDetalleCausacion InvCgunoMasNaturalezasDetalleCausacion
		{
			get { return Big.Helper.Domain.Generated.InvCgunoMasNaturalezasDetalleCausacion.FetchByID(this.IdNaturaleza); }
			set { SetColumnValue("ID_NATURALEZA", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvReferenciasProducto ActiveRecord object related to this InvCgunoDetallesCausacionesPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvReferenciasProducto InvReferenciasProducto
		{
			get { return Big.Helper.Domain.Generated.InvReferenciasProducto.FetchByID(this.GuidReferencia); }
			set { SetColumnValue("GUID_REFERENCIA", value.GuidReferenciaProducto); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,int varIdCuenta,decimal varIdCausacionPeriodo,int? varIdNaturaleza,decimal? varValorCausacion,string varGuidReferencia,int? varCantidad,string varCentroUtilidad,string varNit)
		{
			InvCgunoDetallesCausacionesPeriodo item = new InvCgunoDetallesCausacionesPeriodo();
			
			item.Guid = varGuid;
			
			item.IdCuenta = varIdCuenta;
			
			item.IdCausacionPeriodo = varIdCausacionPeriodo;
			
			item.IdNaturaleza = varIdNaturaleza;
			
			item.ValorCausacion = varValorCausacion;
			
			item.GuidReferencia = varGuidReferencia;
			
			item.Cantidad = varCantidad;
			
			item.CentroUtilidad = varCentroUtilidad;
			
			item.Nit = varNit;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,decimal varId,int varIdCuenta,decimal varIdCausacionPeriodo,int? varIdNaturaleza,decimal? varValorCausacion,string varGuidReferencia,int? varCantidad,string varCentroUtilidad,string varNit)
		{
			InvCgunoDetallesCausacionesPeriodo item = new InvCgunoDetallesCausacionesPeriodo();
			
				item.Guid = varGuid;
			
				item.Id = varId;
			
				item.IdCuenta = varIdCuenta;
			
				item.IdCausacionPeriodo = varIdCausacionPeriodo;
			
				item.IdNaturaleza = varIdNaturaleza;
			
				item.ValorCausacion = varValorCausacion;
			
				item.GuidReferencia = varGuidReferencia;
			
				item.Cantidad = varCantidad;
			
				item.CentroUtilidad = varCentroUtilidad;
			
				item.Nit = varNit;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCuentaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCausacionPeriodoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdNaturalezaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorCausacionColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn CentroUtilidadColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn NitColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string Id = @"ID";
			 public static string IdCuenta = @"ID_CUENTA";
			 public static string IdCausacionPeriodo = @"ID_CAUSACION_PERIODO";
			 public static string IdNaturaleza = @"ID_NATURALEZA";
			 public static string ValorCausacion = @"VALOR_CAUSACION";
			 public static string GuidReferencia = @"GUID_REFERENCIA";
			 public static string Cantidad = @"CANTIDAD";
			 public static string CentroUtilidad = @"CENTRO_UTILIDAD";
			 public static string Nit = @"NIT";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
