using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_HISTORICO_BODEGA
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvHistoricoBodegaController
    {
        // Preload our schema..
        InvHistoricoBodega thisSchemaLoad = new InvHistoricoBodega();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvHistoricoBodegaCollection FetchAll()
        {
            InvHistoricoBodegaCollection coll = new InvHistoricoBodegaCollection();
            Query qry = new Query(InvHistoricoBodega.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvHistoricoBodegaCollection FetchByID(object IdHistoricoBodega)
        {
            InvHistoricoBodegaCollection coll = new InvHistoricoBodegaCollection().Where("ID_HISTORICO_BODEGA", IdHistoricoBodega).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvHistoricoBodegaCollection FetchByQuery(Query qry)
        {
            InvHistoricoBodegaCollection coll = new InvHistoricoBodegaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdHistoricoBodega)
        {
            return (InvHistoricoBodega.Delete(IdHistoricoBodega) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdHistoricoBodega)
        {
            return (InvHistoricoBodega.Destroy(IdHistoricoBodega) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(bool Entrada,DateTime Fecha,string GuidDetalleFactura,int? IdUsuario,decimal IdBodegaProducto,string Observaciones,decimal? CantidadTransaccion,DateTime? FechaVencimiento,long? NumeroFactura,string GuidDetalleAjuste,string GuidDetalleFacturaEmpresa)
	    {
		    InvHistoricoBodega item = new InvHistoricoBodega();
		    
            item.Entrada = Entrada;
            
            item.Fecha = Fecha;
            
            item.GuidDetalleFactura = GuidDetalleFactura;
            
            item.IdUsuario = IdUsuario;
            
            item.IdBodegaProducto = IdBodegaProducto;
            
            item.Observaciones = Observaciones;
            
            item.CantidadTransaccion = CantidadTransaccion;
            
            item.FechaVencimiento = FechaVencimiento;
            
            item.NumeroFactura = NumeroFactura;
            
            item.GuidDetalleAjuste = GuidDetalleAjuste;
            
            item.GuidDetalleFacturaEmpresa = GuidDetalleFacturaEmpresa;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(decimal IdHistoricoBodega,bool Entrada,DateTime Fecha,string GuidDetalleFactura,int? IdUsuario,decimal IdBodegaProducto,string Observaciones,decimal? CantidadTransaccion,DateTime? FechaVencimiento,long? NumeroFactura,string GuidDetalleAjuste,string GuidDetalleFacturaEmpresa)
	    {
		    InvHistoricoBodega item = new InvHistoricoBodega();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdHistoricoBodega = IdHistoricoBodega;
				
			item.Entrada = Entrada;
				
			item.Fecha = Fecha;
				
			item.GuidDetalleFactura = GuidDetalleFactura;
				
			item.IdUsuario = IdUsuario;
				
			item.IdBodegaProducto = IdBodegaProducto;
				
			item.Observaciones = Observaciones;
				
			item.CantidadTransaccion = CantidadTransaccion;
				
			item.FechaVencimiento = FechaVencimiento;
				
			item.NumeroFactura = NumeroFactura;
				
			item.GuidDetalleAjuste = GuidDetalleAjuste;
				
			item.GuidDetalleFacturaEmpresa = GuidDetalleFacturaEmpresa;
				
	        item.Save(UserName);
	    }
    }
}
