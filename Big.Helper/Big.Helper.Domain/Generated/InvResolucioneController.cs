using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_RESOLUCIONES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvResolucioneController
    {
        // Preload our schema..
        InvResolucione thisSchemaLoad = new InvResolucione();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvResolucioneCollection FetchAll()
        {
            InvResolucioneCollection coll = new InvResolucioneCollection();
            Query qry = new Query(InvResolucione.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvResolucioneCollection FetchByID(object IdResolucion)
        {
            InvResolucioneCollection coll = new InvResolucioneCollection().Where("ID_RESOLUCION", IdResolucion).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvResolucioneCollection FetchByQuery(Query qry)
        {
            InvResolucioneCollection coll = new InvResolucioneCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdResolucion)
        {
            return (InvResolucione.Delete(IdResolucion) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdResolucion)
        {
            return (InvResolucione.Destroy(IdResolucion) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Resolucion,DateTime FechaResolucion,string CodigoCentroCosto,int ValorInicial,int ValorFinal,int ValorActual,int IdTipoResolucion,string NumeroResolucion)
	    {
		    InvResolucione item = new InvResolucione();
		    
            item.Resolucion = Resolucion;
            
            item.FechaResolucion = FechaResolucion;
            
            item.CodigoCentroCosto = CodigoCentroCosto;
            
            item.ValorInicial = ValorInicial;
            
            item.ValorFinal = ValorFinal;
            
            item.ValorActual = ValorActual;
            
            item.IdTipoResolucion = IdTipoResolucion;
            
            item.NumeroResolucion = NumeroResolucion;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdResolucion,string Resolucion,DateTime FechaResolucion,string CodigoCentroCosto,int ValorInicial,int ValorFinal,int ValorActual,int IdTipoResolucion,string NumeroResolucion)
	    {
		    InvResolucione item = new InvResolucione();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdResolucion = IdResolucion;
				
			item.Resolucion = Resolucion;
				
			item.FechaResolucion = FechaResolucion;
				
			item.CodigoCentroCosto = CodigoCentroCosto;
				
			item.ValorInicial = ValorInicial;
				
			item.ValorFinal = ValorFinal;
				
			item.ValorActual = ValorActual;
				
			item.IdTipoResolucion = IdTipoResolucion;
				
			item.NumeroResolucion = NumeroResolucion;
				
	        item.Save(UserName);
	    }
    }
}
