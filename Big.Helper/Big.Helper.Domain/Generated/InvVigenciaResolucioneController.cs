using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_VIGENCIA_RESOLUCIONES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvVigenciaResolucioneController
    {
        // Preload our schema..
        InvVigenciaResolucione thisSchemaLoad = new InvVigenciaResolucione();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvVigenciaResolucioneCollection FetchAll()
        {
            InvVigenciaResolucioneCollection coll = new InvVigenciaResolucioneCollection();
            Query qry = new Query(InvVigenciaResolucione.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvVigenciaResolucioneCollection FetchByID(object Id)
        {
            InvVigenciaResolucioneCollection coll = new InvVigenciaResolucioneCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvVigenciaResolucioneCollection FetchByQuery(Query qry)
        {
            InvVigenciaResolucioneCollection coll = new InvVigenciaResolucioneCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvVigenciaResolucione.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvVigenciaResolucione.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdResolucion,int IdPrograma,DateTime FechaInicio,DateTime? FechaFin)
	    {
		    InvVigenciaResolucione item = new InvVigenciaResolucione();
		    
            item.IdResolucion = IdResolucion;
            
            item.IdPrograma = IdPrograma;
            
            item.FechaInicio = FechaInicio;
            
            item.FechaFin = FechaFin;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,int IdResolucion,int IdPrograma,DateTime FechaInicio,DateTime? FechaFin)
	    {
		    InvVigenciaResolucione item = new InvVigenciaResolucione();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.IdResolucion = IdResolucion;
				
			item.IdPrograma = IdPrograma;
				
			item.FechaInicio = FechaInicio;
				
			item.FechaFin = FechaFin;
				
	        item.Save(UserName);
	    }
    }
}
