using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CGUNO_CENTRO_UTILIDAD
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoCentroUtilidadController
    {
        // Preload our schema..
        InvCgunoCentroUtilidad thisSchemaLoad = new InvCgunoCentroUtilidad();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoCentroUtilidadCollection FetchAll()
        {
            InvCgunoCentroUtilidadCollection coll = new InvCgunoCentroUtilidadCollection();
            Query qry = new Query(InvCgunoCentroUtilidad.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCentroUtilidadCollection FetchByID(object Id)
        {
            InvCgunoCentroUtilidadCollection coll = new InvCgunoCentroUtilidadCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCentroUtilidadCollection FetchByQuery(Query qry)
        {
            InvCgunoCentroUtilidadCollection coll = new InvCgunoCentroUtilidadCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoCentroUtilidad.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoCentroUtilidad.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,string NumeroCentroUtilidad,string NombreCentroUtilidad)
	    {
		    InvCgunoCentroUtilidad item = new InvCgunoCentroUtilidad();
		    
            item.Guid = Guid;
            
            item.NumeroCentroUtilidad = NumeroCentroUtilidad;
            
            item.NombreCentroUtilidad = NombreCentroUtilidad;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Id,string Guid,string NumeroCentroUtilidad,string NombreCentroUtilidad)
	    {
		    InvCgunoCentroUtilidad item = new InvCgunoCentroUtilidad();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Id = Id;
				
			item.Guid = Guid;
				
			item.NumeroCentroUtilidad = NumeroCentroUtilidad;
				
			item.NombreCentroUtilidad = NombreCentroUtilidad;
				
	        item.Save(UserName);
	    }
    }
}
