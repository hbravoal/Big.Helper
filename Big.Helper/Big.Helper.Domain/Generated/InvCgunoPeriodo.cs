using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoPeriodo class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoPeriodoCollection : ActiveList<InvCgunoPeriodo, InvCgunoPeriodoCollection>
	{	   
		public InvCgunoPeriodoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoPeriodoCollection</returns>
		public InvCgunoPeriodoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoPeriodo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_PERIODOS table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoPeriodo : ActiveRecord<InvCgunoPeriodo>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoPeriodo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoPeriodo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoPeriodo(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoPeriodo(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_PERIODOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Decimal;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.AnsiString;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarPeriodo = new TableSchema.TableColumn(schema);
				colvarPeriodo.ColumnName = "PERIODO";
				colvarPeriodo.DataType = DbType.DateTime;
				colvarPeriodo.MaxLength = 0;
				colvarPeriodo.AutoIncrement = false;
				colvarPeriodo.IsNullable = false;
				colvarPeriodo.IsPrimaryKey = false;
				colvarPeriodo.IsForeignKey = false;
				colvarPeriodo.IsReadOnly = false;
				colvarPeriodo.DefaultSetting = @"";
				colvarPeriodo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeriodo);
				
				TableSchema.TableColumn colvarCodigoEmpresa = new TableSchema.TableColumn(schema);
				colvarCodigoEmpresa.ColumnName = "CODIGO_EMPRESA";
				colvarCodigoEmpresa.DataType = DbType.AnsiString;
				colvarCodigoEmpresa.MaxLength = 10;
				colvarCodigoEmpresa.AutoIncrement = false;
				colvarCodigoEmpresa.IsNullable = false;
				colvarCodigoEmpresa.IsPrimaryKey = false;
				colvarCodigoEmpresa.IsForeignKey = false;
				colvarCodigoEmpresa.IsReadOnly = false;
				colvarCodigoEmpresa.DefaultSetting = @"";
				colvarCodigoEmpresa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCodigoEmpresa);
				
				TableSchema.TableColumn colvarCentroUtilidad = new TableSchema.TableColumn(schema);
				colvarCentroUtilidad.ColumnName = "CENTRO_UTILIDAD";
				colvarCentroUtilidad.DataType = DbType.AnsiString;
				colvarCentroUtilidad.MaxLength = 10;
				colvarCentroUtilidad.AutoIncrement = false;
				colvarCentroUtilidad.IsNullable = false;
				colvarCentroUtilidad.IsPrimaryKey = false;
				colvarCentroUtilidad.IsForeignKey = false;
				colvarCentroUtilidad.IsReadOnly = false;
				colvarCentroUtilidad.DefaultSetting = @"";
				colvarCentroUtilidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCentroUtilidad);
				
				TableSchema.TableColumn colvarComprobante = new TableSchema.TableColumn(schema);
				colvarComprobante.ColumnName = "COMPROBANTE";
				colvarComprobante.DataType = DbType.AnsiString;
				colvarComprobante.MaxLength = 50;
				colvarComprobante.AutoIncrement = false;
				colvarComprobante.IsNullable = false;
				colvarComprobante.IsPrimaryKey = false;
				colvarComprobante.IsForeignKey = false;
				colvarComprobante.IsReadOnly = false;
				colvarComprobante.DefaultSetting = @"";
				colvarComprobante.ForeignKeyTableName = "";
				schema.Columns.Add(colvarComprobante);
				
				TableSchema.TableColumn colvarLote = new TableSchema.TableColumn(schema);
				colvarLote.ColumnName = "LOTE";
				colvarLote.DataType = DbType.AnsiString;
				colvarLote.MaxLength = 50;
				colvarLote.AutoIncrement = false;
				colvarLote.IsNullable = false;
				colvarLote.IsPrimaryKey = false;
				colvarLote.IsForeignKey = false;
				colvarLote.IsReadOnly = false;
				colvarLote.DefaultSetting = @"";
				colvarLote.ForeignKeyTableName = "";
				schema.Columns.Add(colvarLote);
				
				TableSchema.TableColumn colvarFechaLote = new TableSchema.TableColumn(schema);
				colvarFechaLote.ColumnName = "FECHA_LOTE";
				colvarFechaLote.DataType = DbType.DateTime;
				colvarFechaLote.MaxLength = 0;
				colvarFechaLote.AutoIncrement = false;
				colvarFechaLote.IsNullable = false;
				colvarFechaLote.IsPrimaryKey = false;
				colvarFechaLote.IsForeignKey = false;
				colvarFechaLote.IsReadOnly = false;
				colvarFechaLote.DefaultSetting = @"";
				colvarFechaLote.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaLote);
				
				TableSchema.TableColumn colvarIdCausacion = new TableSchema.TableColumn(schema);
				colvarIdCausacion.ColumnName = "ID_CAUSACION";
				colvarIdCausacion.DataType = DbType.Decimal;
				colvarIdCausacion.MaxLength = 0;
				colvarIdCausacion.AutoIncrement = false;
				colvarIdCausacion.IsNullable = false;
				colvarIdCausacion.IsPrimaryKey = false;
				colvarIdCausacion.IsForeignKey = true;
				colvarIdCausacion.IsReadOnly = false;
				colvarIdCausacion.DefaultSetting = @"";
				
					colvarIdCausacion.ForeignKeyTableName = "INV_CGUNO_CAUSACIONES";
				schema.Columns.Add(colvarIdCausacion);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
				colvarIdEstado.ColumnName = "ID_ESTADO";
				colvarIdEstado.DataType = DbType.Int32;
				colvarIdEstado.MaxLength = 0;
				colvarIdEstado.AutoIncrement = false;
				colvarIdEstado.IsNullable = false;
				colvarIdEstado.IsPrimaryKey = false;
				colvarIdEstado.IsForeignKey = true;
				colvarIdEstado.IsReadOnly = false;
				colvarIdEstado.DefaultSetting = @"";
				
					colvarIdEstado.ForeignKeyTableName = "INV_CGUNO_MAS_ESTADOS_PERIODO";
				schema.Columns.Add(colvarIdEstado);
				
				TableSchema.TableColumn colvarIdCentroUtilidad = new TableSchema.TableColumn(schema);
				colvarIdCentroUtilidad.ColumnName = "ID_CENTRO_UTILIDAD";
				colvarIdCentroUtilidad.DataType = DbType.Int32;
				colvarIdCentroUtilidad.MaxLength = 0;
				colvarIdCentroUtilidad.AutoIncrement = false;
				colvarIdCentroUtilidad.IsNullable = true;
				colvarIdCentroUtilidad.IsPrimaryKey = false;
				colvarIdCentroUtilidad.IsForeignKey = false;
				colvarIdCentroUtilidad.IsReadOnly = false;
				colvarIdCentroUtilidad.DefaultSetting = @"";
				colvarIdCentroUtilidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdCentroUtilidad);
				
				TableSchema.TableColumn colvarRutaArchivo = new TableSchema.TableColumn(schema);
				colvarRutaArchivo.ColumnName = "RUTA_ARCHIVO";
				colvarRutaArchivo.DataType = DbType.String;
				colvarRutaArchivo.MaxLength = 500;
				colvarRutaArchivo.AutoIncrement = false;
				colvarRutaArchivo.IsNullable = true;
				colvarRutaArchivo.IsPrimaryKey = false;
				colvarRutaArchivo.IsForeignKey = false;
				colvarRutaArchivo.IsReadOnly = false;
				colvarRutaArchivo.DefaultSetting = @"";
				colvarRutaArchivo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRutaArchivo);
				
				TableSchema.TableColumn colvarIdEstadoGeneracion = new TableSchema.TableColumn(schema);
				colvarIdEstadoGeneracion.ColumnName = "ID_ESTADO_GENERACION";
				colvarIdEstadoGeneracion.DataType = DbType.Int32;
				colvarIdEstadoGeneracion.MaxLength = 0;
				colvarIdEstadoGeneracion.AutoIncrement = false;
				colvarIdEstadoGeneracion.IsNullable = true;
				colvarIdEstadoGeneracion.IsPrimaryKey = false;
				colvarIdEstadoGeneracion.IsForeignKey = true;
				colvarIdEstadoGeneracion.IsReadOnly = false;
				colvarIdEstadoGeneracion.DefaultSetting = @"";
				
					colvarIdEstadoGeneracion.ForeignKeyTableName = "INV_CGUNO_MAS_ESTADOS_PERIODO";
				schema.Columns.Add(colvarIdEstadoGeneracion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_PERIODOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public decimal Id 
		{
			get { return GetColumnValue<decimal>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Periodo")]
		[Bindable(true)]
		public DateTime Periodo 
		{
			get { return GetColumnValue<DateTime>(Columns.Periodo); }
			set { SetColumnValue(Columns.Periodo, value); }
		}
		  
		[XmlAttribute("CodigoEmpresa")]
		[Bindable(true)]
		public string CodigoEmpresa 
		{
			get { return GetColumnValue<string>(Columns.CodigoEmpresa); }
			set { SetColumnValue(Columns.CodigoEmpresa, value); }
		}
		  
		[XmlAttribute("CentroUtilidad")]
		[Bindable(true)]
		public string CentroUtilidad 
		{
			get { return GetColumnValue<string>(Columns.CentroUtilidad); }
			set { SetColumnValue(Columns.CentroUtilidad, value); }
		}
		  
		[XmlAttribute("Comprobante")]
		[Bindable(true)]
		public string Comprobante 
		{
			get { return GetColumnValue<string>(Columns.Comprobante); }
			set { SetColumnValue(Columns.Comprobante, value); }
		}
		  
		[XmlAttribute("Lote")]
		[Bindable(true)]
		public string Lote 
		{
			get { return GetColumnValue<string>(Columns.Lote); }
			set { SetColumnValue(Columns.Lote, value); }
		}
		  
		[XmlAttribute("FechaLote")]
		[Bindable(true)]
		public DateTime FechaLote 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaLote); }
			set { SetColumnValue(Columns.FechaLote, value); }
		}
		  
		[XmlAttribute("IdCausacion")]
		[Bindable(true)]
		public decimal IdCausacion 
		{
			get { return GetColumnValue<decimal>(Columns.IdCausacion); }
			set { SetColumnValue(Columns.IdCausacion, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdEstado")]
		[Bindable(true)]
		public int IdEstado 
		{
			get { return GetColumnValue<int>(Columns.IdEstado); }
			set { SetColumnValue(Columns.IdEstado, value); }
		}
		  
		[XmlAttribute("IdCentroUtilidad")]
		[Bindable(true)]
		public int? IdCentroUtilidad 
		{
			get { return GetColumnValue<int?>(Columns.IdCentroUtilidad); }
			set { SetColumnValue(Columns.IdCentroUtilidad, value); }
		}
		  
		[XmlAttribute("RutaArchivo")]
		[Bindable(true)]
		public string RutaArchivo 
		{
			get { return GetColumnValue<string>(Columns.RutaArchivo); }
			set { SetColumnValue(Columns.RutaArchivo, value); }
		}
		  
		[XmlAttribute("IdEstadoGeneracion")]
		[Bindable(true)]
		public int? IdEstadoGeneracion 
		{
			get { return GetColumnValue<int?>(Columns.IdEstadoGeneracion); }
			set { SetColumnValue(Columns.IdEstadoGeneracion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvCgunoCausacionesPeriodoCollection InvCgunoCausacionesPeriodos()
		{
			return new Big.Helper.Domain.Generated.InvCgunoCausacionesPeriodoCollection().Where(InvCgunoCausacionesPeriodo.Columns.IdPeriodo, Id).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvPrograma ActiveRecord object related to this InvCgunoPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvPrograma InvPrograma
		{
			get { return Big.Helper.Domain.Generated.InvPrograma.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoCausacione ActiveRecord object related to this InvCgunoPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCausacione InvCgunoCausacione
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCausacione.FetchByID(this.IdCausacion); }
			set { SetColumnValue("ID_CAUSACION", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoMasEstadosPeriodo ActiveRecord object related to this InvCgunoPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoMasEstadosPeriodo InvCgunoMasEstadosPeriodo
		{
			get { return Big.Helper.Domain.Generated.InvCgunoMasEstadosPeriodo.FetchByID(this.IdEstado); }
			set { SetColumnValue("ID_ESTADO", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoMasEstadosPeriodo ActiveRecord object related to this InvCgunoPeriodo
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoMasEstadosPeriodo InvCgunoMasEstadosPeriodoToIdEstadoGeneracion
		{
			get { return Big.Helper.Domain.Generated.InvCgunoMasEstadosPeriodo.FetchByID(this.IdEstadoGeneracion); }
			set { SetColumnValue("ID_ESTADO_GENERACION", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,DateTime varPeriodo,string varCodigoEmpresa,string varCentroUtilidad,string varComprobante,string varLote,DateTime varFechaLote,decimal varIdCausacion,int varIdPrograma,int varIdEstado,int? varIdCentroUtilidad,string varRutaArchivo,int? varIdEstadoGeneracion)
		{
			InvCgunoPeriodo item = new InvCgunoPeriodo();
			
			item.Guid = varGuid;
			
			item.Periodo = varPeriodo;
			
			item.CodigoEmpresa = varCodigoEmpresa;
			
			item.CentroUtilidad = varCentroUtilidad;
			
			item.Comprobante = varComprobante;
			
			item.Lote = varLote;
			
			item.FechaLote = varFechaLote;
			
			item.IdCausacion = varIdCausacion;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdEstado = varIdEstado;
			
			item.IdCentroUtilidad = varIdCentroUtilidad;
			
			item.RutaArchivo = varRutaArchivo;
			
			item.IdEstadoGeneracion = varIdEstadoGeneracion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(decimal varId,string varGuid,DateTime varPeriodo,string varCodigoEmpresa,string varCentroUtilidad,string varComprobante,string varLote,DateTime varFechaLote,decimal varIdCausacion,int varIdPrograma,int varIdEstado,int? varIdCentroUtilidad,string varRutaArchivo,int? varIdEstadoGeneracion)
		{
			InvCgunoPeriodo item = new InvCgunoPeriodo();
			
				item.Id = varId;
			
				item.Guid = varGuid;
			
				item.Periodo = varPeriodo;
			
				item.CodigoEmpresa = varCodigoEmpresa;
			
				item.CentroUtilidad = varCentroUtilidad;
			
				item.Comprobante = varComprobante;
			
				item.Lote = varLote;
			
				item.FechaLote = varFechaLote;
			
				item.IdCausacion = varIdCausacion;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdEstado = varIdEstado;
			
				item.IdCentroUtilidad = varIdCentroUtilidad;
			
				item.RutaArchivo = varRutaArchivo;
			
				item.IdEstadoGeneracion = varIdEstadoGeneracion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PeriodoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CodigoEmpresaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn CentroUtilidadColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ComprobanteColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn LoteColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaLoteColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCausacionColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEstadoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCentroUtilidadColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn RutaArchivoColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEstadoGeneracionColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Guid = @"GUID";
			 public static string Periodo = @"PERIODO";
			 public static string CodigoEmpresa = @"CODIGO_EMPRESA";
			 public static string CentroUtilidad = @"CENTRO_UTILIDAD";
			 public static string Comprobante = @"COMPROBANTE";
			 public static string Lote = @"LOTE";
			 public static string FechaLote = @"FECHA_LOTE";
			 public static string IdCausacion = @"ID_CAUSACION";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdEstado = @"ID_ESTADO";
			 public static string IdCentroUtilidad = @"ID_CENTRO_UTILIDAD";
			 public static string RutaArchivo = @"RUTA_ARCHIVO";
			 public static string IdEstadoGeneracion = @"ID_ESTADO_GENERACION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
