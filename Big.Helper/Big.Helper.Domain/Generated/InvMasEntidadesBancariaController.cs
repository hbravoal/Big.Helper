using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_ENTIDADES_BANCARIAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasEntidadesBancariaController
    {
        // Preload our schema..
        InvMasEntidadesBancaria thisSchemaLoad = new InvMasEntidadesBancaria();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasEntidadesBancariaCollection FetchAll()
        {
            InvMasEntidadesBancariaCollection coll = new InvMasEntidadesBancariaCollection();
            Query qry = new Query(InvMasEntidadesBancaria.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasEntidadesBancariaCollection FetchByID(object IdEntidadBancaria)
        {
            InvMasEntidadesBancariaCollection coll = new InvMasEntidadesBancariaCollection().Where("ID_ENTIDAD_BANCARIA", IdEntidadBancaria).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasEntidadesBancariaCollection FetchByQuery(Query qry)
        {
            InvMasEntidadesBancariaCollection coll = new InvMasEntidadesBancariaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdEntidadBancaria)
        {
            return (InvMasEntidadesBancaria.Delete(IdEntidadBancaria) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdEntidadBancaria)
        {
            return (InvMasEntidadesBancaria.Destroy(IdEntidadBancaria) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string NombreEntidad)
	    {
		    InvMasEntidadesBancaria item = new InvMasEntidadesBancaria();
		    
            item.NombreEntidad = NombreEntidad;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdEntidadBancaria,string NombreEntidad)
	    {
		    InvMasEntidadesBancaria item = new InvMasEntidadesBancaria();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdEntidadBancaria = IdEntidadBancaria;
				
			item.NombreEntidad = NombreEntidad;
				
	        item.Save(UserName);
	    }
    }
}
