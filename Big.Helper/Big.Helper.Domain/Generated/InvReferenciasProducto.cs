using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvReferenciasProducto class.
	/// </summary>
    [Serializable]
	public partial class InvReferenciasProductoCollection : ActiveList<InvReferenciasProducto, InvReferenciasProductoCollection>
	{	   
		public InvReferenciasProductoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvReferenciasProductoCollection</returns>
		public InvReferenciasProductoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvReferenciasProducto o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_REFERENCIAS_PRODUCTO table.
	/// </summary>
	[Serializable]
	public partial class InvReferenciasProducto : ActiveRecord<InvReferenciasProducto>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvReferenciasProducto()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvReferenciasProducto(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvReferenciasProducto(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvReferenciasProducto(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_REFERENCIAS_PRODUCTO", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = true;
				colvarGuidReferenciaProducto.IsForeignKey = false;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				colvarGuidReferenciaProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				TableSchema.TableColumn colvarNombreReferencia = new TableSchema.TableColumn(schema);
				colvarNombreReferencia.ColumnName = "NOMBRE_REFERENCIA";
				colvarNombreReferencia.DataType = DbType.String;
				colvarNombreReferencia.MaxLength = 100;
				colvarNombreReferencia.AutoIncrement = false;
				colvarNombreReferencia.IsNullable = false;
				colvarNombreReferencia.IsPrimaryKey = false;
				colvarNombreReferencia.IsForeignKey = false;
				colvarNombreReferencia.IsReadOnly = false;
				colvarNombreReferencia.DefaultSetting = @"";
				colvarNombreReferencia.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreReferencia);
				
				TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
				colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
				colvarGuidProducto.DataType = DbType.String;
				colvarGuidProducto.MaxLength = 36;
				colvarGuidProducto.AutoIncrement = false;
				colvarGuidProducto.IsNullable = false;
				colvarGuidProducto.IsPrimaryKey = false;
				colvarGuidProducto.IsForeignKey = true;
				colvarGuidProducto.IsReadOnly = false;
				colvarGuidProducto.DefaultSetting = @"";
				
					colvarGuidProducto.ForeignKeyTableName = "INV_PRODUCTOS";
				schema.Columns.Add(colvarGuidProducto);
				
				TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
				colvarIdEstado.ColumnName = "ID_ESTADO";
				colvarIdEstado.DataType = DbType.Int32;
				colvarIdEstado.MaxLength = 0;
				colvarIdEstado.AutoIncrement = false;
				colvarIdEstado.IsNullable = false;
				colvarIdEstado.IsPrimaryKey = false;
				colvarIdEstado.IsForeignKey = true;
				colvarIdEstado.IsReadOnly = false;
				colvarIdEstado.DefaultSetting = @"";
				
					colvarIdEstado.ForeignKeyTableName = "INV_MAS_ESTADOS";
				schema.Columns.Add(colvarIdEstado);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_REFERENCIAS_PRODUCTO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		  
		[XmlAttribute("NombreReferencia")]
		[Bindable(true)]
		public string NombreReferencia 
		{
			get { return GetColumnValue<string>(Columns.NombreReferencia); }
			set { SetColumnValue(Columns.NombreReferencia, value); }
		}
		  
		[XmlAttribute("GuidProducto")]
		[Bindable(true)]
		public string GuidProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidProducto); }
			set { SetColumnValue(Columns.GuidProducto, value); }
		}
		  
		[XmlAttribute("IdEstado")]
		[Bindable(true)]
		public int IdEstado 
		{
			get { return GetColumnValue<int>(Columns.IdEstado); }
			set { SetColumnValue(Columns.IdEstado, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvBodegasProductoCollection InvBodegasProductos()
		{
			return new Big.Helper.Domain.Generated.InvBodegasProductoCollection().Where(InvBodegasProducto.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load();
		}
		public Big.Helper.Domain.Generated.InvCacheDetallesFacturaCollection InvCacheDetallesFacturaRecords()
		{
			return new Big.Helper.Domain.Generated.InvCacheDetallesFacturaCollection().Where(InvCacheDetallesFactura.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load();
		}
		public Big.Helper.Domain.Generated.InvCgunoDetallesCausacionesPeriodoCollection InvCgunoDetallesCausacionesPeriodoRecords()
		{
			return new Big.Helper.Domain.Generated.InvCgunoDetallesCausacionesPeriodoCollection().Where(InvCgunoDetallesCausacionesPeriodo.Columns.GuidReferencia, GuidReferenciaProducto).Load();
		}
		public Big.Helper.Domain.Generated.InvDetallesFacturaBancoCollection InvDetallesFacturaBancoRecords()
		{
			return new Big.Helper.Domain.Generated.InvDetallesFacturaBancoCollection().Where(InvDetallesFacturaBanco.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load();
		}
		public Big.Helper.Domain.Generated.InvDetallesFacturaCollection InvDetallesFacturaRecords()
		{
			return new Big.Helper.Domain.Generated.InvDetallesFacturaCollection().Where(InvDetallesFactura.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load();
		}
		public Big.Helper.Domain.Generated.InvSiniestroCollection InvSiniestros()
		{
			return new Big.Helper.Domain.Generated.InvSiniestroCollection().Where(InvSiniestro.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasEstado ActiveRecord object related to this InvReferenciasProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasEstado InvMasEstado
		{
			get { return Big.Helper.Domain.Generated.InvMasEstado.FetchByID(this.IdEstado); }
			set { SetColumnValue("ID_ESTADO", value.IdEstado); }
		}
		
		
		/// <summary>
		/// Returns a InvProducto ActiveRecord object related to this InvReferenciasProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvProducto InvProducto
		{
			get { return Big.Helper.Domain.Generated.InvProducto.FetchByID(this.GuidProducto); }
			set { SetColumnValue("GUID_PRODUCTO", value.Guid); }
		}
		
		
		/// <summary>
		/// Returns a InvPrograma ActiveRecord object related to this InvReferenciasProducto
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvPrograma InvPrograma
		{
			get { return Big.Helper.Domain.Generated.InvPrograma.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidReferenciaProducto,string varNombreReferencia,string varGuidProducto,int varIdEstado,int varIdPrograma)
		{
			InvReferenciasProducto item = new InvReferenciasProducto();
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.NombreReferencia = varNombreReferencia;
			
			item.GuidProducto = varGuidProducto;
			
			item.IdEstado = varIdEstado;
			
			item.IdPrograma = varIdPrograma;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidReferenciaProducto,string varNombreReferencia,string varGuidProducto,int varIdEstado,int varIdPrograma)
		{
			InvReferenciasProducto item = new InvReferenciasProducto();
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
				item.NombreReferencia = varNombreReferencia;
			
				item.GuidProducto = varGuidProducto;
			
				item.IdEstado = varIdEstado;
			
				item.IdPrograma = varIdPrograma;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreReferenciaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidProductoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEstadoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
			 public static string NombreReferencia = @"NOMBRE_REFERENCIA";
			 public static string GuidProducto = @"GUID_PRODUCTO";
			 public static string IdEstado = @"ID_ESTADO";
			 public static string IdPrograma = @"ID_PROGRAMA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
