using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_AVM_CONTROL_ARCHIVOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvAvmControlArchivoController
    {
        // Preload our schema..
        InvAvmControlArchivo thisSchemaLoad = new InvAvmControlArchivo();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvAvmControlArchivoCollection FetchAll()
        {
            InvAvmControlArchivoCollection coll = new InvAvmControlArchivoCollection();
            Query qry = new Query(InvAvmControlArchivo.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvAvmControlArchivoCollection FetchByID(object Programa)
        {
            InvAvmControlArchivoCollection coll = new InvAvmControlArchivoCollection().Where("Programa", Programa).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvAvmControlArchivoCollection FetchByQuery(Query qry)
        {
            InvAvmControlArchivoCollection coll = new InvAvmControlArchivoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Programa)
        {
            return (InvAvmControlArchivo.Delete(Programa) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Programa)
        {
            return (InvAvmControlArchivo.Destroy(Programa) == 1);
        }
        
        
        
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(int Programa,int PeriodoMes,int PeridoAno)
        {
            Query qry = new Query(InvAvmControlArchivo.Schema);
            qry.QueryType = QueryType.Delete;
            qry.AddWhere("Programa", Programa).AND("PeriodoMes", PeriodoMes).AND("PeridoAno", PeridoAno);
            qry.Execute();
            return (true);
        }        
       
    	
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int Programa,int PeriodoMes,int PeridoAno,int CurrentNumber,int NextNumber)
	    {
		    InvAvmControlArchivo item = new InvAvmControlArchivo();
		    
            item.Programa = Programa;
            
            item.PeriodoMes = PeriodoMes;
            
            item.PeridoAno = PeridoAno;
            
            item.CurrentNumber = CurrentNumber;
            
            item.NextNumber = NextNumber;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int Programa,int PeriodoMes,int PeridoAno,int CurrentNumber,int NextNumber)
	    {
		    InvAvmControlArchivo item = new InvAvmControlArchivo();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Programa = Programa;
				
			item.PeriodoMes = PeriodoMes;
				
			item.PeridoAno = PeridoAno;
				
			item.CurrentNumber = CurrentNumber;
				
			item.NextNumber = NextNumber;
				
	        item.Save(UserName);
	    }
    }
}
