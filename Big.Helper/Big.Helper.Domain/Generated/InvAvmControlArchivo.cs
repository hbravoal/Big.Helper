using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvAvmControlArchivo class.
	/// </summary>
    [Serializable]
	public partial class InvAvmControlArchivoCollection : ActiveList<InvAvmControlArchivo, InvAvmControlArchivoCollection>
	{	   
		public InvAvmControlArchivoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvAvmControlArchivoCollection</returns>
		public InvAvmControlArchivoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvAvmControlArchivo o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_AVM_CONTROL_ARCHIVOS table.
	/// </summary>
	[Serializable]
	public partial class InvAvmControlArchivo : ActiveRecord<InvAvmControlArchivo>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvAvmControlArchivo()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvAvmControlArchivo(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvAvmControlArchivo(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvAvmControlArchivo(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_AVM_CONTROL_ARCHIVOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarPrograma = new TableSchema.TableColumn(schema);
				colvarPrograma.ColumnName = "Programa";
				colvarPrograma.DataType = DbType.Int32;
				colvarPrograma.MaxLength = 0;
				colvarPrograma.AutoIncrement = false;
				colvarPrograma.IsNullable = false;
				colvarPrograma.IsPrimaryKey = true;
				colvarPrograma.IsForeignKey = false;
				colvarPrograma.IsReadOnly = false;
				colvarPrograma.DefaultSetting = @"";
				colvarPrograma.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrograma);
				
				TableSchema.TableColumn colvarPeriodoMes = new TableSchema.TableColumn(schema);
				colvarPeriodoMes.ColumnName = "PeriodoMes";
				colvarPeriodoMes.DataType = DbType.Int32;
				colvarPeriodoMes.MaxLength = 0;
				colvarPeriodoMes.AutoIncrement = false;
				colvarPeriodoMes.IsNullable = false;
				colvarPeriodoMes.IsPrimaryKey = true;
				colvarPeriodoMes.IsForeignKey = false;
				colvarPeriodoMes.IsReadOnly = false;
				colvarPeriodoMes.DefaultSetting = @"";
				colvarPeriodoMes.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeriodoMes);
				
				TableSchema.TableColumn colvarPeridoAno = new TableSchema.TableColumn(schema);
				colvarPeridoAno.ColumnName = "PeridoAno";
				colvarPeridoAno.DataType = DbType.Int32;
				colvarPeridoAno.MaxLength = 0;
				colvarPeridoAno.AutoIncrement = false;
				colvarPeridoAno.IsNullable = false;
				colvarPeridoAno.IsPrimaryKey = true;
				colvarPeridoAno.IsForeignKey = false;
				colvarPeridoAno.IsReadOnly = false;
				colvarPeridoAno.DefaultSetting = @"";
				colvarPeridoAno.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeridoAno);
				
				TableSchema.TableColumn colvarCurrentNumber = new TableSchema.TableColumn(schema);
				colvarCurrentNumber.ColumnName = "CurrentNumber";
				colvarCurrentNumber.DataType = DbType.Int32;
				colvarCurrentNumber.MaxLength = 0;
				colvarCurrentNumber.AutoIncrement = false;
				colvarCurrentNumber.IsNullable = false;
				colvarCurrentNumber.IsPrimaryKey = false;
				colvarCurrentNumber.IsForeignKey = false;
				colvarCurrentNumber.IsReadOnly = false;
				colvarCurrentNumber.DefaultSetting = @"";
				colvarCurrentNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCurrentNumber);
				
				TableSchema.TableColumn colvarNextNumber = new TableSchema.TableColumn(schema);
				colvarNextNumber.ColumnName = "NextNumber";
				colvarNextNumber.DataType = DbType.Int32;
				colvarNextNumber.MaxLength = 0;
				colvarNextNumber.AutoIncrement = false;
				colvarNextNumber.IsNullable = false;
				colvarNextNumber.IsPrimaryKey = false;
				colvarNextNumber.IsForeignKey = false;
				colvarNextNumber.IsReadOnly = false;
				colvarNextNumber.DefaultSetting = @"";
				colvarNextNumber.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNextNumber);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_AVM_CONTROL_ARCHIVOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Programa")]
		[Bindable(true)]
		public int Programa 
		{
			get { return GetColumnValue<int>(Columns.Programa); }
			set { SetColumnValue(Columns.Programa, value); }
		}
		  
		[XmlAttribute("PeriodoMes")]
		[Bindable(true)]
		public int PeriodoMes 
		{
			get { return GetColumnValue<int>(Columns.PeriodoMes); }
			set { SetColumnValue(Columns.PeriodoMes, value); }
		}
		  
		[XmlAttribute("PeridoAno")]
		[Bindable(true)]
		public int PeridoAno 
		{
			get { return GetColumnValue<int>(Columns.PeridoAno); }
			set { SetColumnValue(Columns.PeridoAno, value); }
		}
		  
		[XmlAttribute("CurrentNumber")]
		[Bindable(true)]
		public int CurrentNumber 
		{
			get { return GetColumnValue<int>(Columns.CurrentNumber); }
			set { SetColumnValue(Columns.CurrentNumber, value); }
		}
		  
		[XmlAttribute("NextNumber")]
		[Bindable(true)]
		public int NextNumber 
		{
			get { return GetColumnValue<int>(Columns.NextNumber); }
			set { SetColumnValue(Columns.NextNumber, value); }
		}
		
		#endregion
		
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varPrograma,int varPeriodoMes,int varPeridoAno,int varCurrentNumber,int varNextNumber)
		{
			InvAvmControlArchivo item = new InvAvmControlArchivo();
			
			item.Programa = varPrograma;
			
			item.PeriodoMes = varPeriodoMes;
			
			item.PeridoAno = varPeridoAno;
			
			item.CurrentNumber = varCurrentNumber;
			
			item.NextNumber = varNextNumber;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varPrograma,int varPeriodoMes,int varPeridoAno,int varCurrentNumber,int varNextNumber)
		{
			InvAvmControlArchivo item = new InvAvmControlArchivo();
			
				item.Programa = varPrograma;
			
				item.PeriodoMes = varPeriodoMes;
			
				item.PeridoAno = varPeridoAno;
			
				item.CurrentNumber = varCurrentNumber;
			
				item.NextNumber = varNextNumber;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn ProgramaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PeriodoMesColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn PeridoAnoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CurrentNumberColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn NextNumberColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Programa = @"Programa";
			 public static string PeriodoMes = @"PeriodoMes";
			 public static string PeridoAno = @"PeridoAno";
			 public static string CurrentNumber = @"CurrentNumber";
			 public static string NextNumber = @"NextNumber";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
