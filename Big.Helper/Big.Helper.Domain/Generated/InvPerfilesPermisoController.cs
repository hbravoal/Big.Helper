using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_PERFILES_PERMISOS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvPerfilesPermisoController
    {
        // Preload our schema..
        InvPerfilesPermiso thisSchemaLoad = new InvPerfilesPermiso();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvPerfilesPermisoCollection FetchAll()
        {
            InvPerfilesPermisoCollection coll = new InvPerfilesPermisoCollection();
            Query qry = new Query(InvPerfilesPermiso.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPerfilesPermisoCollection FetchByID(object IdPefilPermiso)
        {
            InvPerfilesPermisoCollection coll = new InvPerfilesPermisoCollection().Where("ID_PEFIL_PERMISO", IdPefilPermiso).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvPerfilesPermisoCollection FetchByQuery(Query qry)
        {
            InvPerfilesPermisoCollection coll = new InvPerfilesPermisoCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPefilPermiso)
        {
            return (InvPerfilesPermiso.Delete(IdPefilPermiso) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPefilPermiso)
        {
            return (InvPerfilesPermiso.Destroy(IdPefilPermiso) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdPerfil,int IdPermiso)
	    {
		    InvPerfilesPermiso item = new InvPerfilesPermiso();
		    
            item.IdPerfil = IdPerfil;
            
            item.IdPermiso = IdPermiso;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(decimal IdPefilPermiso,int IdPerfil,int IdPermiso)
	    {
		    InvPerfilesPermiso item = new InvPerfilesPermiso();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPefilPermiso = IdPefilPermiso;
				
			item.IdPerfil = IdPerfil;
				
			item.IdPermiso = IdPermiso;
				
	        item.Save(UserName);
	    }
    }
}
