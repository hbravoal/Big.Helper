using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwProductosExportar class.
    /// </summary>
    [Serializable]
    public partial class InvVwProductosExportarCollection : ReadOnlyList<InvVwProductosExportar, InvVwProductosExportarCollection>
    {        
        public InvVwProductosExportarCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_PRODUCTOS_EXPORTAR view.
    /// </summary>
    [Serializable]
    public partial class InvVwProductosExportar : ReadOnlyRecord<InvVwProductosExportar>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_PRODUCTOS_EXPORTAR", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.String;
                colvarGuid.MaxLength = 36;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarCodigo = new TableSchema.TableColumn(schema);
                colvarCodigo.ColumnName = "CODIGO";
                colvarCodigo.DataType = DbType.AnsiString;
                colvarCodigo.MaxLength = 15;
                colvarCodigo.AutoIncrement = false;
                colvarCodigo.IsNullable = false;
                colvarCodigo.IsPrimaryKey = false;
                colvarCodigo.IsForeignKey = false;
                colvarCodigo.IsReadOnly = false;
                
                schema.Columns.Add(colvarCodigo);
                
                TableSchema.TableColumn colvarNombreDelProducto = new TableSchema.TableColumn(schema);
                colvarNombreDelProducto.ColumnName = "Nombre del Producto";
                colvarNombreDelProducto.DataType = DbType.String;
                colvarNombreDelProducto.MaxLength = 100;
                colvarNombreDelProducto.AutoIncrement = false;
                colvarNombreDelProducto.IsNullable = false;
                colvarNombreDelProducto.IsPrimaryKey = false;
                colvarNombreDelProducto.IsForeignKey = false;
                colvarNombreDelProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreDelProducto);
                
                TableSchema.TableColumn colvarDescripciónDelProducto = new TableSchema.TableColumn(schema);
                colvarDescripciónDelProducto.ColumnName = "Descripción del Producto";
                colvarDescripciónDelProducto.DataType = DbType.String;
                colvarDescripciónDelProducto.MaxLength = 4000;
                colvarDescripciónDelProducto.AutoIncrement = false;
                colvarDescripciónDelProducto.IsNullable = true;
                colvarDescripciónDelProducto.IsPrimaryKey = false;
                colvarDescripciónDelProducto.IsForeignKey = false;
                colvarDescripciónDelProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescripciónDelProducto);
                
                TableSchema.TableColumn colvarLocalización = new TableSchema.TableColumn(schema);
                colvarLocalización.ColumnName = "Localización";
                colvarLocalización.DataType = DbType.String;
                colvarLocalización.MaxLength = 200;
                colvarLocalización.AutoIncrement = false;
                colvarLocalización.IsNullable = false;
                colvarLocalización.IsPrimaryKey = false;
                colvarLocalización.IsForeignKey = false;
                colvarLocalización.IsReadOnly = false;
                
                schema.Columns.Add(colvarLocalización);
                
                TableSchema.TableColumn colvarCódigoDeBarras = new TableSchema.TableColumn(schema);
                colvarCódigoDeBarras.ColumnName = "Código de Barras";
                colvarCódigoDeBarras.DataType = DbType.String;
                colvarCódigoDeBarras.MaxLength = 50;
                colvarCódigoDeBarras.AutoIncrement = false;
                colvarCódigoDeBarras.IsNullable = false;
                colvarCódigoDeBarras.IsPrimaryKey = false;
                colvarCódigoDeBarras.IsForeignKey = false;
                colvarCódigoDeBarras.IsReadOnly = false;
                
                schema.Columns.Add(colvarCódigoDeBarras);
                
                TableSchema.TableColumn colvarTipoDeEmpaque = new TableSchema.TableColumn(schema);
                colvarTipoDeEmpaque.ColumnName = "Tipo de Empaque";
                colvarTipoDeEmpaque.DataType = DbType.String;
                colvarTipoDeEmpaque.MaxLength = 200;
                colvarTipoDeEmpaque.AutoIncrement = false;
                colvarTipoDeEmpaque.IsNullable = false;
                colvarTipoDeEmpaque.IsPrimaryKey = false;
                colvarTipoDeEmpaque.IsForeignKey = false;
                colvarTipoDeEmpaque.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoDeEmpaque);
                
                TableSchema.TableColumn colvarUnidadesPorEmpaque = new TableSchema.TableColumn(schema);
                colvarUnidadesPorEmpaque.ColumnName = "Unidades Por Empaque";
                colvarUnidadesPorEmpaque.DataType = DbType.AnsiString;
                colvarUnidadesPorEmpaque.MaxLength = 3;
                colvarUnidadesPorEmpaque.AutoIncrement = false;
                colvarUnidadesPorEmpaque.IsNullable = false;
                colvarUnidadesPorEmpaque.IsPrimaryKey = false;
                colvarUnidadesPorEmpaque.IsForeignKey = false;
                colvarUnidadesPorEmpaque.IsReadOnly = false;
                
                schema.Columns.Add(colvarUnidadesPorEmpaque);
                
                TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
                colvarCostoBase.ColumnName = "Costo Base";
                colvarCostoBase.DataType = DbType.Decimal;
                colvarCostoBase.MaxLength = 0;
                colvarCostoBase.AutoIncrement = false;
                colvarCostoBase.IsNullable = false;
                colvarCostoBase.IsPrimaryKey = false;
                colvarCostoBase.IsForeignKey = false;
                colvarCostoBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarCostoBase);
                
                TableSchema.TableColumn colvarLargo = new TableSchema.TableColumn(schema);
                colvarLargo.ColumnName = "LARGO";
                colvarLargo.DataType = DbType.Decimal;
                colvarLargo.MaxLength = 0;
                colvarLargo.AutoIncrement = false;
                colvarLargo.IsNullable = false;
                colvarLargo.IsPrimaryKey = false;
                colvarLargo.IsForeignKey = false;
                colvarLargo.IsReadOnly = false;
                
                schema.Columns.Add(colvarLargo);
                
                TableSchema.TableColumn colvarAncho = new TableSchema.TableColumn(schema);
                colvarAncho.ColumnName = "ANCHO";
                colvarAncho.DataType = DbType.Decimal;
                colvarAncho.MaxLength = 0;
                colvarAncho.AutoIncrement = false;
                colvarAncho.IsNullable = false;
                colvarAncho.IsPrimaryKey = false;
                colvarAncho.IsForeignKey = false;
                colvarAncho.IsReadOnly = false;
                
                schema.Columns.Add(colvarAncho);
                
                TableSchema.TableColumn colvarAlto = new TableSchema.TableColumn(schema);
                colvarAlto.ColumnName = "ALTO";
                colvarAlto.DataType = DbType.Decimal;
                colvarAlto.MaxLength = 0;
                colvarAlto.AutoIncrement = false;
                colvarAlto.IsNullable = false;
                colvarAlto.IsPrimaryKey = false;
                colvarAlto.IsForeignKey = false;
                colvarAlto.IsReadOnly = false;
                
                schema.Columns.Add(colvarAlto);
                
                TableSchema.TableColumn colvarPeso = new TableSchema.TableColumn(schema);
                colvarPeso.ColumnName = "PESO";
                colvarPeso.DataType = DbType.Decimal;
                colvarPeso.MaxLength = 0;
                colvarPeso.AutoIncrement = false;
                colvarPeso.IsNullable = false;
                colvarPeso.IsPrimaryKey = false;
                colvarPeso.IsForeignKey = false;
                colvarPeso.IsReadOnly = false;
                
                schema.Columns.Add(colvarPeso);
                
                TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
                colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
                colvarIdProveedor.DataType = DbType.Int32;
                colvarIdProveedor.MaxLength = 0;
                colvarIdProveedor.AutoIncrement = false;
                colvarIdProveedor.IsNullable = false;
                colvarIdProveedor.IsPrimaryKey = false;
                colvarIdProveedor.IsForeignKey = false;
                colvarIdProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdProveedor);
                
                TableSchema.TableColumn colvarNombreDelProveedor = new TableSchema.TableColumn(schema);
                colvarNombreDelProveedor.ColumnName = "Nombre del Proveedor";
                colvarNombreDelProveedor.DataType = DbType.String;
                colvarNombreDelProveedor.MaxLength = 200;
                colvarNombreDelProveedor.AutoIncrement = false;
                colvarNombreDelProveedor.IsNullable = false;
                colvarNombreDelProveedor.IsPrimaryKey = false;
                colvarNombreDelProveedor.IsForeignKey = false;
                colvarNombreDelProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreDelProveedor);
                
                TableSchema.TableColumn colvarIdTipoProducto = new TableSchema.TableColumn(schema);
                colvarIdTipoProducto.ColumnName = "ID_TIPO_PRODUCTO";
                colvarIdTipoProducto.DataType = DbType.Int32;
                colvarIdTipoProducto.MaxLength = 0;
                colvarIdTipoProducto.AutoIncrement = false;
                colvarIdTipoProducto.IsNullable = false;
                colvarIdTipoProducto.IsPrimaryKey = false;
                colvarIdTipoProducto.IsForeignKey = false;
                colvarIdTipoProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdTipoProducto);
                
                TableSchema.TableColumn colvarTipoDeProducto = new TableSchema.TableColumn(schema);
                colvarTipoDeProducto.ColumnName = "Tipo de Producto";
                colvarTipoDeProducto.DataType = DbType.String;
                colvarTipoDeProducto.MaxLength = 100;
                colvarTipoDeProducto.AutoIncrement = false;
                colvarTipoDeProducto.IsNullable = false;
                colvarTipoDeProducto.IsPrimaryKey = false;
                colvarTipoDeProducto.IsForeignKey = false;
                colvarTipoDeProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoDeProducto);
                
                TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
                colvarIva.ColumnName = "IVA";
                colvarIva.DataType = DbType.Decimal;
                colvarIva.MaxLength = 0;
                colvarIva.AutoIncrement = false;
                colvarIva.IsNullable = false;
                colvarIva.IsPrimaryKey = false;
                colvarIva.IsForeignKey = false;
                colvarIva.IsReadOnly = false;
                
                schema.Columns.Add(colvarIva);
                
                TableSchema.TableColumn colvarRetenciónEnLaFuente = new TableSchema.TableColumn(schema);
                colvarRetenciónEnLaFuente.ColumnName = "Retención en la fuente";
                colvarRetenciónEnLaFuente.DataType = DbType.Decimal;
                colvarRetenciónEnLaFuente.MaxLength = 0;
                colvarRetenciónEnLaFuente.AutoIncrement = false;
                colvarRetenciónEnLaFuente.IsNullable = false;
                colvarRetenciónEnLaFuente.IsPrimaryKey = false;
                colvarRetenciónEnLaFuente.IsForeignKey = false;
                colvarRetenciónEnLaFuente.IsReadOnly = false;
                
                schema.Columns.Add(colvarRetenciónEnLaFuente);
                
                TableSchema.TableColumn colvarDescuento = new TableSchema.TableColumn(schema);
                colvarDescuento.ColumnName = "DESCUENTO";
                colvarDescuento.DataType = DbType.Decimal;
                colvarDescuento.MaxLength = 0;
                colvarDescuento.AutoIncrement = false;
                colvarDescuento.IsNullable = true;
                colvarDescuento.IsPrimaryKey = false;
                colvarDescuento.IsForeignKey = false;
                colvarDescuento.IsReadOnly = false;
                
                schema.Columns.Add(colvarDescuento);
                
                TableSchema.TableColumn colvarPrograma = new TableSchema.TableColumn(schema);
                colvarPrograma.ColumnName = "Programa";
                colvarPrograma.DataType = DbType.String;
                colvarPrograma.MaxLength = 100;
                colvarPrograma.AutoIncrement = false;
                colvarPrograma.IsNullable = true;
                colvarPrograma.IsPrimaryKey = false;
                colvarPrograma.IsForeignKey = false;
                colvarPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrograma);
                
                TableSchema.TableColumn colvarPrecioDeVentaBase = new TableSchema.TableColumn(schema);
                colvarPrecioDeVentaBase.ColumnName = "Precio de Venta Base";
                colvarPrecioDeVentaBase.DataType = DbType.Decimal;
                colvarPrecioDeVentaBase.MaxLength = 0;
                colvarPrecioDeVentaBase.AutoIncrement = false;
                colvarPrecioDeVentaBase.IsNullable = true;
                colvarPrecioDeVentaBase.IsPrimaryKey = false;
                colvarPrecioDeVentaBase.IsForeignKey = false;
                colvarPrecioDeVentaBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarPrecioDeVentaBase);
                
                TableSchema.TableColumn colvarCostoDeEnvíoBase = new TableSchema.TableColumn(schema);
                colvarCostoDeEnvíoBase.ColumnName = "Costo de Envío Base";
                colvarCostoDeEnvíoBase.DataType = DbType.Decimal;
                colvarCostoDeEnvíoBase.MaxLength = 0;
                colvarCostoDeEnvíoBase.AutoIncrement = false;
                colvarCostoDeEnvíoBase.IsNullable = true;
                colvarCostoDeEnvíoBase.IsPrimaryKey = false;
                colvarCostoDeEnvíoBase.IsForeignKey = false;
                colvarCostoDeEnvíoBase.IsReadOnly = false;
                
                schema.Columns.Add(colvarCostoDeEnvíoBase);
                
                TableSchema.TableColumn colvarEstablecimiento = new TableSchema.TableColumn(schema);
                colvarEstablecimiento.ColumnName = "Establecimiento";
                colvarEstablecimiento.DataType = DbType.String;
                colvarEstablecimiento.MaxLength = 200;
                colvarEstablecimiento.AutoIncrement = false;
                colvarEstablecimiento.IsNullable = true;
                colvarEstablecimiento.IsPrimaryKey = false;
                colvarEstablecimiento.IsForeignKey = false;
                colvarEstablecimiento.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstablecimiento);
                
                TableSchema.TableColumn colvarTipoDeDelivery = new TableSchema.TableColumn(schema);
                colvarTipoDeDelivery.ColumnName = "Tipo de Delivery";
                colvarTipoDeDelivery.DataType = DbType.String;
                colvarTipoDeDelivery.MaxLength = 100;
                colvarTipoDeDelivery.AutoIncrement = false;
                colvarTipoDeDelivery.IsNullable = true;
                colvarTipoDeDelivery.IsPrimaryKey = false;
                colvarTipoDeDelivery.IsForeignKey = false;
                colvarTipoDeDelivery.IsReadOnly = false;
                
                schema.Columns.Add(colvarTipoDeDelivery);
                
                TableSchema.TableColumn colvarIdMarca = new TableSchema.TableColumn(schema);
                colvarIdMarca.ColumnName = "ID_MARCA";
                colvarIdMarca.DataType = DbType.Int32;
                colvarIdMarca.MaxLength = 0;
                colvarIdMarca.AutoIncrement = false;
                colvarIdMarca.IsNullable = true;
                colvarIdMarca.IsPrimaryKey = false;
                colvarIdMarca.IsForeignKey = false;
                colvarIdMarca.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdMarca);
                
                TableSchema.TableColumn colvarValorAsegurado = new TableSchema.TableColumn(schema);
                colvarValorAsegurado.ColumnName = "Valor Asegurado";
                colvarValorAsegurado.DataType = DbType.Decimal;
                colvarValorAsegurado.MaxLength = 0;
                colvarValorAsegurado.AutoIncrement = false;
                colvarValorAsegurado.IsNullable = false;
                colvarValorAsegurado.IsPrimaryKey = false;
                colvarValorAsegurado.IsForeignKey = false;
                colvarValorAsegurado.IsReadOnly = false;
                
                schema.Columns.Add(colvarValorAsegurado);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_PRODUCTOS_EXPORTAR",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwProductosExportar()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwProductosExportar(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwProductosExportar(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwProductosExportar(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public string Guid 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("Codigo")]
        [Bindable(true)]
        public string Codigo 
	    {
		    get
		    {
			    return GetColumnValue<string>("CODIGO");
		    }
            set 
		    {
			    SetColumnValue("CODIGO", value);
            }
        }
	      
        [XmlAttribute("NombreDelProducto")]
        [Bindable(true)]
        public string NombreDelProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nombre del Producto");
		    }
            set 
		    {
			    SetColumnValue("Nombre del Producto", value);
            }
        }
	      
        [XmlAttribute("DescripciónDelProducto")]
        [Bindable(true)]
        public string DescripciónDelProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("Descripción del Producto");
		    }
            set 
		    {
			    SetColumnValue("Descripción del Producto", value);
            }
        }
	      
        [XmlAttribute("Localización")]
        [Bindable(true)]
        public string Localización 
	    {
		    get
		    {
			    return GetColumnValue<string>("Localización");
		    }
            set 
		    {
			    SetColumnValue("Localización", value);
            }
        }
	      
        [XmlAttribute("CódigoDeBarras")]
        [Bindable(true)]
        public string CódigoDeBarras 
	    {
		    get
		    {
			    return GetColumnValue<string>("Código de Barras");
		    }
            set 
		    {
			    SetColumnValue("Código de Barras", value);
            }
        }
	      
        [XmlAttribute("TipoDeEmpaque")]
        [Bindable(true)]
        public string TipoDeEmpaque 
	    {
		    get
		    {
			    return GetColumnValue<string>("Tipo de Empaque");
		    }
            set 
		    {
			    SetColumnValue("Tipo de Empaque", value);
            }
        }
	      
        [XmlAttribute("UnidadesPorEmpaque")]
        [Bindable(true)]
        public string UnidadesPorEmpaque 
	    {
		    get
		    {
			    return GetColumnValue<string>("Unidades Por Empaque");
		    }
            set 
		    {
			    SetColumnValue("Unidades Por Empaque", value);
            }
        }
	      
        [XmlAttribute("CostoBase")]
        [Bindable(true)]
        public decimal CostoBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Costo Base");
		    }
            set 
		    {
			    SetColumnValue("Costo Base", value);
            }
        }
	      
        [XmlAttribute("Largo")]
        [Bindable(true)]
        public decimal Largo 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("LARGO");
		    }
            set 
		    {
			    SetColumnValue("LARGO", value);
            }
        }
	      
        [XmlAttribute("Ancho")]
        [Bindable(true)]
        public decimal Ancho 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("ANCHO");
		    }
            set 
		    {
			    SetColumnValue("ANCHO", value);
            }
        }
	      
        [XmlAttribute("Alto")]
        [Bindable(true)]
        public decimal Alto 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("ALTO");
		    }
            set 
		    {
			    SetColumnValue("ALTO", value);
            }
        }
	      
        [XmlAttribute("Peso")]
        [Bindable(true)]
        public decimal Peso 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("PESO");
		    }
            set 
		    {
			    SetColumnValue("PESO", value);
            }
        }
	      
        [XmlAttribute("IdProveedor")]
        [Bindable(true)]
        public int IdProveedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("ID_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("NombreDelProveedor")]
        [Bindable(true)]
        public string NombreDelProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nombre del Proveedor");
		    }
            set 
		    {
			    SetColumnValue("Nombre del Proveedor", value);
            }
        }
	      
        [XmlAttribute("IdTipoProducto")]
        [Bindable(true)]
        public int IdTipoProducto 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_TIPO_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("ID_TIPO_PRODUCTO", value);
            }
        }
	      
        [XmlAttribute("TipoDeProducto")]
        [Bindable(true)]
        public string TipoDeProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("Tipo de Producto");
		    }
            set 
		    {
			    SetColumnValue("Tipo de Producto", value);
            }
        }
	      
        [XmlAttribute("Iva")]
        [Bindable(true)]
        public decimal Iva 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("IVA");
		    }
            set 
		    {
			    SetColumnValue("IVA", value);
            }
        }
	      
        [XmlAttribute("RetenciónEnLaFuente")]
        [Bindable(true)]
        public decimal RetenciónEnLaFuente 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Retención en la fuente");
		    }
            set 
		    {
			    SetColumnValue("Retención en la fuente", value);
            }
        }
	      
        [XmlAttribute("Descuento")]
        [Bindable(true)]
        public decimal? Descuento 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("DESCUENTO");
		    }
            set 
		    {
			    SetColumnValue("DESCUENTO", value);
            }
        }
	      
        [XmlAttribute("Programa")]
        [Bindable(true)]
        public string Programa 
	    {
		    get
		    {
			    return GetColumnValue<string>("Programa");
		    }
            set 
		    {
			    SetColumnValue("Programa", value);
            }
        }
	      
        [XmlAttribute("PrecioDeVentaBase")]
        [Bindable(true)]
        public decimal? PrecioDeVentaBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("Precio de Venta Base");
		    }
            set 
		    {
			    SetColumnValue("Precio de Venta Base", value);
            }
        }
	      
        [XmlAttribute("CostoDeEnvíoBase")]
        [Bindable(true)]
        public decimal? CostoDeEnvíoBase 
	    {
		    get
		    {
			    return GetColumnValue<decimal?>("Costo de Envío Base");
		    }
            set 
		    {
			    SetColumnValue("Costo de Envío Base", value);
            }
        }
	      
        [XmlAttribute("Establecimiento")]
        [Bindable(true)]
        public string Establecimiento 
	    {
		    get
		    {
			    return GetColumnValue<string>("Establecimiento");
		    }
            set 
		    {
			    SetColumnValue("Establecimiento", value);
            }
        }
	      
        [XmlAttribute("TipoDeDelivery")]
        [Bindable(true)]
        public string TipoDeDelivery 
	    {
		    get
		    {
			    return GetColumnValue<string>("Tipo de Delivery");
		    }
            set 
		    {
			    SetColumnValue("Tipo de Delivery", value);
            }
        }
	      
        [XmlAttribute("IdMarca")]
        [Bindable(true)]
        public int? IdMarca 
	    {
		    get
		    {
			    return GetColumnValue<int?>("ID_MARCA");
		    }
            set 
		    {
			    SetColumnValue("ID_MARCA", value);
            }
        }
	      
        [XmlAttribute("ValorAsegurado")]
        [Bindable(true)]
        public decimal ValorAsegurado 
	    {
		    get
		    {
			    return GetColumnValue<decimal>("Valor Asegurado");
		    }
            set 
		    {
			    SetColumnValue("Valor Asegurado", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Guid = @"GUID";
            
            public static string Codigo = @"CODIGO";
            
            public static string NombreDelProducto = @"Nombre del Producto";
            
            public static string DescripciónDelProducto = @"Descripción del Producto";
            
            public static string Localización = @"Localización";
            
            public static string CódigoDeBarras = @"Código de Barras";
            
            public static string TipoDeEmpaque = @"Tipo de Empaque";
            
            public static string UnidadesPorEmpaque = @"Unidades Por Empaque";
            
            public static string CostoBase = @"Costo Base";
            
            public static string Largo = @"LARGO";
            
            public static string Ancho = @"ANCHO";
            
            public static string Alto = @"ALTO";
            
            public static string Peso = @"PESO";
            
            public static string IdProveedor = @"ID_PROVEEDOR";
            
            public static string NombreDelProveedor = @"Nombre del Proveedor";
            
            public static string IdTipoProducto = @"ID_TIPO_PRODUCTO";
            
            public static string TipoDeProducto = @"Tipo de Producto";
            
            public static string Iva = @"IVA";
            
            public static string RetenciónEnLaFuente = @"Retención en la fuente";
            
            public static string Descuento = @"DESCUENTO";
            
            public static string Programa = @"Programa";
            
            public static string PrecioDeVentaBase = @"Precio de Venta Base";
            
            public static string CostoDeEnvíoBase = @"Costo de Envío Base";
            
            public static string Establecimiento = @"Establecimiento";
            
            public static string TipoDeDelivery = @"Tipo de Delivery";
            
            public static string IdMarca = @"ID_MARCA";
            
            public static string ValorAsegurado = @"Valor Asegurado";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
