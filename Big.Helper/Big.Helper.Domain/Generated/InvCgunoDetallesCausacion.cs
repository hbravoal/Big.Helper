using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvCgunoDetallesCausacion class.
	/// </summary>
    [Serializable]
	public partial class InvCgunoDetallesCausacionCollection : ActiveList<InvCgunoDetallesCausacion, InvCgunoDetallesCausacionCollection>
	{	   
		public InvCgunoDetallesCausacionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvCgunoDetallesCausacionCollection</returns>
		public InvCgunoDetallesCausacionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvCgunoDetallesCausacion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_DETALLES_CAUSACION table.
	/// </summary>
	[Serializable]
	public partial class InvCgunoDetallesCausacion : ActiveRecord<InvCgunoDetallesCausacion>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvCgunoDetallesCausacion()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvCgunoDetallesCausacion(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvCgunoDetallesCausacion(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvCgunoDetallesCausacion(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_DETALLES_CAUSACION", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.AnsiString;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Decimal;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIdCuenta = new TableSchema.TableColumn(schema);
				colvarIdCuenta.ColumnName = "ID_CUENTA";
				colvarIdCuenta.DataType = DbType.Int32;
				colvarIdCuenta.MaxLength = 0;
				colvarIdCuenta.AutoIncrement = false;
				colvarIdCuenta.IsNullable = false;
				colvarIdCuenta.IsPrimaryKey = false;
				colvarIdCuenta.IsForeignKey = true;
				colvarIdCuenta.IsReadOnly = false;
				colvarIdCuenta.DefaultSetting = @"";
				
					colvarIdCuenta.ForeignKeyTableName = "INV_CGUNO_CUENTAS";
				schema.Columns.Add(colvarIdCuenta);
				
				TableSchema.TableColumn colvarIdCausacion = new TableSchema.TableColumn(schema);
				colvarIdCausacion.ColumnName = "ID_CAUSACION";
				colvarIdCausacion.DataType = DbType.Decimal;
				colvarIdCausacion.MaxLength = 0;
				colvarIdCausacion.AutoIncrement = false;
				colvarIdCausacion.IsNullable = false;
				colvarIdCausacion.IsPrimaryKey = false;
				colvarIdCausacion.IsForeignKey = true;
				colvarIdCausacion.IsReadOnly = false;
				colvarIdCausacion.DefaultSetting = @"";
				
					colvarIdCausacion.ForeignKeyTableName = "INV_CGUNO_CAUSACIONES";
				schema.Columns.Add(colvarIdCausacion);
				
				TableSchema.TableColumn colvarIdNaturaleza = new TableSchema.TableColumn(schema);
				colvarIdNaturaleza.ColumnName = "ID_NATURALEZA";
				colvarIdNaturaleza.DataType = DbType.Int32;
				colvarIdNaturaleza.MaxLength = 0;
				colvarIdNaturaleza.AutoIncrement = false;
				colvarIdNaturaleza.IsNullable = false;
				colvarIdNaturaleza.IsPrimaryKey = false;
				colvarIdNaturaleza.IsForeignKey = true;
				colvarIdNaturaleza.IsReadOnly = false;
				colvarIdNaturaleza.DefaultSetting = @"";
				
					colvarIdNaturaleza.ForeignKeyTableName = "INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION";
				schema.Columns.Add(colvarIdNaturaleza);
				
				TableSchema.TableColumn colvarIdCampoTipoCausacion = new TableSchema.TableColumn(schema);
				colvarIdCampoTipoCausacion.ColumnName = "ID_CAMPO_TIPO_CAUSACION";
				colvarIdCampoTipoCausacion.DataType = DbType.Int32;
				colvarIdCampoTipoCausacion.MaxLength = 0;
				colvarIdCampoTipoCausacion.AutoIncrement = false;
				colvarIdCampoTipoCausacion.IsNullable = false;
				colvarIdCampoTipoCausacion.IsPrimaryKey = false;
				colvarIdCampoTipoCausacion.IsForeignKey = true;
				colvarIdCampoTipoCausacion.IsReadOnly = false;
				colvarIdCampoTipoCausacion.DefaultSetting = @"";
				
					colvarIdCampoTipoCausacion.ForeignKeyTableName = "INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION";
				schema.Columns.Add(colvarIdCampoTipoCausacion);
				
				TableSchema.TableColumn colvarFechaCreacion = new TableSchema.TableColumn(schema);
				colvarFechaCreacion.ColumnName = "FECHA_CREACION";
				colvarFechaCreacion.DataType = DbType.DateTime;
				colvarFechaCreacion.MaxLength = 0;
				colvarFechaCreacion.AutoIncrement = false;
				colvarFechaCreacion.IsNullable = false;
				colvarFechaCreacion.IsPrimaryKey = false;
				colvarFechaCreacion.IsForeignKey = false;
				colvarFechaCreacion.IsReadOnly = false;
				colvarFechaCreacion.DefaultSetting = @"";
				colvarFechaCreacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaCreacion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_CGUNO_DETALLES_CAUSACION",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public decimal Id 
		{
			get { return GetColumnValue<decimal>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IdCuenta")]
		[Bindable(true)]
		public int IdCuenta 
		{
			get { return GetColumnValue<int>(Columns.IdCuenta); }
			set { SetColumnValue(Columns.IdCuenta, value); }
		}
		  
		[XmlAttribute("IdCausacion")]
		[Bindable(true)]
		public decimal IdCausacion 
		{
			get { return GetColumnValue<decimal>(Columns.IdCausacion); }
			set { SetColumnValue(Columns.IdCausacion, value); }
		}
		  
		[XmlAttribute("IdNaturaleza")]
		[Bindable(true)]
		public int IdNaturaleza 
		{
			get { return GetColumnValue<int>(Columns.IdNaturaleza); }
			set { SetColumnValue(Columns.IdNaturaleza, value); }
		}
		  
		[XmlAttribute("IdCampoTipoCausacion")]
		[Bindable(true)]
		public int IdCampoTipoCausacion 
		{
			get { return GetColumnValue<int>(Columns.IdCampoTipoCausacion); }
			set { SetColumnValue(Columns.IdCampoTipoCausacion, value); }
		}
		  
		[XmlAttribute("FechaCreacion")]
		[Bindable(true)]
		public DateTime FechaCreacion 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaCreacion); }
			set { SetColumnValue(Columns.FechaCreacion, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvCgunoCausacione ActiveRecord object related to this InvCgunoDetallesCausacion
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCausacione InvCgunoCausacione
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCausacione.FetchByID(this.IdCausacion); }
			set { SetColumnValue("ID_CAUSACION", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoCuenta ActiveRecord object related to this InvCgunoDetallesCausacion
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoCuenta InvCgunoCuenta
		{
			get { return Big.Helper.Domain.Generated.InvCgunoCuenta.FetchByID(this.IdCuenta); }
			set { SetColumnValue("ID_CUENTA", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoMasCamposTipoCausacion ActiveRecord object related to this InvCgunoDetallesCausacion
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoMasCamposTipoCausacion InvCgunoMasCamposTipoCausacion
		{
			get { return Big.Helper.Domain.Generated.InvCgunoMasCamposTipoCausacion.FetchByID(this.IdCampoTipoCausacion); }
			set { SetColumnValue("ID_CAMPO_TIPO_CAUSACION", value.Id); }
		}
		
		
		/// <summary>
		/// Returns a InvCgunoMasNaturalezasDetalleCausacion ActiveRecord object related to this InvCgunoDetallesCausacion
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvCgunoMasNaturalezasDetalleCausacion InvCgunoMasNaturalezasDetalleCausacion
		{
			get { return Big.Helper.Domain.Generated.InvCgunoMasNaturalezasDetalleCausacion.FetchByID(this.IdNaturaleza); }
			set { SetColumnValue("ID_NATURALEZA", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,int varIdCuenta,decimal varIdCausacion,int varIdNaturaleza,int varIdCampoTipoCausacion,DateTime varFechaCreacion)
		{
			InvCgunoDetallesCausacion item = new InvCgunoDetallesCausacion();
			
			item.Guid = varGuid;
			
			item.IdCuenta = varIdCuenta;
			
			item.IdCausacion = varIdCausacion;
			
			item.IdNaturaleza = varIdNaturaleza;
			
			item.IdCampoTipoCausacion = varIdCampoTipoCausacion;
			
			item.FechaCreacion = varFechaCreacion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,decimal varId,int varIdCuenta,decimal varIdCausacion,int varIdNaturaleza,int varIdCampoTipoCausacion,DateTime varFechaCreacion)
		{
			InvCgunoDetallesCausacion item = new InvCgunoDetallesCausacion();
			
				item.Guid = varGuid;
			
				item.Id = varId;
			
				item.IdCuenta = varIdCuenta;
			
				item.IdCausacion = varIdCausacion;
			
				item.IdNaturaleza = varIdNaturaleza;
			
				item.IdCampoTipoCausacion = varIdCampoTipoCausacion;
			
				item.FechaCreacion = varFechaCreacion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCuentaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCausacionColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdNaturalezaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCampoTipoCausacionColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaCreacionColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string Id = @"ID";
			 public static string IdCuenta = @"ID_CUENTA";
			 public static string IdCausacion = @"ID_CAUSACION";
			 public static string IdNaturaleza = @"ID_NATURALEZA";
			 public static string IdCampoTipoCausacion = @"ID_CAMPO_TIPO_CAUSACION";
			 public static string FechaCreacion = @"FECHA_CREACION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
