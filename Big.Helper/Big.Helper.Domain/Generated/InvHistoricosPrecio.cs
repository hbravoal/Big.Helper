using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvHistoricosPrecio class.
	/// </summary>
    [Serializable]
	public partial class InvHistoricosPrecioCollection : ActiveList<InvHistoricosPrecio, InvHistoricosPrecioCollection>
	{	   
		public InvHistoricosPrecioCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvHistoricosPrecioCollection</returns>
		public InvHistoricosPrecioCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvHistoricosPrecio o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_HISTORICOS_PRECIO table.
	/// </summary>
	[Serializable]
	public partial class InvHistoricosPrecio : ActiveRecord<InvHistoricosPrecio>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvHistoricosPrecio()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvHistoricosPrecio(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvHistoricosPrecio(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvHistoricosPrecio(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_HISTORICOS_PRECIO", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdHistorialProducto = new TableSchema.TableColumn(schema);
				colvarIdHistorialProducto.ColumnName = "ID_HISTORIAL_PRODUCTO";
				colvarIdHistorialProducto.DataType = DbType.Decimal;
				colvarIdHistorialProducto.MaxLength = 0;
				colvarIdHistorialProducto.AutoIncrement = true;
				colvarIdHistorialProducto.IsNullable = false;
				colvarIdHistorialProducto.IsPrimaryKey = true;
				colvarIdHistorialProducto.IsForeignKey = false;
				colvarIdHistorialProducto.IsReadOnly = false;
				colvarIdHistorialProducto.DefaultSetting = @"";
				colvarIdHistorialProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdHistorialProducto);
				
				TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
				colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
				colvarGuidProducto.DataType = DbType.String;
				colvarGuidProducto.MaxLength = 36;
				colvarGuidProducto.AutoIncrement = false;
				colvarGuidProducto.IsNullable = true;
				colvarGuidProducto.IsPrimaryKey = false;
				colvarGuidProducto.IsForeignKey = true;
				colvarGuidProducto.IsReadOnly = false;
				colvarGuidProducto.DefaultSetting = @"";
				
					colvarGuidProducto.ForeignKeyTableName = "INV_PRODUCTOS";
				schema.Columns.Add(colvarGuidProducto);
				
				TableSchema.TableColumn colvarGuidPrecioProducto = new TableSchema.TableColumn(schema);
				colvarGuidPrecioProducto.ColumnName = "GUID_PRECIO_PRODUCTO";
				colvarGuidPrecioProducto.DataType = DbType.String;
				colvarGuidPrecioProducto.MaxLength = 36;
				colvarGuidPrecioProducto.AutoIncrement = false;
				colvarGuidPrecioProducto.IsNullable = true;
				colvarGuidPrecioProducto.IsPrimaryKey = false;
				colvarGuidPrecioProducto.IsForeignKey = true;
				colvarGuidPrecioProducto.IsReadOnly = false;
				colvarGuidPrecioProducto.DefaultSetting = @"";
				
					colvarGuidPrecioProducto.ForeignKeyTableName = "INV_PRECIOS_BASE_PROGRAMA";
				schema.Columns.Add(colvarGuidPrecioProducto);
				
				TableSchema.TableColumn colvarCostoBase = new TableSchema.TableColumn(schema);
				colvarCostoBase.ColumnName = "COSTO_BASE";
				colvarCostoBase.DataType = DbType.Decimal;
				colvarCostoBase.MaxLength = 0;
				colvarCostoBase.AutoIncrement = false;
				colvarCostoBase.IsNullable = true;
				colvarCostoBase.IsPrimaryKey = false;
				colvarCostoBase.IsForeignKey = false;
				colvarCostoBase.IsReadOnly = false;
				colvarCostoBase.DefaultSetting = @"";
				colvarCostoBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoBase);
				
				TableSchema.TableColumn colvarValorAsegurado = new TableSchema.TableColumn(schema);
				colvarValorAsegurado.ColumnName = "VALOR_ASEGURADO";
				colvarValorAsegurado.DataType = DbType.Decimal;
				colvarValorAsegurado.MaxLength = 0;
				colvarValorAsegurado.AutoIncrement = false;
				colvarValorAsegurado.IsNullable = true;
				colvarValorAsegurado.IsPrimaryKey = false;
				colvarValorAsegurado.IsForeignKey = false;
				colvarValorAsegurado.IsReadOnly = false;
				colvarValorAsegurado.DefaultSetting = @"";
				colvarValorAsegurado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorAsegurado);
				
				TableSchema.TableColumn colvarPrecioBase = new TableSchema.TableColumn(schema);
				colvarPrecioBase.ColumnName = "PRECIO_BASE";
				colvarPrecioBase.DataType = DbType.Decimal;
				colvarPrecioBase.MaxLength = 0;
				colvarPrecioBase.AutoIncrement = false;
				colvarPrecioBase.IsNullable = true;
				colvarPrecioBase.IsPrimaryKey = false;
				colvarPrecioBase.IsForeignKey = false;
				colvarPrecioBase.IsReadOnly = false;
				colvarPrecioBase.DefaultSetting = @"";
				colvarPrecioBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrecioBase);
				
				TableSchema.TableColumn colvarCostoEnvioBase = new TableSchema.TableColumn(schema);
				colvarCostoEnvioBase.ColumnName = "COSTO_ENVIO_BASE";
				colvarCostoEnvioBase.DataType = DbType.Decimal;
				colvarCostoEnvioBase.MaxLength = 0;
				colvarCostoEnvioBase.AutoIncrement = false;
				colvarCostoEnvioBase.IsNullable = true;
				colvarCostoEnvioBase.IsPrimaryKey = false;
				colvarCostoEnvioBase.IsForeignKey = false;
				colvarCostoEnvioBase.IsReadOnly = false;
				colvarCostoEnvioBase.DefaultSetting = @"";
				colvarCostoEnvioBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoEnvioBase);
				
				TableSchema.TableColumn colvarIdMarca = new TableSchema.TableColumn(schema);
				colvarIdMarca.ColumnName = "ID_MARCA";
				colvarIdMarca.DataType = DbType.Int32;
				colvarIdMarca.MaxLength = 0;
				colvarIdMarca.AutoIncrement = false;
				colvarIdMarca.IsNullable = true;
				colvarIdMarca.IsPrimaryKey = false;
				colvarIdMarca.IsForeignKey = true;
				colvarIdMarca.IsReadOnly = false;
				colvarIdMarca.DefaultSetting = @"";
				
					colvarIdMarca.ForeignKeyTableName = "INV_MARCAS";
				schema.Columns.Add(colvarIdMarca);
				
				TableSchema.TableColumn colvarIdTipoDelivery = new TableSchema.TableColumn(schema);
				colvarIdTipoDelivery.ColumnName = "ID_TIPO_DELIVERY";
				colvarIdTipoDelivery.DataType = DbType.Int32;
				colvarIdTipoDelivery.MaxLength = 0;
				colvarIdTipoDelivery.AutoIncrement = false;
				colvarIdTipoDelivery.IsNullable = true;
				colvarIdTipoDelivery.IsPrimaryKey = false;
				colvarIdTipoDelivery.IsForeignKey = true;
				colvarIdTipoDelivery.IsReadOnly = false;
				colvarIdTipoDelivery.DefaultSetting = @"";
				
					colvarIdTipoDelivery.ForeignKeyTableName = "INV_MAS_TIPOS_DELIVERY";
				schema.Columns.Add(colvarIdTipoDelivery);
				
				TableSchema.TableColumn colvarFechaCambio = new TableSchema.TableColumn(schema);
				colvarFechaCambio.ColumnName = "FECHA_CAMBIO";
				colvarFechaCambio.DataType = DbType.DateTime;
				colvarFechaCambio.MaxLength = 0;
				colvarFechaCambio.AutoIncrement = false;
				colvarFechaCambio.IsNullable = false;
				colvarFechaCambio.IsPrimaryKey = false;
				colvarFechaCambio.IsForeignKey = false;
				colvarFechaCambio.IsReadOnly = false;
				colvarFechaCambio.DefaultSetting = @"";
				colvarFechaCambio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaCambio);
				
				TableSchema.TableColumn colvarIdUsuarioCambio = new TableSchema.TableColumn(schema);
				colvarIdUsuarioCambio.ColumnName = "ID_USUARIO_CAMBIO";
				colvarIdUsuarioCambio.DataType = DbType.Int32;
				colvarIdUsuarioCambio.MaxLength = 0;
				colvarIdUsuarioCambio.AutoIncrement = false;
				colvarIdUsuarioCambio.IsNullable = false;
				colvarIdUsuarioCambio.IsPrimaryKey = false;
				colvarIdUsuarioCambio.IsForeignKey = true;
				colvarIdUsuarioCambio.IsReadOnly = false;
				colvarIdUsuarioCambio.DefaultSetting = @"";
				
					colvarIdUsuarioCambio.ForeignKeyTableName = "INV_USUARIOS";
				schema.Columns.Add(colvarIdUsuarioCambio);
				
				TableSchema.TableColumn colvarActivo = new TableSchema.TableColumn(schema);
				colvarActivo.ColumnName = "ACTIVO";
				colvarActivo.DataType = DbType.Boolean;
				colvarActivo.MaxLength = 0;
				colvarActivo.AutoIncrement = false;
				colvarActivo.IsNullable = true;
				colvarActivo.IsPrimaryKey = false;
				colvarActivo.IsForeignKey = false;
				colvarActivo.IsReadOnly = false;
				
						colvarActivo.DefaultSetting = @"((1))";
				colvarActivo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_HISTORICOS_PRECIO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdHistorialProducto")]
		[Bindable(true)]
		public decimal IdHistorialProducto 
		{
			get { return GetColumnValue<decimal>(Columns.IdHistorialProducto); }
			set { SetColumnValue(Columns.IdHistorialProducto, value); }
		}
		  
		[XmlAttribute("GuidProducto")]
		[Bindable(true)]
		public string GuidProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidProducto); }
			set { SetColumnValue(Columns.GuidProducto, value); }
		}
		  
		[XmlAttribute("GuidPrecioProducto")]
		[Bindable(true)]
		public string GuidPrecioProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidPrecioProducto); }
			set { SetColumnValue(Columns.GuidPrecioProducto, value); }
		}
		  
		[XmlAttribute("CostoBase")]
		[Bindable(true)]
		public decimal? CostoBase 
		{
			get { return GetColumnValue<decimal?>(Columns.CostoBase); }
			set { SetColumnValue(Columns.CostoBase, value); }
		}
		  
		[XmlAttribute("ValorAsegurado")]
		[Bindable(true)]
		public decimal? ValorAsegurado 
		{
			get { return GetColumnValue<decimal?>(Columns.ValorAsegurado); }
			set { SetColumnValue(Columns.ValorAsegurado, value); }
		}
		  
		[XmlAttribute("PrecioBase")]
		[Bindable(true)]
		public decimal? PrecioBase 
		{
			get { return GetColumnValue<decimal?>(Columns.PrecioBase); }
			set { SetColumnValue(Columns.PrecioBase, value); }
		}
		  
		[XmlAttribute("CostoEnvioBase")]
		[Bindable(true)]
		public decimal? CostoEnvioBase 
		{
			get { return GetColumnValue<decimal?>(Columns.CostoEnvioBase); }
			set { SetColumnValue(Columns.CostoEnvioBase, value); }
		}
		  
		[XmlAttribute("IdMarca")]
		[Bindable(true)]
		public int? IdMarca 
		{
			get { return GetColumnValue<int?>(Columns.IdMarca); }
			set { SetColumnValue(Columns.IdMarca, value); }
		}
		  
		[XmlAttribute("IdTipoDelivery")]
		[Bindable(true)]
		public int? IdTipoDelivery 
		{
			get { return GetColumnValue<int?>(Columns.IdTipoDelivery); }
			set { SetColumnValue(Columns.IdTipoDelivery, value); }
		}
		  
		[XmlAttribute("FechaCambio")]
		[Bindable(true)]
		public DateTime FechaCambio 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaCambio); }
			set { SetColumnValue(Columns.FechaCambio, value); }
		}
		  
		[XmlAttribute("IdUsuarioCambio")]
		[Bindable(true)]
		public int IdUsuarioCambio 
		{
			get { return GetColumnValue<int>(Columns.IdUsuarioCambio); }
			set { SetColumnValue(Columns.IdUsuarioCambio, value); }
		}
		  
		[XmlAttribute("Activo")]
		[Bindable(true)]
		public bool? Activo 
		{
			get { return GetColumnValue<bool?>(Columns.Activo); }
			set { SetColumnValue(Columns.Activo, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMarca ActiveRecord object related to this InvHistoricosPrecio
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMarca InvMarca
		{
			get { return Big.Helper.Domain.Generated.InvMarca.FetchByID(this.IdMarca); }
			set { SetColumnValue("ID_MARCA", value.IdMarca); }
		}
		
		
		/// <summary>
		/// Returns a InvMasTiposDelivery ActiveRecord object related to this InvHistoricosPrecio
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasTiposDelivery InvMasTiposDelivery
		{
			get { return Big.Helper.Domain.Generated.InvMasTiposDelivery.FetchByID(this.IdTipoDelivery); }
			set { SetColumnValue("ID_TIPO_DELIVERY", value.IdTipoDelivery); }
		}
		
		
		/// <summary>
		/// Returns a InvPreciosBasePrograma ActiveRecord object related to this InvHistoricosPrecio
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvPreciosBasePrograma InvPreciosBasePrograma
		{
			get { return Big.Helper.Domain.Generated.InvPreciosBasePrograma.FetchByID(this.GuidPrecioProducto); }
			set { SetColumnValue("GUID_PRECIO_PRODUCTO", value.GuidPrecioProducto); }
		}
		
		
		/// <summary>
		/// Returns a InvProducto ActiveRecord object related to this InvHistoricosPrecio
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvProducto InvProducto
		{
			get { return Big.Helper.Domain.Generated.InvProducto.FetchByID(this.GuidProducto); }
			set { SetColumnValue("GUID_PRODUCTO", value.Guid); }
		}
		
		
		/// <summary>
		/// Returns a InvUsuario ActiveRecord object related to this InvHistoricosPrecio
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvUsuario InvUsuario
		{
			get { return Big.Helper.Domain.Generated.InvUsuario.FetchByID(this.IdUsuarioCambio); }
			set { SetColumnValue("ID_USUARIO_CAMBIO", value.IdUsuario); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidProducto,string varGuidPrecioProducto,decimal? varCostoBase,decimal? varValorAsegurado,decimal? varPrecioBase,decimal? varCostoEnvioBase,int? varIdMarca,int? varIdTipoDelivery,DateTime varFechaCambio,int varIdUsuarioCambio,bool? varActivo)
		{
			InvHistoricosPrecio item = new InvHistoricosPrecio();
			
			item.GuidProducto = varGuidProducto;
			
			item.GuidPrecioProducto = varGuidPrecioProducto;
			
			item.CostoBase = varCostoBase;
			
			item.ValorAsegurado = varValorAsegurado;
			
			item.PrecioBase = varPrecioBase;
			
			item.CostoEnvioBase = varCostoEnvioBase;
			
			item.IdMarca = varIdMarca;
			
			item.IdTipoDelivery = varIdTipoDelivery;
			
			item.FechaCambio = varFechaCambio;
			
			item.IdUsuarioCambio = varIdUsuarioCambio;
			
			item.Activo = varActivo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(decimal varIdHistorialProducto,string varGuidProducto,string varGuidPrecioProducto,decimal? varCostoBase,decimal? varValorAsegurado,decimal? varPrecioBase,decimal? varCostoEnvioBase,int? varIdMarca,int? varIdTipoDelivery,DateTime varFechaCambio,int varIdUsuarioCambio,bool? varActivo)
		{
			InvHistoricosPrecio item = new InvHistoricosPrecio();
			
				item.IdHistorialProducto = varIdHistorialProducto;
			
				item.GuidProducto = varGuidProducto;
			
				item.GuidPrecioProducto = varGuidPrecioProducto;
			
				item.CostoBase = varCostoBase;
			
				item.ValorAsegurado = varValorAsegurado;
			
				item.PrecioBase = varPrecioBase;
			
				item.CostoEnvioBase = varCostoEnvioBase;
			
				item.IdMarca = varIdMarca;
			
				item.IdTipoDelivery = varIdTipoDelivery;
			
				item.FechaCambio = varFechaCambio;
			
				item.IdUsuarioCambio = varIdUsuarioCambio;
			
				item.Activo = varActivo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdHistorialProductoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidProductoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidPrecioProductoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoBaseColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorAseguradoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PrecioBaseColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoEnvioBaseColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdMarcaColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoDeliveryColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaCambioColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdUsuarioCambioColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdHistorialProducto = @"ID_HISTORIAL_PRODUCTO";
			 public static string GuidProducto = @"GUID_PRODUCTO";
			 public static string GuidPrecioProducto = @"GUID_PRECIO_PRODUCTO";
			 public static string CostoBase = @"COSTO_BASE";
			 public static string ValorAsegurado = @"VALOR_ASEGURADO";
			 public static string PrecioBase = @"PRECIO_BASE";
			 public static string CostoEnvioBase = @"COSTO_ENVIO_BASE";
			 public static string IdMarca = @"ID_MARCA";
			 public static string IdTipoDelivery = @"ID_TIPO_DELIVERY";
			 public static string FechaCambio = @"FECHA_CAMBIO";
			 public static string IdUsuarioCambio = @"ID_USUARIO_CAMBIO";
			 public static string Activo = @"ACTIVO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
