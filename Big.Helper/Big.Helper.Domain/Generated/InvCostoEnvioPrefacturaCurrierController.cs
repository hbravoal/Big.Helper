using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_COSTO_ENVIO_PREFACTURA_CURRIER
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCostoEnvioPrefacturaCurrierController
    {
        // Preload our schema..
        InvCostoEnvioPrefacturaCurrier thisSchemaLoad = new InvCostoEnvioPrefacturaCurrier();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCostoEnvioPrefacturaCurrierCollection FetchAll()
        {
            InvCostoEnvioPrefacturaCurrierCollection coll = new InvCostoEnvioPrefacturaCurrierCollection();
            Query qry = new Query(InvCostoEnvioPrefacturaCurrier.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCostoEnvioPrefacturaCurrierCollection FetchByID(object IdPeriodo)
        {
            InvCostoEnvioPrefacturaCurrierCollection coll = new InvCostoEnvioPrefacturaCurrierCollection().Where("ID_PERIODO", IdPeriodo).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCostoEnvioPrefacturaCurrierCollection FetchByQuery(Query qry)
        {
            InvCostoEnvioPrefacturaCurrierCollection coll = new InvCostoEnvioPrefacturaCurrierCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdPeriodo)
        {
            return (InvCostoEnvioPrefacturaCurrier.Delete(IdPeriodo) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdPeriodo)
        {
            return (InvCostoEnvioPrefacturaCurrier.Destroy(IdPeriodo) == 1);
        }
        
        
        
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(int IdPeriodo,string GuidDetalleFactura)
        {
            Query qry = new Query(InvCostoEnvioPrefacturaCurrier.Schema);
            qry.QueryType = QueryType.Delete;
            qry.AddWhere("IdPeriodo", IdPeriodo).AND("GuidDetalleFactura", GuidDetalleFactura);
            qry.Execute();
            return (true);
        }        
       
    	
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdPeriodo,string GuidDetalleFactura,decimal CostoEnvio)
	    {
		    InvCostoEnvioPrefacturaCurrier item = new InvCostoEnvioPrefacturaCurrier();
		    
            item.IdPeriodo = IdPeriodo;
            
            item.GuidDetalleFactura = GuidDetalleFactura;
            
            item.CostoEnvio = CostoEnvio;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdPeriodo,string GuidDetalleFactura,decimal CostoEnvio)
	    {
		    InvCostoEnvioPrefacturaCurrier item = new InvCostoEnvioPrefacturaCurrier();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdPeriodo = IdPeriodo;
				
			item.GuidDetalleFactura = GuidDetalleFactura;
				
			item.CostoEnvio = CostoEnvio;
				
	        item.Save(UserName);
	    }
    }
}
