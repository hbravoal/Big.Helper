using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_CIUDADES
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasCiudadeController
    {
        // Preload our schema..
        InvMasCiudade thisSchemaLoad = new InvMasCiudade();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasCiudadeCollection FetchAll()
        {
            InvMasCiudadeCollection coll = new InvMasCiudadeCollection();
            Query qry = new Query(InvMasCiudade.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasCiudadeCollection FetchByID(object IdCiudad)
        {
            InvMasCiudadeCollection coll = new InvMasCiudadeCollection().Where("ID_CIUDAD", IdCiudad).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasCiudadeCollection FetchByQuery(Query qry)
        {
            InvMasCiudadeCollection coll = new InvMasCiudadeCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdCiudad)
        {
            return (InvMasCiudade.Delete(IdCiudad) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdCiudad)
        {
            return (InvMasCiudade.Destroy(IdCiudad) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdCiudad,string Nombre,int IdDepartamento)
	    {
		    InvMasCiudade item = new InvMasCiudade();
		    
            item.IdCiudad = IdCiudad;
            
            item.Nombre = Nombre;
            
            item.IdDepartamento = IdDepartamento;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdCiudad,string Nombre,int IdDepartamento)
	    {
		    InvMasCiudade item = new InvMasCiudade();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdCiudad = IdCiudad;
				
			item.Nombre = Nombre;
				
			item.IdDepartamento = IdDepartamento;
				
	        item.Save(UserName);
	    }
    }
}
