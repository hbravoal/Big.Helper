using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_EMPRESAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvEmpresaController
    {
        // Preload our schema..
        InvEmpresa thisSchemaLoad = new InvEmpresa();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvEmpresaCollection FetchAll()
        {
            InvEmpresaCollection coll = new InvEmpresaCollection();
            Query qry = new Query(InvEmpresa.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvEmpresaCollection FetchByID(object IdEmpresa)
        {
            InvEmpresaCollection coll = new InvEmpresaCollection().Where("ID_EMPRESA", IdEmpresa).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvEmpresaCollection FetchByQuery(Query qry)
        {
            InvEmpresaCollection coll = new InvEmpresaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdEmpresa)
        {
            return (InvEmpresa.Delete(IdEmpresa) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdEmpresa)
        {
            return (InvEmpresa.Destroy(IdEmpresa) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string CodigoEmpresa,string RazonSocial,string Nit,string Direccion,string Telefono,string Fax,int IdCiudad)
	    {
		    InvEmpresa item = new InvEmpresa();
		    
            item.CodigoEmpresa = CodigoEmpresa;
            
            item.RazonSocial = RazonSocial;
            
            item.Nit = Nit;
            
            item.Direccion = Direccion;
            
            item.Telefono = Telefono;
            
            item.Fax = Fax;
            
            item.IdCiudad = IdCiudad;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdEmpresa,string CodigoEmpresa,string RazonSocial,string Nit,string Direccion,string Telefono,string Fax,int IdCiudad)
	    {
		    InvEmpresa item = new InvEmpresa();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdEmpresa = IdEmpresa;
				
			item.CodigoEmpresa = CodigoEmpresa;
				
			item.RazonSocial = RazonSocial;
				
			item.Nit = Nit;
				
			item.Direccion = Direccion;
				
			item.Telefono = Telefono;
				
			item.Fax = Fax;
				
			item.IdCiudad = IdCiudad;
				
	        item.Save(UserName);
	    }
    }
}
