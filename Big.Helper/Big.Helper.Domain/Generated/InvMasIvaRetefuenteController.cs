using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_MAS_IVA_RETEFUENTE
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvMasIvaRetefuenteController
    {
        // Preload our schema..
        InvMasIvaRetefuente thisSchemaLoad = new InvMasIvaRetefuente();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvMasIvaRetefuenteCollection FetchAll()
        {
            InvMasIvaRetefuenteCollection coll = new InvMasIvaRetefuenteCollection();
            Query qry = new Query(InvMasIvaRetefuente.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasIvaRetefuenteCollection FetchByID(object IdIvaRetefuente)
        {
            InvMasIvaRetefuenteCollection coll = new InvMasIvaRetefuenteCollection().Where("ID_IVA_RETEFUENTE", IdIvaRetefuente).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvMasIvaRetefuenteCollection FetchByQuery(Query qry)
        {
            InvMasIvaRetefuenteCollection coll = new InvMasIvaRetefuenteCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object IdIvaRetefuente)
        {
            return (InvMasIvaRetefuente.Delete(IdIvaRetefuente) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object IdIvaRetefuente)
        {
            return (InvMasIvaRetefuente.Destroy(IdIvaRetefuente) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(int IdIvaRetefuente,decimal Iva,decimal RetencionFuente)
	    {
		    InvMasIvaRetefuente item = new InvMasIvaRetefuente();
		    
            item.IdIvaRetefuente = IdIvaRetefuente;
            
            item.Iva = Iva;
            
            item.RetencionFuente = RetencionFuente;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(int IdIvaRetefuente,decimal Iva,decimal RetencionFuente)
	    {
		    InvMasIvaRetefuente item = new InvMasIvaRetefuente();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.IdIvaRetefuente = IdIvaRetefuente;
				
			item.Iva = Iva;
				
			item.RetencionFuente = RetencionFuente;
				
	        item.Save(UserName);
	    }
    }
}
