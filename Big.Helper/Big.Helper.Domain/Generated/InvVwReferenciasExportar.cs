using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated{
    /// <summary>
    /// Strongly-typed collection for the InvVwReferenciasExportar class.
    /// </summary>
    [Serializable]
    public partial class InvVwReferenciasExportarCollection : ReadOnlyList<InvVwReferenciasExportar, InvVwReferenciasExportarCollection>
    {        
        public InvVwReferenciasExportarCollection() {}
    }
    /// <summary>
    /// This is  Read-only wrapper class for the INV_VW_REFERENCIAS_EXPORTAR view.
    /// </summary>
    [Serializable]
    public partial class InvVwReferenciasExportar : ReadOnlyRecord<InvVwReferenciasExportar>, IReadOnlyRecord
    {
    
	    #region Default Settings
	    protected static void SetSQLProps() 
	    {
		    GetTableSchema();
	    }
	    #endregion
        #region Schema Accessor
	    public static TableSchema.Table Schema
        {
            get
            {
                if (BaseSchema == null)
                {
                    SetSQLProps();
                }
                return BaseSchema;
            }
        }
    	
        private static void GetTableSchema() 
        {
            if(!IsSchemaInitialized)
            {
                //Schema declaration
                TableSchema.Table schema = new TableSchema.Table("INV_VW_REFERENCIAS_EXPORTAR", TableType.View, DataService.GetInstance("dbInventariosV2"));
                schema.Columns = new TableSchema.TableColumnCollection();
                schema.SchemaName = @"dbo";
                //columns
                
                TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
                colvarGuid.ColumnName = "GUID";
                colvarGuid.DataType = DbType.String;
                colvarGuid.MaxLength = 36;
                colvarGuid.AutoIncrement = false;
                colvarGuid.IsNullable = false;
                colvarGuid.IsPrimaryKey = false;
                colvarGuid.IsForeignKey = false;
                colvarGuid.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuid);
                
                TableSchema.TableColumn colvarReferencia = new TableSchema.TableColumn(schema);
                colvarReferencia.ColumnName = "Referencia";
                colvarReferencia.DataType = DbType.String;
                colvarReferencia.MaxLength = 100;
                colvarReferencia.AutoIncrement = false;
                colvarReferencia.IsNullable = false;
                colvarReferencia.IsPrimaryKey = false;
                colvarReferencia.IsForeignKey = false;
                colvarReferencia.IsReadOnly = false;
                
                schema.Columns.Add(colvarReferencia);
                
                TableSchema.TableColumn colvarEstado = new TableSchema.TableColumn(schema);
                colvarEstado.ColumnName = "Estado";
                colvarEstado.DataType = DbType.AnsiString;
                colvarEstado.MaxLength = 50;
                colvarEstado.AutoIncrement = false;
                colvarEstado.IsNullable = false;
                colvarEstado.IsPrimaryKey = false;
                colvarEstado.IsForeignKey = false;
                colvarEstado.IsReadOnly = false;
                
                schema.Columns.Add(colvarEstado);
                
                TableSchema.TableColumn colvarRazónSocialDelProveedor = new TableSchema.TableColumn(schema);
                colvarRazónSocialDelProveedor.ColumnName = "Razón Social del Proveedor";
                colvarRazónSocialDelProveedor.DataType = DbType.String;
                colvarRazónSocialDelProveedor.MaxLength = 200;
                colvarRazónSocialDelProveedor.AutoIncrement = false;
                colvarRazónSocialDelProveedor.IsNullable = false;
                colvarRazónSocialDelProveedor.IsPrimaryKey = false;
                colvarRazónSocialDelProveedor.IsForeignKey = false;
                colvarRazónSocialDelProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarRazónSocialDelProveedor);
                
                TableSchema.TableColumn colvarNombreDelProducto = new TableSchema.TableColumn(schema);
                colvarNombreDelProducto.ColumnName = "Nombre del Producto";
                colvarNombreDelProducto.DataType = DbType.String;
                colvarNombreDelProducto.MaxLength = 100;
                colvarNombreDelProducto.AutoIncrement = false;
                colvarNombreDelProducto.IsNullable = false;
                colvarNombreDelProducto.IsPrimaryKey = false;
                colvarNombreDelProducto.IsForeignKey = false;
                colvarNombreDelProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreDelProducto);
                
                TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
                colvarIdPrograma.ColumnName = "ID_PROGRAMA";
                colvarIdPrograma.DataType = DbType.Int32;
                colvarIdPrograma.MaxLength = 0;
                colvarIdPrograma.AutoIncrement = false;
                colvarIdPrograma.IsNullable = false;
                colvarIdPrograma.IsPrimaryKey = false;
                colvarIdPrograma.IsForeignKey = false;
                colvarIdPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdPrograma);
                
                TableSchema.TableColumn colvarNombreDelPrograma = new TableSchema.TableColumn(schema);
                colvarNombreDelPrograma.ColumnName = "Nombre del Programa";
                colvarNombreDelPrograma.DataType = DbType.String;
                colvarNombreDelPrograma.MaxLength = 100;
                colvarNombreDelPrograma.AutoIncrement = false;
                colvarNombreDelPrograma.IsNullable = false;
                colvarNombreDelPrograma.IsPrimaryKey = false;
                colvarNombreDelPrograma.IsForeignKey = false;
                colvarNombreDelPrograma.IsReadOnly = false;
                
                schema.Columns.Add(colvarNombreDelPrograma);
                
                TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
                colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
                colvarIdProveedor.DataType = DbType.Int32;
                colvarIdProveedor.MaxLength = 0;
                colvarIdProveedor.AutoIncrement = false;
                colvarIdProveedor.IsNullable = false;
                colvarIdProveedor.IsPrimaryKey = false;
                colvarIdProveedor.IsForeignKey = false;
                colvarIdProveedor.IsReadOnly = false;
                
                schema.Columns.Add(colvarIdProveedor);
                
                TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
                colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
                colvarGuidProducto.DataType = DbType.String;
                colvarGuidProducto.MaxLength = 36;
                colvarGuidProducto.AutoIncrement = false;
                colvarGuidProducto.IsNullable = false;
                colvarGuidProducto.IsPrimaryKey = false;
                colvarGuidProducto.IsForeignKey = false;
                colvarGuidProducto.IsReadOnly = false;
                
                schema.Columns.Add(colvarGuidProducto);
                
                
                BaseSchema = schema;
                //add this schema to the provider
                //so we can query it later
                DataService.Providers["dbInventariosV2"].AddSchema("INV_VW_REFERENCIAS_EXPORTAR",schema);
            }
        }
        #endregion
        
        #region Query Accessor
	    public static Query CreateQuery()
	    {
		    return new Query(Schema);
	    }
	    #endregion
	    
	    #region .ctors
	    public InvVwReferenciasExportar()
	    {
            SetSQLProps();
            SetDefaults();
            MarkNew();
        }
        public InvVwReferenciasExportar(bool useDatabaseDefaults)
	    {
		    SetSQLProps();
		    if(useDatabaseDefaults)
		    {
				ForceDefaults();
			}
			MarkNew();
	    }
	    
	    public InvVwReferenciasExportar(object keyID)
	    {
		    SetSQLProps();
		    LoadByKey(keyID);
	    }
    	 
	    public InvVwReferenciasExportar(string columnName, object columnValue)
        {
            SetSQLProps();
            LoadByParam(columnName,columnValue);
        }
        
	    #endregion
	    
	    #region Props
	    
          
        [XmlAttribute("Guid")]
        [Bindable(true)]
        public string Guid 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID");
		    }
            set 
		    {
			    SetColumnValue("GUID", value);
            }
        }
	      
        [XmlAttribute("Referencia")]
        [Bindable(true)]
        public string Referencia 
	    {
		    get
		    {
			    return GetColumnValue<string>("Referencia");
		    }
            set 
		    {
			    SetColumnValue("Referencia", value);
            }
        }
	      
        [XmlAttribute("Estado")]
        [Bindable(true)]
        public string Estado 
	    {
		    get
		    {
			    return GetColumnValue<string>("Estado");
		    }
            set 
		    {
			    SetColumnValue("Estado", value);
            }
        }
	      
        [XmlAttribute("RazónSocialDelProveedor")]
        [Bindable(true)]
        public string RazónSocialDelProveedor 
	    {
		    get
		    {
			    return GetColumnValue<string>("Razón Social del Proveedor");
		    }
            set 
		    {
			    SetColumnValue("Razón Social del Proveedor", value);
            }
        }
	      
        [XmlAttribute("NombreDelProducto")]
        [Bindable(true)]
        public string NombreDelProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nombre del Producto");
		    }
            set 
		    {
			    SetColumnValue("Nombre del Producto", value);
            }
        }
	      
        [XmlAttribute("IdPrograma")]
        [Bindable(true)]
        public int IdPrograma 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROGRAMA");
		    }
            set 
		    {
			    SetColumnValue("ID_PROGRAMA", value);
            }
        }
	      
        [XmlAttribute("NombreDelPrograma")]
        [Bindable(true)]
        public string NombreDelPrograma 
	    {
		    get
		    {
			    return GetColumnValue<string>("Nombre del Programa");
		    }
            set 
		    {
			    SetColumnValue("Nombre del Programa", value);
            }
        }
	      
        [XmlAttribute("IdProveedor")]
        [Bindable(true)]
        public int IdProveedor 
	    {
		    get
		    {
			    return GetColumnValue<int>("ID_PROVEEDOR");
		    }
            set 
		    {
			    SetColumnValue("ID_PROVEEDOR", value);
            }
        }
	      
        [XmlAttribute("GuidProducto")]
        [Bindable(true)]
        public string GuidProducto 
	    {
		    get
		    {
			    return GetColumnValue<string>("GUID_PRODUCTO");
		    }
            set 
		    {
			    SetColumnValue("GUID_PRODUCTO", value);
            }
        }
	    
	    #endregion
    
	    #region Columns Struct
	    public struct Columns
	    {
		    
		    
            public static string Guid = @"GUID";
            
            public static string Referencia = @"Referencia";
            
            public static string Estado = @"Estado";
            
            public static string RazónSocialDelProveedor = @"Razón Social del Proveedor";
            
            public static string NombreDelProducto = @"Nombre del Producto";
            
            public static string IdPrograma = @"ID_PROGRAMA";
            
            public static string NombreDelPrograma = @"Nombre del Programa";
            
            public static string IdProveedor = @"ID_PROVEEDOR";
            
            public static string GuidProducto = @"GUID_PRODUCTO";
            
	    }
	    #endregion
	    
	    
	    #region IAbstractRecord Members
        public new CT GetColumnValue<CT>(string columnName) {
            return base.GetColumnValue<CT>(columnName);
        }
        public object GetColumnValue(string columnName) {
            return base.GetColumnValue<object>(columnName);
        }
        #endregion
	    
    }
}
