using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
    /// <summary>
    /// Controller class for INV_CGUNO_CUENTAS
    /// </summary>
    [System.ComponentModel.DataObject]
    public partial class InvCgunoCuentaController
    {
        // Preload our schema..
        InvCgunoCuenta thisSchemaLoad = new InvCgunoCuenta();
        private string userName = String.Empty;
        protected string UserName
        {
            get
            {
				if (userName.Length == 0) 
				{
    				if (System.Web.HttpContext.Current != null)
    				{
						userName=System.Web.HttpContext.Current.User.Identity.Name;
					}
					else
					{
						userName=System.Threading.Thread.CurrentPrincipal.Identity.Name;
					}
				}
				return userName;
            }
        }
        [DataObjectMethod(DataObjectMethodType.Select, true)]
        public InvCgunoCuentaCollection FetchAll()
        {
            InvCgunoCuentaCollection coll = new InvCgunoCuentaCollection();
            Query qry = new Query(InvCgunoCuenta.Schema);
            coll.LoadAndCloseReader(qry.ExecuteReader());
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCuentaCollection FetchByID(object Id)
        {
            InvCgunoCuentaCollection coll = new InvCgunoCuentaCollection().Where("ID", Id).Load();
            return coll;
        }
		
		[DataObjectMethod(DataObjectMethodType.Select, false)]
        public InvCgunoCuentaCollection FetchByQuery(Query qry)
        {
            InvCgunoCuentaCollection coll = new InvCgunoCuentaCollection();
            coll.LoadAndCloseReader(qry.ExecuteReader()); 
            return coll;
        }
        [DataObjectMethod(DataObjectMethodType.Delete, true)]
        public bool Delete(object Id)
        {
            return (InvCgunoCuenta.Delete(Id) == 1);
        }
        [DataObjectMethod(DataObjectMethodType.Delete, false)]
        public bool Destroy(object Id)
        {
            return (InvCgunoCuenta.Destroy(Id) == 1);
        }
        
        
    	
	    /// <summary>
	    /// Inserts a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Insert, true)]
	    public void Insert(string Guid,string NumeroCuenta,string NombreCuenta,bool RequiereTerceros,bool RequiereCentroCostos,bool RequiereReferencia,bool UtilizaBase,decimal? BaseX,DateTime FechaCreacion,int? IdCentroUtilidad)
	    {
		    InvCgunoCuenta item = new InvCgunoCuenta();
		    
            item.Guid = Guid;
            
            item.NumeroCuenta = NumeroCuenta;
            
            item.NombreCuenta = NombreCuenta;
            
            item.RequiereTerceros = RequiereTerceros;
            
            item.RequiereCentroCostos = RequiereCentroCostos;
            
            item.RequiereReferencia = RequiereReferencia;
            
            item.UtilizaBase = UtilizaBase;
            
            item.BaseX = BaseX;
            
            item.FechaCreacion = FechaCreacion;
            
            item.IdCentroUtilidad = IdCentroUtilidad;
            
	    
		    item.Save(UserName);
	    }
    	
	    /// <summary>
	    /// Updates a record, can be used with the Object Data Source
	    /// </summary>
        [DataObjectMethod(DataObjectMethodType.Update, true)]
	    public void Update(string Guid,int Id,string NumeroCuenta,string NombreCuenta,bool RequiereTerceros,bool RequiereCentroCostos,bool RequiereReferencia,bool UtilizaBase,decimal? BaseX,DateTime FechaCreacion,int? IdCentroUtilidad)
	    {
		    InvCgunoCuenta item = new InvCgunoCuenta();
	        item.MarkOld();
	        item.IsLoaded = true;
		    
			item.Guid = Guid;
				
			item.Id = Id;
				
			item.NumeroCuenta = NumeroCuenta;
				
			item.NombreCuenta = NombreCuenta;
				
			item.RequiereTerceros = RequiereTerceros;
				
			item.RequiereCentroCostos = RequiereCentroCostos;
				
			item.RequiereReferencia = RequiereReferencia;
				
			item.UtilizaBase = UtilizaBase;
				
			item.BaseX = BaseX;
				
			item.FechaCreacion = FechaCreacion;
				
			item.IdCentroUtilidad = IdCentroUtilidad;
				
	        item.Save(UserName);
	    }
    }
}
