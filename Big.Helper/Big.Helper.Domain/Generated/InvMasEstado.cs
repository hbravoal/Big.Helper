using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace Big.Helper.Domain.Generated
{
	/// <summary>
	/// Strongly-typed collection for the InvMasEstado class.
	/// </summary>
    [Serializable]
	public partial class InvMasEstadoCollection : ActiveList<InvMasEstado, InvMasEstadoCollection>
	{	   
		public InvMasEstadoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>InvMasEstadoCollection</returns>
		public InvMasEstadoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                InvMasEstado o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_ESTADOS table.
	/// </summary>
	[Serializable]
	public partial class InvMasEstado : ActiveRecord<InvMasEstado>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public InvMasEstado()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public InvMasEstado(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public InvMasEstado(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public InvMasEstado(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_ESTADOS", TableType.Table, DataService.GetInstance("dbInventariosV2"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
				colvarIdEstado.ColumnName = "ID_ESTADO";
				colvarIdEstado.DataType = DbType.Int32;
				colvarIdEstado.MaxLength = 0;
				colvarIdEstado.AutoIncrement = false;
				colvarIdEstado.IsNullable = false;
				colvarIdEstado.IsPrimaryKey = true;
				colvarIdEstado.IsForeignKey = false;
				colvarIdEstado.IsReadOnly = false;
				colvarIdEstado.DefaultSetting = @"";
				colvarIdEstado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdEstado);
				
				TableSchema.TableColumn colvarDescripcion = new TableSchema.TableColumn(schema);
				colvarDescripcion.ColumnName = "DESCRIPCION";
				colvarDescripcion.DataType = DbType.AnsiString;
				colvarDescripcion.MaxLength = 50;
				colvarDescripcion.AutoIncrement = false;
				colvarDescripcion.IsNullable = false;
				colvarDescripcion.IsPrimaryKey = false;
				colvarDescripcion.IsForeignKey = false;
				colvarDescripcion.IsReadOnly = false;
				colvarDescripcion.DefaultSetting = @"";
				colvarDescripcion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDescripcion);
				
				TableSchema.TableColumn colvarIdTipoEstado = new TableSchema.TableColumn(schema);
				colvarIdTipoEstado.ColumnName = "ID_TIPO_ESTADO";
				colvarIdTipoEstado.DataType = DbType.Int32;
				colvarIdTipoEstado.MaxLength = 0;
				colvarIdTipoEstado.AutoIncrement = false;
				colvarIdTipoEstado.IsNullable = false;
				colvarIdTipoEstado.IsPrimaryKey = false;
				colvarIdTipoEstado.IsForeignKey = true;
				colvarIdTipoEstado.IsReadOnly = false;
				colvarIdTipoEstado.DefaultSetting = @"";
				
					colvarIdTipoEstado.ForeignKeyTableName = "INV_MAS_TIPOS_ESTADO";
				schema.Columns.Add(colvarIdTipoEstado);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["dbInventariosV2"].AddSchema("INV_MAS_ESTADOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdEstado")]
		[Bindable(true)]
		public int IdEstado 
		{
			get { return GetColumnValue<int>(Columns.IdEstado); }
			set { SetColumnValue(Columns.IdEstado, value); }
		}
		  
		[XmlAttribute("Descripcion")]
		[Bindable(true)]
		public string Descripcion 
		{
			get { return GetColumnValue<string>(Columns.Descripcion); }
			set { SetColumnValue(Columns.Descripcion, value); }
		}
		  
		[XmlAttribute("IdTipoEstado")]
		[Bindable(true)]
		public int IdTipoEstado 
		{
			get { return GetColumnValue<int>(Columns.IdTipoEstado); }
			set { SetColumnValue(Columns.IdTipoEstado, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public Big.Helper.Domain.Generated.InvMasTiposProductoCollection InvMasTiposProductoRecords()
		{
			return new Big.Helper.Domain.Generated.InvMasTiposProductoCollection().Where(InvMasTiposProducto.Columns.IdEstado, IdEstado).Load();
		}
		public Big.Helper.Domain.Generated.InvProveedoreCollection InvProveedores()
		{
			return new Big.Helper.Domain.Generated.InvProveedoreCollection().Where(InvProveedore.Columns.IdEstado, IdEstado).Load();
		}
		public Big.Helper.Domain.Generated.InvReferenciasProductoCollection InvReferenciasProductoRecords()
		{
			return new Big.Helper.Domain.Generated.InvReferenciasProductoCollection().Where(InvReferenciasProducto.Columns.IdEstado, IdEstado).Load();
		}
		public Big.Helper.Domain.Generated.InvUsuarioCollection InvUsuarios()
		{
			return new Big.Helper.Domain.Generated.InvUsuarioCollection().Where(InvUsuario.Columns.IdEstado, IdEstado).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a InvMasTiposEstado ActiveRecord object related to this InvMasEstado
		/// 
		/// </summary>
		public Big.Helper.Domain.Generated.InvMasTiposEstado InvMasTiposEstado
		{
			get { return Big.Helper.Domain.Generated.InvMasTiposEstado.FetchByID(this.IdTipoEstado); }
			set { SetColumnValue("ID_TIPO_ESTADO", value.IdTipoEstado); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdEstado,string varDescripcion,int varIdTipoEstado)
		{
			InvMasEstado item = new InvMasEstado();
			
			item.IdEstado = varIdEstado;
			
			item.Descripcion = varDescripcion;
			
			item.IdTipoEstado = varIdTipoEstado;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdEstado,string varDescripcion,int varIdTipoEstado)
		{
			InvMasEstado item = new InvMasEstado();
			
				item.IdEstado = varIdEstado;
			
				item.Descripcion = varDescripcion;
			
				item.IdTipoEstado = varIdTipoEstado;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdEstadoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn DescripcionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoEstadoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdEstado = @"ID_ESTADO";
			 public static string Descripcion = @"DESCRIPCION";
			 public static string IdTipoEstado = @"ID_TIPO_ESTADO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
