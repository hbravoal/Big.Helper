﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Big.Helper.UI.Startup))]
namespace Big.Helper.UI
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
