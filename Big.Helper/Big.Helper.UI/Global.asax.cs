﻿using Big.Helper.UI.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Big.Helper.UI
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            //CreateRolesAndSuperuser();
        }
        private void CreateRolesAndSuperuser()
        {
            ApplicationDbContext db = new ApplicationDbContext();
            CreateRoles(db);
            CreateSuperuser(db);
            AddPermisionsToSuperuser(db);
            db.Dispose();
        }
        private void CreateRoles(ApplicationDbContext db)
        {
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            if (!roleManager.RoleExists("Admin"))
            {
                var result = roleManager.Create(new IdentityRole("Admin"));
            }

            if (!roleManager.RoleExists("Guess"))
            {
                var result = roleManager.Create(new IdentityRole("Guess"));
            }

           
        }
        private void CreateSuperuser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var user = userManager.FindByName("henry.bravo@bigholding.com.co");
            if (user == null)
            {
                user = new ApplicationUser();
                user.UserName = "henry.bravo@bigholding.com.co";
                user.Email = "henry.bravo@bigholding.com.co";
                var result = userManager.Create(user, "25Junio1996");
            }
        }
        private void AddPermisionsToSuperuser(ApplicationDbContext db)
        {
            var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(db));
            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(db));

            var user = userManager.FindByName("henry.bravo@bigholding.com.co");

            if (!userManager.IsInRole(user.Id, "Admin"))
            {
                userManager.AddToRole(user.Id, "Admin");
            }

            if (!userManager.IsInRole(user.Id, "Guess"))
            {
                userManager.AddToRole(user.Id, "Guess");
            }

            
        }



    }
}
