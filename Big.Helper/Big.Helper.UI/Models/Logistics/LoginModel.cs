﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Big.Helper.UI.Models.Logistics
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Debes ingresar  {0}")]

            public string User { get; set; }
        [DataType(DataType.Password)]
        [Required(ErrorMessage = "Debes ingresar  {0}")]
            public string Password { get; set; }
    }
}