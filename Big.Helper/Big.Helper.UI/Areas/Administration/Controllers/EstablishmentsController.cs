﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.Helper.UI.Areas.Administration.Controllers
{
    public class EstablishmentsController : Controller
    {
        // GET: Administration/Establishments
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administration/Establishments/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administration/Establishments/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/Establishments/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Establishments/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administration/Establishments/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Establishments/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administration/Establishments/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
