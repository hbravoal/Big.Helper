﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.Helper.UI.Areas.Administration.Controllers
{
    public class ProvidersController : Controller
    {
        // GET: Administration/Providers
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administration/Providers/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administration/Providers/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/Providers/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Providers/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administration/Providers/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Providers/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administration/Providers/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
