﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.Helper.UI.Areas.Administration.Controllers
{
    public class ConsecutivesController : Controller
    {
        // GET: Administration/Consecutives
        public ActionResult Index()
        {
            return View();
        }

        // GET: Administration/Consecutives/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Administration/Consecutives/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Administration/Consecutives/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Consecutives/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Administration/Consecutives/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Administration/Consecutives/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Administration/Consecutives/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
