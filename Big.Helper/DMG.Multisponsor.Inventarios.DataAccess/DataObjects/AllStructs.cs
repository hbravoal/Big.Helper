using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.CGUno
{
	#region Tables Struct
	public partial struct Tables
	{
		
		public static string Causaciones = @"INV_CGUNO_CAUSACIONES";
        
		public static string CausacionesPeriodos = @"INV_CGUNO_CAUSACIONES_PERIODOS";
        
		public static string CentroUtilidad = @"INV_CGUNO_CENTRO_UTILIDAD";
        
		public static string Cuentas = @"INV_CGUNO_CUENTAS";
        
		public static string DetallesCausacion = @"INV_CGUNO_DETALLES_CAUSACION";
        
		public static string DetallesCausacionesPeriodo = @"INV_CGUNO_DETALLES_CAUSACIONES_PERIODO";
        
		public static string MasCamposTipoCausacion = @"INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION";
        
		public static string MasEstadosPeriodo = @"INV_CGUNO_MAS_ESTADOS_PERIODO";
        
		public static string MasNaturalezasDetalleCausacion = @"INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION";
        
		public static string MasTiposCausacion = @"INV_CGUNO_MAS_TIPOS_CAUSACION";
        
		public static string Periodos = @"INV_CGUNO_PERIODOS";
        
	}
	#endregion
    #region Schemas
    public partial class Schemas {
		
		public static TableSchema.Table Causaciones{
            get { return DataService.GetSchema("INV_CGUNO_CAUSACIONES","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table CausacionesPeriodos{
            get { return DataService.GetSchema("INV_CGUNO_CAUSACIONES_PERIODOS","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table CentroUtilidad{
            get { return DataService.GetSchema("INV_CGUNO_CENTRO_UTILIDAD","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table Cuentas{
            get { return DataService.GetSchema("INV_CGUNO_CUENTAS","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table DetallesCausacion{
            get { return DataService.GetSchema("INV_CGUNO_DETALLES_CAUSACION","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table DetallesCausacionesPeriodo{
            get { return DataService.GetSchema("INV_CGUNO_DETALLES_CAUSACIONES_PERIODO","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table MasCamposTipoCausacion{
            get { return DataService.GetSchema("INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table MasEstadosPeriodo{
            get { return DataService.GetSchema("INV_CGUNO_MAS_ESTADOS_PERIODO","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table MasNaturalezasDetalleCausacion{
            get { return DataService.GetSchema("INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table MasTiposCausacion{
            get { return DataService.GetSchema("INV_CGUNO_MAS_TIPOS_CAUSACION","InventariosCGUnoProvider"); }
		}
        
		public static TableSchema.Table Periodos{
            get { return DataService.GetSchema("INV_CGUNO_PERIODOS","InventariosCGUnoProvider"); }
		}
        
	
    }
    #endregion
    #region View Struct
    public partial struct Views 
    {
		
    }
    #endregion
    
    #region Query Factories
	public static partial class DB
	{
        public static DataProvider _provider = DataService.Providers["InventariosCGUnoProvider"];
        static ISubSonicRepository _repository;
        public static ISubSonicRepository Repository {
            get {
                if (_repository == null)
                    return new SubSonicRepository(_provider);
                return _repository; 
            }
            set { _repository = value; }
        }
	
        public static Select SelectAllColumnsFrom<T>() where T : RecordBase<T>, new()
	    {
            return Repository.SelectAllColumnsFrom<T>();
            
	    }
	    public static Select Select()
	    {
            return Repository.Select();
	    }
	    
		public static Select Select(params string[] columns)
		{
            return Repository.Select(columns);
        }
	    
		public static Select Select(params Aggregate[] aggregates)
		{
            return Repository.Select(aggregates);
        }
   
	    public static Update Update<T>() where T : RecordBase<T>, new()
	    {
            return Repository.Update<T>();
	    }
     
	    
	    public static Insert Insert()
	    {
            return Repository.Insert();
	    }
	    
	    public static Delete Delete()
	    {
            
            return Repository.Delete();
	    }
	    
	    public static InlineQuery Query()
	    {
            
            return Repository.Query();
	    }
	    	    
	    
	}
    #endregion
    
}
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	#region Tables Struct
	public partial struct Tables
	{
		
		public static string Ajustes = @"INV_AJUSTES";
        
		public static string Bodegas = @"INV_BODEGAS";
        
		public static string BodegasProductos = @"INV_BODEGAS_PRODUCTOS";
        
		public static string CacheDetalleAjuste = @"INV_CACHE_DETALLE_AJUSTE";
        
		public static string CacheDetallesFactura = @"INV_CACHE_DETALLES_FACTURA";
        
		public static string CgunoCausaciones = @"INV_CGUNO_CAUSACIONES";
        
		public static string CgunoCausacionesPeriodos = @"INV_CGUNO_CAUSACIONES_PERIODOS";
        
		public static string CgunoCentroUtilidad = @"INV_CGUNO_CENTRO_UTILIDAD";
        
		public static string CgunoCuentas = @"INV_CGUNO_CUENTAS";
        
		public static string CgunoDetallesCausacion = @"INV_CGUNO_DETALLES_CAUSACION";
        
		public static string CgunoDetallesCausacionesPeriodo = @"INV_CGUNO_DETALLES_CAUSACIONES_PERIODO";
        
		public static string CgunoMasCamposTipoCausacion = @"INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION";
        
		public static string CgunoMasEstadosPeriodo = @"INV_CGUNO_MAS_ESTADOS_PERIODO";
        
		public static string CgunoMasNaturalezasDetalleCausacion = @"INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION";
        
		public static string CgunoMasTiposCausacion = @"INV_CGUNO_MAS_TIPOS_CAUSACION";
        
		public static string CgunoPeriodos = @"INV_CGUNO_PERIODOS";
        
		public static string CierreMensual = @"INV_CIERRE_MENSUAL";
        
		public static string Clientes = @"INV_CLIENTES";
        
		public static string CostoEnvioAlterno = @"INV_COSTO_ENVIO_ALTERNO";
        
		public static string DetalleAjuste = @"INV_DETALLE_AJUSTE";
        
		public static string DetallesFactura = @"INV_DETALLES_FACTURA";
        
		public static string DetallesFacturaBanco = @"INV_DETALLES_FACTURA_BANCO";
        
		public static string DetallesFacturaProveedorPuntos = @"INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS";
        
		public static string Empresas = @"INV_EMPRESAS";
        
		public static string Facturas = @"INV_FACTURAS";
        
		public static string FacturasBanco = @"INV_FACTURAS_BANCO";
        
		public static string FacturasProveedorPuntos = @"INV_FACTURAS_PROVEEDOR_PUNTOS";
        
		public static string HistoricoBodega = @"INV_HISTORICO_BODEGA";
        
		public static string HistoricoSiniestros = @"INV_HISTORICO_SINIESTROS";
        
		public static string HistoricosPrecio = @"INV_HISTORICOS_PRECIO";
        
		public static string Marcas = @"INV_MARCAS";
        
		public static string MasCiudades = @"INV_MAS_CIUDADES";
        
		public static string MasDepartamentos = @"INV_MAS_DEPARTAMENTOS";
        
		public static string MasEntidadesBancarias = @"INV_MAS_ENTIDADES_BANCARIAS";
        
		public static string MasEstados = @"INV_MAS_ESTADOS";
        
		public static string MasIvaRetefuente = @"INV_MAS_IVA_RETEFUENTE";
        
		public static string MasPaises = @"INV_MAS_PAISES";
        
		public static string MasPermisos = @"INV_MAS_PERMISOS";
        
		public static string MasTiposBodega = @"INV_MAS_TIPOS_BODEGA";
        
		public static string MasTiposCuenta = @"INV_MAS_TIPOS_CUENTA";
        
		public static string MasTiposDelivery = @"INV_MAS_TIPOS_DELIVERY";
        
		public static string MasTiposEmpaque = @"INV_MAS_TIPOS_EMPAQUE";
        
		public static string MasTiposEstado = @"INV_MAS_TIPOS_ESTADO";
        
		public static string MasTiposFacturacion = @"INV_MAS_TIPOS_FACTURACION";
        
		public static string MasTiposProducto = @"INV_MAS_TIPOS_PRODUCTO";
        
		public static string MasTiposResolucion = @"INV_MAS_TIPOS_RESOLUCION";
        
		public static string Perfiles = @"INV_PERFILES";
        
		public static string PerfilesPermisos = @"INV_PERFILES_PERMISOS";
        
		public static string PeriodoFacturacionPuntos = @"INV_PERIODO_FACTURACION_PUNTOS";
        
		public static string PreciosBasePrograma = @"INV_PRECIOS_BASE_PROGRAMA";
        
		public static string PrefacturaPeriodo = @"INV_PREFACTURA_PERIODO";
        
		public static string Prefacturas = @"INV_PREFACTURAS";
        
		public static string Productos = @"INV_PRODUCTOS";
        
		public static string Programas = @"INV_PROGRAMAS";
        
		public static string Proveedores = @"INV_PROVEEDORES";
        
		public static string ReferenciasProducto = @"INV_REFERENCIAS_PRODUCTO";
        
		public static string ReplicacionModulos = @"INV_REPLICACION_MODULOS";
        
		public static string ReplicacionPeticiones = @"INV_REPLICACION_PETICIONES";
        
		public static string Resoluciones = @"INV_RESOLUCIONES";
        
		public static string Siniestros = @"INV_SINIESTROS";
        
		public static string TmpCoordinadora = @"INV_TMP_COORDINADORA";
        
		public static string Usuarios = @"INV_USUARIOS";
        
	}
	#endregion
    #region Schemas
    public partial class Schemas {
		
		public static TableSchema.Table Ajustes{
            get { return DataService.GetSchema("INV_AJUSTES","InventariosProvider"); }
		}
        
		public static TableSchema.Table Bodegas{
            get { return DataService.GetSchema("INV_BODEGAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table BodegasProductos{
            get { return DataService.GetSchema("INV_BODEGAS_PRODUCTOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table CacheDetalleAjuste{
            get { return DataService.GetSchema("INV_CACHE_DETALLE_AJUSTE","InventariosProvider"); }
		}
        
		public static TableSchema.Table CacheDetallesFactura{
            get { return DataService.GetSchema("INV_CACHE_DETALLES_FACTURA","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoCausaciones{
            get { return DataService.GetSchema("INV_CGUNO_CAUSACIONES","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoCausacionesPeriodos{
            get { return DataService.GetSchema("INV_CGUNO_CAUSACIONES_PERIODOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoCentroUtilidad{
            get { return DataService.GetSchema("INV_CGUNO_CENTRO_UTILIDAD","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoCuentas{
            get { return DataService.GetSchema("INV_CGUNO_CUENTAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoDetallesCausacion{
            get { return DataService.GetSchema("INV_CGUNO_DETALLES_CAUSACION","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoDetallesCausacionesPeriodo{
            get { return DataService.GetSchema("INV_CGUNO_DETALLES_CAUSACIONES_PERIODO","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoMasCamposTipoCausacion{
            get { return DataService.GetSchema("INV_CGUNO_MAS_CAMPOS_TIPO_CAUSACION","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoMasEstadosPeriodo{
            get { return DataService.GetSchema("INV_CGUNO_MAS_ESTADOS_PERIODO","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoMasNaturalezasDetalleCausacion{
            get { return DataService.GetSchema("INV_CGUNO_MAS_NATURALEZAS_DETALLE_CAUSACION","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoMasTiposCausacion{
            get { return DataService.GetSchema("INV_CGUNO_MAS_TIPOS_CAUSACION","InventariosProvider"); }
		}
        
		public static TableSchema.Table CgunoPeriodos{
            get { return DataService.GetSchema("INV_CGUNO_PERIODOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table CierreMensual{
            get { return DataService.GetSchema("INV_CIERRE_MENSUAL","InventariosProvider"); }
		}
        
		public static TableSchema.Table Clientes{
            get { return DataService.GetSchema("INV_CLIENTES","InventariosProvider"); }
		}
        
		public static TableSchema.Table CostoEnvioAlterno{
            get { return DataService.GetSchema("INV_COSTO_ENVIO_ALTERNO","InventariosProvider"); }
		}
        
		public static TableSchema.Table DetalleAjuste{
            get { return DataService.GetSchema("INV_DETALLE_AJUSTE","InventariosProvider"); }
		}
        
		public static TableSchema.Table DetallesFactura{
            get { return DataService.GetSchema("INV_DETALLES_FACTURA","InventariosProvider"); }
		}
        
		public static TableSchema.Table DetallesFacturaBanco{
            get { return DataService.GetSchema("INV_DETALLES_FACTURA_BANCO","InventariosProvider"); }
		}
        
		public static TableSchema.Table DetallesFacturaProveedorPuntos{
            get { return DataService.GetSchema("INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table Empresas{
            get { return DataService.GetSchema("INV_EMPRESAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table Facturas{
            get { return DataService.GetSchema("INV_FACTURAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table FacturasBanco{
            get { return DataService.GetSchema("INV_FACTURAS_BANCO","InventariosProvider"); }
		}
        
		public static TableSchema.Table FacturasProveedorPuntos{
            get { return DataService.GetSchema("INV_FACTURAS_PROVEEDOR_PUNTOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table HistoricoBodega{
            get { return DataService.GetSchema("INV_HISTORICO_BODEGA","InventariosProvider"); }
		}
        
		public static TableSchema.Table HistoricoSiniestros{
            get { return DataService.GetSchema("INV_HISTORICO_SINIESTROS","InventariosProvider"); }
		}
        
		public static TableSchema.Table HistoricosPrecio{
            get { return DataService.GetSchema("INV_HISTORICOS_PRECIO","InventariosProvider"); }
		}
        
		public static TableSchema.Table Marcas{
            get { return DataService.GetSchema("INV_MARCAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasCiudades{
            get { return DataService.GetSchema("INV_MAS_CIUDADES","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasDepartamentos{
            get { return DataService.GetSchema("INV_MAS_DEPARTAMENTOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasEntidadesBancarias{
            get { return DataService.GetSchema("INV_MAS_ENTIDADES_BANCARIAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasEstados{
            get { return DataService.GetSchema("INV_MAS_ESTADOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasIvaRetefuente{
            get { return DataService.GetSchema("INV_MAS_IVA_RETEFUENTE","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasPaises{
            get { return DataService.GetSchema("INV_MAS_PAISES","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasPermisos{
            get { return DataService.GetSchema("INV_MAS_PERMISOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposBodega{
            get { return DataService.GetSchema("INV_MAS_TIPOS_BODEGA","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposCuenta{
            get { return DataService.GetSchema("INV_MAS_TIPOS_CUENTA","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposDelivery{
            get { return DataService.GetSchema("INV_MAS_TIPOS_DELIVERY","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposEmpaque{
            get { return DataService.GetSchema("INV_MAS_TIPOS_EMPAQUE","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposEstado{
            get { return DataService.GetSchema("INV_MAS_TIPOS_ESTADO","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposFacturacion{
            get { return DataService.GetSchema("INV_MAS_TIPOS_FACTURACION","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposProducto{
            get { return DataService.GetSchema("INV_MAS_TIPOS_PRODUCTO","InventariosProvider"); }
		}
        
		public static TableSchema.Table MasTiposResolucion{
            get { return DataService.GetSchema("INV_MAS_TIPOS_RESOLUCION","InventariosProvider"); }
		}
        
		public static TableSchema.Table Perfiles{
            get { return DataService.GetSchema("INV_PERFILES","InventariosProvider"); }
		}
        
		public static TableSchema.Table PerfilesPermisos{
            get { return DataService.GetSchema("INV_PERFILES_PERMISOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table PeriodoFacturacionPuntos{
            get { return DataService.GetSchema("INV_PERIODO_FACTURACION_PUNTOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table PreciosBasePrograma{
            get { return DataService.GetSchema("INV_PRECIOS_BASE_PROGRAMA","InventariosProvider"); }
		}
        
		public static TableSchema.Table PrefacturaPeriodo{
            get { return DataService.GetSchema("INV_PREFACTURA_PERIODO","InventariosProvider"); }
		}
        
		public static TableSchema.Table Prefacturas{
            get { return DataService.GetSchema("INV_PREFACTURAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table Productos{
            get { return DataService.GetSchema("INV_PRODUCTOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table Programas{
            get { return DataService.GetSchema("INV_PROGRAMAS","InventariosProvider"); }
		}
        
		public static TableSchema.Table Proveedores{
            get { return DataService.GetSchema("INV_PROVEEDORES","InventariosProvider"); }
		}
        
		public static TableSchema.Table ReferenciasProducto{
            get { return DataService.GetSchema("INV_REFERENCIAS_PRODUCTO","InventariosProvider"); }
		}
        
		public static TableSchema.Table ReplicacionModulos{
            get { return DataService.GetSchema("INV_REPLICACION_MODULOS","InventariosProvider"); }
		}
        
		public static TableSchema.Table ReplicacionPeticiones{
            get { return DataService.GetSchema("INV_REPLICACION_PETICIONES","InventariosProvider"); }
		}
        
		public static TableSchema.Table Resoluciones{
            get { return DataService.GetSchema("INV_RESOLUCIONES","InventariosProvider"); }
		}
        
		public static TableSchema.Table Siniestros{
            get { return DataService.GetSchema("INV_SINIESTROS","InventariosProvider"); }
		}
        
		public static TableSchema.Table TmpCoordinadora{
            get { return DataService.GetSchema("INV_TMP_COORDINADORA","InventariosProvider"); }
		}
        
		public static TableSchema.Table Usuarios{
            get { return DataService.GetSchema("INV_USUARIOS","InventariosProvider"); }
		}
        
	
    }
    #endregion
    #region View Struct
    public partial struct Views 
    {
		
		public static string Ciudades = @"INV_VW_CIUDADES";
        
		public static string CiudadesMillas = @"INV_VW_CIUDADES_MILLAS";
        
		public static string ClientesMillas = @"INV_VW_CLIENTES_MILLAS";
        
		public static string ConsultaSaldos = @"INV_VW_CONSULTA_SALDOS";
        
		public static string DetallesFacturaMillas = @"INV_VW_DETALLES_FACTURA_MILLAS";
        
		public static string ExportarProveedores = @"INV_VW_EXPORTAR_PROVEEDORES";
        
		public static string FACTURAPROVPUNTOSProductoProveedorMillas = @"INV_VW_FACTURA(PROV_PUNTOS)_PRODUCTO_PROVEEDOR_MILLAS";
        
		public static string FacturaProductoProveedorMillas = @"INV_VW_FACTURA_PRODUCTO_PROVEEDOR_MILLAS";
        
		public static string FacturasMillas = @"INV_VW_FACTURAS_MILLAS";
        
		public static string PreciosExportar = @"INV_VW_PRECIOS_EXPORTAR";
        
		public static string Productos = @"INV_VW_PRODUCTOS";
        
		public static string ProductosExportar = @"INV_VW_PRODUCTOS_EXPORTAR";
        
		public static string ReferenciasExportar = @"INV_VW_REFERENCIAS_EXPORTAR";
        
		public static string ReportesFacturacion = @"INV_VW_REPORTES_FACTURACION";
        
    }
    #endregion
    
    #region Query Factories
	public static partial class DB
	{
        public static DataProvider _provider = DataService.Providers["InventariosProvider"];
        static ISubSonicRepository _repository;
        public static ISubSonicRepository Repository {
            get {
                if (_repository == null)
                    return new SubSonicRepository(_provider);
                return _repository; 
            }
            set { _repository = value; }
        }
	
        public static Select SelectAllColumnsFrom<T>() where T : RecordBase<T>, new()
	    {
            return Repository.SelectAllColumnsFrom<T>();
            
	    }
	    public static Select Select()
	    {
            return Repository.Select();
	    }
	    
		public static Select Select(params string[] columns)
		{
            return Repository.Select(columns);
        }
	    
		public static Select Select(params Aggregate[] aggregates)
		{
            return Repository.Select(aggregates);
        }
   
	    public static Update Update<T>() where T : RecordBase<T>, new()
	    {
            return Repository.Update<T>();
	    }
     
	    
	    public static Insert Insert()
	    {
            return Repository.Insert();
	    }
	    
	    public static Delete Delete()
	    {
            
            return Repository.Delete();
	    }
	    
	    public static InlineQuery Query()
	    {
            
            return Repository.Query();
	    }
	    	    
	    
	}
    #endregion
    
}
#region Databases
public partial struct Databases 
{
	
	public static string InventariosCGUnoProvider = @"InventariosCGUnoProvider";
    
	public static string InventariosProvider = @"InventariosProvider";
    
}
#endregion