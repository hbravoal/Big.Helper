using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the ReplicacionPeticiones class.
	/// </summary>
    [Serializable]
	public partial class ReplicacionPeticionesCollection : ActiveList<ReplicacionPeticiones, ReplicacionPeticionesCollection>
	{	   
		public ReplicacionPeticionesCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReplicacionPeticionesCollection</returns>
		public ReplicacionPeticionesCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReplicacionPeticiones o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_REPLICACION_PETICIONES table.
	/// </summary>
	[Serializable]
	public partial class ReplicacionPeticiones : ActiveRecord<ReplicacionPeticiones>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public ReplicacionPeticiones()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReplicacionPeticiones(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public ReplicacionPeticiones(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public ReplicacionPeticiones(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_REPLICACION_PETICIONES", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarIdModulo = new TableSchema.TableColumn(schema);
				colvarIdModulo.ColumnName = "ID_MODULO";
				colvarIdModulo.DataType = DbType.Int32;
				colvarIdModulo.MaxLength = 0;
				colvarIdModulo.AutoIncrement = false;
				colvarIdModulo.IsNullable = false;
				colvarIdModulo.IsPrimaryKey = false;
				colvarIdModulo.IsForeignKey = true;
				colvarIdModulo.IsReadOnly = false;
				colvarIdModulo.DefaultSetting = @"";
				
					colvarIdModulo.ForeignKeyTableName = "INV_REPLICACION_MODULOS";
				schema.Columns.Add(colvarIdModulo);
				
				TableSchema.TableColumn colvarFechaPeticion = new TableSchema.TableColumn(schema);
				colvarFechaPeticion.ColumnName = "FECHA_PETICION";
				colvarFechaPeticion.DataType = DbType.DateTime;
				colvarFechaPeticion.MaxLength = 0;
				colvarFechaPeticion.AutoIncrement = false;
				colvarFechaPeticion.IsNullable = false;
				colvarFechaPeticion.IsPrimaryKey = false;
				colvarFechaPeticion.IsForeignKey = false;
				colvarFechaPeticion.IsReadOnly = false;
				colvarFechaPeticion.DefaultSetting = @"";
				colvarFechaPeticion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaPeticion);
				
				TableSchema.TableColumn colvarProcesada = new TableSchema.TableColumn(schema);
				colvarProcesada.ColumnName = "PROCESADA";
				colvarProcesada.DataType = DbType.Boolean;
				colvarProcesada.MaxLength = 0;
				colvarProcesada.AutoIncrement = false;
				colvarProcesada.IsNullable = false;
				colvarProcesada.IsPrimaryKey = false;
				colvarProcesada.IsForeignKey = false;
				colvarProcesada.IsReadOnly = false;
				
						colvarProcesada.DefaultSetting = @"((0))";
				colvarProcesada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProcesada);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_REPLICACION_PETICIONES",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("IdModulo")]
		[Bindable(true)]
		public int IdModulo 
		{
			get { return GetColumnValue<int>(Columns.IdModulo); }
			set { SetColumnValue(Columns.IdModulo, value); }
		}
		  
		[XmlAttribute("FechaPeticion")]
		[Bindable(true)]
		public DateTime FechaPeticion 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaPeticion); }
			set { SetColumnValue(Columns.FechaPeticion, value); }
		}
		  
		[XmlAttribute("Procesada")]
		[Bindable(true)]
		public bool Procesada 
		{
			get { return GetColumnValue<bool>(Columns.Procesada); }
			set { SetColumnValue(Columns.Procesada, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a ReplicacionModulos ActiveRecord object related to this ReplicacionPeticiones
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ReplicacionModulos ReplicacionModulos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.ReplicacionModulos.FetchByID(this.IdModulo); }
			set { SetColumnValue("ID_MODULO", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdModulo,DateTime varFechaPeticion,bool varProcesada)
		{
			ReplicacionPeticiones item = new ReplicacionPeticiones();
			
			item.IdModulo = varIdModulo;
			
			item.FechaPeticion = varFechaPeticion;
			
			item.Procesada = varProcesada;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,int varIdModulo,DateTime varFechaPeticion,bool varProcesada)
		{
			ReplicacionPeticiones item = new ReplicacionPeticiones();
			
				item.Id = varId;
			
				item.IdModulo = varIdModulo;
			
				item.FechaPeticion = varFechaPeticion;
			
				item.Procesada = varProcesada;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdModuloColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaPeticionColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ProcesadaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string IdModulo = @"ID_MODULO";
			 public static string FechaPeticion = @"FECHA_PETICION";
			 public static string Procesada = @"PROCESADA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
