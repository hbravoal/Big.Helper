using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the Facturas class.
	/// </summary>
    [Serializable]
	public partial class FacturasCollection : ActiveList<Facturas, FacturasCollection>
	{	   
		public FacturasCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FacturasCollection</returns>
		public FacturasCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Facturas o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_FACTURAS table.
	/// </summary>
	[Serializable]
	public partial class Facturas : ActiveRecord<Facturas>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Facturas()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Facturas(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Facturas(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Facturas(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_FACTURAS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidFactura = new TableSchema.TableColumn(schema);
				colvarGuidFactura.ColumnName = "GUID_FACTURA";
				colvarGuidFactura.DataType = DbType.String;
				colvarGuidFactura.MaxLength = 36;
				colvarGuidFactura.AutoIncrement = false;
				colvarGuidFactura.IsNullable = false;
				colvarGuidFactura.IsPrimaryKey = true;
				colvarGuidFactura.IsForeignKey = false;
				colvarGuidFactura.IsReadOnly = false;
				colvarGuidFactura.DefaultSetting = @"";
				colvarGuidFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidFactura);
				
				TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
				colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
				colvarNumeroFactura.DataType = DbType.Int32;
				colvarNumeroFactura.MaxLength = 0;
				colvarNumeroFactura.AutoIncrement = false;
				colvarNumeroFactura.IsNullable = true;
				colvarNumeroFactura.IsPrimaryKey = false;
				colvarNumeroFactura.IsForeignKey = false;
				colvarNumeroFactura.IsReadOnly = false;
				colvarNumeroFactura.DefaultSetting = @"";
				colvarNumeroFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroFactura);
				
				TableSchema.TableColumn colvarTotalBase = new TableSchema.TableColumn(schema);
				colvarTotalBase.ColumnName = "TOTAL_BASE";
				colvarTotalBase.DataType = DbType.Decimal;
				colvarTotalBase.MaxLength = 0;
				colvarTotalBase.AutoIncrement = false;
				colvarTotalBase.IsNullable = false;
				colvarTotalBase.IsPrimaryKey = false;
				colvarTotalBase.IsForeignKey = false;
				colvarTotalBase.IsReadOnly = false;
				colvarTotalBase.DefaultSetting = @"";
				colvarTotalBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalBase);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarPrefacturada = new TableSchema.TableColumn(schema);
				colvarPrefacturada.ColumnName = "PREFACTURADA";
				colvarPrefacturada.DataType = DbType.Boolean;
				colvarPrefacturada.MaxLength = 0;
				colvarPrefacturada.AutoIncrement = false;
				colvarPrefacturada.IsNullable = false;
				colvarPrefacturada.IsPrimaryKey = false;
				colvarPrefacturada.IsForeignKey = false;
				colvarPrefacturada.IsReadOnly = false;
				colvarPrefacturada.DefaultSetting = @"";
				colvarPrefacturada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrefacturada);
				
				TableSchema.TableColumn colvarAnulada = new TableSchema.TableColumn(schema);
				colvarAnulada.ColumnName = "ANULADA";
				colvarAnulada.DataType = DbType.Boolean;
				colvarAnulada.MaxLength = 0;
				colvarAnulada.AutoIncrement = false;
				colvarAnulada.IsNullable = false;
				colvarAnulada.IsPrimaryKey = false;
				colvarAnulada.IsForeignKey = false;
				colvarAnulada.IsReadOnly = false;
				colvarAnulada.DefaultSetting = @"";
				colvarAnulada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAnulada);
				
				TableSchema.TableColumn colvarGeneradaEmpresa = new TableSchema.TableColumn(schema);
				colvarGeneradaEmpresa.ColumnName = "GENERADA_EMPRESA";
				colvarGeneradaEmpresa.DataType = DbType.Boolean;
				colvarGeneradaEmpresa.MaxLength = 0;
				colvarGeneradaEmpresa.AutoIncrement = false;
				colvarGeneradaEmpresa.IsNullable = false;
				colvarGeneradaEmpresa.IsPrimaryKey = false;
				colvarGeneradaEmpresa.IsForeignKey = false;
				colvarGeneradaEmpresa.IsReadOnly = false;
				colvarGeneradaEmpresa.DefaultSetting = @"";
				colvarGeneradaEmpresa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGeneradaEmpresa);
				
				TableSchema.TableColumn colvarFechaFactura = new TableSchema.TableColumn(schema);
				colvarFechaFactura.ColumnName = "FECHA_FACTURA";
				colvarFechaFactura.DataType = DbType.DateTime;
				colvarFechaFactura.MaxLength = 0;
				colvarFechaFactura.AutoIncrement = false;
				colvarFechaFactura.IsNullable = true;
				colvarFechaFactura.IsPrimaryKey = false;
				colvarFechaFactura.IsForeignKey = false;
				colvarFechaFactura.IsReadOnly = false;
				colvarFechaFactura.DefaultSetting = @"";
				colvarFechaFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaFactura);
				
				TableSchema.TableColumn colvarFechaAnulacion = new TableSchema.TableColumn(schema);
				colvarFechaAnulacion.ColumnName = "FECHA_ANULACION";
				colvarFechaAnulacion.DataType = DbType.DateTime;
				colvarFechaAnulacion.MaxLength = 0;
				colvarFechaAnulacion.AutoIncrement = false;
				colvarFechaAnulacion.IsNullable = true;
				colvarFechaAnulacion.IsPrimaryKey = false;
				colvarFechaAnulacion.IsForeignKey = false;
				colvarFechaAnulacion.IsReadOnly = false;
				colvarFechaAnulacion.DefaultSetting = @"";
				colvarFechaAnulacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaAnulacion);
				
				TableSchema.TableColumn colvarFechaPrefacturada = new TableSchema.TableColumn(schema);
				colvarFechaPrefacturada.ColumnName = "FECHA_PREFACTURADA";
				colvarFechaPrefacturada.DataType = DbType.DateTime;
				colvarFechaPrefacturada.MaxLength = 0;
				colvarFechaPrefacturada.AutoIncrement = false;
				colvarFechaPrefacturada.IsNullable = true;
				colvarFechaPrefacturada.IsPrimaryKey = false;
				colvarFechaPrefacturada.IsForeignKey = false;
				colvarFechaPrefacturada.IsReadOnly = false;
				colvarFechaPrefacturada.DefaultSetting = @"";
				colvarFechaPrefacturada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaPrefacturada);
				
				TableSchema.TableColumn colvarFechaGeneracionEmpresa = new TableSchema.TableColumn(schema);
				colvarFechaGeneracionEmpresa.ColumnName = "FECHA_GENERACION_EMPRESA";
				colvarFechaGeneracionEmpresa.DataType = DbType.DateTime;
				colvarFechaGeneracionEmpresa.MaxLength = 0;
				colvarFechaGeneracionEmpresa.AutoIncrement = false;
				colvarFechaGeneracionEmpresa.IsNullable = true;
				colvarFechaGeneracionEmpresa.IsPrimaryKey = false;
				colvarFechaGeneracionEmpresa.IsForeignKey = false;
				colvarFechaGeneracionEmpresa.IsReadOnly = false;
				colvarFechaGeneracionEmpresa.DefaultSetting = @"";
				colvarFechaGeneracionEmpresa.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaGeneracionEmpresa);
				
				TableSchema.TableColumn colvarValorEnLetras = new TableSchema.TableColumn(schema);
				colvarValorEnLetras.ColumnName = "VALOR_EN_LETRAS";
				colvarValorEnLetras.DataType = DbType.String;
				colvarValorEnLetras.MaxLength = 1000;
				colvarValorEnLetras.AutoIncrement = false;
				colvarValorEnLetras.IsNullable = false;
				colvarValorEnLetras.IsPrimaryKey = false;
				colvarValorEnLetras.IsForeignKey = false;
				colvarValorEnLetras.IsReadOnly = false;
				colvarValorEnLetras.DefaultSetting = @"";
				colvarValorEnLetras.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorEnLetras);
				
				TableSchema.TableColumn colvarDireccionCliente = new TableSchema.TableColumn(schema);
				colvarDireccionCliente.ColumnName = "DIRECCION_CLIENTE";
				colvarDireccionCliente.DataType = DbType.String;
				colvarDireccionCliente.MaxLength = 200;
				colvarDireccionCliente.AutoIncrement = false;
				colvarDireccionCliente.IsNullable = false;
				colvarDireccionCliente.IsPrimaryKey = false;
				colvarDireccionCliente.IsForeignKey = false;
				colvarDireccionCliente.IsReadOnly = false;
				colvarDireccionCliente.DefaultSetting = @"";
				colvarDireccionCliente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDireccionCliente);
				
				TableSchema.TableColumn colvarTelefonoCliente = new TableSchema.TableColumn(schema);
				colvarTelefonoCliente.ColumnName = "TELEFONO_CLIENTE";
				colvarTelefonoCliente.DataType = DbType.AnsiString;
				colvarTelefonoCliente.MaxLength = 50;
				colvarTelefonoCliente.AutoIncrement = false;
				colvarTelefonoCliente.IsNullable = false;
				colvarTelefonoCliente.IsPrimaryKey = false;
				colvarTelefonoCliente.IsForeignKey = false;
				colvarTelefonoCliente.IsReadOnly = false;
				colvarTelefonoCliente.DefaultSetting = @"";
				colvarTelefonoCliente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTelefonoCliente);
				
				TableSchema.TableColumn colvarFaxCliente = new TableSchema.TableColumn(schema);
				colvarFaxCliente.ColumnName = "FAX_CLIENTE";
				colvarFaxCliente.DataType = DbType.AnsiString;
				colvarFaxCliente.MaxLength = 50;
				colvarFaxCliente.AutoIncrement = false;
				colvarFaxCliente.IsNullable = true;
				colvarFaxCliente.IsPrimaryKey = false;
				colvarFaxCliente.IsForeignKey = false;
				colvarFaxCliente.IsReadOnly = false;
				colvarFaxCliente.DefaultSetting = @"";
				colvarFaxCliente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFaxCliente);
				
				TableSchema.TableColumn colvarIdCiudadCliente = new TableSchema.TableColumn(schema);
				colvarIdCiudadCliente.ColumnName = "ID_CIUDAD_CLIENTE";
				colvarIdCiudadCliente.DataType = DbType.Int32;
				colvarIdCiudadCliente.MaxLength = 0;
				colvarIdCiudadCliente.AutoIncrement = false;
				colvarIdCiudadCliente.IsNullable = false;
				colvarIdCiudadCliente.IsPrimaryKey = false;
				colvarIdCiudadCliente.IsForeignKey = true;
				colvarIdCiudadCliente.IsReadOnly = false;
				colvarIdCiudadCliente.DefaultSetting = @"";
				
					colvarIdCiudadCliente.ForeignKeyTableName = "INV_MAS_CIUDADES";
				schema.Columns.Add(colvarIdCiudadCliente);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarGuidCliente = new TableSchema.TableColumn(schema);
				colvarGuidCliente.ColumnName = "GUID_CLIENTE";
				colvarGuidCliente.DataType = DbType.String;
				colvarGuidCliente.MaxLength = 36;
				colvarGuidCliente.AutoIncrement = false;
				colvarGuidCliente.IsNullable = false;
				colvarGuidCliente.IsPrimaryKey = false;
				colvarGuidCliente.IsForeignKey = true;
				colvarGuidCliente.IsReadOnly = false;
				colvarGuidCliente.DefaultSetting = @"";
				
					colvarGuidCliente.ForeignKeyTableName = "INV_CLIENTES";
				schema.Columns.Add(colvarGuidCliente);
				
				TableSchema.TableColumn colvarNotaCredito = new TableSchema.TableColumn(schema);
				colvarNotaCredito.ColumnName = "NOTA_CREDITO";
				colvarNotaCredito.DataType = DbType.Boolean;
				colvarNotaCredito.MaxLength = 0;
				colvarNotaCredito.AutoIncrement = false;
				colvarNotaCredito.IsNullable = false;
				colvarNotaCredito.IsPrimaryKey = false;
				colvarNotaCredito.IsForeignKey = false;
				colvarNotaCredito.IsReadOnly = false;
				colvarNotaCredito.DefaultSetting = @"";
				colvarNotaCredito.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNotaCredito);
				
				TableSchema.TableColumn colvarSiniestro = new TableSchema.TableColumn(schema);
				colvarSiniestro.ColumnName = "SINIESTRO";
				colvarSiniestro.DataType = DbType.Boolean;
				colvarSiniestro.MaxLength = 0;
				colvarSiniestro.AutoIncrement = false;
				colvarSiniestro.IsNullable = false;
				colvarSiniestro.IsPrimaryKey = false;
				colvarSiniestro.IsForeignKey = false;
				colvarSiniestro.IsReadOnly = false;
				colvarSiniestro.DefaultSetting = @"";
				colvarSiniestro.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSiniestro);
				
				TableSchema.TableColumn colvarFechaCompra = new TableSchema.TableColumn(schema);
				colvarFechaCompra.ColumnName = "FECHA_COMPRA";
				colvarFechaCompra.DataType = DbType.DateTime;
				colvarFechaCompra.MaxLength = 0;
				colvarFechaCompra.AutoIncrement = false;
				colvarFechaCompra.IsNullable = true;
				colvarFechaCompra.IsPrimaryKey = false;
				colvarFechaCompra.IsForeignKey = false;
				colvarFechaCompra.IsReadOnly = false;
				colvarFechaCompra.DefaultSetting = @"";
				colvarFechaCompra.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaCompra);
				
				TableSchema.TableColumn colvarFacturaNotaCredito = new TableSchema.TableColumn(schema);
				colvarFacturaNotaCredito.ColumnName = "FACTURA_NOTA_CREDITO";
				colvarFacturaNotaCredito.DataType = DbType.Int32;
				colvarFacturaNotaCredito.MaxLength = 0;
				colvarFacturaNotaCredito.AutoIncrement = false;
				colvarFacturaNotaCredito.IsNullable = true;
				colvarFacturaNotaCredito.IsPrimaryKey = false;
				colvarFacturaNotaCredito.IsForeignKey = false;
				colvarFacturaNotaCredito.IsReadOnly = false;
				colvarFacturaNotaCredito.DefaultSetting = @"";
				colvarFacturaNotaCredito.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFacturaNotaCredito);
				
				TableSchema.TableColumn colvarMotivoNotaCredito = new TableSchema.TableColumn(schema);
				colvarMotivoNotaCredito.ColumnName = "MOTIVO_NOTA_CREDITO";
				colvarMotivoNotaCredito.DataType = DbType.String;
				colvarMotivoNotaCredito.MaxLength = 200;
				colvarMotivoNotaCredito.AutoIncrement = false;
				colvarMotivoNotaCredito.IsNullable = true;
				colvarMotivoNotaCredito.IsPrimaryKey = false;
				colvarMotivoNotaCredito.IsForeignKey = false;
				colvarMotivoNotaCredito.IsReadOnly = false;
				colvarMotivoNotaCredito.DefaultSetting = @"";
				colvarMotivoNotaCredito.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMotivoNotaCredito);
				
				TableSchema.TableColumn colvarFechaEntrega = new TableSchema.TableColumn(schema);
				colvarFechaEntrega.ColumnName = "FECHA_ENTREGA";
				colvarFechaEntrega.DataType = DbType.DateTime;
				colvarFechaEntrega.MaxLength = 0;
				colvarFechaEntrega.AutoIncrement = false;
				colvarFechaEntrega.IsNullable = true;
				colvarFechaEntrega.IsPrimaryKey = false;
				colvarFechaEntrega.IsForeignKey = false;
				colvarFechaEntrega.IsReadOnly = false;
				colvarFechaEntrega.DefaultSetting = @"";
				colvarFechaEntrega.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaEntrega);
				
				TableSchema.TableColumn colvarGeneradaEstablecimiento = new TableSchema.TableColumn(schema);
				colvarGeneradaEstablecimiento.ColumnName = "GENERADA_ESTABLECIMIENTO";
				colvarGeneradaEstablecimiento.DataType = DbType.Boolean;
				colvarGeneradaEstablecimiento.MaxLength = 0;
				colvarGeneradaEstablecimiento.AutoIncrement = false;
				colvarGeneradaEstablecimiento.IsNullable = true;
				colvarGeneradaEstablecimiento.IsPrimaryKey = false;
				colvarGeneradaEstablecimiento.IsForeignKey = false;
				colvarGeneradaEstablecimiento.IsReadOnly = false;
				colvarGeneradaEstablecimiento.DefaultSetting = @"";
				colvarGeneradaEstablecimiento.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGeneradaEstablecimiento);
				
				TableSchema.TableColumn colvarFechaGeneracionEstablecimiento = new TableSchema.TableColumn(schema);
				colvarFechaGeneracionEstablecimiento.ColumnName = "FECHA_GENERACION_ESTABLECIMIENTO";
				colvarFechaGeneracionEstablecimiento.DataType = DbType.DateTime;
				colvarFechaGeneracionEstablecimiento.MaxLength = 0;
				colvarFechaGeneracionEstablecimiento.AutoIncrement = false;
				colvarFechaGeneracionEstablecimiento.IsNullable = true;
				colvarFechaGeneracionEstablecimiento.IsPrimaryKey = false;
				colvarFechaGeneracionEstablecimiento.IsForeignKey = false;
				colvarFechaGeneracionEstablecimiento.IsReadOnly = false;
				colvarFechaGeneracionEstablecimiento.DefaultSetting = @"";
				colvarFechaGeneracionEstablecimiento.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaGeneracionEstablecimiento);
				
				TableSchema.TableColumn colvarFechaTransaccion = new TableSchema.TableColumn(schema);
				colvarFechaTransaccion.ColumnName = "FECHA_TRANSACCION";
				colvarFechaTransaccion.DataType = DbType.DateTime;
				colvarFechaTransaccion.MaxLength = 0;
				colvarFechaTransaccion.AutoIncrement = false;
				colvarFechaTransaccion.IsNullable = true;
				colvarFechaTransaccion.IsPrimaryKey = false;
				colvarFechaTransaccion.IsForeignKey = false;
				colvarFechaTransaccion.IsReadOnly = false;
				colvarFechaTransaccion.DefaultSetting = @"";
				colvarFechaTransaccion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaTransaccion);
				
				TableSchema.TableColumn colvarFechaVencimiento = new TableSchema.TableColumn(schema);
				colvarFechaVencimiento.ColumnName = "FECHA_VENCIMIENTO";
				colvarFechaVencimiento.DataType = DbType.DateTime;
				colvarFechaVencimiento.MaxLength = 0;
				colvarFechaVencimiento.AutoIncrement = false;
				colvarFechaVencimiento.IsNullable = false;
				colvarFechaVencimiento.IsPrimaryKey = false;
				colvarFechaVencimiento.IsForeignKey = false;
				colvarFechaVencimiento.IsReadOnly = false;
				
						colvarFechaVencimiento.DefaultSetting = @"(getdate())";
				colvarFechaVencimiento.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaVencimiento);
				
				TableSchema.TableColumn colvarObservaciones = new TableSchema.TableColumn(schema);
				colvarObservaciones.ColumnName = "OBSERVACIONES";
				colvarObservaciones.DataType = DbType.String;
				colvarObservaciones.MaxLength = 200;
				colvarObservaciones.AutoIncrement = false;
				colvarObservaciones.IsNullable = true;
				colvarObservaciones.IsPrimaryKey = false;
				colvarObservaciones.IsForeignKey = false;
				colvarObservaciones.IsReadOnly = false;
				colvarObservaciones.DefaultSetting = @"";
				colvarObservaciones.ForeignKeyTableName = "";
				schema.Columns.Add(colvarObservaciones);
				
				TableSchema.TableColumn colvarBitNotaCreditoValor = new TableSchema.TableColumn(schema);
				colvarBitNotaCreditoValor.ColumnName = "BIT_NOTA_CREDITO_VALOR";
				colvarBitNotaCreditoValor.DataType = DbType.Boolean;
				colvarBitNotaCreditoValor.MaxLength = 0;
				colvarBitNotaCreditoValor.AutoIncrement = false;
				colvarBitNotaCreditoValor.IsNullable = false;
				colvarBitNotaCreditoValor.IsPrimaryKey = false;
				colvarBitNotaCreditoValor.IsForeignKey = false;
				colvarBitNotaCreditoValor.IsReadOnly = false;
				
						colvarBitNotaCreditoValor.DefaultSetting = @"((0))";
				colvarBitNotaCreditoValor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBitNotaCreditoValor);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_FACTURAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidFactura")]
		[Bindable(true)]
		public string GuidFactura 
		{
			get { return GetColumnValue<string>(Columns.GuidFactura); }
			set { SetColumnValue(Columns.GuidFactura, value); }
		}
		  
		[XmlAttribute("NumeroFactura")]
		[Bindable(true)]
		public int? NumeroFactura 
		{
			get { return GetColumnValue<int?>(Columns.NumeroFactura); }
			set { SetColumnValue(Columns.NumeroFactura, value); }
		}
		  
		[XmlAttribute("TotalBase")]
		[Bindable(true)]
		public decimal TotalBase 
		{
			get { return GetColumnValue<decimal>(Columns.TotalBase); }
			set { SetColumnValue(Columns.TotalBase, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("Prefacturada")]
		[Bindable(true)]
		public bool Prefacturada 
		{
			get { return GetColumnValue<bool>(Columns.Prefacturada); }
			set { SetColumnValue(Columns.Prefacturada, value); }
		}
		  
		[XmlAttribute("Anulada")]
		[Bindable(true)]
		public bool Anulada 
		{
			get { return GetColumnValue<bool>(Columns.Anulada); }
			set { SetColumnValue(Columns.Anulada, value); }
		}
		  
		[XmlAttribute("GeneradaEmpresa")]
		[Bindable(true)]
		public bool GeneradaEmpresa 
		{
			get { return GetColumnValue<bool>(Columns.GeneradaEmpresa); }
			set { SetColumnValue(Columns.GeneradaEmpresa, value); }
		}
		  
		[XmlAttribute("FechaFactura")]
		[Bindable(true)]
		public DateTime? FechaFactura 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaFactura); }
			set { SetColumnValue(Columns.FechaFactura, value); }
		}
		  
		[XmlAttribute("FechaAnulacion")]
		[Bindable(true)]
		public DateTime? FechaAnulacion 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaAnulacion); }
			set { SetColumnValue(Columns.FechaAnulacion, value); }
		}
		  
		[XmlAttribute("FechaPrefacturada")]
		[Bindable(true)]
		public DateTime? FechaPrefacturada 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaPrefacturada); }
			set { SetColumnValue(Columns.FechaPrefacturada, value); }
		}
		  
		[XmlAttribute("FechaGeneracionEmpresa")]
		[Bindable(true)]
		public DateTime? FechaGeneracionEmpresa 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaGeneracionEmpresa); }
			set { SetColumnValue(Columns.FechaGeneracionEmpresa, value); }
		}
		  
		[XmlAttribute("ValorEnLetras")]
		[Bindable(true)]
		public string ValorEnLetras 
		{
			get { return GetColumnValue<string>(Columns.ValorEnLetras); }
			set { SetColumnValue(Columns.ValorEnLetras, value); }
		}
		  
		[XmlAttribute("DireccionCliente")]
		[Bindable(true)]
		public string DireccionCliente 
		{
			get { return GetColumnValue<string>(Columns.DireccionCliente); }
			set { SetColumnValue(Columns.DireccionCliente, value); }
		}
		  
		[XmlAttribute("TelefonoCliente")]
		[Bindable(true)]
		public string TelefonoCliente 
		{
			get { return GetColumnValue<string>(Columns.TelefonoCliente); }
			set { SetColumnValue(Columns.TelefonoCliente, value); }
		}
		  
		[XmlAttribute("FaxCliente")]
		[Bindable(true)]
		public string FaxCliente 
		{
			get { return GetColumnValue<string>(Columns.FaxCliente); }
			set { SetColumnValue(Columns.FaxCliente, value); }
		}
		  
		[XmlAttribute("IdCiudadCliente")]
		[Bindable(true)]
		public int IdCiudadCliente 
		{
			get { return GetColumnValue<int>(Columns.IdCiudadCliente); }
			set { SetColumnValue(Columns.IdCiudadCliente, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("GuidCliente")]
		[Bindable(true)]
		public string GuidCliente 
		{
			get { return GetColumnValue<string>(Columns.GuidCliente); }
			set { SetColumnValue(Columns.GuidCliente, value); }
		}
		  
		[XmlAttribute("NotaCredito")]
		[Bindable(true)]
		public bool NotaCredito 
		{
			get { return GetColumnValue<bool>(Columns.NotaCredito); }
			set { SetColumnValue(Columns.NotaCredito, value); }
		}
		  
		[XmlAttribute("Siniestro")]
		[Bindable(true)]
		public bool Siniestro 
		{
			get { return GetColumnValue<bool>(Columns.Siniestro); }
			set { SetColumnValue(Columns.Siniestro, value); }
		}
		  
		[XmlAttribute("FechaCompra")]
		[Bindable(true)]
		public DateTime? FechaCompra 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaCompra); }
			set { SetColumnValue(Columns.FechaCompra, value); }
		}
		  
		[XmlAttribute("FacturaNotaCredito")]
		[Bindable(true)]
		public int? FacturaNotaCredito 
		{
			get { return GetColumnValue<int?>(Columns.FacturaNotaCredito); }
			set { SetColumnValue(Columns.FacturaNotaCredito, value); }
		}
		  
		[XmlAttribute("MotivoNotaCredito")]
		[Bindable(true)]
		public string MotivoNotaCredito 
		{
			get { return GetColumnValue<string>(Columns.MotivoNotaCredito); }
			set { SetColumnValue(Columns.MotivoNotaCredito, value); }
		}
		  
		[XmlAttribute("FechaEntrega")]
		[Bindable(true)]
		public DateTime? FechaEntrega 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaEntrega); }
			set { SetColumnValue(Columns.FechaEntrega, value); }
		}
		  
		[XmlAttribute("GeneradaEstablecimiento")]
		[Bindable(true)]
		public bool? GeneradaEstablecimiento 
		{
			get { return GetColumnValue<bool?>(Columns.GeneradaEstablecimiento); }
			set { SetColumnValue(Columns.GeneradaEstablecimiento, value); }
		}
		  
		[XmlAttribute("FechaGeneracionEstablecimiento")]
		[Bindable(true)]
		public DateTime? FechaGeneracionEstablecimiento 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaGeneracionEstablecimiento); }
			set { SetColumnValue(Columns.FechaGeneracionEstablecimiento, value); }
		}
		  
		[XmlAttribute("FechaTransaccion")]
		[Bindable(true)]
		public DateTime? FechaTransaccion 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaTransaccion); }
			set { SetColumnValue(Columns.FechaTransaccion, value); }
		}
		  
		[XmlAttribute("FechaVencimiento")]
		[Bindable(true)]
		public DateTime FechaVencimiento 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaVencimiento); }
			set { SetColumnValue(Columns.FechaVencimiento, value); }
		}
		  
		[XmlAttribute("Observaciones")]
		[Bindable(true)]
		public string Observaciones 
		{
			get { return GetColumnValue<string>(Columns.Observaciones); }
			set { SetColumnValue(Columns.Observaciones, value); }
		}
		  
		[XmlAttribute("BitNotaCreditoValor")]
		[Bindable(true)]
		public bool BitNotaCreditoValor 
		{
			get { return GetColumnValue<bool>(Columns.BitNotaCreditoValor); }
			set { SetColumnValue(Columns.BitNotaCreditoValor, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaCollection DetallesFacturaRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaCollection().Where(DetallesFactura.Columns.GuidFactura, GuidFactura).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Clientes ActiveRecord object related to this Facturas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Clientes Clientes
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Clientes.FetchByID(this.GuidCliente); }
			set { SetColumnValue("GUID_CLIENTE", value.GuidCliente); }
		}
		
		
		/// <summary>
		/// Returns a MasCiudades ActiveRecord object related to this Facturas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasCiudades MasCiudades
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasCiudades.FetchByID(this.IdCiudadCliente); }
			set { SetColumnValue("ID_CIUDAD_CLIENTE", value.IdCiudad); }
		}
		
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this Facturas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidFactura,int? varNumeroFactura,decimal varTotalBase,decimal varIva,bool varPrefacturada,bool varAnulada,bool varGeneradaEmpresa,DateTime? varFechaFactura,DateTime? varFechaAnulacion,DateTime? varFechaPrefacturada,DateTime? varFechaGeneracionEmpresa,string varValorEnLetras,string varDireccionCliente,string varTelefonoCliente,string varFaxCliente,int varIdCiudadCliente,int varIdPrograma,string varGuidCliente,bool varNotaCredito,bool varSiniestro,DateTime? varFechaCompra,int? varFacturaNotaCredito,string varMotivoNotaCredito,DateTime? varFechaEntrega,bool? varGeneradaEstablecimiento,DateTime? varFechaGeneracionEstablecimiento,DateTime? varFechaTransaccion,DateTime varFechaVencimiento,string varObservaciones,bool varBitNotaCreditoValor)
		{
			Facturas item = new Facturas();
			
			item.GuidFactura = varGuidFactura;
			
			item.NumeroFactura = varNumeroFactura;
			
			item.TotalBase = varTotalBase;
			
			item.Iva = varIva;
			
			item.Prefacturada = varPrefacturada;
			
			item.Anulada = varAnulada;
			
			item.GeneradaEmpresa = varGeneradaEmpresa;
			
			item.FechaFactura = varFechaFactura;
			
			item.FechaAnulacion = varFechaAnulacion;
			
			item.FechaPrefacturada = varFechaPrefacturada;
			
			item.FechaGeneracionEmpresa = varFechaGeneracionEmpresa;
			
			item.ValorEnLetras = varValorEnLetras;
			
			item.DireccionCliente = varDireccionCliente;
			
			item.TelefonoCliente = varTelefonoCliente;
			
			item.FaxCliente = varFaxCliente;
			
			item.IdCiudadCliente = varIdCiudadCliente;
			
			item.IdPrograma = varIdPrograma;
			
			item.GuidCliente = varGuidCliente;
			
			item.NotaCredito = varNotaCredito;
			
			item.Siniestro = varSiniestro;
			
			item.FechaCompra = varFechaCompra;
			
			item.FacturaNotaCredito = varFacturaNotaCredito;
			
			item.MotivoNotaCredito = varMotivoNotaCredito;
			
			item.FechaEntrega = varFechaEntrega;
			
			item.GeneradaEstablecimiento = varGeneradaEstablecimiento;
			
			item.FechaGeneracionEstablecimiento = varFechaGeneracionEstablecimiento;
			
			item.FechaTransaccion = varFechaTransaccion;
			
			item.FechaVencimiento = varFechaVencimiento;
			
			item.Observaciones = varObservaciones;
			
			item.BitNotaCreditoValor = varBitNotaCreditoValor;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidFactura,int? varNumeroFactura,decimal varTotalBase,decimal varIva,bool varPrefacturada,bool varAnulada,bool varGeneradaEmpresa,DateTime? varFechaFactura,DateTime? varFechaAnulacion,DateTime? varFechaPrefacturada,DateTime? varFechaGeneracionEmpresa,string varValorEnLetras,string varDireccionCliente,string varTelefonoCliente,string varFaxCliente,int varIdCiudadCliente,int varIdPrograma,string varGuidCliente,bool varNotaCredito,bool varSiniestro,DateTime? varFechaCompra,int? varFacturaNotaCredito,string varMotivoNotaCredito,DateTime? varFechaEntrega,bool? varGeneradaEstablecimiento,DateTime? varFechaGeneracionEstablecimiento,DateTime? varFechaTransaccion,DateTime varFechaVencimiento,string varObservaciones,bool varBitNotaCreditoValor)
		{
			Facturas item = new Facturas();
			
				item.GuidFactura = varGuidFactura;
			
				item.NumeroFactura = varNumeroFactura;
			
				item.TotalBase = varTotalBase;
			
				item.Iva = varIva;
			
				item.Prefacturada = varPrefacturada;
			
				item.Anulada = varAnulada;
			
				item.GeneradaEmpresa = varGeneradaEmpresa;
			
				item.FechaFactura = varFechaFactura;
			
				item.FechaAnulacion = varFechaAnulacion;
			
				item.FechaPrefacturada = varFechaPrefacturada;
			
				item.FechaGeneracionEmpresa = varFechaGeneracionEmpresa;
			
				item.ValorEnLetras = varValorEnLetras;
			
				item.DireccionCliente = varDireccionCliente;
			
				item.TelefonoCliente = varTelefonoCliente;
			
				item.FaxCliente = varFaxCliente;
			
				item.IdCiudadCliente = varIdCiudadCliente;
			
				item.IdPrograma = varIdPrograma;
			
				item.GuidCliente = varGuidCliente;
			
				item.NotaCredito = varNotaCredito;
			
				item.Siniestro = varSiniestro;
			
				item.FechaCompra = varFechaCompra;
			
				item.FacturaNotaCredito = varFacturaNotaCredito;
			
				item.MotivoNotaCredito = varMotivoNotaCredito;
			
				item.FechaEntrega = varFechaEntrega;
			
				item.GeneradaEstablecimiento = varGeneradaEstablecimiento;
			
				item.FechaGeneracionEstablecimiento = varFechaGeneracionEstablecimiento;
			
				item.FechaTransaccion = varFechaTransaccion;
			
				item.FechaVencimiento = varFechaVencimiento;
			
				item.Observaciones = varObservaciones;
			
				item.BitNotaCreditoValor = varBitNotaCreditoValor;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidFacturaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroFacturaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalBaseColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn PrefacturadaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn AnuladaColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn GeneradaEmpresaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaFacturaColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaAnulacionColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaPrefacturadaColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaGeneracionEmpresaColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorEnLetrasColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn DireccionClienteColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn TelefonoClienteColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn FaxClienteColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCiudadClienteColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidClienteColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        public static TableSchema.TableColumn NotaCreditoColumn
        {
            get { return Schema.Columns[18]; }
        }
        
        
        
        public static TableSchema.TableColumn SiniestroColumn
        {
            get { return Schema.Columns[19]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaCompraColumn
        {
            get { return Schema.Columns[20]; }
        }
        
        
        
        public static TableSchema.TableColumn FacturaNotaCreditoColumn
        {
            get { return Schema.Columns[21]; }
        }
        
        
        
        public static TableSchema.TableColumn MotivoNotaCreditoColumn
        {
            get { return Schema.Columns[22]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaEntregaColumn
        {
            get { return Schema.Columns[23]; }
        }
        
        
        
        public static TableSchema.TableColumn GeneradaEstablecimientoColumn
        {
            get { return Schema.Columns[24]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaGeneracionEstablecimientoColumn
        {
            get { return Schema.Columns[25]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaTransaccionColumn
        {
            get { return Schema.Columns[26]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaVencimientoColumn
        {
            get { return Schema.Columns[27]; }
        }
        
        
        
        public static TableSchema.TableColumn ObservacionesColumn
        {
            get { return Schema.Columns[28]; }
        }
        
        
        
        public static TableSchema.TableColumn BitNotaCreditoValorColumn
        {
            get { return Schema.Columns[29]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidFactura = @"GUID_FACTURA";
			 public static string NumeroFactura = @"NUMERO_FACTURA";
			 public static string TotalBase = @"TOTAL_BASE";
			 public static string Iva = @"IVA";
			 public static string Prefacturada = @"PREFACTURADA";
			 public static string Anulada = @"ANULADA";
			 public static string GeneradaEmpresa = @"GENERADA_EMPRESA";
			 public static string FechaFactura = @"FECHA_FACTURA";
			 public static string FechaAnulacion = @"FECHA_ANULACION";
			 public static string FechaPrefacturada = @"FECHA_PREFACTURADA";
			 public static string FechaGeneracionEmpresa = @"FECHA_GENERACION_EMPRESA";
			 public static string ValorEnLetras = @"VALOR_EN_LETRAS";
			 public static string DireccionCliente = @"DIRECCION_CLIENTE";
			 public static string TelefonoCliente = @"TELEFONO_CLIENTE";
			 public static string FaxCliente = @"FAX_CLIENTE";
			 public static string IdCiudadCliente = @"ID_CIUDAD_CLIENTE";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string GuidCliente = @"GUID_CLIENTE";
			 public static string NotaCredito = @"NOTA_CREDITO";
			 public static string Siniestro = @"SINIESTRO";
			 public static string FechaCompra = @"FECHA_COMPRA";
			 public static string FacturaNotaCredito = @"FACTURA_NOTA_CREDITO";
			 public static string MotivoNotaCredito = @"MOTIVO_NOTA_CREDITO";
			 public static string FechaEntrega = @"FECHA_ENTREGA";
			 public static string GeneradaEstablecimiento = @"GENERADA_ESTABLECIMIENTO";
			 public static string FechaGeneracionEstablecimiento = @"FECHA_GENERACION_ESTABLECIMIENTO";
			 public static string FechaTransaccion = @"FECHA_TRANSACCION";
			 public static string FechaVencimiento = @"FECHA_VENCIMIENTO";
			 public static string Observaciones = @"OBSERVACIONES";
			 public static string BitNotaCreditoValor = @"BIT_NOTA_CREDITO_VALOR";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
