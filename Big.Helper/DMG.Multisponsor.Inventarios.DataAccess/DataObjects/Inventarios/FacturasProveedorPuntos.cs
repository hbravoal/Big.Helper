using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the FacturasProveedorPuntos class.
	/// </summary>
    [Serializable]
	public partial class FacturasProveedorPuntosCollection : ActiveList<FacturasProveedorPuntos, FacturasProveedorPuntosCollection>
	{	   
		public FacturasProveedorPuntosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FacturasProveedorPuntosCollection</returns>
		public FacturasProveedorPuntosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FacturasProveedorPuntos o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_FACTURAS_PROVEEDOR_PUNTOS table.
	/// </summary>
	[Serializable]
	public partial class FacturasProveedorPuntos : ActiveRecord<FacturasProveedorPuntos>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public FacturasProveedorPuntos()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FacturasProveedorPuntos(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public FacturasProveedorPuntos(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public FacturasProveedorPuntos(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_FACTURAS_PROVEEDOR_PUNTOS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidFacturaProveedorPuntos = new TableSchema.TableColumn(schema);
				colvarGuidFacturaProveedorPuntos.ColumnName = "GUID_FACTURA_PROVEEDOR_PUNTOS";
				colvarGuidFacturaProveedorPuntos.DataType = DbType.String;
				colvarGuidFacturaProveedorPuntos.MaxLength = 36;
				colvarGuidFacturaProveedorPuntos.AutoIncrement = false;
				colvarGuidFacturaProveedorPuntos.IsNullable = false;
				colvarGuidFacturaProveedorPuntos.IsPrimaryKey = true;
				colvarGuidFacturaProveedorPuntos.IsForeignKey = false;
				colvarGuidFacturaProveedorPuntos.IsReadOnly = false;
				colvarGuidFacturaProveedorPuntos.DefaultSetting = @"";
				colvarGuidFacturaProveedorPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidFacturaProveedorPuntos);
				
				TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
				colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
				colvarNumeroFactura.DataType = DbType.Int32;
				colvarNumeroFactura.MaxLength = 0;
				colvarNumeroFactura.AutoIncrement = false;
				colvarNumeroFactura.IsNullable = false;
				colvarNumeroFactura.IsPrimaryKey = false;
				colvarNumeroFactura.IsForeignKey = false;
				colvarNumeroFactura.IsReadOnly = false;
				colvarNumeroFactura.DefaultSetting = @"";
				colvarNumeroFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroFactura);
				
				TableSchema.TableColumn colvarTotalBase = new TableSchema.TableColumn(schema);
				colvarTotalBase.ColumnName = "TOTAL_BASE";
				colvarTotalBase.DataType = DbType.Decimal;
				colvarTotalBase.MaxLength = 0;
				colvarTotalBase.AutoIncrement = false;
				colvarTotalBase.IsNullable = false;
				colvarTotalBase.IsPrimaryKey = false;
				colvarTotalBase.IsForeignKey = false;
				colvarTotalBase.IsReadOnly = false;
				colvarTotalBase.DefaultSetting = @"";
				colvarTotalBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalBase);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarFechaFactura = new TableSchema.TableColumn(schema);
				colvarFechaFactura.ColumnName = "FECHA_FACTURA";
				colvarFechaFactura.DataType = DbType.DateTime;
				colvarFechaFactura.MaxLength = 0;
				colvarFechaFactura.AutoIncrement = false;
				colvarFechaFactura.IsNullable = false;
				colvarFechaFactura.IsPrimaryKey = false;
				colvarFechaFactura.IsForeignKey = false;
				colvarFechaFactura.IsReadOnly = false;
				colvarFechaFactura.DefaultSetting = @"";
				colvarFechaFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaFactura);
				
				TableSchema.TableColumn colvarValorEnLetras = new TableSchema.TableColumn(schema);
				colvarValorEnLetras.ColumnName = "VALOR_EN_LETRAS";
				colvarValorEnLetras.DataType = DbType.String;
				colvarValorEnLetras.MaxLength = 200;
				colvarValorEnLetras.AutoIncrement = false;
				colvarValorEnLetras.IsNullable = false;
				colvarValorEnLetras.IsPrimaryKey = false;
				colvarValorEnLetras.IsForeignKey = false;
				colvarValorEnLetras.IsReadOnly = false;
				colvarValorEnLetras.DefaultSetting = @"";
				colvarValorEnLetras.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorEnLetras);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdEmpresa = new TableSchema.TableColumn(schema);
				colvarIdEmpresa.ColumnName = "ID_EMPRESA";
				colvarIdEmpresa.DataType = DbType.Int32;
				colvarIdEmpresa.MaxLength = 0;
				colvarIdEmpresa.AutoIncrement = false;
				colvarIdEmpresa.IsNullable = false;
				colvarIdEmpresa.IsPrimaryKey = false;
				colvarIdEmpresa.IsForeignKey = true;
				colvarIdEmpresa.IsReadOnly = false;
				colvarIdEmpresa.DefaultSetting = @"";
				
					colvarIdEmpresa.ForeignKeyTableName = "INV_EMPRESAS";
				schema.Columns.Add(colvarIdEmpresa);
				
				TableSchema.TableColumn colvarIdPeriodoFacturacionPuntos = new TableSchema.TableColumn(schema);
				colvarIdPeriodoFacturacionPuntos.ColumnName = "ID_PERIODO_FACTURACION_PUNTOS";
				colvarIdPeriodoFacturacionPuntos.DataType = DbType.Int32;
				colvarIdPeriodoFacturacionPuntos.MaxLength = 0;
				colvarIdPeriodoFacturacionPuntos.AutoIncrement = false;
				colvarIdPeriodoFacturacionPuntos.IsNullable = true;
				colvarIdPeriodoFacturacionPuntos.IsPrimaryKey = false;
				colvarIdPeriodoFacturacionPuntos.IsForeignKey = true;
				colvarIdPeriodoFacturacionPuntos.IsReadOnly = false;
				colvarIdPeriodoFacturacionPuntos.DefaultSetting = @"";
				
					colvarIdPeriodoFacturacionPuntos.ForeignKeyTableName = "INV_PERIODO_FACTURACION_PUNTOS";
				schema.Columns.Add(colvarIdPeriodoFacturacionPuntos);
				
				TableSchema.TableColumn colvarNotaCreditoProveedorPuntos = new TableSchema.TableColumn(schema);
				colvarNotaCreditoProveedorPuntos.ColumnName = "NOTA_CREDITO_PROVEEDOR_PUNTOS";
				colvarNotaCreditoProveedorPuntos.DataType = DbType.Boolean;
				colvarNotaCreditoProveedorPuntos.MaxLength = 0;
				colvarNotaCreditoProveedorPuntos.AutoIncrement = false;
				colvarNotaCreditoProveedorPuntos.IsNullable = false;
				colvarNotaCreditoProveedorPuntos.IsPrimaryKey = false;
				colvarNotaCreditoProveedorPuntos.IsForeignKey = false;
				colvarNotaCreditoProveedorPuntos.IsReadOnly = false;
				
						colvarNotaCreditoProveedorPuntos.DefaultSetting = @"((0))";
				colvarNotaCreditoProveedorPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNotaCreditoProveedorPuntos);
				
				TableSchema.TableColumn colvarAnulada = new TableSchema.TableColumn(schema);
				colvarAnulada.ColumnName = "ANULADA";
				colvarAnulada.DataType = DbType.Boolean;
				colvarAnulada.MaxLength = 0;
				colvarAnulada.AutoIncrement = false;
				colvarAnulada.IsNullable = true;
				colvarAnulada.IsPrimaryKey = false;
				colvarAnulada.IsForeignKey = false;
				colvarAnulada.IsReadOnly = false;
				colvarAnulada.DefaultSetting = @"";
				colvarAnulada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarAnulada);
				
				TableSchema.TableColumn colvarFechaAnulacion = new TableSchema.TableColumn(schema);
				colvarFechaAnulacion.ColumnName = "FECHA_ANULACION";
				colvarFechaAnulacion.DataType = DbType.DateTime;
				colvarFechaAnulacion.MaxLength = 0;
				colvarFechaAnulacion.AutoIncrement = false;
				colvarFechaAnulacion.IsNullable = true;
				colvarFechaAnulacion.IsPrimaryKey = false;
				colvarFechaAnulacion.IsForeignKey = false;
				colvarFechaAnulacion.IsReadOnly = false;
				colvarFechaAnulacion.DefaultSetting = @"";
				colvarFechaAnulacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaAnulacion);
				
				TableSchema.TableColumn colvarTipoCurrier = new TableSchema.TableColumn(schema);
				colvarTipoCurrier.ColumnName = "TIPO_CURRIER";
				colvarTipoCurrier.DataType = DbType.Boolean;
				colvarTipoCurrier.MaxLength = 0;
				colvarTipoCurrier.AutoIncrement = false;
				colvarTipoCurrier.IsNullable = false;
				colvarTipoCurrier.IsPrimaryKey = false;
				colvarTipoCurrier.IsForeignKey = false;
				colvarTipoCurrier.IsReadOnly = false;
				
						colvarTipoCurrier.DefaultSetting = @"((0))";
				colvarTipoCurrier.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTipoCurrier);
				
				TableSchema.TableColumn colvarObservaciones = new TableSchema.TableColumn(schema);
				colvarObservaciones.ColumnName = "OBSERVACIONES";
				colvarObservaciones.DataType = DbType.String;
				colvarObservaciones.MaxLength = 1000;
				colvarObservaciones.AutoIncrement = false;
				colvarObservaciones.IsNullable = true;
				colvarObservaciones.IsPrimaryKey = false;
				colvarObservaciones.IsForeignKey = false;
				colvarObservaciones.IsReadOnly = false;
				colvarObservaciones.DefaultSetting = @"";
				colvarObservaciones.ForeignKeyTableName = "";
				schema.Columns.Add(colvarObservaciones);
				
				TableSchema.TableColumn colvarMotivoNotaCredito = new TableSchema.TableColumn(schema);
				colvarMotivoNotaCredito.ColumnName = "MOTIVO_NOTA_CREDITO";
				colvarMotivoNotaCredito.DataType = DbType.String;
				colvarMotivoNotaCredito.MaxLength = 200;
				colvarMotivoNotaCredito.AutoIncrement = false;
				colvarMotivoNotaCredito.IsNullable = true;
				colvarMotivoNotaCredito.IsPrimaryKey = false;
				colvarMotivoNotaCredito.IsForeignKey = false;
				colvarMotivoNotaCredito.IsReadOnly = false;
				colvarMotivoNotaCredito.DefaultSetting = @"";
				colvarMotivoNotaCredito.ForeignKeyTableName = "";
				schema.Columns.Add(colvarMotivoNotaCredito);
				
				TableSchema.TableColumn colvarFacturaNotaCredito = new TableSchema.TableColumn(schema);
				colvarFacturaNotaCredito.ColumnName = "FACTURA_NOTA_CREDITO";
				colvarFacturaNotaCredito.DataType = DbType.Int32;
				colvarFacturaNotaCredito.MaxLength = 0;
				colvarFacturaNotaCredito.AutoIncrement = false;
				colvarFacturaNotaCredito.IsNullable = true;
				colvarFacturaNotaCredito.IsPrimaryKey = false;
				colvarFacturaNotaCredito.IsForeignKey = false;
				colvarFacturaNotaCredito.IsReadOnly = false;
				colvarFacturaNotaCredito.DefaultSetting = @"";
				colvarFacturaNotaCredito.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFacturaNotaCredito);
				
				TableSchema.TableColumn colvarNotaCredito = new TableSchema.TableColumn(schema);
				colvarNotaCredito.ColumnName = "NOTA_CREDITO";
				colvarNotaCredito.DataType = DbType.Boolean;
				colvarNotaCredito.MaxLength = 0;
				colvarNotaCredito.AutoIncrement = false;
				colvarNotaCredito.IsNullable = true;
				colvarNotaCredito.IsPrimaryKey = false;
				colvarNotaCredito.IsForeignKey = false;
				colvarNotaCredito.IsReadOnly = false;
				colvarNotaCredito.DefaultSetting = @"";
				colvarNotaCredito.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNotaCredito);
				
				TableSchema.TableColumn colvarBitNotaCreditoValor = new TableSchema.TableColumn(schema);
				colvarBitNotaCreditoValor.ColumnName = "BIT_NOTA_CREDITO_VALOR";
				colvarBitNotaCreditoValor.DataType = DbType.Boolean;
				colvarBitNotaCreditoValor.MaxLength = 0;
				colvarBitNotaCreditoValor.AutoIncrement = false;
				colvarBitNotaCreditoValor.IsNullable = false;
				colvarBitNotaCreditoValor.IsPrimaryKey = false;
				colvarBitNotaCreditoValor.IsForeignKey = false;
				colvarBitNotaCreditoValor.IsReadOnly = false;
				
						colvarBitNotaCreditoValor.DefaultSetting = @"((0))";
				colvarBitNotaCreditoValor.ForeignKeyTableName = "";
				schema.Columns.Add(colvarBitNotaCreditoValor);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_FACTURAS_PROVEEDOR_PUNTOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidFacturaProveedorPuntos")]
		[Bindable(true)]
		public string GuidFacturaProveedorPuntos 
		{
			get { return GetColumnValue<string>(Columns.GuidFacturaProveedorPuntos); }
			set { SetColumnValue(Columns.GuidFacturaProveedorPuntos, value); }
		}
		  
		[XmlAttribute("NumeroFactura")]
		[Bindable(true)]
		public int NumeroFactura 
		{
			get { return GetColumnValue<int>(Columns.NumeroFactura); }
			set { SetColumnValue(Columns.NumeroFactura, value); }
		}
		  
		[XmlAttribute("TotalBase")]
		[Bindable(true)]
		public decimal TotalBase 
		{
			get { return GetColumnValue<decimal>(Columns.TotalBase); }
			set { SetColumnValue(Columns.TotalBase, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("FechaFactura")]
		[Bindable(true)]
		public DateTime FechaFactura 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaFactura); }
			set { SetColumnValue(Columns.FechaFactura, value); }
		}
		  
		[XmlAttribute("ValorEnLetras")]
		[Bindable(true)]
		public string ValorEnLetras 
		{
			get { return GetColumnValue<string>(Columns.ValorEnLetras); }
			set { SetColumnValue(Columns.ValorEnLetras, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdEmpresa")]
		[Bindable(true)]
		public int IdEmpresa 
		{
			get { return GetColumnValue<int>(Columns.IdEmpresa); }
			set { SetColumnValue(Columns.IdEmpresa, value); }
		}
		  
		[XmlAttribute("IdPeriodoFacturacionPuntos")]
		[Bindable(true)]
		public int? IdPeriodoFacturacionPuntos 
		{
			get { return GetColumnValue<int?>(Columns.IdPeriodoFacturacionPuntos); }
			set { SetColumnValue(Columns.IdPeriodoFacturacionPuntos, value); }
		}
		  
		[XmlAttribute("NotaCreditoProveedorPuntos")]
		[Bindable(true)]
		public bool NotaCreditoProveedorPuntos 
		{
			get { return GetColumnValue<bool>(Columns.NotaCreditoProveedorPuntos); }
			set { SetColumnValue(Columns.NotaCreditoProveedorPuntos, value); }
		}
		  
		[XmlAttribute("Anulada")]
		[Bindable(true)]
		public bool? Anulada 
		{
			get { return GetColumnValue<bool?>(Columns.Anulada); }
			set { SetColumnValue(Columns.Anulada, value); }
		}
		  
		[XmlAttribute("FechaAnulacion")]
		[Bindable(true)]
		public DateTime? FechaAnulacion 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaAnulacion); }
			set { SetColumnValue(Columns.FechaAnulacion, value); }
		}
		  
		[XmlAttribute("TipoCurrier")]
		[Bindable(true)]
		public bool TipoCurrier 
		{
			get { return GetColumnValue<bool>(Columns.TipoCurrier); }
			set { SetColumnValue(Columns.TipoCurrier, value); }
		}
		  
		[XmlAttribute("Observaciones")]
		[Bindable(true)]
		public string Observaciones 
		{
			get { return GetColumnValue<string>(Columns.Observaciones); }
			set { SetColumnValue(Columns.Observaciones, value); }
		}
		  
		[XmlAttribute("MotivoNotaCredito")]
		[Bindable(true)]
		public string MotivoNotaCredito 
		{
			get { return GetColumnValue<string>(Columns.MotivoNotaCredito); }
			set { SetColumnValue(Columns.MotivoNotaCredito, value); }
		}
		  
		[XmlAttribute("FacturaNotaCredito")]
		[Bindable(true)]
		public int? FacturaNotaCredito 
		{
			get { return GetColumnValue<int?>(Columns.FacturaNotaCredito); }
			set { SetColumnValue(Columns.FacturaNotaCredito, value); }
		}
		  
		[XmlAttribute("NotaCredito")]
		[Bindable(true)]
		public bool? NotaCredito 
		{
			get { return GetColumnValue<bool?>(Columns.NotaCredito); }
			set { SetColumnValue(Columns.NotaCredito, value); }
		}
		  
		[XmlAttribute("BitNotaCreditoValor")]
		[Bindable(true)]
		public bool BitNotaCreditoValor 
		{
			get { return GetColumnValue<bool>(Columns.BitNotaCreditoValor); }
			set { SetColumnValue(Columns.BitNotaCreditoValor, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaProveedorPuntosCollection DetallesFacturaProveedorPuntosRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaProveedorPuntosCollection().Where(DetallesFacturaProveedorPuntos.Columns.GuidFacturaProveedorPuntos, GuidFacturaProveedorPuntos).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Empresas ActiveRecord object related to this FacturasProveedorPuntos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Empresas Empresas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Empresas.FetchByID(this.IdEmpresa); }
			set { SetColumnValue("ID_EMPRESA", value.IdEmpresa); }
		}
		
		
		/// <summary>
		/// Returns a PeriodoFacturacionPuntos ActiveRecord object related to this FacturasProveedorPuntos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.PeriodoFacturacionPuntos PeriodoFacturacionPuntos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.PeriodoFacturacionPuntos.FetchByID(this.IdPeriodoFacturacionPuntos); }
			set { SetColumnValue("ID_PERIODO_FACTURACION_PUNTOS", value.IdFacturacionPuntos); }
		}
		
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this FacturasProveedorPuntos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidFacturaProveedorPuntos,int varNumeroFactura,decimal varTotalBase,decimal varIva,DateTime varFechaFactura,string varValorEnLetras,int varIdPrograma,int varIdEmpresa,int? varIdPeriodoFacturacionPuntos,bool varNotaCreditoProveedorPuntos,bool? varAnulada,DateTime? varFechaAnulacion,bool varTipoCurrier,string varObservaciones,string varMotivoNotaCredito,int? varFacturaNotaCredito,bool? varNotaCredito,bool varBitNotaCreditoValor)
		{
			FacturasProveedorPuntos item = new FacturasProveedorPuntos();
			
			item.GuidFacturaProveedorPuntos = varGuidFacturaProveedorPuntos;
			
			item.NumeroFactura = varNumeroFactura;
			
			item.TotalBase = varTotalBase;
			
			item.Iva = varIva;
			
			item.FechaFactura = varFechaFactura;
			
			item.ValorEnLetras = varValorEnLetras;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdEmpresa = varIdEmpresa;
			
			item.IdPeriodoFacturacionPuntos = varIdPeriodoFacturacionPuntos;
			
			item.NotaCreditoProveedorPuntos = varNotaCreditoProveedorPuntos;
			
			item.Anulada = varAnulada;
			
			item.FechaAnulacion = varFechaAnulacion;
			
			item.TipoCurrier = varTipoCurrier;
			
			item.Observaciones = varObservaciones;
			
			item.MotivoNotaCredito = varMotivoNotaCredito;
			
			item.FacturaNotaCredito = varFacturaNotaCredito;
			
			item.NotaCredito = varNotaCredito;
			
			item.BitNotaCreditoValor = varBitNotaCreditoValor;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidFacturaProveedorPuntos,int varNumeroFactura,decimal varTotalBase,decimal varIva,DateTime varFechaFactura,string varValorEnLetras,int varIdPrograma,int varIdEmpresa,int? varIdPeriodoFacturacionPuntos,bool varNotaCreditoProveedorPuntos,bool? varAnulada,DateTime? varFechaAnulacion,bool varTipoCurrier,string varObservaciones,string varMotivoNotaCredito,int? varFacturaNotaCredito,bool? varNotaCredito,bool varBitNotaCreditoValor)
		{
			FacturasProveedorPuntos item = new FacturasProveedorPuntos();
			
				item.GuidFacturaProveedorPuntos = varGuidFacturaProveedorPuntos;
			
				item.NumeroFactura = varNumeroFactura;
			
				item.TotalBase = varTotalBase;
			
				item.Iva = varIva;
			
				item.FechaFactura = varFechaFactura;
			
				item.ValorEnLetras = varValorEnLetras;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdEmpresa = varIdEmpresa;
			
				item.IdPeriodoFacturacionPuntos = varIdPeriodoFacturacionPuntos;
			
				item.NotaCreditoProveedorPuntos = varNotaCreditoProveedorPuntos;
			
				item.Anulada = varAnulada;
			
				item.FechaAnulacion = varFechaAnulacion;
			
				item.TipoCurrier = varTipoCurrier;
			
				item.Observaciones = varObservaciones;
			
				item.MotivoNotaCredito = varMotivoNotaCredito;
			
				item.FacturaNotaCredito = varFacturaNotaCredito;
			
				item.NotaCredito = varNotaCredito;
			
				item.BitNotaCreditoValor = varBitNotaCreditoValor;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidFacturaProveedorPuntosColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroFacturaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalBaseColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaFacturaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorEnLetrasColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEmpresaColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPeriodoFacturacionPuntosColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn NotaCreditoProveedorPuntosColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn AnuladaColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaAnulacionColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn TipoCurrierColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn ObservacionesColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn MotivoNotaCreditoColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn FacturaNotaCreditoColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn NotaCreditoColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        public static TableSchema.TableColumn BitNotaCreditoValorColumn
        {
            get { return Schema.Columns[17]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidFacturaProveedorPuntos = @"GUID_FACTURA_PROVEEDOR_PUNTOS";
			 public static string NumeroFactura = @"NUMERO_FACTURA";
			 public static string TotalBase = @"TOTAL_BASE";
			 public static string Iva = @"IVA";
			 public static string FechaFactura = @"FECHA_FACTURA";
			 public static string ValorEnLetras = @"VALOR_EN_LETRAS";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdEmpresa = @"ID_EMPRESA";
			 public static string IdPeriodoFacturacionPuntos = @"ID_PERIODO_FACTURACION_PUNTOS";
			 public static string NotaCreditoProveedorPuntos = @"NOTA_CREDITO_PROVEEDOR_PUNTOS";
			 public static string Anulada = @"ANULADA";
			 public static string FechaAnulacion = @"FECHA_ANULACION";
			 public static string TipoCurrier = @"TIPO_CURRIER";
			 public static string Observaciones = @"OBSERVACIONES";
			 public static string MotivoNotaCredito = @"MOTIVO_NOTA_CREDITO";
			 public static string FacturaNotaCredito = @"FACTURA_NOTA_CREDITO";
			 public static string NotaCredito = @"NOTA_CREDITO";
			 public static string BitNotaCreditoValor = @"BIT_NOTA_CREDITO_VALOR";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
