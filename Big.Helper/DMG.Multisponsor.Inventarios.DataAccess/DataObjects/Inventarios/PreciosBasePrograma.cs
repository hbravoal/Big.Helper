using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the PreciosBasePrograma class.
	/// </summary>
    [Serializable]
	public partial class PreciosBaseProgramaCollection : ActiveList<PreciosBasePrograma, PreciosBaseProgramaCollection>
	{	   
		public PreciosBaseProgramaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PreciosBaseProgramaCollection</returns>
		public PreciosBaseProgramaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PreciosBasePrograma o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PRECIOS_BASE_PROGRAMA table.
	/// </summary>
	[Serializable]
	public partial class PreciosBasePrograma : ActiveRecord<PreciosBasePrograma>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public PreciosBasePrograma()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PreciosBasePrograma(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public PreciosBasePrograma(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public PreciosBasePrograma(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PRECIOS_BASE_PROGRAMA", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidPrecioProducto = new TableSchema.TableColumn(schema);
				colvarGuidPrecioProducto.ColumnName = "GUID_PRECIO_PRODUCTO";
				colvarGuidPrecioProducto.DataType = DbType.String;
				colvarGuidPrecioProducto.MaxLength = 36;
				colvarGuidPrecioProducto.AutoIncrement = false;
				colvarGuidPrecioProducto.IsNullable = false;
				colvarGuidPrecioProducto.IsPrimaryKey = true;
				colvarGuidPrecioProducto.IsForeignKey = false;
				colvarGuidPrecioProducto.IsReadOnly = false;
				colvarGuidPrecioProducto.DefaultSetting = @"";
				colvarGuidPrecioProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidPrecioProducto);
				
				TableSchema.TableColumn colvarPrecioVentaBase = new TableSchema.TableColumn(schema);
				colvarPrecioVentaBase.ColumnName = "PRECIO_VENTA_BASE";
				colvarPrecioVentaBase.DataType = DbType.Decimal;
				colvarPrecioVentaBase.MaxLength = 0;
				colvarPrecioVentaBase.AutoIncrement = false;
				colvarPrecioVentaBase.IsNullable = false;
				colvarPrecioVentaBase.IsPrimaryKey = false;
				colvarPrecioVentaBase.IsForeignKey = false;
				colvarPrecioVentaBase.IsReadOnly = false;
				colvarPrecioVentaBase.DefaultSetting = @"";
				colvarPrecioVentaBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrecioVentaBase);
				
				TableSchema.TableColumn colvarCostoEnvioBase = new TableSchema.TableColumn(schema);
				colvarCostoEnvioBase.ColumnName = "COSTO_ENVIO_BASE";
				colvarCostoEnvioBase.DataType = DbType.Decimal;
				colvarCostoEnvioBase.MaxLength = 0;
				colvarCostoEnvioBase.AutoIncrement = false;
				colvarCostoEnvioBase.IsNullable = false;
				colvarCostoEnvioBase.IsPrimaryKey = false;
				colvarCostoEnvioBase.IsForeignKey = false;
				colvarCostoEnvioBase.IsReadOnly = false;
				colvarCostoEnvioBase.DefaultSetting = @"";
				colvarCostoEnvioBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCostoEnvioBase);
				
				TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
				colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
				colvarGuidProducto.DataType = DbType.String;
				colvarGuidProducto.MaxLength = 36;
				colvarGuidProducto.AutoIncrement = false;
				colvarGuidProducto.IsNullable = false;
				colvarGuidProducto.IsPrimaryKey = false;
				colvarGuidProducto.IsForeignKey = true;
				colvarGuidProducto.IsReadOnly = false;
				colvarGuidProducto.DefaultSetting = @"";
				
					colvarGuidProducto.ForeignKeyTableName = "INV_PRODUCTOS";
				schema.Columns.Add(colvarGuidProducto);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdTipoDelivery = new TableSchema.TableColumn(schema);
				colvarIdTipoDelivery.ColumnName = "ID_TIPO_DELIVERY";
				colvarIdTipoDelivery.DataType = DbType.Int32;
				colvarIdTipoDelivery.MaxLength = 0;
				colvarIdTipoDelivery.AutoIncrement = false;
				colvarIdTipoDelivery.IsNullable = false;
				colvarIdTipoDelivery.IsPrimaryKey = false;
				colvarIdTipoDelivery.IsForeignKey = true;
				colvarIdTipoDelivery.IsReadOnly = false;
				colvarIdTipoDelivery.DefaultSetting = @"";
				
					colvarIdTipoDelivery.ForeignKeyTableName = "INV_MAS_TIPOS_DELIVERY";
				schema.Columns.Add(colvarIdTipoDelivery);
				
				TableSchema.TableColumn colvarIdMarca = new TableSchema.TableColumn(schema);
				colvarIdMarca.ColumnName = "ID_MARCA";
				colvarIdMarca.DataType = DbType.Int32;
				colvarIdMarca.MaxLength = 0;
				colvarIdMarca.AutoIncrement = false;
				colvarIdMarca.IsNullable = false;
				colvarIdMarca.IsPrimaryKey = false;
				colvarIdMarca.IsForeignKey = true;
				colvarIdMarca.IsReadOnly = false;
				colvarIdMarca.DefaultSetting = @"";
				
					colvarIdMarca.ForeignKeyTableName = "INV_MARCAS";
				schema.Columns.Add(colvarIdMarca);
				
				TableSchema.TableColumn colvarValorAsegurado = new TableSchema.TableColumn(schema);
				colvarValorAsegurado.ColumnName = "VALOR_ASEGURADO";
				colvarValorAsegurado.DataType = DbType.Decimal;
				colvarValorAsegurado.MaxLength = 0;
				colvarValorAsegurado.AutoIncrement = false;
				colvarValorAsegurado.IsNullable = true;
				colvarValorAsegurado.IsPrimaryKey = false;
				colvarValorAsegurado.IsForeignKey = false;
				colvarValorAsegurado.IsReadOnly = false;
				colvarValorAsegurado.DefaultSetting = @"";
				colvarValorAsegurado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorAsegurado);
				
				TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
				colvarIdCiudad.ColumnName = "ID_CIUDAD";
				colvarIdCiudad.DataType = DbType.Int32;
				colvarIdCiudad.MaxLength = 0;
				colvarIdCiudad.AutoIncrement = false;
				colvarIdCiudad.IsNullable = true;
				colvarIdCiudad.IsPrimaryKey = false;
				colvarIdCiudad.IsForeignKey = false;
				colvarIdCiudad.IsReadOnly = false;
				colvarIdCiudad.DefaultSetting = @"";
				colvarIdCiudad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdCiudad);
				
				TableSchema.TableColumn colvarDireccion = new TableSchema.TableColumn(schema);
				colvarDireccion.ColumnName = "DIRECCION";
				colvarDireccion.DataType = DbType.String;
				colvarDireccion.MaxLength = 200;
				colvarDireccion.AutoIncrement = false;
				colvarDireccion.IsNullable = true;
				colvarDireccion.IsPrimaryKey = false;
				colvarDireccion.IsForeignKey = false;
				colvarDireccion.IsReadOnly = false;
				colvarDireccion.DefaultSetting = @"";
				colvarDireccion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDireccion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_PRECIOS_BASE_PROGRAMA",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidPrecioProducto")]
		[Bindable(true)]
		public string GuidPrecioProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidPrecioProducto); }
			set { SetColumnValue(Columns.GuidPrecioProducto, value); }
		}
		  
		[XmlAttribute("PrecioVentaBase")]
		[Bindable(true)]
		public decimal PrecioVentaBase 
		{
			get { return GetColumnValue<decimal>(Columns.PrecioVentaBase); }
			set { SetColumnValue(Columns.PrecioVentaBase, value); }
		}
		  
		[XmlAttribute("CostoEnvioBase")]
		[Bindable(true)]
		public decimal CostoEnvioBase 
		{
			get { return GetColumnValue<decimal>(Columns.CostoEnvioBase); }
			set { SetColumnValue(Columns.CostoEnvioBase, value); }
		}
		  
		[XmlAttribute("GuidProducto")]
		[Bindable(true)]
		public string GuidProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidProducto); }
			set { SetColumnValue(Columns.GuidProducto, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdTipoDelivery")]
		[Bindable(true)]
		public int IdTipoDelivery 
		{
			get { return GetColumnValue<int>(Columns.IdTipoDelivery); }
			set { SetColumnValue(Columns.IdTipoDelivery, value); }
		}
		  
		[XmlAttribute("IdMarca")]
		[Bindable(true)]
		public int IdMarca 
		{
			get { return GetColumnValue<int>(Columns.IdMarca); }
			set { SetColumnValue(Columns.IdMarca, value); }
		}
		  
		[XmlAttribute("ValorAsegurado")]
		[Bindable(true)]
		public decimal? ValorAsegurado 
		{
			get { return GetColumnValue<decimal?>(Columns.ValorAsegurado); }
			set { SetColumnValue(Columns.ValorAsegurado, value); }
		}
		  
		[XmlAttribute("IdCiudad")]
		[Bindable(true)]
		public int? IdCiudad 
		{
			get { return GetColumnValue<int?>(Columns.IdCiudad); }
			set { SetColumnValue(Columns.IdCiudad, value); }
		}
		  
		[XmlAttribute("Direccion")]
		[Bindable(true)]
		public string Direccion 
		{
			get { return GetColumnValue<string>(Columns.Direccion); }
			set { SetColumnValue(Columns.Direccion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricosPrecioCollection HistoricosPrecioRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricosPrecioCollection().Where(HistoricosPrecio.Columns.GuidPrecioProducto, GuidPrecioProducto).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Marcas ActiveRecord object related to this PreciosBasePrograma
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Marcas Marcas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Marcas.FetchByID(this.IdMarca); }
			set { SetColumnValue("ID_MARCA", value.IdMarca); }
		}
		
		
		/// <summary>
		/// Returns a MasTiposDelivery ActiveRecord object related to this PreciosBasePrograma
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposDelivery MasTiposDelivery
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposDelivery.FetchByID(this.IdTipoDelivery); }
			set { SetColumnValue("ID_TIPO_DELIVERY", value.IdTipoDelivery); }
		}
		
		
		/// <summary>
		/// Returns a Productos ActiveRecord object related to this PreciosBasePrograma
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Productos Productos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Productos.FetchByID(this.GuidProducto); }
			set { SetColumnValue("GUID_PRODUCTO", value.Guid); }
		}
		
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this PreciosBasePrograma
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidPrecioProducto,decimal varPrecioVentaBase,decimal varCostoEnvioBase,string varGuidProducto,int varIdPrograma,int varIdTipoDelivery,int varIdMarca,decimal? varValorAsegurado,int? varIdCiudad,string varDireccion)
		{
			PreciosBasePrograma item = new PreciosBasePrograma();
			
			item.GuidPrecioProducto = varGuidPrecioProducto;
			
			item.PrecioVentaBase = varPrecioVentaBase;
			
			item.CostoEnvioBase = varCostoEnvioBase;
			
			item.GuidProducto = varGuidProducto;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdTipoDelivery = varIdTipoDelivery;
			
			item.IdMarca = varIdMarca;
			
			item.ValorAsegurado = varValorAsegurado;
			
			item.IdCiudad = varIdCiudad;
			
			item.Direccion = varDireccion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidPrecioProducto,decimal varPrecioVentaBase,decimal varCostoEnvioBase,string varGuidProducto,int varIdPrograma,int varIdTipoDelivery,int varIdMarca,decimal? varValorAsegurado,int? varIdCiudad,string varDireccion)
		{
			PreciosBasePrograma item = new PreciosBasePrograma();
			
				item.GuidPrecioProducto = varGuidPrecioProducto;
			
				item.PrecioVentaBase = varPrecioVentaBase;
			
				item.CostoEnvioBase = varCostoEnvioBase;
			
				item.GuidProducto = varGuidProducto;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdTipoDelivery = varIdTipoDelivery;
			
				item.IdMarca = varIdMarca;
			
				item.ValorAsegurado = varValorAsegurado;
			
				item.IdCiudad = varIdCiudad;
			
				item.Direccion = varDireccion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidPrecioProductoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PrecioVentaBaseColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn CostoEnvioBaseColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidProductoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoDeliveryColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdMarcaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorAseguradoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCiudadColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn DireccionColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidPrecioProducto = @"GUID_PRECIO_PRODUCTO";
			 public static string PrecioVentaBase = @"PRECIO_VENTA_BASE";
			 public static string CostoEnvioBase = @"COSTO_ENVIO_BASE";
			 public static string GuidProducto = @"GUID_PRODUCTO";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdTipoDelivery = @"ID_TIPO_DELIVERY";
			 public static string IdMarca = @"ID_MARCA";
			 public static string ValorAsegurado = @"VALOR_ASEGURADO";
			 public static string IdCiudad = @"ID_CIUDAD";
			 public static string Direccion = @"DIRECCION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
