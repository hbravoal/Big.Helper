using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the ReplicacionModulos class.
	/// </summary>
    [Serializable]
	public partial class ReplicacionModulosCollection : ActiveList<ReplicacionModulos, ReplicacionModulosCollection>
	{	   
		public ReplicacionModulosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ReplicacionModulosCollection</returns>
		public ReplicacionModulosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                ReplicacionModulos o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_REPLICACION_MODULOS table.
	/// </summary>
	[Serializable]
	public partial class ReplicacionModulos : ActiveRecord<ReplicacionModulos>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public ReplicacionModulos()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public ReplicacionModulos(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public ReplicacionModulos(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public ReplicacionModulos(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_REPLICACION_MODULOS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Int32;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = false;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
				colvarNombre.ColumnName = "NOMBRE";
				colvarNombre.DataType = DbType.String;
				colvarNombre.MaxLength = 100;
				colvarNombre.AutoIncrement = false;
				colvarNombre.IsNullable = false;
				colvarNombre.IsPrimaryKey = false;
				colvarNombre.IsForeignKey = false;
				colvarNombre.IsReadOnly = false;
				colvarNombre.DefaultSetting = @"";
				colvarNombre.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombre);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = false;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				colvarIdPrograma.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarProveedores = new TableSchema.TableColumn(schema);
				colvarProveedores.ColumnName = "PROVEEDORES";
				colvarProveedores.DataType = DbType.String;
				colvarProveedores.MaxLength = 500;
				colvarProveedores.AutoIncrement = false;
				colvarProveedores.IsNullable = true;
				colvarProveedores.IsPrimaryKey = false;
				colvarProveedores.IsForeignKey = false;
				colvarProveedores.IsReadOnly = false;
				colvarProveedores.DefaultSetting = @"";
				colvarProveedores.ForeignKeyTableName = "";
				schema.Columns.Add(colvarProveedores);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_REPLICACION_MODULOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public int Id 
		{
			get { return GetColumnValue<int>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Nombre")]
		[Bindable(true)]
		public string Nombre 
		{
			get { return GetColumnValue<string>(Columns.Nombre); }
			set { SetColumnValue(Columns.Nombre, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("Proveedores")]
		[Bindable(true)]
		public string Proveedores 
		{
			get { return GetColumnValue<string>(Columns.Proveedores); }
			set { SetColumnValue(Columns.Proveedores, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ReplicacionPeticionesCollection ReplicacionPeticionesRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.ReplicacionPeticionesCollection().Where(ReplicacionPeticiones.Columns.IdModulo, Id).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varId,string varNombre,int varIdPrograma,string varProveedores)
		{
			ReplicacionModulos item = new ReplicacionModulos();
			
			item.Id = varId;
			
			item.Nombre = varNombre;
			
			item.IdPrograma = varIdPrograma;
			
			item.Proveedores = varProveedores;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varId,string varNombre,int varIdPrograma,string varProveedores)
		{
			ReplicacionModulos item = new ReplicacionModulos();
			
				item.Id = varId;
			
				item.Nombre = varNombre;
			
				item.IdPrograma = varIdPrograma;
			
				item.Proveedores = varProveedores;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn ProveedoresColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Nombre = @"NOMBRE";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string Proveedores = @"PROVEEDORES";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
