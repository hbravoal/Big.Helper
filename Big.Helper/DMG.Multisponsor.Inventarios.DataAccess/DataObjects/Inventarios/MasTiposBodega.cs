using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the MasTiposBodega class.
	/// </summary>
    [Serializable]
	public partial class MasTiposBodegaCollection : ActiveList<MasTiposBodega, MasTiposBodegaCollection>
	{	   
		public MasTiposBodegaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasTiposBodegaCollection</returns>
		public MasTiposBodegaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasTiposBodega o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_BODEGA table.
	/// </summary>
	[Serializable]
	public partial class MasTiposBodega : ActiveRecord<MasTiposBodega>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public MasTiposBodega()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasTiposBodega(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public MasTiposBodega(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public MasTiposBodega(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_BODEGA", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoBodega = new TableSchema.TableColumn(schema);
				colvarIdTipoBodega.ColumnName = "ID_TIPO_BODEGA";
				colvarIdTipoBodega.DataType = DbType.Int32;
				colvarIdTipoBodega.MaxLength = 0;
				colvarIdTipoBodega.AutoIncrement = true;
				colvarIdTipoBodega.IsNullable = false;
				colvarIdTipoBodega.IsPrimaryKey = true;
				colvarIdTipoBodega.IsForeignKey = false;
				colvarIdTipoBodega.IsReadOnly = false;
				colvarIdTipoBodega.DefaultSetting = @"";
				colvarIdTipoBodega.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTipoBodega);
				
				TableSchema.TableColumn colvarNombreTipo = new TableSchema.TableColumn(schema);
				colvarNombreTipo.ColumnName = "NOMBRE_TIPO";
				colvarNombreTipo.DataType = DbType.String;
				colvarNombreTipo.MaxLength = 100;
				colvarNombreTipo.AutoIncrement = false;
				colvarNombreTipo.IsNullable = false;
				colvarNombreTipo.IsPrimaryKey = false;
				colvarNombreTipo.IsForeignKey = false;
				colvarNombreTipo.IsReadOnly = false;
				colvarNombreTipo.DefaultSetting = @"";
				colvarNombreTipo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreTipo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_MAS_TIPOS_BODEGA",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoBodega")]
		[Bindable(true)]
		public int IdTipoBodega 
		{
			get { return GetColumnValue<int>(Columns.IdTipoBodega); }
			set { SetColumnValue(Columns.IdTipoBodega, value); }
		}
		  
		[XmlAttribute("NombreTipo")]
		[Bindable(true)]
		public string NombreTipo 
		{
			get { return GetColumnValue<string>(Columns.NombreTipo); }
			set { SetColumnValue(Columns.NombreTipo, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasCollection BodegasRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasCollection().Where(Bodegas.Columns.IdTipoBodega, IdTipoBodega).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombreTipo)
		{
			MasTiposBodega item = new MasTiposBodega();
			
			item.NombreTipo = varNombreTipo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoBodega,string varNombreTipo)
		{
			MasTiposBodega item = new MasTiposBodega();
			
				item.IdTipoBodega = varIdTipoBodega;
			
				item.NombreTipo = varNombreTipo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoBodegaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreTipoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoBodega = @"ID_TIPO_BODEGA";
			 public static string NombreTipo = @"NOMBRE_TIPO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
