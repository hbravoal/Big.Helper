using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the MasTiposEmpaque class.
	/// </summary>
    [Serializable]
	public partial class MasTiposEmpaqueCollection : ActiveList<MasTiposEmpaque, MasTiposEmpaqueCollection>
	{	   
		public MasTiposEmpaqueCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasTiposEmpaqueCollection</returns>
		public MasTiposEmpaqueCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasTiposEmpaque o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_EMPAQUE table.
	/// </summary>
	[Serializable]
	public partial class MasTiposEmpaque : ActiveRecord<MasTiposEmpaque>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public MasTiposEmpaque()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasTiposEmpaque(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public MasTiposEmpaque(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public MasTiposEmpaque(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_EMPAQUE", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoEmpaque = new TableSchema.TableColumn(schema);
				colvarIdTipoEmpaque.ColumnName = "ID_TIPO_EMPAQUE";
				colvarIdTipoEmpaque.DataType = DbType.Int32;
				colvarIdTipoEmpaque.MaxLength = 0;
				colvarIdTipoEmpaque.AutoIncrement = true;
				colvarIdTipoEmpaque.IsNullable = false;
				colvarIdTipoEmpaque.IsPrimaryKey = true;
				colvarIdTipoEmpaque.IsForeignKey = false;
				colvarIdTipoEmpaque.IsReadOnly = false;
				colvarIdTipoEmpaque.DefaultSetting = @"";
				colvarIdTipoEmpaque.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTipoEmpaque);
				
				TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
				colvarNombre.ColumnName = "NOMBRE";
				colvarNombre.DataType = DbType.String;
				colvarNombre.MaxLength = 200;
				colvarNombre.AutoIncrement = false;
				colvarNombre.IsNullable = false;
				colvarNombre.IsPrimaryKey = false;
				colvarNombre.IsForeignKey = false;
				colvarNombre.IsReadOnly = false;
				colvarNombre.DefaultSetting = @"";
				colvarNombre.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombre);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_MAS_TIPOS_EMPAQUE",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoEmpaque")]
		[Bindable(true)]
		public int IdTipoEmpaque 
		{
			get { return GetColumnValue<int>(Columns.IdTipoEmpaque); }
			set { SetColumnValue(Columns.IdTipoEmpaque, value); }
		}
		  
		[XmlAttribute("Nombre")]
		[Bindable(true)]
		public string Nombre 
		{
			get { return GetColumnValue<string>(Columns.Nombre); }
			set { SetColumnValue(Columns.Nombre, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ProductosCollection ProductosRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.ProductosCollection().Where(Productos.Columns.IdTipoEmpaque, IdTipoEmpaque).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombre)
		{
			MasTiposEmpaque item = new MasTiposEmpaque();
			
			item.Nombre = varNombre;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoEmpaque,string varNombre)
		{
			MasTiposEmpaque item = new MasTiposEmpaque();
			
				item.IdTipoEmpaque = varIdTipoEmpaque;
			
				item.Nombre = varNombre;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoEmpaqueColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoEmpaque = @"ID_TIPO_EMPAQUE";
			 public static string Nombre = @"NOMBRE";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
