using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the MasTiposResolucion class.
	/// </summary>
    [Serializable]
	public partial class MasTiposResolucionCollection : ActiveList<MasTiposResolucion, MasTiposResolucionCollection>
	{	   
		public MasTiposResolucionCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasTiposResolucionCollection</returns>
		public MasTiposResolucionCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasTiposResolucion o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_RESOLUCION table.
	/// </summary>
	[Serializable]
	public partial class MasTiposResolucion : ActiveRecord<MasTiposResolucion>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public MasTiposResolucion()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasTiposResolucion(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public MasTiposResolucion(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public MasTiposResolucion(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_RESOLUCION", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoResolucion = new TableSchema.TableColumn(schema);
				colvarIdTipoResolucion.ColumnName = "ID_TIPO_RESOLUCION";
				colvarIdTipoResolucion.DataType = DbType.Int32;
				colvarIdTipoResolucion.MaxLength = 0;
				colvarIdTipoResolucion.AutoIncrement = false;
				colvarIdTipoResolucion.IsNullable = false;
				colvarIdTipoResolucion.IsPrimaryKey = true;
				colvarIdTipoResolucion.IsForeignKey = false;
				colvarIdTipoResolucion.IsReadOnly = false;
				colvarIdTipoResolucion.DefaultSetting = @"";
				colvarIdTipoResolucion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTipoResolucion);
				
				TableSchema.TableColumn colvarTipoResolucion = new TableSchema.TableColumn(schema);
				colvarTipoResolucion.ColumnName = "TIPO_RESOLUCION";
				colvarTipoResolucion.DataType = DbType.AnsiString;
				colvarTipoResolucion.MaxLength = 50;
				colvarTipoResolucion.AutoIncrement = false;
				colvarTipoResolucion.IsNullable = false;
				colvarTipoResolucion.IsPrimaryKey = false;
				colvarTipoResolucion.IsForeignKey = false;
				colvarTipoResolucion.IsReadOnly = false;
				colvarTipoResolucion.DefaultSetting = @"";
				colvarTipoResolucion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTipoResolucion);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_MAS_TIPOS_RESOLUCION",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoResolucion")]
		[Bindable(true)]
		public int IdTipoResolucion 
		{
			get { return GetColumnValue<int>(Columns.IdTipoResolucion); }
			set { SetColumnValue(Columns.IdTipoResolucion, value); }
		}
		  
		[XmlAttribute("TipoResolucion")]
		[Bindable(true)]
		public string TipoResolucion 
		{
			get { return GetColumnValue<string>(Columns.TipoResolucion); }
			set { SetColumnValue(Columns.TipoResolucion, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ResolucionesCollection ResolucionesRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.ResolucionesCollection().Where(Resoluciones.Columns.IdTipoResolucion, IdTipoResolucion).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdTipoResolucion,string varTipoResolucion)
		{
			MasTiposResolucion item = new MasTiposResolucion();
			
			item.IdTipoResolucion = varIdTipoResolucion;
			
			item.TipoResolucion = varTipoResolucion;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoResolucion,string varTipoResolucion)
		{
			MasTiposResolucion item = new MasTiposResolucion();
			
				item.IdTipoResolucion = varIdTipoResolucion;
			
				item.TipoResolucion = varTipoResolucion;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoResolucionColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TipoResolucionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoResolucion = @"ID_TIPO_RESOLUCION";
			 public static string TipoResolucion = @"TIPO_RESOLUCION";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
