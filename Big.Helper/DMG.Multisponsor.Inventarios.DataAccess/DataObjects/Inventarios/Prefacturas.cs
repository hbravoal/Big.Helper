using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the Prefacturas class.
	/// </summary>
    [Serializable]
	public partial class PrefacturasCollection : ActiveList<Prefacturas, PrefacturasCollection>
	{	   
		public PrefacturasCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PrefacturasCollection</returns>
		public PrefacturasCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Prefacturas o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PREFACTURAS table.
	/// </summary>
	[Serializable]
	public partial class Prefacturas : ActiveRecord<Prefacturas>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Prefacturas()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Prefacturas(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Prefacturas(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Prefacturas(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PREFACTURAS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPrefactura = new TableSchema.TableColumn(schema);
				colvarIdPrefactura.ColumnName = "ID_PREFACTURA";
				colvarIdPrefactura.DataType = DbType.Int32;
				colvarIdPrefactura.MaxLength = 0;
				colvarIdPrefactura.AutoIncrement = true;
				colvarIdPrefactura.IsNullable = false;
				colvarIdPrefactura.IsPrimaryKey = true;
				colvarIdPrefactura.IsForeignKey = false;
				colvarIdPrefactura.IsReadOnly = false;
				colvarIdPrefactura.DefaultSetting = @"";
				colvarIdPrefactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPrefactura);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = true;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarFechaInicio = new TableSchema.TableColumn(schema);
				colvarFechaInicio.ColumnName = "FECHA_INICIO";
				colvarFechaInicio.DataType = DbType.DateTime;
				colvarFechaInicio.MaxLength = 0;
				colvarFechaInicio.AutoIncrement = false;
				colvarFechaInicio.IsNullable = true;
				colvarFechaInicio.IsPrimaryKey = false;
				colvarFechaInicio.IsForeignKey = false;
				colvarFechaInicio.IsReadOnly = false;
				colvarFechaInicio.DefaultSetting = @"";
				colvarFechaInicio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaInicio);
				
				TableSchema.TableColumn colvarFechaFin = new TableSchema.TableColumn(schema);
				colvarFechaFin.ColumnName = "FECHA_FIN";
				colvarFechaFin.DataType = DbType.DateTime;
				colvarFechaFin.MaxLength = 0;
				colvarFechaFin.AutoIncrement = false;
				colvarFechaFin.IsNullable = true;
				colvarFechaFin.IsPrimaryKey = false;
				colvarFechaFin.IsForeignKey = false;
				colvarFechaFin.IsReadOnly = false;
				colvarFechaFin.DefaultSetting = @"";
				colvarFechaFin.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaFin);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_PREFACTURAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPrefactura")]
		[Bindable(true)]
		public int IdPrefactura 
		{
			get { return GetColumnValue<int>(Columns.IdPrefactura); }
			set { SetColumnValue(Columns.IdPrefactura, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int? IdPrograma 
		{
			get { return GetColumnValue<int?>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("FechaInicio")]
		[Bindable(true)]
		public DateTime? FechaInicio 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaInicio); }
			set { SetColumnValue(Columns.FechaInicio, value); }
		}
		  
		[XmlAttribute("FechaFin")]
		[Bindable(true)]
		public DateTime? FechaFin 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaFin); }
			set { SetColumnValue(Columns.FechaFin, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this Prefacturas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int? varIdPrograma,DateTime? varFechaInicio,DateTime? varFechaFin)
		{
			Prefacturas item = new Prefacturas();
			
			item.IdPrograma = varIdPrograma;
			
			item.FechaInicio = varFechaInicio;
			
			item.FechaFin = varFechaFin;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdPrefactura,int? varIdPrograma,DateTime? varFechaInicio,DateTime? varFechaFin)
		{
			Prefacturas item = new Prefacturas();
			
				item.IdPrefactura = varIdPrefactura;
			
				item.IdPrograma = varIdPrograma;
			
				item.FechaInicio = varFechaInicio;
			
				item.FechaFin = varFechaFin;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdPrefacturaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaInicioColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaFinColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPrefactura = @"ID_PREFACTURA";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string FechaInicio = @"FECHA_INICIO";
			 public static string FechaFin = @"FECHA_FIN";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
