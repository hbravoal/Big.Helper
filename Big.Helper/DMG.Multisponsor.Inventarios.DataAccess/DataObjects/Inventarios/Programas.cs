using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the Programas class.
	/// </summary>
    [Serializable]
	public partial class ProgramasCollection : ActiveList<Programas, ProgramasCollection>
	{	   
		public ProgramasCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>ProgramasCollection</returns>
		public ProgramasCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Programas o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PROGRAMAS table.
	/// </summary>
	[Serializable]
	public partial class Programas : ActiveRecord<Programas>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Programas()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Programas(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Programas(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Programas(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PROGRAMAS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = true;
				colvarIdPrograma.IsForeignKey = false;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				colvarIdPrograma.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarNombrePrograma = new TableSchema.TableColumn(schema);
				colvarNombrePrograma.ColumnName = "NOMBRE_PROGRAMA";
				colvarNombrePrograma.DataType = DbType.String;
				colvarNombrePrograma.MaxLength = 100;
				colvarNombrePrograma.AutoIncrement = false;
				colvarNombrePrograma.IsNullable = false;
				colvarNombrePrograma.IsPrimaryKey = false;
				colvarNombrePrograma.IsForeignKey = false;
				colvarNombrePrograma.IsReadOnly = false;
				colvarNombrePrograma.DefaultSetting = @"";
				colvarNombrePrograma.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombrePrograma);
				
				TableSchema.TableColumn colvarIdEmpresa = new TableSchema.TableColumn(schema);
				colvarIdEmpresa.ColumnName = "ID_EMPRESA";
				colvarIdEmpresa.DataType = DbType.Int32;
				colvarIdEmpresa.MaxLength = 0;
				colvarIdEmpresa.AutoIncrement = false;
				colvarIdEmpresa.IsNullable = false;
				colvarIdEmpresa.IsPrimaryKey = false;
				colvarIdEmpresa.IsForeignKey = true;
				colvarIdEmpresa.IsReadOnly = false;
				colvarIdEmpresa.DefaultSetting = @"";
				
					colvarIdEmpresa.ForeignKeyTableName = "INV_EMPRESAS";
				schema.Columns.Add(colvarIdEmpresa);
				
				TableSchema.TableColumn colvarFechaInicio = new TableSchema.TableColumn(schema);
				colvarFechaInicio.ColumnName = "FECHA_INICIO";
				colvarFechaInicio.DataType = DbType.DateTime;
				colvarFechaInicio.MaxLength = 0;
				colvarFechaInicio.AutoIncrement = false;
				colvarFechaInicio.IsNullable = false;
				colvarFechaInicio.IsPrimaryKey = false;
				colvarFechaInicio.IsForeignKey = false;
				colvarFechaInicio.IsReadOnly = false;
				colvarFechaInicio.DefaultSetting = @"";
				colvarFechaInicio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaInicio);
				
				TableSchema.TableColumn colvarIdResolucion = new TableSchema.TableColumn(schema);
				colvarIdResolucion.ColumnName = "ID_RESOLUCION";
				colvarIdResolucion.DataType = DbType.Int32;
				colvarIdResolucion.MaxLength = 0;
				colvarIdResolucion.AutoIncrement = false;
				colvarIdResolucion.IsNullable = true;
				colvarIdResolucion.IsPrimaryKey = false;
				colvarIdResolucion.IsForeignKey = true;
				colvarIdResolucion.IsReadOnly = false;
				colvarIdResolucion.DefaultSetting = @"";
				
					colvarIdResolucion.ForeignKeyTableName = "INV_RESOLUCIONES";
				schema.Columns.Add(colvarIdResolucion);
				
				TableSchema.TableColumn colvarIdNotaCredito = new TableSchema.TableColumn(schema);
				colvarIdNotaCredito.ColumnName = "ID_NOTA_CREDITO";
				colvarIdNotaCredito.DataType = DbType.Int32;
				colvarIdNotaCredito.MaxLength = 0;
				colvarIdNotaCredito.AutoIncrement = false;
				colvarIdNotaCredito.IsNullable = true;
				colvarIdNotaCredito.IsPrimaryKey = false;
				colvarIdNotaCredito.IsForeignKey = true;
				colvarIdNotaCredito.IsReadOnly = false;
				colvarIdNotaCredito.DefaultSetting = @"";
				
					colvarIdNotaCredito.ForeignKeyTableName = "INV_RESOLUCIONES";
				schema.Columns.Add(colvarIdNotaCredito);
				
				TableSchema.TableColumn colvarIdResolucionBanco = new TableSchema.TableColumn(schema);
				colvarIdResolucionBanco.ColumnName = "ID_RESOLUCION_BANCO";
				colvarIdResolucionBanco.DataType = DbType.Int32;
				colvarIdResolucionBanco.MaxLength = 0;
				colvarIdResolucionBanco.AutoIncrement = false;
				colvarIdResolucionBanco.IsNullable = true;
				colvarIdResolucionBanco.IsPrimaryKey = false;
				colvarIdResolucionBanco.IsForeignKey = true;
				colvarIdResolucionBanco.IsReadOnly = false;
				colvarIdResolucionBanco.DefaultSetting = @"";
				
					colvarIdResolucionBanco.ForeignKeyTableName = "INV_RESOLUCIONES";
				schema.Columns.Add(colvarIdResolucionBanco);
				
				TableSchema.TableColumn colvarFactorPunto = new TableSchema.TableColumn(schema);
				colvarFactorPunto.ColumnName = "FACTOR_PUNTO";
				colvarFactorPunto.DataType = DbType.Decimal;
				colvarFactorPunto.MaxLength = 0;
				colvarFactorPunto.AutoIncrement = false;
				colvarFactorPunto.IsNullable = false;
				colvarFactorPunto.IsPrimaryKey = false;
				colvarFactorPunto.IsForeignKey = false;
				colvarFactorPunto.IsReadOnly = false;
				colvarFactorPunto.DefaultSetting = @"";
				colvarFactorPunto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFactorPunto);
				
				TableSchema.TableColumn colvarRedondeo = new TableSchema.TableColumn(schema);
				colvarRedondeo.ColumnName = "REDONDEO";
				colvarRedondeo.DataType = DbType.Int32;
				colvarRedondeo.MaxLength = 0;
				colvarRedondeo.AutoIncrement = false;
				colvarRedondeo.IsNullable = false;
				colvarRedondeo.IsPrimaryKey = false;
				colvarRedondeo.IsForeignKey = false;
				colvarRedondeo.IsReadOnly = false;
				colvarRedondeo.DefaultSetting = @"";
				colvarRedondeo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRedondeo);
				
				TableSchema.TableColumn colvarFacturaProveedorPuntos = new TableSchema.TableColumn(schema);
				colvarFacturaProveedorPuntos.ColumnName = "FACTURA_PROVEEDOR_PUNTOS";
				colvarFacturaProveedorPuntos.DataType = DbType.Boolean;
				colvarFacturaProveedorPuntos.MaxLength = 0;
				colvarFacturaProveedorPuntos.AutoIncrement = false;
				colvarFacturaProveedorPuntos.IsNullable = false;
				colvarFacturaProveedorPuntos.IsPrimaryKey = false;
				colvarFacturaProveedorPuntos.IsForeignKey = false;
				colvarFacturaProveedorPuntos.IsReadOnly = false;
				colvarFacturaProveedorPuntos.DefaultSetting = @"";
				colvarFacturaProveedorPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFacturaProveedorPuntos);
				
				TableSchema.TableColumn colvarPorcentajeComision = new TableSchema.TableColumn(schema);
				colvarPorcentajeComision.ColumnName = "PORCENTAJE_COMISION";
				colvarPorcentajeComision.DataType = DbType.Decimal;
				colvarPorcentajeComision.MaxLength = 0;
				colvarPorcentajeComision.AutoIncrement = false;
				colvarPorcentajeComision.IsNullable = false;
				colvarPorcentajeComision.IsPrimaryKey = false;
				colvarPorcentajeComision.IsForeignKey = false;
				colvarPorcentajeComision.IsReadOnly = false;
				colvarPorcentajeComision.DefaultSetting = @"";
				colvarPorcentajeComision.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPorcentajeComision);
				
				TableSchema.TableColumn colvarCobrarEnvio = new TableSchema.TableColumn(schema);
				colvarCobrarEnvio.ColumnName = "COBRAR_ENVIO";
				colvarCobrarEnvio.DataType = DbType.Boolean;
				colvarCobrarEnvio.MaxLength = 0;
				colvarCobrarEnvio.AutoIncrement = false;
				colvarCobrarEnvio.IsNullable = false;
				colvarCobrarEnvio.IsPrimaryKey = false;
				colvarCobrarEnvio.IsForeignKey = false;
				colvarCobrarEnvio.IsReadOnly = false;
				colvarCobrarEnvio.DefaultSetting = @"";
				colvarCobrarEnvio.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCobrarEnvio);
				
				TableSchema.TableColumn colvarPlazoVencimientoFactura = new TableSchema.TableColumn(schema);
				colvarPlazoVencimientoFactura.ColumnName = "PLAZO_VENCIMIENTO_FACTURA";
				colvarPlazoVencimientoFactura.DataType = DbType.Int32;
				colvarPlazoVencimientoFactura.MaxLength = 0;
				colvarPlazoVencimientoFactura.AutoIncrement = false;
				colvarPlazoVencimientoFactura.IsNullable = true;
				colvarPlazoVencimientoFactura.IsPrimaryKey = false;
				colvarPlazoVencimientoFactura.IsForeignKey = false;
				colvarPlazoVencimientoFactura.IsReadOnly = false;
				colvarPlazoVencimientoFactura.DefaultSetting = @"";
				colvarPlazoVencimientoFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPlazoVencimientoFactura);
				
				TableSchema.TableColumn colvarIdTipoFacturacion = new TableSchema.TableColumn(schema);
				colvarIdTipoFacturacion.ColumnName = "ID_TIPO_FACTURACION";
				colvarIdTipoFacturacion.DataType = DbType.Int32;
				colvarIdTipoFacturacion.MaxLength = 0;
				colvarIdTipoFacturacion.AutoIncrement = false;
				colvarIdTipoFacturacion.IsNullable = true;
				colvarIdTipoFacturacion.IsPrimaryKey = false;
				colvarIdTipoFacturacion.IsForeignKey = true;
				colvarIdTipoFacturacion.IsReadOnly = false;
				colvarIdTipoFacturacion.DefaultSetting = @"";
				
					colvarIdTipoFacturacion.ForeignKeyTableName = "INV_MAS_TIPOS_FACTURACION";
				schema.Columns.Add(colvarIdTipoFacturacion);
				
				TableSchema.TableColumn colvarNumeroPeriodosProveedorPuntos = new TableSchema.TableColumn(schema);
				colvarNumeroPeriodosProveedorPuntos.ColumnName = "NUMERO_PERIODOS_PROVEEDOR_PUNTOS";
				colvarNumeroPeriodosProveedorPuntos.DataType = DbType.Int32;
				colvarNumeroPeriodosProveedorPuntos.MaxLength = 0;
				colvarNumeroPeriodosProveedorPuntos.AutoIncrement = false;
				colvarNumeroPeriodosProveedorPuntos.IsNullable = true;
				colvarNumeroPeriodosProveedorPuntos.IsPrimaryKey = false;
				colvarNumeroPeriodosProveedorPuntos.IsForeignKey = false;
				colvarNumeroPeriodosProveedorPuntos.IsReadOnly = false;
				colvarNumeroPeriodosProveedorPuntos.DefaultSetting = @"";
				colvarNumeroPeriodosProveedorPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroPeriodosProveedorPuntos);
				
				TableSchema.TableColumn colvarNumeroPeriodosEstablecimientos = new TableSchema.TableColumn(schema);
				colvarNumeroPeriodosEstablecimientos.ColumnName = "NUMERO_PERIODOS_ESTABLECIMIENTOS";
				colvarNumeroPeriodosEstablecimientos.DataType = DbType.Int32;
				colvarNumeroPeriodosEstablecimientos.MaxLength = 0;
				colvarNumeroPeriodosEstablecimientos.AutoIncrement = false;
				colvarNumeroPeriodosEstablecimientos.IsNullable = true;
				colvarNumeroPeriodosEstablecimientos.IsPrimaryKey = false;
				colvarNumeroPeriodosEstablecimientos.IsForeignKey = false;
				colvarNumeroPeriodosEstablecimientos.IsReadOnly = false;
				colvarNumeroPeriodosEstablecimientos.DefaultSetting = @"";
				colvarNumeroPeriodosEstablecimientos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroPeriodosEstablecimientos);
				
				TableSchema.TableColumn colvarCerrado = new TableSchema.TableColumn(schema);
				colvarCerrado.ColumnName = "CERRADO";
				colvarCerrado.DataType = DbType.Boolean;
				colvarCerrado.MaxLength = 0;
				colvarCerrado.AutoIncrement = false;
				colvarCerrado.IsNullable = true;
				colvarCerrado.IsPrimaryKey = false;
				colvarCerrado.IsForeignKey = false;
				colvarCerrado.IsReadOnly = false;
				colvarCerrado.DefaultSetting = @"";
				colvarCerrado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCerrado);

                TableSchema.TableColumn colvarIvaCurrier = new TableSchema.TableColumn(schema);
                colvarIvaCurrier.ColumnName = "IVA_CURRIER";
                colvarIvaCurrier.DataType = DbType.Boolean;
                colvarIvaCurrier.MaxLength = 0;
                colvarIvaCurrier.AutoIncrement = false;
                colvarIvaCurrier.IsNullable = true;
                colvarIvaCurrier.IsPrimaryKey = false;
                colvarIvaCurrier.IsForeignKey = false;
                colvarIvaCurrier.IsReadOnly = false;
                colvarIvaCurrier.DefaultSetting = @"";
                colvarIvaCurrier.ForeignKeyTableName = "";
                schema.Columns.Add(colvarIvaCurrier);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_PROGRAMAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("NombrePrograma")]
		[Bindable(true)]
		public string NombrePrograma 
		{
			get { return GetColumnValue<string>(Columns.NombrePrograma); }
			set { SetColumnValue(Columns.NombrePrograma, value); }
		}
		  
		[XmlAttribute("IdEmpresa")]
		[Bindable(true)]
		public int IdEmpresa 
		{
			get { return GetColumnValue<int>(Columns.IdEmpresa); }
			set { SetColumnValue(Columns.IdEmpresa, value); }
		}
		  
		[XmlAttribute("FechaInicio")]
		[Bindable(true)]
		public DateTime FechaInicio 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaInicio); }
			set { SetColumnValue(Columns.FechaInicio, value); }
		}
		  
		[XmlAttribute("IdResolucion")]
		[Bindable(true)]
		public int? IdResolucion 
		{
			get { return GetColumnValue<int?>(Columns.IdResolucion); }
			set { SetColumnValue(Columns.IdResolucion, value); }
		}
		  
		[XmlAttribute("IdNotaCredito")]
		[Bindable(true)]
		public int? IdNotaCredito 
		{
			get { return GetColumnValue<int?>(Columns.IdNotaCredito); }
			set { SetColumnValue(Columns.IdNotaCredito, value); }
		}
		  
		[XmlAttribute("IdResolucionBanco")]
		[Bindable(true)]
		public int? IdResolucionBanco 
		{
			get { return GetColumnValue<int?>(Columns.IdResolucionBanco); }
			set { SetColumnValue(Columns.IdResolucionBanco, value); }
		}
		  
		[XmlAttribute("FactorPunto")]
		[Bindable(true)]
		public decimal FactorPunto 
		{
			get { return GetColumnValue<decimal>(Columns.FactorPunto); }
			set { SetColumnValue(Columns.FactorPunto, value); }
		}
		  
		[XmlAttribute("Redondeo")]
		[Bindable(true)]
		public int Redondeo 
		{
			get { return GetColumnValue<int>(Columns.Redondeo); }
			set { SetColumnValue(Columns.Redondeo, value); }
		}
		  
		[XmlAttribute("FacturaProveedorPuntos")]
		[Bindable(true)]
		public bool FacturaProveedorPuntos 
		{
			get { return GetColumnValue<bool>(Columns.FacturaProveedorPuntos); }
			set { SetColumnValue(Columns.FacturaProveedorPuntos, value); }
		}
		  
		[XmlAttribute("PorcentajeComision")]
		[Bindable(true)]
		public decimal PorcentajeComision 
		{
			get { return GetColumnValue<decimal>(Columns.PorcentajeComision); }
			set { SetColumnValue(Columns.PorcentajeComision, value); }
		}
		  
		[XmlAttribute("CobrarEnvio")]
		[Bindable(true)]
		public bool CobrarEnvio 
		{
			get { return GetColumnValue<bool>(Columns.CobrarEnvio); }
			set { SetColumnValue(Columns.CobrarEnvio, value); }
		}
		  
		[XmlAttribute("PlazoVencimientoFactura")]
		[Bindable(true)]
		public int? PlazoVencimientoFactura 
		{
			get { return GetColumnValue<int?>(Columns.PlazoVencimientoFactura); }
			set { SetColumnValue(Columns.PlazoVencimientoFactura, value); }
		}
		  
		[XmlAttribute("IdTipoFacturacion")]
		[Bindable(true)]
		public int? IdTipoFacturacion 
		{
			get { return GetColumnValue<int?>(Columns.IdTipoFacturacion); }
			set { SetColumnValue(Columns.IdTipoFacturacion, value); }
		}
		  
		[XmlAttribute("NumeroPeriodosProveedorPuntos")]
		[Bindable(true)]
		public int? NumeroPeriodosProveedorPuntos 
		{
			get { return GetColumnValue<int?>(Columns.NumeroPeriodosProveedorPuntos); }
			set { SetColumnValue(Columns.NumeroPeriodosProveedorPuntos, value); }
		}
		  
		[XmlAttribute("NumeroPeriodosEstablecimientos")]
		[Bindable(true)]
		public int? NumeroPeriodosEstablecimientos 
		{
			get { return GetColumnValue<int?>(Columns.NumeroPeriodosEstablecimientos); }
			set { SetColumnValue(Columns.NumeroPeriodosEstablecimientos, value); }
		}
		  
		[XmlAttribute("Cerrado")]
		[Bindable(true)]
		public bool? Cerrado 
		{
			get { return GetColumnValue<bool?>(Columns.Cerrado); }
			set { SetColumnValue(Columns.Cerrado, value); }
		}

        [XmlAttribute("IvaCurrier")]
        [Bindable(true)]
        public decimal? IvaCurrier
        {
            get { return GetColumnValue<decimal?>(Columns.IvaCurrier); }
            set { SetColumnValue(Columns.IvaCurrier, value); }
        }
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasCollection BodegasRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasCollection().Where(Bodegas.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.FacturasCollection FacturasRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.FacturasCollection().Where(Facturas.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.FacturasProveedorPuntosCollection FacturasProveedorPuntosRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.FacturasProveedorPuntosCollection().Where(FacturasProveedorPuntos.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.FacturasBancoCollection FacturasBancoRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.FacturasBancoCollection().Where(FacturasBanco.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MarcasCollection MarcasRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.MarcasCollection().Where(Marcas.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.PeriodoFacturacionPuntosCollection PeriodoFacturacionPuntosRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.PeriodoFacturacionPuntosCollection().Where(PeriodoFacturacionPuntos.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.PreciosBaseProgramaCollection PreciosBaseProgramaRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.PreciosBaseProgramaCollection().Where(PreciosBasePrograma.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.PrefacturasCollection PrefacturasRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.PrefacturasCollection().Where(Prefacturas.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ReferenciasProductoCollection ReferenciasProductoRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.ReferenciasProductoCollection().Where(ReferenciasProducto.Columns.IdPrograma, IdPrograma).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.SiniestrosCollection SiniestrosRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.SiniestrosCollection().Where(Siniestros.Columns.IdPrograma, IdPrograma).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Empresas ActiveRecord object related to this Programas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Empresas Empresas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Empresas.FetchByID(this.IdEmpresa); }
			set { SetColumnValue("ID_EMPRESA", value.IdEmpresa); }
		}
		
		
		/// <summary>
		/// Returns a Resoluciones ActiveRecord object related to this Programas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Resoluciones Resoluciones
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Resoluciones.FetchByID(this.IdResolucion); }
			set { SetColumnValue("ID_RESOLUCION", value.IdResolucion); }
		}
		
		
		/// <summary>
		/// Returns a Resoluciones ActiveRecord object related to this Programas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Resoluciones ResolucionesToIdNotaCredito
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Resoluciones.FetchByID(this.IdNotaCredito); }
			set { SetColumnValue("ID_NOTA_CREDITO", value.IdResolucion); }
		}
		
		
		/// <summary>
		/// Returns a Resoluciones ActiveRecord object related to this Programas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Resoluciones ResolucionesToIdResolucionBanco
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Resoluciones.FetchByID(this.IdResolucionBanco); }
			set { SetColumnValue("ID_RESOLUCION_BANCO", value.IdResolucion); }
		}
		
		
		/// <summary>
		/// Returns a MasTiposFacturacion ActiveRecord object related to this Programas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposFacturacion MasTiposFacturacion
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposFacturacion.FetchByID(this.IdTipoFacturacion); }
			set { SetColumnValue("ID_TIPO_FACTURACION", value.IdTipoFacturacion); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdPrograma,string varNombrePrograma,int varIdEmpresa,DateTime varFechaInicio,int? varIdResolucion,int? varIdNotaCredito,int? varIdResolucionBanco,decimal varFactorPunto,int varRedondeo,bool varFacturaProveedorPuntos,decimal varPorcentajeComision,bool varCobrarEnvio,int? varPlazoVencimientoFactura,int? varIdTipoFacturacion,int? varNumeroPeriodosProveedorPuntos,int? varNumeroPeriodosEstablecimientos,bool? varCerrado)
		{
			Programas item = new Programas();
			
			item.IdPrograma = varIdPrograma;
			
			item.NombrePrograma = varNombrePrograma;
			
			item.IdEmpresa = varIdEmpresa;
			
			item.FechaInicio = varFechaInicio;
			
			item.IdResolucion = varIdResolucion;
			
			item.IdNotaCredito = varIdNotaCredito;
			
			item.IdResolucionBanco = varIdResolucionBanco;
			
			item.FactorPunto = varFactorPunto;
			
			item.Redondeo = varRedondeo;
			
			item.FacturaProveedorPuntos = varFacturaProveedorPuntos;
			
			item.PorcentajeComision = varPorcentajeComision;
			
			item.CobrarEnvio = varCobrarEnvio;
			
			item.PlazoVencimientoFactura = varPlazoVencimientoFactura;
			
			item.IdTipoFacturacion = varIdTipoFacturacion;
			
			item.NumeroPeriodosProveedorPuntos = varNumeroPeriodosProveedorPuntos;
			
			item.NumeroPeriodosEstablecimientos = varNumeroPeriodosEstablecimientos;
			
			item.Cerrado = varCerrado;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdPrograma,string varNombrePrograma,int varIdEmpresa,DateTime varFechaInicio,int? varIdResolucion,int? varIdNotaCredito,int? varIdResolucionBanco,decimal varFactorPunto,int varRedondeo,bool varFacturaProveedorPuntos,decimal varPorcentajeComision,bool varCobrarEnvio,int? varPlazoVencimientoFactura,int? varIdTipoFacturacion,int? varNumeroPeriodosProveedorPuntos,int? varNumeroPeriodosEstablecimientos,bool? varCerrado)
		{
			Programas item = new Programas();
			
				item.IdPrograma = varIdPrograma;
			
				item.NombrePrograma = varNombrePrograma;
			
				item.IdEmpresa = varIdEmpresa;
			
				item.FechaInicio = varFechaInicio;
			
				item.IdResolucion = varIdResolucion;
			
				item.IdNotaCredito = varIdNotaCredito;
			
				item.IdResolucionBanco = varIdResolucionBanco;
			
				item.FactorPunto = varFactorPunto;
			
				item.Redondeo = varRedondeo;
			
				item.FacturaProveedorPuntos = varFacturaProveedorPuntos;
			
				item.PorcentajeComision = varPorcentajeComision;
			
				item.CobrarEnvio = varCobrarEnvio;
			
				item.PlazoVencimientoFactura = varPlazoVencimientoFactura;
			
				item.IdTipoFacturacion = varIdTipoFacturacion;
			
				item.NumeroPeriodosProveedorPuntos = varNumeroPeriodosProveedorPuntos;
			
				item.NumeroPeriodosEstablecimientos = varNumeroPeriodosEstablecimientos;
			
				item.Cerrado = varCerrado;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreProgramaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEmpresaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaInicioColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdResolucionColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdNotaCreditoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdResolucionBancoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FactorPuntoColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn RedondeoColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn FacturaProveedorPuntosColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn PorcentajeComisionColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CobrarEnvioColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn PlazoVencimientoFacturaColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoFacturacionColumn
        {
            get { return Schema.Columns[13]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroPeriodosProveedorPuntosColumn
        {
            get { return Schema.Columns[14]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroPeriodosEstablecimientosColumn
        {
            get { return Schema.Columns[15]; }
        }
        
        
        
        public static TableSchema.TableColumn CerradoColumn
        {
            get { return Schema.Columns[16]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string NombrePrograma = @"NOMBRE_PROGRAMA";
			 public static string IdEmpresa = @"ID_EMPRESA";
			 public static string FechaInicio = @"FECHA_INICIO";
			 public static string IdResolucion = @"ID_RESOLUCION";
			 public static string IdNotaCredito = @"ID_NOTA_CREDITO";
			 public static string IdResolucionBanco = @"ID_RESOLUCION_BANCO";
			 public static string FactorPunto = @"FACTOR_PUNTO";
			 public static string Redondeo = @"REDONDEO";
			 public static string FacturaProveedorPuntos = @"FACTURA_PROVEEDOR_PUNTOS";
			 public static string PorcentajeComision = @"PORCENTAJE_COMISION";
			 public static string CobrarEnvio = @"COBRAR_ENVIO";
			 public static string PlazoVencimientoFactura = @"PLAZO_VENCIMIENTO_FACTURA";
			 public static string IdTipoFacturacion = @"ID_TIPO_FACTURACION";
			 public static string NumeroPeriodosProveedorPuntos = @"NUMERO_PERIODOS_PROVEEDOR_PUNTOS";
			 public static string NumeroPeriodosEstablecimientos = @"NUMERO_PERIODOS_ESTABLECIMIENTOS";
			 public static string Cerrado = @"CERRADO";
             public static string IvaCurrier = @"IVA_CURRIER";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
