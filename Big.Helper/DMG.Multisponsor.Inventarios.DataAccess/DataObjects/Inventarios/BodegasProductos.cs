using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the BodegasProductos class.
	/// </summary>
    [Serializable]
	public partial class BodegasProductosCollection : ActiveList<BodegasProductos, BodegasProductosCollection>
	{	   
		public BodegasProductosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BodegasProductosCollection</returns>
		public BodegasProductosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                BodegasProductos o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_BODEGAS_PRODUCTOS table.
	/// </summary>
	[Serializable]
	public partial class BodegasProductos : ActiveRecord<BodegasProductos>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public BodegasProductos()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public BodegasProductos(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public BodegasProductos(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public BodegasProductos(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_BODEGAS_PRODUCTOS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdBodegaProducto = new TableSchema.TableColumn(schema);
				colvarIdBodegaProducto.ColumnName = "ID_BODEGA_PRODUCTO";
				colvarIdBodegaProducto.DataType = DbType.Decimal;
				colvarIdBodegaProducto.MaxLength = 0;
				colvarIdBodegaProducto.AutoIncrement = true;
				colvarIdBodegaProducto.IsNullable = false;
				colvarIdBodegaProducto.IsPrimaryKey = true;
				colvarIdBodegaProducto.IsForeignKey = false;
				colvarIdBodegaProducto.IsReadOnly = false;
				colvarIdBodegaProducto.DefaultSetting = @"";
				colvarIdBodegaProducto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdBodegaProducto);
				
				TableSchema.TableColumn colvarIdBodega = new TableSchema.TableColumn(schema);
				colvarIdBodega.ColumnName = "ID_BODEGA";
				colvarIdBodega.DataType = DbType.Int32;
				colvarIdBodega.MaxLength = 0;
				colvarIdBodega.AutoIncrement = false;
				colvarIdBodega.IsNullable = false;
				colvarIdBodega.IsPrimaryKey = false;
				colvarIdBodega.IsForeignKey = true;
				colvarIdBodega.IsReadOnly = false;
				colvarIdBodega.DefaultSetting = @"";
				
					colvarIdBodega.ForeignKeyTableName = "INV_BODEGAS";
				schema.Columns.Add(colvarIdBodega);
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = false;
				colvarGuidReferenciaProducto.IsForeignKey = true;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				
					colvarGuidReferenciaProducto.ForeignKeyTableName = "INV_REFERENCIAS_PRODUCTO";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Decimal;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_BODEGAS_PRODUCTOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdBodegaProducto")]
		[Bindable(true)]
		public decimal IdBodegaProducto 
		{
			get { return GetColumnValue<decimal>(Columns.IdBodegaProducto); }
			set { SetColumnValue(Columns.IdBodegaProducto, value); }
		}
		  
		[XmlAttribute("IdBodega")]
		[Bindable(true)]
		public int IdBodega 
		{
			get { return GetColumnValue<int>(Columns.IdBodega); }
			set { SetColumnValue(Columns.IdBodega, value); }
		}
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public decimal Cantidad 
		{
			get { return GetColumnValue<decimal>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodegaCollection HistoricoBodegaRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodegaCollection().Where(HistoricoBodega.Columns.IdBodegaProducto, IdBodegaProducto).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a ReferenciasProducto ActiveRecord object related to this BodegasProductos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ReferenciasProducto ReferenciasProducto
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.ReferenciasProducto.FetchByID(this.GuidReferenciaProducto); }
			set { SetColumnValue("GUID_REFERENCIA_PRODUCTO", value.GuidReferenciaProducto); }
		}
		
		
		/// <summary>
		/// Returns a Bodegas ActiveRecord object related to this BodegasProductos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Bodegas Bodegas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Bodegas.FetchByID(this.IdBodega); }
			set { SetColumnValue("ID_BODEGA", value.IdBodega); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdBodega,string varGuidReferenciaProducto,decimal varCantidad)
		{
			BodegasProductos item = new BodegasProductos();
			
			item.IdBodega = varIdBodega;
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.Cantidad = varCantidad;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(decimal varIdBodegaProducto,int varIdBodega,string varGuidReferenciaProducto,decimal varCantidad)
		{
			BodegasProductos item = new BodegasProductos();
			
				item.IdBodegaProducto = varIdBodegaProducto;
			
				item.IdBodega = varIdBodega;
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
				item.Cantidad = varCantidad;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdBodegaProductoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdBodegaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdBodegaProducto = @"ID_BODEGA_PRODUCTO";
			 public static string IdBodega = @"ID_BODEGA";
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
			 public static string Cantidad = @"CANTIDAD";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
