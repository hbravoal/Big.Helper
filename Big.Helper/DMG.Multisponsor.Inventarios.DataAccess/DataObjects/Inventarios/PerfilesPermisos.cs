using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the PerfilesPermisos class.
	/// </summary>
    [Serializable]
	public partial class PerfilesPermisosCollection : ActiveList<PerfilesPermisos, PerfilesPermisosCollection>
	{	   
		public PerfilesPermisosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>PerfilesPermisosCollection</returns>
		public PerfilesPermisosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                PerfilesPermisos o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_PERFILES_PERMISOS table.
	/// </summary>
	[Serializable]
	public partial class PerfilesPermisos : ActiveRecord<PerfilesPermisos>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public PerfilesPermisos()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public PerfilesPermisos(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public PerfilesPermisos(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public PerfilesPermisos(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_PERFILES_PERMISOS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdPefilPermiso = new TableSchema.TableColumn(schema);
				colvarIdPefilPermiso.ColumnName = "ID_PEFIL_PERMISO";
				colvarIdPefilPermiso.DataType = DbType.Decimal;
				colvarIdPefilPermiso.MaxLength = 0;
				colvarIdPefilPermiso.AutoIncrement = true;
				colvarIdPefilPermiso.IsNullable = false;
				colvarIdPefilPermiso.IsPrimaryKey = true;
				colvarIdPefilPermiso.IsForeignKey = false;
				colvarIdPefilPermiso.IsReadOnly = false;
				colvarIdPefilPermiso.DefaultSetting = @"";
				colvarIdPefilPermiso.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPefilPermiso);
				
				TableSchema.TableColumn colvarIdPerfil = new TableSchema.TableColumn(schema);
				colvarIdPerfil.ColumnName = "ID_PERFIL";
				colvarIdPerfil.DataType = DbType.Int32;
				colvarIdPerfil.MaxLength = 0;
				colvarIdPerfil.AutoIncrement = false;
				colvarIdPerfil.IsNullable = false;
				colvarIdPerfil.IsPrimaryKey = false;
				colvarIdPerfil.IsForeignKey = true;
				colvarIdPerfil.IsReadOnly = false;
				colvarIdPerfil.DefaultSetting = @"";
				
					colvarIdPerfil.ForeignKeyTableName = "INV_PERFILES";
				schema.Columns.Add(colvarIdPerfil);
				
				TableSchema.TableColumn colvarIdPermiso = new TableSchema.TableColumn(schema);
				colvarIdPermiso.ColumnName = "ID_PERMISO";
				colvarIdPermiso.DataType = DbType.Int32;
				colvarIdPermiso.MaxLength = 0;
				colvarIdPermiso.AutoIncrement = false;
				colvarIdPermiso.IsNullable = false;
				colvarIdPermiso.IsPrimaryKey = false;
				colvarIdPermiso.IsForeignKey = true;
				colvarIdPermiso.IsReadOnly = false;
				colvarIdPermiso.DefaultSetting = @"";
				
					colvarIdPermiso.ForeignKeyTableName = "INV_MAS_PERMISOS";
				schema.Columns.Add(colvarIdPermiso);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_PERFILES_PERMISOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdPefilPermiso")]
		[Bindable(true)]
		public decimal IdPefilPermiso 
		{
			get { return GetColumnValue<decimal>(Columns.IdPefilPermiso); }
			set { SetColumnValue(Columns.IdPefilPermiso, value); }
		}
		  
		[XmlAttribute("IdPerfil")]
		[Bindable(true)]
		public int IdPerfil 
		{
			get { return GetColumnValue<int>(Columns.IdPerfil); }
			set { SetColumnValue(Columns.IdPerfil, value); }
		}
		  
		[XmlAttribute("IdPermiso")]
		[Bindable(true)]
		public int IdPermiso 
		{
			get { return GetColumnValue<int>(Columns.IdPermiso); }
			set { SetColumnValue(Columns.IdPermiso, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a MasPermisos ActiveRecord object related to this PerfilesPermisos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasPermisos MasPermisos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasPermisos.FetchByID(this.IdPermiso); }
			set { SetColumnValue("ID_PERMISO", value.IdPermiso); }
		}
		
		
		/// <summary>
		/// Returns a Perfiles ActiveRecord object related to this PerfilesPermisos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Perfiles Perfiles
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Perfiles.FetchByID(this.IdPerfil); }
			set { SetColumnValue("ID_PERFIL", value.IdPerfil); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdPerfil,int varIdPermiso)
		{
			PerfilesPermisos item = new PerfilesPermisos();
			
			item.IdPerfil = varIdPerfil;
			
			item.IdPermiso = varIdPermiso;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(decimal varIdPefilPermiso,int varIdPerfil,int varIdPermiso)
		{
			PerfilesPermisos item = new PerfilesPermisos();
			
				item.IdPefilPermiso = varIdPefilPermiso;
			
				item.IdPerfil = varIdPerfil;
			
				item.IdPermiso = varIdPermiso;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdPefilPermisoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPerfilColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPermisoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdPefilPermiso = @"ID_PEFIL_PERMISO";
			 public static string IdPerfil = @"ID_PERFIL";
			 public static string IdPermiso = @"ID_PERMISO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
