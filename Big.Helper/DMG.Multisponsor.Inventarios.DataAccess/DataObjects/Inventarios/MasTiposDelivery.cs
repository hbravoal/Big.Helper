using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the MasTiposDelivery class.
	/// </summary>
    [Serializable]
	public partial class MasTiposDeliveryCollection : ActiveList<MasTiposDelivery, MasTiposDeliveryCollection>
	{	   
		public MasTiposDeliveryCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasTiposDeliveryCollection</returns>
		public MasTiposDeliveryCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasTiposDelivery o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_TIPOS_DELIVERY table.
	/// </summary>
	[Serializable]
	public partial class MasTiposDelivery : ActiveRecord<MasTiposDelivery>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public MasTiposDelivery()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasTiposDelivery(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public MasTiposDelivery(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public MasTiposDelivery(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_TIPOS_DELIVERY", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdTipoDelivery = new TableSchema.TableColumn(schema);
				colvarIdTipoDelivery.ColumnName = "ID_TIPO_DELIVERY";
				colvarIdTipoDelivery.DataType = DbType.Int32;
				colvarIdTipoDelivery.MaxLength = 0;
				colvarIdTipoDelivery.AutoIncrement = false;
				colvarIdTipoDelivery.IsNullable = false;
				colvarIdTipoDelivery.IsPrimaryKey = true;
				colvarIdTipoDelivery.IsForeignKey = false;
				colvarIdTipoDelivery.IsReadOnly = false;
				colvarIdTipoDelivery.DefaultSetting = @"";
				colvarIdTipoDelivery.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdTipoDelivery);
				
				TableSchema.TableColumn colvarTipoDelivery = new TableSchema.TableColumn(schema);
				colvarTipoDelivery.ColumnName = "TIPO_DELIVERY";
				colvarTipoDelivery.DataType = DbType.String;
				colvarTipoDelivery.MaxLength = 100;
				colvarTipoDelivery.AutoIncrement = false;
				colvarTipoDelivery.IsNullable = false;
				colvarTipoDelivery.IsPrimaryKey = false;
				colvarTipoDelivery.IsForeignKey = false;
				colvarTipoDelivery.IsReadOnly = false;
				colvarTipoDelivery.DefaultSetting = @"";
				colvarTipoDelivery.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTipoDelivery);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_MAS_TIPOS_DELIVERY",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdTipoDelivery")]
		[Bindable(true)]
		public int IdTipoDelivery 
		{
			get { return GetColumnValue<int>(Columns.IdTipoDelivery); }
			set { SetColumnValue(Columns.IdTipoDelivery, value); }
		}
		  
		[XmlAttribute("TipoDelivery")]
		[Bindable(true)]
		public string TipoDelivery 
		{
			get { return GetColumnValue<string>(Columns.TipoDelivery); }
			set { SetColumnValue(Columns.TipoDelivery, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricosPrecioCollection HistoricosPrecioRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricosPrecioCollection().Where(HistoricosPrecio.Columns.IdTipoDelivery, IdTipoDelivery).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.PreciosBaseProgramaCollection PreciosBaseProgramaRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.PreciosBaseProgramaCollection().Where(PreciosBasePrograma.Columns.IdTipoDelivery, IdTipoDelivery).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varIdTipoDelivery,string varTipoDelivery)
		{
			MasTiposDelivery item = new MasTiposDelivery();
			
			item.IdTipoDelivery = varIdTipoDelivery;
			
			item.TipoDelivery = varTipoDelivery;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdTipoDelivery,string varTipoDelivery)
		{
			MasTiposDelivery item = new MasTiposDelivery();
			
				item.IdTipoDelivery = varIdTipoDelivery;
			
				item.TipoDelivery = varTipoDelivery;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdTipoDeliveryColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn TipoDeliveryColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdTipoDelivery = @"ID_TIPO_DELIVERY";
			 public static string TipoDelivery = @"TIPO_DELIVERY";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
