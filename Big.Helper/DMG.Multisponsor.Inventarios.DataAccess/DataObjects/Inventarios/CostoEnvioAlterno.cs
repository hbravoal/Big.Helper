using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the CostoEnvioAlterno class.
	/// </summary>
    [Serializable]
	public partial class CostoEnvioAlternoCollection : ActiveList<CostoEnvioAlterno, CostoEnvioAlternoCollection>
	{	   
		public CostoEnvioAlternoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CostoEnvioAlternoCollection</returns>
		public CostoEnvioAlternoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CostoEnvioAlterno o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_COSTO_ENVIO_ALTERNO table.
	/// </summary>
	[Serializable]
	public partial class CostoEnvioAlterno : ActiveRecord<CostoEnvioAlterno>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CostoEnvioAlterno()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CostoEnvioAlterno(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CostoEnvioAlterno(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CostoEnvioAlterno(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_COSTO_ENVIO_ALTERNO", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.String;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				
						colvarGuid.DefaultSetting = @"(newid())";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdCiudad = new TableSchema.TableColumn(schema);
				colvarIdCiudad.ColumnName = "ID_CIUDAD";
				colvarIdCiudad.DataType = DbType.Int32;
				colvarIdCiudad.MaxLength = 0;
				colvarIdCiudad.AutoIncrement = false;
				colvarIdCiudad.IsNullable = false;
				colvarIdCiudad.IsPrimaryKey = false;
				colvarIdCiudad.IsForeignKey = true;
				colvarIdCiudad.IsReadOnly = false;
				colvarIdCiudad.DefaultSetting = @"";
				
					colvarIdCiudad.ForeignKeyTableName = "INV_MAS_CIUDADES";
				schema.Columns.Add(colvarIdCiudad);
				
				TableSchema.TableColumn colvarGuidProducto = new TableSchema.TableColumn(schema);
				colvarGuidProducto.ColumnName = "GUID_PRODUCTO";
				colvarGuidProducto.DataType = DbType.String;
				colvarGuidProducto.MaxLength = 36;
				colvarGuidProducto.AutoIncrement = false;
				colvarGuidProducto.IsNullable = false;
				colvarGuidProducto.IsPrimaryKey = false;
				colvarGuidProducto.IsForeignKey = true;
				colvarGuidProducto.IsReadOnly = false;
				colvarGuidProducto.DefaultSetting = @"";
				
					colvarGuidProducto.ForeignKeyTableName = "INV_PRODUCTOS";
				schema.Columns.Add(colvarGuidProducto);
				
				TableSchema.TableColumn colvarIdTipoDelivery = new TableSchema.TableColumn(schema);
				colvarIdTipoDelivery.ColumnName = "ID_TIPO_DELIVERY";
				colvarIdTipoDelivery.DataType = DbType.Int32;
				colvarIdTipoDelivery.MaxLength = 0;
				colvarIdTipoDelivery.AutoIncrement = false;
				colvarIdTipoDelivery.IsNullable = false;
				colvarIdTipoDelivery.IsPrimaryKey = false;
				colvarIdTipoDelivery.IsForeignKey = true;
				colvarIdTipoDelivery.IsReadOnly = false;
				colvarIdTipoDelivery.DefaultSetting = @"";
				
					colvarIdTipoDelivery.ForeignKeyTableName = "INV_MAS_TIPOS_DELIVERY";
				schema.Columns.Add(colvarIdTipoDelivery);
				
				TableSchema.TableColumn colvarValorAlterno = new TableSchema.TableColumn(schema);
				colvarValorAlterno.ColumnName = "VALOR_ALTERNO";
				colvarValorAlterno.DataType = DbType.Decimal;
				colvarValorAlterno.MaxLength = 0;
				colvarValorAlterno.AutoIncrement = false;
				colvarValorAlterno.IsNullable = false;
				colvarValorAlterno.IsPrimaryKey = false;
				colvarValorAlterno.IsForeignKey = false;
				colvarValorAlterno.IsReadOnly = false;
				colvarValorAlterno.DefaultSetting = @"";
				colvarValorAlterno.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorAlterno);
				
				TableSchema.TableColumn colvarActivo = new TableSchema.TableColumn(schema);
				colvarActivo.ColumnName = "ACTIVO";
				colvarActivo.DataType = DbType.Boolean;
				colvarActivo.MaxLength = 0;
				colvarActivo.AutoIncrement = false;
				colvarActivo.IsNullable = false;
				colvarActivo.IsPrimaryKey = false;
				colvarActivo.IsForeignKey = false;
				colvarActivo.IsReadOnly = false;
				
						colvarActivo.DefaultSetting = @"((1))";
				colvarActivo.ForeignKeyTableName = "";
				schema.Columns.Add(colvarActivo);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_COSTO_ENVIO_ALTERNO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdCiudad")]
		[Bindable(true)]
		public int IdCiudad 
		{
			get { return GetColumnValue<int>(Columns.IdCiudad); }
			set { SetColumnValue(Columns.IdCiudad, value); }
		}
		  
		[XmlAttribute("GuidProducto")]
		[Bindable(true)]
		public string GuidProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidProducto); }
			set { SetColumnValue(Columns.GuidProducto, value); }
		}
		  
		[XmlAttribute("IdTipoDelivery")]
		[Bindable(true)]
		public int IdTipoDelivery 
		{
			get { return GetColumnValue<int>(Columns.IdTipoDelivery); }
			set { SetColumnValue(Columns.IdTipoDelivery, value); }
		}
		  
		[XmlAttribute("ValorAlterno")]
		[Bindable(true)]
		public decimal ValorAlterno 
		{
			get { return GetColumnValue<decimal>(Columns.ValorAlterno); }
			set { SetColumnValue(Columns.ValorAlterno, value); }
		}
		  
		[XmlAttribute("Activo")]
		[Bindable(true)]
		public bool Activo 
		{
			get { return GetColumnValue<bool>(Columns.Activo); }
			set { SetColumnValue(Columns.Activo, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a MasCiudades ActiveRecord object related to this CostoEnvioAlterno
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasCiudades MasCiudades
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasCiudades.FetchByID(this.IdCiudad); }
			set { SetColumnValue("ID_CIUDAD", value.IdCiudad); }
		}
		
		
		/// <summary>
		/// Returns a MasTiposDelivery ActiveRecord object related to this CostoEnvioAlterno
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposDelivery MasTiposDelivery
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposDelivery.FetchByID(this.IdTipoDelivery); }
			set { SetColumnValue("ID_TIPO_DELIVERY", value.IdTipoDelivery); }
		}
		
		
		/// <summary>
		/// Returns a Productos ActiveRecord object related to this CostoEnvioAlterno
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Productos Productos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Productos.FetchByID(this.GuidProducto); }
			set { SetColumnValue("GUID_PRODUCTO", value.Guid); }
		}
		
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this CostoEnvioAlterno
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,int varIdPrograma,int varIdCiudad,string varGuidProducto,int varIdTipoDelivery,decimal varValorAlterno,bool varActivo)
		{
			CostoEnvioAlterno item = new CostoEnvioAlterno();
			
			item.Guid = varGuid;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdCiudad = varIdCiudad;
			
			item.GuidProducto = varGuidProducto;
			
			item.IdTipoDelivery = varIdTipoDelivery;
			
			item.ValorAlterno = varValorAlterno;
			
			item.Activo = varActivo;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,int varIdPrograma,int varIdCiudad,string varGuidProducto,int varIdTipoDelivery,decimal varValorAlterno,bool varActivo)
		{
			CostoEnvioAlterno item = new CostoEnvioAlterno();
			
				item.Guid = varGuid;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdCiudad = varIdCiudad;
			
				item.GuidProducto = varGuidProducto;
			
				item.IdTipoDelivery = varIdTipoDelivery;
			
				item.ValorAlterno = varValorAlterno;
			
				item.Activo = varActivo;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdCiudadColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidProductoColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoDeliveryColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorAlternoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ActivoColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdCiudad = @"ID_CIUDAD";
			 public static string GuidProducto = @"GUID_PRODUCTO";
			 public static string IdTipoDelivery = @"ID_TIPO_DELIVERY";
			 public static string ValorAlterno = @"VALOR_ALTERNO";
			 public static string Activo = @"ACTIVO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
