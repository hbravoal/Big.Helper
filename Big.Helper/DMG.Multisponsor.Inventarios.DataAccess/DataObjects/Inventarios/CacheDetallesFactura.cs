using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the CacheDetallesFactura class.
	/// </summary>
    [Serializable]
	public partial class CacheDetallesFacturaCollection : ActiveList<CacheDetallesFactura, CacheDetallesFacturaCollection>
	{	   
		public CacheDetallesFacturaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CacheDetallesFacturaCollection</returns>
		public CacheDetallesFacturaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CacheDetallesFactura o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CACHE_DETALLES_FACTURA table.
	/// </summary>
	[Serializable]
	public partial class CacheDetallesFactura : ActiveRecord<CacheDetallesFactura>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CacheDetallesFactura()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CacheDetallesFactura(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CacheDetallesFactura(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CacheDetallesFactura(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CACHE_DETALLES_FACTURA", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdCacheDetalle = new TableSchema.TableColumn(schema);
				colvarIdCacheDetalle.ColumnName = "ID_CACHE_DETALLE";
				colvarIdCacheDetalle.DataType = DbType.Int32;
				colvarIdCacheDetalle.MaxLength = 0;
				colvarIdCacheDetalle.AutoIncrement = true;
				colvarIdCacheDetalle.IsNullable = false;
				colvarIdCacheDetalle.IsPrimaryKey = true;
				colvarIdCacheDetalle.IsForeignKey = false;
				colvarIdCacheDetalle.IsReadOnly = false;
				colvarIdCacheDetalle.DefaultSetting = @"";
				colvarIdCacheDetalle.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdCacheDetalle);
				
				TableSchema.TableColumn colvarCantidad = new TableSchema.TableColumn(schema);
				colvarCantidad.ColumnName = "CANTIDAD";
				colvarCantidad.DataType = DbType.Int32;
				colvarCantidad.MaxLength = 0;
				colvarCantidad.AutoIncrement = false;
				colvarCantidad.IsNullable = false;
				colvarCantidad.IsPrimaryKey = false;
				colvarCantidad.IsForeignKey = false;
				colvarCantidad.IsReadOnly = false;
				colvarCantidad.DefaultSetting = @"";
				colvarCantidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidad);
				
				TableSchema.TableColumn colvarValorBase = new TableSchema.TableColumn(schema);
				colvarValorBase.ColumnName = "VALOR_BASE";
				colvarValorBase.DataType = DbType.Decimal;
				colvarValorBase.MaxLength = 0;
				colvarValorBase.AutoIncrement = false;
				colvarValorBase.IsNullable = false;
				colvarValorBase.IsPrimaryKey = false;
				colvarValorBase.IsForeignKey = false;
				colvarValorBase.IsReadOnly = false;
				colvarValorBase.DefaultSetting = @"";
				colvarValorBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarValorBase);
				
				TableSchema.TableColumn colvarEnvioBase = new TableSchema.TableColumn(schema);
				colvarEnvioBase.ColumnName = "ENVIO_BASE";
				colvarEnvioBase.DataType = DbType.Decimal;
				colvarEnvioBase.MaxLength = 0;
				colvarEnvioBase.AutoIncrement = false;
				colvarEnvioBase.IsNullable = false;
				colvarEnvioBase.IsPrimaryKey = false;
				colvarEnvioBase.IsForeignKey = false;
				colvarEnvioBase.IsReadOnly = false;
				colvarEnvioBase.DefaultSetting = @"";
				colvarEnvioBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEnvioBase);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarGuidReferenciaProducto = new TableSchema.TableColumn(schema);
				colvarGuidReferenciaProducto.ColumnName = "GUID_REFERENCIA_PRODUCTO";
				colvarGuidReferenciaProducto.DataType = DbType.String;
				colvarGuidReferenciaProducto.MaxLength = 36;
				colvarGuidReferenciaProducto.AutoIncrement = false;
				colvarGuidReferenciaProducto.IsNullable = false;
				colvarGuidReferenciaProducto.IsPrimaryKey = false;
				colvarGuidReferenciaProducto.IsForeignKey = true;
				colvarGuidReferenciaProducto.IsReadOnly = false;
				colvarGuidReferenciaProducto.DefaultSetting = @"";
				
					colvarGuidReferenciaProducto.ForeignKeyTableName = "INV_REFERENCIAS_PRODUCTO";
				schema.Columns.Add(colvarGuidReferenciaProducto);
				
				TableSchema.TableColumn colvarIdUsuario = new TableSchema.TableColumn(schema);
				colvarIdUsuario.ColumnName = "ID_USUARIO";
				colvarIdUsuario.DataType = DbType.Int32;
				colvarIdUsuario.MaxLength = 0;
				colvarIdUsuario.AutoIncrement = false;
				colvarIdUsuario.IsNullable = false;
				colvarIdUsuario.IsPrimaryKey = false;
				colvarIdUsuario.IsForeignKey = true;
				colvarIdUsuario.IsReadOnly = false;
				colvarIdUsuario.DefaultSetting = @"";
				
					colvarIdUsuario.ForeignKeyTableName = "INV_USUARIOS";
				schema.Columns.Add(colvarIdUsuario);
				
				TableSchema.TableColumn colvarFactorCompraPuntos = new TableSchema.TableColumn(schema);
				colvarFactorCompraPuntos.ColumnName = "FACTOR_COMPRA_PUNTOS";
				colvarFactorCompraPuntos.DataType = DbType.Decimal;
				colvarFactorCompraPuntos.MaxLength = 0;
				colvarFactorCompraPuntos.AutoIncrement = false;
				colvarFactorCompraPuntos.IsNullable = false;
				colvarFactorCompraPuntos.IsPrimaryKey = false;
				colvarFactorCompraPuntos.IsForeignKey = false;
				colvarFactorCompraPuntos.IsReadOnly = false;
				colvarFactorCompraPuntos.DefaultSetting = @"";
				colvarFactorCompraPuntos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFactorCompraPuntos);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_CACHE_DETALLES_FACTURA",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdCacheDetalle")]
		[Bindable(true)]
		public int IdCacheDetalle 
		{
			get { return GetColumnValue<int>(Columns.IdCacheDetalle); }
			set { SetColumnValue(Columns.IdCacheDetalle, value); }
		}
		  
		[XmlAttribute("Cantidad")]
		[Bindable(true)]
		public int Cantidad 
		{
			get { return GetColumnValue<int>(Columns.Cantidad); }
			set { SetColumnValue(Columns.Cantidad, value); }
		}
		  
		[XmlAttribute("ValorBase")]
		[Bindable(true)]
		public decimal ValorBase 
		{
			get { return GetColumnValue<decimal>(Columns.ValorBase); }
			set { SetColumnValue(Columns.ValorBase, value); }
		}
		  
		[XmlAttribute("EnvioBase")]
		[Bindable(true)]
		public decimal EnvioBase 
		{
			get { return GetColumnValue<decimal>(Columns.EnvioBase); }
			set { SetColumnValue(Columns.EnvioBase, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("GuidReferenciaProducto")]
		[Bindable(true)]
		public string GuidReferenciaProducto 
		{
			get { return GetColumnValue<string>(Columns.GuidReferenciaProducto); }
			set { SetColumnValue(Columns.GuidReferenciaProducto, value); }
		}
		  
		[XmlAttribute("IdUsuario")]
		[Bindable(true)]
		public int IdUsuario 
		{
			get { return GetColumnValue<int>(Columns.IdUsuario); }
			set { SetColumnValue(Columns.IdUsuario, value); }
		}
		  
		[XmlAttribute("FactorCompraPuntos")]
		[Bindable(true)]
		public decimal FactorCompraPuntos 
		{
			get { return GetColumnValue<decimal>(Columns.FactorCompraPuntos); }
			set { SetColumnValue(Columns.FactorCompraPuntos, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a ReferenciasProducto ActiveRecord object related to this CacheDetallesFactura
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ReferenciasProducto ReferenciasProducto
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.ReferenciasProducto.FetchByID(this.GuidReferenciaProducto); }
			set { SetColumnValue("GUID_REFERENCIA_PRODUCTO", value.GuidReferenciaProducto); }
		}
		
		
		/// <summary>
		/// Returns a Usuarios ActiveRecord object related to this CacheDetallesFactura
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Usuarios Usuarios
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Usuarios.FetchByID(this.IdUsuario); }
			set { SetColumnValue("ID_USUARIO", value.IdUsuario); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(int varCantidad,decimal varValorBase,decimal varEnvioBase,decimal varIva,string varGuidReferenciaProducto,int varIdUsuario,decimal varFactorCompraPuntos)
		{
			CacheDetallesFactura item = new CacheDetallesFactura();
			
			item.Cantidad = varCantidad;
			
			item.ValorBase = varValorBase;
			
			item.EnvioBase = varEnvioBase;
			
			item.Iva = varIva;
			
			item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
			item.IdUsuario = varIdUsuario;
			
			item.FactorCompraPuntos = varFactorCompraPuntos;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdCacheDetalle,int varCantidad,decimal varValorBase,decimal varEnvioBase,decimal varIva,string varGuidReferenciaProducto,int varIdUsuario,decimal varFactorCompraPuntos)
		{
			CacheDetallesFactura item = new CacheDetallesFactura();
			
				item.IdCacheDetalle = varIdCacheDetalle;
			
				item.Cantidad = varCantidad;
			
				item.ValorBase = varValorBase;
			
				item.EnvioBase = varEnvioBase;
			
				item.Iva = varIva;
			
				item.GuidReferenciaProducto = varGuidReferenciaProducto;
			
				item.IdUsuario = varIdUsuario;
			
				item.FactorCompraPuntos = varFactorCompraPuntos;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdCacheDetalleColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn ValorBaseColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EnvioBaseColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidReferenciaProductoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdUsuarioColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FactorCompraPuntosColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdCacheDetalle = @"ID_CACHE_DETALLE";
			 public static string Cantidad = @"CANTIDAD";
			 public static string ValorBase = @"VALOR_BASE";
			 public static string EnvioBase = @"ENVIO_BASE";
			 public static string Iva = @"IVA";
			 public static string GuidReferenciaProducto = @"GUID_REFERENCIA_PRODUCTO";
			 public static string IdUsuario = @"ID_USUARIO";
			 public static string FactorCompraPuntos = @"FACTOR_COMPRA_PUNTOS";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
