using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the Bodegas class.
	/// </summary>
    [Serializable]
	public partial class BodegasCollection : ActiveList<Bodegas, BodegasCollection>
	{	   
		public BodegasCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>BodegasCollection</returns>
		public BodegasCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Bodegas o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_BODEGAS table.
	/// </summary>
	[Serializable]
	public partial class Bodegas : ActiveRecord<Bodegas>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Bodegas()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Bodegas(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Bodegas(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Bodegas(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_BODEGAS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdBodega = new TableSchema.TableColumn(schema);
				colvarIdBodega.ColumnName = "ID_BODEGA";
				colvarIdBodega.DataType = DbType.Int32;
				colvarIdBodega.MaxLength = 0;
				colvarIdBodega.AutoIncrement = true;
				colvarIdBodega.IsNullable = false;
				colvarIdBodega.IsPrimaryKey = true;
				colvarIdBodega.IsForeignKey = false;
				colvarIdBodega.IsReadOnly = false;
				colvarIdBodega.DefaultSetting = @"";
				colvarIdBodega.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdBodega);
				
				TableSchema.TableColumn colvarNombreBodega = new TableSchema.TableColumn(schema);
				colvarNombreBodega.ColumnName = "NOMBRE_BODEGA";
				colvarNombreBodega.DataType = DbType.String;
				colvarNombreBodega.MaxLength = 100;
				colvarNombreBodega.AutoIncrement = false;
				colvarNombreBodega.IsNullable = false;
				colvarNombreBodega.IsPrimaryKey = false;
				colvarNombreBodega.IsForeignKey = false;
				colvarNombreBodega.IsReadOnly = false;
				colvarNombreBodega.DefaultSetting = @"";
				colvarNombreBodega.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreBodega);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdTipoBodega = new TableSchema.TableColumn(schema);
				colvarIdTipoBodega.ColumnName = "ID_TIPO_BODEGA";
				colvarIdTipoBodega.DataType = DbType.Int32;
				colvarIdTipoBodega.MaxLength = 0;
				colvarIdTipoBodega.AutoIncrement = false;
				colvarIdTipoBodega.IsNullable = false;
				colvarIdTipoBodega.IsPrimaryKey = false;
				colvarIdTipoBodega.IsForeignKey = true;
				colvarIdTipoBodega.IsReadOnly = false;
				colvarIdTipoBodega.DefaultSetting = @"";
				
					colvarIdTipoBodega.ForeignKeyTableName = "INV_MAS_TIPOS_BODEGA";
				schema.Columns.Add(colvarIdTipoBodega);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_BODEGAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdBodega")]
		[Bindable(true)]
		public int IdBodega 
		{
			get { return GetColumnValue<int>(Columns.IdBodega); }
			set { SetColumnValue(Columns.IdBodega, value); }
		}
		  
		[XmlAttribute("NombreBodega")]
		[Bindable(true)]
		public string NombreBodega 
		{
			get { return GetColumnValue<string>(Columns.NombreBodega); }
			set { SetColumnValue(Columns.NombreBodega, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdTipoBodega")]
		[Bindable(true)]
		public int IdTipoBodega 
		{
			get { return GetColumnValue<int>(Columns.IdTipoBodega); }
			set { SetColumnValue(Columns.IdTipoBodega, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection BodegasProductosRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection().Where(BodegasProductos.Columns.IdBodega, IdBodega).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a MasTiposBodega ActiveRecord object related to this Bodegas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposBodega MasTiposBodega
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasTiposBodega.FetchByID(this.IdTipoBodega); }
			set { SetColumnValue("ID_TIPO_BODEGA", value.IdTipoBodega); }
		}
		
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this Bodegas
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombreBodega,int varIdPrograma,int varIdTipoBodega)
		{
			Bodegas item = new Bodegas();
			
			item.NombreBodega = varNombreBodega;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdTipoBodega = varIdTipoBodega;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdBodega,string varNombreBodega,int varIdPrograma,int varIdTipoBodega)
		{
			Bodegas item = new Bodegas();
			
				item.IdBodega = varIdBodega;
			
				item.NombreBodega = varNombreBodega;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdTipoBodega = varIdTipoBodega;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdBodegaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreBodegaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IdTipoBodegaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdBodega = @"ID_BODEGA";
			 public static string NombreBodega = @"NOMBRE_BODEGA";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdTipoBodega = @"ID_TIPO_BODEGA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
