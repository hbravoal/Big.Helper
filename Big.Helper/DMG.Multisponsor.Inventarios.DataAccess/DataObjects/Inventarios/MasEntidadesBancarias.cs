using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the MasEntidadesBancarias class.
	/// </summary>
    [Serializable]
	public partial class MasEntidadesBancariasCollection : ActiveList<MasEntidadesBancarias, MasEntidadesBancariasCollection>
	{	   
		public MasEntidadesBancariasCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>MasEntidadesBancariasCollection</returns>
		public MasEntidadesBancariasCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                MasEntidadesBancarias o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_MAS_ENTIDADES_BANCARIAS table.
	/// </summary>
	[Serializable]
	public partial class MasEntidadesBancarias : ActiveRecord<MasEntidadesBancarias>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public MasEntidadesBancarias()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public MasEntidadesBancarias(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public MasEntidadesBancarias(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public MasEntidadesBancarias(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_MAS_ENTIDADES_BANCARIAS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdEntidadBancaria = new TableSchema.TableColumn(schema);
				colvarIdEntidadBancaria.ColumnName = "ID_ENTIDAD_BANCARIA";
				colvarIdEntidadBancaria.DataType = DbType.Int32;
				colvarIdEntidadBancaria.MaxLength = 0;
				colvarIdEntidadBancaria.AutoIncrement = true;
				colvarIdEntidadBancaria.IsNullable = false;
				colvarIdEntidadBancaria.IsPrimaryKey = true;
				colvarIdEntidadBancaria.IsForeignKey = false;
				colvarIdEntidadBancaria.IsReadOnly = false;
				colvarIdEntidadBancaria.DefaultSetting = @"";
				colvarIdEntidadBancaria.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdEntidadBancaria);
				
				TableSchema.TableColumn colvarNombreEntidad = new TableSchema.TableColumn(schema);
				colvarNombreEntidad.ColumnName = "NOMBRE_ENTIDAD";
				colvarNombreEntidad.DataType = DbType.String;
				colvarNombreEntidad.MaxLength = 100;
				colvarNombreEntidad.AutoIncrement = false;
				colvarNombreEntidad.IsNullable = false;
				colvarNombreEntidad.IsPrimaryKey = false;
				colvarNombreEntidad.IsForeignKey = false;
				colvarNombreEntidad.IsReadOnly = false;
				colvarNombreEntidad.DefaultSetting = @"";
				colvarNombreEntidad.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreEntidad);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_MAS_ENTIDADES_BANCARIAS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdEntidadBancaria")]
		[Bindable(true)]
		public int IdEntidadBancaria 
		{
			get { return GetColumnValue<int>(Columns.IdEntidadBancaria); }
			set { SetColumnValue(Columns.IdEntidadBancaria, value); }
		}
		  
		[XmlAttribute("NombreEntidad")]
		[Bindable(true)]
		public string NombreEntidad 
		{
			get { return GetColumnValue<string>(Columns.NombreEntidad); }
			set { SetColumnValue(Columns.NombreEntidad, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.ProveedoresCollection ProveedoresRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.ProveedoresCollection().Where(Proveedores.Columns.IdEntidadBancaria, IdEntidadBancaria).Load();
		}
		#endregion
		
			
		
		//no foreign key tables defined (0)
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varNombreEntidad)
		{
			MasEntidadesBancarias item = new MasEntidadesBancarias();
			
			item.NombreEntidad = varNombreEntidad;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdEntidadBancaria,string varNombreEntidad)
		{
			MasEntidadesBancarias item = new MasEntidadesBancarias();
			
				item.IdEntidadBancaria = varIdEntidadBancaria;
			
				item.NombreEntidad = varNombreEntidad;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdEntidadBancariaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreEntidadColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdEntidadBancaria = @"ID_ENTIDAD_BANCARIA";
			 public static string NombreEntidad = @"NOMBRE_ENTIDAD";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
