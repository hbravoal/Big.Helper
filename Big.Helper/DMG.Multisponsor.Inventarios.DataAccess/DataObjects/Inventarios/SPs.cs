using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core{
    public partial class SPs{
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_ANULACION_AJUSTES Procedure
        /// </summary>
        public static StoredProcedure AnulacionAjustes(string GUID)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_ANULACION_AJUSTES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID", GUID, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_AW_REFERENCIAS_BODEGA Procedure
        /// </summary>
        public static StoredProcedure AwReferenciasBodega(int? IDBODEGA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_AW_REFERENCIAS_BODEGA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDBODEGA", IDBODEGA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_AW_SELECT_BODEGAS Procedure
        /// </summary>
        public static StoredProcedure AwSelectBodegas(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_AW_SELECT_BODEGAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_AW_SELECT_HISTORICO_BODEGA_REFERENCIA Procedure
        /// </summary>
        public static StoredProcedure AwSelectHistoricoBodegaReferencia(int? IDPROGRAMA, string GUIDREFERENCIA, long? FACTURA, decimal? BODEGAPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_AW_SELECT_HISTORICO_BODEGA_REFERENCIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUIDREFERENCIA", GUIDREFERENCIA, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FACTURA", FACTURA, DbType.Int64, 0, 19);
        	
            sp.Command.AddParameter("@BODEGAPRODUCTO", BODEGAPRODUCTO, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CARGA_CONSULTAR_FACTURA Procedure
        /// </summary>
        public static StoredProcedure CargaConsultarFactura(long? NUMEROFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CARGA_CONSULTAR_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@NUMERO_FACTURA", NUMEROFACTURA, DbType.Int64, 0, 19);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_AJUSTE Procedure
        /// </summary>
        public static StoredProcedure CrearAjuste(string GUIDAJUSTE)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_AJUSTE", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_AJUSTE", GUIDAJUSTE, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_COSTO_ENVIO_ALTERNO_FACTURA Procedure
        /// </summary>
        public static StoredProcedure CrearCostoEnvioAlternoFactura(int? IDPROGRAMA, string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_COSTO_ENVIO_ALTERNO_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_FACTURA Procedure
        /// </summary>
        public static StoredProcedure CrearFactura(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_FACTURA_ESTABLECIMIENTO Procedure
        /// </summary>
        public static StoredProcedure CrearFacturaEstablecimiento(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_FACTURA_ESTABLECIMIENTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_FACTURA_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure CrearFacturaProveedor(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_FACTURA_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_NOTACREDITO Procedure
        /// </summary>
        public static StoredProcedure CrearNotacredito(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_NOTACREDITO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_NOTACREDITO_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure CrearNotacreditoProveedor(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_NOTACREDITO_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_NOTACREDITO_PROVEEDOR_VALOR Procedure
        /// </summary>
        public static StoredProcedure CrearNotacreditoProveedorValor(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_NOTACREDITO_PROVEEDOR_VALOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_CREAR_NOTACREDITOVALOR Procedure
        /// </summary>
        public static StoredProcedure CrearNotacreditovalor(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_CREAR_NOTACREDITOVALOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_DELETE_ALL_TABLES Procedure
        /// </summary>
        public static StoredProcedure DeleteAllTables()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_DELETE_ALL_TABLES", DataService.GetInstance("InventariosProvider"), "");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_DELETE_DETALLES_FACTURA Procedure
        /// </summary>
        public static StoredProcedure DeleteDetallesFactura(string guidFactura)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_DELETE_DETALLES_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@guidFactura", guidFactura, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_DETALLES_PREFACTURA_ESTABLECIMIENTO Procedure
        /// </summary>
        public static StoredProcedure DetallesPrefacturaEstablecimiento(int? IDPERIODO, string GUIDPRODUCTO, int? IDMARCA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_DETALLES_PREFACTURA_ESTABLECIMIENTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_EXPORTAR_PRECIOS Procedure
        /// </summary>
        public static StoredProcedure ExportarPrecios(int? IDPROVEEDOR, int? IDMARCA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_EXPORTAR_PRECIOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_EXPORTAR_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure ExportarProductos(int? IDPROVEEDOR, int? IDTIPOPRODUCTO, int? IDMARCA, string NOMBREPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_EXPORTAR_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_TIPO_PRODUCTO", IDTIPOPRODUCTO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@NOMBRE_PRODUCTO", NOMBREPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_EXPORTAR_PROVEEDORES Procedure
        /// </summary>
        public static StoredProcedure ExportarProveedores(string codigo, string nit, int? estado, int? idProveedor)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_EXPORTAR_PROVEEDORES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@codigo", codigo, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@nit", nit, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@estado", estado, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@idProveedor", idProveedor, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_EXPORTAR_REFERENCIAS Procedure
        /// </summary>
        public static StoredProcedure ExportarReferencias(string GUIDPRODUCTO, int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_EXPORTAR_REFERENCIAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@IDPROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_AJUSTES_BY_CRITERIAS Procedure
        /// </summary>
        public static StoredProcedure GetAjustesByCriterias(int? IDBODEGA, int? IDPROGRAMA, string GUIDPRODUCTO, int? IDPROVEEDOR, int? CONSECUTIVO, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_AJUSTES_BY_CRITERIAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_BODEGA", IDBODEGA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@CONSECUTIVO", CONSECUTIVO, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_CACHE_DETALLES_FACTURA Procedure
        /// </summary>
        public static StoredProcedure GetCacheDetallesFactura(int? IDUSUARIO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_CACHE_DETALLES_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_USUARIO", IDUSUARIO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_CAMBIOS_BASE_PRECIOS Procedure
        /// </summary>
        public static StoredProcedure GetCambiosBasePrecios(string GUIDPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_CAMBIOS_BASE_PRECIOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_CAMBIOS_COSTO Procedure
        /// </summary>
        public static StoredProcedure GetCambiosCosto(DateTime? FECHACAMBIO, string GUIDPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_CAMBIOS_COSTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_CAMBIO", FECHACAMBIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_CAMBIOS_PRECIOS Procedure
        /// </summary>
        public static StoredProcedure GetCambiosPrecios(DateTime? FECHACAMBIO, string GUIDPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_CAMBIOS_PRECIOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_CAMBIO", FECHACAMBIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_COSTOS_ENVIO_ALTERNOS Procedure
        /// </summary>
        public static StoredProcedure GetCostosEnvioAlternos(int? IDPROGRAMA, string GUIDPRODUCTO, int? IDTIPODELIVERY, int? IDCIUDAD)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_COSTOS_ENVIO_ALTERNOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_TIPO_DELIVERY", IDTIPODELIVERY, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_CIUDAD", IDCIUDAD, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_DETALLE_AJUSTES_BY_AJUSTE Procedure
        /// </summary>
        public static StoredProcedure GetDetalleAjustesByAjuste(string GUIDAJUSTE)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_DETALLE_AJUSTES_BY_AJUSTE", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_AJUSTE", GUIDAJUSTE, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FACTURAS_A_ANULAR Procedure
        /// </summary>
        public static StoredProcedure GetFacturasAAnular(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FACTURAS_A_ANULAR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@cedulaCliente", cedulaCliente, DbType.AnsiString, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FACTURAS_A_ANULAR_EMPRESA Procedure
        /// </summary>
        public static StoredProcedure GetFacturasAAnularEmpresa(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FACTURAS_A_ANULAR_EMPRESA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FACTURAS_BETWEEN_RANGOS Procedure
        /// </summary>
        public static StoredProcedure GetFacturasBetweenRangos(int? IDPROGRAMA, int? VALORINICIAL, int? VALORFINAL)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FACTURAS_BETWEEN_RANGOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@VALOR_INICIAL", VALORINICIAL, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@VALOR_FINAL", VALORFINAL, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FACTURAS_BY_CRITERIA Procedure
        /// </summary>
        public static StoredProcedure GetFacturasByCriteria(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, bool? anulada, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FACTURAS_BY_CRITERIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@cedulaCliente", cedulaCliente, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@anulada", anulada, DbType.Boolean, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FACTURAS_CATALOG Procedure
        /// </summary>
        public static StoredProcedure GetFacturasCatalog(int? idPrograma, int? idMarca, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FACTURAS_CATALOG", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@idMarca", idMarca, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@cedulaCliente", cedulaCliente, DbType.AnsiString, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FACTURAS_EMPRESA_BY_CRITERIA Procedure
        /// </summary>
        public static StoredProcedure GetFacturasEmpresaByCriteria(int? idEmpresa, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FACTURAS_EMPRESA_BY_CRITERIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idEmpresa", idEmpresa, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FECHA_INICIAL_EMPRESA Procedure
        /// </summary>
        public static StoredProcedure GetFechaInicialEmpresa(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FECHA_INICIAL_EMPRESA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_FECHA_INICIAL_ESTABLECIMIENTO Procedure
        /// </summary>
        public static StoredProcedure GetFechaInicialEstablecimiento(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_FECHA_INICIAL_ESTABLECIMIENTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_ID_PROGRAMA_BY_GUID_PRODUCTO Procedure
        /// </summary>
        public static StoredProcedure GetIdProgramaByGuidProducto(string GUIDPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_ID_PROGRAMA_BY_GUID_PRODUCTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_NOTA_CREDITO_BY_CRITERIA Procedure
        /// </summary>
        public static StoredProcedure GetNotaCreditoByCriteria(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_NOTA_CREDITO_BY_CRITERIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@cedulaCliente", cedulaCliente, DbType.AnsiString, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_NOTA_CREDITO_EMPRESA_BY_CRITERIA Procedure
        /// </summary>
        public static StoredProcedure GetNotaCreditoEmpresaByCriteria(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_NOTA_CREDITO_EMPRESA_BY_CRITERIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_NOTAS_CREDITO_CATALOG Procedure
        /// </summary>
        public static StoredProcedure GetNotasCreditoCatalog(int? idPrograma, int? idMarca, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_NOTAS_CREDITO_CATALOG", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@idMarca", idMarca, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@numeroFactura", numeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@fechaInicial", fechaInicial, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@fechaFinal", fechaFinal, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@cedulaCliente", cedulaCliente, DbType.AnsiString, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_PRODUCTOS_BY_PROV_AND_MARCA Procedure
        /// </summary>
        public static StoredProcedure GetProductosByProvAndMarca(int? IDPROVEEDOR, int? IDMARCA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_PRODUCTOS_BY_PROV_AND_MARCA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_PRODUCTOS_FROM_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure GetProductosFromPrograma(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_PRODUCTOS_FROM_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_PRODUCTOS_FROM_PROGRAMA_AJUSTES Procedure
        /// </summary>
        public static StoredProcedure GetProductosFromProgramaAjustes(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_PRODUCTOS_FROM_PROGRAMA_AJUSTES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_PRODUCTOS_FROM_PROGRAMA_CANTIDAD Procedure
        /// </summary>
        public static StoredProcedure GetProductosFromProgramaCantidad(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_PRODUCTOS_FROM_PROGRAMA_CANTIDAD", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_PRODUCTOS_PREFACTURABLES_BY_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure GetProductosPrefacturablesByProveedor(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA, int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_PRODUCTOS_PREFACTURABLES_BY_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_PROVEEDORES_PREFACTURACION_BY_FECHA Procedure
        /// </summary>
        public static StoredProcedure GetProveedoresPrefacturacionByFecha(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_PROVEEDORES_PREFACTURACION_BY_FECHA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_REFERENCIAS_ACTIVAS_CON_EXISTENCIAS_POR_PRODUCTO Procedure
        /// </summary>
        public static StoredProcedure GetReferenciasActivasConExistenciasPorProducto(string GUIDPRODUCTO, int? IDESTADO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_REFERENCIAS_ACTIVAS_CON_EXISTENCIAS_POR_PRODUCTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_ESTADO", IDESTADO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_REFERENCIAS_ACTIVAS_POR_PRODUCTO Procedure
        /// </summary>
        public static StoredProcedure GetReferenciasActivasPorProducto(string GUIDPRODUCTO, int? IDESTADO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_REFERENCIAS_ACTIVAS_POR_PRODUCTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_ESTADO", IDESTADO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_REFERENCIAS_POR_PRODUCTO Procedure
        /// </summary>
        public static StoredProcedure GetReferenciasPorProducto(string GUIDPRODUCTO, int? IDESTADO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_REFERENCIAS_POR_PRODUCTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_ESTADO", IDESTADO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_REFERENCIAS_POR_PRODUCTO_POR_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure GetReferenciasPorProductoPorPrograma(string GUIDPRODUCTO, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_REFERENCIAS_POR_PRODUCTO_POR_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_REPORTE_FACTURACION Procedure
        /// </summary>
        public static StoredProcedure GetReporteFacturacion(int? NUMEROFACTURA, string NIT, DateTime? FECHAINICIAL, DateTime? FECHAFINAL, int? IDPROGRAMA, int? IDPROVEEDOR, string GUIDPRODUCTO, decimal? PORCENTAJEPUNTOS, bool? ANULADA, bool? NOTACREDITO, int? PAGESIZE, int? PAGEINDEX, long? ROWCOUNT)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_REPORTE_FACTURACION", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@NUMERO_FACTURA", NUMEROFACTURA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@NIT", NIT, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUIDPRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@PORCENTAJE_PUNTOS", PORCENTAJEPUNTOS, DbType.Decimal, 2, 2);
        	
            sp.Command.AddParameter("@ANULADA", ANULADA, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@NOTACREDITO", NOTACREDITO, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@PAGE_SIZE", PAGESIZE, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@PAGE_INDEX", PAGEINDEX, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@ROW_COUNT", DbType.Int64, 0, 19);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_GET_REPORTE_FACTURACION_CHARTS Procedure
        /// </summary>
        public static StoredProcedure GetReporteFacturacionCharts(DateTime? FECHAINICIAL, DateTime? FECHAFINAL, int? IDPROGRAMA, int? TIPO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_GET_REPORTE_FACTURACION_CHARTS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@TIPO", TIPO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_INSERT_HISTORICO_PRECIOS_BY_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure InsertHistoricoPreciosByPrograma(string PERIODO, int? USER, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_INSERT_HISTORICO_PRECIOS_BY_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@PERIODO", PERIODO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@USER", USER, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PREFACTURA_ESTABLECIMIENTO Procedure
        /// </summary>
        public static StoredProcedure PrefacturaEstablecimiento(int? IDMARCA, int? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PREFACTURA_ESTABLECIMIENTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PREFACTURA_GET_PERIODOS_PREFACTURADOS Procedure
        /// </summary>
        public static StoredProcedure PrefacturaGetPeriodosPrefacturados(int? IDPROGRAMA, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PREFACTURA_GET_PERIODOS_PREFACTURADOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PREFACTURA_GET_PRIMER_DETALLE Procedure
        /// </summary>
        public static StoredProcedure PrefacturaGetPrimerDetalle(int? IDPROGRAMA, int? IDPREFACTURA, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PREFACTURA_GET_PRIMER_DETALLE", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PREFACTURA", IDPREFACTURA, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PREFACTURA_GET_PRIMER_DETALLE_MARCA Procedure
        /// </summary>
        public static StoredProcedure PrefacturaGetPrimerDetalleMarca(int? IDPROGRAMA, int? IDMARCA, int? IDPERIODOPREFACTURADO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PREFACTURA_GET_PRIMER_DETALLE_MARCA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PERIODO_PREFACTURADO", IDPERIODOPREFACTURADO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PREFACTURA_GET_SEGUNDO_DETALLE Procedure
        /// </summary>
        public static StoredProcedure PrefacturaGetSegundoDetalle(string GUIDDETALLEFACTURABANCO, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PREFACTURA_GET_SEGUNDO_DETALLE", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_DETALLE_FACTURA_BANCO", GUIDDETALLEFACTURABANCO, DbType.String, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PROVEEDOR_PUNTOS_GET_DETALLES_TRANSACCION Procedure
        /// </summary>
        public static StoredProcedure ProveedorPuntosGetDetallesTransaccion(int? IDPERIODO, string GUIDPRODUCTO, decimal? FACTORCOMPRA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_DETALLES_TRANSACCION", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FACTOR_COMPRA", FACTORCOMPRA, DbType.Decimal, 2, 3);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PROVEEDOR_PUNTOS_GET_DETALLES_TRANSACCION_CURRIER Procedure
        /// </summary>
        public static StoredProcedure ProveedorPuntosGetDetallesTransaccionCurrier(int? IDPERIODO, string GUIDPRODUCTO, decimal? FACTORCOMPRA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_DETALLES_TRANSACCION_CURRIER", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FACTOR_COMPRA", FACTORCOMPRA, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO Procedure
        /// </summary>
        public static StoredProcedure ProveedorPuntosGetTransaccionesPeriodo(int? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO_CURRIER Procedure
        /// </summary>
        public static StoredProcedure ProveedorPuntosGetTransaccionesPeriodoCurrier(int? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO_CURRIER", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO_DINERO Procedure
        /// </summary>
        public static StoredProcedure ProveedorPuntosGetTransaccionesPeriodoDinero(int? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO_DINERO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_REPLICACION_MARCAR_PETICIONES_PROCESADAS Procedure
        /// </summary>
        public static StoredProcedure ReplicacionMarcarPeticionesProcesadas(int? IDPETICION)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_REPLICACION_MARCAR_PETICIONES_PROCESADAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PETICION", IDPETICION, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_REPLICACION_OBTENER_PETICIONES Procedure
        /// </summary>
        public static StoredProcedure ReplicacionObtenerPeticiones()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_REPLICACION_OBTENER_PETICIONES", DataService.GetInstance("InventariosProvider"), "");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_REPORT_GET_ALL_DETALLES_PREFACTURA Procedure
        /// </summary>
        public static StoredProcedure ReportGetAllDetallesPrefactura(int? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_REPORT_GET_ALL_DETALLES_PREFACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.CommandTimeout = 18000;
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
        	
            return sp;
        }

        public static StoredProcedure ReportGetAllDetallesPrefacturaByCedula(int? IDPERIODO, string CEDULA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_REPORT_GET_ALL_DETALLES_PREFACTURA_BY_CEDULA", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.CommandTimeout = 18000;
            sp.Command.AddParameter("@ID_PERIODO", IDPERIODO, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@CEDULA", CEDULA, DbType.String, 0, 10);

            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SALDOS_CONTABLE Procedure
        /// </summary>
        public static StoredProcedure SaldosContable(int? IDPROGRAMA, string GUIDPRODUCTO, string GUIDREFERENCIAPRODUCTO, int? IDPROVEEDOR, int? IDMARCA, DateTime? FECHAINICIAL, DateTime? FECHAFINAL, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SALDOS_CONTABLE", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@GUID_REFERENCIA_PRODUCTO", GUIDREFERENCIAPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SALDOS_TIENDA Procedure
        /// </summary>
        public static StoredProcedure SaldosTienda(int? IDPROGRAMA, string GUIDPRODUCTO, string GUIDREFERENCIAPRODUCTO, int? IDPROVEEDOR, int? IDMARCA, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SALDOS_TIENDA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@GUID_REFERENCIA_PRODUCTO", GUIDREFERENCIAPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT__PERIODO_MES Procedure
        /// </summary>
        public static StoredProcedure SelectPeriodoMes(int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT__PERIODO_MES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_BODEGAS Procedure
        /// </summary>
        public static StoredProcedure SelectBodegas(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_BODEGAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }

        public static StoredProcedure SelectReferenciasProductosByBodega(int idBodega)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_REFERENCIAS_PRODUCTOS_BY_BODEGA", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@ID_BODEGA", idBodega, DbType.Int32, 0, 10);

            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_BODEGAS_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure SelectBodegasPrograma(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_BODEGAS_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }

        public static StoredProcedure SelectBodegas(int? IDPROGRAMA, int IDTIPO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_BODEGAS_PROGRAMA_TIPO", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@ID_TIPO", IDTIPO, DbType.Int32, 0, 10);
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_CLIENTES Procedure
        /// </summary>
        public static StoredProcedure SelectClientes(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_CLIENTES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_DEPARTAMENTO_PAIS Procedure
        /// </summary>
        public static StoredProcedure SelectDepartamentoPais(int? idCiudad)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_DEPARTAMENTO_PAIS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idCiudad", idCiudad, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_DETALLE_ENTRADAS_SALIDAS_SALDOS Procedure
        /// </summary>
        public static StoredProcedure SelectDetalleEntradasSalidasSaldos(bool? ENTRADA, string GUIDREFERENCIA, DateTime? FECHAINICIAL, DateTime? FECHAFINAL, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_DETALLE_ENTRADAS_SALIDAS_SALDOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ENTRADA", ENTRADA, DbType.Boolean, null, null);
        	
            sp.Command.AddParameter("@GUID_REFERENCIA", GUIDREFERENCIA, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_DETALLE_FACTURA_BY_FACTURA Procedure
        /// </summary>
        public static StoredProcedure SelectDetalleFacturaByFactura(string guidFactura)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_DETALLE_FACTURA_BY_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@guidFactura", guidFactura, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_DETALLE_FACTURA_BY_FACTURA_BANCO Procedure
        /// </summary>
        public static StoredProcedure SelectDetalleFacturaByFacturaBanco(string guidFactura)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_DETALLE_FACTURA_BY_FACTURA_BANCO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@guidFactura", guidFactura, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_DETALLE_FACTURA_EMPRESA_BY_FACTURA Procedure
        /// </summary>
        public static StoredProcedure SelectDetalleFacturaEmpresaByFactura(string guidFactura)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_DETALLE_FACTURA_EMPRESA_BY_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@guidFactura", guidFactura, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_EMPRESAS Procedure
        /// </summary>
        public static StoredProcedure SelectEmpresas(string codigo, string nit, string razonSocial, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_EMPRESAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@codigo", codigo, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@nit", nit, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@razonSocial", razonSocial, DbType.AnsiString, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_EMPRESAS_PROGRAMAS Procedure
        /// </summary>
        public static StoredProcedure SelectEmpresasProgramas(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_EMPRESAS_PROGRAMAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ENTIDADES_BANCARIAS Procedure
        /// </summary>
        public static StoredProcedure SelectEntidadesBancarias(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ENTIDADES_BANCARIAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ESTADOS Procedure
        /// </summary>
        public static StoredProcedure SelectEstados(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ESTADOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ESTADOS_PROVEEDORES Procedure
        /// </summary>
        public static StoredProcedure SelectEstadosProveedores(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ESTADOS_PROVEEDORES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ESTADOS_TIPOS_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectEstadosTiposProductos(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ESTADOS_TIPOS_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTOR_PUNTO Procedure
        /// </summary>
        public static StoredProcedure SelectFactorPunto(string GUIDDETALLE)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTOR_PUNTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_DETALLE", GUIDDETALLE, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS Procedure
        /// </summary>
        public static StoredProcedure SelectFacturas(int? ParametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ParametroFantasma", ParametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_BODEGAS_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasBodegasProductos(DateTime? FECHAINICIAL, DateTime? FECHAFINAL)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_BODEGAS_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);
        	
            return sp;
        }

        public static StoredProcedure SelectFacturasBodegasProductosAndFactura(string numeroFactura, DateTime? FECHAINICIAL, DateTime? FECHAFINAL)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_BODEGAS_PRODUCTOS_BY_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@NUMERO_FACTURA", numeroFactura, DbType.String, null, null);

            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);

            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);

            return sp;
        }

        public static StoredProcedure SelectFacturasBodegasProductosAndFacturaV2(string numeroFactura,int idProveedor, DateTime? FECHAINICIAL, DateTime? FECHAFINAL)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_BODEGAS_PRODUCTOS_BY_FACTURA_V2", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@NUMERO_FACTURA", numeroFactura, DbType.String, null, null);

            sp.Command.AddParameter("@ID_PROVEEDOR", idProveedor, DbType.Int32, null, null);
            
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);

            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);

            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_FECHAS_PREFACTURACION Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasFechasPrefacturacion(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_FECHAS_PREFACTURACION", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasPrograma(int? NumeroFactura, int? Programa)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@NumeroFactura", NumeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Programa", Programa, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_PROGRAMA_DETALLE Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasProgramaDetalle(int? NumeroFactura, int? Programa)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_PROGRAMA_DETALLE", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@NumeroFactura", NumeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Programa", Programa, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_PROGRAMA_PRODUCTO Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasProgramaProducto(string GUIDPRODUCTO, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_PROGRAMA_PRODUCTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_PROGRAMA_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasProgramaProveedor(int? NumeroFactura, int? Programa)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_PROGRAMA_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@NumeroFactura", NumeroFactura, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Programa", Programa, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FACTURAS_RANGO_FECHAS Procedure
        /// </summary>
        public static StoredProcedure SelectFacturasRangoFechas(DateTime? FechaInicio, DateTime? FechaFin, int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FACTURAS_RANGO_FECHAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FechaInicio", FechaInicio, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FechaFin", FechaFin, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FECHAS_FACTURA Procedure
        /// </summary>
        public static StoredProcedure SelectFechasFactura(int? IdPrograma, int? IdPeriodo)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FECHAS_FACTURA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@IdPeriodo", IdPeriodo, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_FECHAS_PERIODO Procedure
        /// </summary>
        public static StoredProcedure SelectFechasPeriodo(int? IdPeriodo, int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_FECHAS_PERIODO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPeriodo", IdPeriodo, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_GUID_TRANSACCIONES_BY_FECHAS Procedure
        /// </summary>
        public static StoredProcedure SelectGuidTransaccionesByFechas(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_GUID_TRANSACCIONES_BY_FECHAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ID_PROGRAMA_BY_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure SelectIdProgramaByPrograma(string NOMBREPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ID_PROGRAMA_BY_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@NOMBRE_PROGRAMA", NOMBREPROGRAMA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_MARCAS Procedure
        /// </summary>
        public static StoredProcedure SelectMarcas(int? ParametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_MARCAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ParametroFantasma", ParametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_MARCAS_PROGRAMAS Procedure
        /// </summary>
        public static StoredProcedure SelectMarcasProveedores(int IDPROOVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_MARCAS_PROVEEDORES", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@IDPROOVEEDOR", IDPROOVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }

        public static StoredProcedure SelectMarcasProgramas(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_MARCAS_PROGRAMAS", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@IDPROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);

            return sp;
        }

        public static StoredProcedure SelectMarcasProgramas(int? IDPROGRAMA, string nombre)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_MARCAS_PROGRAMAS_NOMBRE", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@IDPROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@NOMBRE", nombre, DbType.String, 0, 10);
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_MAS_BODEGAS Procedure
        /// </summary>
        public static StoredProcedure SelectMasBodegas(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_MAS_BODEGAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_NOTAS_CREDITO_PROGRAMAS Procedure
        /// </summary>
        public static StoredProcedure SelectNotasCreditoProgramas(int? idTipoResolucion)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_NOTAS_CREDITO_PROGRAMAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idTipoResolucion", idTipoResolucion, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PERIODO_MES_ULTIMO Procedure
        /// </summary>
        public static StoredProcedure SelectPeriodoMesUltimo(int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PERIODO_MES_ULTIMO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PERIODOS_MES_ESTABLECIMIENTOS Procedure
        /// </summary>
        public static StoredProcedure SelectPeriodosMesEstablecimientos(int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PERIODOS_MES_ESTABLECIMIENTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PERIODOS_MES_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure SelectPeriodosMesProveedor(int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PERIODOS_MES_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PREFACTURACION_FACTURAS Procedure
        /// </summary>
        public static StoredProcedure SelectPrefacturacionFacturas(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA, int? IDPROVEEDOR, string GUIDPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PREFACTURACION_FACTURAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PREFACTURACION_FACTURAS_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectPrefacturacionFacturasProductos(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA, int? IDPROVEEDOR, string GUIDPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PREFACTURACION_FACTURAS_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHA_INICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PREFACTURACION_FECHAINICIO Procedure
        /// </summary>
        public static StoredProcedure SelectPrefacturacionFechainicio(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PREFACTURACION_FECHAINICIO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PREFACTURACION_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectPrefacturacionProductos(string GUIDFACTURA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PREFACTURACION_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_FACTURA", GUIDFACTURA, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PREFACTURACION_PRODUCTOS_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure SelectPrefacturacionProductosProveedor(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA, int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PREFACTURACION_PRODUCTOS_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHAINICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHAFIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PREFACTURACION_PROVEEDORES_FECHAS Procedure
        /// </summary>
        public static StoredProcedure SelectPrefacturacionProveedoresFechas(DateTime? FECHAINICIO, DateTime? FECHAFIN, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PREFACTURACION_PROVEEDORES_FECHAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@FECHAINICIO", FECHAINICIO, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHAFIN", FECHAFIN, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTO_BY_REFERENCIA Procedure
        /// </summary>
        public static StoredProcedure SelectProductoByReferencia(string REFERENCIAPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTO_BY_REFERENCIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@REFERENCIA_PRODUCTO", REFERENCIAPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectProductos(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTOS_BODEGA_PROVEEDOR_MARCA Procedure
        /// </summary>
        public static StoredProcedure SelectProductosBodegaProveedorMarca(int? IDBODEGAPRODUCTO, int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS_BODEGA_PROVEEDOR_MARCA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_BODEGA_PRODUCTO", IDBODEGAPRODUCTO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTOS_CARGA Procedure
        /// </summary>
        public static StoredProcedure SelectProductosCarga(int? IDPROVEEDOR, int? IDMARCA, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS_CARGA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@IDMARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@IDPROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTOS_NOMBRES Procedure
        /// </summary>
        public static StoredProcedure SelectProductosNombres(int? IDPROVEEDOR, int? IDTIPOPRODUCTO, int? IDMARCA, int? TOTALREGISTROS, string NOMBREPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS_NOMBRES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_TIPO_PRODUCTO", IDTIPOPRODUCTO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            sp.Command.AddParameter("@NOMBRE_PRODUCTO", NOMBREPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }

        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTOS_NOMBRES Procedure
        /// </summary>
        public static StoredProcedure SelectProductosNombres(int? IDPROVEEDOR, int? IDTIPOPRODUCTO, int? IDMARCA, int? TOTALREGISTROS, string NOMBREPRODUCTO, string CODIGOPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS_NOMBRES_V2", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);

            sp.Command.AddParameter("@ID_TIPO_PRODUCTO", IDTIPOPRODUCTO, DbType.Int32, 0, 10);

            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);

            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);

            sp.Command.AddParameter("@NOMBRE_PRODUCTO", NOMBREPRODUCTO, DbType.String, null, null);

            sp.Command.AddParameter("@CODIGO_PRODUCTO", CODIGOPRODUCTO, DbType.String, null, null);

            return sp;
        }

        public static StoredProcedure CargarTiposProductosByProgram(int IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_PRODUCTOS_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@ID_PROGRAMA", IdPrograma, DbType.Int32, 0, 10);

            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PRODUCTOS_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure SelectProductosProveedor(int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PROGRAMAS Procedure
        /// </summary>
        public static StoredProcedure SelectProgramas(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PROGRAMAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PROGRAMAS_NOMBRES Procedure
        /// </summary>
        public static StoredProcedure SelectProgramasNombres(string nombrePrograma, int? idCliente, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PROGRAMAS_NOMBRES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@nombrePrograma", nombrePrograma, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@idCliente", idCliente, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PROVEEDORES Procedure
        /// </summary>
        public static StoredProcedure SelectProveedores(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PROVEEDORES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PROVEEDORES_AUTO Procedure
        /// </summary>
        public static StoredProcedure SelectProveedoresAuto(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PROVEEDORES_AUTO", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_PROVEEDORES_NOMBRES Procedure
        /// </summary>
        public static StoredProcedure SelectProveedoresNombres(string codigo, string nit, int? estado, int? idProveedor, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PROVEEDORES_NOMBRES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@codigo", codigo, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@nit", nit, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@estado", estado, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@idProveedor", idProveedor, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_REFERENCIA_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectReferenciaProductos(string GUIDPRODUCTO, int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_REFERENCIA_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@IDPROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_REFERENCIAS_PROGRAMA_REFERENCIA Procedure
        /// </summary>
        public static StoredProcedure SelectReferenciasProgramaReferencia(int? IDPROGRAMA, string GUIDREFERENCIAPRODUCTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_REFERENCIAS_PROGRAMA_REFERENCIA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_REFERENCIA_PRODUCTO", GUIDREFERENCIAPRODUCTO, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_RESOLUCION_TIPO_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure SelectResolucionTipoPrograma(int? IDTIPORESOLUCION, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_RESOLUCION_TIPO_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_TIPO_RESOLUCION", IDTIPORESOLUCION, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_RESOLUCIONES Procedure
        /// </summary>
        public static StoredProcedure SelectResoluciones(int? TipoResolucion, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_RESOLUCIONES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@TipoResolucion", TipoResolucion, DbType.Int32, 0, 10);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_RESOLUCIONES_PROGRAMAS Procedure
        /// </summary>
        public static StoredProcedure SelectResolucionesProgramas(int? idTipoResolucion)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_RESOLUCIONES_PROGRAMAS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@idTipoResolucion", idTipoResolucion, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_SALDOS Procedure
        /// </summary>
        public static StoredProcedure SelectSaldos(int? Programa, int? Bodega, int? Proveedor, string Producto, DateTime? FechaInicio, DateTime? FechaFin, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_SALDOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@Programa", Programa, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Bodega", Bodega, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Proveedor", Proveedor, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Producto", Producto, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FechaInicio", FechaInicio, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FechaFin", FechaFin, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_SALDOS_BY_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure SelectSaldosByPrograma(int? IDPROGRAMA, int? IDTIPOBODEGA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_SALDOS_BY_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_TIPO_BODEGA", IDTIPOBODEGA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_SALDOS_MARCA_PROVEEDOR Procedure
        /// </summary>
        public static StoredProcedure SelectSaldosMarcaProveedor(int? IDPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_SALDOS_MARCA_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_SALDOS_NEW_V2 Procedure
        /// </summary>
        public static StoredProcedure SelectSaldosNewV2(int? IDPROGRAMA, int? IDPROVEEDOR, int? IDMARCA, string GUIDPRODUCTO, DateTime? FECHAINICIAL, DateTime? FECHAFINAL, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_SALDOS_NEW_V2", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROVEEDOR", IDPROVEEDOR, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_MARCA", IDMARCA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@GUID_PRODUCTO", GUIDPRODUCTO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FECHA_INICIAL", FECHAINICIAL, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FECHA_FINAL", FECHAFINAL, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_SALDOSV2 Procedure
        /// </summary>
        public static StoredProcedure SelectSALDOSV2(int? Programa, int? Bodega, int? Proveedor, string Producto, DateTime? FechaInicio, DateTime? FechaFin, int? TOTALREGISTROS)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_SALDOSV2", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@Programa", Programa, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Bodega", Bodega, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Proveedor", Proveedor, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@Producto", Producto, DbType.String, null, null);
        	
            sp.Command.AddParameter("@FechaInicio", FechaInicio, DbType.DateTime, null, null);
        	
            sp.Command.AddParameter("@FechaFin", FechaFin, DbType.DateTime, null, null);
        	
            sp.Command.AddOutputParameter("@TOTAL_REGISTROS", DbType.Int32, 0, 10);
            
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_SINIESTROS Procedure
        /// </summary>
        public static StoredProcedure SelectSiniestros(int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_SINIESTROS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_TIPOS_BODEGA Procedure
        /// </summary>
        public static StoredProcedure SelectTiposBodega(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_BODEGA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_TIPOS_CUENTA Procedure
        /// </summary>
        public static StoredProcedure SelectTiposCuenta(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_CUENTA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_TIPOS_EMPAQUES Procedure
        /// </summary>
        public static StoredProcedure SelectTiposEmpaques(int? parametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_EMPAQUES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@parametroFantasma", parametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_TIPOS_FACTURA Procedure
        /// </summary>
        public static StoredProcedure SelectTiposFactura()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_FACTURA", DataService.GetInstance("InventariosProvider"), "");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_TIPOS_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure SelectTiposProductos(int? ParametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ParametroFantasma", ParametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_TIPOS_PRODUCTOSA Procedure
        /// </summary>
        public static StoredProcedure SelectTiposProductosa(int? ParametroFantasma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_TIPOS_PRODUCTOSA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@ParametroFantasma", ParametroFantasma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ULTIMO_PERIODO_MES Procedure
        /// </summary>
        public static StoredProcedure SelectUltimoPeriodoMes(int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ULTIMO_PERIODO_MES", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_SELECT_ULTIMO_PERIODO_PROGRAMA Procedure
        /// </summary>
        public static StoredProcedure SelectUltimoPeriodoPrograma(int? IdPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_ULTIMO_PERIODO_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@IdPrograma", IdPrograma, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_USP_VERIFICAR_REFERENCIA_PRODUCTOS Procedure
        /// </summary>
        public static StoredProcedure VerificarReferenciaProductos(string nombreReferencia, string guidProducto, int? idPrograma, int? idProgramaDisponible)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_VERIFICAR_REFERENCIA_PRODUCTOS", DataService.GetInstance("InventariosProvider"), "dbo");
        	
            sp.Command.AddParameter("@nombreReferencia", nombreReferencia, DbType.String, null, null);
        	
            sp.Command.AddParameter("@guidProducto", guidProducto, DbType.String, null, null);
        	
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@idProgramaDisponible", idProgramaDisponible, DbType.Int32, 0, 10);
        	
            return sp;
        }

        public static StoredProcedure ObtenerProductosByEstablecimientoAndProveedor(int idEstablecimiento, int idProveedor, int idPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_OBTENER_PRODUCTOS_BY_ESTABLECIMIENTO_AND_PROVEEDOR", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@idEstablecimiento", idEstablecimiento, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@idProveedor", idProveedor, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@idPrograma", idPrograma, DbType.Int32, 0, 10);
            return sp;
        }

        public static StoredProcedure CargarProveedoresByProgram(int idPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PROVEEDORES_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");

            sp.Command.AddParameter("@ID_PROGRAMA", idPrograma, DbType.Int32, 0, 10);
            return sp;
        }


        public static StoredProcedure SelectProductosProveedorPrograma(int idProveedor,int idPrograma)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_PRODUCTOS_PROVEEDOR_PROGRAMA", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.AddParameter("@ID_PROVEEDOR", idProveedor, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@ID_PROGRAMA", idPrograma, DbType.Int32, 0, 10);
            return sp;
        }

        public static StoredProcedure GetMarcasPorIdProgramaProveedor(int idPrograma, int idProveedor)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_SELECT_MARCAS_PROGRAMAS_PROVEEDORES", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.AddParameter("@IDPROVEEDOR", idProveedor, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@IDPROGRAMA", idPrograma, DbType.Int32, 0, 10);
            return sp;
        }

        public static StoredProcedure GetInformeEntradasySalidasDetallado(int year, int? month)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("Get_Informe_Entradas_y_Salidas_Detallado", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.AddParameter("@YEAR", year, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@MONTH", month, DbType.Int32, 0, 10);
            return sp;
        }

        public static StoredProcedure GetInformeEntradasySalidasConsolidado(int year, int? month)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("Get_Informe_Entradas_y_Salidas_Consolidado", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.AddParameter("@YEAR", year, DbType.Int32, 0, 10);
            sp.Command.AddParameter("@MONTH", month, DbType.Int32, 0, 10);
            return sp;
        }

        public static StoredProcedure GetPeriodoCosteo(int idPeriodo)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO_COSTEO", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.AddParameter("@ID_PERIODO", idPeriodo, DbType.Int32, 0, 10);
            return sp;
        }

        public static StoredProcedure GetPeriodoCosteoV2(int idPeriodo)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_USP_PROVEEDOR_PUNTOS_GET_TRANSACCIONES_PERIODO_COSTEO_2", DataService.GetInstance("InventariosProvider"), "dbo");
            sp.Command.AddParameter("@ID_PERIODO", idPeriodo, DbType.Int32, 0, 10);
            return sp;
        }
        
        
    }
    
}
