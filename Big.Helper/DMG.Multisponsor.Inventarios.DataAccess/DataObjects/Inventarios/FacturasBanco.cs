using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the FacturasBanco class.
	/// </summary>
    [Serializable]
	public partial class FacturasBancoCollection : ActiveList<FacturasBanco, FacturasBancoCollection>
	{	   
		public FacturasBancoCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>FacturasBancoCollection</returns>
		public FacturasBancoCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                FacturasBanco o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_FACTURAS_BANCO table.
	/// </summary>
	[Serializable]
	public partial class FacturasBanco : ActiveRecord<FacturasBanco>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public FacturasBanco()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public FacturasBanco(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public FacturasBanco(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public FacturasBanco(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_FACTURAS_BANCO", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuidFacturaBanco = new TableSchema.TableColumn(schema);
				colvarGuidFacturaBanco.ColumnName = "GUID_FACTURA_BANCO";
				colvarGuidFacturaBanco.DataType = DbType.String;
				colvarGuidFacturaBanco.MaxLength = 36;
				colvarGuidFacturaBanco.AutoIncrement = false;
				colvarGuidFacturaBanco.IsNullable = false;
				colvarGuidFacturaBanco.IsPrimaryKey = true;
				colvarGuidFacturaBanco.IsForeignKey = false;
				colvarGuidFacturaBanco.IsReadOnly = false;
				colvarGuidFacturaBanco.DefaultSetting = @"";
				colvarGuidFacturaBanco.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuidFacturaBanco);
				
				TableSchema.TableColumn colvarConsecutivoFacturaBanco = new TableSchema.TableColumn(schema);
				colvarConsecutivoFacturaBanco.ColumnName = "CONSECUTIVO_FACTURA_BANCO";
				colvarConsecutivoFacturaBanco.DataType = DbType.Int32;
				colvarConsecutivoFacturaBanco.MaxLength = 0;
				colvarConsecutivoFacturaBanco.AutoIncrement = false;
				colvarConsecutivoFacturaBanco.IsNullable = false;
				colvarConsecutivoFacturaBanco.IsPrimaryKey = false;
				colvarConsecutivoFacturaBanco.IsForeignKey = false;
				colvarConsecutivoFacturaBanco.IsReadOnly = false;
				colvarConsecutivoFacturaBanco.DefaultSetting = @"";
				colvarConsecutivoFacturaBanco.ForeignKeyTableName = "";
				schema.Columns.Add(colvarConsecutivoFacturaBanco);
				
				TableSchema.TableColumn colvarTotalBase = new TableSchema.TableColumn(schema);
				colvarTotalBase.ColumnName = "TOTAL_BASE";
				colvarTotalBase.DataType = DbType.Decimal;
				colvarTotalBase.MaxLength = 0;
				colvarTotalBase.AutoIncrement = false;
				colvarTotalBase.IsNullable = false;
				colvarTotalBase.IsPrimaryKey = false;
				colvarTotalBase.IsForeignKey = false;
				colvarTotalBase.IsReadOnly = false;
				colvarTotalBase.DefaultSetting = @"";
				colvarTotalBase.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotalBase);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarFechaFacturaBanco = new TableSchema.TableColumn(schema);
				colvarFechaFacturaBanco.ColumnName = "FECHA_FACTURA_BANCO";
				colvarFechaFacturaBanco.DataType = DbType.DateTime;
				colvarFechaFacturaBanco.MaxLength = 0;
				colvarFechaFacturaBanco.AutoIncrement = false;
				colvarFechaFacturaBanco.IsNullable = false;
				colvarFechaFacturaBanco.IsPrimaryKey = false;
				colvarFechaFacturaBanco.IsForeignKey = false;
				colvarFechaFacturaBanco.IsReadOnly = false;
				colvarFechaFacturaBanco.DefaultSetting = @"";
				colvarFechaFacturaBanco.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaFacturaBanco);
				
				TableSchema.TableColumn colvarIdProveedor = new TableSchema.TableColumn(schema);
				colvarIdProveedor.ColumnName = "ID_PROVEEDOR";
				colvarIdProveedor.DataType = DbType.Int32;
				colvarIdProveedor.MaxLength = 0;
				colvarIdProveedor.AutoIncrement = false;
				colvarIdProveedor.IsNullable = false;
				colvarIdProveedor.IsPrimaryKey = false;
				colvarIdProveedor.IsForeignKey = true;
				colvarIdProveedor.IsReadOnly = false;
				colvarIdProveedor.DefaultSetting = @"";
				
					colvarIdProveedor.ForeignKeyTableName = "INV_PROVEEDORES";
				schema.Columns.Add(colvarIdProveedor);
				
				TableSchema.TableColumn colvarIdPrograma = new TableSchema.TableColumn(schema);
				colvarIdPrograma.ColumnName = "ID_PROGRAMA";
				colvarIdPrograma.DataType = DbType.Int32;
				colvarIdPrograma.MaxLength = 0;
				colvarIdPrograma.AutoIncrement = false;
				colvarIdPrograma.IsNullable = false;
				colvarIdPrograma.IsPrimaryKey = false;
				colvarIdPrograma.IsForeignKey = true;
				colvarIdPrograma.IsReadOnly = false;
				colvarIdPrograma.DefaultSetting = @"";
				
					colvarIdPrograma.ForeignKeyTableName = "INV_PROGRAMAS";
				schema.Columns.Add(colvarIdPrograma);
				
				TableSchema.TableColumn colvarIdPrefactura = new TableSchema.TableColumn(schema);
				colvarIdPrefactura.ColumnName = "ID_PREFACTURA";
				colvarIdPrefactura.DataType = DbType.Int32;
				colvarIdPrefactura.MaxLength = 0;
				colvarIdPrefactura.AutoIncrement = false;
				colvarIdPrefactura.IsNullable = false;
				colvarIdPrefactura.IsPrimaryKey = false;
				colvarIdPrefactura.IsForeignKey = false;
				colvarIdPrefactura.IsReadOnly = false;
				colvarIdPrefactura.DefaultSetting = @"";
				colvarIdPrefactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdPrefactura);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_FACTURAS_BANCO",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("GuidFacturaBanco")]
		[Bindable(true)]
		public string GuidFacturaBanco 
		{
			get { return GetColumnValue<string>(Columns.GuidFacturaBanco); }
			set { SetColumnValue(Columns.GuidFacturaBanco, value); }
		}
		  
		[XmlAttribute("ConsecutivoFacturaBanco")]
		[Bindable(true)]
		public int ConsecutivoFacturaBanco 
		{
			get { return GetColumnValue<int>(Columns.ConsecutivoFacturaBanco); }
			set { SetColumnValue(Columns.ConsecutivoFacturaBanco, value); }
		}
		  
		[XmlAttribute("TotalBase")]
		[Bindable(true)]
		public decimal TotalBase 
		{
			get { return GetColumnValue<decimal>(Columns.TotalBase); }
			set { SetColumnValue(Columns.TotalBase, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("FechaFacturaBanco")]
		[Bindable(true)]
		public DateTime FechaFacturaBanco 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaFacturaBanco); }
			set { SetColumnValue(Columns.FechaFacturaBanco, value); }
		}
		  
		[XmlAttribute("IdProveedor")]
		[Bindable(true)]
		public int IdProveedor 
		{
			get { return GetColumnValue<int>(Columns.IdProveedor); }
			set { SetColumnValue(Columns.IdProveedor, value); }
		}
		  
		[XmlAttribute("IdPrograma")]
		[Bindable(true)]
		public int IdPrograma 
		{
			get { return GetColumnValue<int>(Columns.IdPrograma); }
			set { SetColumnValue(Columns.IdPrograma, value); }
		}
		  
		[XmlAttribute("IdPrefactura")]
		[Bindable(true)]
		public int IdPrefactura 
		{
			get { return GetColumnValue<int>(Columns.IdPrefactura); }
			set { SetColumnValue(Columns.IdPrefactura, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaBancoCollection DetallesFacturaBancoRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaBancoCollection().Where(DetallesFacturaBanco.Columns.GuidFacturaBanco, GuidFacturaBanco).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Programas ActiveRecord object related to this FacturasBanco
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Programas Programas
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Programas.FetchByID(this.IdPrograma); }
			set { SetColumnValue("ID_PROGRAMA", value.IdPrograma); }
		}
		
		
		/// <summary>
		/// Returns a Proveedores ActiveRecord object related to this FacturasBanco
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Proveedores Proveedores
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Proveedores.FetchByID(this.IdProveedor); }
			set { SetColumnValue("ID_PROVEEDOR", value.IdProveedor); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuidFacturaBanco,int varConsecutivoFacturaBanco,decimal varTotalBase,decimal varIva,DateTime varFechaFacturaBanco,int varIdProveedor,int varIdPrograma,int varIdPrefactura)
		{
			FacturasBanco item = new FacturasBanco();
			
			item.GuidFacturaBanco = varGuidFacturaBanco;
			
			item.ConsecutivoFacturaBanco = varConsecutivoFacturaBanco;
			
			item.TotalBase = varTotalBase;
			
			item.Iva = varIva;
			
			item.FechaFacturaBanco = varFechaFacturaBanco;
			
			item.IdProveedor = varIdProveedor;
			
			item.IdPrograma = varIdPrograma;
			
			item.IdPrefactura = varIdPrefactura;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuidFacturaBanco,int varConsecutivoFacturaBanco,decimal varTotalBase,decimal varIva,DateTime varFechaFacturaBanco,int varIdProveedor,int varIdPrograma,int varIdPrefactura)
		{
			FacturasBanco item = new FacturasBanco();
			
				item.GuidFacturaBanco = varGuidFacturaBanco;
			
				item.ConsecutivoFacturaBanco = varConsecutivoFacturaBanco;
			
				item.TotalBase = varTotalBase;
			
				item.Iva = varIva;
			
				item.FechaFacturaBanco = varFechaFacturaBanco;
			
				item.IdProveedor = varIdProveedor;
			
				item.IdPrograma = varIdPrograma;
			
				item.IdPrefactura = varIdPrefactura;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidFacturaBancoColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn ConsecutivoFacturaBancoColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalBaseColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaFacturaBancoColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProveedorColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdProgramaColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPrefacturaColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string GuidFacturaBanco = @"GUID_FACTURA_BANCO";
			 public static string ConsecutivoFacturaBanco = @"CONSECUTIVO_FACTURA_BANCO";
			 public static string TotalBase = @"TOTAL_BASE";
			 public static string Iva = @"IVA";
			 public static string FechaFacturaBanco = @"FECHA_FACTURA_BANCO";
			 public static string IdProveedor = @"ID_PROVEEDOR";
			 public static string IdPrograma = @"ID_PROGRAMA";
			 public static string IdPrefactura = @"ID_PREFACTURA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
