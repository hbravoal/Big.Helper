using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the HistoricoBodega class.
	/// </summary>
    [Serializable]
	public partial class HistoricoBodegaCollection : ActiveList<HistoricoBodega, HistoricoBodegaCollection>
	{	   
		public HistoricoBodegaCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>HistoricoBodegaCollection</returns>
		public HistoricoBodegaCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                HistoricoBodega o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_HISTORICO_BODEGA table.
	/// </summary>
	[Serializable]
	public partial class HistoricoBodega : ActiveRecord<HistoricoBodega>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public HistoricoBodega()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public HistoricoBodega(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public HistoricoBodega(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public HistoricoBodega(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_HISTORICO_BODEGA", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdHistoricoBodega = new TableSchema.TableColumn(schema);
				colvarIdHistoricoBodega.ColumnName = "ID_HISTORICO_BODEGA";
				colvarIdHistoricoBodega.DataType = DbType.Decimal;
				colvarIdHistoricoBodega.MaxLength = 0;
				colvarIdHistoricoBodega.AutoIncrement = true;
				colvarIdHistoricoBodega.IsNullable = false;
				colvarIdHistoricoBodega.IsPrimaryKey = true;
				colvarIdHistoricoBodega.IsForeignKey = false;
				colvarIdHistoricoBodega.IsReadOnly = false;
				colvarIdHistoricoBodega.DefaultSetting = @"";
				colvarIdHistoricoBodega.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdHistoricoBodega);
				
				TableSchema.TableColumn colvarEntrada = new TableSchema.TableColumn(schema);
				colvarEntrada.ColumnName = "ENTRADA";
				colvarEntrada.DataType = DbType.Boolean;
				colvarEntrada.MaxLength = 0;
				colvarEntrada.AutoIncrement = false;
				colvarEntrada.IsNullable = false;
				colvarEntrada.IsPrimaryKey = false;
				colvarEntrada.IsForeignKey = false;
				colvarEntrada.IsReadOnly = false;
				colvarEntrada.DefaultSetting = @"";
				colvarEntrada.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEntrada);
				
				TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
				colvarFecha.ColumnName = "FECHA";
				colvarFecha.DataType = DbType.DateTime;
				colvarFecha.MaxLength = 0;
				colvarFecha.AutoIncrement = false;
				colvarFecha.IsNullable = false;
				colvarFecha.IsPrimaryKey = false;
				colvarFecha.IsForeignKey = false;
				colvarFecha.IsReadOnly = false;
				colvarFecha.DefaultSetting = @"";
				colvarFecha.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecha);
				
				TableSchema.TableColumn colvarGuidDetalleFactura = new TableSchema.TableColumn(schema);
				colvarGuidDetalleFactura.ColumnName = "GUID_DETALLE_FACTURA";
				colvarGuidDetalleFactura.DataType = DbType.String;
				colvarGuidDetalleFactura.MaxLength = 36;
				colvarGuidDetalleFactura.AutoIncrement = false;
				colvarGuidDetalleFactura.IsNullable = true;
				colvarGuidDetalleFactura.IsPrimaryKey = false;
				colvarGuidDetalleFactura.IsForeignKey = true;
				colvarGuidDetalleFactura.IsReadOnly = false;
				colvarGuidDetalleFactura.DefaultSetting = @"";
				
					colvarGuidDetalleFactura.ForeignKeyTableName = "INV_DETALLES_FACTURA";
				schema.Columns.Add(colvarGuidDetalleFactura);
				
				TableSchema.TableColumn colvarIdUsuario = new TableSchema.TableColumn(schema);
				colvarIdUsuario.ColumnName = "ID_USUARIO";
				colvarIdUsuario.DataType = DbType.Int32;
				colvarIdUsuario.MaxLength = 0;
				colvarIdUsuario.AutoIncrement = false;
				colvarIdUsuario.IsNullable = true;
				colvarIdUsuario.IsPrimaryKey = false;
				colvarIdUsuario.IsForeignKey = true;
				colvarIdUsuario.IsReadOnly = false;
				colvarIdUsuario.DefaultSetting = @"";
				
					colvarIdUsuario.ForeignKeyTableName = "INV_USUARIOS";
				schema.Columns.Add(colvarIdUsuario);
				
				TableSchema.TableColumn colvarIdBodegaProducto = new TableSchema.TableColumn(schema);
				colvarIdBodegaProducto.ColumnName = "ID_BODEGA_PRODUCTO";
				colvarIdBodegaProducto.DataType = DbType.Decimal;
				colvarIdBodegaProducto.MaxLength = 0;
				colvarIdBodegaProducto.AutoIncrement = false;
				colvarIdBodegaProducto.IsNullable = false;
				colvarIdBodegaProducto.IsPrimaryKey = false;
				colvarIdBodegaProducto.IsForeignKey = true;
				colvarIdBodegaProducto.IsReadOnly = false;
				colvarIdBodegaProducto.DefaultSetting = @"";
				
					colvarIdBodegaProducto.ForeignKeyTableName = "INV_BODEGAS_PRODUCTOS";
				schema.Columns.Add(colvarIdBodegaProducto);
				
				TableSchema.TableColumn colvarObservaciones = new TableSchema.TableColumn(schema);
				colvarObservaciones.ColumnName = "OBSERVACIONES";
				colvarObservaciones.DataType = DbType.String;
				colvarObservaciones.MaxLength = 1000;
				colvarObservaciones.AutoIncrement = false;
				colvarObservaciones.IsNullable = true;
				colvarObservaciones.IsPrimaryKey = false;
				colvarObservaciones.IsForeignKey = false;
				colvarObservaciones.IsReadOnly = false;
				colvarObservaciones.DefaultSetting = @"";
				colvarObservaciones.ForeignKeyTableName = "";
				schema.Columns.Add(colvarObservaciones);
				
				TableSchema.TableColumn colvarCantidadTransaccion = new TableSchema.TableColumn(schema);
				colvarCantidadTransaccion.ColumnName = "CANTIDAD_TRANSACCION";
				colvarCantidadTransaccion.DataType = DbType.Decimal;
				colvarCantidadTransaccion.MaxLength = 0;
				colvarCantidadTransaccion.AutoIncrement = false;
				colvarCantidadTransaccion.IsNullable = true;
				colvarCantidadTransaccion.IsPrimaryKey = false;
				colvarCantidadTransaccion.IsForeignKey = false;
				colvarCantidadTransaccion.IsReadOnly = false;
				colvarCantidadTransaccion.DefaultSetting = @"";
				colvarCantidadTransaccion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCantidadTransaccion);
				
				TableSchema.TableColumn colvarFechaVencimiento = new TableSchema.TableColumn(schema);
				colvarFechaVencimiento.ColumnName = "FECHA_VENCIMIENTO";
				colvarFechaVencimiento.DataType = DbType.DateTime;
				colvarFechaVencimiento.MaxLength = 0;
				colvarFechaVencimiento.AutoIncrement = false;
				colvarFechaVencimiento.IsNullable = true;
				colvarFechaVencimiento.IsPrimaryKey = false;
				colvarFechaVencimiento.IsForeignKey = false;
				colvarFechaVencimiento.IsReadOnly = false;
				colvarFechaVencimiento.DefaultSetting = @"";
				colvarFechaVencimiento.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaVencimiento);
				
				TableSchema.TableColumn colvarNumeroFactura = new TableSchema.TableColumn(schema);
				colvarNumeroFactura.ColumnName = "NUMERO_FACTURA";
				colvarNumeroFactura.DataType = DbType.Int64;
				colvarNumeroFactura.MaxLength = 0;
				colvarNumeroFactura.AutoIncrement = false;
				colvarNumeroFactura.IsNullable = true;
				colvarNumeroFactura.IsPrimaryKey = false;
				colvarNumeroFactura.IsForeignKey = false;
				colvarNumeroFactura.IsReadOnly = false;
				colvarNumeroFactura.DefaultSetting = @"";
				colvarNumeroFactura.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNumeroFactura);
				
				TableSchema.TableColumn colvarGuidDetalleAjuste = new TableSchema.TableColumn(schema);
				colvarGuidDetalleAjuste.ColumnName = "GUID_DETALLE_AJUSTE";
				colvarGuidDetalleAjuste.DataType = DbType.String;
				colvarGuidDetalleAjuste.MaxLength = 36;
				colvarGuidDetalleAjuste.AutoIncrement = false;
				colvarGuidDetalleAjuste.IsNullable = true;
				colvarGuidDetalleAjuste.IsPrimaryKey = false;
				colvarGuidDetalleAjuste.IsForeignKey = true;
				colvarGuidDetalleAjuste.IsReadOnly = false;
				colvarGuidDetalleAjuste.DefaultSetting = @"";
				
					colvarGuidDetalleAjuste.ForeignKeyTableName = "INV_DETALLE_AJUSTE";
				schema.Columns.Add(colvarGuidDetalleAjuste);
				
				TableSchema.TableColumn colvarGuidDetalleFacturaEmpresa = new TableSchema.TableColumn(schema);
				colvarGuidDetalleFacturaEmpresa.ColumnName = "GUID_DETALLE_FACTURA_EMPRESA";
				colvarGuidDetalleFacturaEmpresa.DataType = DbType.String;
				colvarGuidDetalleFacturaEmpresa.MaxLength = 36;
				colvarGuidDetalleFacturaEmpresa.AutoIncrement = false;
				colvarGuidDetalleFacturaEmpresa.IsNullable = true;
				colvarGuidDetalleFacturaEmpresa.IsPrimaryKey = false;
				colvarGuidDetalleFacturaEmpresa.IsForeignKey = true;
				colvarGuidDetalleFacturaEmpresa.IsReadOnly = false;
				colvarGuidDetalleFacturaEmpresa.DefaultSetting = @"";
				
					colvarGuidDetalleFacturaEmpresa.ForeignKeyTableName = "INV_DETALLES_FACTURA_PROVEEDOR_PUNTOS";
				schema.Columns.Add(colvarGuidDetalleFacturaEmpresa);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_HISTORICO_BODEGA",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdHistoricoBodega")]
		[Bindable(true)]
		public decimal IdHistoricoBodega 
		{
			get { return GetColumnValue<decimal>(Columns.IdHistoricoBodega); }
			set { SetColumnValue(Columns.IdHistoricoBodega, value); }
		}
		  
		[XmlAttribute("Entrada")]
		[Bindable(true)]
		public bool Entrada 
		{
			get { return GetColumnValue<bool>(Columns.Entrada); }
			set { SetColumnValue(Columns.Entrada, value); }
		}
		  
		[XmlAttribute("Fecha")]
		[Bindable(true)]
		public DateTime Fecha 
		{
			get { return GetColumnValue<DateTime>(Columns.Fecha); }
			set { SetColumnValue(Columns.Fecha, value); }
		}
		  
		[XmlAttribute("GuidDetalleFactura")]
		[Bindable(true)]
		public string GuidDetalleFactura 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleFactura); }
			set { SetColumnValue(Columns.GuidDetalleFactura, value); }
		}
		  
		[XmlAttribute("IdUsuario")]
		[Bindable(true)]
		public int? IdUsuario 
		{
			get { return GetColumnValue<int?>(Columns.IdUsuario); }
			set { SetColumnValue(Columns.IdUsuario, value); }
		}
		  
		[XmlAttribute("IdBodegaProducto")]
		[Bindable(true)]
		public decimal IdBodegaProducto 
		{
			get { return GetColumnValue<decimal>(Columns.IdBodegaProducto); }
			set { SetColumnValue(Columns.IdBodegaProducto, value); }
		}
		  
		[XmlAttribute("Observaciones")]
		[Bindable(true)]
		public string Observaciones 
		{
			get { return GetColumnValue<string>(Columns.Observaciones); }
			set { SetColumnValue(Columns.Observaciones, value); }
		}
		  
		[XmlAttribute("CantidadTransaccion")]
		[Bindable(true)]
		public decimal? CantidadTransaccion 
		{
			get { return GetColumnValue<decimal?>(Columns.CantidadTransaccion); }
			set { SetColumnValue(Columns.CantidadTransaccion, value); }
		}
		  
		[XmlAttribute("FechaVencimiento")]
		[Bindable(true)]
		public DateTime? FechaVencimiento 
		{
			get { return GetColumnValue<DateTime?>(Columns.FechaVencimiento); }
			set { SetColumnValue(Columns.FechaVencimiento, value); }
		}
		  
		[XmlAttribute("NumeroFactura")]
		[Bindable(true)]
		public long? NumeroFactura 
		{
			get { return GetColumnValue<long?>(Columns.NumeroFactura); }
			set { SetColumnValue(Columns.NumeroFactura, value); }
		}
		  
		[XmlAttribute("GuidDetalleAjuste")]
		[Bindable(true)]
		public string GuidDetalleAjuste 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleAjuste); }
			set { SetColumnValue(Columns.GuidDetalleAjuste, value); }
		}
		  
		[XmlAttribute("GuidDetalleFacturaEmpresa")]
		[Bindable(true)]
		public string GuidDetalleFacturaEmpresa 
		{
			get { return GetColumnValue<string>(Columns.GuidDetalleFacturaEmpresa); }
			set { SetColumnValue(Columns.GuidDetalleFacturaEmpresa, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a BodegasProductos ActiveRecord object related to this HistoricoBodega
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos BodegasProductos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos.FetchByID(this.IdBodegaProducto); }
			set { SetColumnValue("ID_BODEGA_PRODUCTO", value.IdBodegaProducto); }
		}
		
		
		/// <summary>
		/// Returns a DetalleAjuste ActiveRecord object related to this HistoricoBodega
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.DetalleAjuste DetalleAjuste
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.DetalleAjuste.FetchByID(this.GuidDetalleAjuste); }
			set { SetColumnValue("GUID_DETALLE_AJUSTE", value.IdDetalleAjuste); }
		}
		
		
		/// <summary>
		/// Returns a DetallesFactura ActiveRecord object related to this HistoricoBodega
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFactura DetallesFactura
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFactura.FetchByID(this.GuidDetalleFactura); }
			set { SetColumnValue("GUID_DETALLE_FACTURA", value.GuidDetalleFactura); }
		}
		
		
		/// <summary>
		/// Returns a DetallesFacturaProveedorPuntos ActiveRecord object related to this HistoricoBodega
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaProveedorPuntos DetallesFacturaProveedorPuntos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.DetallesFacturaProveedorPuntos.FetchByID(this.GuidDetalleFacturaEmpresa); }
			set { SetColumnValue("GUID_DETALLE_FACTURA_EMPRESA", value.GuidDetalleFacturaProveedorPuntos); }
		}
		
		
		/// <summary>
		/// Returns a Usuarios ActiveRecord object related to this HistoricoBodega
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Usuarios Usuarios
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Usuarios.FetchByID(this.IdUsuario); }
			set { SetColumnValue("ID_USUARIO", value.IdUsuario); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(bool varEntrada,DateTime varFecha,string varGuidDetalleFactura,int? varIdUsuario,decimal varIdBodegaProducto,string varObservaciones,decimal? varCantidadTransaccion,DateTime? varFechaVencimiento,long? varNumeroFactura,string varGuidDetalleAjuste,string varGuidDetalleFacturaEmpresa)
		{
			HistoricoBodega item = new HistoricoBodega();
			
			item.Entrada = varEntrada;
			
			item.Fecha = varFecha;
			
			item.GuidDetalleFactura = varGuidDetalleFactura;
			
			item.IdUsuario = varIdUsuario;
			
			item.IdBodegaProducto = varIdBodegaProducto;
			
			item.Observaciones = varObservaciones;
			
			item.CantidadTransaccion = varCantidadTransaccion;
			
			item.FechaVencimiento = varFechaVencimiento;
			
			item.NumeroFactura = varNumeroFactura;
			
			item.GuidDetalleAjuste = varGuidDetalleAjuste;
			
			item.GuidDetalleFacturaEmpresa = varGuidDetalleFacturaEmpresa;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(decimal varIdHistoricoBodega,bool varEntrada,DateTime varFecha,string varGuidDetalleFactura,int? varIdUsuario,decimal varIdBodegaProducto,string varObservaciones,decimal? varCantidadTransaccion,DateTime? varFechaVencimiento,long? varNumeroFactura,string varGuidDetalleAjuste,string varGuidDetalleFacturaEmpresa)
		{
			HistoricoBodega item = new HistoricoBodega();
			
				item.IdHistoricoBodega = varIdHistoricoBodega;
			
				item.Entrada = varEntrada;
			
				item.Fecha = varFecha;
			
				item.GuidDetalleFactura = varGuidDetalleFactura;
			
				item.IdUsuario = varIdUsuario;
			
				item.IdBodegaProducto = varIdBodegaProducto;
			
				item.Observaciones = varObservaciones;
			
				item.CantidadTransaccion = varCantidadTransaccion;
			
				item.FechaVencimiento = varFechaVencimiento;
			
				item.NumeroFactura = varNumeroFactura;
			
				item.GuidDetalleAjuste = varGuidDetalleAjuste;
			
				item.GuidDetalleFacturaEmpresa = varGuidDetalleFacturaEmpresa;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdHistoricoBodegaColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn EntradaColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidDetalleFacturaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdUsuarioColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn IdBodegaProductoColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn ObservacionesColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn CantidadTransaccionColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaVencimientoColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn NumeroFacturaColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidDetalleAjusteColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidDetalleFacturaEmpresaColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdHistoricoBodega = @"ID_HISTORICO_BODEGA";
			 public static string Entrada = @"ENTRADA";
			 public static string Fecha = @"FECHA";
			 public static string GuidDetalleFactura = @"GUID_DETALLE_FACTURA";
			 public static string IdUsuario = @"ID_USUARIO";
			 public static string IdBodegaProducto = @"ID_BODEGA_PRODUCTO";
			 public static string Observaciones = @"OBSERVACIONES";
			 public static string CantidadTransaccion = @"CANTIDAD_TRANSACCION";
			 public static string FechaVencimiento = @"FECHA_VENCIMIENTO";
			 public static string NumeroFactura = @"NUMERO_FACTURA";
			 public static string GuidDetalleAjuste = @"GUID_DETALLE_AJUSTE";
			 public static string GuidDetalleFacturaEmpresa = @"GUID_DETALLE_FACTURA_EMPRESA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
