using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.Core
{
	/// <summary>
	/// Strongly-typed collection for the Usuarios class.
	/// </summary>
    [Serializable]
	public partial class UsuariosCollection : ActiveList<Usuarios, UsuariosCollection>
	{	   
		public UsuariosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>UsuariosCollection</returns>
		public UsuariosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Usuarios o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_USUARIOS table.
	/// </summary>
	[Serializable]
	public partial class Usuarios : ActiveRecord<Usuarios>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Usuarios()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Usuarios(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Usuarios(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Usuarios(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_USUARIOS", TableType.Table, DataService.GetInstance("InventariosProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarIdUsuario = new TableSchema.TableColumn(schema);
				colvarIdUsuario.ColumnName = "ID_USUARIO";
				colvarIdUsuario.DataType = DbType.Int32;
				colvarIdUsuario.MaxLength = 0;
				colvarIdUsuario.AutoIncrement = true;
				colvarIdUsuario.IsNullable = false;
				colvarIdUsuario.IsPrimaryKey = true;
				colvarIdUsuario.IsForeignKey = false;
				colvarIdUsuario.IsReadOnly = false;
				colvarIdUsuario.DefaultSetting = @"";
				colvarIdUsuario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdUsuario);
				
				TableSchema.TableColumn colvarIdentificacion = new TableSchema.TableColumn(schema);
				colvarIdentificacion.ColumnName = "IDENTIFICACION";
				colvarIdentificacion.DataType = DbType.AnsiString;
				colvarIdentificacion.MaxLength = 15;
				colvarIdentificacion.AutoIncrement = false;
				colvarIdentificacion.IsNullable = false;
				colvarIdentificacion.IsPrimaryKey = false;
				colvarIdentificacion.IsForeignKey = false;
				colvarIdentificacion.IsReadOnly = false;
				colvarIdentificacion.DefaultSetting = @"";
				colvarIdentificacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdentificacion);
				
				TableSchema.TableColumn colvarNombre = new TableSchema.TableColumn(schema);
				colvarNombre.ColumnName = "NOMBRE";
				colvarNombre.DataType = DbType.String;
				colvarNombre.MaxLength = 255;
				colvarNombre.AutoIncrement = false;
				colvarNombre.IsNullable = false;
				colvarNombre.IsPrimaryKey = false;
				colvarNombre.IsForeignKey = false;
				colvarNombre.IsReadOnly = false;
				colvarNombre.DefaultSetting = @"";
				colvarNombre.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombre);
				
				TableSchema.TableColumn colvarEmail = new TableSchema.TableColumn(schema);
				colvarEmail.ColumnName = "EMAIL";
				colvarEmail.DataType = DbType.AnsiString;
				colvarEmail.MaxLength = 70;
				colvarEmail.AutoIncrement = false;
				colvarEmail.IsNullable = false;
				colvarEmail.IsPrimaryKey = false;
				colvarEmail.IsForeignKey = false;
				colvarEmail.IsReadOnly = false;
				colvarEmail.DefaultSetting = @"";
				colvarEmail.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEmail);
				
				TableSchema.TableColumn colvarNombreUsuario = new TableSchema.TableColumn(schema);
				colvarNombreUsuario.ColumnName = "NOMBRE_USUARIO";
				colvarNombreUsuario.DataType = DbType.AnsiString;
				colvarNombreUsuario.MaxLength = 50;
				colvarNombreUsuario.AutoIncrement = false;
				colvarNombreUsuario.IsNullable = false;
				colvarNombreUsuario.IsPrimaryKey = false;
				colvarNombreUsuario.IsForeignKey = false;
				colvarNombreUsuario.IsReadOnly = false;
				colvarNombreUsuario.DefaultSetting = @"";
				colvarNombreUsuario.ForeignKeyTableName = "";
				schema.Columns.Add(colvarNombreUsuario);
				
				TableSchema.TableColumn colvarPassword = new TableSchema.TableColumn(schema);
				colvarPassword.ColumnName = "PASSWORD";
				colvarPassword.DataType = DbType.AnsiString;
				colvarPassword.MaxLength = 100;
				colvarPassword.AutoIncrement = false;
				colvarPassword.IsNullable = false;
				colvarPassword.IsPrimaryKey = false;
				colvarPassword.IsForeignKey = false;
				colvarPassword.IsReadOnly = false;
				colvarPassword.DefaultSetting = @"";
				colvarPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPassword);
				
				TableSchema.TableColumn colvarIdPerfil = new TableSchema.TableColumn(schema);
				colvarIdPerfil.ColumnName = "ID_PERFIL";
				colvarIdPerfil.DataType = DbType.Int32;
				colvarIdPerfil.MaxLength = 0;
				colvarIdPerfil.AutoIncrement = false;
				colvarIdPerfil.IsNullable = false;
				colvarIdPerfil.IsPrimaryKey = false;
				colvarIdPerfil.IsForeignKey = true;
				colvarIdPerfil.IsReadOnly = false;
				colvarIdPerfil.DefaultSetting = @"";
				
					colvarIdPerfil.ForeignKeyTableName = "INV_PERFILES";
				schema.Columns.Add(colvarIdPerfil);
				
				TableSchema.TableColumn colvarFechaPassword = new TableSchema.TableColumn(schema);
				colvarFechaPassword.ColumnName = "FECHA_PASSWORD";
				colvarFechaPassword.DataType = DbType.DateTime;
				colvarFechaPassword.MaxLength = 0;
				colvarFechaPassword.AutoIncrement = false;
				colvarFechaPassword.IsNullable = false;
				colvarFechaPassword.IsPrimaryKey = false;
				colvarFechaPassword.IsForeignKey = false;
				colvarFechaPassword.IsReadOnly = false;
				colvarFechaPassword.DefaultSetting = @"";
				colvarFechaPassword.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFechaPassword);
				
				TableSchema.TableColumn colvarIntentosFallidos = new TableSchema.TableColumn(schema);
				colvarIntentosFallidos.ColumnName = "INTENTOS_FALLIDOS";
				colvarIntentosFallidos.DataType = DbType.Int32;
				colvarIntentosFallidos.MaxLength = 0;
				colvarIntentosFallidos.AutoIncrement = false;
				colvarIntentosFallidos.IsNullable = false;
				colvarIntentosFallidos.IsPrimaryKey = false;
				colvarIntentosFallidos.IsForeignKey = false;
				colvarIntentosFallidos.IsReadOnly = false;
				colvarIntentosFallidos.DefaultSetting = @"";
				colvarIntentosFallidos.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIntentosFallidos);
				
				TableSchema.TableColumn colvarPrimeraVez = new TableSchema.TableColumn(schema);
				colvarPrimeraVez.ColumnName = "PRIMERA_VEZ";
				colvarPrimeraVez.DataType = DbType.Boolean;
				colvarPrimeraVez.MaxLength = 0;
				colvarPrimeraVez.AutoIncrement = false;
				colvarPrimeraVez.IsNullable = false;
				colvarPrimeraVez.IsPrimaryKey = false;
				colvarPrimeraVez.IsForeignKey = false;
				colvarPrimeraVez.IsReadOnly = false;
				colvarPrimeraVez.DefaultSetting = @"";
				colvarPrimeraVez.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPrimeraVez);
				
				TableSchema.TableColumn colvarIdEstado = new TableSchema.TableColumn(schema);
				colvarIdEstado.ColumnName = "ID_ESTADO";
				colvarIdEstado.DataType = DbType.Int32;
				colvarIdEstado.MaxLength = 0;
				colvarIdEstado.AutoIncrement = false;
				colvarIdEstado.IsNullable = false;
				colvarIdEstado.IsPrimaryKey = false;
				colvarIdEstado.IsForeignKey = true;
				colvarIdEstado.IsReadOnly = false;
				colvarIdEstado.DefaultSetting = @"";
				
					colvarIdEstado.ForeignKeyTableName = "INV_MAS_ESTADOS";
				schema.Columns.Add(colvarIdEstado);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosProvider"].AddSchema("INV_USUARIOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("IdUsuario")]
		[Bindable(true)]
		public int IdUsuario 
		{
			get { return GetColumnValue<int>(Columns.IdUsuario); }
			set { SetColumnValue(Columns.IdUsuario, value); }
		}
		  
		[XmlAttribute("Identificacion")]
		[Bindable(true)]
		public string Identificacion 
		{
			get { return GetColumnValue<string>(Columns.Identificacion); }
			set { SetColumnValue(Columns.Identificacion, value); }
		}
		  
		[XmlAttribute("Nombre")]
		[Bindable(true)]
		public string Nombre 
		{
			get { return GetColumnValue<string>(Columns.Nombre); }
			set { SetColumnValue(Columns.Nombre, value); }
		}
		  
		[XmlAttribute("Email")]
		[Bindable(true)]
		public string Email 
		{
			get { return GetColumnValue<string>(Columns.Email); }
			set { SetColumnValue(Columns.Email, value); }
		}
		  
		[XmlAttribute("NombreUsuario")]
		[Bindable(true)]
		public string NombreUsuario 
		{
			get { return GetColumnValue<string>(Columns.NombreUsuario); }
			set { SetColumnValue(Columns.NombreUsuario, value); }
		}
		  
		[XmlAttribute("Password")]
		[Bindable(true)]
		public string Password 
		{
			get { return GetColumnValue<string>(Columns.Password); }
			set { SetColumnValue(Columns.Password, value); }
		}
		  
		[XmlAttribute("IdPerfil")]
		[Bindable(true)]
		public int IdPerfil 
		{
			get { return GetColumnValue<int>(Columns.IdPerfil); }
			set { SetColumnValue(Columns.IdPerfil, value); }
		}
		  
		[XmlAttribute("FechaPassword")]
		[Bindable(true)]
		public DateTime FechaPassword 
		{
			get { return GetColumnValue<DateTime>(Columns.FechaPassword); }
			set { SetColumnValue(Columns.FechaPassword, value); }
		}
		  
		[XmlAttribute("IntentosFallidos")]
		[Bindable(true)]
		public int IntentosFallidos 
		{
			get { return GetColumnValue<int>(Columns.IntentosFallidos); }
			set { SetColumnValue(Columns.IntentosFallidos, value); }
		}
		  
		[XmlAttribute("PrimeraVez")]
		[Bindable(true)]
		public bool PrimeraVez 
		{
			get { return GetColumnValue<bool>(Columns.PrimeraVez); }
			set { SetColumnValue(Columns.PrimeraVez, value); }
		}
		  
		[XmlAttribute("IdEstado")]
		[Bindable(true)]
		public int IdEstado 
		{
			get { return GetColumnValue<int>(Columns.IdEstado); }
			set { SetColumnValue(Columns.IdEstado, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.Core.CacheDetallesFacturaCollection CacheDetallesFacturaRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.CacheDetallesFacturaCollection().Where(CacheDetallesFactura.Columns.IdUsuario, IdUsuario).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodegaCollection HistoricoBodegaRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodegaCollection().Where(HistoricoBodega.Columns.IdUsuario, IdUsuario).Load();
		}
		public DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricosPrecioCollection HistoricosPrecioRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricosPrecioCollection().Where(HistoricosPrecio.Columns.IdUsuarioCambio, IdUsuario).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a MasEstados ActiveRecord object related to this Usuarios
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.MasEstados MasEstados
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.MasEstados.FetchByID(this.IdEstado); }
			set { SetColumnValue("ID_ESTADO", value.IdEstado); }
		}
		
		
		/// <summary>
		/// Returns a Perfiles ActiveRecord object related to this Usuarios
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.Core.Perfiles Perfiles
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.Core.Perfiles.FetchByID(this.IdPerfil); }
			set { SetColumnValue("ID_PERFIL", value.IdPerfil); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varIdentificacion,string varNombre,string varEmail,string varNombreUsuario,string varPassword,int varIdPerfil,DateTime varFechaPassword,int varIntentosFallidos,bool varPrimeraVez,int varIdEstado)
		{
			Usuarios item = new Usuarios();
			
			item.Identificacion = varIdentificacion;
			
			item.Nombre = varNombre;
			
			item.Email = varEmail;
			
			item.NombreUsuario = varNombreUsuario;
			
			item.Password = varPassword;
			
			item.IdPerfil = varIdPerfil;
			
			item.FechaPassword = varFechaPassword;
			
			item.IntentosFallidos = varIntentosFallidos;
			
			item.PrimeraVez = varPrimeraVez;
			
			item.IdEstado = varIdEstado;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(int varIdUsuario,string varIdentificacion,string varNombre,string varEmail,string varNombreUsuario,string varPassword,int varIdPerfil,DateTime varFechaPassword,int varIntentosFallidos,bool varPrimeraVez,int varIdEstado)
		{
			Usuarios item = new Usuarios();
			
				item.IdUsuario = varIdUsuario;
			
				item.Identificacion = varIdentificacion;
			
				item.Nombre = varNombre;
			
				item.Email = varEmail;
			
				item.NombreUsuario = varNombreUsuario;
			
				item.Password = varPassword;
			
				item.IdPerfil = varIdPerfil;
			
				item.FechaPassword = varFechaPassword;
			
				item.IntentosFallidos = varIntentosFallidos;
			
				item.PrimeraVez = varPrimeraVez;
			
				item.IdEstado = varIdEstado;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdUsuarioColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn IdentificacionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn EmailColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn NombreUsuarioColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn PasswordColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPerfilColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaPasswordColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn IntentosFallidosColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn PrimeraVezColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdEstadoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string IdUsuario = @"ID_USUARIO";
			 public static string Identificacion = @"IDENTIFICACION";
			 public static string Nombre = @"NOMBRE";
			 public static string Email = @"EMAIL";
			 public static string NombreUsuario = @"NOMBRE_USUARIO";
			 public static string Password = @"PASSWORD";
			 public static string IdPerfil = @"ID_PERFIL";
			 public static string FechaPassword = @"FECHA_PASSWORD";
			 public static string IntentosFallidos = @"INTENTOS_FALLIDOS";
			 public static string PrimeraVez = @"PRIMERA_VEZ";
			 public static string IdEstado = @"ID_ESTADO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
