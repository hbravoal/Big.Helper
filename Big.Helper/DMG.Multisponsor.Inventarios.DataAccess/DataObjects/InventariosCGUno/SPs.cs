using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.CGUno{
    public partial class SPs{
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_BATCH1 Procedure
        /// </summary>
        public static StoredProcedure BATCH1(decimal? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_BATCH1", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPERIODO", IDPERIODO, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_BATCH2 Procedure
        /// </summary>
        public static StoredProcedure BATCH2(decimal? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_BATCH2", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPERIODO", IDPERIODO, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_BATCHS Procedure
        /// </summary>
        public static StoredProcedure Batchs(decimal? IDPERIODO, string USER)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_BATCHS", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPERIODO", IDPERIODO, DbType.Decimal, 0, 18);
        	
            sp.Command.AddParameter("@USER", USER, DbType.String, null, null);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_ELIMINAR_CENTRO_UTILIDAD Procedure
        /// </summary>
        public static StoredProcedure EliminarCentroUtilidad(int? ID)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_ELIMINAR_CENTRO_UTILIDAD", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@ID", ID, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_ELIMINAR_PERIODOS_CAUSACIONES_BASICAS Procedure
        /// </summary>
        public static StoredProcedure EliminarPeriodosCausacionesBasicas(decimal? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_ELIMINAR_PERIODOS_CAUSACIONES_BASICAS", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPERIODO", IDPERIODO, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_MODIFICAR_ESTADOS_PERIODOS_CAUSACIONES Procedure
        /// </summary>
        public static StoredProcedure ModificarEstadosPeriodosCausaciones(decimal? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_MODIFICAR_ESTADOS_PERIODOS_CAUSACIONES", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPERIODO", IDPERIODO, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_CAUSACIONES_BASICAS Procedure
        /// </summary>
        public static StoredProcedure ObtenerCausacionesBasicas()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_CAUSACIONES_BASICAS", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_CENTROS_UTILIDAD Procedure
        /// </summary>
        public static StoredProcedure ObtenerCentrosUtilidad()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_CENTROS_UTILIDAD", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_DETALLE_ITEM_CAUSACION_PERIODO Procedure
        /// </summary>
        public static StoredProcedure ObtenerDetalleItemCausacionPeriodo(decimal? IDDETALLECAUSACION, string PERIODO, int? IDPROGRAMA)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_DETALLE_ITEM_CAUSACION_PERIODO", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDDETALLECAUSACION", IDDETALLECAUSACION, DbType.Decimal, 0, 18);
        	
            sp.Command.AddParameter("@PERIODO", PERIODO, DbType.String, null, null);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_NATURALEZAS Procedure
        /// </summary>
        public static StoredProcedure ObtenerNaturalezas()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_NATURALEZAS", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_PERIODOS_CONFIGURADOS Procedure
        /// </summary>
        public static StoredProcedure ObtenerPeriodosConfigurados()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_PERIODOS_CONFIGURADOS", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_PROVEEDORES_CUENTA Procedure
        /// </summary>
        public static StoredProcedure ObtenerProveedoresCuenta(int? CODIGOPROVEEDOR)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_PROVEEDORES_CUENTA", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@CODIGO_PROVEEDOR", CODIGOPROVEEDOR, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_TIPOS_CAUSACION_BASICA Procedure
        /// </summary>
        public static StoredProcedure ObtenerTiposCausacionBasica()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_TIPOS_CAUSACION_BASICA", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_TODAS_LAS_CUENTAS Procedure
        /// </summary>
        public static StoredProcedure ObtenerTodasLasCuentas()
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_TODAS_LAS_CUENTAS", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_OBTENER_TRASACCIONES_CAUSACION_BASICA Procedure
        /// </summary>
        public static StoredProcedure ObtenerTrasaccionesCausacionBasica(int? TIPO, int? IDPROGRAMA, string PERIODO, int? DOCUMENTO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_OBTENER_TRASACCIONES_CAUSACION_BASICA", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@TIPO", TIPO, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@ID_PROGRAMA", IDPROGRAMA, DbType.Int32, 0, 10);
        	
            sp.Command.AddParameter("@PERIODO", PERIODO, DbType.AnsiString, null, null);
        	
            sp.Command.AddParameter("@DOCUMENTO", DOCUMENTO, DbType.Int32, 0, 10);
        	
            return sp;
        }
        
        /// <summary>
        /// Creates an object wrapper for the INV_CGUNO_USP_REVERSAR_PERIODO_CAUSACION Procedure
        /// </summary>
        public static StoredProcedure ReversarPeriodoCausacion(decimal? IDPERIODO)
        {
            SubSonic.StoredProcedure sp = new SubSonic.StoredProcedure("INV_CGUNO_USP_REVERSAR_PERIODO_CAUSACION", DataService.GetInstance("InventariosCGUnoProvider"), "dbo");
        	
            sp.Command.AddParameter("@IDPERIODO", IDPERIODO, DbType.Decimal, 0, 18);
        	
            return sp;
        }
        
    }
    
}
