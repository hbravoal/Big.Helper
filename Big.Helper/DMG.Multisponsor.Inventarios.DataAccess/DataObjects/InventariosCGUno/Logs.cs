using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.CGUno
{
	/// <summary>
	/// Strongly-typed collection for the Logs class.
	/// </summary>
    [Serializable]
	public partial class LogsCollection : ActiveList<Logs, LogsCollection>
	{	   
		public LogsCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>LogsCollection</returns>
		public LogsCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                Logs o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_LOGS table.
	/// </summary>
	[Serializable]
	public partial class Logs : ActiveRecord<Logs>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public Logs()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public Logs(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public Logs(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public Logs(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_LOGS", TableType.Table, DataService.GetInstance("InventariosCGUnoProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.String;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = true;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarPeriodoCausacion = new TableSchema.TableColumn(schema);
				colvarPeriodoCausacion.ColumnName = "PERIODO_CAUSACION";
				colvarPeriodoCausacion.DataType = DbType.String;
				colvarPeriodoCausacion.MaxLength = 300;
				colvarPeriodoCausacion.AutoIncrement = false;
				colvarPeriodoCausacion.IsNullable = false;
				colvarPeriodoCausacion.IsPrimaryKey = false;
				colvarPeriodoCausacion.IsForeignKey = false;
				colvarPeriodoCausacion.IsReadOnly = false;
				colvarPeriodoCausacion.DefaultSetting = @"";
				colvarPeriodoCausacion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarPeriodoCausacion);
				
				TableSchema.TableColumn colvarEstado = new TableSchema.TableColumn(schema);
				colvarEstado.ColumnName = "ESTADO";
				colvarEstado.DataType = DbType.String;
				colvarEstado.MaxLength = 30;
				colvarEstado.AutoIncrement = false;
				colvarEstado.IsNullable = false;
				colvarEstado.IsPrimaryKey = false;
				colvarEstado.IsForeignKey = false;
				colvarEstado.IsReadOnly = false;
				colvarEstado.DefaultSetting = @"";
				colvarEstado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarEstado);
				
				TableSchema.TableColumn colvarUsuario = new TableSchema.TableColumn(schema);
				colvarUsuario.ColumnName = "USUARIO";
				colvarUsuario.DataType = DbType.Int32;
				colvarUsuario.MaxLength = 0;
				colvarUsuario.AutoIncrement = false;
				colvarUsuario.IsNullable = false;
				colvarUsuario.IsPrimaryKey = false;
				colvarUsuario.IsForeignKey = true;
				colvarUsuario.IsReadOnly = false;
				colvarUsuario.DefaultSetting = @"";
				
					colvarUsuario.ForeignKeyTableName = "INV_USUARIOS";
				schema.Columns.Add(colvarUsuario);
				
				TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
				colvarFecha.ColumnName = "FECHA";
				colvarFecha.DataType = DbType.DateTime;
				colvarFecha.MaxLength = 0;
				colvarFecha.AutoIncrement = false;
				colvarFecha.IsNullable = false;
				colvarFecha.IsPrimaryKey = false;
				colvarFecha.IsForeignKey = false;
				colvarFecha.IsReadOnly = false;
				colvarFecha.DefaultSetting = @"";
				colvarFecha.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecha);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosCGUnoProvider"].AddSchema("INV_CGUNO_LOGS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("PeriodoCausacion")]
		[Bindable(true)]
		public string PeriodoCausacion 
		{
			get { return GetColumnValue<string>(Columns.PeriodoCausacion); }
			set { SetColumnValue(Columns.PeriodoCausacion, value); }
		}
		  
		[XmlAttribute("Estado")]
		[Bindable(true)]
		public string Estado 
		{
			get { return GetColumnValue<string>(Columns.Estado); }
			set { SetColumnValue(Columns.Estado, value); }
		}
		  
		[XmlAttribute("Usuario")]
		[Bindable(true)]
		public int Usuario 
		{
			get { return GetColumnValue<int>(Columns.Usuario); }
			set { SetColumnValue(Columns.Usuario, value); }
		}
		  
		[XmlAttribute("Fecha")]
		[Bindable(true)]
		public DateTime Fecha 
		{
			get { return GetColumnValue<DateTime>(Columns.Fecha); }
			set { SetColumnValue(Columns.Fecha, value); }
		}
		
		#endregion
		
		
			
		
		#region ForeignKey Properties
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,string varPeriodoCausacion,string varEstado,int varUsuario,DateTime varFecha)
		{
			Logs item = new Logs();
			
			item.Guid = varGuid;
			
			item.PeriodoCausacion = varPeriodoCausacion;
			
			item.Estado = varEstado;
			
			item.Usuario = varUsuario;
			
			item.Fecha = varFecha;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(string varGuid,string varPeriodoCausacion,string varEstado,int varUsuario,DateTime varFecha)
		{
			Logs item = new Logs();
			
				item.Guid = varGuid;
			
				item.PeriodoCausacion = varPeriodoCausacion;
			
				item.Estado = varEstado;
			
				item.Usuario = varUsuario;
			
				item.Fecha = varFecha;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn PeriodoCausacionColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn EstadoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn UsuarioColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Guid = @"GUID";
			 public static string PeriodoCausacion = @"PERIODO_CAUSACION";
			 public static string Estado = @"ESTADO";
			 public static string Usuario = @"USUARIO";
			 public static string Fecha = @"FECHA";
						
		}
		#endregion
		
		#region Update PK Collections
		
        #endregion
    
        #region Deep Save
		
        #endregion
	}
}
