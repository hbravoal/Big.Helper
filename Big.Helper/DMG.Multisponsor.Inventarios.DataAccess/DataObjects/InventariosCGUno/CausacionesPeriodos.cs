using System; 
using System.Text; 
using System.Data;
using System.Data.SqlClient;
using System.Data.Common;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration; 
using System.Xml; 
using System.Xml.Serialization;
using SubSonic; 
using SubSonic.Utilities;
namespace DMG.Multisponsor.Inventarios.DataAccess.CGUno
{
	/// <summary>
	/// Strongly-typed collection for the CausacionesPeriodos class.
	/// </summary>
    [Serializable]
	public partial class CausacionesPeriodosCollection : ActiveList<CausacionesPeriodos, CausacionesPeriodosCollection>
	{	   
		public CausacionesPeriodosCollection() {}
        
        /// <summary>
		/// Filters an existing collection based on the set criteria. This is an in-memory filter
		/// Thanks to developingchris for this!
        /// </summary>
        /// <returns>CausacionesPeriodosCollection</returns>
		public CausacionesPeriodosCollection Filter()
        {
            for (int i = this.Count - 1; i > -1; i--)
            {
                CausacionesPeriodos o = this[i];
                foreach (SubSonic.Where w in this.wheres)
                {
                    bool remove = false;
                    System.Reflection.PropertyInfo pi = o.GetType().GetProperty(w.ColumnName);
                    if (pi.CanRead)
                    {
                        object val = pi.GetValue(o, null);
                        switch (w.Comparison)
                        {
                            case SubSonic.Comparison.Equals:
                                if (!val.Equals(w.ParameterValue))
                                {
                                    remove = true;
                                }
                                break;
                        }
                    }
                    if (remove)
                    {
                        this.Remove(o);
                        break;
                    }
                }
            }
            return this;
        }
		
		
	}
	/// <summary>
	/// This is an ActiveRecord class which wraps the INV_CGUNO_CAUSACIONES_PERIODOS table.
	/// </summary>
	[Serializable]
	public partial class CausacionesPeriodos : ActiveRecord<CausacionesPeriodos>, IActiveRecord
	{
		#region .ctors and Default Settings
		
		public CausacionesPeriodos()
		{
		  SetSQLProps();
		  InitSetDefaults();
		  MarkNew();
		}
		
		private void InitSetDefaults() { SetDefaults(); }
		
		public CausacionesPeriodos(bool useDatabaseDefaults)
		{
			SetSQLProps();
			if(useDatabaseDefaults)
				ForceDefaults();
			MarkNew();
		}
        
		public CausacionesPeriodos(object keyID)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByKey(keyID);
		}
		 
		public CausacionesPeriodos(string columnName, object columnValue)
		{
			SetSQLProps();
			InitSetDefaults();
			LoadByParam(columnName,columnValue);
		}
		
		protected static void SetSQLProps() { GetTableSchema(); }
		
		#endregion
		
		#region Schema and Query Accessor	
		public static Query CreateQuery() { return new Query(Schema); }
		public static TableSchema.Table Schema
		{
			get
			{
				if (BaseSchema == null)
					SetSQLProps();
				return BaseSchema;
			}
		}
		
		private static void GetTableSchema() 
		{
			if(!IsSchemaInitialized)
			{
				//Schema declaration
				TableSchema.Table schema = new TableSchema.Table("INV_CGUNO_CAUSACIONES_PERIODOS", TableType.Table, DataService.GetInstance("InventariosCGUnoProvider"));
				schema.Columns = new TableSchema.TableColumnCollection();
				schema.SchemaName = @"dbo";
				//columns
				
				TableSchema.TableColumn colvarId = new TableSchema.TableColumn(schema);
				colvarId.ColumnName = "ID";
				colvarId.DataType = DbType.Decimal;
				colvarId.MaxLength = 0;
				colvarId.AutoIncrement = true;
				colvarId.IsNullable = false;
				colvarId.IsPrimaryKey = true;
				colvarId.IsForeignKey = false;
				colvarId.IsReadOnly = false;
				colvarId.DefaultSetting = @"";
				colvarId.ForeignKeyTableName = "";
				schema.Columns.Add(colvarId);
				
				TableSchema.TableColumn colvarGuid = new TableSchema.TableColumn(schema);
				colvarGuid.ColumnName = "GUID";
				colvarGuid.DataType = DbType.String;
				colvarGuid.MaxLength = 36;
				colvarGuid.AutoIncrement = false;
				colvarGuid.IsNullable = false;
				colvarGuid.IsPrimaryKey = false;
				colvarGuid.IsForeignKey = false;
				colvarGuid.IsReadOnly = false;
				colvarGuid.DefaultSetting = @"";
				colvarGuid.ForeignKeyTableName = "";
				schema.Columns.Add(colvarGuid);
				
				TableSchema.TableColumn colvarDocumento = new TableSchema.TableColumn(schema);
				colvarDocumento.ColumnName = "DOCUMENTO";
				colvarDocumento.DataType = DbType.String;
				colvarDocumento.MaxLength = 15;
				colvarDocumento.AutoIncrement = false;
				colvarDocumento.IsNullable = false;
				colvarDocumento.IsPrimaryKey = false;
				colvarDocumento.IsForeignKey = false;
				colvarDocumento.IsReadOnly = false;
				colvarDocumento.DefaultSetting = @"";
				colvarDocumento.ForeignKeyTableName = "";
				schema.Columns.Add(colvarDocumento);
				
				TableSchema.TableColumn colvarFecha = new TableSchema.TableColumn(schema);
				colvarFecha.ColumnName = "FECHA";
				colvarFecha.DataType = DbType.DateTime;
				colvarFecha.MaxLength = 0;
				colvarFecha.AutoIncrement = false;
				colvarFecha.IsNullable = false;
				colvarFecha.IsPrimaryKey = false;
				colvarFecha.IsForeignKey = false;
				colvarFecha.IsReadOnly = false;
				colvarFecha.DefaultSetting = @"";
				colvarFecha.ForeignKeyTableName = "";
				schema.Columns.Add(colvarFecha);
				
				TableSchema.TableColumn colvarIdentificacionCliente = new TableSchema.TableColumn(schema);
				colvarIdentificacionCliente.ColumnName = "IDENTIFICACION_CLIENTE";
				colvarIdentificacionCliente.DataType = DbType.String;
				colvarIdentificacionCliente.MaxLength = 20;
				colvarIdentificacionCliente.AutoIncrement = false;
				colvarIdentificacionCliente.IsNullable = false;
				colvarIdentificacionCliente.IsPrimaryKey = false;
				colvarIdentificacionCliente.IsForeignKey = false;
				colvarIdentificacionCliente.IsReadOnly = false;
				colvarIdentificacionCliente.DefaultSetting = @"";
				colvarIdentificacionCliente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIdentificacionCliente);
				
				TableSchema.TableColumn colvarCliente = new TableSchema.TableColumn(schema);
				colvarCliente.ColumnName = "CLIENTE";
				colvarCliente.DataType = DbType.String;
				colvarCliente.MaxLength = 100;
				colvarCliente.AutoIncrement = false;
				colvarCliente.IsNullable = false;
				colvarCliente.IsPrimaryKey = false;
				colvarCliente.IsForeignKey = false;
				colvarCliente.IsReadOnly = false;
				colvarCliente.DefaultSetting = @"";
				colvarCliente.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCliente);
				
				TableSchema.TableColumn colvarSubtotal = new TableSchema.TableColumn(schema);
				colvarSubtotal.ColumnName = "SUBTOTAL";
				colvarSubtotal.DataType = DbType.Decimal;
				colvarSubtotal.MaxLength = 0;
				colvarSubtotal.AutoIncrement = false;
				colvarSubtotal.IsNullable = false;
				colvarSubtotal.IsPrimaryKey = false;
				colvarSubtotal.IsForeignKey = false;
				colvarSubtotal.IsReadOnly = false;
				colvarSubtotal.DefaultSetting = @"";
				colvarSubtotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarSubtotal);
				
				TableSchema.TableColumn colvarIva = new TableSchema.TableColumn(schema);
				colvarIva.ColumnName = "IVA";
				colvarIva.DataType = DbType.Decimal;
				colvarIva.MaxLength = 0;
				colvarIva.AutoIncrement = false;
				colvarIva.IsNullable = false;
				colvarIva.IsPrimaryKey = false;
				colvarIva.IsForeignKey = false;
				colvarIva.IsReadOnly = false;
				colvarIva.DefaultSetting = @"";
				colvarIva.ForeignKeyTableName = "";
				schema.Columns.Add(colvarIva);
				
				TableSchema.TableColumn colvarRetencion = new TableSchema.TableColumn(schema);
				colvarRetencion.ColumnName = "RETENCION";
				colvarRetencion.DataType = DbType.Decimal;
				colvarRetencion.MaxLength = 0;
				colvarRetencion.AutoIncrement = false;
				colvarRetencion.IsNullable = false;
				colvarRetencion.IsPrimaryKey = false;
				colvarRetencion.IsForeignKey = false;
				colvarRetencion.IsReadOnly = false;
				colvarRetencion.DefaultSetting = @"";
				colvarRetencion.ForeignKeyTableName = "";
				schema.Columns.Add(colvarRetencion);
				
				TableSchema.TableColumn colvarTotal = new TableSchema.TableColumn(schema);
				colvarTotal.ColumnName = "TOTAL";
				colvarTotal.DataType = DbType.Decimal;
				colvarTotal.MaxLength = 0;
				colvarTotal.AutoIncrement = false;
				colvarTotal.IsNullable = false;
				colvarTotal.IsPrimaryKey = false;
				colvarTotal.IsForeignKey = false;
				colvarTotal.IsReadOnly = false;
				colvarTotal.DefaultSetting = @"";
				colvarTotal.ForeignKeyTableName = "";
				schema.Columns.Add(colvarTotal);
				
				TableSchema.TableColumn colvarIdPeriodo = new TableSchema.TableColumn(schema);
				colvarIdPeriodo.ColumnName = "ID_PERIODO";
				colvarIdPeriodo.DataType = DbType.Decimal;
				colvarIdPeriodo.MaxLength = 0;
				colvarIdPeriodo.AutoIncrement = false;
				colvarIdPeriodo.IsNullable = false;
				colvarIdPeriodo.IsPrimaryKey = false;
				colvarIdPeriodo.IsForeignKey = true;
				colvarIdPeriodo.IsReadOnly = false;
				colvarIdPeriodo.DefaultSetting = @"";
				
					colvarIdPeriodo.ForeignKeyTableName = "INV_CGUNO_PERIODOS";
				schema.Columns.Add(colvarIdPeriodo);
				
				TableSchema.TableColumn colvarCorrecto = new TableSchema.TableColumn(schema);
				colvarCorrecto.ColumnName = "CORRECTO";
				colvarCorrecto.DataType = DbType.Boolean;
				colvarCorrecto.MaxLength = 0;
				colvarCorrecto.AutoIncrement = false;
				colvarCorrecto.IsNullable = false;
				colvarCorrecto.IsPrimaryKey = false;
				colvarCorrecto.IsForeignKey = false;
				colvarCorrecto.IsReadOnly = false;
				
						colvarCorrecto.DefaultSetting = @"((0))";
				colvarCorrecto.ForeignKeyTableName = "";
				schema.Columns.Add(colvarCorrecto);
				
				TableSchema.TableColumn colvarExportado = new TableSchema.TableColumn(schema);
				colvarExportado.ColumnName = "EXPORTADO";
				colvarExportado.DataType = DbType.Boolean;
				colvarExportado.MaxLength = 0;
				colvarExportado.AutoIncrement = false;
				colvarExportado.IsNullable = false;
				colvarExportado.IsPrimaryKey = false;
				colvarExportado.IsForeignKey = false;
				colvarExportado.IsReadOnly = false;
				
						colvarExportado.DefaultSetting = @"((0))";
				colvarExportado.ForeignKeyTableName = "";
				schema.Columns.Add(colvarExportado);
				
				BaseSchema = schema;
				//add this schema to the provider
				//so we can query it later
				DataService.Providers["InventariosCGUnoProvider"].AddSchema("INV_CGUNO_CAUSACIONES_PERIODOS",schema);
			}
		}
		#endregion
		
		#region Props
		  
		[XmlAttribute("Id")]
		[Bindable(true)]
		public decimal Id 
		{
			get { return GetColumnValue<decimal>(Columns.Id); }
			set { SetColumnValue(Columns.Id, value); }
		}
		  
		[XmlAttribute("Guid")]
		[Bindable(true)]
		public string Guid 
		{
			get { return GetColumnValue<string>(Columns.Guid); }
			set { SetColumnValue(Columns.Guid, value); }
		}
		  
		[XmlAttribute("Documento")]
		[Bindable(true)]
		public string Documento 
		{
			get { return GetColumnValue<string>(Columns.Documento); }
			set { SetColumnValue(Columns.Documento, value); }
		}
		  
		[XmlAttribute("Fecha")]
		[Bindable(true)]
		public DateTime Fecha 
		{
			get { return GetColumnValue<DateTime>(Columns.Fecha); }
			set { SetColumnValue(Columns.Fecha, value); }
		}
		  
		[XmlAttribute("IdentificacionCliente")]
		[Bindable(true)]
		public string IdentificacionCliente 
		{
			get { return GetColumnValue<string>(Columns.IdentificacionCliente); }
			set { SetColumnValue(Columns.IdentificacionCliente, value); }
		}
		  
		[XmlAttribute("Cliente")]
		[Bindable(true)]
		public string Cliente 
		{
			get { return GetColumnValue<string>(Columns.Cliente); }
			set { SetColumnValue(Columns.Cliente, value); }
		}
		  
		[XmlAttribute("Subtotal")]
		[Bindable(true)]
		public decimal Subtotal 
		{
			get { return GetColumnValue<decimal>(Columns.Subtotal); }
			set { SetColumnValue(Columns.Subtotal, value); }
		}
		  
		[XmlAttribute("Iva")]
		[Bindable(true)]
		public decimal Iva 
		{
			get { return GetColumnValue<decimal>(Columns.Iva); }
			set { SetColumnValue(Columns.Iva, value); }
		}
		  
		[XmlAttribute("Retencion")]
		[Bindable(true)]
		public decimal Retencion 
		{
			get { return GetColumnValue<decimal>(Columns.Retencion); }
			set { SetColumnValue(Columns.Retencion, value); }
		}
		  
		[XmlAttribute("Total")]
		[Bindable(true)]
		public decimal Total 
		{
			get { return GetColumnValue<decimal>(Columns.Total); }
			set { SetColumnValue(Columns.Total, value); }
		}
		  
		[XmlAttribute("IdPeriodo")]
		[Bindable(true)]
		public decimal IdPeriodo 
		{
			get { return GetColumnValue<decimal>(Columns.IdPeriodo); }
			set { SetColumnValue(Columns.IdPeriodo, value); }
		}
		  
		[XmlAttribute("Correcto")]
		[Bindable(true)]
		public bool Correcto 
		{
			get { return GetColumnValue<bool>(Columns.Correcto); }
			set { SetColumnValue(Columns.Correcto, value); }
		}
		  
		[XmlAttribute("Exportado")]
		[Bindable(true)]
		public bool Exportado 
		{
			get { return GetColumnValue<bool>(Columns.Exportado); }
			set { SetColumnValue(Columns.Exportado, value); }
		}
		
		#endregion
		
		
		#region PrimaryKey Methods		
		
        protected override void SetPrimaryKey(object oValue)
        {
            base.SetPrimaryKey(oValue);
            
            SetPKValues();
        }
        
		
		public DMG.Multisponsor.Inventarios.DataAccess.CGUno.DetallesCausacionesPeriodoCollection DetallesCausacionesPeriodoRecords()
		{
			return new DMG.Multisponsor.Inventarios.DataAccess.CGUno.DetallesCausacionesPeriodoCollection().Where(DetallesCausacionesPeriodo.Columns.IdCausacionPeriodo, Id).Load();
		}
		#endregion
		
			
		
		#region ForeignKey Properties
		
		/// <summary>
		/// Returns a Periodos ActiveRecord object related to this CausacionesPeriodos
		/// 
		/// </summary>
		public DMG.Multisponsor.Inventarios.DataAccess.CGUno.Periodos Periodos
		{
			get { return DMG.Multisponsor.Inventarios.DataAccess.CGUno.Periodos.FetchByID(this.IdPeriodo); }
			set { SetColumnValue("ID_PERIODO", value.Id); }
		}
		
		
		#endregion
		
		
		
		//no ManyToMany tables defined (0)
		
        
        
		#region ObjectDataSource support
		
		
		/// <summary>
		/// Inserts a record, can be used with the Object Data Source
		/// </summary>
		public static void Insert(string varGuid,string varDocumento,DateTime varFecha,string varIdentificacionCliente,string varCliente,decimal varSubtotal,decimal varIva,decimal varRetencion,decimal varTotal,decimal varIdPeriodo,bool varCorrecto,bool varExportado)
		{
			CausacionesPeriodos item = new CausacionesPeriodos();
			
			item.Guid = varGuid;
			
			item.Documento = varDocumento;
			
			item.Fecha = varFecha;
			
			item.IdentificacionCliente = varIdentificacionCliente;
			
			item.Cliente = varCliente;
			
			item.Subtotal = varSubtotal;
			
			item.Iva = varIva;
			
			item.Retencion = varRetencion;
			
			item.Total = varTotal;
			
			item.IdPeriodo = varIdPeriodo;
			
			item.Correcto = varCorrecto;
			
			item.Exportado = varExportado;
			
		
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		
		/// <summary>
		/// Updates a record, can be used with the Object Data Source
		/// </summary>
		public static void Update(decimal varId,string varGuid,string varDocumento,DateTime varFecha,string varIdentificacionCliente,string varCliente,decimal varSubtotal,decimal varIva,decimal varRetencion,decimal varTotal,decimal varIdPeriodo,bool varCorrecto,bool varExportado)
		{
			CausacionesPeriodos item = new CausacionesPeriodos();
			
				item.Id = varId;
			
				item.Guid = varGuid;
			
				item.Documento = varDocumento;
			
				item.Fecha = varFecha;
			
				item.IdentificacionCliente = varIdentificacionCliente;
			
				item.Cliente = varCliente;
			
				item.Subtotal = varSubtotal;
			
				item.Iva = varIva;
			
				item.Retencion = varRetencion;
			
				item.Total = varTotal;
			
				item.IdPeriodo = varIdPeriodo;
			
				item.Correcto = varCorrecto;
			
				item.Exportado = varExportado;
			
			item.IsNew = false;
			if (System.Web.HttpContext.Current != null)
				item.Save(System.Web.HttpContext.Current.User.Identity.Name);
			else
				item.Save(System.Threading.Thread.CurrentPrincipal.Identity.Name);
		}
		#endregion
        
        
        
        #region Typed Columns
        
        
        public static TableSchema.TableColumn IdColumn
        {
            get { return Schema.Columns[0]; }
        }
        
        
        
        public static TableSchema.TableColumn GuidColumn
        {
            get { return Schema.Columns[1]; }
        }
        
        
        
        public static TableSchema.TableColumn DocumentoColumn
        {
            get { return Schema.Columns[2]; }
        }
        
        
        
        public static TableSchema.TableColumn FechaColumn
        {
            get { return Schema.Columns[3]; }
        }
        
        
        
        public static TableSchema.TableColumn IdentificacionClienteColumn
        {
            get { return Schema.Columns[4]; }
        }
        
        
        
        public static TableSchema.TableColumn ClienteColumn
        {
            get { return Schema.Columns[5]; }
        }
        
        
        
        public static TableSchema.TableColumn SubtotalColumn
        {
            get { return Schema.Columns[6]; }
        }
        
        
        
        public static TableSchema.TableColumn IvaColumn
        {
            get { return Schema.Columns[7]; }
        }
        
        
        
        public static TableSchema.TableColumn RetencionColumn
        {
            get { return Schema.Columns[8]; }
        }
        
        
        
        public static TableSchema.TableColumn TotalColumn
        {
            get { return Schema.Columns[9]; }
        }
        
        
        
        public static TableSchema.TableColumn IdPeriodoColumn
        {
            get { return Schema.Columns[10]; }
        }
        
        
        
        public static TableSchema.TableColumn CorrectoColumn
        {
            get { return Schema.Columns[11]; }
        }
        
        
        
        public static TableSchema.TableColumn ExportadoColumn
        {
            get { return Schema.Columns[12]; }
        }
        
        
        
        #endregion
		#region Columns Struct
		public struct Columns
		{
			 public static string Id = @"ID";
			 public static string Guid = @"GUID";
			 public static string Documento = @"DOCUMENTO";
			 public static string Fecha = @"FECHA";
			 public static string IdentificacionCliente = @"IDENTIFICACION_CLIENTE";
			 public static string Cliente = @"CLIENTE";
			 public static string Subtotal = @"SUBTOTAL";
			 public static string Iva = @"IVA";
			 public static string Retencion = @"RETENCION";
			 public static string Total = @"TOTAL";
			 public static string IdPeriodo = @"ID_PERIODO";
			 public static string Correcto = @"CORRECTO";
			 public static string Exportado = @"EXPORTADO";
						
		}
		#endregion
		
		#region Update PK Collections
		
        public void SetPKValues()
        {
}
        #endregion
    
        #region Deep Save
		
        public void DeepSave()
        {
            Save();
            
}
        #endregion
	}
}
