﻿using System.Web;
using System.Web.Mvc;

namespace Big.Multisponsor.Inventarios.IUV2
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}
