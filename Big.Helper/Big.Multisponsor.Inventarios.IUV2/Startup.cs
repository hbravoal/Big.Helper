﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Big.Multisponsor.Inventarios.IUV2.Startup))]
namespace Big.Multisponsor.Inventarios.IUV2
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
