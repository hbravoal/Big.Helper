﻿using Big.Multisponsor.Inventarios.IUV2.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Big.Multisponsor.Inventarios.IUV2.Controllers
{
    public class StartedController : Controller
    {
        // GET: Started
        public ActionResult Index()
        {
            var product = Categories.GetAllRecords();
            ViewBag.datasource = product;
            return View();
            
        }

        // GET: Started/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Started/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Started/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Started/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Started/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Started/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Started/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
