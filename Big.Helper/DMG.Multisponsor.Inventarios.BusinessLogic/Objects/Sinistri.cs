// =============================================
// Author:		C�sar A. L�pez A.
// Create date: 10 de Diciembre de 2008.
// Description:	InventariosController.cs
// Email: clopez@dmgcolombia.com
// =============================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class Sinistri
  {
    private List<SinistriDetails> _detalle;

    public Sinistri() { }
    
    public List<SinistriDetails> GetDetailsSinistri(int? idprograma)
    {
      _detalle = new List<SinistriDetails>();
      DataSet ds = SinistriController.GetSiniestros(idprograma);
      if (ds != null && ds.Tables.Count > 0)
      {
        foreach (DataRow dr in ds.Tables[0].Rows)
        {
          _detalle.Add(new SinistriDetails(
            dr["PROGRAMA"].ToString(),
            dr["RAZON_SOCIAL"].ToString(),
            dr["PRODUCTO"].ToString(),
            dr["FECHA"].ToString(),
            dr["CANTIDAD"].ToString(),
            dr["COSTO_BASE"].ToString(),
            dr["COSTO_TOTAL"].ToString(),
            dr["JUSTIFICACION"].ToString()));
        }
      }
      return _detalle;
    }

  }
}
