using System;
using System.Collections.Generic;
using System.Text;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class PreciosProgramas
    {
        #region VariablesPrivadas
        private decimal _precioBase;
        private decimal _costoEnvioBase;
        private decimal? _valorAsegurado;
        private int _guidPrograma;
        private int _guidMarca;
        private int _guidTipoDelivery;
        private string _direcciones;
        private int? _idCiudades;
        #endregion

        #region Propiedades
        public decimal PrecioBase
        {
            set { _precioBase = value; }
            get { return _precioBase; }
        }
        public decimal CostoEnvioBase
        {
            set { _costoEnvioBase = value; }
            get { return _costoEnvioBase; }
        }

        public decimal? ValorAsegurado
        {
            set { _valorAsegurado = value; }
            get { return _valorAsegurado; }
        }
        public int GuidPrograma
        {
            set { _guidPrograma = value; }
            get { return _guidPrograma; }
        }
        public int GuidMarca
        {
            set { _guidMarca = value; }
            get { return _guidMarca; }
        }
        public int GuidTipoDelivery
        {
            set { _guidTipoDelivery = value; }
            get { return _guidTipoDelivery; }
        }
        public string Direcciones
        {
            set { _direcciones = value; }
            get { return _direcciones; }
        }
        public int? Ciudades
        {
            set { _idCiudades = value; }
            get { return _idCiudades; }
        }
        #endregion

        #region Constructores
        public PreciosProgramas() { }
        public PreciosProgramas(decimal precioBase, decimal costoEnvioBase, decimal? valorAsegurado, int guidPrograma, int guidMarca, int guidTipoDelivery, string direcciones,int? ciudades)
        {
            _precioBase = precioBase;
            _costoEnvioBase = costoEnvioBase;
            _valorAsegurado = valorAsegurado;
            _guidPrograma = guidPrograma;
            _guidMarca = guidMarca;
            _guidTipoDelivery = guidTipoDelivery;
            _direcciones = direcciones;
            _idCiudades = ciudades;
        }
        #endregion
    }
}
