// =============================================
// Author:		C�sar A. L�pez A.
// Create date: 10 de Diciembre de 2008.
// Description:	InventariosController.cs
// Email: clopez@dmgcolombia.com
// =============================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class SinistriDetails
  {
    private string _programa;

    public string Programa
    {
      get { return _programa; }
      set { _programa = value; }
    }

    private string _proveedor;

    public string Proveedor
    {
      get { return _proveedor; }
      set { _proveedor = value; }
    }
    private string _producto;

    public string Producto
    {
      get { return _producto; }
      set { _producto = value; }
    }
    private string _fecha;

    public string Fecha
    {
      get { return _fecha; }
      set { _fecha = value; }
    }
    private string _cantidad;

    public string Cantidad
    {
      get { return _cantidad; }
      set { _cantidad = value; }
    }
    private string _costo;

    public string Costo 
    {
      get { return _costo; }
      set { _costo = value; }
    }
    private string _total;

    public string Total
    {
      get { return _total; }
      set { _total = value; }
    }

    private string _justificacion;

    public string Justificacion
    {
      get { return _justificacion; }
      set { _justificacion = value; }
    }
    public SinistriDetails() { }

    public SinistriDetails(string programa, string proveedor, string producto, string fecha, string cantidad, string costo, string total, string justificaicion)
    {
      _cantidad = cantidad;
      _costo = costo;
      _justificacion = justificaicion;
      _fecha = fecha;
      _producto = producto;
      _programa = programa;
      _proveedor = proveedor;
      _total = total;
    }
  }  
}
