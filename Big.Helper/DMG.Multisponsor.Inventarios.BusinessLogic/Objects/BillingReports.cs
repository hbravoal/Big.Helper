// =============================================
// Author:		C�sar A. L�pez A.
// Create date: 10 de Diciembre de 2008.
// Description:	BillingReports.cs
// Email: cesar.lopez@ondata.com.co
// =============================================
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Text;
using System.Data;

namespace DMG.Multisponsor.Inventarios.BusinessLogic.Objects
{
  public class BillingReports
  {
    #region [Private Fields]
    private string _numeroFactura;
    public string NumeroFactura
    {
      get { return _numeroFactura; }
      set { _numeroFactura = value; }
    }
    private string _totalNeto;
    public string TotalNeto
    {
      get { return _totalNeto; }
      set { _totalNeto = value; }
    }
    private string _fecha;
    public string Fecha
    {
      get { return _fecha; }
      set { _fecha = value; }
    }
    private string _nit;
    public string Nit
    {
      get { return _nit; }
      set { _nit = value; }
    }
    private string _nombreCliente;
    public string NombreCliente
    {
      get { return _nombreCliente; }
      set { _nombreCliente = value; }
    }
    private string _anulada;
    public string Anulada
    {
      get { return _anulada; }
      set { _anulada = value; }
    }
    private string _notaCredito;
    public string NotaCredito
    {
      get { return _notaCredito; }
      set { _notaCredito = value; }
    }
    private string _razonSocial;
    public string RazonSocial
    {
      get { return _razonSocial; }
      set { _razonSocial = value; }
    }
    private string _producto;
    public string Producto
    {
      get { return _producto; }
      set { _producto = value; }
    }
    private string _cantidad;
    public string Cantidad
    {
      get { return _cantidad; }
      set { _cantidad = value; }
    }
    private string _valorUnitario;
    public string ValorUnitario
    {
      get { return _valorUnitario; }
      set { _valorUnitario = value; }
    }
    private string _iva;
    public string Iva
    {
      get { return _iva; }
      set { _iva = value; }
    }
    private string _valorTotal;
    public string ValorTotal
    {
      get { return _valorTotal; }
      set { _valorTotal = value; }
    }
    private string _porcetanjePuntos;
    public string PorcetanjePuntos
    {
      get { return _porcetanjePuntos; }
      set { _porcetanjePuntos = value; }
    }
    #endregion

    #region [Public Properties]
    public BillingReports()
    {
      _numeroFactura = string.Empty;
      _totalNeto = string.Empty;
      _fecha = string.Empty;
      _nit = string.Empty;
      _nombreCliente = string.Empty;
      _anulada = string.Empty;
      _notaCredito = string.Empty;
      _razonSocial = string.Empty;
      _producto = string.Empty;
      _cantidad = string.Empty;
      _valorUnitario = string.Empty;
      _iva = string.Empty;
      _valorTotal = string.Empty;
      _porcetanjePuntos = string.Empty;
    }
    public BillingReports
      (
        string numeroFactura,
        string totalNeto,
        string fecha,
        string nit,
        string nombreCliente,
        string anulada,
        string notaCredito,
        string razonSocial,
        string producto,
        string cantidad,
        string valorUnitario,
        string iva,
        string valorTotal,
        string porcetanjePuntos
      )
    {
      _numeroFactura = numeroFactura;
      _totalNeto = totalNeto;
      _fecha = fecha;
      _nit = nit;
      _nombreCliente = nombreCliente;
      _anulada = anulada;
      _notaCredito = notaCredito;
      _razonSocial = razonSocial;
      _producto = producto;
      _cantidad = cantidad;
      _valorUnitario = valorUnitario;
      _iva = iva;
      _valorTotal = valorTotal;
      _porcetanjePuntos = porcetanjePuntos;
    }
    #endregion

    #region [Methods]
    public List<BillingReports> GetBillingReports
      (
        int? numeroFactura,
        string nit,
        DateTime? dtInicio,
        DateTime? dtFinal,
        int? idPrograma,
        int? idProveedor,
        string guidProducto,
        decimal? porcentajePuntos,
        bool? anulada,
        bool? notaCredito
        )
    {
      try
      {
        if (anulada == true)
          anulada = null;
        if (notaCredito == true)
          notaCredito = null;

        int TotalRecords = 0;
        DataSet ds = FacturacionController.GetInformeFacturas(
          numeroFactura,
          nit,
          dtInicio,
          dtFinal,
          idPrograma,
          idProveedor,
          null,
          porcentajePuntos,
          anulada,
          notaCredito,
          int.MaxValue,
          0,
          out TotalRecords);


        List<BillingReports> lsBilling = new List<BillingReports>();

        if (ds != null && ds.Tables.Count > 0)
        {
          foreach (DataRow dr in ds.Tables[0].Rows)
          {
            lsBilling.Add(
              new BillingReports(
              dr["NUMERO_FACTURA"].ToString(),
              dr["TOTAL_NETO"].ToString(),
              dr["FECHA"].ToString(),
              dr["NIT"].ToString(),
              dr["NOMBRE_CLIENTE"].ToString(),
              dr["ANULADA"].ToString(),
              dr["NOTA_CREDITO"].ToString(),
              dr["RAZON_SOCIAL"].ToString(),
              dr["PRODUCTO"].ToString(),
              dr["CANTIDAD"].ToString(),
              dr["VALOR_UNITARIO"].ToString(),
              dr["IVA"].ToString(),
              dr["VALOR_TOTAL"].ToString(),
              dr["PORCENTAJE_PUNTOS"].ToString()));
          }
          return lsBilling;
        }
        else
          return null;
      }
      catch
      {
        return null;
      }
    }
    #endregion
  }
}
