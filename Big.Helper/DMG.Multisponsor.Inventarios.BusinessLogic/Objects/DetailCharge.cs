using System;
using System.Collections.Generic;
using System.Text;
using System.Transactions;
using SubSonic;
namespace DMG.Multisponsor.Inventarios.BusinessLogic.Objects
{
    [Serializable]
    public class DetailCharge
    {

        private string _guidDetalle;

        public string GuidDetalle
        {
            get { return _guidDetalle; }
            set { _guidDetalle = value; }
        }

        private int _idPrograma;

        public int IdPrograma
        {
            get { return _idPrograma; }
            set { _idPrograma = value; }
        }


        private int _idEstablecimiento;

        public int IdEstablecimiento
        {
            get { return _idEstablecimiento; }
            set { _idEstablecimiento = value; }
        }


        private int _idBodega;

        public int IdBodega
        {
            get { return _idBodega; }
            set { _idBodega = value; }
        }


        private string _guidReferenciaProducto;

        public string GuidReferenciaProducto
        {
            get { return _guidReferenciaProducto; }
            set { _guidReferenciaProducto = value; }
        }


        private DateTime? _fechaFactura;

        public DateTime? FechaFactura
        {
            get { return _fechaFactura; }
            set { _fechaFactura = value; }
        }


        private int _cantidad;

        public int Cantidad
        {
            get { return _cantidad; }
            set { _cantidad = value; }
        }


        private decimal _costo;

        public decimal Costo
        {
            get { return _costo; }
            set { _costo = value; }
        }


        private DateTime? _fechaVencimiento;

        public DateTime? FechaVencimiento
        {
            get { return _fechaVencimiento; }
            set { _fechaVencimiento = value; }
        }


        private string _numeroFactura;

        public string NumeroFactura
        {
            get { return _numeroFactura; }
            set { _numeroFactura = value; }
        }


        private int _idProveedor;

        public int IdProveedor
        {
            get { return _idProveedor; }
            set { _idProveedor = value; }
        }
        private string _observaciones;

        public string Observaciones
        {
            get { return _observaciones; }
            set { _observaciones = value; }
        }

        private string _nombreProveedor;

        public string NombreProveedor
        {
            get { return _nombreProveedor; }
            set { _nombreProveedor = value; }
        }

        private string _nombreProducto;

        public string NombreProducto
        {
            get { return _nombreProducto; }
            set { _nombreProducto = value; }
        }

        private string _nombreBodega;

        public string NombreBodega
        {
            get { return _nombreBodega; }
            set { _nombreBodega = value; }
        }


        private decimal _total;

        public decimal Total
        {
            get { return _total; }
            set { _total = value; }
        }

        private int _idUser;

        public int IdUser
        {
            get { return _idUser; }
            set { _idUser = value; }
        }

        public void Save()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                using (SharedDbConnectionScope sharedConnectionScope = new SharedDbConnectionScope())
                {

                    try
                    {
                        DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection bodegasProductoCol = new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection()
                                     .Where("ID_BODEGA", this._idBodega);

                        DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos bodegaProducto = null;

                        List<DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos> bodegasProducto = bodegasProductoCol.Load().GetList();
                        foreach (DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos bodegasPro in bodegasProducto)
                        {
                            if (bodegasPro.GuidReferenciaProducto == this.GuidReferenciaProducto)
                                bodegaProducto = bodegasPro;
                        }

                        if (bodegaProducto != null)
                        {
                            //bodegaProducto = bodegasProducto[0];
                            bodegaProducto.Cantidad += this.Cantidad;
                            bodegaProducto.Save();
                        }
                        else
                        {
                            bodegaProducto = new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos();
                            bodegaProducto.IdBodega = this.IdBodega;
                            bodegaProducto.GuidReferenciaProducto = this.GuidReferenciaProducto;
                            bodegaProducto.Cantidad = this.Cantidad;
                            bodegaProducto.Save();
                        }

                        DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodega hitorico = new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodega();
                        hitorico.Entrada = true;
                        hitorico.Fecha = DateTime.Now;
                        hitorico.GuidDetalleFactura = null;
                        hitorico.IdUsuario = this.IdUser;
                        hitorico.IdBodegaProducto = bodegaProducto.IdBodegaProducto;
                        hitorico.Observaciones = this.Observaciones;
                        hitorico.CantidadTransaccion = this.Cantidad;
                        hitorico.FechaVencimiento = this.FechaVencimiento;

                        try
                        {
                            hitorico.NumeroFactura = long.Parse(this.NumeroFactura);
                        }
                        catch (Exception)
                        {
                            hitorico.NumeroFactura = null;
                        }
                        hitorico.GuidDetalleAjuste = null;
                        hitorico.GuidDetalleFacturaEmpresa = null;
                        hitorico.Save();

                        ts.Complete();
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        public void transferExit()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                using (SharedDbConnectionScope sharedConnectionScope = new SharedDbConnectionScope())
                {
                    try
                    {
                        DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection bodegasProductoCol = new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection()
                                     .Where("ID_BODEGA", this._idBodega);

                        DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos bodegaProducto = null;

                        List<DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos> bodegasProducto = bodegasProductoCol.Load().GetList();
                        foreach (DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos bodegasPro in bodegasProducto)
                        {
                            if (bodegasPro.GuidReferenciaProducto == this.GuidReferenciaProducto)
                                bodegaProducto = bodegasPro;
                        }

                        if (bodegaProducto != null)
                        {
                            //bodegaProducto = bodegasProducto[0];
                            bodegaProducto.Cantidad -= this.Cantidad;
                            bodegaProducto.Save();
                        }
                        else
                        {
                            bodegaProducto = new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos();
                            bodegaProducto.IdBodega = this.IdBodega;
                            bodegaProducto.GuidReferenciaProducto = this.GuidReferenciaProducto;
                            bodegaProducto.Cantidad = (this.Cantidad) * -1;
                            bodegaProducto.Save();
                        }

                        DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodega hitorico = new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodega();
                        hitorico.Entrada = false;
                        hitorico.Fecha = DateTime.Now;
                        hitorico.GuidDetalleFactura = null;
                        hitorico.IdUsuario = this.IdUser;
                        hitorico.IdBodegaProducto = bodegaProducto.IdBodegaProducto;
                        hitorico.Observaciones = "Salida por Transferencia de Saldos";
                        hitorico.CantidadTransaccion = this.Cantidad;
                        hitorico.FechaVencimiento = DateTime.Now;
                        hitorico.NumeroFactura = null;
                        hitorico.GuidDetalleAjuste = null;
                        hitorico.GuidDetalleFacturaEmpresa = null;
                        hitorico.Save();

                        ts.Complete();
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

        public void transferInto()
        {
            using (TransactionScope ts = new TransactionScope())
            {
                using (SharedDbConnectionScope sharedConnectionScope = new SharedDbConnectionScope())
                {
                    try
                    {
                        DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection bodegasProductoCol = new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductosCollection()
                                     .Where("ID_BODEGA", this._idBodega);

                        DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos bodegaProducto = null;

                        List<DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos> bodegasProducto = bodegasProductoCol.Load().GetList();
                        foreach (DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos bodegasPro in bodegasProducto)
                        {
                            if (bodegasPro.GuidReferenciaProducto == this.GuidReferenciaProducto)
                                bodegaProducto = bodegasPro;
                        }

                        if (bodegaProducto != null)
                        {
                            //bodegaProducto = bodegasProducto[0];
                            bodegaProducto.Cantidad += this.Cantidad;
                            bodegaProducto.Save();
                        }
                        else
                        {
                            bodegaProducto = new DMG.Multisponsor.Inventarios.DataAccess.Core.BodegasProductos();
                            bodegaProducto.IdBodega = this.IdBodega;
                            bodegaProducto.GuidReferenciaProducto = this.GuidReferenciaProducto;
                            bodegaProducto.Cantidad = (this.Cantidad);
                            bodegaProducto.Save();
                        }

                        DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodega hitorico = new DMG.Multisponsor.Inventarios.DataAccess.Core.HistoricoBodega();
                        hitorico.Entrada = true;
                        hitorico.Fecha = DateTime.Now;
                        hitorico.GuidDetalleFactura = null;
                        hitorico.IdUsuario = this.IdUser;
                        hitorico.IdBodegaProducto = bodegaProducto.IdBodegaProducto;
                        hitorico.Observaciones = "Entrada por Transferencia de Saldos";
                        hitorico.CantidadTransaccion = this.Cantidad;
                        hitorico.FechaVencimiento = DateTime.Now;
                        hitorico.NumeroFactura = null;
                        hitorico.GuidDetalleAjuste = null;
                        hitorico.GuidDetalleFacturaEmpresa = null;
                        hitorico.Save();

                        ts.Complete();
                    }
                    catch (Exception)
                    {

                    }
                }
            }
        }

    }
}
