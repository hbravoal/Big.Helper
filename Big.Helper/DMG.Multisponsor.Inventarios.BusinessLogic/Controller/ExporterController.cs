using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Text;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class ExporterController
    {
        /// <summary>
        /// Exporta el encabezado del archivo generado para la aplicaci�n cguno
        /// </summary>
        /// <param name="cgunoCompanyCode">C�digo de la empresa en CGUNO.</param>
        /// <param name="cgunoUtilityCode">C�digo del centro de utilidad en CGUNO.</param>
        /// <param name="accountingVoucherCode">C�digo de Comprobante Contable.</param>
        /// <param name="batch">N�mero del Lote.</param>
        /// <param name="batchDate">Fecha con la cual se cargar� el Movimiento en CGUNO</param>
        /// <param name="userName">Nombre de usuario de quien realiza la importaci�n.</param>
        /// <param name="fileExtension">Extensi�n con la cual se va a exportar el archivo.</param>
        public static void ExportHeader(string cgunoCompanyCode, string cgunoUtilityCode,
            string accountingVoucherCode, string batch, string batchDate, string userName, string fileExtension)
        {
            //DataTable cgUnoHeaderData = null;
            //HttpResponse response = null;
            //string fileName = string.Empty;
            //StringBuilder fileNameBuilder = null;
            //StringBuilder dataBuilder = null;
            //int characterLimit = 0;
            //int i = 0;

            //if (cgUnoHeaderData != null)
            //{
            //    fileNameBuilder = new StringBuilder();
            //    fileNameBuilder.Append("CGBATCH1");
            //    fileNameBuilder.Append(fileExtension);

            //    response = HttpContext.Current.Response;
            //    response.ContentType = "application/text";
            //    response.ContentEncoding = Encoding.Unicode;
            //    response.AddHeader("Content-Disposition", "attachment; filename=" + fileNameBuilder.ToString());
            //    response.Write("LAPSO");
            //    response.Write("EMPRESA");
            //    response.Write("CU");
            //    response.Write("CBTE");
            //    response.Write("LOTE");
            //    response.Write("REGISTRO");
            //    response.Write("FECHALOTE");
            //    response.Write("GRABADOPOR");
            //    response.Write("DEBITOS");
            //    response.Write("CREDITOS");
            //    response.Write("OBS1");
            //    response.Write("OBS1");
            //    response.Write("ESTADO");
            //    response.Write("COMPLEMENTO");
            //    response.Write("\r\n");



            //    response.Write("LAPSO");
            //    response.Write(cgunoCompanyCode);
            //    response.Write(cgunoUtilityCode);
            //    response.Write(accountingVoucherCode);
            //    response.Write(batch);

            //    string anio = cgUnoHeaderData.Rows[0]["ANIO"].ToString();
            //    anio = anio.Substring(2);
            //    string mes = cgUnoHeaderData.Rows[0]["MES"].ToString();


            //    dataBuilder = new StringBuilder();
            //    dataBuilder.Append(anio);
            //    if (mes.Length < 2) {                    
            //        dataBuilder.Append('0');                    
            //    }
            //    dataBuilder.Append(mes);

            //    response.Write(dataBuilder.Append);

            //    //se arma el campo de total de registros
            //    characterLimit = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudRegistros"));
            //    dataBuilder = new StringBuilder();
            //    string registros = cgUnoHeaderData.Rows[0]["REGISTRO"].ToString();

            //    for (i = 0; registros.Length + i < characterLimit; i++)
            //    {
            //        dataBuilder.Append("0");
            //    }
            //    dataBuilder.Append(registros);
            //    response.Write(dataBuilder.ToString());


            //    //se arma la fecha del lote
            //    response.Write(batchDate.Replace("/", string.Empty));


            //    // Se arma el campo de nombre de usuario
            //    characterLimit = int.Parse(ConfigurationManager.AppSettings["longitudUsuario"]);
            //    dataBuilder = new StringBuilder();

            //    for (i = 0; userName.Length + i < characterLimit; i++)
            //    {
            //        dataBuilder.Append(" ");
            //    }
            //    dataBuilder.Append(userName);

            //    response.Write(dataBuilder.ToString());


            //    //se arma el campo de debitos
            //    characterLimit = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudDebitos"));
            //    dataBuilder = new StringBuilder();
            //    string debitos = cgUnoHeaderData.Rows[0]["TOTAL_DEBITOS"].ToString();
            //    Int64 debits = Convert.ToInt64(cgUnoHeaderData.Rows[0]["TOTAL_DEBITOS"].ToString());
            //    for (i = 0; debitos.Length + i < characterLimit; i++)
            //    {
            //        dataBuilder.Append("0");
            //    }
            //    dataBuilder.Append(debitos);
            //    if (debits >= 0)
            //        dataBuilder.Append("+");
            //    else
            //        dataBuilder.Append("-");
            //    response.Write(dataBuilder.ToString());


            //    //se arma el campo de creditos
            //    characterLimit = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudCreditos"));
            //    dataBuilder = new StringBuilder();
            //    string creditos = cgUnoHeaderData.Rows[0]["TOTAL_CREDITOS"].ToString();
            //    Int64 credits = Convert.ToInt64(cgUnoHeaderData.Rows[0]["TOTAL_CREDITOS"].ToString());
            //    for (i = 0; creditos.Length + i < characterLimit; i++)
            //    {
            //        dataBuilder.Append("0");
            //    }
            //    dataBuilder.Append(creditos);
            //    if (credits >= 0)
            //        dataBuilder.Append("+");
            //    else
            //        dataBuilder.Append("-");
            //    response.Write(dataBuilder.ToString());


            //    //se arman los comentarios
            //    characterLimit = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudComentarios"));
            //    dataBuilder = new StringBuilder();
            //    for (i = 0; i < characterLimit; i++)
            //    {
            //        dataBuilder.Append(" ");
            //    }

            //    response.Write(dataBuilder.ToString());
            //    response.Write(dataBuilder.ToString());

            //    response.Write(ConfigurationManager.AppSettings.Get("campoEstado"));


            //    //se arma el campo complemento
            //    characterLimit = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudComplemento"));
            //    dataBuilder = new StringBuilder();
            //    for (i = 0; i < characterLimit; i++)
            //    {
            //        dataBuilder.Append(" ");
            //    }

            //    response.Write(dataBuilder.ToString());
            //    response.End();


            //}
        }

        public static void ExportBody(string fileExtension, string cgUnoUtilityCenterCode, string cgUnoCompanyCode,
            string accountablityVoucher, string batch) {
            //DataTable cgUnoBodyData = null;
            //HttpResponse response = null;
            //string fileName = string.Empty;
            //StringBuilder fileNameBuilder = null;
            //StringBuilder dataBuilder = null;
            //int characterLimit = 0;
            //int i = 0;
            //int longitudCuenta;
            //int longitudBeneficia;
            //int longitudCU;
            //int longitudCU2;
            //int longitudTipoDocumento;
            //int longitudNumeroDoc;
            //int longitudFecha;
            //int longitudTipoT;
            //int longitudBase;
            //int longitudTipoCruce;
            //int longitudNumCruce;
            //int longitudCueCruce;
            //int longitudVencimiento;
            //int longitudPorcentaje;
            //int longitudNit;
            //int longitudNombre;
            //int longitudClaseBenef;
            //int longitudCupo;
            //int longitudDescuento;
            //int longitudResolucion;
            //int longitudPlazo;
            //int longitudValorT;
            //int longitudDetalle;
            //int longitudDestino;
            //int longitudDocumentoConciliacion;
            //int longitudNumeroConciliacion;
            //int longitudCantidad;

            //int auxiliaryValue;

            //string cuenta = string.Empty;
            //string beneficiario = string.Empty;
            //string anio = string.Empty;
            //string mes = string.Empty;
            //string numeroDocumento = string.Empty;
            //string fecha = string.Empty;
            //string valorT = string.Empty;
            //string valorBase = string.Empty;
            //string tipoCruce = string.Empty;
            //string numeroCruce = string.Empty;
            //string cuentaCruce = string.Empty;

            //if (cgUnoBodyData != null) {
            //    fileNameBuilder = new StringBuilder();
            //    fileNameBuilder.Append("CGBATCH2");
            //    fileNameBuilder.Append(fileExtension);

            //    response = HttpContext.Current.Response;
            //    response.ContentType = "application/text";
            //    response.ContentEncoding = Encoding.Unicode;
            //    response.AddHeader("Content-Disposition", "attachment; filename=" + fileNameBuilder.ToString());
                
            //    foreach (DataRow currentRow in cgUnoBodyData.Rows) {
                    
            //        // El C�digo Puc de la cuenta
            //        dataBuilder = new StringBuilder();                    
            //        cuenta = currentRow["CUENTA"].ToString(); 
            //        longitudCuenta = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudCuenta"));
            //        dataBuilder.Append(cuenta);
            //        for (i = 0; cuenta.Length + i < longitudCuenta; i++) {
            //            dataBuilder.Append(" ");
            //        }
            //        response.Write(dataBuilder.ToString());

            //        // El beneficiario
            //        dataBuilder = new StringBuilder();
            //        beneficiario = currentRow["BENEFICIA"].ToString();
            //        longitudBeneficia = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudBeneficia"));
            //        dataBuilder.Append(beneficiario);
            //        for (i = 0; cuenta.Length + i < longitudBeneficia; i++)
            //        {
            //            dataBuilder.Append(" ");
            //        }
            //        response.Write(dataBuilder.ToString());

            //        //El CU

            //        response.Write(currentRow["CU"].ToString());

            //        //El Lapso
            //        anio = cgUnoBodyData.Rows[0]["ANIO"].ToString();
            //        anio = anio.Substring(2);
            //        mes = cgUnoBodyData.Rows[0]["MES"].ToString();
            //        dataBuilder = new StringBuilder();
            //        dataBuilder.Append(anio);
            //        if (mes.Length < 2)
            //        {
            //            dataBuilder.Append('0');
            //        }
            //        dataBuilder.Append(mes);

            //        //Empresa           
                    
            //        response.Write(currentRow["EMPRESA"].ToString());

            //        // El CU2

            //        response.Write(currentRow["CU2"].ToString());

            //        // C�digo de Comprobante Contable

            //        response.Write(accountablityVoucher);

            //        // El N�mero de Lote

            //        response.Write(batch);

            //        // Registro ????

                    
            //        // El tipo de Documento

            //        response.Write(currentRow["TIP_DOC"].ToString());

            //        // El n�mero del documento

            //        dataBuilder = new StringBuilder();
            //        longitudNumeroDoc = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudNumDocumento"));
            //        cuenta = currentRow["NUMDOC"].ToString();
            //        for (i = 0; cuenta.Length + i < longitudBeneficia; i++)
            //        {
            //            dataBuilder.Append("0");
            //        }
            //        dataBuilder.Append(cuenta);
            //        response.Write(dataBuilder);

            //        // La fecha

            //        dataBuilder = new StringBuilder();
            //        fecha = currentRow["FECHA"].ToString();
            //        fecha = fecha.Replace("/", string.Empty);
            //        dataBuilder.Append(fecha.Substring(0, 4));
            //        dataBuilder.Append(fecha.Substring(fecha.Length - 2));
            //        response.Write(dataBuilder.ToString());

            //        //El tipo

            //        response.Write(currentRow["TIPOT"].ToString());

            //        //El Valor T

            //        dataBuilder = new StringBuilder();
            //        valorT = currentRow["VALORT"].ToString();
            //        auxiliaryValue = Convert.ToInt32(valorT);
            //        longitudValorT = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudValorT"));
            //        for (i = 0; valorT.Length + i < longitudValorT; i++) {
            //            dataBuilder.Append("0");
            //        }
            //        dataBuilder.Append(valorT);
            //        if (valorT >= 0)
            //            dataBuilder.Append("+");
            //        else
            //            dataBuilder.Append("-");
            //        response.Write(dataBuilder.ToString());

            //        // Detalles

            //        dataBuilder = new StringBuilder();
            //        longitudDetalle = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudDetalle"));
            //        for (i = 0; i < longitudDetalle; i++) {
            //            dataBuilder.Append(" ");
            //        }

            //        response.Write(dataBuilder.ToString());
            //        response.Write(dataBuilder.ToString());

            //        // Destino (una de las cosas que toca preguntar)

            //        dataBuilder = new StringBuilder();
            //        longitudDestino = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudDestino"));
            //        for (i = 0; i < longitudDestino; i++)
            //        {
            //            dataBuilder.Append(" ");
            //        }

            //        response.Write(dataBuilder.ToString());

            //        // Tipo Documento de conciliaci�n

            //        dataBuilder = new StringBuilder();
            //        longitudDocumentoConciliacion = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudDocumentoConciliacion"));

            //        for (i = 0; i < longitudDocumentoConciliacion; i++) {
            //            dataBuilder.Append(" ");
            //        }

            //        response.Write(dataBuilder.ToString());

            //        // Numero conciliaci�n

            //        dataBuilder = new StringBuilder();
            //        longitudNumeroConciliacion = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudNumeroConciliacion"));

            //        for (i = 0; i < longitudNumeroConciliacion; i++)
            //        {
            //            dataBuilder.Append(" ");
            //        }

            //        response.Write(dataBuilder.ToString());

            //        // Base (preguntar)

            //        dataBuilder = new StringBuilder();

            //        longitudBase = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudBase"));
            //        valorBase = currentRow["BASE"].ToString();
            //        auxiliaryValue = Convert.ToInt32(valorBase);

            //        for (i = 0; i + valorBase.Length < longitudBase; i++) {
            //            dataBuilder.Append("0");
            //        }
            //        dataBuilder.Append(valorBase);
            //        if (auxiliaryValue >= 0)
            //            dataBuilder.Append("+");
            //        else
            //            dataBuilder.Append("-");

            //        response.Write(dataBuilder.ToString());

            //        // Cantidad

            //        dataBuilder = new StringBuilder();
            //        longitudCantidad = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudCantidad"));
            //        for (i = 0; i < longitudCantidad; i++) {
            //            dataBuilder.Append("0");
            //        }

            //        response.Write(dataBuilder.ToString());

            //        // Tipo Cruce

            //        dataBuilder = new StringBuilder();
            //        longitudTipoCruce = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudTipoCruce"));
            //        tipoCruce = currentRow["TIPCRUCE"].ToString();
            //        for (i = 0; i + tipoCruce.Length < longitudTipoCruce; i++) {
            //            dataBuilder.Append(" ");
            //        }
            //        dataBuilder.Append(tipoCruce);

            //        response.Write(tipoCruce);

            //        // N�mero Cruce

            //        dataBuilder = new StringBuilder();
            //        longitudNumCruce = Convert(ConfigurationManager.AppSettings.Get("longitudNumCruce"));
            //        numeroCruce = currentRow["NUMCRUCE"].ToString();
            //        for (i = 0; i + numeroCruce.Length < longitudNumCruce; i++) {
            //            dataBuilder.Append("0");
            //        }
            //        dataBuilder.Append(numeroCruce);

            //        response.Write(dataBuilder.ToString());

            //        // Cuenta de Cruce

            //        dataBuilder = new StringBuilder();
            //        longitudCueCruce = Convert.ToInt32(ConfigurationManager.AppSettings.Get("longitudCueCruce"));


            //    }

            //}

        }

        /// <summary>
        /// M�todo que por medio del objeto Response del HttpContext produce un archivo
        /// separado por comas, usando como insumo los datos de un DataTable
        /// </summary>
        /// <param name="data">Objeto DataTable a procesar</param>
        /// <param name="context">Objeto Response usado para generar el archivo separado
        /// por comas</param>
        public static void GenerateFile(DataTable data, HttpResponse response, string filename)
        {
            //string filename = "InventariosSaldos{0:yyyyMMdd}.xls";
            //filename = string.Format(filename, DateTime.Today);
            response.ContentType = "application/text";
            response.ContentEncoding = Encoding.Unicode;
            response.AddHeader("Content-Disposition", "filename=" + filename);

            int cols = data.Columns.Count;

            for (int i = 0; i < cols; i++)
            {
                response.Write(data.Columns[i].ColumnName.ToUpper());
                if (i < cols - 1)
                {
                    response.Write("\t");
                }
            }

            response.Write("\r\n");

            foreach (DataRow row in data.Rows)
            {
                for (int i = 0; i < cols; i++)
                {
                    response.Write(row[i].ToString().Replace("\n", "").Replace("\r", ""));
                    if (i < cols - 1)
                    {
                        response.Write("\t");
                    }
                }
                //for (int j = 0; j < cols; j++)
                //{
                //    response.Write(row[j]);
                //    if (j < cols - 1)
                //    {
                //        response.Write("\t");
                //    }
                //}
                response.Write("\r\n");
            }

            response.End();
        }
    }
}
