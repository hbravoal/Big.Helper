using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
   public class TypesProductsController
    {
       public static void AgregarTiposProductos(string codigo, string nombreTipoProducto, string descripcion, int estado)
       {
           try
           {
               MasTiposProducto tiposProducto = new MasTiposProducto();
               tiposProducto.Codigo = codigo;
               tiposProducto.Nombre = nombreTipoProducto;
               tiposProducto.Descripcion = descripcion;
               tiposProducto.IdEstado = estado;
               tiposProducto.Save();
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo AgregarTiposProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TypesProductsController\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
           }
       }

       public static bool VerificarCodigoTiposProducto(string codigoTipoProducto)
       {
           MasTiposProducto tipo = new MasTiposProducto(MasTiposProducto.Columns.Codigo, codigoTipoProducto);
           if (!tipo.IsNew)
           {
               return false;
           }
           else
           {
               return true;
           }

       }

       public static void ActualizarTiposProductos(int idCodigoTipoProducto ,string codigo, string nombreTipoProducto, string descripcion, int estado)
       {
           try
           {
               MasTiposProducto tiposProducto = new MasTiposProducto(idCodigoTipoProducto);
               if (tiposProducto.IsLoaded)
               {
                   tiposProducto.Codigo = codigo;
                   tiposProducto.Nombre = nombreTipoProducto;
                   tiposProducto.Descripcion = descripcion;
                   tiposProducto.IdEstado = estado;
                   tiposProducto.Save();
               }
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo ActualizarTiposProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TypesProductsController\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
           }

       }

       public static DataTable CargarTiposProducto(int idFantasma)
       {
           try
           {
               DataTable dt = SPs.SelectTiposProductos(idFantasma).GetDataSet().Tables[0];
               return dt;
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo CargarTiposProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TypesProductsController\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
               return null;
           }           
       }

       public static DataTable CargarTiposProductoActivo(int idFantasma)
       {
           try
           {
               DataTable dt = SPs.SelectTiposProductosa(idFantasma).GetDataSet().Tables[0];
               return dt;
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo CargarTiposProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TypesProductsController\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
               return null;
           }
       }


       public static MasTiposProducto GetTiposProductoByIdTipoProducto(int idtipoproducto)
       {
           try
           {
               MasTiposProducto mastipos = new MasTiposProducto(idtipoproducto);
               return mastipos;
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo CargarTiposProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TypesProductsController\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
               return null;
           }
       }

       public static DataTable CargarEstados()
       {
           try
           {
               DataTable dt = SPs.SelectEstadosTiposProductos(0).GetDataSet().Tables[0];
               return dt;
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo CargarEstados() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TypesProductsController\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
               return null;
           }
       }
    }
}
