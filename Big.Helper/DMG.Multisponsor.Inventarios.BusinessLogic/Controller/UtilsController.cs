using System;
using System.Collections.Generic;
using System.Text;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class UtilsController
    {
        public static string GetCiudadNameByID(int idCiudad)
        {
            try
            {
                StringBuilder nombreCiudad = new StringBuilder();
                MasCiudades ciudad = new MasCiudades(idCiudad);
                if (ciudad.IsLoaded)
                {
                    nombreCiudad.Append(ciudad.Nombre).Append(" / ").Append(ciudad.MasDepartamentos.Nombre).Append(" / ").Append(ciudad.MasDepartamentos.MasPaises.Nombre);
                    return nombreCiudad.ToString();
                }
                else
                    throw new Exception("Una ciudad que no existe accedio a este metodo");
            }
            catch
            {
                return null;
            }
        }

        public static string GetProveedorNameByID(int idProveedor)
        {
            try
            {
                string nombreProveedor;
                Proveedores proveedor = new Proveedores(idProveedor);
                if (proveedor.IsLoaded)
                {
                 nombreProveedor = proveedor.RazonSocial;
                 return nombreProveedor;
                }
                else
                    throw new Exception("El proveedor no existe");
            }
            catch
            {
                return null;
            }
        }

        public static string GetMarcaNameByID(int idMarca)
        {
            try
            {
                string nombreMarca;
                Marcas marca = new Marcas(idMarca);
                if (marca.IsLoaded)
                {
                    nombreMarca = marca.NombreMarca;
                    return nombreMarca;
                }
                else
                    throw new Exception("La marca no existe");
            }
            catch
            {
                return null;
            }
        }

        public static string GetTipoProductoNameByID(int idTipoProducto)
        {
            try
            {
                string nombreTipoProducto;
                MasTiposProducto tipoProducto = new MasTiposProducto(idTipoProducto);
                if (tipoProducto.IsLoaded)
                {
                    nombreTipoProducto = tipoProducto.Nombre;
                    return nombreTipoProducto;
                }
                else
                    throw new Exception("El tipo de producto no existe");
            }
            catch
            {
                return null;
            }
        }
    }
}
