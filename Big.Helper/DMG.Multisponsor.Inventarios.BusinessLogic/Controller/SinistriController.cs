using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
   public class SinistriController
   {

      public static bool SetSiniestros(int IdPrograma, string GuidReferenciaProducto, int Cantidad, string Justificacion)
      {
         try
         {
            
            BodegasProductosCollection colBodegasProductos = new BodegasProductosCollection();
            colBodegasProductos.Where(BodegasProductos.Columns.GuidReferenciaProducto, GuidReferenciaProducto);
            BodegasProductos BodegaProducto = new BodegasProductos();
            foreach (BodegasProductos bp in colBodegasProductos.Load())
            {
               if(bp.Bodegas.IdTipoBodega==Int16.Parse(ConfigurationManager.AppSettings["BodegaFisica"]))
                  BodegaProducto = bp;
            }

            if (BodegaProducto.Cantidad != null && BodegaProducto.Cantidad >= Cantidad)
            {

               string GuidSiniestro = string.Empty;
               SiniestrosCollection col = new SiniestrosCollection();
               int cantidadItems = col.Where(Siniestros.Columns.IdPrograma, IdPrograma).Where(Siniestros.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load().Count;
               if (cantidadItems == 1)
               {
                  foreach (Siniestros siniestro in col)
                  {
                     siniestro.Cantidad += Cantidad;
                     GuidSiniestro = siniestro.Guid;
                     siniestro.Save();
                  }
               }
               else
               {
                  Siniestros siniestro = new Siniestros();
                  siniestro.Guid = System.Guid.NewGuid().ToString();
                  GuidSiniestro = siniestro.Guid;
                  siniestro.IdPrograma = IdPrograma;
                  siniestro.GuidReferenciaProducto = GuidReferenciaProducto;
                  siniestro.Cantidad = Cantidad;
                  siniestro.Save();
               }

               HistoricoSiniestros hist = new HistoricoSiniestros();
               hist.Cantidad = Cantidad;
               hist.GuidHistoricoSiniestros = System.Guid.NewGuid().ToString();
               hist.GuidSiniestro = GuidSiniestro;
               hist.Justificacion = Justificacion;
               hist.Fecha = DateTime.Now;
               hist.Save();

               //decimal resul = InventariosController.SetProductosBodegas(BodegaProducto.IdBodega, GuidReferenciaProducto, Cantidad, DateTime.Now, null, null, null, Justificacion, null, false, true, -1);
               return true;
            }
            else
               return false;
         }
         catch (Exception ex)
         {
            //Construimos la excepcion a ser almacenada en un log
            StringBuilder exception = new StringBuilder();
            exception.Append("Error en el metodo SetSiniestros() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.SinistriController:\n");
            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            return false;
         }
      }

      public static DataSet GetBodegasProductoByProgramaReferencia(int IdPrograma, string GuidReferencia)
      {
         try
         {
            return SPs.SelectReferenciasProgramaReferencia(IdPrograma, GuidReferencia).GetDataSet();
         }
         catch (Exception ex)
         {
            //Construimos la excepcion a ser almacenada en un log
            StringBuilder exception = new StringBuilder();
            exception.Append("Error en el metodo GetBodegasProductoByProgramaReferencia() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.SinistriController:\n");
            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            return null;
         }
      }

       public static DataSet GetSiniestros(int? IdPrograma)
       {
           try
           {
               return SPs.SelectSiniestros(IdPrograma).GetDataSet();
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo GetBodegasProductoByProgramaReferencia() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.SinistriController:\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
               return null;
           }
       }
   }
}
