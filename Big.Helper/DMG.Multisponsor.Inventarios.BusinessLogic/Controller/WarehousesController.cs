using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class WarehousesController
  {
    public static DataSet CargarProgramasTodos()
    {
      DataSet ds = SPs.SelectProgramas(0).GetDataSet();
      return ds;
    }

    public static DataSet CargarTiposBodega(int idFantasma)
    {
      DataSet ds = SPs.SelectTiposBodega(idFantasma).GetDataSet();
      return ds;
    }

    public static Bodegas CargarBodegasporId(int idBodega)
    {
      Bodegas Bodega = new Bodegas(Bodegas.Columns.IdBodega, idBodega);
      if (Bodega.IsLoaded)
      {
        return Bodega;
      }
      else
      {
        return null;
      }
    }

    public static Bodegas CargarBodegasPorTipoPrograma(int idPrograma,int idTipo)
    {
      BodegasCollection colBodegas = new BodegasCollection();
      colBodegas.Where(Bodegas.Columns.IdPrograma, idPrograma);
      colBodegas.Where(Bodegas.Columns.IdTipoBodega, idTipo);
      colBodegas.Load();

      Bodegas bodega = new Bodegas();

      try
      {
        bodega = colBodegas[0];
        return bodega;
      }
      catch
      {
        return null;
      }      
    }

      public static DataTable CargarBodegasPorPrograma(int idPrograma)
      {
          BodegasCollection colBodegas = new BodegasCollection();
          colBodegas.Where(Bodegas.Columns.IdPrograma, idPrograma);
          return colBodegas.Load().ToDataTable();
      }

    public static void AgregarBodegas(string nombreBodega, int idPrograma, int idTipoBodega)
    {
      Bodegas bodega = new Bodegas();

      bodega.NombreBodega = nombreBodega;
      bodega.IdPrograma = idPrograma;
      bodega.IdTipoBodega = idTipoBodega;
      bodega.Save();

    }

    public static void ActualizarBodegas(int idBodega, string nombreBodega, int idPrograma, int idTipoBodega)
    {
      Bodegas bodega = new Bodegas(idBodega);
      bodega.NombreBodega = nombreBodega;
      bodega.IdPrograma = idPrograma;
      bodega.IdTipoBodega = idTipoBodega;
      bodega.Save();

    }

    public static DataSet CargarBodegas(int parametroFantasma)
    {
      DataSet ds = SPs.SelectBodegas(parametroFantasma).GetDataSet();
      return ds;
    }

    public static DataSet CargarBodegasProgramaTipo(int idPrograma, int idTipoBodega)
    {
        DataSet ds = SPs.SelectBodegas(idPrograma, idTipoBodega).GetDataSet();
        return ds;
    }


      public static DataSet ObtenerReferenciasProductosByBodega(int idBodega)
      {
          DataSet ds = SPs.SelectReferenciasProductosByBodega(idBodega).GetDataSet();
          return ds;
      }

    public static bool VerificarNombreBodega(string nombreBodega)
    {
      Bodegas bodegas = new Bodegas(Bodegas.Columns.NombreBodega, nombreBodega);
      if (!bodegas.IsNew)
      {
        return false;
      }
      else
      {
        return true;
      }

    }

    public static bool VerificarDisponiblesPrograma(int idPrograma, int idBodegaDisp)
    {
      BodegasCollection bc = new BodegasCollection();
      bc.Where(Bodegas.Columns.IdTipoBodega, idBodegaDisp);
      bc.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
      if (bc.Count > 0)
        return false;
      else
        return true;
    }

    public static bool VerificarBodegasDisponiblesByPrograma(int idPrograma, int idTipoBodega)
    {
      BodegasCollection bc = new BodegasCollection();
      bc.Where(Bodegas.Columns.IdPrograma, idPrograma);
      bc.Where(Bodegas.Columns.IdTipoBodega, idTipoBodega);
      bc.Load();
      if (bc.Count > 0)
        return false;
      else
        return true;
    }

    public static bool VerificarSaldosPrograma(int idPrograma, int idBodegaSaldos)
    {
      BodegasCollection bc = new BodegasCollection();
      bc.Where(Bodegas.Columns.IdTipoBodega, idBodegaSaldos);
      bc.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
      if (bc.Count > 0)
        return false;
      else
        return true;
    }

  }
}
