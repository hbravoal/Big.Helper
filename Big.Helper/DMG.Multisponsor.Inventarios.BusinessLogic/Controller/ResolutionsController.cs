using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using System.Data;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class ResolutionsController
    {
        public static void AgregarResolucines(string resolucion, DateTime fechaAprobacion, string codigoCentroCosto
                                             , int valorInicial, int valorFinal, int tipoResolucion, string numResolucion)
        {
            Resoluciones resoluciones = new Resoluciones();

            resoluciones.Resolucion = resolucion;
            resoluciones.FechaResolucion = fechaAprobacion;
            resoluciones.CodigoCentroCosto = codigoCentroCosto;
            resoluciones.ValorInicial = valorInicial;
            resoluciones.ValorActual = valorInicial;
            resoluciones.ValorFinal = valorFinal;
            resoluciones.IdTipoResolucion = tipoResolucion;
            resoluciones.NumeroResolucion = numResolucion;
            resoluciones.Save();

        }

        public static void ActualizarResolucines(int idResolucion, string resolucion, DateTime fechaAprobacion, string codigoCentroCosto
                                            , int valorInicial, int valorFinal, int tipoResolucion, string numResolucion)
        {
            Resoluciones resoluciones = new Resoluciones(idResolucion);

            resoluciones.Resolucion = resolucion;
            resoluciones.FechaResolucion = fechaAprobacion;
            resoluciones.CodigoCentroCosto = codigoCentroCosto;
            resoluciones.ValorInicial = valorInicial;
            //resoluciones.ValorActual = valorInicial;
            resoluciones.ValorFinal = valorFinal;
            resoluciones.IdTipoResolucion = tipoResolucion;
            resoluciones.NumeroResolucion = numResolucion;
            resoluciones.Save();

        }
        //public static DataSet CargarResoluciones(int parametroFantasma)
        //{
        //    DataSet ds = SPs.SelectResoluciones(parametroFantasma).GetDataSet();
        //    return ds;
        //}

        public static Resoluciones CargarResolucionPorId(int idResolucion)
        {
            Resoluciones resolucion = new Resoluciones(Resoluciones.Columns.IdResolucion, idResolucion);
            if (resolucion.IsLoaded)
            {
                return resolucion;
            }
            else
            {
                return null;
            }

        }

        /* public static DataSet GetResolutionsEntries(int entryState, int pageIndex, int pageSize, out int totalRecords)
         {
             totalRecords = 0;
             SubSonic.StoredProcedure sp = SPs.SelectResoluciones(entryState, totalRecords);
             sp.PageIndex = pageIndex;
             sp.PageSize = pageSize;
             sp.PagingResult = true;
             DataSet ds = sp.GetDataSet();
             int.TryParse(sp.OutputValues[0].ToString(), out totalRecords);
             return ds;
         }*/

        public static bool GetResolutionsByResoluction(string Resolucion)
        {
            try
            {
                Resoluciones get = new Resoluciones(Resoluciones.Columns.NumeroResolucion, Resolucion);
                if (get.IsNew)
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetResolutionsByResoluction() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static DataSet GetResolutionsByCriterias(int? tipoResolucion, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.SelectResoluciones(tipoResolucion, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static MasTiposResolucionCollection GetTiposDeResolucion()
        {
            try
            {
                MasTiposResolucionCollection tiposResolucion = new MasTiposResolucionCollection();
                return tiposResolucion.OrderByAsc(MasTiposResolucion.Columns.IdTipoResolucion).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetTiposDeResolucion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static MasTiposResolucion GetTiposDeResolucionByID(int idResolucion)
        {
            try
            {
                MasTiposResolucion tiposResolucion = new MasTiposResolucion(idResolucion);
                if (tiposResolucion.IsNew)
                    throw new Exception("Una resolucion que no existe no debe ser accedida en este metodo");
                else
                    return tiposResolucion;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetTiposDeResolucionByID() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable GetResolutionsByProgram(int idTipoResolucion, int idPrograma)
        {
            try
            {
                DataTable datos = SPs.SelectResolucionTipoPrograma(idTipoResolucion,idPrograma).GetDataSet().Tables[0];
                return datos;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetResolutionsByProgram() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetProgramaByIdPrograma(string NombrePrograma)
        {
            try
            {
                object datos = SPs.SelectIdProgramaByPrograma(NombrePrograma).ExecuteScalar();
                if (datos != null)
                    return datos.ToString();
                else
                    return null;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetResolutionsByProgram() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static Resoluciones GetResolutionsAjuste(int idTipoResolucion)
        {
            try
            {
                Resoluciones resolucion = new Resoluciones(Resoluciones.Columns.IdTipoResolucion,idTipoResolucion);

                if (!resolucion.IsNew)
                    return resolucion;
                else
                    return null;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetResolutionsAjuste(int idTipoResolucion) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ResolutionsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }


    }


}
