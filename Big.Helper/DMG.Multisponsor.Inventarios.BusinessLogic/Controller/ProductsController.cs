using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DMG.Multisponsor.Inventarios.DataAccess.Core;
using SubSonic;
namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class ProductsController
    {
        /// <summary>
        /// Obtiene los productos de un determinado programa
        /// </summary>
        /// <param name="IdPrograma">Id del programa</param>
        /// <returns>Un objeto tipo DataTable</returns>
        public static DataTable GetProductos(int IdPrograma)
        {
            try
            {
                return SPs.GetProductosFromPrograma(IdPrograma).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetReferenciaByProducto(string guidProducto)
        {
            try
            {
                string guid = "";
                object guidReferencia = SPs.GetReferenciasPorProducto(guidProducto, null).ExecuteScalar();
                if (guidReferencia != null)
                    guid = guidReferencia.ToString();

                return guid;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetReferenciaByProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }


        public static DataTable GetProductosSinCantidad(int IdPrograma)
        {
            try
            {
                return SPs.GetProductosFromProgramaCantidad(IdPrograma).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable GetProductosByProveedorAndEstablecimiento(int establecimiento, int proveedor, int programa)
        {
            try
            {
                DataTable dt = SPs.ObtenerProductosByEstablecimientoAndProveedor(establecimiento, proveedor, programa).GetDataSet().Tables[0];
                return dt;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static DataTable GetProductosByProveedorMarca(int IdProveedor, int IdMarca)
        {
            try
            {
                ProductosCollection col = new ProductosCollection();
                col.Where(Productos.Columns.IdProveedor, IdProveedor)./*Where(Productos.Columns.IdMarca, IdMarca).*/Load();
                return col.ToDataTable();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static decimal[] GetIvaAndPrecioBaseProducto(string guidProducto)
        {
            try
            {
                Productos prods = new Productos(guidProducto);
                if (prods.IsLoaded)
                {
                    decimal[] arreglo = new decimal[3];
                    arreglo[0] = prods.Iva;
                    PreciosBasePrograma pbp = new PreciosBasePrograma(PreciosBasePrograma.Columns.GuidProducto, guidProducto);
                    if (pbp.IsNew)
                        throw new Exception("El producto no tiene precio de venta y apareci� como posiblidad en este metodo");
                    arreglo[1] = pbp.PrecioVentaBase;
                    arreglo[2] = pbp.CostoEnvioBase;
                    return arreglo;
                }
                else
                    throw new Exception("Un producto que no existe esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetIvaProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable CargarTiposEmpaque()
        {
            DataTable dt = SPs.SelectTiposEmpaques(0).GetDataSet().Tables[0];
            return dt;
        }

        public static DataTable CargarProveedores()
        {
            DataTable dt = SPs.SelectProveedores(0).GetDataSet().Tables[0];
            return dt;
        }

        public static DataTable CargarMarcas()
        {
            DataTable dt = SPs.SelectMarcas(0).GetDataSet().Tables[0];
            return dt;
        }

        public static DataTable CargarTiposProductos()
        {
            DataTable dt = SPs.SelectTiposProductos(0).GetDataSet().Tables[0];
            return dt;
        }

        public static void AgregarProductos(string codigo, string nombre, string descripcion
                                            , string localizacion, string codigoBarras, int tipoEmpaque
                                            , string unidadesXempaque, decimal costoBase, List<PreciosProgramas> listaPreciosPrograma, decimal largoProd, decimal anchoProd, decimal altoProd, decimal pesoProd
                                            , int idProveedor, /*int idMarca,*/ int idTipoProducto, decimal ivaProd
                                            , decimal reteFuente, decimal? descuento)
        {
            Productos prods = new Productos();
            prods.Guid = System.Guid.NewGuid().ToString();
            prods.Codigo = codigo;
            prods.Nombre = nombre;
            prods.Descripcion = descripcion;
            prods.Localizacion = localizacion;
            prods.CodigoBarras = codigoBarras;
            prods.IdTipoEmpaque = tipoEmpaque;
            prods.Unidadesxempaque = unidadesXempaque;
            prods.CostoBase = costoBase;
            prods.Largo = largoProd;
            prods.Ancho = anchoProd;
            prods.Alto = altoProd;
            prods.Peso = pesoProd;
            prods.IdProveedor = idProveedor;
            prods.IdTipoProducto = idTipoProducto;
            prods.Iva = ivaProd;
            prods.ReteFuente = reteFuente;
            prods.Descuento = descuento;
            prods.Save();
            foreach (PreciosProgramas pp in listaPreciosPrograma)
            {
                PreciosBasePrograma pbp = new PreciosBasePrograma();
                pbp.GuidPrecioProducto = System.Guid.NewGuid().ToString();
                pbp.PrecioVentaBase = pp.PrecioBase;
                pbp.CostoEnvioBase = pp.CostoEnvioBase;
                pbp.ValorAsegurado = pp.ValorAsegurado;
                pbp.GuidProducto = prods.Guid;
                pbp.IdPrograma = pp.GuidPrograma;
                pbp.IdMarca = pp.GuidMarca;
                pbp.IdTipoDelivery = pp.GuidTipoDelivery;
                pbp.IdCiudad = pp.Ciudades;
                pbp.Direccion = pp.Direcciones;
                pbp.Save();
            }
            SetDefaultIvaAndRete(prods.Iva, prods.ReteFuente);
        }

        public static void AgregarProductos(string codigo, string nombre, string descripcion
                                            , string localizacion, string codigoBarras, int tipoEmpaque
                                            , string unidadesXempaque, decimal costoBase, List<PreciosProgramas> listaPreciosPrograma, decimal largoProd, decimal anchoProd, decimal altoProd, decimal pesoProd
                                            , int idProveedor, /*int idMarca,*/ int idTipoProducto, decimal ivaProd
                                            , decimal reteFuente, decimal? descuento, decimal? ivaCosto)
        {
            Productos prods = new Productos();
            prods.Guid = System.Guid.NewGuid().ToString();
            prods.Codigo = codigo;
            prods.Nombre = nombre;
            prods.Descripcion = descripcion;
            prods.Localizacion = localizacion;
            prods.CodigoBarras = codigoBarras;
            prods.IdTipoEmpaque = tipoEmpaque;
            prods.Unidadesxempaque = unidadesXempaque;
            prods.CostoBase = costoBase;
            prods.Largo = largoProd;
            prods.Ancho = anchoProd;
            prods.Alto = altoProd;
            prods.Peso = pesoProd;
            prods.IdProveedor = idProveedor;
            prods.IdTipoProducto = idTipoProducto;
            prods.Iva = ivaProd;
            prods.ReteFuente = reteFuente;
            prods.Descuento = descuento;
            prods.IvaCosto = ivaCosto;
            prods.Save();
            foreach (PreciosProgramas pp in listaPreciosPrograma)
            {
                PreciosBasePrograma pbp = new PreciosBasePrograma();
                pbp.GuidPrecioProducto = System.Guid.NewGuid().ToString();
                pbp.PrecioVentaBase = pp.PrecioBase;
                pbp.CostoEnvioBase = pp.CostoEnvioBase;
                pbp.ValorAsegurado = pp.ValorAsegurado;
                pbp.GuidProducto = prods.Guid;
                pbp.IdPrograma = pp.GuidPrograma;
                pbp.IdMarca = pp.GuidMarca;
                pbp.IdTipoDelivery = pp.GuidTipoDelivery;
                pbp.IdCiudad = pp.Ciudades;
                pbp.Direccion = pp.Direcciones;
                pbp.Save();
            }
            SetDefaultIvaAndRete(prods.Iva, prods.ReteFuente);
        }

        public static void ActualizarProductos(string codigoProducto, string codigo, string nombre, string descripcion
                                            , string localizacion, string codigoBarras, int tipoEmpaque
                                            , string unidadesXempaque, decimal costoBase
                                            , decimal largoProd, decimal anchoProd, decimal altoProd, decimal pesoProd
                                            , int idProveedor, int idTipoProducto, decimal ivaProd
                                            , decimal reteFuente, decimal? descuento)
        {
            Productos prods = new Productos(codigoProducto);
            prods.Codigo = codigo;
            prods.Nombre = nombre;
            prods.Descripcion = descripcion;
            prods.Localizacion = localizacion;
            prods.CodigoBarras = codigoBarras;
            prods.IdTipoEmpaque = tipoEmpaque;
            prods.Unidadesxempaque = unidadesXempaque;
            prods.CostoBase = costoBase;
            //prods.ValorAsegurado = valorAsegurado;
            prods.Largo = largoProd;
            prods.Ancho = anchoProd;
            prods.Alto = altoProd;
            prods.Peso = pesoProd;
            prods.IdProveedor = idProveedor;
            prods.IdTipoProducto = idTipoProducto;
            prods.Iva = ivaProd;
            prods.ReteFuente = reteFuente;
            prods.Descuento = descuento;
            prods.Save();
            SetDefaultIvaAndRete(prods.Iva, prods.ReteFuente);
        }

        public static void ActualizarProductos(string codigoProducto, string codigo, string nombre, string descripcion
                                            , string localizacion, string codigoBarras, int tipoEmpaque
                                            , string unidadesXempaque, decimal costoBase
                                            , decimal largoProd, decimal anchoProd, decimal altoProd, decimal pesoProd
                                            , int idProveedor, int idTipoProducto, decimal ivaProd
                                            , decimal reteFuente, decimal? descuento, decimal? ivaCosto)
        {
            Productos prods = new Productos(codigoProducto);
            prods.Codigo = codigo;
            prods.Nombre = nombre;
            prods.Descripcion = descripcion;
            prods.Localizacion = localizacion;
            prods.CodigoBarras = codigoBarras;
            prods.IdTipoEmpaque = tipoEmpaque;
            prods.Unidadesxempaque = unidadesXempaque;
            prods.CostoBase = costoBase;
            //prods.ValorAsegurado = valorAsegurado;
            prods.Largo = largoProd;
            prods.Ancho = anchoProd;
            prods.Alto = altoProd;
            prods.Peso = pesoProd;
            prods.IdProveedor = idProveedor;
            prods.IdTipoProducto = idTipoProducto;
            prods.Iva = ivaProd;
            prods.ReteFuente = reteFuente;
            prods.Descuento = descuento;
            prods.IvaCosto = ivaCosto;
            prods.Save();
            SetDefaultIvaAndRete(prods.Iva, prods.ReteFuente);
        }

        public static Productos CargarProductoPorId(string guidProducto)
        {
            Productos producto = new Productos(Productos.Columns.Guid, guidProducto);
            if (producto.IsLoaded)
            {
                return producto;
            }
            else
            {
                return null;
            }

        }

        public static DataTable CargarProductoPorIds(string guidReferenciaProducto)
        {
            ReferenciasProducto referencia = new ReferenciasProducto(guidReferenciaProducto);
            ProductosCollection productos = new ProductosCollection().Where(Productos.Columns.Guid, referencia.GuidProducto);

            return productos.Load().ToDataTable();


        }

        public static ProductosCollection GetProductosByCriterias(int? idProveedor, int? idMarca, int? tipoProducto)
        {
            try
            {
                ProductosCollection prods = new ProductosCollection();
                if (idProveedor.HasValue)
                    prods.Where(Productos.Columns.IdProveedor, idProveedor.Value);
                //if (idMarca.HasValue)
                //    prods.Where(Productos.Columns.IdMarca, idMarca.Value);

                return prods.OrderByAsc(Productos.Columns.Codigo).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetProductoNameByGuid(string guidProducto)
        {
            try
            {
                Productos prods = new Productos(new ReferenciasProducto(guidProducto).GuidProducto);
                if (prods.IsLoaded)
                    return prods.Nombre;
                else
                    throw new Exception("Un producto que no existe esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductoNameByGuid() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetProductoNameByGuidEmpresa(string guidProducto)
        {
            try
            {
                Productos prods = new Productos(guidProducto);
                if (!prods.IsNew)
                    return prods.Nombre;
                else
                    throw new Exception("Un producto que no existe esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductoNameByGuidEmpresa() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetProductoNameByGuidReferencia(string guidReferenciaProducto)
        {
            try
            {
                ReferenciasProducto refs = new ReferenciasProducto(guidReferenciaProducto);
                if (refs.IsLoaded)
                {
                    Productos prods = new Productos(refs.GuidProducto);
                    if (prods.IsLoaded)
                        return prods.Nombre;
                    else
                        throw new Exception("Un producto que no existe esta tratando de acceder a este metodo");
                }
                else
                    throw new Exception("Una referencia inexistente esta tratando de acceder a este m�todo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetIvaProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetProductosByCriterias(int? idProveedor, int? idMarca, int? idTipoProducto, string nombreProducto, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.SelectProductosNombres(idProveedor, idTipoProducto, idMarca, TotalRecords, nombreProducto);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetProductosByCriterias(int? idProveedor, int? idMarca, int? idTipoProducto, string nombreProducto, string codigoProducto, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.SelectProductosNombres(idProveedor, idTipoProducto, idMarca, TotalRecords, nombreProducto, codigoProducto);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool VerificarCodigoProducto(string codigoProducto)
        {
            Productos producto = new Productos(Productos.Columns.Codigo, codigoProducto);
            if (!producto.IsNew)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public static List<PreciosProgramas> GetPreciosProductoPorPrograma(string guidProducto)
        {
            try
            {
                List<PreciosProgramas> listaPrecios = new List<PreciosProgramas>();
                PreciosBaseProgramaCollection pbpc = new PreciosBaseProgramaCollection();
                pbpc = pbpc.Where(PreciosBasePrograma.Columns.GuidProducto, guidProducto).Load();
                foreach (PreciosBasePrograma pbp in pbpc)
                {

                    listaPrecios.Add(new PreciosProgramas(pbp.PrecioVentaBase, pbp.CostoEnvioBase, pbp.ValorAsegurado, pbp.IdPrograma, pbp.IdMarca, pbp.IdTipoDelivery, pbp.Direccion, pbp.IdCiudad));
                }
                return listaPrecios;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPreciosProductoPorPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string[] GetDefaultIvaAndRete()
        {
            try
            {
                string[] ivarete = new string[2];
                MasIvaRetefuenteCollection ivaYRete = new MasIvaRetefuenteCollection();
                ivaYRete.Load();
                foreach (MasIvaRetefuente iYr in ivaYRete)
                {
                    ivarete[0] = iYr.Iva.ToString();
                    ivarete[1] = iYr.RetencionFuente.ToString();
                }
                return ivarete;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDefaultIvaAndRete() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static void SetDefaultIvaAndRete(decimal iva, decimal rete)
        {
            try
            {
                MasIvaRetefuenteCollection ivaYRete = new MasIvaRetefuenteCollection();
                ivaYRete.Load();
                if (ivaYRete.Count > 0)
                {
                    foreach (MasIvaRetefuente iYr in ivaYRete)
                    {
                        iYr.Iva = iva;
                        iYr.RetencionFuente = rete;
                        iYr.Save();
                    }
                }
                else
                {
                    MasIvaRetefuente mirf = new MasIvaRetefuente();
                    mirf.IdIvaRetefuente = 1;
                    mirf.Iva = iva;
                    mirf.RetencionFuente = rete;
                    mirf.Save();
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDefaultIvaAndRete() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        public static MasTiposDeliveryCollection GetTiposDelivery()
        {
            try
            {
                MasTiposDeliveryCollection mtdc = new MasTiposDeliveryCollection();
                return mtdc.Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetTiposDelivery() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetNombreTipoDelivery(int idTipoDelivery)
        {
            try
            {
                MasTiposDelivery mtd = new MasTiposDelivery(idTipoDelivery);
                if (mtd.IsNew)
                    throw new Exception("El tipo de delivery no tiene nombre porque no existe");
                return mtd.TipoDelivery;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetNombreTipoDelivery() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable ExportarProductos(int? providerId, int? productTypeId, int? brandId, string productName)
        {
            try
            {
                return SPs.ExportarProductos(providerId, productTypeId, brandId, productName).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetNombreTipoDelivery() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable ExportarPrecios(int? providerId, int? brandId)
        {
            try
            {
                return SPs.ExportarPrecios(providerId, brandId).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetNombreTipoDelivery() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static Productos CargarProductoPorGuidReferencia(string guidReferencia)
        {
            ReferenciasProducto referencia = new ReferenciasProducto(ReferenciasProducto.Columns.GuidReferenciaProducto, guidReferencia);

            if (referencia.IsLoaded)
            {
                Productos producto = new Productos(Productos.Columns.Guid, referencia.GuidProducto);
                if (producto.IsLoaded)
                {
                    return producto;
                }
                else
                {
                    return null;
                }

            }
            else
            {
                return null;
            }

        }

        public static decimal? CargarCostoProducto(string guidProducto)
        {
            Productos producto = new Productos(Productos.Columns.Guid, guidProducto);
            if (producto.IsLoaded)
            {
                return producto.CostoBase;
            }
            else
            {
                return null;
            }

        }

        /// <summary>
        /// Obtiene los productos de un determinado programa
        /// </summary>
        /// <param name="IdPrograma">Id del programa</param>
        /// <returns>Un objeto tipo DataTable</returns>
        public static DataTable GetProductosAjustes(int IdPrograma)
        {
            try
            {
                return SPs.GetProductosFromProgramaAjustes(IdPrograma).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static decimal[] GetIvaAndPrecioBaseProductoAjuste(string guidProducto)
        {
            try
            {
                string[] parametros = guidProducto.Split('|');
                Productos prods = new Productos(parametros[0]);
                if (prods.IsLoaded)
                {
                    decimal[] arreglo = new decimal[3];
                    arreglo[0] = prods.Iva;
                    PreciosBasePrograma pbp = new PreciosBasePrograma(PreciosBasePrograma.Columns.GuidProducto, guidProducto);
                    if (pbp.IsNew)
                        throw new Exception("El producto no tiene precio de venta y apareci� como posiblidad en este metodo");
                    arreglo[1] = pbp.PrecioVentaBase;
                    arreglo[2] = pbp.CostoEnvioBase;
                    return arreglo;
                }
                else
                    throw new Exception("Un producto que no existe esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetIvaProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable CargarProveedoresByProgram(int idPrograma)
        {
            try
            {
                return SPs.CargarProveedoresByProgram(idPrograma).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable CargarTiposProductosByProgram(int idPrograma)
        {
            try
            {
                return SPs.CargarTiposProductosByProgram(idPrograma).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

    }
}
