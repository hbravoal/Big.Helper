using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using System.Transactions;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class FacturacionController
    {
        #region StaticMethods
        /// <summary>
        /// Obtiene la resolución de facturación de un programa
        /// </summary>
        /// <param name="nombrePrograma">string - Nombre del programa</param>
        /// <returns>Objeto Resoluciones / null si no contiene resoluciones u ocurrio un error en el proceso</returns>
        public static Resoluciones GetResolucionByPrograma(string nombrePrograma)
        {
            try
            {
                //Cargamos el programa para conocer con que resolucion trabaja
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (programa.IsLoaded)
                {
                    //Cargamos la resolucion a ser retornada
                    Resoluciones resolucionFactura = new Resoluciones(programa.IdResolucion);
                    if (resolucionFactura.IsLoaded)
                        return resolucionFactura;
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetResolucionByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// Obtiene la resolución de facturación e incrementa el consecutivo
        /// </summary>
        /// <param name="nombrePrograma">string - Nombre del programa</param>
        /// <returns>Objeto Resoluciones / null si no contiene resoluciones u ocurrio un error en el proceso</returns>
        public static Resoluciones GetConsecutivoFacturacion(string nombrePrograma)
        {
            try
            {
                //Cargamos la resolucion del programa para obtener todos los datos e incrementar el consecutivo
                Resoluciones consecutivo = GetResolucionByPrograma(nombrePrograma);
                if (consecutivo.IsLoaded)
                {
                    //Copiamos el objeto para retornarlo con el consecutivo tal como viene en base de datos
                    Resoluciones consRetornable = consecutivo.Clone();
                    //Sumamos 1 al consecutivo original
                    consecutivo.ValorActual++;
                    consecutivo.Save();
                    return consRetornable;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetConsecutivoFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// Obtiene la resolución de notas credito de un programa
        /// </summary>
        /// <param name="nombrePrograma">int - Id del programa</param>
        /// <returns>Objeto Resoluciones / null si no contiene resoluciones u ocurrio un error en el proceso</returns>
        public static Resoluciones GetResolucionNotaCreditoByPrograma(int idPrograma)
        {
            try
            {
                //Cargamos el programa para conocer con que resolucion trabaja
                Programas programa = new Programas(idPrograma);
                if (!programa.IsNew)
                {
                    //Cargamos la resolucion a ser retornada
                    Resoluciones resolucionFactura = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdNotaCredito);
                    if (resolucionFactura.IsLoaded)
                        return resolucionFactura;
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetResolucionNotaCreditoByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// Obtiene la resolución de nota credito e incrementa el consecutivo
        /// </summary>
        /// <param name="nombrePrograma">int - Id del programa</param>
        /// <returns>Objeto Resoluciones / null si no contiene resoluciones u ocurrio un error en el proceso</returns>
        public static Resoluciones GetConsecutivoNotasCredito(int idPrograma)
        {
            try
            {
                //Cargamos la resolucion del programa para obtener todos los datos e incrementar el consecutivo
                Resoluciones consecutivo = GetResolucionNotaCreditoByPrograma(idPrograma);
                if (consecutivo.IsLoaded)
                {
                    //Copiamos el objeto para retornarlo con el consecutivo tal como viene en base de datos
                    Resoluciones consRetornable = consecutivo.Clone();
                    //Sumamos 1 al consecutivo original
                    consecutivo.ValorActual++;
                    consecutivo.Save();
                    return consRetornable;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetConsecutivoNotasCredito() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static int GetUltimoIdPeriodo(int idPrograma)
        {
            object idPeriodo = SPs.SelectUltimoPeriodoPrograma(idPrograma).ExecuteScalar();
            if (idPeriodo != null)
                return Convert.ToInt32(idPeriodo);
            else
                return 0;
        }

        public static void DelDetallesFacturaAndFactura(string guidFactura)
        {
            SPs.DeleteDetallesFactura(guidFactura);
        }

        public static bool SaveConsecutivoFactura(bool estadoTransaccion, string guidFactura, string nombrePrograma)
        {
            try
            {
                int idPrograma = 0;
                // preguntamos si las transacciones se guadaron correctamente
                if (estadoTransaccion)
                {
                    // obtenemos el id del programa apartir del nombre
                    Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                    //Verifico si el programa fue cargado, en ese caso hay una excepcion
                    if (programa.IsNew)
                    {
                        StringBuilder error = new StringBuilder();
                        error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                        throw new Exception(error.ToString());
                        idPrograma = programa.IdPrograma;
                    }

                    // aumento el consecutivo de la resolución
                    DetallesFactura df = new DetallesFactura(DetallesFactura.Columns.GuidFactura, guidFactura);
                    decimal factorPunto = df.FactorCompraPuntos;
                    if (factorPunto != 1)
                    {
                        Resoluciones res = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdResolucion);
                        Facturas fact = new Facturas(Facturas.Columns.GuidFactura, guidFactura);
                        if (factorPunto == 0)
                        {
                            fact.FechaFactura = DateTime.Now;

                        }
                        fact.NumeroFactura = res.ValorActual;
                        fact.Save();
                        res.ValorActual = res.ValorActual + 1;
                        res.Save();
                    }

                }
                else
                {
                    DetallesFacturaCollection dtFactura = new DetallesFacturaCollection();
                    dtFactura.Where(DetallesFactura.Columns.GuidFactura, guidFactura).Load();

                    foreach (DetallesFactura dt in dtFactura)
                    {
                        if (dt.FactorCompraPuntos == 0)
                        {
                            BodegasCollection bodegasCollDescarga = new BodegasCollection();
                            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescarga.Load();

                            foreach (Bodegas bod in bodegasCollDescarga)
                            {
                                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, dt.GuidReferenciaProducto);
                                disponibilidadDescarga.Load();
                                if (disponibilidadDescarga.Count == 1)
                                {
                                    InventariosController.DelProductosBodegas(bod.IdBodega, dt.GuidReferenciaProducto, dt.Cantidad, dt.GuidDetalleFactura);
                                }
                            }
                        }
                        else
                        {
                            BodegasCollection bodegasCollDescarga = new BodegasCollection();
                            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                            bodegasCollDescarga.Load();

                            foreach (Bodegas bod in bodegasCollDescarga)
                            {
                                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, dt.GuidReferenciaProducto);
                                disponibilidadDescarga.Load();
                                if (disponibilidadDescarga.Count == 1)
                                {
                                    InventariosController.DelProductosBodegas(bod.IdBodega, dt.GuidReferenciaProducto, dt.Cantidad, dt.GuidDetalleFactura);
                                }
                            }
                        }
                    }
                    // Borramos la factura y los detalles
                    DelDetallesFacturaAndFactura(guidFactura);
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFacturaAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool SaveConsecutivoFacturaEmpresa(bool estadoTransaccion, string guidFactura, string nombrePrograma)
        {
            try
            {
                int idPrograma = 0;
                // preguntamos si las transacciones se guadaron correctamente
                if (estadoTransaccion)
                {
                    // obtenemos el id del programa apartir del nombre
                    Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                    //Verifico si el programa fue cargado, en ese caso hay una excepcion
                    if (programa.IsNew)
                    {
                        StringBuilder error = new StringBuilder();
                        error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                        throw new Exception(error.ToString());

                    }
                    idPrograma = programa.IdPrograma;
                    Resoluciones res = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdResolucion);
                    FacturasProveedorPuntos fact = new FacturasProveedorPuntos(FacturasProveedorPuntos.Columns.GuidFacturaProveedorPuntos, guidFactura);
                    fact.NumeroFactura = res.ValorActual;
                    fact.Save();
                    res.ValorActual = res.ValorActual + 1;
                    res.Save();


                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveConsecutivoFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// Guarda la factura y los datos del cliente si el cliente es nuevo
        /// </summary>
        /// <param name="nombrePrograma">string - Nombre del programa</param>
        /// <param name="numeroFactura">int - Numero de la factura</param>
        /// <param name="totalBase">decimal - Total Base</param>
        /// <param name="iva">decimal - Iva</param>
        /// <param name="valorEnLetras">string - Valor en letras</param>
        /// <param name="direccionCliente">string - Direccion del cliente</param>
        /// <param name="telefonoCliente">string - Telefono del cliente</param>
        /// <param name="faxCliente">string - Fax Cliente</param>
        /// <param name="nombresCliente">string - Nombre del Cliente</param>
        /// <param name="apellidosCliente">string - Apellidos del cliente</param>
        /// <param name="cedulaCliente">string - Cedula del cliente</param>
        /// <returns>El guid de la factura</returns>
        public static string SaveFacturaAndCliente(string nombrePrograma, decimal totalBase, decimal iva,
            string direccionCliente, string telefonoCliente, string faxCliente, string nombresCliente, string apellidosCliente, string cedulaCliente,
            int idCiudad)
        {
            try
            {
                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedulaCliente);
                //Verifico si el cliente es nuevo para guardarlo
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Cedula = cedulaCliente;
                    cliente.Nombres = nombresCliente;
                    cliente.Apellidos = apellidosCliente;
                    cliente.Save();
                }
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                Resoluciones res = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdResolucion);
                //Almaceno la informacion necesaria para la factura
                Facturas facturaCliente = new Facturas();
                facturaCliente.GuidFactura = System.Guid.NewGuid().ToString();
                facturaCliente.NumeroFactura = res.ValorActual;
                facturaCliente.TotalBase = totalBase;
                facturaCliente.Iva = iva;
                facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                facturaCliente.DireccionCliente = direccionCliente;
                facturaCliente.TelefonoCliente = telefonoCliente;
                facturaCliente.FaxCliente = faxCliente;
                facturaCliente.IdPrograma = programa.IdPrograma;
                facturaCliente.GuidCliente = cliente.GuidCliente;
                facturaCliente.Prefacturada = false;
                facturaCliente.Anulada = false;
                facturaCliente.GeneradaEmpresa = false;
                facturaCliente.FechaFactura = DateTime.Now;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    facturaCliente.FechaTransaccion = DateTime.Now;
                }
                else
                {
                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    facturaCliente.FechaTransaccion = newDate;
                }
                facturaCliente.IdCiudadCliente = idCiudad;
                facturaCliente.NotaCredito = false;
                facturaCliente.Save();
                res.ValorActual = res.ValorActual + 1;
                res.Save();
                StringBuilder retorno = new StringBuilder();
                retorno.Append(facturaCliente.GuidFactura);
                return retorno.ToString();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFacturaAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string SaveFacturaAndClienteTMP(string nombrePrograma, string fechaTransaccion, decimal totalBase, decimal iva,
            string direccionCliente, string telefonoCliente, string faxCliente, string nombresCliente, string apellidosCliente, string cedulaCliente,
            int idCiudad, string fechaCompra)
        {
            try
            {
                string[] dtmTMP = fechaTransaccion.Split('/');
                string[] dtmCompra = fechaCompra.Split('/');
                DateTime fechaParametro = new DateTime();
                DateTime fechaCompraFinal = new DateTime();
                try
                {
                    fechaParametro = new DateTime(Convert.ToInt32(dtmTMP[2]), Convert.ToInt32(dtmTMP[1]), Convert.ToInt32(dtmTMP[0]), 12, 0, 0);
                }
                catch
                {
                    fechaParametro = DateTime.Now;
                }
                try
                {
                    fechaCompraFinal = new DateTime(Convert.ToInt32(dtmCompra[2]), Convert.ToInt32(dtmCompra[1]), Convert.ToInt32(dtmCompra[0]), Convert.ToInt32(dtmCompra[3]), Convert.ToInt32(dtmCompra[4]), Convert.ToInt32(dtmCompra[5]));
                }
                catch
                {
                    fechaCompraFinal = DateTime.Now;
                }
                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedulaCliente);
                //Verifico si el cliente es nuevo para guardarlo
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Cedula = cedulaCliente;
                    cliente.Nombres = nombresCliente;
                    cliente.Apellidos = apellidosCliente;
                    cliente.Save();
                }
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                Resoluciones res = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdResolucion);
                //Almaceno la informacion necesaria para la factura
                Facturas facturaCliente = new Facturas();
                facturaCliente.GuidFactura = System.Guid.NewGuid().ToString();
                facturaCliente.NumeroFactura = res.ValorActual;
                facturaCliente.TotalBase = totalBase;
                facturaCliente.Iva = iva;
                facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                facturaCliente.DireccionCliente = direccionCliente;
                facturaCliente.TelefonoCliente = telefonoCliente;
                facturaCliente.FaxCliente = faxCliente;
                facturaCliente.IdPrograma = programa.IdPrograma;
                facturaCliente.GuidCliente = cliente.GuidCliente;
                facturaCliente.Prefacturada = false;
                facturaCliente.Anulada = false;
                facturaCliente.GeneradaEmpresa = false;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    facturaCliente.FechaFactura = fechaParametro;
                }
                else
                {
                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    facturaCliente.FechaFactura = newDate;
                }
                facturaCliente.IdCiudadCliente = idCiudad;
                facturaCliente.NotaCredito = false;
                facturaCliente.Save();
                res.ValorActual = res.ValorActual + 1;
                res.Save();
                StringBuilder retorno = new StringBuilder();
                retorno.Append(facturaCliente.GuidFactura);
                return retorno.ToString();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFacturaAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string SaveTransaccionMillasAndCliente(string nombrePrograma, decimal totalBase, decimal iva,
            string direccionCliente, string telefonoCliente, string faxCliente, string nombresCliente, string apellidosCliente, string cedulaCliente,
            int idCiudad, string fechaCompra)
        {
            try
            {
                string[] dtmCompra = fechaCompra.Split('/');
                DateTime fechaCompraFinal = new DateTime();
                try
                {
                    fechaCompraFinal = new DateTime(Convert.ToInt32(dtmCompra[2]), Convert.ToInt32(dtmCompra[1]), Convert.ToInt32(dtmCompra[0]), Convert.ToInt32(dtmCompra[3]), Convert.ToInt32(dtmCompra[4]), Convert.ToInt32(dtmCompra[5]));
                }
                catch
                {
                    fechaCompraFinal = DateTime.Now;
                }
                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedulaCliente);
                //Verifico si el cliente es nuevo para guardarlo
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Cedula = cedulaCliente;
                    cliente.Nombres = nombresCliente;
                    cliente.Apellidos = apellidosCliente;
                    cliente.Save();
                }
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                //Resoluciones res = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdResolucion);
                //Almaceno la informacion necesaria para la factura
                Facturas facturaCliente = new Facturas();
                facturaCliente.GuidFactura = System.Guid.NewGuid().ToString();
                facturaCliente.NumeroFactura = null;
                facturaCliente.TotalBase = totalBase;
                facturaCliente.Iva = iva;
                try
                {
                    facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch
                {
                    facturaCliente.ValorEnLetras = "";
                }
                facturaCliente.DireccionCliente = direccionCliente;
                facturaCliente.TelefonoCliente = telefonoCliente;
                facturaCliente.FaxCliente = faxCliente;
                facturaCliente.IdPrograma = programa.IdPrograma;
                facturaCliente.GuidCliente = cliente.GuidCliente;
                facturaCliente.Prefacturada = false;
                facturaCliente.Anulada = false;
                facturaCliente.GeneradaEmpresa = false;
                //facturaCliente.FechaFactura = DateTime.Now;
                facturaCliente.FechaCompra = fechaCompraFinal;
                facturaCliente.IdCiudadCliente = idCiudad;
                facturaCliente.NotaCredito = false;
                facturaCliente.Save();
                //StringBuilder retorno = new StringBuilder();
                return facturaCliente.GuidFactura;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveTransaccionMillasAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string SaveTransaccionMillasAndClienteTMP(string nombrePrograma, string fechaTransaccion, decimal totalBase, decimal iva,
            string direccionCliente, string telefonoCliente, string faxCliente, string nombresCliente, string apellidosCliente, string cedulaCliente,
            int idCiudad, string fechaCompra)
        {
            try
            {
                string[] dtmTMP = fechaTransaccion.Split('/');
                string[] dtmCompra = fechaCompra.Split('/');
                DateTime fechaParametro = new DateTime();
                DateTime fechaCompraFinal = new DateTime();
                try
                {
                    fechaParametro = new DateTime(Convert.ToInt32(dtmTMP[2]), Convert.ToInt32(dtmTMP[1]), Convert.ToInt32(dtmTMP[0]), 12, 0, 0);
                }
                catch
                {
                    fechaParametro = DateTime.Now;
                }
                try
                {
                    fechaCompraFinal = new DateTime(Convert.ToInt32(dtmCompra[2]), Convert.ToInt32(dtmCompra[1]), Convert.ToInt32(dtmCompra[0]), Convert.ToInt32(dtmCompra[3]), Convert.ToInt32(dtmCompra[4]), Convert.ToInt32(dtmCompra[5]));
                }
                catch
                {
                    fechaCompraFinal = DateTime.Now;
                }
                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedulaCliente);
                //Verifico si el cliente es nuevo para guardarlo
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Cedula = cedulaCliente;
                    cliente.Nombres = nombresCliente;
                    cliente.Apellidos = apellidosCliente;
                    cliente.Save();
                }
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                //Resoluciones res = new Resoluciones(Resoluciones.Columns.IdResolucion, programa.IdResolucion);
                //Almaceno la informacion necesaria para la factura
                Facturas facturaCliente = new Facturas();
                facturaCliente.GuidFactura = System.Guid.NewGuid().ToString();
                facturaCliente.NumeroFactura = null;
                facturaCliente.TotalBase = totalBase;
                facturaCliente.Iva = iva;
                try
                {
                    facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch
                {
                    facturaCliente.ValorEnLetras = "";
                }
                facturaCliente.DireccionCliente = direccionCliente;
                facturaCliente.TelefonoCliente = telefonoCliente;
                facturaCliente.FaxCliente = faxCliente;
                facturaCliente.IdPrograma = programa.IdPrograma;
                facturaCliente.GuidCliente = cliente.GuidCliente;
                facturaCliente.Prefacturada = false;
                facturaCliente.Anulada = false;
                facturaCliente.GeneradaEmpresa = false;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    facturaCliente.FechaFactura = DateTime.Now;
                }
                else
                {
                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    facturaCliente.FechaFactura = newDate;
                }
                facturaCliente.FechaCompra = fechaCompraFinal;
                //facturaCliente.FechaFactura = DateTime.Now;
                facturaCliente.IdCiudadCliente = idCiudad;
                facturaCliente.NotaCredito = false;
                facturaCliente.Save();
                //StringBuilder retorno = new StringBuilder();
                return facturaCliente.GuidFactura;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveTransaccionMillasAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool ActualizarFechaTransaccion(string guidFactura, DateTime fechaFactura)
        {
            try
            {
                Facturas fact = new Facturas(guidFactura);
                if (fact.IsNew)
                    throw new Exception("No se puede actualizar la fecha de una factura que no existe");
                fact.FechaFactura = fechaFactura;
                fact.Save("");
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ActualizarFechaTransaccion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// Replica la informacion de una factura como una nota a credito
        /// </summary>
        /// <param name="factura">Facturas - factura</param>
        /// <returns>True si el proceso corrio exitosamente, false si hubo un error</returns>
        public static bool GenerateNotaCredito(Facturas factura)
        {
            try
            {
                //Almaceno la informacion necesaria para la nota credito
                Facturas notaCredito = factura.Clone();
                notaCredito.NumeroFactura = GetConsecutivoNotasCredito(factura.IdPrograma).ValorActual;
                notaCredito.GuidFactura = System.Guid.NewGuid().ToString();
                notaCredito.Prefacturada = false;
                notaCredito.FechaPrefacturada = null;
                notaCredito.Anulada = false;
                notaCredito.FechaAnulacion = null;
                notaCredito.GeneradaEmpresa = false;
                notaCredito.FechaGeneracionEmpresa = null;
                notaCredito.FechaFactura = DateTime.Now;
                string ultimaFecha = GetUltimoperiodoMes(factura.IdPrograma);
                if (ultimaFecha == null)
                {
                    notaCredito.FechaTransaccion = DateTime.Now;
                }
                else
                {
                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    notaCredito.FechaTransaccion = newDate;
                }
                notaCredito.NotaCredito = true;
                notaCredito.FacturaNotaCredito = factura.NumeroFactura;
                notaCredito.MotivoNotaCredito = "Anulación factura";
                notaCredito.Save();
                DetallesFacturaCollection detalleFactCollection = new DetallesFacturaCollection();
                detalleFactCollection = detalleFactCollection.Where(DetallesFactura.Columns.GuidFactura, factura.GuidFactura).Load();
                foreach (DetallesFactura detalleFact in detalleFactCollection)
                {
                    DetallesFactura detalleNotaCredito = detalleFact.Clone();

                    detalleNotaCredito.GuidDetalleFactura = System.Guid.NewGuid().ToString();
                    detalleNotaCredito.GuidFactura = notaCredito.GuidFactura;
                    detalleNotaCredito.Save();
                    int cantidadACargar = detalleFact.Cantidad;
                    BodegasCollection bodegasCollDescarga = new BodegasCollection();
                    bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                    bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, factura.IdPrograma).Load();
                    foreach (Bodegas bod in bodegasCollDescarga)
                    {
                        BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                        disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleNotaCredito.GuidReferenciaProducto);
                        disponibilidadDescarga.Load();
                        foreach (BodegasProductos bp in disponibilidadDescarga)
                        {
                            if (cantidadACargar > 0)
                            {
                                decimal res = 0;
                                if (detalleFact.DescargoEnInventario.HasValue)
                                {
                                    if (detalleFact.FactorCompraPuntos == 0)
                                    {
                                        res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleNotaCredito.GuidReferenciaProducto, (cantidadACargar), DateTime.Now, detalleNotaCredito.GuidDetalleFactura, null, null, "Descarga de producto  nota credito", null, true, true, -1);
                                        cantidadACargar = 0;
                                    }
                                }
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GenerateNotaCredito() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool GenerateNotaCreditoEmpresa(FacturasProveedorPuntos factura)
        {
            try
            {
                //Almaceno la informacion necesaria para la nota credito
                FacturasProveedorPuntos notaCredito = factura.Clone();
                notaCredito.NumeroFactura = GetConsecutivoNotasCredito(factura.IdPrograma).ValorActual;
                notaCredito.GuidFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                notaCredito.Anulada = false;
                notaCredito.FechaAnulacion = null;
                notaCredito.FechaFactura = DateTime.Now;
                notaCredito.NotaCredito = true;
                notaCredito.Save();
                DetallesFacturaProveedorPuntosCollection detalleFactCollection = new DetallesFacturaProveedorPuntosCollection();
                detalleFactCollection = detalleFactCollection.Where(DetallesFacturaProveedorPuntos.Columns.GuidFacturaProveedorPuntos, factura.GuidFacturaProveedorPuntos).Load();

                foreach (DetallesFacturaProveedorPuntos detalleFact in detalleFactCollection)
                {
                    DetallesFacturaProveedorPuntos detalleNotaCredito = detalleFact.Clone();
                    detalleNotaCredito.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                    detalleNotaCredito.GuidFacturaProveedorPuntos = notaCredito.GuidFacturaProveedorPuntos;
                    detalleNotaCredito.Save();
                    int cantidadACargar = detalleFact.Cantidad;
                    BodegasCollection bodegasCollDescarga = new BodegasCollection();
                    bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                    bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, factura.IdPrograma).Load();

                    string guidReferencia = ProductsController.GetReferenciaByProducto(detalleFact.GuidProducto);


                    foreach (Bodegas bod in bodegasCollDescarga)
                    {
                        BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                        disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferencia);
                        disponibilidadDescarga.Load();
                        foreach (BodegasProductos bp in disponibilidadDescarga)
                        {
                            if (cantidadACargar > 0)
                            {
                                decimal res = 0;

                                res = InventariosController.SetProductosBodegasEmpresa(bod.IdBodega, guidReferencia, (cantidadACargar), DateTime.Now, detalleNotaCredito.GuidDetalleFacturaProveedorPuntos, null, null, "Descarga de producto nota credito", null, true, true, -1);
                                cantidadACargar = 0;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GenerateNotaCredito() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }


        public static bool GenerateFacturaEmpresaNew(FacturasProveedorPuntos fact)
        {
            try
            {
                FacturasProveedorPuntos factNew = fact.Clone();
                factNew.GuidFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                factNew.FechaFactura = DateTime.Now;
                factNew.NotaCredito = false;
                factNew.Anulada = false;
                factNew.Save();

                //guarda los detalles

                DetallesFacturaProveedorPuntosCollection detalleFactCollection = new DetallesFacturaProveedorPuntosCollection();
                detalleFactCollection = detalleFactCollection.Where(DetallesFacturaProveedorPuntos.Columns.GuidFacturaProveedorPuntos, fact.GuidFacturaProveedorPuntos).Load();
                bool isok = false;
                foreach (DetallesFacturaProveedorPuntos detalleFact in detalleFactCollection)
                {
                    DetallesFacturaProveedorPuntos detalleFactNew = detalleFact.Clone();
                    detalleFactNew.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                    detalleFactNew.GuidFacturaProveedorPuntos = factNew.GuidFacturaProveedorPuntos;
                    detalleFactNew.Save();

                    //descargo de bodega

                    int cantidadACargar = detalleFactNew.Cantidad;
                    BodegasCollection bodegasCollDescarga = new BodegasCollection();
                    bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                    bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, factNew.IdPrograma).Load();

                    string guidReferencia = ProductsController.GetReferenciaByProducto(detalleFact.GuidProducto);


                    foreach (Bodegas bod in bodegasCollDescarga)
                    {
                        BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                        disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferencia);
                        disponibilidadDescarga.Load();
                        foreach (BodegasProductos bp in disponibilidadDescarga)
                        {
                            if (cantidadACargar > 0)
                            {
                                decimal res = 0;

                                res = InventariosController.SetProductosBodegasEmpresa(bod.IdBodega, guidReferencia, (cantidadACargar), DateTime.Now, detalleFactNew.GuidDetalleFacturaProveedorPuntos, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar = 0;
                            }
                        }
                    }
                }
                Programas prog = new Programas(Programas.Columns.IdPrograma, fact.IdPrograma);
                if (!prog.IsNew)
                {
                    FacturacionController.SaveConsecutivoFacturaEmpresa(true, factNew.GuidFacturaProveedorPuntos, prog.NombrePrograma);
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GenerateNotaCredito() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }


        /// <summary>
        /// Guarda el detalle de la factura
        /// </summary>
        /// <param name="numeroItem">int - Numero del item</param>
        /// <param name="cantidad">int - Cantidad</param>
        /// <param name="valorBase">decimal - Valor base</param>
        /// <param name="envioBase">decimal - Envio base</param>
        /// <param name="iva">decimal - Iva</param>
        /// <param name="guidFactura">string - guid de la factura</param>
        /// <param name="nombreMarca">string - nombre de la marca</param>
        /// <param name="nombreProducto">string - nombre del producto</param>
        /// <returns>True si el proceso corrio exitosamente, false si hubo un error</returns>
        public static bool SaveDetalleFactura(int numeroItem, int cantidad, decimal valorBase, decimal envioBase, decimal iva, string guidFactura,
            string nombreMarca, string nombreProducto, string nombreReferenciaProducto, decimal factorCompraPunto)
        {
            try
            {
                Facturas fact = new Facturas(guidFactura);
                ReferenciasProducto refs = new ReferenciasProducto(nombreReferenciaProducto);
                if (refs.IsNew)
                    throw new Exception("La referencia del producto que está tratando de insertar no existe");
                DetallesFactura detalle = new DetallesFactura();
                detalle.GuidDetalleFactura = System.Guid.NewGuid().ToString();
                detalle.NumeroItem = numeroItem;
                detalle.Cantidad = cantidad;
                detalle.ValorBase = valorBase;
                detalle.EnvioBase = envioBase;
                detalle.Iva = iva;
                detalle.GuidFactura = guidFactura;
                detalle.GuidReferenciaProducto = refs.GuidReferenciaProducto;
                detalle.FactorCompraPuntos = factorCompraPunto;
                detalle.Save();
                DescargarInventarioTransaccionNew(fact.IdPrograma, refs.GuidReferenciaProducto, cantidad, detalle.GuidDetalleFactura);
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool SaveDetalleTransaccion(int numeroItem, int cantidad, decimal valorBase, decimal envioBase, decimal iva, string guidFactura,
            string guidReferenciaProducto, decimal factorCompraPunto)
        {
            try
            {
                Facturas fact = new Facturas(guidFactura);
                if (fact.IsNew)
                    throw new Exception("No existe la transacción para el detalle que se pretende crear.");
                if (VerificarInventario(fact.IdPrograma, guidReferenciaProducto, cantidad))
                {
                    DetallesFactura detalle = new DetallesFactura();
                    detalle.GuidDetalleFactura = System.Guid.NewGuid().ToString();
                    detalle.NumeroItem = numeroItem;
                    detalle.Cantidad = cantidad;
                    detalle.ValorBase = valorBase;
                    detalle.EnvioBase = envioBase;
                    detalle.Iva = iva;
                    detalle.GuidFactura = guidFactura;
                    detalle.GuidReferenciaProducto = guidReferenciaProducto;
                    detalle.FactorCompraPuntos = factorCompraPunto;
                    detalle.Save();
                    if (DescargarInventarioTransaccionNew(fact.IdPrograma, guidReferenciaProducto, cantidad, detalle.GuidDetalleFactura))
                        return true;
                    else
                        throw new Exception("No se pudo descargar el inventario, la transacción fue reversada");
                }
                else
                    throw new Exception("No existe inventario para la transaccion que se pretende crear");
            }
            catch (Exception ex)
            {
                ReversarTransaccion(guidFactura);
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveDetalleTransaccion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool DescargarInventarioTransaccionNew(int idPrograma, string guidReferenciaProducto, int cantidad, string guidDetalleFactura)
        {

            //Descarga para bodegas fisicas y si es 100% dinero
            decimal factorDetalle = 0;
            decimal factorPunto = InventariosController.GetFactorPunto(guidDetalleFactura);
            if (factorPunto == factorDetalle)
            {

                // Descarga de la bodegas de saldo si hay existencias

                int cantidadACargarSaldos = cantidad;
                BodegasCollection bodegasCollDescargaSaldos = new BodegasCollection();
                bodegasCollDescargaSaldos.Where(Bodegas.Columns.IdPrograma, idPrograma);
                bodegasCollDescargaSaldos.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaSaldos"]);
                bodegasCollDescargaSaldos.Load();

                foreach (Bodegas bod in bodegasCollDescargaSaldos)
                {
                    BodegasProductosCollection disponibilidadDescargaSaldos = new BodegasProductosCollection();
                    disponibilidadDescargaSaldos.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidadDescargaSaldos.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidadDescargaSaldos.Load();
                    foreach (BodegasProductos bp in disponibilidadDescargaSaldos)
                    {
                        if (cantidadACargarSaldos > 0)
                        {
                            decimal res = 0;
                            if (cantidadACargarSaldos > bp.Cantidad)
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargarSaldos -= Convert.ToInt32(bp.Cantidad);
                            }
                            else
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, cantidadACargarSaldos, DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargarSaldos = 0;
                            }
                        }
                    }
                }

                //Descarga bodegas fisicas
                BodegasCollection bodegasCollDescarga = new BodegasCollection();
                bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma);
                bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                bodegasCollDescarga.Load();

                foreach (Bodegas bod in bodegasCollDescarga)
                {
                    BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                    disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidadDescarga.Load();
                    if (disponibilidadDescarga.Count == 0)
                    {
                        BodegasProductos bdp = new BodegasProductos();
                        bdp.IdBodega = bod.IdBodega;
                        bdp.GuidReferenciaProducto = guidReferenciaProducto;
                        bdp.Cantidad = 0;
                        bdp.Save();

                        disponibilidadDescarga = new BodegasProductosCollection();
                        disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                        disponibilidadDescarga.Load();


                    }
                    foreach (BodegasProductos bp in disponibilidadDescarga)
                    {
                        if (cantidadACargarSaldos > 0)
                        {
                            decimal res = 0;
                            res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, cantidadACargarSaldos, DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                            cantidadACargarSaldos = 0;
                        }
                    }
                }

                //descarga de boedgas virtuales si es mayor que 0 la cantidad
                int cantidadACargar = cantidad;
                BodegasCollection bodegasCollDescargaVirutal = new BodegasCollection();
                bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdPrograma, idPrograma);
                bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                bodegasCollDescargaVirutal.Load();

                foreach (Bodegas bod in bodegasCollDescargaVirutal)
                {
                    BodegasProductosCollection disponibilidadDescargaVirutal = new BodegasProductosCollection();
                    disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidadDescargaVirutal.Load();
                    foreach (BodegasProductos bp in disponibilidadDescargaVirutal)
                    {
                        if (cantidadACargar > 0)
                        {
                            decimal res = 0;
                            if (cantidadACargar > bp.Cantidad)
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                            }
                            else
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, cantidadACargar, DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar = 0;
                            }
                        }
                    }
                }

                // Se marca como descargado el detalle
                DetallesFactura detalle = new DetallesFactura(DetallesFactura.Columns.GuidDetalleFactura, guidDetalleFactura);
                if (!detalle.IsNew)
                {
                    detalle.DescargoEnInventario = true;
                    detalle.Save();
                }
            }
            else
            {
                //descargo de virtual solamente

                //descarga de boedgas virtuales si es mayor que 0 la cantidad
                int cantidadACargar = cantidad;
                BodegasCollection bodegasCollDescargaVirutal = new BodegasCollection();
                bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdPrograma, idPrograma);
                bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                bodegasCollDescargaVirutal.Load();

                foreach (Bodegas bod in bodegasCollDescargaVirutal)
                {
                    BodegasProductosCollection disponibilidadDescargaVirutal = new BodegasProductosCollection();
                    disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidadDescargaVirutal.Load();
                    foreach (BodegasProductos bp in disponibilidadDescargaVirutal)
                    {
                        if (cantidadACargar > 0)
                        {
                            decimal res = 0;
                            if (cantidadACargar > bp.Cantidad)
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                            }
                            else
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, cantidadACargar, DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar = 0;
                            }
                        }
                    }
                }

                // Se marca como no descargado el detalle
                DetallesFactura detalle = new DetallesFactura(DetallesFactura.Columns.GuidDetalleFactura, guidDetalleFactura);
                if (!detalle.IsNew)
                {
                    detalle.DescargoEnInventario = false;
                    detalle.Save();
                }
            }
            return true;
        }

        public static bool DescargarInventarioTransaccionNewPeriodo(int idPrograma, string guidReferenciaProducto, int cantidad, string guidDetalleFactura)
        {
            BodegasCollection bodegasCollDescarga = new BodegasCollection();
            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma);
            bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
            bodegasCollDescarga.Load();

            foreach (Bodegas bod in bodegasCollDescarga)
            {
                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                disponibilidadDescarga.Load();
                if (disponibilidadDescarga.Count == 0)
                {
                    BodegasProductos bdp = new BodegasProductos();
                    bdp.IdBodega = bod.IdBodega;
                    bdp.GuidReferenciaProducto = guidReferenciaProducto;
                    bdp.Cantidad = 0;
                    bdp.Save();

                    disponibilidadDescarga = new BodegasProductosCollection();
                    disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidadDescarga.Load();


                }
                foreach (BodegasProductos bp in disponibilidadDescarga)
                {
                    if (cantidad > 0)
                    {
                        decimal res = 0;
                        res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, cantidad, DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                    }
                }
            }
            return true;
        }
        public static bool DescargarInventarioTransaccion(int idPrograma, string guidReferenciaProducto, int cantidad, string guidDetalleFactura)
        {
            try
            {
                int cantidadACargar = cantidad;
                BodegasCollection bodegasCollDescarga = new BodegasCollection();
                bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                foreach (Bodegas bod in bodegasCollDescarga)
                {
                    BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                    disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidadDescarga.Load();
                    foreach (BodegasProductos bp in disponibilidadDescarga)
                    {
                        if (cantidadACargar > 0)
                        {
                            decimal res = 0;
                            if (cantidadACargar > bp.Cantidad)
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                            }
                            else
                            {
                                res = InventariosController.SetProductosBodegas(bod.IdBodega, guidReferenciaProducto, cantidadACargar, DateTime.Now, guidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                cantidadACargar = 0;
                            }
                        }
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo DescargarInventarioTransaccion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool VerificarInventario(int idPrograma, string guidReferenciaProducto, int cantidad)
        {
            try
            {
                BodegasCollection bodegasColl = new BodegasCollection();
                bodegasColl.Where(Bodegas.Columns.IdPrograma, idPrograma);
                bodegasColl.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]).Load();
                int cantidadEnBodega = 0;
                foreach (Bodegas bod in bodegasColl)
                {
                    BodegasProductosCollection disponibilidad = new BodegasProductosCollection();
                    disponibilidad.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                    disponibilidad.Where(BodegasProductos.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                    disponibilidad.Load();
                    foreach (BodegasProductos bp in disponibilidad)
                    {
                        cantidadEnBodega += Convert.ToInt32(bp.Cantidad);
                    }
                }
                if (cantidad > cantidadEnBodega)
                    return false;
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo VerificarInventario() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static void ReversarTransaccion(string guidFactura)
        {
            try
            {
                DetallesFacturaCollection dfc = new DetallesFacturaCollection();
                dfc.Where(DetallesFactura.Columns.GuidFactura, guidFactura).Load();
                foreach (DetallesFactura detalle in dfc)
                {
                    DetallesFactura.Delete(detalle.GuidDetalleFactura);
                }
                Facturas.Delete(guidFactura);
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ReversarTransaccion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        /// <summary>
        /// Anula una factura
        /// </summary>
        /// <param name="nombrePrograma">string - Nombre del programa</param>
        /// <param name="numeroFactura">int - Consecutivo de la factura</param>
        /// <returns>True si el proceso corrio exitosamente, false si hubo un error</returns>
        public static bool AnularFactura(string nombrePrograma, int numeroFactura)
        {
            try
            {
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                FacturasCollection factsCollection = new FacturasCollection();
                factsCollection = factsCollection.Where(Facturas.Columns.IdPrograma, programa.IdPrograma).Where(Facturas.Columns.NumeroFactura, numeroFactura).Load();
                //Verifico que la factura solicitada exista y sea unica en la base de datos por programa
                if (factsCollection.Count == 0)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("La factura ").Append(numeroFactura).Append(" perteneciente al programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                else if (factsCollection.Count > 1)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("Existe mas de una factura con el consecutivo ").Append(numeroFactura).Append(" perteneciente al programa ").Append(nombrePrograma).Append(", error de inconsistencia en la base de datos");
                    throw new Exception(error.ToString());
                }
                //Marco la factura como anulada
                foreach (Facturas facts in factsCollection)
                {
                    facts.Anulada = true;
                    facts.FechaAnulacion = DateTime.Now;
                    facts.Save();
                    if (facts.Prefacturada)
                    {
                        return GenerateNotaCredito(facts);
                    }
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AnularFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// Prefactura un rango de facturas pertenecientes a un periodo de tiempo
        /// </summary>
        /// <param name="nombrePrograma">string - Nombre del programa</param>
        /// <param name="fechaInicial">DateTime - fecha inicial del periodo prefacturado</param>
        /// <param name="fechaFinal">DateTime - fecha final del periodo prefacturado</param>
        /// <returns>True si el proceso corrio exitosamente, false si hubo un error</returns>
        public static bool PrefacturarFactura(string nombrePrograma, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                FacturasCollection factsCollection = new FacturasCollection();
                factsCollection = factsCollection.Where(Facturas.Columns.IdPrograma, programa.IdPrograma).BetweenAnd(Facturas.Columns.FechaFactura, fechaInicial, fechaFinal).Where(Facturas.Columns.Anulada, false).Load();
                //Prefacturo las facturas de ese periodo pertenecientes a ese programa que no hayan sido anuladas
                foreach (Facturas facts in factsCollection)
                {
                    facts.Prefacturada = true;
                    facts.FechaFactura = DateTime.Now;
                    facts.Save();
                }
                Prefacturas pr = new Prefacturas();
                pr.IdPrograma = programa.IdPrograma;
                pr.FechaInicio = fechaInicial;
                pr.FechaFin = fechaFinal;
                pr.Save();
                //Nuevo proceso para generar la factura tipo banco
                DataTable proveedoresAPrefacturar = PreBillingsController.GetProveedoresPrefacturacionByFecha(fechaInicial, fechaFinal, programa.IdPrograma).Tables[0];
                foreach (DataRow dr in proveedoresAPrefacturar.Rows)
                {
                    int idProveedor = Convert.ToInt32(dr["ID_PROVEEDOR"]);
                    string guidFacturaBanco = PreBillingsController.GenerateBillForPreBilling(0, 0, idProveedor, programa.IdPrograma, pr.IdPrefactura);
                    DataTable productosAGenerar = PreBillingsController.GetProductosPrefacturablesByProveedor(fechaInicial, fechaFinal, programa.IdPrograma, idProveedor).Tables[0];
                    decimal totalBase = 0;
                    decimal totalIva = 0;
                    foreach (DataRow dr2 in productosAGenerar.Rows)
                    {
                        string guidProducto = dr2["GUID_REFERENCIA_PRODUCTO"].ToString();
                        decimal costoBase = Convert.ToDecimal(dr2["COSTO_BASE"]);
                        decimal iva = Convert.ToDecimal(dr2["IVA"]);
                        int cantidad = Convert.ToInt32(dr2["CANTIDAD"]);
                        totalBase += costoBase * cantidad;
                        totalIva += iva * cantidad;
                        PreBillingsController.GenerateBillDetailForPreBilling(cantidad, costoBase, iva, guidFacturaBanco, guidProducto);
                    }
                    PreBillingsController.ActualizarBankBill(guidFacturaBanco, totalBase, totalIva);
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo PrefacturarFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool SecondPrefacturarFacturas(string nombrePrograma, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                Programas progs = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!progs.IsLoaded)
                    throw new Exception("El programa no existe");

                int cantidadReal = 0;
                int idPrefactura = PreBillingsController.SetPrefacturacion(progs.IdPrograma, fechaInicial, fechaFinal);

                DataTable guidFacturasPrefacturar = PreBillingsController.GetGuidTransaccionesPorFecha(fechaInicial, fechaFinal, progs.IdPrograma).Tables[0];
                foreach (DataRow dr in guidFacturasPrefacturar.Rows)
                {
                    String GuidTransaccion = dr["GUID_FACTURA"].ToString();
                    cantidadReal += PreBillingsController.SetPrefacturacionFacturas(GuidTransaccion);
                }

                //Nuevo proceso para generar la factura tipo banco
                DataTable proveedoresAPrefacturar = PreBillingsController.GetProveedoresPrefacturacionByFecha(fechaInicial, fechaFinal, progs.IdPrograma).Tables[0];
                foreach (DataRow dr in proveedoresAPrefacturar.Rows)
                {
                    int idProveedor = Convert.ToInt32(dr["ID_PROVEEDOR"]);
                    string guidFacturaBanco = PreBillingsController.GenerateBillForPreBilling(0, 0, idProveedor, progs.IdPrograma, idPrefactura);
                    DataTable productosAGenerar = PreBillingsController.GetProductosPrefacturablesByProveedor(fechaInicial, fechaFinal, progs.IdPrograma, idProveedor).Tables[0];
                    decimal totalBase = 0;
                    decimal totalIva = 0;
                    foreach (DataRow dr2 in productosAGenerar.Rows)
                    {
                        string guidProducto = dr2["GUID_REFERENCIA_PRODUCTO"].ToString();
                        decimal costoBase = Convert.ToDecimal(dr2["COSTO_BASE"]);
                        decimal iva = Convert.ToDecimal(dr2["IVA"]);
                        int cantidad = Convert.ToInt32(dr2["CANTIDAD"]);
                        totalBase += costoBase * cantidad;
                        totalIva += iva * cantidad;
                        PreBillingsController.GenerateBillDetailForPreBilling(cantidad, costoBase, iva, guidFacturaBanco, guidProducto);
                    }
                    PreBillingsController.ActualizarBankBill(guidFacturaBanco, totalBase, totalIva);
                }

                //Continua el proceso anterior
                if (cantidadReal == guidFacturasPrefacturar.Rows.Count)
                    return true;
                else
                    return false;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SecondPrefacturarFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static string GetUltimaFechaPrefacturadaByPrograma(string nombrePrograma)
        {
            try
            {
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                PrefacturasCollection prCol = new PrefacturasCollection();
                prCol = prCol.Where(Prefacturas.Columns.IdPrograma, programa.IdPrograma).OrderByAsc(Prefacturas.Columns.FechaFin).Load();
                if (prCol.Count == 0)
                    return programa.FechaInicio.ToString("dd/MM/yyyy");
                else
                {
                    string fecha = "";
                    foreach (Prefacturas pr in prCol)
                    {
                        fecha = pr.FechaFin.Value.ToString("dd/MM/yyyy");
                    }
                    return fecha;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo PrefacturarFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return DateTime.Now.ToString("dd/MM/yyyy");
            }
        }

        public static string GetLatestPrefacturaProveedorPuntos(string nombrePrograma)
        {
            try
            {
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                PeriodoFacturacionPuntosCollection prCol = new PeriodoFacturacionPuntosCollection();
                prCol.Where(PeriodoFacturacionPuntos.Columns.IdPrograma, programa.IdPrograma);
                prCol.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, true).OrderByAsc(PeriodoFacturacionPuntos.Columns.FechaFin).Load();
                if (prCol.Count == 0)
                    return programa.FechaInicio.ToString("dd/MM/yyyy");
                else
                {
                    string fecha = "";
                    foreach (PeriodoFacturacionPuntos pr in prCol)
                    {
                        fecha = pr.FechaFin.ToString("dd/MM/yyyy");
                    }
                    return fecha;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetLatestPrefacturaProveedorPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="IdUsuario"></param>
        /// <returns></returns>
        public static DataTable GetCacheDetalleFactura(int IdUsuario)
        {
            try
            {
                return SPs.GetCacheDetallesFactura(IdUsuario).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetCacheDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static CacheDetallesFacturaCollection GetCacheDetalleFacturaByCaches(string idCaches)
        {

            try
            {
                string[] caches = idCaches.Split(',');
                CacheDetallesFacturaCollection cdc = new CacheDetallesFacturaCollection();
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetallesFactura cdf = new CacheDetallesFactura(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                    cdc.Add(cdf);
                }


                return cdc;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetCacheDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static CacheDetalleAjusteCollection GetCacheDetalleAjustesByCaches(string idCaches)
        {

            try
            {
                string[] caches = idCaches.Split(',');
                CacheDetalleAjusteCollection cdc = new CacheDetalleAjusteCollection();
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetalleAjuste cdf = new CacheDetalleAjuste(CacheDetalleAjuste.Columns.IdCacheDetalleAjuste, caches[i]);
                    cdc.Add(cdf);
                }


                return cdc;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetCacheDetalleAjustesByCaches(string idCaches) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// Limpia los detalles de factura de un usuario
        /// </summary>
        /// <param name="IdUsuario">Id del usuario</param>
        public static void CleanCacheDetalle(int IdUsuario)
        {
            try
            {
                CacheDetallesFactura.Delete(CacheDetallesFactura.Columns.IdUsuario, IdUsuario);
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CleanCacheDetalle() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        /// <summary>
        /// Limpia los detalles de factura de un usuario
        /// </summary>
        /// <param name="IdUsuario">Id del usuario</param>
        public static void CleanCacheDetalle2(string idCaches)
        {
            try
            {
                string[] caches = idCaches.Split(',');
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetallesFactura.Delete(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                }

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CleanCacheDetalle() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        /// <summary>
        /// Limpia los detalles de AJUSTE que vienen en el string
        /// </summary>
        /// <param name="IdUsuario">Id del usuario</param>
        public static void CleanCacheDetalle3(string idCaches)
        {
            try
            {
                string[] caches = idCaches.Split(',');
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetalleAjuste.Delete(CacheDetalleAjuste.Columns.IdCacheDetalleAjuste, caches[i]);
                }

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CleanCacheDetalle3() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }
        /// <summary>
        /// Limpia los detalles del ajuste de un usuario
        /// </summary>
        /// <param name="IdUsuario">Id del usuario</param>
        public static void CleanCacheDetalle4(int IdUsuario)
        {
            try
            {
                CacheDetalleAjuste.Delete(CacheDetalleAjuste.Columns.IdUsuario, IdUsuario);
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CleanCacheDetalle4() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        /// <summary>
        ///  Limpia los detalles de AJUSTE que vienen en el string
        /// </summary>
        /// <param name="idCaches">id de los ajustes</param>
        /// <param name="idCache">id del ajuste a limpiar</param>
        /// <returns>id de los ajustes que quedan para mostrar</returns>
        public static string CleanCacheDetalle5(string idCaches, string idCache)
        {
            try
            {
                string[] caches = idCaches.Split(',');
                string hfIdCollec = "";
                for (int i = 0; i < caches.Length - 1; i++)
                {

                    if (caches[i].Equals(idCache))
                    {
                        CacheDetalleAjuste.Delete(CacheDetalleAjuste.Columns.IdCacheDetalleAjuste, caches[i]);
                    }
                    else
                    {
                        hfIdCollec += caches[i] + ",";
                    }
                }
                return hfIdCollec;

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CleanCacheDetalle5() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return idCaches;
            }
        }
        /// <summary>
        /// Limpia los detalles de factura de un usuario

        /// </summary>
        /// <param name="idCaches">id de los detalle de la factura en contexto </param>
        /// <param name="idCache">id del detalle a eliminar de la factura en contexto</param>
        /// <returns>id de las facturas que quedan en contexto</returns>
        public static string CleanCacheDetalle6(string idCaches, string idCache)
        {
            try
            {
                string[] caches = idCaches.Split(',');
                string hfIdCollec = "";
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    if (caches[i].Equals(idCache))
                    {
                        CacheDetallesFactura.Delete(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                    }
                    else
                    {
                        hfIdCollec += caches[i] + ",";
                    }
                }
                return hfIdCollec;

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CleanCacheDetalle() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return idCaches;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="cantidad"></param>
        /// <param name="valorBase"></param>
        /// <param name="envioBase"></param>
        /// <param name="iva"></param>
        /// <param name="guidProducto"></param>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public static bool SaveCacheDetalleFactura(int cantidad, decimal valorBase, decimal envioBase, decimal iva, string guidReferenciaProducto, int idUsuario, decimal factorCompraPunto)
        {
            try
            {
                CacheDetallesFacturaCollection sumador = new CacheDetallesFacturaCollection();
                sumador.Where(CacheDetallesFactura.Columns.IdUsuario, idUsuario);
                sumador.Where(CacheDetallesFactura.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                sumador.Where(CacheDetallesFactura.Columns.EnvioBase, envioBase);
                sumador.Where(CacheDetallesFactura.Columns.FactorCompraPuntos, factorCompraPunto);
                sumador.Load();
                if (sumador.Count == 1)
                {
                    foreach (CacheDetallesFactura cdf in sumador)
                    {
                        cdf.Cantidad = cdf.Cantidad + cantidad;
                        cdf.Save();
                    }
                }
                else
                {
                    CacheDetallesFactura cache = new CacheDetallesFactura();
                    cache.Cantidad = cantidad;
                    cache.ValorBase = valorBase;
                    cache.EnvioBase = envioBase;
                    cache.Iva = iva;
                    cache.GuidReferenciaProducto = guidReferenciaProducto;
                    cache.IdUsuario = idUsuario;
                    cache.FactorCompraPuntos = factorCompraPunto;
                    cache.Save();
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveCacheDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static string SaveCacheDetalleFactura2(int cantidad, decimal valorBase, decimal envioBase, decimal iva, string guidReferenciaProducto, int idUsuario, decimal factorCompraPunto)
        {
            string idCache = "";
            try
            {
                CacheDetallesFacturaCollection sumador = new CacheDetallesFacturaCollection();
                sumador.Where(CacheDetallesFactura.Columns.IdUsuario, idUsuario);
                sumador.Where(CacheDetallesFactura.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                sumador.Where(CacheDetallesFactura.Columns.EnvioBase, envioBase);
                sumador.Where(CacheDetallesFactura.Columns.FactorCompraPuntos, factorCompraPunto);
                sumador.Load();
                if (sumador.Count == 1)
                {
                    foreach (CacheDetallesFactura cdf in sumador)
                    {
                        cdf.Cantidad = cdf.Cantidad + cantidad;
                        cdf.Save();
                        idCache = cdf.IdCacheDetalle.ToString();
                    }
                }
                else
                {
                    CacheDetallesFactura cache = new CacheDetallesFactura();
                    cache.Cantidad = cantidad;
                    cache.ValorBase = valorBase;
                    cache.EnvioBase = envioBase;
                    cache.Iva = iva;
                    cache.GuidReferenciaProducto = guidReferenciaProducto;
                    cache.IdUsuario = idUsuario;
                    cache.FactorCompraPuntos = factorCompraPunto;
                    cache.Save();
                    idCache = cache.IdCacheDetalle.ToString();
                }
                return idCache;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveCacheDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string SaveCacheDetalleAjuste(int cantidad, decimal precioBase, decimal costoBase, string guidReferenciaProducto, int idUsuario)
        {
            string idCache = "";
            try
            {
                CacheDetalleAjusteCollection sumador = new CacheDetalleAjusteCollection();
                sumador.Where(CacheDetalleAjuste.Columns.GuidReferenciaProducto, guidReferenciaProducto);
                sumador.Where(CacheDetalleAjuste.Columns.CostoUnitario, costoBase);
                sumador.Where(CacheDetalleAjuste.Columns.PrecioUnitario, precioBase);
                sumador.Where(CacheDetalleAjuste.Columns.IdUsuario, idUsuario);
                sumador.Load();
                if (sumador.Count == 1)
                {
                    foreach (CacheDetalleAjuste cdf in sumador)
                    {
                        cdf.Cantidad = cdf.Cantidad + cantidad;
                        cdf.Save();
                        //idCache = cdf.IdCacheDetalleAjuste.ToString();
                    }
                }
                else
                {
                    CacheDetalleAjuste cache = new CacheDetalleAjuste();
                    cache.Cantidad = cantidad;
                    cache.CostoUnitario = costoBase;
                    cache.PrecioUnitario = precioBase;
                    cache.GuidReferenciaProducto = guidReferenciaProducto;
                    cache.IdUsuario = idUsuario;
                    cache.Save();
                    idCache = cache.IdCacheDetalleAjuste.ToString();
                }
                return idCache;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveCacheDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }


        public static Clientes GetClienteByCedula(string cedula)
        {
            try
            {
                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedula);
                if (cliente.IsNew)
                {
                    return null;
                }
                else
                {
                    return cliente;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetClienteByCedula() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static Clientes GetClienteByID(string idCliente)
        {
            try
            {
                Clientes cliente = new Clientes(idCliente);
                if (!cliente.IsLoaded)
                    throw new Exception("Un cliente que no existe se presenta en una factura");
                else
                    return cliente;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetClienteByCedula() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static string SaveCliente(string cedula, string nombres, string apellidos)
        {
            try
            {
                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedula);
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Nombres = nombres;
                    cliente.Apellidos = apellidos;
                    cliente.Cedula = cedula;
                    cliente.Save();
                }
                return cliente.GuidCliente;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        #endregion

        #region ConvertidorALetras Util

        private static string[] _grupos = { "", "millon", "billon", "trillon" };
        private static string[] _unidades = { "", "un", "dos", "tres", "cuatro", "cinco", "seis", "siete", "ocho", "nueve" };
        private static string[] _decena1 = { "", "once", "doce", "trece", "catorce", "quince", "dieciseis", "diecisiete", "dieciocho", "diecinueve" };
        private static string[] _decenas = { "", "diez", "veinte", "treinta", "cuarenta", "cincuenta", "sesenta", "setenta", "ochenta", "noventa" };
        private static string[] _centenas = { "", "cien", "doscientos", "trescientos", "cuatrocientos", "quinientos", "seiscientos", "setecientos", "ochocientos", "novecientos" };

        public static string MillarATexto(int n)
        {
            if (n == 0)
                return "";

            int centenas = n / 100;
            n = n % 100;
            int decenas = n / 10;
            int unidades = n % 10;

            string sufijo = "";

            if (decenas == 0 && unidades != 0)
                sufijo = _unidades[unidades];
            if (decenas == 1 && unidades != 0)
                sufijo = _decena1[unidades];
            if (decenas == 2 && unidades != 0)
                sufijo = "veinti" + _unidades[unidades];
            if (unidades == 0)
                sufijo = _decenas[decenas];
            if (decenas > 2 && unidades != 0)
                sufijo = _decenas[decenas] + " y " + _unidades[unidades];
            if (centenas != 1)
                return _centenas[centenas] + " " + sufijo;
            if (unidades == 0 && decenas == 0)
                return "cien";

            return "ciento " + sufijo;
        }

        public static string NumeroACastellano(decimal n)
        {
            string resultado = "";
            int grupo = 0;
            while (n != 0 && grupo < _grupos.Length)
            {
                decimal fragmento = n % 1000000;
                int millarAlto = (int)(fragmento / 1000);
                int millarBajo = (int)(fragmento % 1000);
                n = n / 1000000;

                string nombreGrupo = _grupos[grupo];
                if (fragmento > 1 && grupo > 0)
                    nombreGrupo += "es";

                if ((millarAlto != 0) || (millarBajo != 0))
                {
                    if (millarAlto > 1)
                        resultado = MillarATexto(millarAlto) + " mil " +
                        MillarATexto(millarBajo) + " " +
                        nombreGrupo + " " + resultado;
                    if (millarAlto == 0)
                        resultado = MillarATexto(millarBajo) + " " +
                        nombreGrupo + " " + resultado;
                    if (millarAlto == 1)
                        resultado = "mil " + MillarATexto(millarBajo) + " " +
                        nombreGrupo + " " + resultado;
                }
                grupo++;
            }
            resultado = resultado.ToUpper().Trim();
            return resultado;
        }

        public static string ConversionFinal(decimal n)
        {
            n = Math.Round(n);
            decimal centavos = n;
            int valorADividir = (int)centavos;
            decimal residuo = centavos % valorADividir;
            residuo = residuo * 100;
            int valor2Dividir = (int)residuo;
            decimal aproximacion = 0;
            if (residuo != 0)
            {
                aproximacion = residuo % valor2Dividir;
            }
            int total = (int)residuo;
            if (aproximacion * 10 >= 5)
            {
                total++;
            }
            string antes = NumeroACastellano(n) + " PESOS";
            string despues = NumeroACastellano(total) + " CENTAVOS";
            string resultado = antes;
            if (!despues.Equals(" CENTAVOS"))
            {
                resultado = resultado + " CON " + despues;
            }
            return resultado + " M/C";
        }
        #endregion

        #region Facturacion atomica

        /// <summary>
        /// 
        /// </summary>
        /// <param name="totalBase"></param>
        /// <param name="iva"></param>
        /// <param name="valorEnLetras"></param>
        /// <param name="direccionCliente"></param>
        /// <param name="telefonoCliente"></param>
        /// <param name="faxCliente"></param>
        /// <param name="idCiudad"></param>
        /// <param name="idPrograma"></param>
        /// <param name="guidCliente"></param>
        /// <returns></returns>
        /// 

        public static bool SaveFacturaAndDetalles(int idUsuario, string direccionCliente, string telefonoCliente, string faxCliente, int idCiudad, int idPrograma, string guidCliente, bool IsNotaCredito, int? numeroFactura, string motivoNotaCredito, DateTime? fechaVencimiento, string observacion)
        {
            try
            {
                CacheDetallesFacturaCollection cacheCollection = new CacheDetallesFacturaCollection();
                cacheCollection.Where(CacheDetallesFactura.Columns.IdUsuario, idUsuario).Load();
                decimal totalBase = 0;
                decimal iva = 0;
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    BodegasCollection bodegasColl = new BodegasCollection();
                    bodegasColl.Where(Bodegas.Columns.IdPrograma, idPrograma);
                    bodegasColl.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]).Load();
                    int cantidadEnBodega = 0;
                    foreach (Bodegas bod in bodegasColl)
                    {
                        BodegasProductosCollection disponibilidad = new BodegasProductosCollection();
                        disponibilidad.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidad.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                        disponibilidad.Load();
                        foreach (BodegasProductos bp in disponibilidad)
                        {
                            cantidadEnBodega += Convert.ToInt32(bp.Cantidad);
                        }
                    }

                    totalBase += (cache.ValorBase + cache.EnvioBase) * cache.Cantidad;
                    iva += cache.Iva * cache.Cantidad;
                }
                string valorEnLetras = "";
                try
                {
                    valorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch { }
                Facturas fact = new Facturas();
                fact.GuidFactura = System.Guid.NewGuid().ToString();
                Programas programa = new Programas(idPrograma);
                if (!programa.IsLoaded)
                    throw new Exception("No se pudo cargar el programa");

                if (!programa.IsLoaded)
                    throw new Exception("No se pudo cargar la resolución");
                if (IsNotaCredito)
                {
                    fact.NumeroFactura = GetConsecutivoNotasCredito(idPrograma).ValorActual;
                }
                else
                {
                    /* Resoluciones resolucionFactura = new Resoluciones(programa.IdResolucion);
                     fact.NumeroFactura = resolucionFactura.ValorActual;
                     resolucionFactura.ValorActual = resolucionFactura.ValorActual + 1;
                     resolucionFactura.Save();*/
                }

                fact.TotalBase = totalBase;
                fact.Iva = iva;
                fact.Prefacturada = false;
                fact.Anulada = false;
                fact.GeneradaEmpresa = false;
                fact.GeneradaEstablecimiento = false;
                DateTime fechaDeHoy = DateTime.Now;
                fact.FechaTransaccion = fechaDeHoy;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    fact.FechaFactura = DateTime.Now;
                }
                else
                {

                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    fact.FechaFactura = newDate;
                }
                fact.FechaCompra = fechaDeHoy;
                fact.FechaAnulacion = null;
                fact.FechaPrefacturada = null;
                fact.ValorEnLetras = valorEnLetras;
                fact.DireccionCliente = direccionCliente;
                fact.TelefonoCliente = telefonoCliente;
                fact.FaxCliente = faxCliente;
                fact.IdCiudadCliente = idCiudad;
                fact.IdPrograma = idPrograma;
                fact.GuidCliente = guidCliente;
                fact.NotaCredito = IsNotaCredito;
                fact.Siniestro = false;
                fact.FacturaNotaCredito = numeroFactura;
                fact.MotivoNotaCredito = motivoNotaCredito;
                if (!string.IsNullOrEmpty(Convert.ToString(fechaVencimiento)))
                {
                    DateTime fecha = Convert.ToDateTime(fechaVencimiento);
                    fact.FechaVencimiento = fecha;
                }
                fact.Observaciones = observacion;
                fact.Save();
                int numeroItem = 1;
                bool bandera = true;
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    DetallesFactura detalleFact = new DetallesFactura();
                    detalleFact.GuidDetalleFactura = System.Guid.NewGuid().ToString();
                    detalleFact.NumeroItem = numeroItem;
                    numeroItem++;
                    detalleFact.Cantidad = cache.Cantidad;
                    detalleFact.ValorBase = cache.ValorBase;
                    detalleFact.EnvioBase = cache.EnvioBase;
                    detalleFact.Iva = cache.Iva;
                    detalleFact.GuidReferenciaProducto = cache.GuidReferenciaProducto;
                    detalleFact.GuidFactura = fact.GuidFactura;
                    detalleFact.FactorCompraPuntos = cache.FactorCompraPuntos;
                    detalleFact.DescargoEnInventario = false;
                    detalleFact.Save();
                    if (detalleFact.FactorCompraPuntos != 0)
                        bandera = false;

                    if (IsNotaCredito)
                    {
                        // Descuento de bodega real si es nota credito
                        if (detalleFact.FactorCompraPuntos == 0)
                        {
                            int cantidadACargar = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescarga = new BodegasCollection();
                            bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                            foreach (Bodegas bod in bodegasCollDescarga)
                            {
                                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescarga.Load();
                                foreach (BodegasProductos bp in disponibilidadDescarga)
                                {
                                    if (detalleFact.DescargoEnInventario.HasValue)
                                    {
                                        decimal res = 0;
                                        if (IsNotaCredito)
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, (cantidadACargar), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto nota credito", null, true, true, -1);
                                        else
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                        cantidadACargar = 0;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //Descarga para bodegas fisicas y si es 100% dinero
                        decimal factorDetalle = 0;
                        decimal factorPunto = InventariosController.GetFactorPunto(detalleFact.GuidDetalleFactura);
                        if (factorPunto == factorDetalle)
                        {
                            // Descarga de la bodegas de saldo si hay existencias

                            int cantidadACargarSaldos = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescargaSaldos = new BodegasCollection();
                            bodegasCollDescargaSaldos.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescargaSaldos.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaSaldos"]);
                            bodegasCollDescargaSaldos.Load();

                            foreach (Bodegas bod in bodegasCollDescargaSaldos)
                            {
                                BodegasProductosCollection disponibilidadDescargaSaldos = new BodegasProductosCollection();
                                disponibilidadDescargaSaldos.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescargaSaldos.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescargaSaldos.Load();
                                foreach (BodegasProductos bp in disponibilidadDescargaSaldos)
                                {
                                    if (cantidadACargarSaldos > 0)
                                    {
                                        decimal res = 0;
                                        if (cantidadACargarSaldos > bp.Cantidad)
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargarSaldos -= Convert.ToInt32(bp.Cantidad);
                                        }
                                        else
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargarSaldos, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargarSaldos = 0;
                                        }
                                    }
                                }
                            }
                            //descargo de bodegas fisicas
                            int cantidadACargar = cantidadACargarSaldos;
                            BodegasCollection bodegasCollDescarga = new BodegasCollection();
                            bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                            foreach (Bodegas bod in bodegasCollDescarga)
                            {
                                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescarga.Load();

                                if (disponibilidadDescarga.Count == 0)
                                {
                                    BodegasProductos bdp = new BodegasProductos();
                                    bdp.IdBodega = bod.IdBodega;
                                    bdp.GuidReferenciaProducto = cache.GuidReferenciaProducto;
                                    bdp.Cantidad = 0;
                                    bdp.Save();

                                    disponibilidadDescarga = new BodegasProductosCollection();
                                    disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                    disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                    disponibilidadDescarga.Load();


                                }
                                foreach (BodegasProductos bp in disponibilidadDescarga)
                                {
                                    decimal res = 0;
                                    if (!IsNotaCredito)
                                    {
                                        res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                    }
                                    else
                                    {
                                        if (IsNotaCredito)
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, (0 - cantidadACargar), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                        else
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                        cantidadACargar = 0;
                                    }
                                }
                            }

                            //descarga de boedgas virtuales si es mayor que 0 la cantidad
                            cantidadACargar = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescargaVirutal = new BodegasCollection();
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                            bodegasCollDescargaVirutal.Load();

                            foreach (Bodegas bod in bodegasCollDescargaVirutal)
                            {
                                BodegasProductosCollection disponibilidadDescargaVirutal = new BodegasProductosCollection();
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescargaVirutal.Load();
                                foreach (BodegasProductos bp in disponibilidadDescargaVirutal)
                                {
                                    if (cantidadACargar > 0)
                                    {
                                        decimal res = 0;
                                        if (cantidadACargar > bp.Cantidad)
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                                        }
                                        else
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar = 0;
                                        }
                                    }
                                }
                            }

                            // Se marca como descargado el detalle
                            DetallesFactura detalle = new DetallesFactura(DetallesFactura.Columns.GuidDetalleFactura, detalleFact.GuidDetalleFactura);
                            if (!detalle.IsNew)
                            {
                                detalle.DescargoEnInventario = true;
                                detalle.Save();
                            }

                        }
                        else
                        {
                            // Descargamos solo de virtual si la cantidad esta disponible.
                            int cantidadACargar = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescargaVirutal = new BodegasCollection();
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                            bodegasCollDescargaVirutal.Load();
                            foreach (Bodegas bod in bodegasCollDescargaVirutal)
                            {
                                BodegasProductosCollection disponibilidadDescargaVirutal = new BodegasProductosCollection();
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescargaVirutal.Load();
                                foreach (BodegasProductos bp in disponibilidadDescargaVirutal)
                                {
                                    if (cantidadACargar > 0)
                                    {
                                        decimal res = 0;
                                        if (cantidadACargar > bp.Cantidad)
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                                        }
                                        else
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar = 0;
                                        }
                                    }
                                }
                            }
                        }

                        SaveConsecutivoFactura(true, fact.GuidFactura, programa.NombrePrograma);
                    }

                }
                if (bandera)
                {
                    fact.FechaCompra = fechaDeHoy;
                    fact.Save();
                }

                CacheDetallesFactura.Delete(CacheDetallesFactura.Columns.IdUsuario, idUsuario);
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }



        public static DataSet VerificarFactura(int idPrograma, int numeroFactura)
        {
            DataSet datos = SPs.SelectFacturasPrograma(numeroFactura, idPrograma).GetDataSet();
            return datos;
        }

        public static DataTable VerificarFacturaProveedorPuntos(int idPrograma, int numeroFactura)
        {
            DataTable datos = SPs.SelectFacturasProgramaProveedor(numeroFactura, idPrograma).GetDataSet().Tables[0];
            return datos;
        }

        public static DataTable GetDetallesFactura(string guidFactura)
        {
            DataTable datos = SPs.SelectDetalleFacturaByFactura(guidFactura).GetDataSet().Tables[0];
            return datos;
        }

        public static DataTable GetDetallesFacturaProveedorPuntos(string guidFactura)
        {
            DataTable datos = SPs.SelectDetalleFacturaByFacturaBanco(guidFactura).GetDataSet().Tables[0];
            return datos;
        }

        public static string GetProductoByReferencia(string referenciaProducto)
        {
            object datos = SPs.SelectProductoByReferencia(referenciaProducto).ExecuteScalar();
            if (datos != null)
                return datos.ToString();
            else
                return null;
        }

        public static DataSet GetFacturasByCriterias(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, bool? anulada, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetFacturasByCriteria(idPrograma, numeroFactura, fechaInicial, fechaFinal, cedulaCliente, anulada, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static DataSet GetFacturasEmpresasByCriterias(int? idEmpresa, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetFacturasEmpresaByCriteria(idEmpresa, numeroFactura, fechaInicial, fechaFinal, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasEmpresasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetFacturasAAnular(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetFacturasAAnular(idPrograma, numeroFactura, fechaInicial, fechaFinal, cedulaCliente, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetFacturasAnularEmpresas(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetFacturasAAnularEmpresa(idPrograma, numeroFactura, fechaInicial, fechaFinal, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasAnularEmpresas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DetallesFacturaCollection GetDetallesByGuidFactura(string guidFactura)
        {
            try
            {
                DetallesFacturaCollection detalles = new DetallesFacturaCollection();
                return detalles.Where(DetallesFactura.Columns.GuidFactura, guidFactura).OrderByAsc(DetallesFactura.Columns.NumeroItem).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesByGuidFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DetallesFacturaProveedorPuntosCollection GetDetallesByGuidFacturaEmpresa(string guidFactura)
        {
            try
            {
                DetallesFacturaProveedorPuntosCollection detalles = new DetallesFacturaProveedorPuntosCollection();
                return detalles.Where(DetallesFacturaProveedorPuntos.Columns.GuidFacturaProveedorPuntos, guidFactura).OrderByAsc(DetallesFacturaProveedorPuntos.Columns.Cantidad).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesByGuidFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static DetallesFacturaProveedorPuntosCollection GetDetallesProveedorPuntosByGuidFactura(string guidFactura)
        {
            try
            {
                DetallesFacturaProveedorPuntosCollection detalles = new DetallesFacturaProveedorPuntosCollection();
                return detalles.Where(DetallesFacturaProveedorPuntos.Columns.GuidFacturaProveedorPuntos, guidFactura).OrderByAsc(DetallesFacturaProveedorPuntos.Columns.GuidProducto).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesByGuidFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static DataSet GetFacturas(string cedula, int? consecutivoFactura, DateTime? fechaInicial, DateTime? fechaFinal, string nombrePrograma, int? idMarca, int pageIndex, int pageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                Programas prog = ProgramsController.GetProgramaByNombre(nombrePrograma);
                //if (prog.IsLoaded)
                //    throw new Exception("El programa no existe.");
                if (cedula.Equals(""))
                    cedula = null;
                SubSonic.StoredProcedure sp = SPs.GetFacturasCatalog(prog.IdPrograma, idMarca, consecutivoFactura, fechaInicial, fechaFinal, cedula, TotalRecords);
                //SubSonic.StoredProcedure sp = SPs.GetFacturasCatalog(null, 0, null, null, null, null, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = pageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetNotasCredito(string cedula, int? consecutivoNotaCredito, DateTime? fechaInicial, DateTime? fechaFinal, string nombrePrograma, int idMarca, int pageIndex, int pageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                Programas prog = ProgramsController.GetProgramaByNombre(nombrePrograma);
                if (cedula.Equals(""))
                    cedula = null;
                SubSonic.StoredProcedure sp = SPs.GetNotasCreditoCatalog(prog.IdPrograma, idMarca, consecutivoNotaCredito, fechaInicial, fechaFinal, cedula, TotalRecords);
                //SubSonic.StoredProcedure sp = SPs.GetNotasCreditoCatalog(null, 0, null, null, null, null, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = pageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesByGuidFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        #endregion

        #region Anulacion de facturas

        /// <summary>
        /// 
        /// </summary>
        /// <param name="guidFactura"></param>
        /// <returns></returns>
        public static bool AnularFactura(string guidFactura)
        {
            try
            {
                Facturas facts = new Facturas(guidFactura);
                if (facts.IsLoaded)
                {
                    facts.Anulada = true;
                    facts.FechaAnulacion = DateTime.Now;
                    facts.Save();
                    //if (facts.Prefacturada)
                    //{
                    //    return GenerateNotaCredito(facts);
                    //}
                    return GenerateNotaCredito(facts);
                }
                else
                    throw new Exception("Una factura que no existe esta tratando de ser anulada");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AnularFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool AnularFacturaEmpresa(string guidFactura)
        {
            try
            {
                FacturasProveedorPuntos facts = new FacturasProveedorPuntos(guidFactura);
                if (facts.IsLoaded)
                {
                    facts.Anulada = true;
                    facts.FechaAnulacion = DateTime.Now;
                    facts.Save();
                    //if (facts.Prefacturada)
                    //{
                    //    return GenerateNotaCredito(facts);
                    //}
                    return GenerateNotaCreditoEmpresa(facts);
                }
                else
                    throw new Exception("Una factura que no existe esta tratando de ser anulada");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AnularFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool CrearNotaCreditoValorCobrado(string direccionCliente, string telefonoCliente, string faxCliente
                                                       , int idCiudadCliente, int idPrograma, string guidCliente, decimal totalBase, string numeroFactura, string motivoNota)
        {
            try
            {
                decimal iva = decimal.Parse(ConfigurationManager.AppSettings["Iva"]) * totalBase;
                Facturas notaCredito = new Facturas();
                notaCredito.GuidFactura = System.Guid.NewGuid().ToString();
                notaCredito.Prefacturada = false;
                notaCredito.TotalBase = totalBase;
                notaCredito.DireccionCliente = direccionCliente;
                notaCredito.TelefonoCliente = telefonoCliente;
                notaCredito.FaxCliente = faxCliente;
                notaCredito.IdCiudadCliente = idCiudadCliente;
                notaCredito.IdPrograma = idPrograma;
                notaCredito.GuidCliente = guidCliente;
                notaCredito.IdPrograma = idPrograma;
                notaCredito.GeneradaEstablecimiento = false;
                notaCredito.FechaPrefacturada = null;
                notaCredito.Anulada = false;
                notaCredito.FechaAnulacion = null;
                notaCredito.GeneradaEmpresa = false;
                notaCredito.FechaGeneracionEmpresa = null;
                notaCredito.FechaFactura = DateTime.Now;
                notaCredito.NotaCredito = true;
                decimal valorTotal = totalBase + iva;
                try
                {
                    notaCredito.ValorEnLetras = ConversionFinal(valorTotal);
                }
                catch
                {
                    notaCredito.ValorEnLetras = "";
                }
                notaCredito.Iva = iva;
                notaCredito.MotivoNotaCredito = motivoNota;
                notaCredito.NumeroFactura = GetConsecutivoNotasCredito(idPrograma).ValorActual;
                notaCredito.FacturaNotaCredito = int.Parse(numeroFactura);
                notaCredito.BitNotaCreditoValor = true;
                notaCredito.Save();
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CrearNotaCreditoValorCobrado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool CrearNotaCreditoValorCobradoEmpresa(int idEmpresa, int idprograma, decimal totalBase, string motivoNotacredito, int numeroFactura)
        {
            try
            {
                decimal iva = decimal.Parse(ConfigurationManager.AppSettings["Iva"]) * totalBase;
                FacturasProveedorPuntos notaCredito = new FacturasProveedorPuntos();
                notaCredito.GuidFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                notaCredito.TotalBase = totalBase;
                notaCredito.IdEmpresa = idEmpresa;
                notaCredito.IdPrograma = idprograma;
                notaCredito.Anulada = false;
                notaCredito.FechaAnulacion = null;
                notaCredito.FechaFactura = DateTime.Now;
                notaCredito.NotaCredito = true;
                notaCredito.Iva = Convert.ToDecimal(iva);
                notaCredito.MotivoNotaCredito = motivoNotacredito;
                notaCredito.FacturaNotaCredito = numeroFactura;
                notaCredito.NumeroFactura = GetConsecutivoNotasCredito(idprograma).ValorActual;
                notaCredito.BitNotaCreditoValor = true;
                decimal total = totalBase + iva;
                try
                {
                    notaCredito.ValorEnLetras = ConversionFinal(total);
                }
                catch
                {
                    notaCredito.ValorEnLetras = "";
                }
                notaCredito.Save();
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo CrearNotaCreditoValorCobrado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool IsGuidTransaccionDisposed(string guidTransaccion)
        {
            try
            {
                Facturas fact = new Facturas(guidTransaccion);
                return !fact.Anulada;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo IsGuidTransaccionDisposed() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return true;
            }
        }

        public static DataSet GetNotasCreditoByCriteria(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, string cedulaCliente, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetNotaCreditoByCriteria(idPrograma, numeroFactura, fechaInicial, fechaFinal, cedulaCliente, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetNotasCreditoEmpresaByCriteria(int? idPrograma, int? numeroFactura, DateTime? fechaInicial, DateTime? fechaFinal, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetNotaCreditoEmpresaByCriteria(idPrograma, numeroFactura, fechaInicial, fechaFinal, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetNotasCreditoEmpresaByCriteria() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable GetFacturasBetweenRangos(int idPrograma, int valorInicial, int valorFinal)
        {
            try
            {
                return SPs.GetFacturasBetweenRangos(idPrograma, valorInicial, valorFinal).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasBetweenRangos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static int GetCantidadReferenciasEnInventario(string guidProducto, string nombrePrograma)
        {
            try
            {
                Programas prog = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!prog.IsLoaded)
                    throw new Exception("No es posible consultar el inventario de un programa que no existe");
                BodegasCollection bocCol = new BodegasCollection();
                bocCol.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                bocCol.Where(Bodegas.Columns.IdPrograma, prog.IdPrograma).Load();
                int cantidadBodega = 0;
                foreach (Bodegas bod in bocCol)
                {
                    BodegasProductosCollection bodProgCol = new BodegasProductosCollection();
                    bodProgCol.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega).Where(BodegasProductos.Columns.GuidReferenciaProducto, guidProducto).Load();
                    foreach (BodegasProductos bodprod in bodProgCol)
                    {
                        cantidadBodega += Convert.ToInt32(bodprod.Cantidad);
                    }
                }
                return cantidadBodega;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetCantidadReferenciasEnInventario() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return -1;
            }
        }

        #endregion

        #region FactProveedorPuntos

        public static bool SavePeriodoFacturacionPuntos(int idPrograma, DateTime fechaInicial, DateTime fechaFinal, bool marcarUltimoPeriodo)
        {
            try
            {
                FacturasCollection fc = new FacturasCollection();
                fc.Where(Facturas.Columns.IdPrograma, idPrograma);
                fc.Where(Facturas.Columns.Siniestro, false);
                fc.Where(Facturas.Columns.GeneradaEmpresa, false);
                fc.BetweenAnd(Facturas.Columns.FechaFactura, fechaInicial, fechaFinal);
                fc.Load();
                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                DateTime fechaGeneracion = DateTime.Now;
                PeriodoFacturacionPuntos pfp = new PeriodoFacturacionPuntos();
                pfp.IdPrograma = idPrograma;
                pfp.FechaInicio = fechaInicial;
                pfp.FechaFin = fechaFinal;
                pfp.PeriodoActual = marcarUltimoPeriodo;
                coll.Add(pfp.GetSaveCommand());
                //pfp.Save();
                foreach (Facturas f in fc)
                {
                    f.GeneradaEmpresa = true;
                    f.FechaGeneracionEmpresa = fechaGeneracion;
                    coll.Add(f.GetUpdateCommand(Environment.UserName));
                    //f.Save();
                }
                SubSonic.DataService.ExecuteTransaction(coll);
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SavePeriodoFacturacionPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool SavePeriodoFacturacionPuntos(int idPrograma, DateTime fechaInicial, DateTime fechaFinal, bool? marcarUltimoPeriodo, bool esPeriodoBanco)
        {
            try
            {
                FacturasCollection fc = new FacturasCollection();
                fc.Where(Facturas.Columns.IdPrograma, idPrograma);
                fc.Where(Facturas.Columns.Siniestro, false);
                fc.Where(Facturas.Columns.GeneradaEmpresa, false);
                fc.BetweenAnd(Facturas.Columns.FechaFactura, fechaInicial, fechaFinal);
                fc.Load();
                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                DateTime fechaGeneracion = DateTime.Now;
                PeriodoFacturacionPuntos pfp = new PeriodoFacturacionPuntos();
                pfp.IdPrograma = idPrograma;
                pfp.FechaInicio = fechaInicial;
                pfp.FechaFin = fechaFinal;
                pfp.PeriodoActual = marcarUltimoPeriodo;
                pfp.EsPeriodoBanco = esPeriodoBanco;
                coll.Add(pfp.GetSaveCommand());
                //pfp.Save();
                foreach (Facturas f in fc)
                {
                    f.GeneradaEmpresa = true;
                    f.FechaGeneracionEmpresa = fechaGeneracion;
                    coll.Add(f.GetUpdateCommand(Environment.UserName));
                    //f.Save();
                }
                SubSonic.DataService.ExecuteTransaction(coll);
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SavePeriodoFacturacionPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool SavePeriodoFacturacionPuntos(string nombrePrograma, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                Programas prog = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!prog.IsLoaded)
                    throw new Exception("El programa no existe");

                // preguntamos si el tipo de facturacion es para empresa segun el programa

                FacturasCollection fc = new FacturasCollection();
                fc.Where(Facturas.Columns.IdPrograma, prog.IdPrograma);
                fc.Where(Facturas.Columns.Siniestro, false);
                fc.Where(Facturas.Columns.GeneradaEmpresa, false);
                fc.BetweenAnd(Facturas.Columns.FechaFactura, fechaInicial, fechaFinal);
                fc.Load();
                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                DateTime fechaGeneracion = DateTime.Now;
                PeriodoFacturacionPuntos pfp = new PeriodoFacturacionPuntos();
                pfp.IdPrograma = prog.IdPrograma;
                pfp.FechaInicio = fechaInicial;
                pfp.FechaFin = fechaFinal;
                coll.Add(pfp.GetSaveCommand());
                //pfp.Save();
                foreach (Facturas f in fc)
                {
                    f.GeneradaEmpresa = true;
                    f.FechaGeneracionEmpresa = fechaGeneracion;
                    coll.Add(f.GetUpdateCommand(Environment.UserName));
                    //f.Save();
                }
                SubSonic.DataService.ExecuteTransaction(coll);
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SavePeriodoFacturacionPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool SavePeriodoFacturacionPuntosGenerico(string nombrePrograma, DateTime fechaInicial, DateTime fechaFinal, string tipoPrefactura)
        {
            try
            {
                int idPrograma = 0;
                int idPeriodo = 0;
                Programas prog = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!prog.IsLoaded)
                    throw new Exception("El programa no existe");
                else
                    idPrograma = prog.IdPrograma;

                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                FacturasCollection fc = new FacturasCollection();

                // preguntamos si el tipo de facturacion es para empresa
                if (tipoPrefactura.Equals(ConfigurationManager.AppSettings["TipoFacturacionProveedor"].ToString()))
                {
                    //pregunto si el tipo de facturacion es por fecha de despacho
                    if (prog.IdTipoFacturacion == Convert.ToInt32(ConfigurationManager.AppSettings["FacturacionDespacho"]))
                    {
                        //consulto las facturas que se encuentren dentro del rango por factura de despacho
                        fc.Where(Facturas.Columns.IdPrograma, prog.IdPrograma);
                        fc.Where(Facturas.Columns.Siniestro, false);
                        fc.Where(Facturas.Columns.GeneradaEmpresa, false);
                        fc.BetweenAnd(Facturas.Columns.FechaFactura, fechaInicial, fechaFinal);
                        fc.Load();
                    }
                    else
                    {
                        //pregunto si el tipo de facturacion es por fecha de entrega
                        if (prog.IdTipoFacturacion == Convert.ToInt32(ConfigurationManager.AppSettings["FacturacionEntrega"]))
                        {
                            //consulto las facturas que se encuentren dentro del rango por factura de entrega
                            fc.Where(Facturas.Columns.IdPrograma, prog.IdPrograma);
                            fc.Where(Facturas.Columns.Siniestro, false);
                            fc.Where(Facturas.Columns.GeneradaEmpresa, false);
                            fc.BetweenAnd(Facturas.Columns.FechaEntrega, fechaInicial, fechaFinal);
                            fc.Load();
                        }
                    }

                    DateTime fechaGeneracion = DateTime.Now;
                    PeriodoFacturacionPuntos pfp = new PeriodoFacturacionPuntos();
                    pfp.IdPrograma = prog.IdPrograma;
                    pfp.FechaInicio = fechaInicial;
                    pfp.FechaFin = fechaFinal;
                    pfp.EsPeriodoBanco = true;
                    coll.Add(pfp.GetSaveCommand());
                    foreach (Facturas f in fc)
                    {
                        f.GeneradaEmpresa = true;
                        f.FechaGeneracionEmpresa = fechaGeneracion;
                        coll.Add(f.GetUpdateCommand(Environment.UserName));
                        //f.Save();
                    }
                    SubSonic.DataService.ExecuteTransaction(coll);

                    //JORGE GONZALEZ: Cambio para prefacturar courrier con costos alternos por destino. :S
                    foreach (Facturas f in fc)
                    {
                        try { SPs.CrearCostoEnvioAlternoFactura(prog.IdPrograma, f.GuidFactura).Execute(); }
                        catch (Exception ex)
                        {
                            StringBuilder exception = new StringBuilder();
                            exception.Append("Error: Se presento un error tratando de asignar el costo de envio alterno del periodo de prefacturación para la factura:\n");
                            exception.Append(f.GuidFactura);
                            exception.Append(" en el programa: ");
                            exception.Append(prog.IdPrograma);
                            exception.Append("\n");
                            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                        }

                    }


                    //pregunto si el tipo de facturacion es por fecha de despacho
                    if (prog.IdTipoFacturacion == Convert.ToInt32(ConfigurationManager.AppSettings["FacturacionDespacho"]))
                    {
                        //SI ES EL ULTIMO PERIODO MARCO LA FECHA  PARA EL PROXIMO MES
                        string ultimoPeriodo = FacturacionController.GetUltimoperiodoMes(idPrograma);
                        if (ultimoPeriodo != null)
                        {
                            DateTime fechaUltimoPeriodo = new DateTime(Convert.ToDateTime(ultimoPeriodo).Year, Convert.ToDateTime(ultimoPeriodo).Month, 1);
                            DateTime fechafinal = fechaUltimoPeriodo.AddMonths(1);
                            //DateTime fechaCorteFinal = fechafinal.AddDays(-1);
                            FacturasCollection facturas = new FacturasCollection();
                            facturas.Where(Facturas.Columns.IdPrograma, idPrograma);
                            facturas.Where(Facturas.Columns.FechaFactura, SubSonic.Comparison.GreaterThan, ultimoPeriodo);
                            facturas.Where(Facturas.Columns.FechaFactura, SubSonic.Comparison.LessThan, fechafinal);
                            facturas.Load();

                            foreach (Facturas fac in facturas)
                            {
                                DateTime fechaFactura;
                                if (fac.FechaFactura.HasValue)
                                {
                                    fechaFactura = Convert.ToDateTime(fac.FechaFactura).AddMonths(1);
                                }
                                else
                                {
                                    fechaFactura = DateTime.Now.AddMonths(1);
                                }

                                //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                                DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                                fac.FechaFactura = newDate;
                                fac.Save();
                            }
                        }
                    }
                    else
                    {
                        //pregunto si el tipo de facturacion es por fecha de entrega
                        if (prog.IdTipoFacturacion == Convert.ToInt32(ConfigurationManager.AppSettings["FacturacionEntrega"]))
                        {
                            //SI ES EL ULTIMO PERIODO MARCO LA FECHA DE ENTREGA PARA EL PROXIMO MES
                            string ultimoPeriodo = FacturacionController.GetUltimoperiodoMes(idPrograma);
                            if (ultimoPeriodo != null)
                            {
                                DateTime fechaUltimoPeriodo = new DateTime(Convert.ToDateTime(ultimoPeriodo).Year, Convert.ToDateTime(ultimoPeriodo).Month, 1);
                                DateTime fechafinal = fechaUltimoPeriodo.AddMonths(1);
                                //DateTime fechaCorteFinal = fechafinal.AddDays(-1);
                                FacturasCollection facturas = new FacturasCollection();
                                facturas.Where(Facturas.Columns.IdPrograma, idPrograma);
                                facturas.Where(Facturas.Columns.FechaEntrega, SubSonic.Comparison.GreaterThan, ultimoPeriodo);
                                facturas.Where(Facturas.Columns.FechaEntrega, SubSonic.Comparison.LessThan, fechafinal);
                                facturas.Load();

                                foreach (Facturas fac in facturas)
                                {
                                    DateTime fechaFactura;
                                    if (fac.FechaFactura.HasValue)
                                    {
                                        fechaFactura = Convert.ToDateTime(fac.FechaFactura).AddMonths(1);
                                    }
                                    else
                                    {
                                        fechaFactura = DateTime.Now.AddMonths(1);
                                    }

                                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                                    fac.FechaEntrega = newDate;
                                    fac.Save();
                                }
                            }
                        }
                    }

                    return true;
                }
                else
                {
                    //facturo a establecimiento

                    // preguntamos si el tipo de facturacion es para establecimiento
                    if (tipoPrefactura.Equals(ConfigurationManager.AppSettings["Tipoestablecimiento"].ToString()))
                    {
                        //pregunto si el tipo de facturacion es por fecha de despacho
                        //if (prog.IdTipoFacturacion == Convert.ToInt32(ConfigurationManager.AppSettings["FacturacionDespacho"]))
                        //{
                        //consulto las facturas que se encuentren dentro del rango por factura de despacho
                        fc.Where(Facturas.Columns.IdPrograma, prog.IdPrograma);
                        fc.Where(Facturas.Columns.Siniestro, false);
                        fc.Where(Facturas.Columns.GeneradaEstablecimiento, false);
                        fc.BetweenAnd(Facturas.Columns.FechaFactura, fechaInicial, fechaFinal);
                        fc.Load();
                        //}
                        //else
                        //{
                        //    //pregunto si el tipo de facturacion es por fecha de entrega
                        //    if (prog.IdTipoFacturacion == Convert.ToInt32(ConfigurationManager.AppSettings["FacturacionEntrega"]))
                        //    {
                        //        //consulto las facturas que se encuentren dentro del rango por factura de entrega
                        //        fc.Where(Facturas.Columns.IdPrograma, prog.IdPrograma);
                        //        fc.Where(Facturas.Columns.Siniestro, false);
                        //        fc.Where(Facturas.Columns.GeneradaEstablecimiento, false);
                        //        fc.BetweenAnd(Facturas.Columns.FechaEntrega, fechaInicial, fechaFinal);
                        //        fc.Load();
                        //    }

                        //}

                        DateTime fechaGeneracion = DateTime.Now;
                        PeriodoFacturacionPuntos pfp = new PeriodoFacturacionPuntos();
                        pfp.IdPrograma = prog.IdPrograma;
                        pfp.FechaInicio = fechaInicial;
                        pfp.FechaFin = fechaFinal;
                        pfp.EsPeriodoBanco = false;
                        coll.Add(pfp.GetSaveCommand());
                        idPeriodo = pfp.IdFacturacionPuntos;
                        foreach (Facturas f in fc)
                        {
                            f.GeneradaEstablecimiento = true;
                            f.FechaGeneracionEstablecimiento = fechaGeneracion;
                            coll.Add(f.GetUpdateCommand(Environment.UserName));
                            //f.Save();
                        }

                        SubSonic.DataService.ExecuteTransaction(coll);
                    }


                    return true;
                }


            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SavePeriodoFacturacionPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static PeriodoFacturacionPuntosCollection GetPeriodosFactEmpresaPorPrograma(int idPrograma)
        {
            try
            {
                PeriodoFacturacionPuntosCollection pfpc = new PeriodoFacturacionPuntosCollection();
                pfpc.Where(PeriodoFacturacionPuntos.Columns.IdPrograma, idPrograma);
                return pfpc.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, true).OrderByAsc(PeriodoFacturacionPuntos.Columns.IdFacturacionPuntos).Load();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPeriodosFactEmpresaPorPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static PeriodoFacturacionPuntosCollection GetPeriodosFactEstablecimientoPorPrograma(int idPrograma)
        {
            try
            {
                PeriodoFacturacionPuntosCollection pfpc = new PeriodoFacturacionPuntosCollection();
                pfpc.Where(PeriodoFacturacionPuntos.Columns.IdPrograma, idPrograma);
                return pfpc.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, false).OrderByAsc(PeriodoFacturacionPuntos.Columns.IdFacturacionPuntos).Load();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPeriodosFactEstablecimientoPorPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static PeriodoFacturacionPuntosCollection GetPeriodosFactEmpresaPorPrograma(string nombrePrograma)
        {
            try
            {
                Programas progs = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!progs.IsLoaded)
                    throw new Exception("El programa no existe");
                PeriodoFacturacionPuntosCollection pfpc = new PeriodoFacturacionPuntosCollection();
                //siguiente linea agregada para diferenciar el tipo de periodo a retornar
                pfpc.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, true);
                return pfpc.Where(PeriodoFacturacionPuntos.Columns.IdPrograma, progs.IdPrograma).OrderByAsc(PeriodoFacturacionPuntos.Columns.IdFacturacionPuntos).Load();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPeriodosFactEmpresaPorPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetRegistroTransaccionesPorPeriodo(int idPeriodoFactEmpresas)
        {
            try
            {
                return SPs.ProveedorPuntosGetTransaccionesPeriodo(idPeriodoFactEmpresas).GetDataSet();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetRegistroTransaccionesPorPeriodo() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetRegistroTransaccionesPorPeriodoEstablecimiento(int idMarca, int idPeriodoFactEmpresas)
        {
            try
            {
                return SPs.PrefacturaEstablecimiento(idMarca, idPeriodoFactEmpresas).GetDataSet();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetRegistroTransaccionesPorPeriodoEstablecimiento() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetRegistroTransaccionesPorPeriodoDinero(int idPeriodoFactEmpresas)
        {
            try
            {
                return SPs.ProveedorPuntosGetTransaccionesPeriodoDinero(idPeriodoFactEmpresas).GetDataSet();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetRegistroTransaccionesPorPeriodoDinero() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetDetallesTransacciones(int idPeriodo, string guidProducto, decimal factorCompra)
        {
            try
            {
                return SPs.ProveedorPuntosGetDetallesTransaccion(idPeriodo, guidProducto, factorCompra).GetDataSet();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesTransacciones() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetDetallesTransaccionesEstablecimiento(int idPeriodo, string guidProducto, int marca)
        {
            try
            {
                return SPs.DetallesPrefacturaEstablecimiento(idPeriodo, guidProducto, marca).GetDataSet();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesTransaccionesEstablecimiento() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool ObtenerRegistrosDeFacturasAprobadas(int idPeriodo)
        {
            bool resultado = false;
            try
            {
                FacturasProveedorPuntosCollection fppc = new FacturasProveedorPuntosCollection();
                fppc.Where(FacturasProveedorPuntos.Columns.IdPeriodoFacturacionPuntos, idPeriodo);
                fppc.Where(FacturasProveedorPuntos.Columns.TipoCurrier, false);
                fppc.Load();
                if (fppc.Count > 0)
                {
                    resultado = true;
                }
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ObtenerRegistrosDeFacturasAprobadas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
            return resultado;
        }

        public static bool ObtenerRegistrosDeFacturasAprobadasCurrier(int idPeriodo)
        {
            bool resultado = false;
            try
            {

                FacturasProveedorPuntosCollection fppc = new FacturasProveedorPuntosCollection();
                fppc.Where(FacturasProveedorPuntos.Columns.IdPeriodoFacturacionPuntos, idPeriodo);
                fppc.Where(FacturasProveedorPuntos.Columns.TipoCurrier, true);
                fppc.Load();
                if (fppc.Count > 0)
                {
                    resultado = true;
                }

            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ObtenerRegistrosDeFacturasAprobadas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
            return resultado;
        }

        public static bool ObtenerRegistrosDeFacturasAprobadasEstablecimiento(int idPeriodo)
        {
            try
            {
                FacturasBanco fb = new FacturasBanco(FacturasBanco.Columns.IdPrefactura, idPeriodo);
                return !fb.IsNew;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ObtenerRegistrosDeFacturasAprobadas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }
        public static bool AprobarFacturacionEmpresa(int idPeriodo)
        {
            try
            {
                DataSet dsDetallesTotales = GetRegistroTransaccionesPorPeriodo(idPeriodo);
                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                SubSonic.QueryCommandCollection collFinal = new SubSonic.QueryCommandCollection();
                DataTable dsDetalles = dsDetallesTotales.Tables[0];
                if (dsDetalles.Rows.Count > 0)
                {
                    PeriodoFacturacionPuntos per = new PeriodoFacturacionPuntos(idPeriodo);
                    Programas prog = new Programas(per.IdPrograma);
                    string guidFactura = System.Guid.NewGuid().ToString();
                    decimal totalBase = 0;
                    decimal iva = 0;
                    foreach (DataRow dr in dsDetalles.Rows)
                    {
                        DetallesFacturaProveedorPuntos dfpp = new DetallesFacturaProveedorPuntos();
                        dfpp.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                        dfpp.Cantidad = Convert.ToInt32(dr["CANTIDAD_TOTAL"]);
                        dfpp.ValorBasePuntosTotal = Convert.ToDecimal(dr["VALOR_BASE_UNITARIO"]);
                        totalBase += dfpp.ValorBasePuntosTotal * dfpp.Cantidad;
                        dfpp.IvaTotal = Convert.ToDecimal(dr["IVA_UNITARIO"]);
                        iva += dfpp.IvaTotal * dfpp.Cantidad;
                        dfpp.GuidFacturaProveedorPuntos = guidFactura;
                        dfpp.GuidProducto = dr["GUID_PRODUCTO"].ToString();
                        coll.Add(dfpp.GetSaveCommand());
                    }
                    Resoluciones res = new Resoluciones(prog.IdResolucion);
                    int valorActual = res.ValorActual;
                    res.ValorActual = valorActual + 1;
                    coll.Add(res.GetUpdateCommand(Environment.UserName));
                    FacturasProveedorPuntos fpp = new FacturasProveedorPuntos();
                    fpp.GuidFacturaProveedorPuntos = guidFactura;
                    fpp.NumeroFactura = valorActual;
                    fpp.TotalBase = totalBase;
                    fpp.Iva = iva;
                    fpp.FechaFactura = DateTime.Now;
                    fpp.ValorEnLetras = ConversionFinal(totalBase + iva);
                    fpp.IdPrograma = per.IdPrograma;
                    fpp.IdEmpresa = prog.IdEmpresa;
                    fpp.IdPeriodoFacturacionPuntos = idPeriodo;
                    fpp.NotaCreditoProveedorPuntos = false;
                    fpp.NotaCredito = false;
                    collFinal.Add(fpp.GetSaveCommand());
                    collFinal.AddRange(coll);
                }
                coll = new SubSonic.QueryCommandCollection();
                DataTable dsDetallesNC = dsDetallesTotales.Tables[1];
                if (dsDetallesNC.Rows.Count > 0)
                {
                    PeriodoFacturacionPuntos per = new PeriodoFacturacionPuntos(idPeriodo);
                    Programas prog = new Programas(per.IdPrograma);
                    string guidFactura = System.Guid.NewGuid().ToString();
                    decimal totalBase = 0;
                    decimal iva = 0;
                    foreach (DataRow dr in dsDetallesNC.Rows)
                    {
                        DetallesFacturaProveedorPuntos dfpp = new DetallesFacturaProveedorPuntos();
                        dfpp.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                        dfpp.Cantidad = Convert.ToInt32(dr["CANTIDAD_TOTAL"]);
                        dfpp.ValorBasePuntosTotal = Convert.ToDecimal(dr["VALOR_BASE_UNITARIO"]);
                        totalBase += dfpp.ValorBasePuntosTotal * dfpp.Cantidad;
                        dfpp.IvaTotal = Convert.ToDecimal(dr["IVA_UNITARIO"]);
                        iva += dfpp.IvaTotal * dfpp.Cantidad;
                        dfpp.GuidFacturaProveedorPuntos = guidFactura;
                        dfpp.GuidProducto = dr["GUID_PRODUCTO"].ToString();
                        coll.Add(dfpp.GetSaveCommand());
                    }
                    Resoluciones res = new Resoluciones(prog.IdNotaCredito);
                    int valorActual = res.ValorActual;
                    res.ValorActual = valorActual + 1;
                    coll.Add(res.GetUpdateCommand(Environment.UserName));
                    FacturasProveedorPuntos fpp = new FacturasProveedorPuntos();
                    fpp.GuidFacturaProveedorPuntos = guidFactura;
                    fpp.NumeroFactura = valorActual;
                    fpp.TotalBase = totalBase;
                    fpp.Iva = iva;
                    fpp.FechaFactura = DateTime.Now;
                    fpp.ValorEnLetras = ConversionFinal(totalBase + iva);
                    fpp.IdPrograma = per.IdPrograma;
                    fpp.IdEmpresa = prog.IdEmpresa;
                    fpp.IdPeriodoFacturacionPuntos = idPeriodo;
                    fpp.NotaCreditoProveedorPuntos = true;
                    collFinal.Add(fpp.GetSaveCommand());
                    collFinal.AddRange(coll);
                }
                SubSonic.DataService.ExecuteTransaction(collFinal);

                //DESCARGAR LOS DETALLES QUE TIENEN PUNTOS
                PeriodoFacturacionPuntos periodo = new PeriodoFacturacionPuntos(idPeriodo);
                //idPeriodo = FacturacionController.GetUltimoIdPeriodo(idPrograma);
                DataTable dt = FacturacionController.GetFacturasDescargas(idPeriodo, periodo.IdPrograma);

                foreach (DataRow dr in dt.Rows)
                {
                    DetallesFacturaCollection DetalleFactura = new DetallesFacturaCollection();
                    DetalleFactura.Where(DetallesFactura.Columns.GuidFactura, dr["GUID_FACTURA"]);
                    DetalleFactura.Load();
                    foreach (DetallesFactura detalle in DetalleFactura)
                    {
                        //DESCARGO SOLO DE BODEGA REAL
                        detalle.DescargoEnInventario = true;
                        detalle.Save();
                        FacturacionController.DescargarInventarioTransaccionNewPeriodo(periodo.IdPrograma, detalle.GuidReferenciaProducto, detalle.Cantidad, detalle.GuidDetalleFactura);
                    }

                }

                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AprobarFacturacionEmpresa() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool AprobarFacturacionCurrier(int idPeriodo)
        {
            try
            {
                DataSet dsDetallesTotales = GetRegistroPrefacturacion(idPeriodo, ConfigurationManager.AppSettings["TipoCurrier"], "");
                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                SubSonic.QueryCommandCollection collFinal = new SubSonic.QueryCommandCollection();
                DataTable dsDetalles = dsDetallesTotales.Tables[0];
                if (dsDetalles.Rows.Count > 0)
                {
                    PeriodoFacturacionPuntos per = new PeriodoFacturacionPuntos(idPeriodo);
                    Programas prog = new Programas(per.IdPrograma);
                    string guidFactura = System.Guid.NewGuid().ToString();
                    decimal totalBase = 0;
                    decimal iva = 0;
                    foreach (DataRow dr in dsDetalles.Rows)
                    {
                        DetallesFacturaProveedorPuntos dfpp = new DetallesFacturaProveedorPuntos();
                        dfpp.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                        dfpp.Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                        dfpp.ValorBasePuntosTotal = Convert.ToDecimal(dr["Costo"]);
                        //totalBase += dfpp.ValorBasePuntosTotal * dfpp.Cantidad;
                        totalBase += dfpp.ValorBasePuntosTotal;
                        dfpp.IvaTotal = Convert.ToDecimal("0");
                        //iva += dfpp.IvaTotal * dfpp.Cantidad;
                        iva += dfpp.IvaTotal;
                        dfpp.GuidFacturaProveedorPuntos = guidFactura;
                        dfpp.GuidProducto = dr["guidProducto"].ToString();
                        coll.Add(dfpp.GetSaveCommand());
                    }
                    Resoluciones res = new Resoluciones(prog.IdResolucion);
                    int valorActual = res.ValorActual;
                    res.ValorActual = valorActual + 1;
                    coll.Add(res.GetUpdateCommand(Environment.UserName));
                    FacturasProveedorPuntos fpp = new FacturasProveedorPuntos();
                    fpp.GuidFacturaProveedorPuntos = guidFactura;
                    fpp.NumeroFactura = valorActual;
                    fpp.TotalBase = totalBase;
                    fpp.Iva = iva;
                    fpp.FechaFactura = DateTime.Now;
                    fpp.ValorEnLetras = ConversionFinal(totalBase + iva);
                    fpp.IdPrograma = per.IdPrograma;
                    fpp.IdEmpresa = prog.IdEmpresa;
                    fpp.IdPeriodoFacturacionPuntos = idPeriodo;
                    fpp.NotaCredito = false;
                    fpp.NotaCreditoProveedorPuntos = false;
                    fpp.TipoCurrier = true;
                    collFinal.Add(fpp.GetSaveCommand());
                    collFinal.AddRange(coll);
                }
                coll = new SubSonic.QueryCommandCollection();
                //DataTable dsDetallesNC = dsDetallesTotales.Tables[1];
                //if (dsDetallesNC.Rows.Count > 0)
                //{
                //    PeriodoFacturacionPuntos per = new PeriodoFacturacionPuntos(idPeriodo);
                //    Programas prog = new Programas(per.IdPrograma);
                //    string guidFactura = System.Guid.NewGuid().ToString();
                //    decimal totalBase = 0;
                //    decimal iva = 0;
                //    foreach (DataRow dr in dsDetallesNC.Rows)
                //    {
                //        DetallesFacturaProveedorPuntos dfpp = new DetallesFacturaProveedorPuntos();
                //        dfpp.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                //        dfpp.Cantidad = Convert.ToInt32(dr["CANTIDAD_TOTAL"]);
                //        dfpp.ValorBasePuntosTotal = Convert.ToDecimal(dr["VALOR_BASE_UNITARIO"]);
                //        totalBase += dfpp.ValorBasePuntosTotal * dfpp.Cantidad;
                //        dfpp.IvaTotal = Convert.ToDecimal(dr["IVA_UNITARIO"]);
                //        iva += dfpp.IvaTotal * dfpp.Cantidad;
                //        dfpp.GuidFacturaProveedorPuntos = guidFactura;
                //        dfpp.GuidProducto = dr["GUID_PRODUCTO"].ToString();
                //        coll.Add(dfpp.GetSaveCommand());
                //    }
                //    Resoluciones res = new Resoluciones(prog.IdNotaCredito);
                //    int valorActual = res.ValorActual;
                //    res.ValorActual = valorActual + 1;
                //    coll.Add(res.GetUpdateCommand(Environment.UserName));
                //    FacturasProveedorPuntos fpp = new FacturasProveedorPuntos();
                //    fpp.GuidFacturaProveedorPuntos = guidFactura;
                //    fpp.NumeroFactura = valorActual;
                //    fpp.TotalBase = totalBase;
                //    fpp.Iva = iva;
                //    fpp.FechaFactura = DateTime.Now;
                //    fpp.ValorEnLetras = ConversionFinal(totalBase + iva);
                //    fpp.IdPrograma = per.IdPrograma;
                //    fpp.IdEmpresa = prog.IdEmpresa;
                //    fpp.IdPeriodoFacturacionPuntos = idPeriodo;
                //    fpp.NotaCreditoProveedorPuntos = true;
                //    collFinal.Add(fpp.GetSaveCommand());
                //    collFinal.AddRange(coll);
                //}
                SubSonic.DataService.ExecuteTransaction(collFinal);
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AprobarFacturacionEmpresa() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool AprobarFacturacionEstablecimiento(int idMarca, int idPeriodo)
        {
            try
            {
                DataSet dsDetallesTotales = GetRegistroTransaccionesPorPeriodoEstablecimiento(idMarca, idPeriodo);
                SubSonic.QueryCommandCollection coll = new SubSonic.QueryCommandCollection();
                SubSonic.QueryCommandCollection collFinal = new SubSonic.QueryCommandCollection();
                DataTable dsDetalles = dsDetallesTotales.Tables[0];
                if (dsDetalles.Rows.Count > 0)
                {
                    PeriodoFacturacionPuntos per = new PeriodoFacturacionPuntos(idPeriodo);
                    Programas prog = new Programas(per.IdPrograma);
                    string guidFactura = System.Guid.NewGuid().ToString();
                    decimal totalBase = 0;
                    string guidProducto = "";
                    decimal iva = 0;
                    foreach (DataRow dr in dsDetalles.Rows)
                    {
                        DetallesFacturaBanco dfb = new DetallesFacturaBanco();
                        dfb.GuidDetalleFacturaBanco = System.Guid.NewGuid().ToString();
                        dfb.Cantidad = Convert.ToInt32(dr["CANTIDAD"]);
                        dfb.CostoBase = Convert.ToDecimal(dr["COSTO_BASE"]);
                        totalBase += dfb.CostoBase * dfb.Cantidad;
                        dfb.Iva = Convert.ToDecimal(dr["IVA"]);
                        iva += dfb.Iva * dfb.Cantidad;
                        dfb.GuidFacturaBanco = guidFactura;
                        dfb.GuidReferenciaProducto = dr["GUID_REFERENCIA_PRODUCTO"].ToString();
                        guidProducto = dr["GUID_PRODUCTO"].ToString();

                        coll.Add(dfb.GetSaveCommand());
                    }
                    Resoluciones res = new Resoluciones(prog.IdResolucionBanco);
                    int valorActual = res.ValorActual;
                    res.ValorActual = valorActual + 1;
                    coll.Add(res.GetUpdateCommand(Environment.UserName));
                    FacturasBanco fb = new FacturasBanco();
                    fb.GuidFacturaBanco = guidFactura;
                    fb.ConsecutivoFacturaBanco = valorActual;
                    fb.TotalBase = totalBase;
                    fb.Iva = iva;
                    fb.FechaFacturaBanco = DateTime.Now;
                    fb.IdPrograma = per.IdPrograma;
                    Productos pro = new Productos(Productos.Columns.Guid, guidProducto);
                    fb.IdProveedor = pro.IdProveedor;
                    fb.IdPrefactura = idPeriodo;
                    collFinal.Add(fb.GetSaveCommand());
                    collFinal.AddRange(coll);
                }
                SubSonic.DataService.ExecuteTransaction(collFinal);
                return true;
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AprobarFacturacionEstablecimiento() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }


        public static string GetUltimoperiodo(int idPrograma)
        {
            object ultimaFecha = SPs.SelectUltimoPeriodoMes(idPrograma).ExecuteScalar();
            if (ultimaFecha != null)
                return ultimaFecha.ToString();
            else
                return null;
        }


        public static int GetNumeroPeriodosMesProveedor(int idPrograma)
        {
            object numeroPeriodos = SPs.SelectPeriodosMesProveedor(idPrograma).ExecuteScalar();
            return Convert.ToInt32(numeroPeriodos);
        }

        public static int GetNumeroPeriodosMesEstablecimientos(int idPrograma)
        {
            object numeroPeriodos = SPs.SelectPeriodosMesEstablecimientos(idPrograma).ExecuteScalar();
            return Convert.ToInt32(numeroPeriodos);
        }

        public static string GetUltimoperiodoMes(int idPrograma)
        {
            object numeroPeriodos = SPs.SelectPeriodosMesProveedor(idPrograma).ExecuteScalar();
            Programas programas = new Programas(Programas.Columns.IdPrograma, idPrograma);
            if (!programas.IsNew)
            {
                if (Convert.ToInt32(numeroPeriodos) == programas.NumeroPeriodosProveedorPuntos)
                {
                    object ultimaFecha = SPs.SelectPeriodoMesUltimo(idPrograma).ExecuteScalar();
                    if (ultimaFecha != null)
                        return ultimaFecha.ToString();
                    else
                        return null;
                }
                else
                {
                    return null;
                }
            }
            else
            {
                return null;
            }

        }

        public static DataTable GetFechasPeriodo(int idPeriodo, int idPrograma)
        {
            DataTable dt = SPs.SelectFechasPeriodo(idPeriodo, idPrograma).GetDataSet().Tables[0];
            return dt;
        }

        public static DataTable GetFacturasDescargas(int idPeriodo, int idPrograma)
        {
            DataTable dt = SPs.SelectFechasFactura(idPrograma, idPeriodo).GetDataSet().Tables[0];
            return dt;
        }

        #endregion
        #region Prefacturacion


        public static PeriodoFacturacionPuntosCollection GetPeriodosPrefacturacionEstablecimeintos(string nombrePrograma)
        {
            try
            {
                Programas progs = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!progs.IsLoaded)
                    throw new Exception("El programa no existe");
                PeriodoFacturacionPuntosCollection pfpc = new PeriodoFacturacionPuntosCollection();
                //siguiente linea agregada para diferenciar el tipo de periodo a retornar
                pfpc.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, false);
                return pfpc.Where(PeriodoFacturacionPuntos.Columns.IdPrograma, progs.IdPrograma).OrderByAsc(PeriodoFacturacionPuntos.Columns.IdFacturacionPuntos).Load();
            }
            catch (Exception ex)
            {
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPeriodosFactEmpresaPorPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string SaveNotasCreditoAndDetallesEmpresa(string idCollectionCache, int idPrograma, int idEmpresa, string motivo, string numeroFactura, string observacion)
        {
            try
            {
                string[] caches = idCollectionCache.Split(',');
                CacheDetallesFacturaCollection cacheCollection = new CacheDetallesFacturaCollection();
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetallesFactura cdf = new CacheDetallesFactura(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                    cacheCollection.Add(cdf);
                }
                decimal totalBase = 0;
                decimal iva = 0;
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    BodegasCollection bodegasColl = new BodegasCollection();
                    bodegasColl.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                    int cantidadEnBodega = 0;
                    foreach (Bodegas bod in bodegasColl)
                    {
                        BodegasProductosCollection disponibilidad = new BodegasProductosCollection();
                        disponibilidad.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidad.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                        disponibilidad.Load();
                        foreach (BodegasProductos bp in disponibilidad)
                        {
                            cantidadEnBodega += Convert.ToInt32(bp.Cantidad);
                        }
                    }
                    //if (cache.Cantidad > cantidadEnBodega)
                    //    return "";
                    totalBase += (cache.ValorBase + cache.EnvioBase) * cache.Cantidad;
                    iva += cache.Iva * cache.Cantidad;
                }
                string valorEnLetras = "";
                try
                {
                    valorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch { }
                FacturasProveedorPuntos fact = new FacturasProveedorPuntos();
                fact.GuidFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                Programas programa = new Programas(idPrograma);
                if (!programa.IsLoaded)
                    throw new Exception("No se pudo cargar el programa");

                Resoluciones resolucionFactura = new Resoluciones(programa.IdNotaCredito);
                fact.NumeroFactura = resolucionFactura.ValorActual;
                resolucionFactura.ValorActual = resolucionFactura.ValorActual + 1;
                resolucionFactura.Save();
                fact.TotalBase = totalBase;
                fact.Iva = iva;
                fact.ValorEnLetras = valorEnLetras;
                fact.TipoCurrier = false;
                fact.Anulada = false;
                fact.FechaAnulacion = null;
                fact.NotaCreditoProveedorPuntos = false;
                fact.IdPeriodoFacturacionPuntos = null;
                fact.IdEmpresa = idEmpresa;
                fact.IdPrograma = programa.IdPrograma;
                fact.FechaFactura = DateTime.Now;
                fact.Observaciones = observacion;
                fact.NotaCredito = true;
                fact.MotivoNotaCredito = motivo;
                fact.FacturaNotaCredito = int.Parse(numeroFactura);
                fact.Save();
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    DetallesFacturaProveedorPuntos detalleFact = new DetallesFacturaProveedorPuntos();
                    detalleFact.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                    detalleFact.Cantidad = cache.Cantidad;
                    detalleFact.ValorBasePuntosTotal = cache.ValorBase;
                    detalleFact.IvaTotal = cache.Iva;
                    detalleFact.GuidFacturaProveedorPuntos = fact.GuidFacturaProveedorPuntos;
                    detalleFact.GuidProducto = ProductsController.CargarProductoPorGuidReferencia(cache.GuidReferenciaProducto).Guid;
                    detalleFact.Save();

                    //Descarga para bodegas fisicas 

                    int cantidadACargar = detalleFact.Cantidad;
                    BodegasCollection bodegasCollDescarga = new BodegasCollection();
                    bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                    bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();

                    string guidReferencia = ProductsController.GetReferenciaByProducto(detalleFact.GuidProducto);
                    foreach (Bodegas bod in bodegasCollDescarga)
                    {
                        BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                        disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                        disponibilidadDescarga.Load();
                        foreach (BodegasProductos bp in disponibilidadDescarga)
                        {

                            decimal res = InventariosController.SetProductosBodegasEmpresa(bod.IdBodega, guidReferencia, (cantidadACargar), DateTime.Now, detalleFact.GuidDetalleFacturaProveedorPuntos, null, null, "Descarga de producto nota credito", null, true, true, -1);

                        }
                    }

                }
                return fact.GuidFacturaProveedorPuntos;


            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetLatestPrefacturaProveedorPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GetUltimaFechaPrefacturadaPorPrograma(string nombrePrograma, string tipo)
        {
            try
            {
                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }
                PeriodoFacturacionPuntosCollection prCol = new PeriodoFacturacionPuntosCollection();
                if (tipo.Equals("banco"))
                {
                    prCol.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, true);
                }
                else if (tipo.Equals("establecimiento"))
                {
                    prCol.Where(PeriodoFacturacionPuntos.Columns.EsPeriodoBanco, false);
                }

                prCol.Where(PeriodoFacturacionPuntos.Columns.IdPrograma, programa.IdPrograma).OrderByAsc(PeriodoFacturacionPuntos.Columns.FechaFin).Load();
                if (prCol.Count == 0)
                    return programa.FechaInicio.ToString("dd/MM/yyyy");
                else
                {
                    string fecha = "";
                    foreach (PeriodoFacturacionPuntos pr in prCol)
                    {
                        fecha = pr.FechaFin.ToString("dd/MM/yyyy");
                    }
                    return fecha;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetLatestPrefacturaProveedorPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }



        public static DataSet GetRegistroPrefacturacion(int idPeriodoFactEmpresas, string tipoPrefactura, string marca)
        {
            if (tipoPrefactura.Equals(ConfigurationManager.AppSettings["TipoFacturacionProveedor"]))
            {
                try
                {
                    return GetRegistroTransaccionesPorPeriodo(idPeriodoFactEmpresas);

                    //return SPs.ProveedorPuntosGetTransaccionesPeriodo(idPeriodoFactEmpresas).GetDataSet();
                }
                catch (Exception ex)
                {
                    StringBuilder exception = new StringBuilder();
                    exception.Append("Error en el metodo GetRegistroPrefacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                    exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                    Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                    return null;
                }
            }
            else if (tipoPrefactura.Equals(ConfigurationManager.AppSettings["TipoCurrier"]))
            {
                try
                {
                    return SPs.ProveedorPuntosGetTransaccionesPeriodoCurrier(idPeriodoFactEmpresas).GetDataSet();
                }
                catch (Exception ex)
                {
                    StringBuilder exception = new StringBuilder();
                    exception.Append("Error en el metodo GetRegistroPrefacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                    exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                    Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                    return null;
                }
            }
            else if (tipoPrefactura.Equals(ConfigurationManager.AppSettings["TipoEstablecimiento"]))
            {
                try
                {
                    if (marca.Equals("0"))
                    {
                        return GetRegistroTransaccionesPorPeriodoEstablecimiento(0, idPeriodoFactEmpresas);
                    }
                    else
                    {
                        Marcas mr = new Marcas(Marcas.Columns.IdMarca, Convert.ToInt32(marca));
                        if (mr.IsNew)
                        {
                            StringBuilder error = new StringBuilder();
                            error.Append("La Marca ").Append(marca).Append(" no existe");
                            throw new Exception(error.ToString());
                        }
                        return GetRegistroTransaccionesPorPeriodoEstablecimiento(mr.IdMarca, idPeriodoFactEmpresas);
                    }
                }
                catch (Exception ex)
                {
                    StringBuilder exception = new StringBuilder();
                    exception.Append("Error en el metodo GetRegistroPrefacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                    exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                    Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                    return null;
                }

            }
            else { return null; }
        }

        public static DataSet GetDetallesPrefacturacion(int idPeriodo, string guidProducto, decimal factorCompra, string tipo, string marca)
        {
            if (tipo.Equals(ConfigurationManager.AppSettings["TipoFacturacionProveedor"]))
            {
                try
                {
                    return GetDetallesTransacciones(idPeriodo, guidProducto, factorCompra);
                    //return SPs.ProveedorPuntosGetDetallesTransaccion(idPeriodo, guidProducto, factorCompra).GetDataSet();
                }
                catch (Exception ex)
                {
                    StringBuilder exception = new StringBuilder();
                    exception.Append("Error en el metodo GetDetallesTransacciones() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                    exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                    Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                    return null;
                }
            }
            else if (tipo.Equals(ConfigurationManager.AppSettings["TipoCurrier"]))
            {
                try
                {
                    return SPs.ProveedorPuntosGetDetallesTransaccionCurrier(idPeriodo, guidProducto, factorCompra).GetDataSet();
                }
                catch (Exception ex)
                {
                    StringBuilder exception = new StringBuilder();
                    exception.Append("Error en el metodo GetDetallesTransacciones() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                    exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                    Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                    return null;
                }
            }
            else if (tipo.Equals(ConfigurationManager.AppSettings["TipoEstablecimiento"]))
            {

                try
                {
                    return GetDetallesTransaccionesEstablecimiento(idPeriodo, guidProducto, Convert.ToInt32(marca));
                    //return SPs.ProveedorPuntosGetDetallesTransaccion(idPeriodo, guidProducto, factorCompra).GetDataSet();
                }
                catch (Exception ex)
                {
                    StringBuilder exception = new StringBuilder();
                    exception.Append("Error en el metodo GetDetallesTransacciones() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                    exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                    Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                    return null;
                }
            }
            else { return null; }
        }

        public static bool SaveDetalleFacturaGenerico(int numeroItem, int cantidad, decimal valorBase, decimal envioBase, decimal iva, string guidFactura,
             string nombreReferenciaProducto, decimal factorCompraPunto)
        {
            try
            {
                Facturas fact = new Facturas(Facturas.Columns.GuidFactura, guidFactura);
                ReferenciasProducto refs = new ReferenciasProducto(ReferenciasProducto.Columns.GuidReferenciaProducto, nombreReferenciaProducto);
                if (refs.IsNew)
                    throw new Exception("La referencia del producto que está tratando de insertar no existe");
                DetallesFactura detalle = new DetallesFactura();
                detalle.GuidDetalleFactura = System.Guid.NewGuid().ToString();
                detalle.NumeroItem = numeroItem;
                detalle.Cantidad = cantidad;
                detalle.ValorBase = valorBase;
                detalle.EnvioBase = envioBase;
                detalle.Iva = iva;
                detalle.GuidFactura = guidFactura;
                detalle.GuidReferenciaProducto = refs.GuidReferenciaProducto;
                detalle.FactorCompraPuntos = factorCompraPunto;
                detalle.Save();
                DescargarInventarioTransaccionNew(fact.IdPrograma, refs.GuidReferenciaProducto, cantidad, detalle.GuidDetalleFactura);
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveDetalleFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static string SaveFacturaAndClienteGenerico(string nombrePrograma, decimal totalBase, decimal iva,
           string direccionCliente, string telefonoCliente, string faxCliente, string nombresCliente, string apellidosCliente, string cedulaCliente,
           int idCiudad, string fechaCompra)
        {
            try
            {
                string[] dtmCompra = fechaCompra.Split('/');
                DateTime fechaCompraFinal = new DateTime();
                try
                {
                    fechaCompraFinal = new DateTime(Convert.ToInt32(dtmCompra[2]), Convert.ToInt32(dtmCompra[1]), Convert.ToInt32(dtmCompra[0]), Convert.ToInt32(dtmCompra[3]), Convert.ToInt32(dtmCompra[4]), Convert.ToInt32(dtmCompra[5]));
                }
                catch
                {
                    fechaCompraFinal = DateTime.Now;
                }

                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedulaCliente);
                //Verifico si el cliente es nuevo para guardarlo
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Cedula = cedulaCliente;
                    cliente.Nombres = nombresCliente;
                    cliente.Apellidos = apellidosCliente;
                    cliente.Save();
                }

                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }

                Facturas facturaCliente = new Facturas();
                facturaCliente.GuidFactura = System.Guid.NewGuid().ToString();
                facturaCliente.TotalBase = totalBase;
                facturaCliente.Iva = iva;
                try
                {
                    facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch
                {
                    facturaCliente.ValorEnLetras = "";
                }
                //facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                facturaCliente.DireccionCliente = direccionCliente;
                facturaCliente.TelefonoCliente = telefonoCliente;
                facturaCliente.FaxCliente = faxCliente;
                facturaCliente.IdPrograma = programa.IdPrograma;
                facturaCliente.GuidCliente = cliente.GuidCliente;
                facturaCliente.Prefacturada = false;
                facturaCliente.Anulada = false;
                facturaCliente.GeneradaEmpresa = false;
                facturaCliente.GeneradaEstablecimiento = false;
                facturaCliente.FechaTransaccion = DateTime.Now;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    facturaCliente.FechaFactura = DateTime.Now;
                }
                else
                {
                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    facturaCliente.FechaFactura = newDate;
                }

                facturaCliente.IdCiudadCliente = idCiudad;
                facturaCliente.NotaCredito = false;
                facturaCliente.FechaCompra = fechaCompraFinal;

                double dias = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(programa.PlazoVencimientoFactura)))
                    dias = Convert.ToDouble(programa.PlazoVencimientoFactura);
                DateTime fechaVencimiento = DateTime.Now;
                facturaCliente.FechaVencimiento = fechaVencimiento.AddDays(dias);
                facturaCliente.Save();

                StringBuilder retorno = new StringBuilder();
                retorno.Append(facturaCliente.GuidFactura);
                return retorno.ToString();

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFacturaAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }

        }

        public static string SaveFacturaAndClienteGenerico(string nombrePrograma, decimal totalBase, decimal iva,
         string direccionCliente, string telefonoCliente, string faxCliente, string nombresCliente, string apellidosCliente, string cedulaCliente,
         int idCiudad, string fechaCompra, bool? personaJuridica, string nombreContacto)
        {
            try
            {
                string[] dtmCompra = fechaCompra.Split('/');
                DateTime fechaCompraFinal = new DateTime();
                try
                {
                    fechaCompraFinal = new DateTime(Convert.ToInt32(dtmCompra[2]), Convert.ToInt32(dtmCompra[1]), Convert.ToInt32(dtmCompra[0]), Convert.ToInt32(dtmCompra[3]), Convert.ToInt32(dtmCompra[4]), Convert.ToInt32(dtmCompra[5]));
                }
                catch
                {
                    fechaCompraFinal = DateTime.Now;
                }

                Clientes cliente = new Clientes(Clientes.Columns.Cedula, cedulaCliente);
                //Verifico si el cliente es nuevo para guardarlo
                if (cliente.IsNew)
                {
                    cliente.GuidCliente = System.Guid.NewGuid().ToString();
                    cliente.Cedula = cedulaCliente;
                    cliente.Nombres = nombresCliente;
                    cliente.Apellidos = apellidosCliente;
                    cliente.PersonaJuridica = personaJuridica;
                    cliente.NombreContacto = nombreContacto;
                    cliente.Save();
                }

                Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                //Verifico si el programa fue cargado, en ese caso hay una excepcion
                if (programa.IsNew)
                {
                    StringBuilder error = new StringBuilder();
                    error.Append("El programa ").Append(nombrePrograma).Append(" no existe");
                    throw new Exception(error.ToString());
                }

                Facturas facturaCliente = new Facturas();
                facturaCliente.GuidFactura = System.Guid.NewGuid().ToString();
                facturaCliente.TotalBase = totalBase;
                facturaCliente.Iva = iva;
                try
                {
                    facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch
                {
                    facturaCliente.ValorEnLetras = "";
                }
                //facturaCliente.ValorEnLetras = ConversionFinal(totalBase + iva);
                facturaCliente.DireccionCliente = direccionCliente;
                facturaCliente.TelefonoCliente = telefonoCliente;
                facturaCliente.FaxCliente = faxCliente;
                facturaCliente.IdPrograma = programa.IdPrograma;
                facturaCliente.GuidCliente = cliente.GuidCliente;
                facturaCliente.Prefacturada = false;
                facturaCliente.Anulada = false;
                facturaCliente.GeneradaEmpresa = false;
                facturaCliente.GeneradaEstablecimiento = false;
                facturaCliente.FechaTransaccion = DateTime.Now;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    facturaCliente.FechaFactura = DateTime.Now;
                }
                else
                {
                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    facturaCliente.FechaFactura = newDate;
                }

                facturaCliente.IdCiudadCliente = idCiudad;
                facturaCliente.NotaCredito = false;
                facturaCliente.FechaCompra = fechaCompraFinal;

                double dias = 0;
                if (!string.IsNullOrEmpty(Convert.ToString(programa.PlazoVencimientoFactura)))
                    dias = Convert.ToDouble(programa.PlazoVencimientoFactura);
                DateTime fechaVencimiento = DateTime.Now;
                facturaCliente.FechaVencimiento = fechaVencimiento.AddDays(dias);
                facturaCliente.Save();

                StringBuilder retorno = new StringBuilder();
                retorno.Append(facturaCliente.GuidFactura);
                return retorno.ToString();

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFacturaAndCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }

        }
        #endregion

        public static CacheDetallesFacturaCollection getDetallesCacheEmpresas(string idColl)
        {
            string[] caches = idColl.Split(',');
            CacheDetallesFacturaCollection cacheCollection = new CacheDetallesFacturaCollection();
            for (int i = 0; i < caches.Length - 1; i++)
            {
                CacheDetallesFactura cdf = new CacheDetallesFactura(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                cacheCollection.Add(cdf);
            }

            return cacheCollection;
        }

        #region FacturacionEmpresa
        public static string SaveFacturaAndDetallesEmpresa(string idCollectionCache, int idPrograma, int idEmpresa, string observacion)
        {
            try
            {

                string[] caches = idCollectionCache.Split(',');
                CacheDetallesFacturaCollection cacheCollection = new CacheDetallesFacturaCollection();
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetallesFactura cdf = new CacheDetallesFactura(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                    cacheCollection.Add(cdf);
                }
                decimal totalBase = 0;
                decimal iva = 0;
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    BodegasCollection bodegasColl = new BodegasCollection();
                    bodegasColl.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                    int cantidadEnBodega = 0;
                    foreach (Bodegas bod in bodegasColl)
                    {
                        BodegasProductosCollection disponibilidad = new BodegasProductosCollection();
                        disponibilidad.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidad.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                        disponibilidad.Load();
                        foreach (BodegasProductos bp in disponibilidad)
                        {
                            cantidadEnBodega += Convert.ToInt32(bp.Cantidad);
                        }
                    }
                    //if (cache.Cantidad > cantidadEnBodega)
                    //    return "";
                    totalBase += (cache.ValorBase + cache.EnvioBase) * cache.Cantidad;
                    iva += cache.Iva * cache.Cantidad;
                }
                string valorEnLetras = "";
                try
                {
                    valorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch { }
                FacturasProveedorPuntos fact = new FacturasProveedorPuntos();
                fact.GuidFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                Programas programa = new Programas(idPrograma);
                if (!programa.IsLoaded)
                    throw new Exception("No se pudo cargar el programa");

                /* Resoluciones resolucionFactura = new Resoluciones(programa.IdResolucion);
                 fact.NumeroFactura = resolucionFactura.ValorActual;
                 resolucionFactura.ValorActual = resolucionFactura.ValorActual + 1;
                 resolucionFactura.Save();*/
                fact.TotalBase = totalBase;
                fact.Iva = iva;
                fact.ValorEnLetras = valorEnLetras;
                fact.TipoCurrier = false;
                fact.Anulada = false;
                fact.FechaAnulacion = null;
                fact.NotaCreditoProveedorPuntos = false;
                fact.NotaCredito = false;
                fact.IdPeriodoFacturacionPuntos = null;
                fact.IdEmpresa = idEmpresa;
                fact.IdPrograma = programa.IdPrograma;
                fact.FechaFactura = DateTime.Now;
                fact.Observaciones = observacion;
                fact.Save();
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    DetallesFacturaProveedorPuntos detalleFact = new DetallesFacturaProveedorPuntos();
                    detalleFact.GuidDetalleFacturaProveedorPuntos = System.Guid.NewGuid().ToString();
                    detalleFact.Cantidad = cache.Cantidad;
                    detalleFact.ValorBasePuntosTotal = cache.ValorBase + cache.EnvioBase;
                    detalleFact.IvaTotal = cache.Iva;
                    detalleFact.GuidFacturaProveedorPuntos = fact.GuidFacturaProveedorPuntos;
                    detalleFact.GuidProducto = ProductsController.CargarProductoPorGuidReferencia(cache.GuidReferenciaProducto).Guid;
                    detalleFact.Save();

                    //Descarga para bodegas fisicas 

                    int cantidadACargar = detalleFact.Cantidad;
                    BodegasCollection bodegasCollDescarga = new BodegasCollection();
                    bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                    bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                    foreach (Bodegas bod in bodegasCollDescarga)
                    {
                        BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                        disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                        disponibilidadDescarga.Load();
                        if (disponibilidadDescarga.Count == 0)
                        {
                            BodegasProductos bdp = new BodegasProductos();
                            bdp.IdBodega = bod.IdBodega;
                            bdp.GuidReferenciaProducto = cache.GuidReferenciaProducto;
                            bdp.Cantidad = 0;
                            bdp.Save();


                        }
                        decimal res = InventariosController.SetProductosBodegasEmpresa(bod.IdBodega, cache.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFacturaProveedorPuntos, null, null, "Descarga de producto", null, false, true, -1);



                    }

                }
                SaveConsecutivoFacturaEmpresa(true, fact.GuidFacturaProveedorPuntos, programa.NombrePrograma);
                CleanCacheDetalle2(idCollectionCache);
                return fact.GuidFacturaProveedorPuntos;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return "";
            }
        }
        #endregion

        #region Ajustes
        public static string SaveAjusteAndDetalles(string idCollectionCache, int idBodega, bool tipoAjuste, string observacion, DateTime fechaAjuste)
        {
            try
            {

                string[] caches = idCollectionCache.Split(',');
                CacheDetalleAjusteCollection cacheCollection = new CacheDetalleAjusteCollection();
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetalleAjuste cdf = new CacheDetalleAjuste(CacheDetalleAjuste.Columns.IdCacheDetalleAjuste, caches[i]);
                    cacheCollection.Add(cdf);
                }
                decimal costoBase = 0;
                decimal precioBase = 0;
                foreach (CacheDetalleAjuste cache in cacheCollection)
                {
                    int cantidadEnBodega = 0;

                    //BodegasProductosCollection disponibilidad = new BodegasProductosCollection();
                    //disponibilidad.Where(BodegasProductos.Columns.IdBodega, idBodega);
                    //disponibilidad.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                    //disponibilidad.Load();
                    //foreach (BodegasProductos bp in disponibilidad)
                    //{
                    //    cantidadEnBodega += Convert.ToInt32(bp.Cantidad);
                    //}

                    //if (cache.Cantidad > cantidadEnBodega)
                    //    return "";
                    costoBase += (cache.CostoUnitario * cache.Cantidad);
                    precioBase += (cache.PrecioUnitario * cache.Cantidad);
                }
                string valorEnLetrasCosto = "";
                string valorEnLetrasPrecio = "";
                try
                {
                    valorEnLetrasCosto = ConversionFinal(costoBase);
                    valorEnLetrasPrecio = ConversionFinal(precioBase);
                }
                catch { }
                Ajustes ajuste = new Ajustes();
                ajuste.GuidAjuste = System.Guid.NewGuid().ToString();
                ajuste.IdBodega = idBodega;
                ajuste.TotalBase = costoBase;
                ajuste.PrecioBase = precioBase;
                ajuste.FechaAjuste = fechaAjuste;
                ajuste.ValorEnLetrasTotal = valorEnLetrasCosto;
                ajuste.ValorEnLetrasPrecio = valorEnLetrasPrecio;
                ajuste.Observaciones = observacion;
                ajuste.Entrada = tipoAjuste;
                Resoluciones res = new Resoluciones(Resoluciones.Columns.IdTipoResolucion, int.Parse(ConfigurationManager.AppSettings["ResolucionAjuste"]));
                ajuste.Consecutivo = res.ValorActual;
                ajuste.Aprobado = false;
                ajuste.Save();
                res.ValorActual = res.ValorActual + 1;
                res.Save();
                int numeroItem = 0;
                foreach (CacheDetalleAjuste cache in cacheCollection)
                {
                    numeroItem += 1;

                    DetalleAjuste detalleAjuste = new DetalleAjuste();
                    detalleAjuste.IdDetalleAjuste = System.Guid.NewGuid().ToString();
                    detalleAjuste.Cantidad = cache.Cantidad;
                    detalleAjuste.CostoUnitario = cache.CostoUnitario;
                    detalleAjuste.CostoTotal = (cache.CostoUnitario * cache.Cantidad);
                    detalleAjuste.PrecioUnitario = cache.PrecioUnitario;
                    detalleAjuste.PrecioTotal = (cache.PrecioUnitario * cache.Cantidad);
                    detalleAjuste.NumeroItem = numeroItem;
                    detalleAjuste.GuidAjuste = ajuste.GuidAjuste;
                    detalleAjuste.GuidReferenciaProducto = cache.GuidReferenciaProducto;
                    detalleAjuste.Save();

                    //falta que descargue inconvenientes con el Historico de bodegas por el guid de la factura
                    //decimal rest = 0;
                    //if (ajuste.Entrada)
                    //{
                    //    rest = InventariosController.SetProductosBodegasAjuste(idBodega, detalleAjuste.GuidReferenciaProducto, Convert.ToInt32(detalleAjuste.Cantidad), ajuste.FechaAjuste, detalleAjuste.IdDetalleAjuste, null, null, "Carga Ajuste de producto", null, true, true, -1);
                    //}
                    //else
                    //{
                    //    rest = InventariosController.SetProductosBodegasAjuste(idBodega, detalleAjuste.GuidReferenciaProducto, Convert.ToInt32(detalleAjuste.Cantidad), ajuste.FechaAjuste, detalleAjuste.IdDetalleAjuste, null, null, "Descarga Ajuste de producto", null, false, true, -1);

                    //}

                }

                CleanCacheDetalle3(idCollectionCache);
                return ajuste.GuidAjuste;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveAjusteAndDetalles(string idCollectionCache, int idBodega, bool tipoAjuste, string observacion, DateTime fechaAjuste) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return "";
            }
        }
        public static DataSet GetAjustesByCriterias(int? idBodega, int? idPrograma, string guidProducto, int? idProveedor, int? consecutivo, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetAjustesByCriterias(idBodega, idPrograma, guidProducto, idProveedor, consecutivo, TotalRecords);

                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetAjustesByCriterias() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static DataSet GetDetallesAjuste(string guidAjuste)
        {
            try
            {
                SubSonic.StoredProcedure sp = SPs.GetDetalleAjustesByAjuste(guidAjuste);
                DataSet ds = sp.GetDataSet();
                return ds;

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesAjuste() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool SetAnularAjuste(string GuidAjuste, string GuidUsuario, ref string msg)
        {
            msg = string.Empty;
            try
            {
                Ajustes obj = new Ajustes(Ajustes.Columns.GuidAjuste, GuidAjuste);
                if (!obj.IsNew)
                {
                    if (obj.Anulada == null)
                    {
                        obj.Anulada = true;
                        obj.GuidUsuarioAnulada = GuidUsuario;
                        obj.FechaAnulada = DateTime.Now;
                        obj.Save();

                        DataSet ds = SPs.AnulacionAjustes(GuidAjuste).GetDataSet();

                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            BodegasProductos objBP = new BodegasProductos(BodegasProductos.Columns.IdBodegaProducto, dr["ID_BODEGA_PRODUCTO"].ToString());
                            if (bool.Parse(dr["ENTRADA"].ToString()) == true)
                                objBP.Cantidad = decimal.Parse(dr["CANTIDAD_TOTAL"].ToString()) - decimal.Parse(dr["CANTIDAD_TRANSACCION"].ToString());
                            else
                                objBP.Cantidad = decimal.Parse(dr["CANTIDAD_TOTAL"].ToString()) + decimal.Parse(dr["CANTIDAD_TRANSACCION"].ToString());
                            objBP.Save();
                        }
                    }
                    else
                    {
                        msg = "El ajuste ya tiene un estado de anulación.";
                        return false;
                    }
                }
                else
                {
                    msg = "El ajuste que desea anular, no existe, por favor verifique.";
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetDetallesAjuste() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static bool AprobarAjuste(string GuidAjuste, string GuidUsuario, ref string msg)
        {
            msg = string.Empty;
            try
            {
                Ajustes obj = new Ajustes(Ajustes.Columns.GuidAjuste, GuidAjuste);
                if (!obj.IsNew)
                {
                    if (!obj.Aprobado)
                    {
                        obj.Aprobado = true;
                        obj.GuidUsuarioAprobacion = GuidUsuario;
                        obj.FechaAprobacion = DateTime.Now;
                        obj.Save();
                        DetalleAjusteCollection detalles = new DetalleAjusteCollection();
                        detalles.Where(DetalleAjuste.Columns.GuidAjuste, obj.GuidAjuste).Load();

                        foreach (DetalleAjuste detalle in detalles)
                        {
                            decimal rest = 0;
                            if (obj.Entrada)
                            {
                                rest = InventariosController.SetProductosBodegasAjuste(obj.IdBodega, detalle.GuidReferenciaProducto, Convert.ToInt32(detalle.Cantidad), obj.FechaAjuste, detalle.IdDetalleAjuste, null, null, "Carga Ajuste de producto", null, true, true, -1);
                            }
                            else
                            {
                                rest = InventariosController.SetProductosBodegasAjuste(obj.IdBodega, detalle.GuidReferenciaProducto, Convert.ToInt32(detalle.Cantidad), obj.FechaAjuste, detalle.IdDetalleAjuste, null, null, "Descarga Ajuste de producto", null, false, true, -1);

                            }
                        }


                    }
                    else
                    {
                        msg = "El ajuste ya esta aprobado.";
                        return false;
                    }
                }
                else
                {
                    return false;
                }
                return true;

            }
            catch (Exception ex)
            {
                msg = ex.Message;
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AprobarAjuste() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }


        #endregion

        #region FacturacionCliente
        public static bool SaveFacturaClienteAndDetalles(string idCollectionCache, string direccionCliente, string telefonoCliente, string faxCliente, int idCiudad, int idPrograma, string guidCliente, bool IsNotaCredito, int? numeroFactura, string motivoNotaCredito, DateTime? fechaVencimiento, string observacion)
        {
            try
            {
                string[] caches = idCollectionCache.Split(',');
                CacheDetallesFacturaCollection cacheCollection = new CacheDetallesFacturaCollection();
                for (int i = 0; i < caches.Length - 1; i++)
                {
                    CacheDetallesFactura cdf = new CacheDetallesFactura(CacheDetallesFactura.Columns.IdCacheDetalle, caches[i]);
                    cacheCollection.Add(cdf);
                }
                //CacheDetallesFacturaCollection cacheCollection = new CacheDetallesFacturaCollection();
                //cacheCollection.Where(CacheDetallesFactura.Columns.IdUsuario, idUsuario).Load();
                decimal totalBase = 0;
                decimal iva = 0;
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    BodegasCollection bodegasColl = new BodegasCollection();
                    bodegasColl.Where(Bodegas.Columns.IdPrograma, idPrograma);
                    bodegasColl.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]).Load();
                    int cantidadEnBodega = 0;
                    foreach (Bodegas bod in bodegasColl)
                    {
                        BodegasProductosCollection disponibilidad = new BodegasProductosCollection();
                        disponibilidad.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                        disponibilidad.Where(BodegasProductos.Columns.GuidReferenciaProducto, cache.GuidReferenciaProducto);
                        disponibilidad.Load();
                        foreach (BodegasProductos bp in disponibilidad)
                        {
                            cantidadEnBodega += Convert.ToInt32(bp.Cantidad);
                        }
                    }

                    totalBase += (cache.ValorBase + cache.EnvioBase) * cache.Cantidad;
                    iva += cache.Iva * cache.Cantidad;
                }
                string valorEnLetras = "";
                try
                {
                    valorEnLetras = ConversionFinal(totalBase + iva);
                }
                catch { }
                Facturas fact = new Facturas();
                fact.GuidFactura = System.Guid.NewGuid().ToString();
                Programas programa = new Programas(idPrograma);
                if (!programa.IsLoaded)
                    throw new Exception("No se pudo cargar el programa");

                if (!programa.IsLoaded)
                    throw new Exception("No se pudo cargar la resolución");
                if (IsNotaCredito)
                {
                    fact.NumeroFactura = GetConsecutivoNotasCredito(idPrograma).ValorActual;
                }
                else
                {
                    /* Resoluciones resolucionFactura = new Resoluciones(programa.IdResolucion);
                     fact.NumeroFactura = resolucionFactura.ValorActual;
                     resolucionFactura.ValorActual = resolucionFactura.ValorActual + 1;
                     resolucionFactura.Save();*/
                }

                fact.TotalBase = totalBase;
                fact.Iva = iva;
                fact.Prefacturada = false;
                fact.Anulada = false;
                fact.GeneradaEmpresa = false;
                fact.GeneradaEstablecimiento = false;
                DateTime fechaDeHoy = DateTime.Now;
                fact.FechaTransaccion = fechaDeHoy;
                string ultimaFecha = GetUltimoperiodoMes(programa.IdPrograma);
                if (ultimaFecha == null)
                {
                    fact.FechaFactura = DateTime.Now;
                }
                else
                {

                    DateTime fechaFactura = DateTime.Now.AddMonths(1);
                    //fechaFactura = fechaFactura.AddDays(-((Convert.ToDouble(fechaFactura.Day)) - 1));
                    DateTime newDate = new DateTime(fechaFactura.Year, fechaFactura.Month, 1);
                    fact.FechaFactura = newDate;
                }
                fact.FechaCompra = fechaDeHoy;
                fact.FechaAnulacion = null;
                fact.FechaPrefacturada = null;
                fact.ValorEnLetras = valorEnLetras;
                fact.DireccionCliente = direccionCliente;
                fact.TelefonoCliente = telefonoCliente;
                fact.FaxCliente = faxCliente;
                fact.IdCiudadCliente = idCiudad;
                fact.IdPrograma = idPrograma;
                fact.GuidCliente = guidCliente;
                fact.NotaCredito = IsNotaCredito;
                fact.Siniestro = false;
                fact.FacturaNotaCredito = numeroFactura;
                fact.MotivoNotaCredito = motivoNotaCredito;
                if (!string.IsNullOrEmpty(Convert.ToString(fechaVencimiento)))
                {
                    DateTime fecha = Convert.ToDateTime(fechaVencimiento);
                    fact.FechaVencimiento = fecha;
                }
                fact.Observaciones = observacion;
                fact.Save();
                int numeroItem = 1;
                bool bandera = true;
                foreach (CacheDetallesFactura cache in cacheCollection)
                {
                    DetallesFactura detalleFact = new DetallesFactura();
                    detalleFact.GuidDetalleFactura = System.Guid.NewGuid().ToString();
                    detalleFact.NumeroItem = numeroItem;
                    numeroItem++;
                    detalleFact.Cantidad = cache.Cantidad;
                    detalleFact.ValorBase = cache.ValorBase;
                    detalleFact.EnvioBase = cache.EnvioBase;
                    detalleFact.Iva = cache.Iva;
                    detalleFact.GuidReferenciaProducto = cache.GuidReferenciaProducto;
                    detalleFact.GuidFactura = fact.GuidFactura;
                    detalleFact.FactorCompraPuntos = cache.FactorCompraPuntos;
                    detalleFact.DescargoEnInventario = false;
                    detalleFact.Save();
                    if (detalleFact.FactorCompraPuntos != 0)
                        bandera = false;

                    if (IsNotaCredito)
                    {
                        // Descuento de bodega real si es nota credito
                        if (detalleFact.FactorCompraPuntos == 0)
                        {
                            int cantidadACargar = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescarga = new BodegasCollection();
                            bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                            foreach (Bodegas bod in bodegasCollDescarga)
                            {
                                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescarga.Load();
                                foreach (BodegasProductos bp in disponibilidadDescarga)
                                {
                                    if (detalleFact.DescargoEnInventario.HasValue)
                                    {
                                        decimal res = 0;
                                        if (IsNotaCredito)
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, (cantidadACargar), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto nota credito", null, true, true, -1);
                                        else
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                        cantidadACargar = 0;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        //Descarga para bodegas fisicas y si es 100% dinero
                        decimal factorDetalle = 0;
                        decimal factorPunto = InventariosController.GetFactorPunto(detalleFact.GuidDetalleFactura);
                        if (factorPunto == factorDetalle)
                        {
                            // Descarga de la bodegas de saldo si hay existencias

                            int cantidadACargarSaldos = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescargaSaldos = new BodegasCollection();
                            bodegasCollDescargaSaldos.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescargaSaldos.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaSaldos"]);
                            bodegasCollDescargaSaldos.Load();

                            foreach (Bodegas bod in bodegasCollDescargaSaldos)
                            {
                                BodegasProductosCollection disponibilidadDescargaSaldos = new BodegasProductosCollection();
                                disponibilidadDescargaSaldos.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescargaSaldos.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescargaSaldos.Load();
                                foreach (BodegasProductos bp in disponibilidadDescargaSaldos)
                                {
                                    if (cantidadACargarSaldos > 0)
                                    {
                                        decimal res = 0;
                                        if (cantidadACargarSaldos > bp.Cantidad)
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargarSaldos -= Convert.ToInt32(bp.Cantidad);
                                        }
                                        else
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargarSaldos, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargarSaldos = 0;
                                        }
                                    }
                                }
                            }
                            //descargo de bodegas fisicas
                            int cantidadACargar = cantidadACargarSaldos;
                            BodegasCollection bodegasCollDescarga = new BodegasCollection();
                            bodegasCollDescarga.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaFisica"]);
                            bodegasCollDescarga.Where(Bodegas.Columns.IdPrograma, idPrograma).Load();
                            foreach (Bodegas bod in bodegasCollDescarga)
                            {
                                BodegasProductosCollection disponibilidadDescarga = new BodegasProductosCollection();
                                disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescarga.Load();

                                if (disponibilidadDescarga.Count == 0)
                                {
                                    BodegasProductos bdp = new BodegasProductos();
                                    bdp.IdBodega = bod.IdBodega;
                                    bdp.GuidReferenciaProducto = cache.GuidReferenciaProducto;
                                    bdp.Cantidad = 0;
                                    bdp.Save();

                                    disponibilidadDescarga = new BodegasProductosCollection();
                                    disponibilidadDescarga.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                    disponibilidadDescarga.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                    disponibilidadDescarga.Load();


                                }
                                foreach (BodegasProductos bp in disponibilidadDescarga)
                                {
                                    decimal res = 0;
                                    if (!IsNotaCredito)
                                    {
                                        res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                    }
                                    else
                                    {
                                        if (IsNotaCredito)
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, (0 - cantidadACargar), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                        else
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                        cantidadACargar = 0;
                                    }
                                }
                            }

                            //descarga de boedgas virtuales si es mayor que 0 la cantidad
                            cantidadACargar = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescargaVirutal = new BodegasCollection();
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                            bodegasCollDescargaVirutal.Load();

                            foreach (Bodegas bod in bodegasCollDescargaVirutal)
                            {
                                BodegasProductosCollection disponibilidadDescargaVirutal = new BodegasProductosCollection();
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescargaVirutal.Load();
                                foreach (BodegasProductos bp in disponibilidadDescargaVirutal)
                                {
                                    if (cantidadACargar > 0)
                                    {
                                        decimal res = 0;
                                        if (cantidadACargar > bp.Cantidad)
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                                        }
                                        else
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar = 0;
                                        }
                                    }
                                }
                            }

                            // Se marca como descargado el detalle
                            DetallesFactura detalle = new DetallesFactura(DetallesFactura.Columns.GuidDetalleFactura, detalleFact.GuidDetalleFactura);
                            if (!detalle.IsNew)
                            {
                                detalle.DescargoEnInventario = true;
                                detalle.Save();
                            }

                        }
                        else
                        {
                            // Descargamos solo de virtual si la cantidad esta disponible.
                            int cantidadACargar = detalleFact.Cantidad;
                            BodegasCollection bodegasCollDescargaVirutal = new BodegasCollection();
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdPrograma, idPrograma);
                            bodegasCollDescargaVirutal.Where(Bodegas.Columns.IdTipoBodega, ConfigurationManager.AppSettings["BodegaVirtual"]);
                            bodegasCollDescargaVirutal.Load();
                            foreach (Bodegas bod in bodegasCollDescargaVirutal)
                            {
                                BodegasProductosCollection disponibilidadDescargaVirutal = new BodegasProductosCollection();
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.IdBodega, bod.IdBodega);
                                disponibilidadDescargaVirutal.Where(BodegasProductos.Columns.GuidReferenciaProducto, detalleFact.GuidReferenciaProducto);
                                disponibilidadDescargaVirutal.Load();
                                foreach (BodegasProductos bp in disponibilidadDescargaVirutal)
                                {
                                    if (cantidadACargar > 0)
                                    {
                                        decimal res = 0;
                                        if (cantidadACargar > bp.Cantidad)
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, Convert.ToInt32(bp.Cantidad), DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar -= Convert.ToInt32(bp.Cantidad);
                                        }
                                        else
                                        {
                                            res = InventariosController.SetProductosBodegas(bod.IdBodega, detalleFact.GuidReferenciaProducto, cantidadACargar, DateTime.Now, detalleFact.GuidDetalleFactura, null, null, "Descarga de producto", null, false, true, -1);
                                            cantidadACargar = 0;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                if (!IsNotaCredito)
                {
                    SaveConsecutivoFactura(true, fact.GuidFactura, programa.NombrePrograma);
                }

                if (bandera)
                {
                    fact.FechaCompra = fechaDeHoy;
                    fact.Save();
                }
                CleanCacheDetalle2(idCollectionCache);
                //CacheDetallesFactura.Delete(CacheDetallesFactura.Columns.IdUsuario, idUsuario);
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveFactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }
        #endregion

        /// <summary>
        /// Obtiene las facturas de clientes y empresas.
        /// </summary>
        /// <param name="numeroFactura"></param>
        /// <param name="nit"></param>
        /// <param name="dtInicio"></param>
        /// <param name="dtFinal"></param>
        /// <param name="idPrograma"></param>
        /// <param name="idProveedor"></param>
        /// <param name="guidProducto"></param>
        /// <param name="porcentajePuntos"></param>
        /// <returns></returns>
        public static DataSet GetInformeFacturas
          (
          int? numeroFactura,
          string nit,
          DateTime? dtInicio,
          DateTime? dtFinal,
        int? idPrograma,
        int? idProveedor,
        string guidProducto,
          decimal? porcentajePuntos,
        bool? anulada,
        bool? notaCredito,
        int pageSize,
        int pageIndex,
        out int TotalRecords
          )
        {
            TotalRecords = 0;
            try
            {

                SubSonic.StoredProcedure sp = SPs.GetReporteFacturacion(
                  numeroFactura,
                  nit,
                  dtInicio,
                  dtFinal,
                  idPrograma,
                  idProveedor,
                  guidProducto,
                  porcentajePuntos,
                  anulada,
                  notaCredito,
                  pageSize,
                  pageIndex,
                  TotalRecords
                  );
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetInformeFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetInformeEntradasySalidasDetallado
         (
         int year, int? month
         )
        {
            try
            {

                SubSonic.StoredProcedure sp = SPs.GetInformeEntradasySalidasDetallado(
                 year, month);
                DataSet ds = sp.GetDataSet();
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetInformeFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetInformeEntradasySalidasConsolidado
        (
        int year, int? month
        )
        {
            try
            {

                SubSonic.StoredProcedure sp = SPs.GetInformeEntradasySalidasConsolidado(
                 year, month);
                DataSet ds = sp.GetDataSet();
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetInformeFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetPeriodoCosteo
       (
       int idPeriodo
       )
        {
            try
            {

                SubSonic.StoredProcedure sp = SPs.GetPeriodoCosteo(
                 idPeriodo);
                DataSet ds = sp.GetDataSet();
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetInformeFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetPeriodoCosteoV2
       (
       int idPeriodo
       )
        {
            try
            {

                SubSonic.StoredProcedure sp = SPs.GetPeriodoCosteoV2(
                 idPeriodo);
                DataSet ds = sp.GetDataSet();
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetInformeFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetInformeFacturasCharts
          (
          DateTime? dtInicio,
          DateTime? dtFinal,
        int? idPrograma,
        int tipo
          )
        {
            try
            {
                return SPs.GetReporteFacturacionCharts(
                   dtInicio,
                   dtFinal,
                   idPrograma,
                   tipo
                   ).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetInformeFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static string updateFechaEntrega(string fecha, string guidFactura)
        {
            try
            {
                Facturas factura = new Facturas(Facturas.Columns.GuidFactura, guidFactura);
                if (!factura.IsNew)
                {
                    DateTime fecha1 = Convert.ToDateTime(fecha);
                    factura.FechaEntrega = fecha1;
                    factura.Save();
                    return "guardo";
                }
                else
                {
                    return "no existe Factura";
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveCliente() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return "error al actualizar";
            }
        }

        public static string GetAjustesByGuid(string GuidAjuste)
        {
            string resultado = "";

            try
            {
                Ajustes obj = new Ajustes(Ajustes.Columns.GuidAjuste, GuidAjuste);
                if (!obj.IsNew)
                {
                    if (obj.Aprobado)
                    {
                        if (obj.Anulada.HasValue)
                        {
                            if (obj.Anulada.Value)
                                resultado = "1"; //mostrar aprobado y anulado si
                            else
                            {
                                resultado = "2";
                            }
                        }
                        else
                        {
                            resultado = "2"; // mostrar aprobado y no anulado 
                        }
                    }
                    else
                    {
                        resultado = "3";
                        //mostrar no aprobado 

                    }
                }
                else
                {
                    resultado = "";
                }
                return resultado;
            }

            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetAjustesByGuid() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

    }



}
