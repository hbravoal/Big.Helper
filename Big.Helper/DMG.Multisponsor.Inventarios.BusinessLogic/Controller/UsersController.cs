using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;
using DMG.Core.Mailer;
using System.Diagnostics;
using OnDataMailer;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class UsersController
    {
        #region Login
        /// <summary>
        /// obtiene un usuario por medio del usuario y la contrase�a
        /// </summary>
        /// <param name="username">nombre de usuario</param>
        /// <param name="password">contrase�a</param>
        /// <param name="exists">parametro de salida que define si existe</param>
        /// <param name="isBlocked">parmetro de salida que define si esta bloqueado</param>
        /// <returns>retorna un objeto usuarios</returns>
        public static Usuarios GetUsuarioByLogin(string username, string password, out bool exists, out bool isBlocked, out bool isInactive)
        {
            exists = false;
            isBlocked = false;
            isInactive = false;
            try
            {
                Usuarios usuario = new Usuarios(Usuarios.Columns.NombreUsuario, username);
                if (!usuario.IsNew)
                {
                    exists = true;
                    if (usuario.IdEstado == 1)
                    {
                        if (usuario.Password.Equals(password))
                        {
                            usuario.IntentosFallidos = 0;
                            usuario.Save();
                            return usuario;
                        }
                        else
                        {
                            usuario.IntentosFallidos = usuario.IntentosFallidos + 1;
                            if (usuario.IntentosFallidos >= Convert.ToInt32(ConfigurationManager.AppSettings["IntentosPermitidos"]))
                            {
                                isBlocked = true;
                                usuario.IdEstado = 3;
                            }
                            usuario.Save();
                            return null;
                        }
                    }
                    else if (usuario.IdEstado == 2)
                    {
                        isInactive = true;
                        return null;
                    }
                    else
                    {
                        isBlocked = true;
                        return null;
                    }
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetUsuarioByLogin() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// obtiene los permisos que tiene el usuario en la aplicaci�n
        /// </summary>
        /// <param name="userName">nombre de usuario</param>
        /// <returns>retorna un arreglo de permisos</returns>
        public static string[] GetPermisosByUserName(string userName)
        {
            try
            {
                Usuarios usuario = new Usuarios(Usuarios.Columns.NombreUsuario, userName);
                if (!usuario.IsNew)
                {
                    PerfilesPermisosCollection perfilPermisos = new PerfilesPermisosCollection();
                    perfilPermisos.Where(PerfilesPermisos.Columns.IdPerfil, usuario.IdPerfil).Load();
                    string[] roles = new string[perfilPermisos.Count];
                    int posicion = 0;
                    foreach (PerfilesPermisos per in perfilPermisos)
                    {
                        roles[posicion] = per.IdPermiso.ToString();
                        posicion++;
                    }
                    return roles;
                }
                else
                    throw new Exception("Un usuario que no existe en la base de datos esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPermisosByPerfil() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                string[] sinPermisos = { "" };
                return sinPermisos;
            }
        }
        #endregion

        #region Perfilacion
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MasPermisosCollection GetPermisos()
        {
            try
            {
                MasPermisosCollection listPermisos = new MasPermisosCollection();
                return listPermisos.OrderByAsc(MasPermisos.Columns.IdPermiso).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPermisos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nombrePerfil"></param>
        /// <param name="descripcionPerfil"></param>
        /// <param name="permisos"></param>
        /// <returns></returns>
        public static bool SavePerfil(string nombrePerfil, string descripcionPerfil, List<int> permisos)
        {
            try
            {
                Perfiles perfil = new Perfiles();
                perfil.NombrePerfil = nombrePerfil;
                perfil.DescripcionPerfil = descripcionPerfil;
                perfil.Save();
                foreach (int idPermiso in permisos)
                {
                    PerfilesPermisos permisosPerfil = new PerfilesPermisos();
                    permisosPerfil.IdPerfil = perfil.IdPerfil;
                    permisosPerfil.IdPermiso = idPermiso;
                    permisosPerfil.Save();
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SavePerfil() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nombrePerfil"></param>
        /// <param name="descripcionPerfil"></param>
        /// <param name="permisos"></param>
        /// <param name="idPerfil"></param>
        /// <returns></returns>
        public static bool EditPerfil(string nombrePerfil, string descripcionPerfil, List<int> permisos, int idPerfil)
        {
            try
            {
                Perfiles perfil = new Perfiles(idPerfil);
                perfil.NombrePerfil = nombrePerfil;
                perfil.DescripcionPerfil = descripcionPerfil;
                perfil.Save();
                PerfilesPermisos.Delete(PerfilesPermisos.Columns.IdPerfil, idPerfil);
                foreach (int idPermiso in permisos)
                {
                    PerfilesPermisos permisosPerfil = new PerfilesPermisos();
                    permisosPerfil.IdPerfil = perfil.IdPerfil;
                    permisosPerfil.IdPermiso = idPermiso;
                    permisosPerfil.Save();
                }
                return true;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo EditPerfil() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPerfil"></param>
        /// <returns></returns>
        public static string[] GetPermisosByPerfil(int idPerfil)
        {
            try
            {
                PerfilesPermisosCollection perfilPermisos = new PerfilesPermisosCollection();
                perfilPermisos.Where(PerfilesPermisos.Columns.IdPerfil, idPerfil).Load();
                string[] roles = new string[perfilPermisos.Count];
                int posicion = 0;
                foreach (PerfilesPermisos per in perfilPermisos)
                {
                    roles[posicion] = per.IdPermiso.ToString();
                    posicion++;
                }
                return roles;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPermisosByPerfil() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                string[] sinPermisos = { "" };
                return sinPermisos;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static PerfilesCollection GetPerfiles()
        {
            try
            {
                PerfilesCollection perfiles = new PerfilesCollection();
                return perfiles.OrderByAsc(Perfiles.Columns.IdPerfil).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPerfiles() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPerfil"></param>
        /// <returns></returns>
        public static string GetPermisosNamesByPerfil(int idPerfil)
        {
            try
            {
                PerfilesPermisosCollection perfilPermisos = new PerfilesPermisosCollection();
                perfilPermisos.Where(PerfilesPermisos.Columns.IdPerfil, idPerfil).Load();
                StringBuilder permisos = new StringBuilder();
                foreach (PerfilesPermisos per in perfilPermisos)
                {
                    MasPermisos pers = new MasPermisos(per.IdPermiso);
                    permisos.Append(pers.NombrePermiso).Append(" / ");
                }
                return permisos.ToString();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPermisosByPerfil() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPerfil"></param>
        /// <param name="listPermisos"></param>
        /// <returns></returns>
        public static Perfiles GetPerfilAndPermisos(int idPerfil, out PerfilesPermisosCollection listPermisos)
        {
            listPermisos = new PerfilesPermisosCollection();
            try
            {
                Perfiles profile = new Perfiles(idPerfil);
                if (profile.IsLoaded)
                {
                    listPermisos.Where(PerfilesPermisos.Columns.IdPerfil, profile.IdPerfil).Load();
                    return profile;
                }
                else
                    throw new Exception("Un perfil que no existe en la base de datos esta tratando de editarse a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPerfilAndPermisos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idPerfil"></param>
        /// <returns></returns>
        public static Perfiles GetPerfil(int idPerfil)
        {
            try
            {
                Perfiles profile = new Perfiles(idPerfil);
                if (profile.IsLoaded)
                {
                    return profile;
                }
                else
                    throw new Exception("Un perfil que no existe en la base de datos esta tratando de editarse a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPerfil() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static PerfilesPermisosCollection GetPerfilesPermisos()
        {
            try
            {
                PerfilesPermisosCollection colPerfilPermiso = new PerfilesPermisosCollection();
                return colPerfilPermiso.OrderByAsc(PerfilesPermisos.Columns.IdPefilPermiso).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPerfilesPermisos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        #endregion

        #region Usuarios

        public static bool RememberPassword(string userName, string url)
        {
            try
            {
                Usuarios newUser = new Usuarios(Usuarios.Columns.NombreUsuario, userName);
                if (newUser.IsLoaded)
                {
                   
                    StringBuilder bodyBuilder = new StringBuilder();
                    CryptographyProvider cp = new CryptographyProvider();
                    bodyBuilder.Append("Sus datos para al aplicaci�n Inventarios\n\n");
                    bodyBuilder.Append("Nombre de usuario: ").Append(newUser.NombreUsuario).Append("\n");
                    bodyBuilder.Append("Contrase�a: ").Append(cp.DecryptData(newUser.Password)).Append("\n\n");
                    bodyBuilder.Append("La contrase�a debera ser cambiada la primera vez que inicie sesi�n.\n");
                    bodyBuilder.Append("Para acceder a la aplicaci�n: ").Append(url);

                    //MailHelper mailHelper = new MailHelper();
                    //mailHelper.Send(newUser.Email, bodyBuilder.ToString(), "Inventarios - Nuevo usuario", false);

                    List<string> contacts = new List<string>();
                    contacts.Add(newUser.Email);
                    OnDataMailer.OnDataMailer.EnvioMail(contacts, "Inventarios - Recordar Contrase�a", bodyBuilder.ToString(), false, "Crear usuario");

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveUsuario() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="password"></param>
        /// <param name="userName"></param>
        /// <returns></returns>
        public static bool ChangePassword(string password, string userName)
        {
            try
            {
                Usuarios user = new Usuarios(Usuarios.Columns.NombreUsuario, userName);
                if (!user.IsNew)
                {
                    if (user.Password.Equals(password))
                        return false;
                    else
                    {
                        user.Password = password;
                        user.PrimeraVez = false;
                        user.FechaPassword = DateTime.Now;
                        user.Save();
                        return true;
                    }
                }
                else
                    throw new Exception("Un usuario que no existe en la base de datos esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ChangePassword() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identificacion"></param>
        /// <param name="nombreCompleto"></param>
        /// <param name="email"></param>
        /// <param name="userName"></param>
        /// <param name="contrasena"></param>
        /// <param name="idPerfil"></param>
        /// <param name="idEstado"></param>
        /// <returns></returns>
        public static bool SaveUsuario(string identificacion, string nombreCompleto, string email, string userName, string contrasena, int idPerfil, int idEstado, string url)
        {
            try
            {
                Usuarios newUser = new Usuarios(Usuarios.Columns.NombreUsuario, userName);
                if (newUser.IsNew)
                {
                    newUser.NombreUsuario = userName;
                    newUser.Identificacion = identificacion;
                    newUser.Nombre = nombreCompleto;
                    newUser.Email = email;
                    newUser.Password = contrasena;
                    newUser.IdPerfil = idPerfil;
                    newUser.FechaPassword = DateTime.Now;
                    newUser.IntentosFallidos = 0;
                    newUser.PrimeraVez = true;
                    newUser.IdEstado = idEstado;
                    newUser.Save();
                    StringBuilder bodyBuilder = new StringBuilder();
                    CryptographyProvider cp = new CryptographyProvider();
                    bodyBuilder.Append("Se ha creado un nuevo usuario registrado a este correo electr�nico para al aplicaci�n Inventarios\n\n");
                    bodyBuilder.Append("Nombre de usuario: ").Append(newUser.NombreUsuario).Append("\n");
                    bodyBuilder.Append("Contrase�a: ").Append(cp.DecryptData(newUser.Password)).Append("\n\n");
                    bodyBuilder.Append("La contrase�a debera ser cambiada la primera vez que inicie sesi�n.\n");
                    bodyBuilder.Append("Para acceder a la aplicaci�n: ").Append(url);

                    //MailHelper mailHelper = new MailHelper();
                    //mailHelper.Send(newUser.Email, bodyBuilder.ToString(), "Inventarios - Nuevo usuario", false);

                    List<string> contacts = new List<string>();
                    contacts.Add(newUser.Email);
                    OnDataMailer.OnDataMailer.EnvioMail(contacts, "Inventarios - Creaci�n de usuario", bodyBuilder.ToString(), false, "Crear usuario");

                    return true;
                }
                else
                    return false;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SaveUsuario() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="identificacion"></param>
        /// <param name="nombreCompleto"></param>
        /// <param name="email"></param>
        /// <param name="userName"></param>
        /// <param name="contrasena"></param>
        /// <param name="idPerfil"></param>
        /// <param name="idEstado"></param>
        /// <returns></returns>
        public static bool EditUsuario(int idUsuario, string identificacion, string nombreCompleto, string email, string userName, string contrasena, int idPerfil, int idEstado, bool nuevoLogin, string url)
        {
            try
            {
                UsuariosCollection verificadorCantidad = new UsuariosCollection();
                verificadorCantidad.Where(Usuarios.Columns.NombreUsuario, userName).Load();
                bool isEditable = false;
                if (verificadorCantidad.Count == 0)
                    isEditable = true;
                else if (verificadorCantidad.Count == 1)
                {
                    foreach (Usuarios us in verificadorCantidad)
                    {
                        if (us.IdUsuario == idUsuario)
                            isEditable = true;
                    }
                }
                if (isEditable)
                {
                    Usuarios newUser = new Usuarios(idUsuario);
                    if (!newUser.IsNew)
                    {
                        newUser.Identificacion = identificacion;
                        newUser.Nombre = nombreCompleto;
                        newUser.Email = email;
                        newUser.NombreUsuario = userName;
                        newUser.Password = contrasena;
                        newUser.IdPerfil = idPerfil;
                        if (nuevoLogin)
                        {
                            newUser.FechaPassword = DateTime.Now;
                            newUser.IntentosFallidos = 0;
                            newUser.PrimeraVez = true;
                        }
                        newUser.IdEstado = idEstado;
                        newUser.Save();
                        StringBuilder bodyBuilder = new StringBuilder();
                        CryptographyProvider cp = new CryptographyProvider();
                        bodyBuilder.Append("Se ha modificado los datos de tu usuario para al aplicaci�n Inventarios y se ha regenerado su contrase�a.\n\n");
                        bodyBuilder.Append("Nombre de usuario: ").Append(newUser.NombreUsuario).Append("\n");
                        bodyBuilder.Append("Contrase�a: ").Append(cp.DecryptData(newUser.Password)).Append("\n\n");
                        bodyBuilder.Append("La contrase�a deber� ser cambiada la pr�xima vez que inicie sesi�n.\n");
                        bodyBuilder.Append("Para acceder a la aplicaci�n: ").Append(url);
                        //MailHelper mailHelper = new MailHelper();
                        //if (nuevoLogin)
                        //    mailHelper.Send(newUser.Email, bodyBuilder.ToString(), "Inventarios - Edici�n de usuario", false);

                        List<string> contacts = new List<string>();
                        contacts.Add(newUser.Email);
                        OnDataMailer.OnDataMailer.EnvioMail(contacts, "Inventarios - Edici�n de usuario", bodyBuilder.ToString(), false, "Editar usuario");

                        return true;
                    }
                    else
                        throw new Exception("Un usuario que no existe en la base de datos esta tratando de editarse a este metodo");
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo EditUsuario() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }





        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static UsuariosCollection GetUsuarios()
        {
            try
            {
                UsuariosCollection usuarios = new UsuariosCollection();
                return usuarios.OrderByAsc(Usuarios.Columns.Nombre).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetUsuarios() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public static MasEstadosCollection GetEstadosDeUsuario()
        {
            try
            {
                return new MasEstadosCollection().Where(MasEstados.Columns.IdTipoEstado, 1).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetEstadosDeUsuario() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idEstado"></param>
        /// <returns></returns>
        public static MasEstados GetEstado(int idEstado)
        {
            try
            {
                MasEstados estado = new MasEstados(idEstado);
                if (estado.IsLoaded)
                {
                    return estado;
                }
                else
                    throw new Exception("Un perfil que no existe en la base de datos esta tratando de editarse a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetEstado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="idUsuario"></param>
        /// <returns></returns>
        public static Usuarios GetUsuarioById(int idUsuario)
        {
            try
            {
                Usuarios usuario = new Usuarios(idUsuario);
                if (usuario.IsLoaded)
                    return usuario;
                else
                    throw new Exception("Un usuario que no existe en la base de datos esta tratando de editarse en este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetUsuarioById() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static Usuarios GetUsuarioByUserName(string userName)
        {
            try
            {
                Usuarios user = new Usuarios(Usuarios.Columns.NombreUsuario, userName);
                if (!user.IsNew)
                    return user;
                else
                    throw new Exception("Un usuario que no existe en la base de datos esta tratando de acceder a este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetUsuarioByUserName() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static List<Usuarios> GetUsuariosByLikeName(string likeName)
        {
            try
            {
                return DB.Select().From(Usuarios.Schema).Where(Usuarios.NombreColumn).Like("%"+likeName+"%").ExecuteTypedList<Usuarios>();
                //UsuariosCollection usuarios = new UsuariosCollection().Where(Usuarios.NombreColumn.ColumnName,SubSonic.Comparison.Like,likeName);
                //return usuarios.OrderByAsc(Usuarios.Columns.Nombre).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetUsuariosByLikeName() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.UsersController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        #endregion
    }
}