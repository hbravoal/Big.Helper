// =============================================
// Author:		C�sar A. L�pez A.
// Create date: 25 de Julio de 2009.
// Description:	CloseMonthlyController.cs
// Email: clopez@ondata.com.com
// =============================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;
using DMG.Multisponsor.Inventarios.Logging;


namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class CloseMonthlyController
  {
    #region [Methods]
    public static bool InsertCloseMonthly(int ano,int idprograma, int mes,int user)
    {
      try
      {
        bool estate = false;

        CierreMensualCollection col = new CierreMensualCollection();
        col.Where(CierreMensual.Columns.Ano, ano);
        col.Where(CierreMensual.Columns.Mes, mes);
        col.Where(CierreMensual.Columns.IdPrograma, idprograma);
        col.Load();

        foreach (CierreMensual mensual in col)
        {
          estate = true;
        }

        if (!estate)
        {
          CierreMensual obj = new CierreMensual();
          obj.Ano = ano;
          obj.IdPrograma = idprograma;
          obj.Mes = mes;
          obj.Save();
          string periodo = ano.ToString();
          if (mes < 10)
            periodo += "0" + mes.ToString();
          else
            periodo += mes.ToString();

          SPs.InsertHistoricoPreciosByPrograma(periodo, user, idprograma).Execute();
        }
        return !estate;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo InsertCloseMonthly(CierreMensual closeMonthly) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.CloseMonthlyController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return false;
      }
    }
    public static bool SelectValidateCloseMonthlyByPrograma(int IdPrograma,DateTime dtFecha)
    {
      try
      {
        bool estate = false;
        CierreMensualCollection colCierre = new CierreMensualCollection();
        colCierre.Where(CierreMensual.Columns.IdPrograma, IdPrograma);
        colCierre.OrderByDesc(CierreMensual.Columns.Id).Load();
        foreach(CierreMensual cierre in colCierre)
        {
          if (dtFecha.Month == cierre.Mes && dtFecha.Year == cierre.Ano)
            estate = true;            
        }
        return estate;        
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo InsertCloseMonthly(CierreMensual closeMonthly) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.CloseMonthlyController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return false;
      }
    }

    public static void UpdateCloseMonthlyByPrograma(int IdPrograma)
    {
      try
      {
        Programas obj = new Programas(IdPrograma);
        obj.Cerrado = true;
        obj.Save();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo InsertCloseMonthly(CierreMensual closeMonthly) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.CloseMonthlyController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
      }
    }

      public static DataTable SelectIdProgramByGuidProduct(string guidProducto)
      {
          try
          {
             return SPs.GetIdProgramaByGuidProducto(guidProducto).GetDataSet().Tables[0];
          }
          catch (Exception ex)
          {
              //Construimos la excepcion a ser almacenada en un log
              StringBuilder exception = new StringBuilder();
              exception.Append("Error en el metodo selectIdProgramByGuidProduct() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.CloseMonthlyController:\n");
              exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
              Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
              return null;
          }
      }
    #endregion
  }
}
 