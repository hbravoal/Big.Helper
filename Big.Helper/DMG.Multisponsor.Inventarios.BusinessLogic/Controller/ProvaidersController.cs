using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class ProvaidersController
    {
        public static DataTable CargarTiposCuenta(int idFantasma)
        {
            DataTable dt = SPs.SelectTiposCuenta(idFantasma).GetDataSet().Tables[0];
            return dt;
        }

       public static DataTable GetProviders()
       {
          try
          {
             ProveedoresCollection colProviders = new ProveedoresCollection();
             return colProviders.ToDataTable();
          }
          catch (Exception ex)
          {
             //Construimos la excepcion a ser almacenada en un log
             StringBuilder exception = new StringBuilder();
             exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
             exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
             Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
             return null;
          }
       }

        public static DataTable GetAllProviders()
        {
            try
            {
                ProveedoresCollection colProviders = new ProveedoresCollection();
                return colProviders.Load().ToDataTable();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable CargarProveedoresAuto()
        {
            DataTable dt = SPs.SelectProveedoresAuto(0).GetDataSet().Tables[0];
            return dt;
        }

        //public static DataTable CargarProveedores(int idFantasma)
        //{
        //    DataTable dt = SPs.SelectProveedoresNombres(idFantasma).GetDataSet().Tables[0];
        //    return dt;
        //}
        public static DataTable CargarEstados(int idFantasma)
        {
            DataTable dt = SPs.SelectEstadosProveedores(idFantasma).GetDataSet().Tables[0];
            return dt;
        }

        public static DataTable CargarEntidadesBancarias(int idFantasma)
        {
            DataTable dt = SPs.SelectEntidadesBancarias(idFantasma).GetDataSet().Tables[0];
            return dt;
        }

        public static void AgregarProveedores(string codigoEmpresa, string razonSocialEmpresa, string nitEmpresa
                                          , string direccionEmpresa, string telefonoEmpresa, string faxEmpresa
                                          , int ciudadEmpresa, int idEntidadBancaria, string numeroCuenta, int estado
                                          , string otraEntidad, int idTipoCuenta, bool GranContribuyente, bool RegimenComun
                                          , bool AutoRetenedor, bool RetenedorIva, int? plazoPago, decimal? cupo)
        {
            Proveedores proveedores = new Proveedores();

            proveedores.CodigoProveedor = codigoEmpresa;
            proveedores.RazonSocial = razonSocialEmpresa;
            proveedores.Nit = nitEmpresa;
            proveedores.Direccion = direccionEmpresa;
            proveedores.Telefono = telefonoEmpresa;
            proveedores.Fax = faxEmpresa;
            proveedores.IdCiudad = ciudadEmpresa;
            proveedores.IdTipoCuenta = idTipoCuenta;
            proveedores.PlazoPago = plazoPago;
            proveedores.Cupo = cupo;
            if (otraEntidad != "")
            {
                MasEntidadesBancarias masEntidades = new MasEntidadesBancarias();
                masEntidades.NombreEntidad = otraEntidad;
                masEntidades.Save();
                proveedores.IdEntidadBancaria = masEntidades.IdEntidadBancaria;
            }
            else
            {
                proveedores.IdEntidadBancaria = idEntidadBancaria;
            }

            proveedores.NumeroCuenta = numeroCuenta;
            proveedores.IdEstado = estado;
            proveedores.GranContribuyente = GranContribuyente;
            proveedores.RegimenComun = RegimenComun;
            proveedores.AutoRetenedor = AutoRetenedor;
            proveedores.RetenedorIva = RetenedorIva;
            proveedores.Save();

        }

        public static void ActualizarProveedores(int idProveedor, string codigoEmpresa, string razonSocialEmpresa, string nitEmpresa
                                          , string direccionEmpresa, string telefonoEmpresa, string faxEmpresa
                                          , int ciudadEmpresa, int idEntidadBancaria, string numeroCuenta, int estado, string otraEntidad
                                          , int idTipoCeunta, bool GranContribuyente, bool RegimenComun, bool AutoRetenedor, bool RetenedorIva, int? plazoPago, decimal? cupo)
        {
            Proveedores proveedores = new Proveedores(idProveedor);
            if (proveedores.IsLoaded)
            {
                proveedores.CodigoProveedor = codigoEmpresa;
                proveedores.RazonSocial = razonSocialEmpresa;
                proveedores.Nit = nitEmpresa;
                proveedores.Direccion = direccionEmpresa;
                proveedores.Telefono = telefonoEmpresa;
                proveedores.Fax = faxEmpresa;
                proveedores.IdCiudad = ciudadEmpresa;
                proveedores.IdTipoCuenta = idTipoCeunta;
                proveedores.PlazoPago = plazoPago;
                proveedores.Cupo = cupo;
                if (otraEntidad != "")
                {
                    MasEntidadesBancarias masEntidades = new MasEntidadesBancarias();
                    masEntidades.NombreEntidad = otraEntidad;
                    masEntidades.Save();
                    proveedores.IdEntidadBancaria = masEntidades.IdEntidadBancaria;
                }
                else
                {
                    proveedores.IdEntidadBancaria = idEntidadBancaria;
                }

                proveedores.NumeroCuenta = numeroCuenta;
                proveedores.IdEstado = estado;
                proveedores.GranContribuyente = GranContribuyente;
                proveedores.RegimenComun = RegimenComun;
                proveedores.AutoRetenedor = AutoRetenedor;
                proveedores.RetenedorIva = RetenedorIva;
                proveedores.Save();
            }
        }

        public static Proveedores CargarProveedorPorId(int idProveedor)
        {
            Proveedores proveedor = new Proveedores(Proveedores.Columns.IdProveedor, idProveedor);
            if (proveedor.IsLoaded)
            {
                return proveedor;
            }
            else
            {
                return null;
            }

        }

        public static bool VerificarNitProveedor(string nitProveedor)
        {
            Proveedores proveedor = new Proveedores(Proveedores.Columns.Nit, nitProveedor);
            if (!proveedor.IsNew)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public static DataSet GetProveedoresByCriterias(string codigoProveedor, string nit, int? idEstado, int? idProveedor, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.SelectProveedoresNombres(codigoProveedor, nit, idEstado, idProveedor, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable ExportarProveedores(string providerCode, string providerNit, int? providerStatusId, int? providerId)
        {
            try
            {
                return SPs.ExportarProveedores(providerCode, providerNit, providerStatusId, providerId).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
    }
}
