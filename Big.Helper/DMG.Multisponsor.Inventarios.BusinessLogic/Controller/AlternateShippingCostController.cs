// =============================================
// Author:		Jorge A. Gonz�lez
// Create date: 19-10-2009
// Description:	AlternateShippingCostController.cs
// Email: jorge.gonzalez@ondata.com.co
// =============================================
using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;

using DMG.Multisponsor.Inventarios.DataAccess.Core;
using SubSonic;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
   public class AlternateShippingCostController
   {
      /// <summary>
      /// Devuelve la lista de costos de envio alternativos.
      /// </summary>
      /// <param name="programId">Id del programa</param>
      /// <param name="productGuid">Guid del producto</param>
      /// <param name="cityId">Id de la ciudad</param>
      /// <param name="deliveryTypeId">Tipo delivery</param>
      /// <param name="pageIndex"></param>
      /// <param name="pageSize"></param>
      /// <param name="totalRecords"></param>
      /// <returns></returns>
      public static DataSet GetAlternateShippingCosts(
         int? programId
         , string productGuid
         , int? cityId
         , int? deliveryTypeId
         , int pageIndex
         , int pageSize
         , out int totalRecords)
      {
         StoredProcedure sp = SPs.GetCostosEnvioAlternos(
            programId
           , productGuid
           , deliveryTypeId
           , cityId);
         sp.PagingResult = true;
         sp.PageIndex = pageIndex;
         sp.PageSize = pageSize;

         DataSet ds = sp.GetDataSet();

         if (ds != null && ds.Tables.Count > 0)
         {
            try { totalRecords = Convert.ToInt32(ds.Tables[1].Rows[0][0]); }
            catch { totalRecords = 0; }
         }
         else
            totalRecords = 0;

         return ds;
         
      }
   }
}
