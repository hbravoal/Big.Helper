using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class BalanceCheckController
  {
    public static DataSet CargarBodegas()
    {
      DataSet ds = SPs.SelectMasBodegas(0).GetDataSet();
      return ds;
    }

    public static DataSet CargarClientes()
    {
      DataSet ds = SPs.SelectClientes(0).GetDataSet();
      return ds;
    }

    public static DataSet GetSaldosByCriterias(int? idPrograma, int? idBodega, int? idProveedor, string guidProducto, DateTime? FechaInicio, DateTime? FechaFin, int pageIndex, int PageSize, out int TotalRecords)
    {
      TotalRecords = 0;
      try
      {
        SubSonic.StoredProcedure sp = SPs.SelectSALDOSV2(idPrograma, idBodega, idProveedor, guidProducto, FechaInicio, FechaFin, TotalRecords);
        sp.PageIndex = pageIndex;
        sp.PageSize = PageSize;
        sp.PagingResult = true;
        DataSet ds = sp.GetDataSet();
        int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
        return ds;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetSaldosByCriterias() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetSaldosBodegaByCriteria(int? idPrograma, string guidProducto, string guidReferenciaProducto, int? idProveedor, int? idMarca, int pageIndex, int pageSize, out int TotalRecords)
    {
      TotalRecords = 0;
      try
      {
        SubSonic.StoredProcedure sp = SPs.SaldosTienda(idPrograma, guidProducto, guidReferenciaProducto, idProveedor, idMarca, TotalRecords);
        sp.PageIndex = pageIndex;
        sp.PageSize = pageSize;
        sp.PagingResult = true;
        DataSet ds = sp.GetDataSet();
        int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
        return ds;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetSaldosBodegaByCriteria() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetSaldosContablesByCriteria(int? idPrograma, string guidProducto, string guidReferenciaProducto, int? idProveedor, int? idMarca, int pageIndex, int pageSize, DateTime? fechaInicial, DateTime? fechaFinal, out int TotalRecords)
    {
      TotalRecords = 0;
      try
      {
        SubSonic.StoredProcedure sp = SPs.SelectSaldosNewV2(
          idPrograma,
          idProveedor,
          idMarca,
          guidProducto,
          fechaInicial,
          fechaFinal,
          TotalRecords);
        sp.PageIndex = pageIndex;
        sp.PageSize = pageSize;
        sp.PagingResult = true;
        DataSet ds = sp.GetDataSet();
        int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
        return ds;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetSaldosBodegaByCriteria() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }
    public static DataSet GetDetalleSaldosContables(string guidReferenciaProducto, bool entrada, DateTime? fechaInicial, DateTime? fechaFinal)
    {
      int TotalRecords = 0;
      try
      {
        SubSonic.StoredProcedure sp = SPs.SelectDetalleEntradasSalidasSaldos(
          entrada,
          guidReferenciaProducto,
           fechaInicial,
           fechaFinal,
          TotalRecords);
        DataSet ds = sp.GetDataSet();
        return ds;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetSaldosBodegaByCriteria() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

  }
}
