// =============================================
// Author:		C�sar A. L�pez A.
// Create date: 10 de Diciembre de 2008.
// Description:	InventariosController.cs
// Email: clopez@dmgcolombia.com
// =============================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;
using DMG.Multisponsor.Inventarios.Logging;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class PreBillingsController
    {
        public static DataSet GetFacturasPorFecha(DateTime FechaInicio, DateTime FechaFinal, int IdPrograma)
        {
            try
            {
                return SPs.SelectFacturasFechasPrefacturacion(FechaInicio, FechaFinal, IdPrograma).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasPorFecha() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetGuidTransaccionesPorFecha(DateTime FechaInicio, DateTime FechaFinal, int IdPrograma)
        {
            try
            {
                return SPs.SelectGuidTransaccionesByFechas(FechaInicio, FechaFinal, IdPrograma).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetGuidTransaccionesPorFecha() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetProveedoresPrefacturacionByFecha(DateTime FechaInicio, DateTime FechaFinal, int IdPrograma)
        {
            try
            {
                return SPs.GetProveedoresPrefacturacionByFecha(FechaInicio, FechaFinal, IdPrograma).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProveedoresPrefacturacionByFecha() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static string GenerateBillForPreBilling(decimal totalBase, decimal iva, int idProveedor, int idPrograma, int idPrefactura)
        {
            try
            {
                FacturasBanco fb = new FacturasBanco();
                fb.GuidFacturaBanco = System.Guid.NewGuid().ToString();
                fb.ConsecutivoFacturaBanco = GetConsecutivoBanco(idPrograma).ValorActual;
                fb.TotalBase = totalBase;
                fb.Iva = iva;
                fb.FechaFacturaBanco = DateTime.Now;
                fb.IdProveedor = idProveedor;
                fb.IdPrograma = idPrograma;
                fb.IdPrefactura = idPrefactura;
                fb.Save();
                return fb.GuidFacturaBanco;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GenerateBillForPreBilling() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static Resoluciones GetConsecutivoBanco(int idPrograma)
        {
            try
            {
                //Cargamos la resolucion del programa para obtener todos los datos e incrementar el consecutivo
                Programas programa = new Programas(idPrograma);
                if (programa.IsLoaded)
                {
                    //Cargamos la resolucion a ser retornada
                    Resoluciones consecutivo = new Resoluciones(programa.IdResolucionBanco);
                    if (consecutivo.IsLoaded)
                    {
                        //Copiamos el objeto para retornarlo con el consecutivo tal como viene en base de datos
                        Resoluciones consRetornable = consecutivo.Clone();
                        //Sumamos 1 al consecutivo original
                        consecutivo.ValorActual++;
                        consecutivo.Save();
                        return consRetornable;
                    }
                    else
                        return null;
                }
                else
                    return null;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetConsecutivoBanco() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetProductosPrefacturablesByProveedor(DateTime FechaInicio, DateTime FechaFinal, int IdPrograma, int idProveedor)
        {
            try
            {
                return SPs.GetProductosPrefacturablesByProveedor(FechaInicio, FechaFinal, IdPrograma, idProveedor).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductosPrefacturablesByProveedor() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static void GenerateBillDetailForPreBilling(int cantidad, decimal costoBase, decimal iva, string guidFacturaBanco, string guidReferenciaProducto)
        {
            try
            {
                DetallesFacturaBanco dfb = new DetallesFacturaBanco();
                dfb.GuidDetalleFacturaBanco = System.Guid.NewGuid().ToString();
                dfb.Cantidad = cantidad;
                dfb.CostoBase = costoBase;
                dfb.Iva = iva;
                dfb.GuidFacturaBanco = guidFacturaBanco;
                dfb.GuidReferenciaProducto = guidReferenciaProducto;
                dfb.Save();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GenerateBillDetailForPreBilling() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        public static void ActualizarBankBill(string guidFacturaBanco, decimal totalBase, decimal iva)
        {
            try
            {
                FacturasBanco fb = new FacturasBanco(guidFacturaBanco);
                if (fb.IsLoaded)
                {
                    fb.TotalBase = totalBase;
                    fb.Iva = iva;
                    fb.Save();
                }
                else
                    throw new Exception("Se esta intentando actualizar una factura que no existe, error de inconsistencia de datos");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ActualizarBankBill() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        public static DateTime GetFechaFacturacion(int IdPrograma)
        {
            try
            {
                return (DateTime)SPs.SelectPrefacturacionFechainicio(IdPrograma).ExecuteScalar();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFechaFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);

                return DateTime.Now;
            }
        }

        public static int SetPrefacturacionFacturas(string GuidFactura)
        {
            try
            {
                Facturas colFacturas = new Facturas(GuidFactura);
                colFacturas.FechaPrefacturada = DateTime.Now;
                colFacturas.Prefacturada = true;
                colFacturas.Save();
                return 1;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SetPrefacturacionFacturas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return 0;
            }
        }
        public static int SetPrefacturacion(int IdPrograma, DateTime FechaInicio, DateTime FechaFinal)
        {
            try
            {
                Prefacturas colPrefacturas = new Prefacturas();
                colPrefacturas.IdPrograma = IdPrograma;
                colPrefacturas.FechaInicio = FechaInicio;
                colPrefacturas.FechaFin = FechaFinal;
                colPrefacturas.Save();
                return colPrefacturas.IdPrefactura;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SetPrefacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return 0;
            }
        }
        public static PrefacturasCollection GetPrefacturasPorPrograma(int IdPrograma)
        {
            try
            {
                PrefacturasCollection colPrefacturas = new PrefacturasCollection();
                return colPrefacturas.Where(Prefacturas.Columns.IdPrograma, IdPrograma).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo SetFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetFacturasPorFechaFacturacion(int IdPrograma, DateTime FechaInicio, DateTime FechaFin, int IdProveedor, string GuidProducto)
        {
            try
            {
                return SPs.SelectPrefacturacionFacturas(FechaInicio, FechaFin, IdPrograma, IdProveedor, GuidProducto).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasPorFechaFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetProveedoresPorFechaPrefacturacion(int IdPrograma, DateTime FechaInicio, DateTime FechaFin)
        {
            try
            {
                return SPs.SelectPrefacturacionProveedoresFechas(FechaInicio, FechaFin, IdPrograma).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProveedoresPorFechaPrefacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetFacturasPorProductosFechas(int IdPrograma, DateTime FechaInicio, DateTime FechaFin, int IdProveedor, string GuidProducto)
        {
            try
            {
                return SPs.SelectPrefacturacionFacturasProductos(FechaInicio, FechaFin, IdPrograma, IdProveedor, GuidProducto).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasPorFechaFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetProductosPorFechaProveedorPrefacturacion(int IdPrograma, DateTime FechaInicio, DateTime FechaFin, int IdProveedor)
        {
            try
            {
                return SPs.SelectPrefacturacionProductosProveedor(FechaInicio, FechaFin, IdPrograma, IdProveedor).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasPorFechaFacturacion() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        #region PrefacturacionEstablecimientos

        #region GetPeridodosPrefacturadosByPrograma(Overload)
        public static DataSet GetPeridodosPrefacturadosByPrograma(string nombrePrograma, int pageIndex, int pageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                Programas progs = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (progs.IsLoaded)
                {
                    SubSonic.StoredProcedure sp = SPs.PrefacturaGetPeriodosPrefacturados(progs.IdPrograma, TotalRecords);
                    sp.PageIndex = pageIndex;
                    sp.PageSize = pageSize;
                    sp.PagingResult = true;
                    DataSet ds = sp.GetDataSet();
                    int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                    return ds;
                }
                else
                    throw new Exception("El programa siendo accedido no existe");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPeridodosPrefacturadosByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetPeridodosPrefacturadosByPrograma(int idPrograma, int pageIndex, int pageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.PrefacturaGetPeriodosPrefacturados(idPrograma, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = pageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPeridodosPrefacturadosByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        #endregion

        public static DataSet GetPrimerDetallePrefactura(int idPrograma, int idMarca, int idPrefactura, int pageIndex, int pageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.PrefacturaGetPrimerDetalle(idPrograma, idPrefactura, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = pageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPrimerDetallePrefactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetPrimerDetallePrefacturaEstablecimiento(int idPrograma, int idMarca, int idPrefactura)
        {
            try
            {
                SubSonic.StoredProcedure sp = SPs.PrefacturaGetPrimerDetalleMarca(idPrograma, idMarca, idPrefactura);
                DataSet ds = sp.GetDataSet();
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetPrimerDetallePrefactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetSegundoDetallePrefactura(string guidDetalleFacturaEstablecimiento, int pageIndex, int pageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.PrefacturaGetSegundoDetalle(guidDetalleFacturaEstablecimiento, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = pageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetSegundoDetallePrefactura() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DateTime? GetFechaInicialFacturacionPuntos(int idPrograma)
        {
            try
            {
                return Convert.ToDateTime(SPs.GetFechaInicialEmpresa(idPrograma).ExecuteScalar());
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFechaInicialFacturacionPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DateTime? GetFechaInicialFacturacionEstablecimiento(int idPrograma)
        {
            try
            {
                return Convert.ToDateTime(SPs.GetFechaInicialEstablecimiento(idPrograma).ExecuteScalar());
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFechaInicialFacturacionEstablecimiento() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetAllDetailsByPeriodo(int idPeriodo)
        {
            try
            {
                return SPs.ReportGetAllDetallesPrefactura(idPeriodo).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetAllDetailsByPeriodo() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataSet GetAllDetailsByPeriodoByCedula(int idPeriodo, string cedula)
        {
            try
            {
                return SPs.ReportGetAllDetallesPrefacturaByCedula(idPeriodo,cedula).GetDataSet();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetAllDetailsByPeriodo() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PreBillingsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        #endregion
    }
}
