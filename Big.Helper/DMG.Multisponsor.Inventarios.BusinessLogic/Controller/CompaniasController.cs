using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
   public class CompaniasController
    {
       public static void AgregarEmpresas(string codigoEmpresa, string razonSocialEmpresa, string nitEmpresa
                                          , string direccionEmpresa, string telefonoEmpresa, string faxEmpresa
                                          , int ciudadEmpresa)
       {
           Empresas empresas = new Empresas();

           empresas.CodigoEmpresa = codigoEmpresa;
           empresas.RazonSocial = razonSocialEmpresa;
           empresas.Nit = nitEmpresa;
           empresas.Direccion = direccionEmpresa;
           empresas.Telefono = telefonoEmpresa;
           empresas.Fax = faxEmpresa;
           empresas.IdCiudad = ciudadEmpresa;
           empresas.Save();

       }

       public static bool VerificarNit(string nitEmpresa)
       {
           Empresas empresa = new Empresas(Empresas.Columns.Nit, nitEmpresa);
           if (!empresa.IsNew)
           {
               return false;
           }
           else
           {
               return true;
           }
           
       }

       public static void ActualizarEmpresas(int idEmpresa, string codigoEmpresa, string razonSocialEmpresa, string nitEmpresa
                                          , string direccionEmpresa, string telefonoEmpresa, string faxEmpresa
                                          , int ciudadEmpresa)
       {
           Empresas empresas = new Empresas(idEmpresa);

           if(empresas.IsLoaded)
           {
           empresas.CodigoEmpresa = codigoEmpresa;
           empresas.RazonSocial = razonSocialEmpresa;
           empresas.Nit = nitEmpresa;
           empresas.Direccion = direccionEmpresa;
           empresas.Telefono = telefonoEmpresa;
           empresas.Fax = faxEmpresa;
           empresas.IdCiudad = ciudadEmpresa;
           empresas.Save();
           }

       }

       public static DataSet CargarEmpresas(int parametroFantasma)
       {
           DataSet ds = SPs.SelectEmpresasProgramas(parametroFantasma).GetDataSet();
           return ds;
       }

       public static DataTable CargarDepartamentoPais(int idCiudad)
       {
         DataTable dt = SPs.SelectDepartamentoPais(idCiudad).GetDataSet().Tables[0];
           return dt;
       }

       public static Empresas CargarEmpresaPorId(int idEmpresa)
       {
           Empresas empresa = new Empresas(Empresas.Columns.IdEmpresa, idEmpresa);
           if (empresa.IsLoaded)
           {
             return empresa;
           }
           else
           {
               return null;
           }

       }

       public static DataSet GetEmpresasByCriterias(string codigoProveedor, string nit, string razonSocial, int pageIndex, int PageSize, out int TotalRecords)
       {
           TotalRecords = 0;
           try
           {
               SubSonic.StoredProcedure sp = SPs.SelectEmpresas(codigoProveedor, nit, razonSocial, TotalRecords);
               sp.PageIndex = pageIndex;
               sp.PageSize = PageSize;
               sp.PagingResult = true;
               DataSet ds = sp.GetDataSet();
               int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
               return ds;
           }
           catch (Exception ex)
           {
               //Construimos la excepcion a ser almacenada en un log
               StringBuilder exception = new StringBuilder();
               exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
               exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
               Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
               return null;
           }
       }

       public static Ciudades CargarCiudadPorId(int idCiudad)
       {
           Ciudades ciudad = new Ciudades(Ciudades.Columns.IdCiudad, idCiudad);
           if (ciudad.IsLoaded)
           {
               return ciudad;
           }
           else
           {
               return null;
           }

       }

       }
}