// =============================================
// Author:		C�sar A. L�pez A.
// Create date: 10 de Diciembre de 2008.
// Description:	InventariosController.cs
// Email: clopez@dmgcolombia.com
// =============================================

using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;
using DMG.Multisponsor.Inventarios.Logging;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class InventariosController
  {
    #region StaticMethods

    /// <summary>
    /// Consulta los Proveedores.
    /// </summary>
    /// <param name="parametroFantasma"></param>
    /// <returns>DataSet con los proveedores existentes</returns>
    public static DataSet GetProveedores()
    {
      try
      {
        return SPs.SelectProveedores(0).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProgramas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetProductos()
    {
      try
      {
        return SPs.SelectProductos(0).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProgramas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetSaldosByPrograma(int idPrograma, int idTipoBadega)
    {
      try
      {
        return SPs.SelectSaldosByPrograma(idPrograma, idTipoBadega).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProgramas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetReferenciasBodegas(int idBodega)
    {
      try
      {
        return SPs.AwReferenciasBodega(idBodega).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProgramas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }



    /// <summary>
    /// Consulta los Programas.
    /// </summary>
    /// <param name="parametroFantasma"></param>
    /// <returns>Colecci�n con los programas existentes</returns>
    public static DataSet GetProgramas()
    {
      try
      {
        return SPs.SelectProgramas(0).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProgramas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetBodegas()
    {
      try
      {
        return SPs.AwSelectBodegas(0).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetBodegas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    /// <summary>
    /// Consulta las Bodegas por Programa 
    /// </summary>
    /// <param name="idPrograma">Id Programa</param>
    /// <returns>Colecci�n con las bodegas filtradas por programa requerido</returns>
    public static DataSet GetBodegasPrograma(int idPrograma)
    {
      try
      {
        return SPs.SelectBodegasPrograma(idPrograma).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetBodegasPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    /// <summary>
    /// Consulta los Productos por Proveedor
    /// </summary>
    /// <param name="idProveedor">Id Proveedor</param>
    /// <returns>Colecci�n con los productos filtrados por el proveedor requerido</returns>
    public static DataSet GetProductosProveedor(int idProveedor, int idMarca)
    {
      try
      {
        return SPs.SelectProductosProveedor(idProveedor/*, idMarca*/).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProductosProveedor() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetProductosCarga(int idProveedor, int idMarca, int idPrograma)
    {
      try
      {
        return SPs.SelectProductosCarga(idProveedor, idMarca, idPrograma).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProductosCarga() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetFacturasProgramaProducto(string GuidProducto, int? IdPrograma)
    {
      try
      {
        return SPs.SelectFacturasProgramaProducto(GuidProducto, IdPrograma).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetFacturasProgramaProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetFacturasBodegasProductoByFechas(DateTime? FechaInicial, DateTime? FechaFinal)
    {
      try
      {
        DataSet ds = SPs.SelectFacturasBodegasProductos(FechaInicial, FechaFinal).GetDataSet();
        return ds;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetFacturasBodegasProductoByFechas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

      public static DataSet GetFacturasBodegasProductoByFechasAndFactura(string numeroFactura, DateTime? FechaInicial, DateTime? FechaFinal)
      {
          try
          {
              if (string.IsNullOrEmpty(numeroFactura))
                  numeroFactura = null;
              DataSet ds = SPs.SelectFacturasBodegasProductosAndFactura(numeroFactura, FechaInicial, FechaFinal).GetDataSet();
              return ds;
          }
          catch (Exception ex)
          {
              //Construimos la excepcion a ser almacenada en un log
              StringBuilder exception = new StringBuilder();
              exception.Append("Error en el metodo GetFacturasBodegasProductoByFechas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
              exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
              Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
              return null;
          }
      }

      public static DataSet GetFacturasBodegasProductoByFechasAndFacturaV2(string numeroFactura,int idProveedor, DateTime? FechaInicial, DateTime? FechaFinal)
      {
          try
          {
              if (string.IsNullOrEmpty(numeroFactura))
                  numeroFactura = null;
              DataSet ds = SPs.SelectFacturasBodegasProductosAndFacturaV2(numeroFactura,idProveedor, FechaInicial, FechaFinal).GetDataSet();
              return ds;
          }
          catch (Exception ex)
          {
              //Construimos la excepcion a ser almacenada en un log
              StringBuilder exception = new StringBuilder();
              exception.Append("Error en el metodo GetFacturasBodegasProductoByFechas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
              exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
              Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
              return null;
          }
      }

    public static DataSet GetDetalleFacturasProveedor(long NumeroFactura)
    {
      try
      {
        DataSet ds = SPs.CargaConsultarFactura(NumeroFactura).GetDataSet();
        return ds;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetDetalleFacturasProveedor() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }



    /// <summary>
    /// Consulta los Productos por Bodega
    /// </summary>
    /// <param name="idBodega"></param>
    /// <returns></returns>
    public static DataSet GetProductosBodegaProveedorMarca(int? idBodega, int? idProveedor, int? idMarca)
    {
      try
      {
        return SPs.SelectProductosBodegaProveedorMarca(idBodega, idProveedor/*, idMarca*/).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetProductosBodega() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    /// <summary>
    /// Consultar las Marcas
    /// </summary>
    /// <returns>Coleccion de las marcas.</returns>
    public static DataSet GetMarcas()
    {
      try
      {
        return SPs.SelectMarcas(0).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetMarcas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetMarcasPrograma(int? IdPrograma)
    {
      try
      {
        return SPs.SelectMarcasProgramas(IdPrograma).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetMarcas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetMarcasPrograma(int? IdPrograma, string nombre)
    {
        try
        {
            return SPs.SelectMarcasProgramas(IdPrograma, nombre).GetDataSet();
        }
        catch (Exception ex)
        {
            //Construimos la excepcion a ser almacenada en un log
            StringBuilder exception = new StringBuilder();
            exception.Append("Error en el metodo GetMarcas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            return null;
        }
    }

    public static DataSet GetMarcasProveedores(int IdProveedor)
    {
        try
        {
            return SPs.SelectMarcasProveedores(IdProveedor).GetDataSet();
        }
        catch (Exception ex)
        {
            //Construimos la excepcion a ser almacenada en un log
            StringBuilder exception = new StringBuilder();
            exception.Append("Error en el metodo GetMarcas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            return null;
        }
    }

    public static bool DelProductosBodegas(int idBodega, string GuidReferenciaProducto, int cantidad, string guidDetalleFactura)
    {
      try
      {

        BodegasProductos bodProdUpdate = new BodegasProductos(idBodega);
        bodProdUpdate.Cantidad += cantidad;
        bodProdUpdate.Save();
        HistoricoBodega.Delete(HistoricoBodega.Columns.GuidDetalleFactura, guidDetalleFactura);

        return true;
      }
      catch
      {
        return false;
      }
    }
    /// <summary>
    /// Ingresar Productos a la bodega y su historial
    /// </summary>
    /// <param name="idBodega">Id de la Bodega</param>
    /// <param name="guidProducto">Guid del Producto</param>
    /// <param name="cantidad">Cantidad del Producto</param>
    /// <param name="fecha">Fecha de Ingreso</param>
    /// <param name="numeroFactura">Numero de la Factura</param>
    /// <param name="idUsuario">Id del usuario que registra</param>
    /// <param name="observaciones">Observaciones adicionales</param>
    /// <param name="fechaVencimiento"></param>
    public static decimal SetProductosBodegas(int idBodega, string GuidReferenciaProducto, int cantidad, DateTime fecha, string guidDetalleFactura, long? numeroFactura, int? idUsuario, string observaciones, DateTime? fechaVencimiento, bool entrada, bool nuevo, int idHistoricoBodega)
    {
      try
      {
        decimal keyBodegaProducto = -1;
        BodegasProductosCollection colBodegaProductos = new BodegasProductosCollection();
        int cantidadItems = colBodegaProductos.Where(BodegasProductos.Columns.IdBodega, idBodega).Where(BodegasProductos.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load().Count;
        if (cantidadItems >= 1)
        {
          foreach (BodegasProductos bodProd in colBodegaProductos)
          {
            BodegasProductos bodProdUpdate = new BodegasProductos(bodProd.IdBodegaProducto);
            if (entrada == false)
            {
              bodProdUpdate.Cantidad = bodProd.Cantidad - cantidad;
            }
            else
            {
              bodProdUpdate.Cantidad = cantidad + bodProd.Cantidad;
            }
            bodProdUpdate.Save();
            keyBodegaProducto = bodProdUpdate.IdBodegaProducto;
          }
        }
        else if (cantidadItems == 0)
        {
          BodegasProductos bodProdInsert = new BodegasProductos();
          bodProdInsert.Cantidad = cantidad;
          bodProdInsert.IdBodega = idBodega;
          bodProdInsert.GuidReferenciaProducto = GuidReferenciaProducto;
          bodProdInsert.Save();
          keyBodegaProducto = bodProdInsert.IdBodegaProducto;
        }
        HistoricoBodega colHistoricoBodega = null;
        if (nuevo)
        {
          colHistoricoBodega = new HistoricoBodega();
          colHistoricoBodega.CantidadTransaccion = cantidad;
        }
        else
        {
          colHistoricoBodega = new HistoricoBodega(HistoricoBodega.Columns.IdHistoricoBodega, idHistoricoBodega);
          colHistoricoBodega.CantidadTransaccion = colHistoricoBodega.CantidadTransaccion + cantidad;
        }
        colHistoricoBodega.Entrada = entrada;
        colHistoricoBodega.Fecha = fecha;
        colHistoricoBodega.GuidDetalleFactura = guidDetalleFactura;
        colHistoricoBodega.IdUsuario = idUsuario;
        colHistoricoBodega.IdBodegaProducto = keyBodegaProducto;
        colHistoricoBodega.Observaciones = observaciones;
        colHistoricoBodega.NumeroFactura = numeroFactura;
        colHistoricoBodega.FechaVencimiento = fechaVencimiento;
        colHistoricoBodega.Save();
        return keyBodegaProducto;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo SetProductosBodegas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return -1;
      }
    }

    /// <summary>
    /// Ingresar Productos a la bodega y su historial
    /// </summary>
    /// <param name="idBodega">Id de la Bodega</param>
    /// <param name="guidProducto">Guid del Producto</param>
    /// <param name="cantidad">Cantidad del Producto</param>
    /// <param name="fecha">Fecha de Ingreso</param>
    /// <param name="numeroFactura">Numero de la Factura</param>
    /// <param name="idUsuario">Id del usuario que registra</param>
    /// <param name="observaciones">Observaciones adicionales</param>
    /// <param name="fechaVencimiento"></param>
    public static decimal SetProductosBodegasCarga(int idPrograma, int idBodega, string GuidReferenciaProducto, int cantidad, DateTime fecha, string guidDetalleFactura, long? numeroFactura, int? idUsuario, string observaciones, DateTime? fechaVencimiento, ref string mensaje, int tipoCarga, ref decimal IdRefBD)
    {
      decimal respuesta = -1;
      try
      {
        if (!CloseMonthlyController.SelectValidateCloseMonthlyByPrograma(idPrograma, fecha))
        {
          switch (tipoCarga)
          {
            case 0://Justificacion


              BodegasProductosCollection obj = new BodegasProductosCollection();
              obj.Where(BodegasProductos.Columns.GuidReferenciaProducto, GuidReferenciaProducto);
              obj.Where(BodegasProductos.Columns.IdBodega, idBodega);
              BodegasProductos objBodegasProductos = new BodegasProductos();
              foreach (BodegasProductos bod in obj.Load())
              {
                objBodegasProductos = bod;
              }

              if (!string.IsNullOrEmpty(objBodegasProductos.GuidReferenciaProducto))
              {
                objBodegasProductos.Cantidad = objBodegasProductos.Cantidad - cantidad;
                respuesta = objBodegasProductos.IdBodegaProducto;
                objBodegasProductos.Save();

                HistoricoBodega regHistoricoBodega = null;
                regHistoricoBodega = new HistoricoBodega();
                regHistoricoBodega.CantidadTransaccion = cantidad;
                regHistoricoBodega.Entrada = false;
                regHistoricoBodega.Fecha = fecha;
                regHistoricoBodega.GuidDetalleFactura = guidDetalleFactura;
                regHistoricoBodega.IdUsuario = idUsuario;
                regHistoricoBodega.IdBodegaProducto = respuesta;
                regHistoricoBodega.Observaciones = observaciones;
                regHistoricoBodega.NumeroFactura = numeroFactura;
                regHistoricoBodega.FechaVencimiento = fechaVencimiento;
                regHistoricoBodega.Save();
                respuesta = objBodegasProductos.IdBodegaProducto;
                mensaje = "Ok";
              }
              else
                mensaje = "La referencia no existe en la bodega selecciona.";

              break;
            case 1://Virtual
              respuesta = SetAWBodegaProductos(idBodega, GuidReferenciaProducto, cantidad);
              if (respuesta != -1)
              {
                DataSet ds = SPs.AwSelectHistoricoBodegaReferencia(idPrograma, GuidReferenciaProducto, null, respuesta).GetDataSet();
                if (ds.Tables[0].Rows.Count > 0) //Si existe suma
                {
                  HistoricoBodega objHistoricoBodega = new HistoricoBodega(ds.Tables[0].Rows[0]["IdHistoricoBodega"]);
                  objHistoricoBodega.CantidadTransaccion = objHistoricoBodega.CantidadTransaccion + cantidad;
                  objHistoricoBodega.Save();
                  mensaje = "Ok";
                }
                else //No existe Crea
                {
                  HistoricoBodega objHistoricoBodega = null;
                  objHistoricoBodega = new HistoricoBodega();
                  objHistoricoBodega.CantidadTransaccion = cantidad;
                  objHistoricoBodega.Entrada = true;
                  objHistoricoBodega.Fecha = fecha;
                  objHistoricoBodega.GuidDetalleFactura = guidDetalleFactura;
                  objHistoricoBodega.IdUsuario = idUsuario;
                  objHistoricoBodega.IdBodegaProducto = respuesta;
                  objHistoricoBodega.Observaciones = observaciones;
                  objHistoricoBodega.NumeroFactura = numeroFactura;
                  objHistoricoBodega.FechaVencimiento = fechaVencimiento;
                  objHistoricoBodega.Save();
                  IdRefBD = objHistoricoBodega.IdHistoricoBodega;
                  mensaje = "Ok";
                }
              }
              else
                mensaje = "Error";
              break;
            case 2://Real
              DataSet dsd = SPs.AwSelectHistoricoBodegaReferencia(idPrograma, GuidReferenciaProducto, numeroFactura, respuesta).GetDataSet();
              if (dsd.Tables[0].Rows.Count == 0)
              {
                respuesta = SetAWBodegaProductos(idBodega, GuidReferenciaProducto, cantidad);
                if (respuesta != -1)
                {
                  HistoricoBodega objHistoricoBodega = null;
                  objHistoricoBodega = new HistoricoBodega();
                  objHistoricoBodega.CantidadTransaccion = cantidad;
                  objHistoricoBodega.Entrada = true;
                  objHistoricoBodega.Fecha = fecha;
                  objHistoricoBodega.GuidDetalleFactura = guidDetalleFactura;
                  objHistoricoBodega.IdUsuario = idUsuario;
                  objHistoricoBodega.IdBodegaProducto = respuesta;
                  objHistoricoBodega.Observaciones = observaciones;
                  objHistoricoBodega.NumeroFactura = numeroFactura;
                  objHistoricoBodega.FechaVencimiento = fechaVencimiento;
                  objHistoricoBodega.Save();
                  IdRefBD = objHistoricoBodega.IdHistoricoBodega;
                  mensaje = "Ok";
                }
                else
                  mensaje = "Error";
              }
              else
                mensaje = "El producto ya se encuentra asociado a esta factura.";
              break;
          }
        }
        else
          mensaje = "Ya existe un periodo de cierre para esa fecha de factura proveedor";
        return respuesta;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo SetProductosBodegas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        mensaje = "Error";
        return -1;
      }
    }

    private static decimal SetAWBodegaProductos(int idBodega, string GuidReferenciaProducto, decimal cantidad)
    {
      decimal keyBodegaProducto = -1;
      BodegasProductosCollection colBodegaProductos = new BodegasProductosCollection();
      int cantidadItems = colBodegaProductos.Where(BodegasProductos.Columns.IdBodega, idBodega).Where(BodegasProductos.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load().Count;
      if (cantidadItems == 1)
      {
        foreach (BodegasProductos bodProd in colBodegaProductos)
        {
          BodegasProductos bodProdUpdate = new BodegasProductos(bodProd.IdBodegaProducto);
          keyBodegaProducto = bodProdUpdate.IdBodegaProducto;
          bodProdUpdate.Cantidad = cantidad + bodProd.Cantidad;
          bodProdUpdate.Save();
        }
      }
      else if (cantidadItems == 0)
      {
        BodegasProductos bodProdInsert = new BodegasProductos();
        bodProdInsert.Cantidad = cantidad;
        bodProdInsert.IdBodega = idBodega;
        bodProdInsert.GuidReferenciaProducto = GuidReferenciaProducto;
        bodProdInsert.Save();
        keyBodegaProducto = bodProdInsert.IdBodegaProducto;
      }
      return keyBodegaProducto;
    }

    public static decimal GetFactorPunto(string guidDetalleFactura)
    {
      decimal bla = 0;
      object factorPunto = SPs.SelectFactorPunto(guidDetalleFactura).ExecuteScalar();
      if (factorPunto != null)
        bla = Convert.ToDecimal(factorPunto);
      else
        bla = -1;

      return bla;
    }

    public static DataSet GetPreciosProveedorMarca(int idProveedor, int idMarca)
    {
      try
      {
        int? idProveedorLocal = null;
        int? idMarcaLocal = null;
        if (idProveedor != 0)
          idProveedorLocal = idProveedor;
        if (idMarca != 0)
          idMarcaLocal = idMarca;
        return SPs.GetProductosByProvAndMarca(idProveedorLocal, idMarcaLocal).GetDataSet();

      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetPreciosProveedorMarca() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static Productos GetPreciosId(string GuidProducto)
    {
      try
      {
        Productos producto = new Productos(Productos.Columns.Guid, GuidProducto);

        if (producto.IsLoaded)
        {
          return producto;
        }
        else
        {
          return null;
        }
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetPreciosId() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }
    public static DataSet GetPreciosDetallesId(string GuidProducto)
    {
      try
      {
        return SPs.GetCambiosBasePrecios(GuidProducto).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetPreciosDetallesId() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static decimal? GetCostoCambiado(string guidProducto, DateTime fechaCambio)
    {
      try
      {
        decimal costoCambiado = 0;
        if (Decimal.TryParse(SPs.GetCambiosCosto(fechaCambio, guidProducto).ExecuteScalar().ToString(), out costoCambiado))
          return costoCambiado;
        else
          return null;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetCostoCambiado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetPreciosCambiados(string guidProducto, DateTime fechaCambio)
    {
      try
      {
        return SPs.GetCambiosPrecios(fechaCambio, guidProducto).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetPreciosCambiados() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static void SetPrecioProducto(string GuidProducto, decimal PrecioCompra, List<PreciosProgramas> preciosVentaPorProgramas, int IdUsuario)
    {
      try
      {
        DateTime fechaCambio = DateTime.Now;
        Productos productos = new Productos(GuidProducto);
        if (productos.CostoBase != PrecioCompra)
        {
          HistoricosPrecio historicoPrecio = new HistoricosPrecio();
          historicoPrecio.GuidProducto = GuidProducto;
          historicoPrecio.CostoBase = productos.CostoBase;
          historicoPrecio.FechaCambio = fechaCambio;
          historicoPrecio.IdUsuarioCambio = IdUsuario;
          historicoPrecio.Activo = true;
          historicoPrecio.Save();
          
          productos.CostoBase = PrecioCompra;
          productos.Save();
        }
        foreach (PreciosProgramas pp in preciosVentaPorProgramas)
        {
          PreciosBaseProgramaCollection pbpCol = new PreciosBaseProgramaCollection();
          pbpCol.Where(PreciosBasePrograma.Columns.GuidProducto, GuidProducto).Where(PreciosBasePrograma.Columns.IdPrograma, pp.GuidPrograma).Load();
          if (pbpCol.Count > 0)
            foreach (PreciosBasePrograma pbp in pbpCol)
            {
              if (pbp.PrecioVentaBase != pp.PrecioBase || pbp.ValorAsegurado != pp.ValorAsegurado || pbp.CostoEnvioBase != pp.CostoEnvioBase || 
                    pbp.IdMarca != pp.GuidMarca || pbp.IdTipoDelivery != pp.GuidTipoDelivery || pbp.Direccion != pp.Direcciones || pbp.IdCiudad != pp.Ciudades)
              {
                HistoricosPrecio historicoPrecio = new HistoricosPrecio();
                historicoPrecio.GuidProducto = pbp.GuidProducto;
                historicoPrecio.GuidPrecioProducto = pbp.GuidPrecioProducto;
                historicoPrecio.PrecioBase = pbp.PrecioVentaBase;
                historicoPrecio.CostoEnvioBase = pbp.CostoEnvioBase;
                historicoPrecio.IdMarca = pbp.IdMarca;
                historicoPrecio.IdTipoDelivery = pbp.IdTipoDelivery;
                historicoPrecio.FechaCambio = fechaCambio;
                historicoPrecio.IdUsuarioCambio = IdUsuario;
                historicoPrecio.Activo = true;
                historicoPrecio.Save();
                pbp.PrecioVentaBase = pp.PrecioBase;
                pbp.CostoEnvioBase = pp.CostoEnvioBase;
                pbp.ValorAsegurado = pp.ValorAsegurado;
                pbp.IdMarca = pp.GuidMarca;
                pbp.IdTipoDelivery = pp.GuidTipoDelivery;
                pbp.Direccion = pp.Direcciones;
                pbp.IdCiudad = pp.Ciudades;
                pbp.Save();
              }
            }
          else
          {
            PreciosBasePrograma ppPrograma = new PreciosBasePrograma();
            ppPrograma.GuidPrecioProducto = System.Guid.NewGuid().ToString();
            ppPrograma.PrecioVentaBase = pp.PrecioBase;
            ppPrograma.CostoEnvioBase = pp.CostoEnvioBase;
            ppPrograma.ValorAsegurado = pp.ValorAsegurado;
            ppPrograma.GuidProducto = GuidProducto;
            ppPrograma.IdPrograma = pp.GuidPrograma;
            ppPrograma.IdMarca = pp.GuidMarca;
            ppPrograma.IdTipoDelivery = pp.GuidTipoDelivery;
            ppPrograma.Direccion = pp.Direcciones;
            ppPrograma.IdCiudad = pp.Ciudades;
            ppPrograma.Save();
          }
        }
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo SetPrecioProducto() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
      }
    }

    /// <summary>
    /// Ingresar Productos a la bodega y su historial
    /// </summary>
    /// <param name="idBodega">Id de la Bodega</param>
    /// <param name="guidProducto">Guid del Producto</param>
    /// <param name="cantidad">Cantidad del Producto</param>
    /// <param name="fecha">Fecha de Ingreso</param>
    /// <param name="numeroFactura">Numero de la Factura</param>
    /// <param name="idUsuario">Id del usuario que registra</param>
    /// <param name="observaciones">Observaciones adicionales</param>
    /// <param name="fechaVencimiento"></param>
    public static decimal SetProductosBodegasAjuste(int idBodega, string GuidReferenciaProducto, int cantidad, DateTime fecha, string guidDetalleFactura, long? numeroFactura, int? idUsuario, string observaciones, DateTime? fechaVencimiento, bool entrada, bool nuevo, int idHistoricoBodega)
    {
        try
        {
            decimal keyBodegaProducto = -1;
            BodegasProductosCollection colBodegaProductos = new BodegasProductosCollection();
            int cantidadItems = colBodegaProductos.Where(BodegasProductos.Columns.IdBodega, idBodega).Where(BodegasProductos.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load().Count;
            if (cantidadItems == 1)
            {
                foreach (BodegasProductos bodProd in colBodegaProductos)
                {
                    BodegasProductos bodProdUpdate = new BodegasProductos(bodProd.IdBodegaProducto);
                    if (entrada == false)
                    {
                        bodProdUpdate.Cantidad = bodProd.Cantidad - cantidad;
                    }
                    else
                    {
                        bodProdUpdate.Cantidad = cantidad + bodProd.Cantidad;
                    }
                    bodProdUpdate.Save();
                    keyBodegaProducto = bodProdUpdate.IdBodegaProducto;
                }
            }
            else if (cantidadItems == 0)
            {
                BodegasProductos bodProdInsert = new BodegasProductos();
                bodProdInsert.Cantidad = cantidad;
                bodProdInsert.IdBodega = idBodega;
                bodProdInsert.GuidReferenciaProducto = GuidReferenciaProducto;
                bodProdInsert.Save();
                keyBodegaProducto = bodProdInsert.IdBodegaProducto;
            }
            HistoricoBodega colHistoricoBodega = null;
            if (nuevo)
            {
                colHistoricoBodega = new HistoricoBodega();
                colHistoricoBodega.CantidadTransaccion = cantidad;
            }
            else
            {
                colHistoricoBodega = new HistoricoBodega(HistoricoBodega.Columns.IdHistoricoBodega, idHistoricoBodega);
                colHistoricoBodega.CantidadTransaccion = colHistoricoBodega.CantidadTransaccion + cantidad;
            }
            colHistoricoBodega.Entrada = entrada;
            colHistoricoBodega.Fecha = fecha;
            colHistoricoBodega.GuidDetalleFactura = null;
            colHistoricoBodega.IdUsuario = idUsuario;
            colHistoricoBodega.IdBodegaProducto = keyBodegaProducto;
            colHistoricoBodega.Observaciones = observaciones;
            colHistoricoBodega.NumeroFactura = numeroFactura;
            colHistoricoBodega.FechaVencimiento = fechaVencimiento;
            colHistoricoBodega.GuidDetalleAjuste = guidDetalleFactura;
            colHistoricoBodega.Save();
            return keyBodegaProducto;
        }
        catch (Exception ex)
        {
            //Construimos la excepcion a ser almacenada en un log
            StringBuilder exception = new StringBuilder();
            exception.Append("Error en el metodo SetProductosBodegas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            return -1;
        }
    }

    /// <summary>
    /// Ingresar Productos a la bodega y su historial relacionados con la factura de proveedores
    /// </summary>
    /// <param name="idBodega">Id de la Bodega</param>
    /// <param name="GuidReferenciaProducto">Guid del Producto</param>
    /// <param name="cantidad">Cantidad del Producto</param>
    /// <param name="fecha">Fecha de Ingreso</param>
    /// <param name="guidDetalleFacturaEmpresa">guid del detalle de la factura empresa</param>
    /// <param name="numeroFactura">Numero de la Factura</param>
    /// <param name="idUsuario">Id del usuario que registra</param>
    /// <param name="observaciones">Observaciones adicionales</param>
    /// <param name="fechaVencimiento"></param>
    /// <param name="entrada">para saber si es una entrada o salida</param>
    /// <param name="nuevo"></param>
    /// <param name="idHistoricoBodega"></param>
    /// <returns></returns>
   
      public static decimal SetProductosBodegasEmpresa(int idBodega, string GuidReferenciaProducto, int cantidad, DateTime fecha, string guidDetalleFacturaEmpresa, long? numeroFactura, int? idUsuario, string observaciones, DateTime? fechaVencimiento, bool entrada, bool nuevo, int idHistoricoBodega)
    {
        try
        {
            decimal keyBodegaProducto = -1;
            BodegasProductosCollection colBodegaProductos = new BodegasProductosCollection();
            int cantidadItems = colBodegaProductos.Where(BodegasProductos.Columns.IdBodega, idBodega).Where(BodegasProductos.Columns.GuidReferenciaProducto, GuidReferenciaProducto).Load().Count;
            if (cantidadItems == 1)
            {
                foreach (BodegasProductos bodProd in colBodegaProductos)
                {
                    BodegasProductos bodProdUpdate = new BodegasProductos(bodProd.IdBodegaProducto);
                    if (entrada == false)
                    {
                        bodProdUpdate.Cantidad = bodProd.Cantidad - cantidad;
                    }
                    else
                    {
                        bodProdUpdate.Cantidad = cantidad + bodProd.Cantidad;
                    }
                    bodProdUpdate.Save();
                    keyBodegaProducto = bodProdUpdate.IdBodegaProducto;
                }
            }
            else if (cantidadItems == 0)
            {
                BodegasProductos bodProdInsert = new BodegasProductos();
                bodProdInsert.Cantidad = cantidad;
                bodProdInsert.IdBodega = idBodega;
                bodProdInsert.GuidReferenciaProducto = GuidReferenciaProducto;
                bodProdInsert.Save();
                keyBodegaProducto = bodProdInsert.IdBodegaProducto;
            }
            HistoricoBodega colHistoricoBodega = null;
            if (nuevo)
            {
                colHistoricoBodega = new HistoricoBodega();
                colHistoricoBodega.CantidadTransaccion = cantidad;
            }
            else
            {
                colHistoricoBodega = new HistoricoBodega(HistoricoBodega.Columns.IdHistoricoBodega, idHistoricoBodega);
                colHistoricoBodega.CantidadTransaccion = colHistoricoBodega.CantidadTransaccion + cantidad;
            }
            colHistoricoBodega.Entrada = entrada;
            colHistoricoBodega.Fecha = fecha;
            colHistoricoBodega.GuidDetalleFacturaEmpresa = guidDetalleFacturaEmpresa;
            colHistoricoBodega.IdUsuario = idUsuario;
            colHistoricoBodega.IdBodegaProducto = keyBodegaProducto;
            colHistoricoBodega.Observaciones = observaciones;
            colHistoricoBodega.NumeroFactura = numeroFactura;
            colHistoricoBodega.FechaVencimiento = fechaVencimiento;
            colHistoricoBodega.Save();
            return keyBodegaProducto;
        }
        catch (Exception ex)
        {
            //Construimos la excepcion a ser almacenada en un log
            StringBuilder exception = new StringBuilder();
            exception.Append("Error en el metodo SetProductosBodegas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.InventariosController:\n");
            exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
            Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            return -1;
        }
    }
    #endregion
  }
}
