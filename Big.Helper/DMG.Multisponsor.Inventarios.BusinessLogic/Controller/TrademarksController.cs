using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;
namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class TrademarksController
    {
        public static void AgregarMarcas(string nombreMarca, int idPrograma, int idCiudad, string direccion, decimal? CI, decimal? prefijo, decimal? guiaInicial, decimal? guiaFinal)
        {
            try
            {
                Marcas marcas = new Marcas();
                marcas.NombreMarca = nombreMarca;
                marcas.IdPrograma = idPrograma;
                marcas.IdCiudad = idCiudad;
                marcas.Direccion = direccion;
                marcas.CiCoordinadora = CI;
                marcas.PrefijoGuias = prefijo;
                marcas.GuiaInicial = guiaInicial;
                marcas.GuiaFinal = guiaFinal;
                marcas.Save();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo AgregarMarcas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TrademarksController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }


        }

        public static Marcas GetMarcasById(int idmarca)
        {
            try
            {

                Marcas marcas = new Marcas(idmarca);
                return marcas;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetMarcasById() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TrademarksController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }

        }

        public static void ActualizarMarcas(int idMarca, string nombreMarca, int idPrograma, int idCiudad, string direccion, decimal? CI, decimal? prefijo, decimal? guiaInicial, decimal? guiaFinal)
        {
            try
            {
                Marcas marcas = new Marcas(idMarca);
                marcas.NombreMarca = nombreMarca;
                marcas.IdPrograma = idPrograma;
                marcas.IdCiudad = idCiudad;
                marcas.Direccion = direccion;
                marcas.CiCoordinadora = CI;
                marcas.PrefijoGuias = prefijo;
                marcas.GuiaInicial = guiaInicial;
                marcas.GuiaFinal = guiaFinal;
                marcas.Save();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo ActualizarMarcas() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TrademarksController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        public static Marcas GetMarcaByNombre(string nombreMarca)
        {
            try
            {
                Marcas mark = new Marcas(Marcas.Columns.NombreMarca, nombreMarca);
                if (!mark.IsNew)
                    return mark;
                else
                    throw new Exception("Una marca que no existe esta siendo accedida");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetMarcaByNombre() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TrademarksController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool VerificarNombreMarca(string nombreMarca)
        {
            Marcas marca = new Marcas(Marcas.Columns.NombreMarca, nombreMarca);
            if (!marca.IsNew)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public static MarcasCollection GetMarcasPorPrograma(int idPrograma)
        {
            try
            {
                MarcasCollection mc = new MarcasCollection();
                mc = mc.Where(Marcas.Columns.IdPrograma, idPrograma).Load();
                return mc;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetMarcasPorPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.TrademarksController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static DataTable GetMarcasPorIdPrograma(int idPrograma)
        {

                MarcasCollection mc = new MarcasCollection();
                mc = mc.Where(Marcas.Columns.IdPrograma, idPrograma);
                return mc.Load().ToDataTable(); ;
         
        }

        public static DataTable GetMarcasPorIdProgramaProveedor(int idPrograma, int idProveedor)
        {
            try
            {
                return SPs.GetMarcasPorIdProgramaProveedor(idPrograma, idProveedor).GetDataSet().Tables[0];
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProductos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProductsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
    }
}
