using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Configuration;
using DMG.Multisponsor.Inventarios.DataAccess.Core;


namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
  public class ReferencesController
  {
    public static MasEstadosCollection GetEstados()
    {
      try
      {
        return new MasEstadosCollection().Where(MasEstados.Columns.IdTipoEstado, 4).Load();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetEstados() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.Controller\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static void SetReferences(string GuidReferencias, string NombreReferencia, string GuidProducto, int IdEstado, int idPrograma)
    {
      try
      {
        ReferenciasProducto refer;
        if (string.IsNullOrEmpty(GuidReferencias))
        {
          refer = new ReferenciasProducto();
          refer.GuidReferenciaProducto = System.Guid.NewGuid().ToString();
        }
        else
        {
          refer = new ReferenciasProducto(GuidReferencias);
        }
        refer.NombreReferencia = NombreReferencia;
        refer.GuidProducto = GuidProducto;
        refer.IdEstado = IdEstado;
        refer.IdPrograma = idPrograma;
        refer.Save();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo SetReferences() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);

      }
    }

    public static string SetReferencesByGuid(string NombreReferencia, string GuidProducto, int IdEstado, int idPrograma)
    {
      try
      {
        ReferenciasProductoCollection col = new ReferenciasProductoCollection();
        col.Where(ReferenciasProducto.Columns.GuidProducto, GuidProducto);
        col.Where(ReferenciasProducto.Columns.IdPrograma, idPrograma);
        col.Where(ReferenciasProducto.Columns.NombreReferencia, NombreReferencia);

        if (col.Load().Count > 0)
        {
          return col[0].GuidReferenciaProducto;
        }
        else
        {
          ReferenciasProducto refer = new ReferenciasProducto();
          refer.GuidReferenciaProducto = Guid.NewGuid().ToString();
          refer.NombreReferencia = NombreReferencia;
          refer.GuidProducto = GuidProducto;
          refer.IdEstado = IdEstado;
          refer.IdPrograma = idPrograma;
          refer.Save();
          return refer.GuidReferenciaProducto;
        }
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo SetReferences() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }


    public static void UpdateReferences(string GuidReferences, string NombreReferencia, string GuidProducto, int IdEstado, int idPrograma)
    {
      try
      {
        ReferenciasProducto refer = new ReferenciasProducto(GuidReferences);
        refer.NombreReferencia = NombreReferencia;
        refer.GuidProducto = GuidProducto;
        refer.IdEstado = IdEstado;
        refer.IdPrograma = idPrograma;
        refer.Save();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo UpdateReferences() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
      }
    }



    public static DataSet GetReferenciasByProductoEstado(string GuidProducto, int? idProveedor)
    {
      try
      {
        return SPs.SelectReferenciaProductos(GuidProducto, idProveedor).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetReferenciasByProductoEstado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static DataSet GetReferenciasByProductoEstadoConExistencias(string GuidProducto, int? IdEstado)
    {
      try
      {
        return SPs.GetReferenciasActivasConExistenciasPorProducto(GuidProducto, IdEstado).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetReferenciasByProductoEstado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

      public static DataSet GetReferenciasByProductoByProgram(string GuidProducto, int? idPrograma)
      {
          try
          {
              return SPs.GetReferenciasPorProductoPorPrograma(GuidProducto, idPrograma).GetDataSet();
          }
          catch (Exception ex)
          {
              //Construimos la excepcion a ser almacenada en un log
              StringBuilder exception = new StringBuilder();
              exception.Append("Error en el metodo GetReferenciasByProductoByProgram() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
              exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
              Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
              return null;
          }
      }

    public static DataSet GetReferenciasByProductoEstadoNota(string GuidProducto, int? IdEstado)
    {
      try
      {
          return SPs.GetReferenciasPorProducto(GuidProducto, IdEstado).GetDataSet();
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetReferenciasByProductoEstado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static ReferenciasProducto GetReferenciasByGuid(string GuidReferencia)
    {
      try
      {
        ReferenciasProducto refer = new ReferenciasProducto(GuidReferencia);
        return refer;
      }
      catch (Exception ex)
      {
        //Construimos la excepcion a ser almacenada en un log
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo GetReferenciasByProductoEstado() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }

    public static bool VerificarReferencia(string nombreReferencia, string guidProducto, int idProveedor, int idPrograma)
    {
      DataSet ds = SPs.VerificarReferenciaProductos(nombreReferencia, guidProducto, idProveedor, idPrograma).GetDataSet();

      if (ds.Tables[0].Rows.Count == 0)
      {
        return false;
      }
      else
      {
        return true;
      }
    }

    public static DataTable ExportarReferencias(string productGuid, int? providerId)
    {
      try
      {
        return SPs.ExportarReferencias(productGuid, providerId).GetDataSet().Tables[0];
      }
      catch (Exception ex)
      {
        StringBuilder exception = new StringBuilder();
        exception.Append("Error en el metodo ExportarReferencias(string productGuid, int providerId) de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ReferencesController\n");
        exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
        Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
        return null;
      }
    }
  }
}
