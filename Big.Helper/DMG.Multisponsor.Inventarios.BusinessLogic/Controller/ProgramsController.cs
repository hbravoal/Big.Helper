using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Data;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class ProgramsController
    {
        public static Programas CargarProgramaPorId(int idPrograma)
        {
            Programas programa = new Programas(Programas.Columns.IdPrograma, idPrograma);
            if (programa.IsLoaded)
            {
                return programa;
            }
            else
            {
                return null;
            }

        }

        public static DataSet CargarResolucionesProgrmas(int idTipoResolucion)
        {
            DataSet ds = SPs.SelectResolucionesProgramas(idTipoResolucion).GetDataSet();
            return ds;
        }

        public static DataSet CargarResolucionesNotasCredito(int idTipoResolucion)
        {
            DataSet ds = SPs.SelectNotasCreditoProgramas(idTipoResolucion).GetDataSet();
            return ds;
        }

        //public static DataSet CargarProgramas(int idFantasma)
        //{
        //    DataSet ds = SPs.SelectProgramasNombres(idFantasma).GetDataSet();
        //    return ds;
        //}

        public static void AgregarProgramas(int codigoPrograma, string nombrePrograma
                                            , int idEmpresa, DateTime fechaInicio
                                            , int idResolucion, int idNotaCredito
                                            , int idResolucionBanco, decimal factorPunto
                                            , int redondeo, bool facturaProveedorPuntos
                                            , decimal porcentajeComision, bool cobrarEnvio
                                            , int plazoPago, int numeroPeriodosEstablecimiento
                                            , int numeroPeriodosProveedorPuntos, int idTipoFacturacion)
        {
            Programas programas = new Programas();

            programas.NombrePrograma = nombrePrograma;
            programas.IdEmpresa = idEmpresa;
            programas.FechaInicio = fechaInicio;
            programas.IdResolucion = idResolucion;
            programas.IdNotaCredito = idNotaCredito;
            programas.IdResolucionBanco = idResolucionBanco;
            programas.FactorPunto = factorPunto;
            programas.Redondeo = redondeo;
            programas.FacturaProveedorPuntos = facturaProveedorPuntos;
            programas.PorcentajeComision = porcentajeComision;
            programas.IdPrograma = codigoPrograma;
            programas.CobrarEnvio = cobrarEnvio;
            programas.IdTipoFacturacion = idTipoFacturacion;
            programas.PlazoVencimientoFactura = plazoPago;
            programas.NumeroPeriodosProveedorPuntos = numeroPeriodosProveedorPuntos;
            programas.NumeroPeriodosEstablecimientos = numeroPeriodosEstablecimiento;
            programas.Save();

        }

        public static void AgregarProgramas(int codigoPrograma, string nombrePrograma
                                            , int idEmpresa, DateTime fechaInicio
                                            , int idResolucion, int idNotaCredito
                                            , int idResolucionBanco, decimal factorPunto
                                            , int redondeo, bool facturaProveedorPuntos
                                            , decimal porcentajeComision, bool cobrarEnvio
                                            , int plazoPago, int numeroPeriodosEstablecimiento
                                            , int numeroPeriodosProveedorPuntos, int idTipoFacturacion
                                            , decimal ivaCurrier)
        {
            Programas programas = new Programas();

            programas.NombrePrograma = nombrePrograma;
            programas.IdEmpresa = idEmpresa;
            programas.FechaInicio = fechaInicio;
            programas.IdResolucion = idResolucion;
            programas.IdNotaCredito = idNotaCredito;
            programas.IdResolucionBanco = idResolucionBanco;
            programas.FactorPunto = factorPunto;
            programas.Redondeo = redondeo;
            programas.FacturaProveedorPuntos = facturaProveedorPuntos;
            programas.PorcentajeComision = porcentajeComision;
            programas.IdPrograma = codigoPrograma;
            programas.CobrarEnvio = cobrarEnvio;
            programas.IdTipoFacturacion = idTipoFacturacion;
            programas.PlazoVencimientoFactura = plazoPago;
            programas.NumeroPeriodosProveedorPuntos = numeroPeriodosProveedorPuntos;
            programas.NumeroPeriodosEstablecimientos = numeroPeriodosEstablecimiento;
            programas.IvaCurrier = ivaCurrier;
            programas.Save();

        }

        public static string NombreResolucion(int codigoResolucion)
        {
            Resoluciones nombreResolucion = new Resoluciones(Resoluciones.Columns.IdResolucion, codigoResolucion);
            if (nombreResolucion.IsLoaded)
            {
                return nombreResolucion.Resolucion;
            }

            return "";
        }

        public static void ActualizarProgramas(int idPrograma, string nombrePrograma
                                               , int idEmpresa, DateTime fechaInicio
                                               , int idResolucion, int idNotaCredito
                                               , int idResolucionBanco, decimal factorPunto
                                               , int redondeo, bool facturaProveedorPuntos
                                               , decimal porcentajeComision, bool cobrarEnvio
                                               , int plazoPago, int numeroPeriodosEstablecimiento
                                                , int numeroPeriodosProveedorPuntos, int idTipoFacturacion
                                                , decimal ivaCurrier)
        {
            Programas programas = new Programas(idPrograma);
            programas.NombrePrograma = nombrePrograma;
            programas.IdEmpresa = idEmpresa;
            programas.FechaInicio = fechaInicio;
            programas.IdResolucion = idResolucion;
            programas.IdNotaCredito = idNotaCredito;
            programas.IdResolucionBanco = idResolucionBanco;
            programas.FactorPunto = factorPunto;
            programas.Redondeo = redondeo;
            programas.FacturaProveedorPuntos = facturaProveedorPuntos;
            programas.PorcentajeComision = porcentajeComision;
            programas.CobrarEnvio = cobrarEnvio;
            programas.IdTipoFacturacion = idTipoFacturacion;
            programas.PlazoVencimientoFactura = plazoPago;
            programas.NumeroPeriodosProveedorPuntos = numeroPeriodosProveedorPuntos;
            programas.NumeroPeriodosEstablecimientos = numeroPeriodosEstablecimiento;
            programas.IvaCurrier = ivaCurrier;
            programas.Save();

        }

        /// <summary>
        /// Obtiene los datos basicos de los programas registrados
        /// </summary>
        /// <returns>Objeto tipo ProgramasCollection</returns>
        public static ProgramasCollection GetProgramasBasicos()
        {
            try
            {
                ProgramasCollection progs = new ProgramasCollection();
                progs.Where(Programas.Columns.Cerrado, false);
                return progs.OrderByAsc(Programas.Columns.NombrePrograma).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasBasicos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static ProgramasCollection GetProgramasBasicosByEstado(bool estado)
        {
            try
            {
                ProgramasCollection progs = new ProgramasCollection();
                progs.Where(Programas.Columns.Cerrado, estado);
                return progs.OrderByAsc(Programas.Columns.NombrePrograma).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasBasicos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
        public static ProgramasCollection GetProgramasFacturadoresAPuntos()
        {
            try
            {
                ProgramasCollection progs = new ProgramasCollection();
                progs.Where(Programas.Columns.FacturaProveedorPuntos, true);
                progs.Where(Programas.Columns.Cerrado, false);
                return progs.OrderByAsc(Programas.Columns.NombrePrograma).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasBasicos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static ProgramasCollection GetProgramasFacturadoresCurrierActivos()
        {
            try
            {
                ProgramasCollection progs = new ProgramasCollection();
                progs.Where(Programas.Columns.CobrarEnvio, false);
                progs.Where(Programas.Columns.Cerrado, false);
                return progs.OrderByAsc(Programas.Columns.NombrePrograma).Load();
            }

            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasBasicos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static ProgramasCollection GetProgramasFacturadoresAPuntosActivos()
        {
            try
            {
                ProgramasCollection progs = new ProgramasCollection();
                progs.Where(Programas.Columns.FacturaProveedorPuntos, true);
                progs.Where(Programas.Columns.Cerrado, false);
                return progs.OrderByAsc(Programas.Columns.NombrePrograma).Load();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasFacturadoresAPuntosActivos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }


        public static DataSet GetTiposFacturacion()
        {
            return SPs.SelectTiposFactura().GetDataSet();
        }

        //public static DataSet GetProgramsEntries(int entryState, int pageIndex, int pageSize, out int totalRecords)
        //{
        //    totalRecords = 0;
        //    SubSonic.StoredProcedure sp = SPs.SelectProgramasNombres(entryState, totalRecords);
        //    sp.PageIndex = pageIndex;
        //    sp.PageSize = pageSize;
        //    sp.PagingResult = true;
        //    DataSet ds = sp.GetDataSet();
        //    int.TryParse(sp.OutputValues[0].ToString(), out totalRecords);
        //    return ds;
        //}

        /// <summary>
        /// 
        /// </summary>
        /// <param name="nombrePrograma"></param>
        /// <returns></returns>
        public static Programas GetProgramaByNombre(string nombrePrograma)
        {
            try
            {
                Programas progs = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
                if (!progs.IsNew)
                    return progs;
                else
                    throw new Exception("Un programa que no existe esta siendo accedido en este metodo");
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramaByNombre() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool VerificarNombrePrograma(string nombrePrograma, string codigoPrograma)
        {
            Programas programa = new Programas(Programas.Columns.NombrePrograma, nombrePrograma);
            Programas programaCodigo = new Programas(Programas.Columns.IdPrograma, codigoPrograma);
            if (programa.IsNew && programaCodigo.IsNew)
            {
                return false;
            }
            else
            {
                return true;
            }

        }

        public static DataSet GetProgramasByCriterias(string nombrePrograma, int? idCliente, int pageIndex, int PageSize, out int TotalRecords)
        {
            TotalRecords = 0;
            try
            {
                SubSonic.StoredProcedure sp = SPs.SelectProgramasNombres(nombrePrograma, idCliente, TotalRecords);
                sp.PageIndex = pageIndex;
                sp.PageSize = PageSize;
                sp.PagingResult = true;
                DataSet ds = sp.GetDataSet();
                int.TryParse(sp.OutputValues[0].ToString(), out TotalRecords);
                return ds;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetFacturasByPrograma() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static bool VerificarDelivery(int idMarca)
        {
            try
            {
                Marcas establecimiento = new Marcas(idMarca);
                if (establecimiento.IsNew)
                    throw new Exception("La marca al que tratan de verificar delivery no existe.");
                bool retorno = establecimiento.CiCoordinadora.HasValue & establecimiento.GuiaInicial.HasValue & establecimiento.GuiaFinal.HasValue & establecimiento.PrefijoGuias.HasValue;
                return retorno;
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo VerificarDelivery() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.FacturacionController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return false;
            }
        }

        public static ProgramasCollection GetProgramasByEmpresa(int idEmpresa)
        {
            try
            {
                ProgramasCollection proCol = new ProgramasCollection();
                proCol.Where(Programas.Columns.IdEmpresa, idEmpresa);
                return proCol.OrderByAsc(Programas.Columns.NombrePrograma).Load();
                if (proCol.Count == 0)
                {
                    return null;
                }
                else
                {
                    return proCol;
                }


            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasByEmpresa() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }

        public static ProgramasCollection GetProgramasByEmpresaActivo(int idEmpresa)
        {
            try
            {
                ProgramasCollection proCol = new ProgramasCollection();
                proCol.Where(Programas.Columns.IdEmpresa, idEmpresa);
                proCol.Where(Programas.Columns.Cerrado, false);
                proCol.Load().OrderByAsc("NOMBRE_PROGRAMA");
                if (proCol.Count == 0)
                {
                    return null;
                }
                else
                {
                    return proCol;
                }


            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GetProgramasByEmpresa() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.ProgramsController:\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
    }
}
