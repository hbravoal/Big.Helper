using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Text;
using DMG.Multisponsor.Inventarios.DataAccess.Core;

namespace DMG.Multisponsor.Inventarios.BusinessLogic
{
    public class PointsProviderController
    {
        public static void Ejemplo()
        {
            try
            {

            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo Ejemplo() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PointsProviderController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
            }
        }

        public static string GenerarRangoFacturacionProveedorPuntos(int idPrograma, DateTime fechaInicial, DateTime fechaFinal)
        {
            try
            {
                PeriodoFacturacionPuntos periodo = new PeriodoFacturacionPuntos();
                periodo.IdPrograma = idPrograma;
                periodo.FechaInicio = fechaInicial;
                periodo.FechaFin = fechaFinal;
                periodo.Save();

                //Traer transacciones del periodo

                //Generar la factura

                return periodo.IdFacturacionPuntos.ToString();
            }
            catch (Exception ex)
            {
                //Construimos la excepcion a ser almacenada en un log
                StringBuilder exception = new StringBuilder();
                exception.Append("Error en el metodo GenerarRangoFacturacionProveedorPuntos() de la clase DMG.Multisponsor.Inventarios.BusinessLogic.PointsProviderController\n");
                exception.Append(ex.Message).Append("\n").Append(ex.StackTrace);
                Logging.EventLoggerHelper.WriteError(exception.ToString(), ConfigurationManager.AppSettings["EventSource"]);
                return null;
            }
        }
    }
}
